﻿#include "stdafx.h"
#include "myclass.h"
#include "dictionaryForm.h"
#include "Mail.h"
#include "NumberAndTimestamp.h"
#include "spravochnik_tara.h"
#include "options.h"
#include "HiddenWeighing.h"
#include "registration_rfid.h"
#include "reports_rfid.h"
#include "ui_reports_rfid.h"

#include <ActiveQt/qaxobject.h>
#include <ActiveQt/qaxbase.h>

#include <QLibrary>

// добить шлагбаумы
#define RELEASE  	// RELEASE DEBUG 
#define PROG_VERSION "1.0.5"

#define INTERVAL_REQUEST 250 // интервал запросов в COM-порт НЕ МЕНЯЙ! ИЛИ ВНИМАТЕЛЬНО СМОТРИ ГДЕ ИСПОЛЬЗУЕТСЯ!!!

#define TIMEOUT_NO_SCALES_CONNECTION 50 // TIMEOUT_NO_SCALES_CONNECTION * SYS_TYCK_TIME  timeOut отсутствия связи с ВП-xx
#define SYS_TYCK_TIME 100 // тик системного таймера

//#pragma comment(lib,"MyDll.lib")

currentSettingsCOM_Port *currSettingsComPortPtr; // объявляем класс текущих настроек COM-порта
currentSettingsIP_Cam *currentSettingsIP_CamPtr; // объявляем класс текущих настроек IP-камеры
SettingsForm *settings;
acceptionForm *acceptform; // Клас форм приёмки/отправки автомобилей
acceptFormModel *acceptFormModelObject; // модель формы приёмки/отправки автомобилей
imageSaveModel *imageSaveModelObject; // модель таблицы хранения фотоснимков
dictionaryFormClass *dictionaryFormObject; // объект работы со справочниками

dialogVPSensors *dialogVPSensorsObj;
reportFormClass *reportFormObject; // Указатель на объект формирования отчётов
mysqlProccess *mysqlProccessObject; // объект для работы с БД MySQL
loginWindowClass *loginWindowObject;
logEventsFormClass *logEventsFormObject;
Form_ttn *formTTN_Object;
spravochnik_tara *spravochnikTaraObj;

options* optionsObj;

registration_rfid *registration_rfidObj;
reports_rfid *reports_rfidObj;
cardReaderRfid *cardReaderRfidObj;

enum StatesWeight statesWeight;

struct Record record;
struct Photofix_data photofix_struct;
struct File_data file_struct;

static int focus_cam = 0;


MyClass::MyClass(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags),
	udp_socket( 0 ),
	flag_UDP_listening_state( false )
{
	ui.setupUi(this);
	progrBarMainTable = new QProgressBar();
	progrBarMainTable->resize(180, 34);
	progrBarReportTable = new QProgressBar();
	progrBarReportTable->resize(180, 34);
	tableViewMain = new QTableView;
	tableViewReports = new QTableView;

	//tableViewReports->setVerticalScrollBarPolicy( Qt::ScrollBarAsNeeded /*ScrollBarAlwaysOn */);
	//tableViewReports->setVerticalScrollMode(QAbstractItemView::/*ScrollPerPixel*/ScrollPerItem);
	//tableViewReports->setAutoScroll(true);
    //tableViewReports->setAutoScrollMargin(200);
	//tableViewReports->verticalScrollBar()->setEnabled( true );

	tableViewReports->setVisible( false );

	//mysqlProccessObject = new mysqlProccess();
	CreateControlsMainWindow( ui.centralWidget ); // Create controls of the Main  Window

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	isVPConnected = true;

	//Проверка триал версии
	if( !optionsObj->isCheked[optionsObj->modules.indexOf("PO")] )
	{
		if( !mysqlProccessObject->read_trial() )
		{
			int result = QMessageBox::question( NULL,
				tr("Software License"),
				tr("The trial version has expired!\nPlease activate the software\nTechnical support - 093 009 9990 \nProm-Soft"),
			QMessageBox::Ok, QMessageBox::Cancel );
			QString dt =  mysqlProccessObject->set_time_leable();
			qDebug()<<dt;
			optionsObj->set_Trial_Time( dt );

			if( result == QMessageBox::Ok )
			{
				buttonAutoComeIn->setEnabled( false );
				buttonAutoComeOut->setEnabled( false );
				optionsObj->show();
			}
			else
			{
				buttonAutoComeIn->setEnabled( false );
				buttonAutoComeOut->setEnabled( false );
			}
		}
		else
		{
			timerTrial = new QTimer( this );
	        timerTrial->setInterval( 900000 );//900000
			connect( timerTrial, SIGNAL( timeout() ), this, SLOT( timer_Trial_Enable() ) );
			timerTrial->start();
			QString dt =  mysqlProccessObject->set_time_leable();
			qDebug()<<dt;
			optionsObj->set_Trial_Time( dt );

		}
	}
	else
	{
		QString dt =  mysqlProccessObject->set_time_leable();
		qDebug()<<dt;
		optionsObj->set_Trial_Time( dt );
	}

	hiddenWeighing = new HiddenWeighing;
	connect( hidden_action, SIGNAL( triggered() ), hiddenWeighing, SLOT( show() ) );
	connect( this, SIGNAL( onBoard_Signal(int) ), hiddenWeighing, SLOT( detectOn_Slot(int) ) );
	connect( this, SIGNAL( offBoard_Signal() ), hiddenWeighing, SLOT( detectOff_Slot() ) );
	connect( this, SIGNAL( reported_Signal() ), hiddenWeighing, SLOT( reported_Slot() ) );
	connect( hiddenWeighing, SIGNAL( makeHiddenPhotos() ), this, SLOT( makeHiddenPhotos_Slot() ) );

	for( int i = 0; i < 4; ++i )
	{
		QNetworkAccessManager *m = new QNetworkAccessManager( this );
		m->setObjectName( QString( "nam_%1" ).arg( i ) );
		connect( m, SIGNAL( finished( QNetworkReply* ) ), this, SLOT( finishedHTTP_Hidden_Cams( QNetworkReply* ) ) );
		networkManagersPhotofix.append( m );

		QNetworkAccessManager *mh = new QNetworkAccessManager( this );
		mh->setObjectName( QString( "namh_%1" ).arg( i ) );
		connect( mh, SIGNAL( finished( QNetworkReply* ) ), this, SLOT( finishedHTTP_Hidden_Cams( QNetworkReply* ) ) );
		networkManagersPhotofixHidden.append( mh );
	}



	bool flag1 = QObject::connect(&threadSavePhotos, SIGNAL(started()), this, SLOT(timerSendMail_Slot()));
    bool flag2 = QObject::connect(this, SIGNAL(finishedSavePhotos()), &threadSavePhotos, SLOT(quit()));

	connect( this, SIGNAL( sayAboutZero_Signal(bool) ), this, SLOT( sayAboutZero_Slot(bool) ) );
	zeroBlock = false;
	isPositionOK = false;

	// Управление светофорами
	settings->initialised_bk01 = false;
	// Инициализация флагов отсутствия связи по UART с весами
	memset( flagsNoUarts_connection, false, sizeof( flagsNoUarts_connection ) );

	formTTN_Object = new Form_ttn( this );

	for( int i = 0; i < settings->getNumVP(); i++ )
	{
		vectorFlagsNoUARTs_Connection.insert( i, &flagsNoUarts_connection[ i ] );
	    timeoutsNoUartConnection[ i ] = 0;
	}

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	this->rowAutoRecept = -1;
	this->columnAutoRecept = -1;
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	loginWindowObject = new loginWindowClass(); // создать окно для входа в систему
	loginWindowObject->loginWin->installEventFilter( this );
	//
	QRect screenResolution = qApp->desktop()->screenGeometry();

	loginWindowObject->show();

	connect( loginWindowObject->confirmButton, SIGNAL( clicked() ), this, SLOT( showMainWindow() ) );

	registration_rfidObj = new registration_rfid();
	reports_rfidObj = new reports_rfid();

	//Коннект слотов для просмотра и сохранения фота в режиме Rfid
	connect( mysqlProccessObject, SIGNAL( showPhotoReports_Signal() ), this, SLOT( showPhotoReports()));
	connect( mysqlProccessObject, SIGNAL( savePhotoReports_Signal() ), this, SLOT( savePhotoReports_Rfid())); 
	//
	networkAccessManager_01 = new QNetworkAccessManager( this );
	QObject::connect( networkAccessManager_01, SIGNAL( finished( QNetworkReply* ) ), this, SLOT( finishedHTTP_Request_Cam_01( QNetworkReply* ) ) ); // обработка ответа от IP-камеры №1

	networkAccessManager_02 = new QNetworkAccessManager( this );
	QObject::connect( networkAccessManager_02, SIGNAL( finished( QNetworkReply* ) ), this, SLOT( finishedHTTP_Request_Cam_02( QNetworkReply* ) ) ); // обработка ответа от IP-камеры №2

	networkAccessManager_03 = new QNetworkAccessManager( this );
	QObject::connect( networkAccessManager_03, SIGNAL( finished( QNetworkReply* ) ), this, SLOT( finishedHTTP_Request_Cam_03( QNetworkReply* ) ) ); // обработка ответа от IP-камеры №3

	networkAccessManager_04 = new QNetworkAccessManager( this );
	QObject::connect( networkAccessManager_04, SIGNAL( finished( QNetworkReply* ) ), this, SLOT( finishedHTTP_Request_Cam_04( QNetworkReply* ) ) ); // обработка ответа от IP-камеры №4
	//

	// Окно отчётов
	reportFormObject = new reportFormClass( ui.centralWidget, this );
	dictionaryFormObject = new dictionaryFormClass( this );
	dictionaryFormObject->setObjectName( "dictionaryWindow" );

	readDictionariesFromDB_ALL();
	ui.centralWidget->installEventFilter( this );

	// ожидалка
	waitW = new QWidget();
	//waitW->
	waitW->setWindowTitle( tr( "Please wait until the process ends." ) );
	waitW->setWindowFlags( Qt::CustomizeWindowHint );
	waitW->setWindowFlags( Qt::WindowTitleHint );
	waitW->setFixedSize( 400, 1 );
	waitW->resize( 400, 1 );
	waitW->setWindowModality(Qt::ApplicationModal);
	waitW->hide();

	// окно дампа базы
	dialogDBdump = new QDialog( this );
	dialogDBdump->setWindowTitle( tr( "Database backup" ) );
	dialogDBdump->setGeometry( screenResolution.width() / 2 - 200, screenResolution.height() / 2 - 50, 400, 100 );
	//QIcon icon;
	//icon.setPixmap( "image/png", QIcon::Large, QIcon::Normal, QIcon::On );
	//dialogDBdump->setWindowIcon( icon );

	QLabel *dbDumpLabel = new QLabel( tr( "Push the button to start the process. It can take a long time." ) );
	dbPhoto = new QCheckBox( tr( "Save photos" ) );
	dbPhoto->setChecked( false );

	QPushButton *dumpButton = new QPushButton( tr( "Dump" ) );
	connect( dumpButton, SIGNAL( clicked() ), this, SLOT( dbBackup_Slot() ) );

	QVBoxLayout *dbDumpLayout = new QVBoxLayout( dialogDBdump );
	dbDumpLayout->addWidget( dbDumpLabel );
	dbDumpLayout->addWidget( dbPhoto );
	dbDumpLayout->addWidget( dumpButton );

	dialogDBdump->hide();

	// окно выбора рестора базы
	dialogDB = new QDialog( this );
    dialogDB->setWindowTitle( tr( "Database restore" ) );
	dialogDB->setGeometry( screenResolution.width() / 2 - 200, screenResolution.height() / 2 - 300, 400, 600 );
	//dialogDB->setModal( true );
	//icon.setPixmap( "image/png", QIcon::Large, QIcon::Normal, QIcon::On );
	//dialogDB->setWindowIcon( icon );

	QLabel *dbLabel = new QLabel( tr( "Select the file to restore. Be careful! Current data will be deleted!" ) );

	listWidget = new QListWidget;
	QDir dir( "backup" );
    dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
    QFileInfoList list = dir.entryInfoList();
    for( int i = 0; i < list.size(); ++i ) {
        QFileInfo fileInfo = list.at(i);
		listWidget->addItem( QString( "%1" ).arg( fileInfo.fileName() ) );
    }

	QPushButton *restoreButton = new QPushButton( tr("Restore") );
	connect( restoreButton, SIGNAL( clicked() ), this, SLOT( dbRestore_Slot() ) );

	QVBoxLayout *dbLayout = new QVBoxLayout( dialogDB );
	dbLayout->addWidget( dbLabel );
	dbLayout->addWidget( listWidget );
	dbLayout->addWidget( restoreButton );

	dialogDB->hide();

	tableViewReports->hide();

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// Повторная проверка соединения с БД

	if (mysqlProccessObject->isConnected()) //->connect()->isOpen() )
	{
		showDataBaseContent_Slot(); // отобразить записи в таблице приёмки/отправки автомобилей
		mysqlProccessObject->getReportsTableData(reports_rfidObj->getHeadersReportsTable(), tableViewReports, progrBarReportTable, QList< QString >());
	}

	connect(this, SIGNAL(writeSensors_Signal(QVector<int>, QVector<int>)), mysqlProccessObject, SLOT(writeSensors_Slot(QVector<int>, QVector<int>)));
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// Запуск таймера опрCOM-портов
	initTimerRequestCOM_Port();

	// таймер защиты от двойного поднесения карты
	timerRepeatedRFid = new QTimer(this);
	timerRepeatedRFid->setInterval(3 * 1000 * 60);
	timerRepeatedRFid->setSingleShot(true);
	//connect( timerRepeatedRFid, SIGNAL( timeout() ), this, SLOT( Timer_Slot() ) );
	//timerRepeatedRFid->start();

	// Добавить обработку отсылки почты
	//mailObject = new Mail();
	//addMailThread();

	timerSendMail = new QTimer( this );
	timerSendMail->setInterval( 1000 );
	timerSendMail->setSingleShot(false);

	timerSendMailEachRec = new QTimer( this );
	timerSendMailEachRec->setInterval( 10000 );
	timerSendMailEachRec->setSingleShot(true);

	if (settings->checkBoxSendEachRec->isChecked())
		QObject::connect( timerSendMailEachRec, SIGNAL( timeout() ), this, SLOT( /*timerSendMail_Slot()*/ startThreadSendEmail_Slot() ) );
	else
	{
		bool flag123 = QObject::connect( timerSendMail, SIGNAL( timeout() ), this, SLOT( /*timerSendMail_Slot() */ startThreadSendEmail_Slot() ) );
		timerSendMail->start();
	}

	QObject::connect( settings->checkBoxSendEachRec, SIGNAL( stateChanged(int) ), this, SLOT( SendMailMode_Slot(int) ) );

	timerServiceValue = 2000; // 10 мин

	connect( this, SIGNAL( cursorChange_Signal( bool ) ), this, SLOT( cursorChange_Slot( bool ) ) );

	udp_socket = new QUdpSocket( this );
	bool flag = QObject::connect( udp_socket, SIGNAL( readyRead() ), this, SLOT( UDP_DataAvalable_Slot() ) );

	// SOCKET2_1
	timerSocket2_1 = new QTimer( this );
	timerSocket2_1->setInterval( 500 );
	connect( timerSocket2_1, SIGNAL( timeout() ), this, SLOT( timerSocket2_1_Slot() ) );

	tcpSocket2_1 = new QTcpSocket( this );
	connect( tcpSocket2_1, SIGNAL( readyRead() ), this, SLOT( readSocket2_1_Slot() ) );
	connect( tcpSocket2_1, SIGNAL( error( QAbstractSocket::SocketError ) ), this, SLOT( socketError_Slot( QAbstractSocket::SocketError ) ) );
	if( settings->chkboxEnableVk1->isChecked() )
	{
		tcpSocket2_1->connectToHost( settings->enterVk1->text(), 9761 );
	}

	// SOCKET2_2
	timerSocket2_2 = new QTimer( this );
	timerSocket2_2->setInterval( 500 );
	connect( timerSocket2_2, SIGNAL( timeout() ), this, SLOT( timerSocket2_2_Slot() ) );

	tcpSocket2_2 = new QTcpSocket( this );
	connect( tcpSocket2_2, SIGNAL( readyRead() ), this, SLOT( readSocket2_2_Slot() ) );
	connect( tcpSocket2_2, SIGNAL( error( QAbstractSocket::SocketError ) ), this, SLOT( socketError_Slot( QAbstractSocket::SocketError ) ) );
	if( settings->chkboxEnableVk2->isChecked() )
	{
		tcpSocket2_2->connectToHost( settings->enterVk2->text(), 9761 );
	}

	// SOCKET2_3
	timerSocket2_3 = new QTimer( this );
	timerSocket2_3->setInterval( 500 );
	connect( timerSocket2_3, SIGNAL( timeout() ), this, SLOT( timerSocket2_3_Slot() ) );

	tcpSocket2_3 = new QTcpSocket( this );
	connect( tcpSocket2_3, SIGNAL( readyRead() ), this, SLOT( readSocket2_3_Slot() ) );
	connect( tcpSocket2_3, SIGNAL( error( QAbstractSocket::SocketError ) ), this, SLOT( socketError_Slot( QAbstractSocket::SocketError ) ) );
	if( settings->chkboxEnableVk3->isChecked() )
	{
		tcpSocket2_3->connectToHost( settings->enterVk3->text(), 9761 );
	}

	nextBlockSize1 = 0;
	nextBlockSize2 = 0;
	nextBlockSize3 = 0;

	// SOCKET2_POSITION
	timer_PositionS2 = new QTimer( this );
	timer_PositionS2->setInterval( 500 );
	connect( timer_PositionS2, SIGNAL( timeout() ), this, SLOT( timer_PositionS2_Slot() ) );

	tcpSocket_PositionS2 = new QTcpSocket( this );
	connect( tcpSocket_PositionS2, SIGNAL( readyRead() ), this, SLOT( readSocket_PositionS2_Slot() ) );
	connect( tcpSocket_PositionS2, SIGNAL( error( QAbstractSocket::SocketError ) ), this, SLOT( socketError_PositionS2_Slot( QAbstractSocket::SocketError ) ) );
	if( settings->group_PositionS2->isChecked() )
	{
		tcpSocket_PositionS2->connectToHost( settings->lineEdit_PositionS2->text(), 9761 );
		timer_PositionS2->start();
	}
	// SOCKET2_Lamps
	timer_Lamps = new QTimer( this );
	timer_Lamps->setInterval( 300 );
		
	tcpSocket_Lamps = new QTcpSocket( this );
	connect( tcpSocket_Lamps, SIGNAL( readyRead() ), this, SLOT( readSocket_Lamps_Slot() ) );
	connect( tcpSocket_Lamps, SIGNAL( error( QAbstractSocket::SocketError ) ), this, SLOT( socketError_Lamps_Slot( QAbstractSocket::SocketError ) ) );
	if( settings->group_Rfid->isChecked() )
	{
		connect( timer_Lamps, SIGNAL( timeout() ), this, SLOT( timer_Lamps_Slot() ) );
		tcpSocket_Lamps->connectToHost( settings->lineEdit_Lamps->text(), 9761 );
		timer_Lamps->start();

		set_Lamp( 0, 0 );
		set_Lamp( 1, 0 );
	}
	// SOCKET2_SVETOFOR
	timer_SvetoforS2 = new QTimer( this );
	timer_SvetoforS2->setInterval( 300 );
	connect( timer_SvetoforS2, SIGNAL( timeout() ), this, SLOT( timer_SvetoforS2_Slot() ) );

	tcpSocket_SvetoforS2 = new QTcpSocket( this );
	connect( tcpSocket_SvetoforS2, SIGNAL( readyRead() ), this, SLOT( readSocket_SvetoforS2_Slot() ) );
	connect( tcpSocket_SvetoforS2, SIGNAL( error( QAbstractSocket::SocketError ) ), this, SLOT( socketError_SvetoforS2_Slot( QAbstractSocket::SocketError ) ) );
	if( settings->group_SvetoforS2->isChecked() )
	{
		tcpSocket_SvetoforS2->connectToHost( settings->lineEdit_SvetoforS2->text(), 9761 );
		timer_SvetoforS2->start();

		set_Svetofor( 0, 0 );
		set_Svetofor( 1, 0 );
	}

	// SOCKET2_BARRIER
	timer_BarrierS2 = new QTimer( this );
	timer_BarrierS2->setInterval( 500 );
	connect( timer_BarrierS2, SIGNAL( timeout() ), this, SLOT( timer_BarrierS2_Slot() ) );

	tcpSocket_BarrierS2 = new QTcpSocket( this );
	connect( tcpSocket_BarrierS2, SIGNAL( readyRead() ), this, SLOT( readSocket_BarrierS2_Slot() ) );
	connect( tcpSocket_BarrierS2, SIGNAL( error( QAbstractSocket::SocketError ) ), this, SLOT( socketError_BarrierS2_Slot( QAbstractSocket::SocketError ) ) );
	if( settings->group_BarrierS2->isChecked() )
	{
		tcpSocket_BarrierS2->connectToHost( settings->lineEdit_BarrierS2->text(), 9761 );
		timer_BarrierS2->start();

		set_Barrier( 0, 0 );
		set_Barrier( 1, 0 );
	}

	// ETHERNET TABLO
	tcpSocket_Tablo1 = new QTcpSocket( this );
	connect( tcpSocket_Tablo1, SIGNAL( error( QAbstractSocket::SocketError ) ), this, SLOT( socketError_tcpSocket_Tablo1_Slot( QAbstractSocket::SocketError ) ) );
	if( settings->radioTablo_IP1->isChecked() )
	{
		tcpSocket_Tablo1->connectToHost( settings->enterTablo_IP1->text(), 9762 );
	}

	tcpSocket_Tablo2 = new QTcpSocket( this );
	connect( tcpSocket_Tablo2, SIGNAL( error( QAbstractSocket::SocketError ) ), this, SLOT( socketError_tcpSocket_Tablo2_Slot( QAbstractSocket::SocketError ) ) );
	if( settings->radioTablo_IP2->isChecked() )
	{
		tcpSocket_Tablo2->connectToHost( settings->enterTablo_IP2->text(), 9763 );
	}

	// WEIGHT ethernet
	timerWeight = new QTimer( this );
	timerWeight->setInterval( INTERVAL_REQUEST );
	connect( timerWeight, SIGNAL( timeout() ), this, SLOT( timerWeight_Slot() ) );
	connect( settings, SIGNAL(StartTimerWeight()), this, SLOT( StartTimerWeight() ) );

	tcpWeight = new QTcpSocket( this );
	connect( tcpWeight, SIGNAL( readyRead() ), this, SLOT( readWeight_Slot() ) );
	connect( tcpWeight, SIGNAL( error( QAbstractSocket::SocketError ) ), this, SLOT( socketErrorWeight_Slot( QAbstractSocket::SocketError ) ) );

	if( settings->group_TCPWeight->isChecked() )
	{
		tcpWeight->connectToHost( settings->lineEditTCP_IP->text(), 9761 );

		timerWeight->start(); 

		QTimer *timerConnection = new QTimer( this );
		timerConnection->setInterval( 5000 );
		connect( timerConnection, SIGNAL( timeout() ), this, SLOT( timerConnection_Slot() ) );
		timerConnection->start();
		isON_flag = true;
	}

	// инициализация CardReader
	cardReaderRfidObj = new cardReaderRfid(this);
	//reopenRfid_Slot();

	flag = connect(cardReaderRfidObj, SIGNAL(sendCodeRfid_Signal(QString)), registration_rfidObj, SLOT(acceptCodeRfid_Slot(QString)));

	// code for verifying in database
	flag = connect(cardReaderRfidObj, SIGNAL(sendCodeRfid_Make_Record_Signal(QString)), this, SLOT(getRfidCode_Slot(QString)));


	QNetworkConfigurationManager manager;
	QSettings settingsQ(QSettings::UserScope, QLatin1String("QtProject"));
        settingsQ.beginGroup(QLatin1String("QtNetwork"));
        const QString id = settingsQ.value(QLatin1String("DefaultNetworkConfiguration")).toString();
        settingsQ.endGroup();
	QNetworkConfiguration config = manager.configurationFromIdentifier(id);
    if ((config.state() & QNetworkConfiguration::Discovered) !=
        QNetworkConfiguration::Discovered) {
        config = manager.defaultConfiguration();
    }
	networkSession = new QNetworkSession(config, this);
    networkSession->open();

	QTextCodec *textCodec = QTextCodec::codecForName("Windows-1251");

	// reset all
	setSocket2State_Slot( 1, 0, 0, 0 );
	setSocket2State_Slot( 1, 1, 0, 0 );
	setSocket2State_Slot( 2, 0, 0, 0 );
	setSocket2State_Slot( 2, 1, 0, 0 );
	setSocket2State_Slot( 3, 0, 0, 0 );
	setSocket2State_Slot( 3, 1, 0, 0 );

	// doublecolor board
	stateHit = 1;
	tabloColor = 1;
	tabloTraff = 1;

	secondsAW = 0;
	QSettings settings_file( "settings.ini", QSettings::IniFormat );
    settings_file.setIniCodec("Windows-1251");
	secondsAW = settings_file.value( "vp01_s/interval" ).toInt();
	if( secondsAW == 1) secondsAW = 2;


	VP01_svetofor_timer = new QTimer( this );
	VP01_svetofor_timer->setInterval( INTERVAL_REQUEST );
	connect( VP01_svetofor_timer, SIGNAL( timeout() ), this, SLOT( VP01_svetofor_timer_Slot() ) );
	if( settings->typesVPList->currentText() == textCodec->toUnicode("ВП-01 (светофор)") )
	{
		VP01_svetofor_timer->start();
	}

	QTimer *tablo_timer = new QTimer( this );
	tablo_timer->setInterval( 1000 );
	connect( tablo_timer, SIGNAL( timeout() ), this, SLOT( tablo_timer_Slot() ) );
	tablo_timer->start();

#ifdef DEBUG
	demoDialWeight->setValue( 0 );
#endif

	spravochnikTaraObj = new spravochnik_tara;
	connect( this, SIGNAL( setTaraWeight_Signal( double ) ), spravochnikTaraObj, SLOT( getTaraWeight_Slot( double ) ) );


	// форма запроса пароля для просмотра датчиков
	sensorsPasswordDialog = new QDialog( 0 );
	sensorsPasswordDialog->setWindowTitle( tr( "Enter password") );
	sensorsPasswordDialog->setFixedSize( 300, 100 );
	sensorsPasswordDialog->hide();

	QGridLayout *controlLayout = new QGridLayout( sensorsPasswordDialog );
	QLabel *labelI = new QLabel( "  Пароль:", sensorsPasswordDialog );
	enterPasswordDC = new QLineEdit( sensorsPasswordDialog );
	enterPasswordDC->setEchoMode( QLineEdit::Password );

	okButtonDC = new QPushButton( sensorsPasswordDialog );
	okButtonDC->setText( "Ок" );
	connect( okButtonDC, SIGNAL( clicked() ), this, SLOT( okButtonDC_Slot() ) );
	cancelButtonDC = new QPushButton( sensorsPasswordDialog );
	cancelButtonDC->setText( "Отмена" );
	connect( cancelButtonDC, SIGNAL( clicked() ), this, SLOT( cancelButtonDC_Slot() ) );

	controlLayout->addWidget( labelI, 0, 0 );
	controlLayout->addWidget( enterPasswordDC, 0, 1 );
	controlLayout->addWidget( okButtonDC, 1, 0 );
	controlLayout->addWidget( cancelButtonDC, 1, 1 );


	// таймер для поддержания соединения с БД MySQL
	QTimer *timerStableDB = new QTimer( this );
	timerStableDB->setInterval( 60000 );
	connect( timerStableDB, SIGNAL( timeout() ), mysqlProccessObject, SLOT( stable_Slot() ) );
	timerStableDB->start();


	isDiscret = settings_file.value( "scales1/sor_discret" ).toBool();

	pathFile1C = settings_file.value( "1c/path" ).toString();
	QTimer* timer1C = new QTimer( this );
	timer1C->setInterval( settings_file.value( "1c/timer_msec" ).toInt() );
	connect( timer1C, SIGNAL( timeout() ), this, SLOT( write_1C() ) );
	timer1C->start();

	if( optionsObj->isCheked[optionsObj->modules.indexOf("rfid")] )
	{
		CardsMode_action->setChecked( settings_file.value( "mode/rfid" ).toBool() );
		CardsMode_Slot( settings_file.value( "mode/rfid" ).toBool() );
	}
	else
	{
		CardsMode_action->setChecked( false );
		CardsMode_Slot( false );
	}
}

// Отправить данные выбранные за заданное время
void MyClass::timerSendMail_Slot()
{
	QSettings settings_file( "settings.ini", QSettings::IniFormat );
    settings_file.setIniCodec("Windows-1251");

	int interval_send_mins = settings_file.value( "mail/interval_send_report" ).toInt();
	bool send_enable = ( settings_file.value( "mail/enable_send_report" ).toString() == "1" || settings->checkBoxEnableMailSend ? true : false );
	bool send_photos = ( settings_file.value( "mail/enable_send_photos" ).toString() == "1" || settings->checkBoxPhotoSaveAll->isChecked() ? true : false );
	bool send_eachrec = ( settings_file.value( "mail/enable_send_eachrec" ).toString() == "1" || settings->checkBoxSendEachRec->isChecked() ? true : false );

	static bool flag_block_send_emeil = false;

	//if( timerSendMail->isActive() )
	//	timerSendMail->stop();

	int current_minutes = QTime::currentTime().hour() * 60 + QTime::currentTime().minute();

	try
	{
		if( send_enable == true )
		{
			//if( ( ( ( current_minutes % ( interval_send_mins <= 0 ? interval_send_mins = 1 : interval_send_mins ) == 0 ) && ! send_eachrec ) ||  send_eachrec ) && flag_block_send_emeil == false  )
			if( ( ( ( current_minutes % ( interval_send_mins <= 0 ? interval_send_mins = 1 : interval_send_mins ) == 0 ) && ! send_eachrec ) || send_eachrec  ) && flag_block_send_emeil == false )
			{
				flag_block_send_emeil = true;

				QString lastDateTime, dateTimeNow;
				QString email;
				QVector< QList< QString > > *data = new QVector< QList< QString > >();

				QString last_date_time = ( settings_file.value( "mail/last_date_time" ).toString() );
				QDateTime lDateTime = QDateTime::fromString( last_date_time, "yyyy-MM-dd hh:mm:ss" );

				last_date_time = lDateTime.toString( "yyyy-MM-dd hh:mm:ss" );

				if( last_date_time == "" || last_date_time.size() == 0 ) // || QDateTime::fromString( last_date_time ) )
				{
					//last_date_time = QDateTime::currentDateTime().toString( "yyyy-MM-dd hh:00:00" );
				}

				dateTimeNow = QDateTime::currentDateTime().toString( "yyyy-MM-dd hh:mm:ss" );
				//
				// Преобразовать в Excel данные из журнала взвешиваний
				int i, j;
				QList<QVariant> row;
				bool flag = false;

				// Создаём файл для сохранения в Excel
				mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
				flag = mysqlProccessObject->getDataForHourlyReports( last_date_time, dateTimeNow, mysqlProccessObject->REPORT_TABLE, data, send_eachrec ); // считываем данные для отправки по e-mail
				if (flag == false || data->size() == 0)
				{
					//threadSavePhotos.terminate();
					emit finishedSavePhotos();
					return;
				}


				// Создать  документ Excel
				QAxObject *mExcel = new QAxObject( "Excel.Application" ); //получаем указатьтель на excel
				mExcel->setProperty( "DisplayAlerts", 0 ); // отключить предупреждения об  ошибках
				QAxObject *workBooks = mExcel->querySubObject( "Workbooks" );

				if( workBooks == 0x00 )
				{
					//ПО Microsoft Excel не установлено!
					workBooks->deleteLater();
					mExcel->deleteLater();

					return;
				}

				QAxObject *workBook = workBooks->querySubObject( "Add" ); // созадать лист Excel
				QAxObject *sheets = workBook->querySubObject( "Worksheets" );
				QAxObject *sheet1 = sheets->querySubObject( "Item( int )", 1 );

				i = 0;
				j = 0;

				// Вывести имена столбцов из Шапки таблицы
				QAxObject *rangeHeaders = sheet1->querySubObject( "Range(const QString&, const QString&)",
					QString("A%1").arg( 1 ), QString("S%1").arg( 1 ) );
				for( int h = 0; h < reportFormObject->tableReports->columnCount() - 2; h++ )
				{
					row.append( reportFormObject->tableReports->horizontalHeaderItem( h )->text() );
				}

				QAxObject *razmer = rangeHeaders->querySubObject( "Rows" ); //получаю указатель на строку
                razmer->setProperty( "ColumnWidth", 20 ); // устанавливаю её размер.
				razmer->setProperty( "HorizontalAlignment", -4108 ); // устанавливаю её размер.

#define LINE_STYLE 1
#define LINE_WIDTH 2

				QAxObject *borderRight = rangeHeaders->querySubObject("Borders(xlEdgeRight)" );
				borderRight->setProperty( "LineStyle", LINE_STYLE ); //тип линии (там пунктиры,сплошная и так далее)
				borderRight->setProperty( "Weight", LINE_WIDTH ); //толщина

				QAxObject* interior = rangeHeaders->querySubObject("Interior");
				// устанавливаем цвет
				interior->setProperty("Color",QColor("yellow"));
				//
				rangeHeaders->setProperty("Value", QVariant( row ) );

				// Создаём записи  в  таблице  Excel
				row.clear();

				QList< int > idf_list;
				QStringList fileNames;
				idf_list.clear();
				fileNames.clear();
				while( j < data->size() )
				{
					QAxObject *range = sheet1->querySubObject( "Range(const QString&, const QString&)",
						QString("A%1").arg( j + 2 ), QString("S%1").arg( j + 2 ) );

					while( i < data->at( j ).size() -3 ) // -1 - ссылка на фото
					{
						if( i == 7 || i == 8 || i == 9 || i == 16 || i == 17 || i == 18 )
						{
							QString temp = data->at( j ).at( i );

                            if( temp.contains( '.' ) )
								row.append( temp.replace( '.', ',' ) );
							else
								row.append( temp );
						}
						else
						if( i == 10 )
						{
							if( !settings->enterExcelDateTimeFormat->text().isEmpty() )
							{
								QAxObject *cell = range->querySubObject( "Range(const QVariant&)", "K1" );
								cell->setProperty( "NumberFormat", settings_file.value( "excel/date_time_format" ).toString() );
							}
							row.append( data->at( j ).at( i ) );
						}
						else
						if( i == 12 )
						{
							if( !settings->enterExcelDateTimeFormat->text().isEmpty() )
							{
								QAxObject *cell = range->querySubObject( "Range(const QVariant&)", "M1" );
								cell->setProperty( "NumberFormat", settings_file.value( "excel/date_time_format" ).toString() );
							}
							row.append( data->at( j ).at( i ) );
						}
						else
						{
							row.append( data->at( j ).at( i ) );
						}

						i++;
					}

					range->setProperty("Value", QVariant( row ) );
					row.clear();

					//for fotos save
					int idfIndex =  data->at( j ).size() - 2;
					idf_list.append( data->at( j ).at( idfIndex ).toInt() );
					fileNames.append( data->at( j ).at( idfIndex + 1 ) );

					//---

					j++;

					i = 0;


				}

				if ( send_photos /* && !send_eachrec */)
				{
					timerSendMail->blockSignals( true );
					timerSendMailEachRec->blockSignals( true );
					reportFormObject->buttonSendReportEmail->blockSignals( true );

					mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
					mysqlProccessObject->getPhotosbyIdAndSave( idf_list, fileNames, true );

					timerSendMail->blockSignals( false );
					timerSendMailEachRec->blockSignals( false );
					reportFormObject->buttonSendReportEmail->blockSignals( false );
				}
				mExcel->setProperty( "Visible", false); // запрещаем отображать документ Excel

				// Сохраняем в файл для отправки по e-mail
				QString filePath;
				QString path = "hourly";

				QDir dir( path );
				if( !dir.exists() )
					dir.mkdir( dir.absolutePath() );

				QString fileName = "Report.xls";

				filePath.append( QString( dir.absolutePath().replace( "/", "\\"  ) + "\\" + fileName ) );

				QFile file( filePath );
				file.open( QIODevice::ReadWrite );
				if( file.exists() )
				{
					file.remove();
				}
				file.close();

				QVariant var = workBook->dynamicCall( "SaveAs(const QString&, int)", filePath, -4143 );
				workBook->dynamicCall( "Close (Boolean)", false );

				QStringList sl;
				sl.append( filePath );

				MailSend mail( "Report (auto)"/*textCodec->toUnicode( "Отчет" )*/, filePath, true );

				mail.moveToThread(&threadSavePhotos);

				if ( mail.SendMail( filePath, false ) )
				{
					settings_file.setValue( "mail/last_date_time", QDateTime::fromString(dateTimeNow, "yyyy-MM-dd hh:mm:ss" ).toString( /*"yyyy-MM-dd 00:00:00"*/ "yyyy-MM-dd hh:mm:ss" ) );
					settings_file.sync();
				}

				emit finishedSavePhotos();
				/*
				emit loadMailData_Signal(
					false, settings_file.value( "mail/host" ).toString(),
					settings_file.value( "mail/port" ).toInt(),
					settings_file.value( "mail/login" ).toString(),
					settings_file.value( "mail/password" ).toString(),
					settings_file.value( "mail/ssl" ).toString(),
					settings_file.value( "mail/mailToSend" ).toString(),
					tr("Weight"),
					tr( "Report -" ) +
					QString(" %1" ).arg( QDateTime::fromString(last_date_time, "yyyy-MM-dd hh:mm:ss").toString("dd.MM.yyyy hh:mm:ss") ),
					false,
					"",
					sl );
					*/

				// delete range;
				delete sheet1;
				delete sheets;
				delete workBook;
				delete workBooks;
				delete mExcel;

			}
			else if( ( ( current_minutes % interval_send_mins ) != 0 && ! send_eachrec && flag_block_send_emeil == true )
				|| ( interval_send_mins == 1 && ! send_eachrec && flag_block_send_emeil == true ) || ( threadSavePhotos.isFinished() && flag_block_send_emeil == true ) )
			{
				flag_block_send_emeil = false;
			}
		}
	}
	catch( ... )
	{

	}

	if ( send_eachrec )
		timerSendMail->stop();

	emit finishedSavePhotos();
}

//checkbox state changed (Settings->EnableSendEachRec)
void MyClass::SendMailMode_Slot(int state)
{
	if ( state == Qt::Checked )
	{
		timerSendMail->stop();
		//timerSendMail->changeInterval( 10000 );
		//timerSendMail->setSingleShot( true );
		settings->selectIntervalSend->setEnabled( false );
		QObject::disconnect( timerSendMail, SIGNAL( timeout() ), this, SLOT( startThreadSendEmail_Slot() ) );
		QObject::connect( timerSendMailEachRec, SIGNAL( timeout() ), this, SLOT( startThreadSendEmail_Slot() ) );
	}
	else if ( state == Qt::Unchecked )
	{
		timerSendMailEachRec->stop();
		//timerSendMail->changeInterval( 1000 );
		//timerSendMail->setSingleShot( false );
		settings->selectIntervalSend->setEnabled( true );
		QObject::disconnect( timerSendMailEachRec, SIGNAL( timeout() ), this, SLOT( startThreadSendEmail_Slot() ) );
		QObject::connect( timerSendMail, SIGNAL( timeout() ), this, SLOT( startThreadSendEmail_Slot() ) );
		timerSendMail->start();
	}
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Отобразить главное окно программы
void MyClass::showMainWindow()
{
	bool result;
	QTextCodec *textCodec = QTextCodec::codecForName("Windows-1251");

	result = loginWindowObject->confirmLogin( loginWindowObject->enterName->currentText(), loginWindowObject->enterPassword->text() );

	if( result == true )
	{
	    loginWindowObject->hide();


		if( settings->typesVPList->currentText() == textCodec->toUnicode("ВП-01 (светофор)") ||
			settings->typesVPList->currentText() == textCodec->toUnicode("ВП-05А\\10\\11(р1)") ||
			settings->typesVPList->currentText() == textCodec->toUnicode("Keli D12 (f1)" ))
		{
            buttonZeroing->show();
		}
		else
		{
			buttonZeroing->hide();
		}

	    // Разрешаем/запрещаем контроллы в зависимости от того, какой пользователь залогинился
	    if( loginWindowObject->role != loginWindowObject->ADMIN )
	    {
			menuSettings->setEnabled( false );
			//restore_action->setEnabled( false );
			menuDB->setEnabled( false );
			addUser_action->setEnabled( false );
			log_action->setEnabled( false );
			sensors_action->setEnabled( false );
			hidden_action->setEnabled( false );
			deleteRecord->hide();
	    }
		else
		{
			menuSettings->setEnabled( true );
			//restore_action->setEnabled( true );
			menuDB->setEnabled( true );
			addUser_action->setEnabled( true );
			log_action->setEnabled( true );
			sensors_action->setEnabled( true );
			hidden_action->setEnabled( true );
			deleteRecord->show();
		}

		// Записать событие в лог
		mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		mysqlProccessObject->saveEventToLog( tr("Login in: ") + QString("%1").arg( loginWindowObject->currentNameUser ) );
		loginWindowObject->loginWin->hide();

		this->show();
	}
	else
	{
		mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
        mysqlProccessObject->saveEventToLog( tr("Incorrect password") );
	}
}

// Создаём контроллы главного окна
void MyClass::CreateControlsMainWindow( QWidget *w )
{
	QIcon icon;
	global_Icon = new QIcon();
	global_Icon->setPixmap( "image/promsoft48.png", QIcon::Large, QIcon::Normal, QIcon::On );

	optionsObj = new options( this );

    acceptform = new acceptionForm(); // инициализация формы настроек
	acceptFormModelObject = new acceptFormModel(); // модель формы приёмки/отправки автомобилей
	imageSaveModelObject = new imageSaveModel(); // модель таблицы хранения фотоснимков
	//
	settings = new SettingsForm(); // инициализация формы настроек

	dialogVPSensorsObj = new dialogVPSensors();
    //
	socketDevices = new optionalDevices( this );
	connect( socketDevices, SIGNAL( radioClicked_Signal( int, int, int ) ), this, SLOT( setSocket2State_Slot( int, int, int ) ) );

	serialPortClassObject = new serialPortClass(); // нициализация класса работы с COM-портом
	serialPortClassObject->initComPortProperties();
	//
	logEventsFormObject = new logEventsFormClass( w, this ); // конструктор класса отображения формы просмотра логов событий
	QList<QString> ports = serialPortClassObject->QuerrySerialPorts();
	QList<QString> bauds = serialPortClassObject->QuerrySerialPortSpeeds();
	settings->setupSettingsForm( ports, bauds, this );
	//
	// Определяем  размеры главного окна, и устанавливаем их
	QRect screenResolution = qApp->desktop()->screenGeometry();

	this->setWindowState( Qt::WindowMaximized );
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Константы и размеры виджеов
	int y_pos;

    QSettings settingsComPort( "settings.ini", QSettings::IniFormat );
	settingsComPort.setIniCodec( "Windows-1251" );

	settingsComPort.beginGroup( "Cam1" );
	QString strData1 = settingsComPort.value( "cam_en", 0 ).toString();
	settingsComPort.endGroup();
	settingsComPort.beginGroup( "Cam2" );
	QString strData2 = settingsComPort.value( "cam_en", 0 ).toString();
	settingsComPort.endGroup();
	settingsComPort.beginGroup( "Cam3" );
	QString strData3 = settingsComPort.value( "cam_en", 0 ).toString();
	settingsComPort.endGroup();
	settingsComPort.beginGroup( "Cam4" );
	QString strData4 = settingsComPort.value( "cam_en", 0 ).toString();
	settingsComPort.endGroup();

	// int width = 233 * 1.26;
	// int height = 167 * 1.26;

	int width = 233;
	int height = 167;

 	imageLabel_minGeometry1 = new QRect( ( screenResolution.height() <= 768 ? ( 555 + 205 + 34 ) : ( 555 + 205 + 64 ) ),
		( screenResolution.height() <= 768 ? 41 - 20 : 34 - 20 ),
		( width ),
		( height ) );
	imageLabel_maxGeometry1 = new QRect( 311, ( screenResolution.height() <= 768 ? 32 : 100 ), 640, 480 );
	// максимальный размер и положение изображения от IP-камеры 1

	imageLabel_minSize1 = new QSize(
		( width ),
		( height ) );
	imageLabel_maxSize1 = new QSize( 640, 480 );

	imageLabel_minGeometry2 = new QRect( ( screenResolution.height() <= 768 ? ( 555 + 205 + 317 ) : ( 555 + 205 + 362 ) ),
		( screenResolution.height() <= 768 ? 41 - 20 : 34 - 20 ),
		( width ),
		( height ) );
	imageLabel_maxGeometry2 = new QRect( 311,  ( screenResolution.height() <= 768 ? 32 : 100 ), 640, 480 );
	// максимальный размер и положение изображения от IP-камеры 2

	imageLabel_minSize2 = new QSize(
		( width ),
		( height ) );
	imageLabel_maxSize2 = new QSize( 640, 480 );

	imageLabel_minGeometry3 = new QRect( ( screenResolution.height() <= 768 ? ( 555 + 205 + 34 ) : ( 555 + 205 + 64 ) ),
		( screenResolution.height() <= 768 ? 41 - 20 + 228 : 34 - 20 + 245 ),
		( width ),
		( height ) );
	imageLabel_maxGeometry3 = new QRect( 311, ( screenResolution.height() <= 768 ? 32 : 100 ), 640, 480 );
	// максимальный размер и положение изображения от IP-камеры 1

	imageLabel_minSize3 = new QSize(
		( width ),
		( height ) );
	imageLabel_maxSize3 = new QSize( 640, 480 );

	imageLabel_minGeometry4 = new QRect( ( screenResolution.height() <= 768 ? ( 555 + 205 + 317 ) : ( 555 + 205 + 362 ) ),
		( screenResolution.height() <= 768 ? 41 - 20 + 228 : 34 - 20 + 245 ),
		( width ),
		( height ) );
	imageLabel_maxGeometry4 = new QRect( 311,  ( screenResolution.height() <= 768 ? 32 : 100 ), 640, 480 );
	// максимальный размер и положение изображения от IP-камеры 2

	imageLabel_minSize4 = new QSize(
		( width ),
		( height ) );
	imageLabel_maxSize4 = new QSize( 640, 480 );

	labelMaxSize = new QLabel( ui.centralWidget );
	labelMaxSize->setGeometry( screenResolution.width()/2 - 640/2,
		screenResolution.height()/2 - 480/2 - 40,
		640, 480 );

	labelMaxSize->setWindowFlags( Qt::FramelessWindowHint );
	labelMaxSize->setObjectName( "imageLabel_max" );
	labelMaxSize->installEventFilter( this );
	labelMaxSize->hide();

	widget_Cam1 = new QLabel( ui.centralWidget );
	widget_Cam1->setGeometry( *imageLabel_minGeometry1 );
	widget_Cam1->setFixedSize( width, height );
	widget_Cam1->setStyleSheet( "border: 3px solid #000; background-color: #000;" );
	widget_Cam1->setObjectName( "imageLabel1" );
	widget_Cam1->installEventFilter( this );

	widget_Cam2 = new QLabel( ui.centralWidget );
	widget_Cam2->setGeometry( *imageLabel_minGeometry2 );
	widget_Cam2->setFixedSize( width, height );
	widget_Cam2->setStyleSheet( "border: 3px solid #000; background-color: #000;" );
	widget_Cam2->setObjectName( "imageLabel2" );
	widget_Cam2->installEventFilter( this );

	widget_Cam3 = new QLabel( ui.centralWidget );
	widget_Cam3->setGeometry( *imageLabel_minGeometry3 );
	widget_Cam3->setFixedSize( width, height );
	widget_Cam3->setStyleSheet( "border: 3px solid #000; background-color: #000;" );
	widget_Cam3->setObjectName( "imageLabel3" );
	widget_Cam3->installEventFilter( this );

	widget_Cam4 = new QLabel( ui.centralWidget );
	widget_Cam4->setGeometry( *imageLabel_minGeometry4 );
	widget_Cam4->setMinimumSize( *imageLabel_minSize4 );
	widget_Cam4->setFixedSize( width, height );
	widget_Cam4->setStyleSheet( "border: 3px solid #000; background-color: #000;" );
	widget_Cam4->setObjectName( "imageLabel4" );
	widget_Cam4->installEventFilter( this );

	if( !optionsObj->isCheked[optionsObj->modules.indexOf("camera")] )
	{
		widget_Cam1->hide();
		widget_Cam2->hide();
		widget_Cam3->hide();
		widget_Cam4->hide();
	}

	currSettingsComPortPtr = new currentSettingsCOM_Port(); // объект настроек COM-порт
	currentSettingsIP_CamPtr = new currentSettingsIP_Cam(); // объект настроек IP-камер
	this->setWindowTitle( tr( "Prom-Soft " ) + PROG_VERSION + tr(" - Material accounting system") ); // имя  заглавного окна

	// Меню
	menuFile = new QMenu( tr("Service") ); //Служебные
	menuFile->addAction( tr("Dictionary"), this, SLOT( showDictionaryWindow() ) );
	menuFile->addAction( tr("Change user"), this, SLOT( changeUser() ) );

	//menuFile->addAction( tr("Add user"), this, SLOT( addUser() ) );
	addUser_action = new QAction( tr("Add user"), this );
	connect( addUser_action, SIGNAL( triggered() ), this, SLOT( addUser() ) );
	menuFile->addAction( addUser_action );

	//menuFile->addAction( tr("Event log"), this, SLOT( showLogEventsWindow_Slot() ) );
	log_action = new QAction( tr("Event log"), this );
	connect( log_action, SIGNAL( triggered() ), this, SLOT( showLogEventsWindow_Slot() ) );
	menuFile->addAction( log_action );

	menuFile->addAction( tr("Tara dictionary"), this, SLOT( showTaraDictionary_Slot() ) );

	//menuFile->addAction( tr("Add user"), this, SLOT( addUser() ) );
	CardsMode_action = new QAction(tr("Cards mode"), this);
	CardsMode_action->setCheckable(true);
	CardsMode_action->setChecked(false);
	connect(CardsMode_action, SIGNAL(toggled(bool)), this, SLOT(CardsMode_Slot(bool)));
	menuFile->addAction(CardsMode_action);

	//menuFile->addAction( tr("Analytics of sensor's codes"), this, SLOT( askSensorPassword() ));
	sensors_action = new QAction( tr("Analytics of sensor's codes"), this );
	connect( sensors_action, SIGNAL( triggered() ), this, SLOT( askSensorPassword() ) );
	menuFile->addAction( sensors_action );

	//menuFile->addAction( tr("Hidden weighing"), hiddenWeighing, SLOT( show() ) );
	hidden_action = new QAction( tr("Hidden weighing"), this );
	//connect( hidden_action, SIGNAL( triggered() ), hiddenWeighing, SLOT( show() ) );
	menuFile->addAction( hidden_action );

	this->menuBar()->addMenu( menuFile );

	//
	menuSettings = new QMenu( tr("Settings") );
	this->menuBar()->addMenu( menuSettings );
	menuSettings->setObjectName( "menuSettings" );
	menuSettings->installEventFilter( this );
	//
	//menuDevices = new QMenu( tr( "Optional devices" ) );
	//this->menuBar()->addMenu( menuDevices );
	//menuDevices->addAction( tr("Enable modules"), optionsObj, SLOT( show() ) );
	//menuDevices->addAction( "Socket-2", this, SLOT( socketsDialog_Slot() ) );
	//
	menuDB = new QMenu( tr( "Database" ) );
	this->menuBar()->addMenu( menuDB );
	restore_action = new QAction(tr( "Restore" ), this);
	connect(restore_action, SIGNAL(triggered()), this,  SLOT( dbRestoreDialog_Slot()) );
	menuDB->addAction( tr( "Backup" ), this, SLOT( dbBackupDialog_Slot() ) );
	//menuDB->addAction( tr( "Restore" ), this, SLOT( dbRestoreDialog_Slot() ) );

	menuDB->addAction(restore_action);

	menuDB->addAction( tr( "Open the dump folder" ), this, SLOT( dbOpenFolder_Slot() ) );
	//
	menuLang = new QMenu( tr( "Language" ) );
	this->menuBar()->addMenu( menuLang );

	QIcon iconLangUA("image/ukraine_48.png");
	QAction *uaAction = new QAction( iconLangUA, "Українська", this );
	connect( uaAction, SIGNAL( triggered() ), this, SLOT( lang_ua_Slot() ) );
	menuLang->addAction( uaAction );

	QIcon iconLangRU("image/russia_48.png");
	QAction *ruAction = new QAction( iconLangRU, "Русский", this );
	connect( ruAction, SIGNAL( triggered() ), this, SLOT( lang_ru_Slot() ) );
	menuLang->addAction( ruAction );

	QIcon iconLangEN("image/united_kingdom_48.png");
	QAction *enAction = new QAction( iconLangEN, "English", this );
	connect( enAction, SIGNAL( triggered() ), this, SLOT( lang_en_Slot() ) );
	menuLang->addAction( enAction );

	// Помощь
	menuHelp = new QMenu( tr("Help") );
	this->menuBar()->addMenu( menuHelp );
	menuHelp->addAction( tr("Enable modules"), optionsObj, SLOT( show() ) );
	//menuHelp->addAction( tr("User manual - F1"), this, SLOT( launchHelp() ) );
	menuHelp->addAction( tr("About"), this, SLOT( aboutSoft() ) );

	// Группа контролов главного окна
    groupBoxFindAuto = new QGroupBox( w );
	groupBoxFindAuto->setFixedSize( 450, 60);
	groupBoxFindAuto->setStyleSheet( "color: #000; font-weight: bold; font-size: 14px; " );

    printAutoTable = new QPushButton( tr("Print"), groupBoxFindAuto );
	printAutoTable->setGeometry( 6 + 100 + 100 + 120, 15, 120 , 40 );
	connect( printAutoTable, SIGNAL( clicked() ), this, SLOT( printAutoReception_Slot() ) );

	radioButtAutoComeIn = new QRadioButton( groupBoxFindAuto );
	radioButtAutoComeIn->setGeometry( 15, 20, 90, 20 );
	radioButtAutoComeIn->setText( tr("Gross") );
	radioButtAutoComeIn->setStyleSheet( "background-color: #f2f2f2;" );
	connect( radioButtAutoComeIn, SIGNAL( toggled(bool) ),	this, SLOT( show_InAutoAcception() ) );

	radioButtAutoComeOut = new QRadioButton( groupBoxFindAuto );
	radioButtAutoComeOut->setGeometry( 15+100, 20, 90, 20 );
    radioButtAutoComeOut->setChecked( true );
    radioButtAutoComeOut->setText( tr("Tara") );
	radioButtAutoComeOut->setStyleSheet( "background-color: #f2f2f2;" );
	connect( radioButtAutoComeOut, SIGNAL( toggled(bool) ),	this, SLOT( show_InAutoAcception() ) );

	deleteRecord = new QPushButton( tr("Delete\nrecord"), groupBoxFindAuto ); //"   Удалить\r\n    запись"
	deleteRecord->setGeometry( 100 + 100, 15, 120, 40 );
	deleteRecord->setToolTip( tr("Deletes the selected entry in the table.") );
	//QPixmap trashPixmap;
	//trashPixmap.load( "image/trash_32.png", "PNG", QPixmap::Auto );
	//deleteRecord->setIconSize( QSize( 32, 32 ) );
	//deleteRecord->setIcon( trashPixmap );
	connect( deleteRecord, SIGNAL( clicked() ), this, SLOT( deleteAutoReception_Record() ) );

	//
	QSettings settingsLang( "settings.ini", QSettings::IniFormat );
	settingsLang.setIniCodec( "Windows-1251" );
	QString langFromConf;
	settingsLang.beginGroup( "Server" );
	langFromConf = settingsLang.value( "lang", "ua" ).toString();
	settingsLang.endGroup();

	// Кнопки вызова формы приемки\отправки
    buttonAutoComeIn = new QPushButton( w );
	buttonAutoComeIn->setText( tr("Check in") ); // заезд
	buttonAutoComeIn->setMinimumWidth( 120 );
	buttonAutoComeIn->setMinimumHeight( 60 );
	buttonAutoComeIn->setObjectName( "buttonAutoComeIn" );
	buttonAutoComeIn->setStyleSheet( "font-weight: bold; font-size: 18px;" );
	connect( buttonAutoComeIn, SIGNAL( clicked() ), this, SLOT( buttonAutoComeIn_Clicked() ) );
	//
	buttonAutoComeOut = new QPushButton( w );
	buttonAutoComeOut->setText( tr("Check out") ); // выезд
	buttonAutoComeOut->setMinimumWidth( 120 );
	buttonAutoComeOut->setMinimumHeight( 60 );
	buttonAutoComeOut->setStyleSheet( "font-weight: bold; font-size: 18px;" );
	connect( buttonAutoComeOut, SIGNAL( clicked() ), this, SLOT( buttonAutoComeOut_Clicked() ) );
	//
    reportViewButton = new QPushButton( w );
	reportViewButton->setFixedHeight( 60 );
	reportViewButton->setMinimumWidth( 120 );
	reportViewButton->setText( tr("Registry") ); //журнал
	reportViewButton->setStyleSheet( "font-weight: bold; font-size: 18px;" );
	connect( reportViewButton, SIGNAL( clicked() ), this, SLOT( showReportsWindow() ) );

	//
	buttonZeroing = new QPushButton( w ); // кнопка обнуления весов
	buttonZeroing->setMinimumSize( 120, 28 );
	buttonZeroing->setMaximumSize( 263, 28 );
	buttonZeroing->setText( tr("Zero >0<") ); //"Обнуление >0<"
	buttonZeroing->setStyleSheet( "font-size: 14px; " );
	connect( buttonZeroing, SIGNAL( clicked() ), this, SLOT( zeroing() ) );

	// Индикатор массы
    tabScalesIndicator = new QTabWidget( w );
	tabScalesIndicator->setFixedSize(263, 120);


	char n[ 16 ];
    int i;
	int cntColumns;
	QList<QString> headersText;
	for( i = 0; i < settings->getNumVP(); i++ )
	{
		QWidget* w_panel = new QWidget( w );

		lcdScalesIndicator = new QLabel( w_panel ); // индикатор веса;
		lcdScalesIndicator->setAlignment( Qt::AlignRight );
		lcdScalesIndicator->setStyleSheet( "font: 60pt 'Calibri';" );
 		//lcdScalesIndicator->setNumDigits( 6 );
		lcdScalesIndicator->setText("------"); //  индикация отсутствия связи с ВП
		lcdScalesIndicator->setGeometry( 0, -10, 244, 110 );
		vectorLCD_Indicator.append( lcdScalesIndicator );

		lcdScalesConnection = new QLabel( w_panel );
		lcdScalesConnection->setText( tr("No connection") );
		lcdScalesConnection->setGeometry( 6, 76, 200, 20 );
		lcdScalesConnection->setStyleSheet( "font-size: 10px;" );

		strcpy( n, tr("WEIGHT") ); //масса
		tabScalesIndicator->addTab( w_panel, n );
		tabScalesIndicator->setStyleSheet( "font-size: 12px;" );
	}
    connect( tabScalesIndicator, SIGNAL( currentChanged( int ) ),
		this, SLOT( mainWinVP_tabChanged( int ) ) ); // сигнал-слот для срабатывания на смену вкладки весов (главное окно)

	labelKg = new QLabel( tr("\nkg"), w );
	labelKg->setStyleSheet( "font-size: 48px; font-weight: normal;" );
	labelKg->setMaximumWidth(50);

	if( screenResolution.width() <= 1024 )
	{
		labelKg->hide();
	}

	// Debug
    #ifdef DEBUG
	demoDialWeight = new QDial( w );
	demoDialWeight->setMinimum( -1 );
	demoDialWeight->setMaximum( 60000 );
	demoDialWeight->setGeometry( 555 + 165, 100, 81, 81 );
	demoDialWeight->setStyleSheet( "background-color: Gray;" );
    QObject::connect( demoDialWeight, SIGNAL( valueChanged( int ) ), this, SLOT( setNewValue( int ) ) );

	// demoDialWeight->hide();
    #endif

	//trial был здесь

	bool flag;

    networkAccessManager_CamView01 = new QNetworkAccessManager( this );
	flag = connect( networkAccessManager_CamView01, SIGNAL( finished( QNetworkReply* ) ), this,
		SLOT( finishedHTTP_Request_CamView_01( QNetworkReply* ) ) ); // видео от IP-камеры №1

	networkAccessManager_CamView02 = new QNetworkAccessManager( this );
	connect( networkAccessManager_CamView02, SIGNAL( finished( QNetworkReply* ) ), this,
		SLOT( finishedHTTP_Request_CamView_02( QNetworkReply* ) ) ); // видео от IP-камеры №2

	networkAccessManager_CamView03 = new QNetworkAccessManager( this );
	connect( networkAccessManager_CamView03, SIGNAL( finished( QNetworkReply* ) ), this,
		SLOT( finishedHTTP_Request_CamView_03( QNetworkReply* ) ) ); // видео от IP-камеры №3

	networkAccessManager_CamView04 = new QNetworkAccessManager( this );
	connect( networkAccessManager_CamView04, SIGNAL( finished( QNetworkReply* ) ), this,
		SLOT( finishedHTTP_Request_CamView_04( QNetworkReply* ) ) ); // видео от IP-камеры №3

	timerRequestIP_Cams = new QTimer( this );
	timerRequestIP_Cams->setInterval( 2000 );
	timerRequestIP_Cams->setSingleShot( true );
	connect( timerRequestIP_Cams, SIGNAL( timeout() ), this, SLOT( timerRequestIP_Cams_Slot() ) );
	timerRequestIP_Cams->start();

	// Timeout после начала запроса фотоснимков от IP-камер
	timeOutSnapshotIP_Cams = new QTimer( this );
	timeOutSnapshotIP_Cams->setInterval( 8000 );
	timeOutSnapshotIP_Cams->setSingleShot( true );
	connect( timeOutSnapshotIP_Cams, SIGNAL( timeout() ), this, SLOT( timeOutSnapshotIP_Cams_Slot() ) );

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// Виджеты отображения видео от IP-камер
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// IP-Cam Canvas №1
	labelIP_CAM1 = new QLabel( tr("CAM 1"), w ); // надпись "Камера 1"
	labelIP_CAM1->setGeometry( imageLabel_minGeometry1->x() + imageLabel_minGeometry1->width() / 3 + 24, imageLabel_minGeometry1->y() - 48, 180, 12 );
	labelIP_CAM1->setStyleSheet( "font-size: 14px; color: #004B7F;" );
	labelIP_CAM1->hide();

	snapshot_1 = new QLabel( this ); // снапшот №1 автомобиля при взвешивании
	snapshot_1->setGeometry( screenResolution.width() / 4, screenResolution.height() / 6, 704, 480 );
	snapshot_1->hide();
	timeSnapShotShow_1 = new QTimer( this ); // время отображения фотоснимка от камеры №1
    timeSnapShotShow_1->setSingleShot( true );
    timeSnapShotShow_1->setInterval( 4000 );
    connect( timeSnapShotShow_1, SIGNAL( timeout() ), this, SLOT( timeSnapShotShow_1_slot() ) );

	previewSnapshot_1 = new QLabel(); // просмотр снапшота №1 автомобиля при взвешивании в журнале
	previewSnapshot_1->setGeometry( 10, 22, 320, 240 );
	previewSnapshot_1->raise();
	previewSnapshot_1->setObjectName( "previewSnapshot_1" );
	previewSnapshot_1->setWindowTitle( tr("View weighing photo #1 cam #1") );
	previewSnapshot_1->installEventFilter( this );
	//
	// IP-Cam Canvas №2
	labelIP_CAM2 = new QLabel( tr("CAM 2"), w ); // надпись "Камера 2" ???
	labelIP_CAM2->setGeometry( imageLabel_minGeometry2->x() + imageLabel_minGeometry2->width() / 3 + 24, imageLabel_minGeometry2->y() - 48, 180, 12 );
	labelIP_CAM2->setStyleSheet( "font-size: 14px; color: #004B7F;" );
	labelIP_CAM2->hide();

	snapshot_2 = new QLabel( this ); // снапшот №2 автомобиля при взвешивании
	snapshot_2->setGeometry( screenResolution.width() / 4, screenResolution.height() / 6, 704, 480 );
	snapshot_2->hide();

	timeSnapShotShow_2 = new QTimer( this ); // время отображения фотоснимка от камеры №2
    timeSnapShotShow_2->setSingleShot( true );
    timeSnapShotShow_2->setInterval( 4000 );
    connect( timeSnapShotShow_2, SIGNAL( timeout() ), this, SLOT( timeSnapShotShow_2_slot() ) );

	previewSnapshot_2 = new QLabel(); // просмотр снапшота №2 автомобиля при взвешивании в журнале
	previewSnapshot_2->setGeometry( 10, 280, 320, 240 );
	previewSnapshot_2->raise();
	previewSnapshot_2->setObjectName( "previewSnapshot_2" );
	previewSnapshot_2->setWindowTitle( tr("View weighing photo #2 cam #1") );
	previewSnapshot_2->installEventFilter( this );
	//
	// IP-Cam Canvas №3
	labelIP_CAM3 = new QLabel( tr("CAM 3"), w );
	labelIP_CAM3->setGeometry( imageLabel_minGeometry3->x() + imageLabel_minGeometry3->width() / 3 + 24, imageLabel_minGeometry3->y() - 48, 180, 12 );
	labelIP_CAM3->setStyleSheet( "font-size: 14px; color: #004B7F;" );
	labelIP_CAM3->hide();

	snapshot_3 = new QLabel( this ); // снапшот №3 автомобиля при взвешивании
	snapshot_3->setGeometry( screenResolution.width() / 4, screenResolution.height() / 6, 704, 480 );
	snapshot_3->hide();
	timeSnapShotShow_3 = new QTimer( this ); // время отображения фотоснимка от камеры №3
    timeSnapShotShow_3->setSingleShot( true );
    timeSnapShotShow_3->setInterval( 4000 );
    connect( timeSnapShotShow_3, SIGNAL( timeout() ), this, SLOT( timeSnapShotShow_3_slot() ) );

	previewSnapshot_3 = new QLabel(); // просмотр снапшота №3 автомобиля при взвешивании в журнале
	previewSnapshot_3->setGeometry( 10 + 320 +10, 22, 320, 240 );
	previewSnapshot_3->raise();
	previewSnapshot_3->setObjectName( "previewSnapshot_3" );
	previewSnapshot_3->setWindowTitle( tr("View weighing photo #1 cam #2") );
	previewSnapshot_3->installEventFilter( this );
	//
	// IP-Cam Canvas №4
	labelIP_CAM4 = new QLabel( tr("CAM 4"), w ); // надпись "Камера 4" ???
	labelIP_CAM4->setGeometry( imageLabel_minGeometry4->x() + imageLabel_minGeometry4->width() / 3 + 24, imageLabel_minGeometry4->y() - 48, 180, 12 );
	labelIP_CAM4->setStyleSheet( "font-size: 14px; color: #004B7F;" );
	labelIP_CAM4->hide();

	snapshot_4 = new QLabel( this ); // снапшот №4 автомобиля при взвешивании
	snapshot_4->setGeometry( screenResolution.width() / 4, screenResolution.height() / 6, 704, 480 );
	snapshot_4->hide();
	timeSnapShotShow_4 = new QTimer( this ); // время отображения фотоснимка от камеры №4
    timeSnapShotShow_4->setSingleShot( true );
    timeSnapShotShow_4->setInterval( 4000 );
    connect( timeSnapShotShow_4, SIGNAL( timeout() ), this, SLOT( timeSnapShotShow_4_slot() ) );

	previewSnapshot_4 = new QLabel(); // просмотр снапшота №4 автомобиля при взвешивании в журнале
	previewSnapshot_4->setGeometry( 10 + 320 + 10, 280, 320, 240 );
	previewSnapshot_4->raise();
	previewSnapshot_4->setObjectName( "previewSnapshot_4" );
	previewSnapshot_4->setWindowTitle( tr("View weighing photo #2 cam #2") );
	previewSnapshot_4->installEventFilter( this );

	//
	previewSnapshot_5 = new QLabel();
	previewSnapshot_5->setGeometry( 10 + 320 + 10 + 320 + 10, 22, 320, 240 );
	previewSnapshot_5->raise();
	previewSnapshot_5->setObjectName( "previewSnapshot_5" );
	previewSnapshot_5->setWindowTitle( tr("View weighing photo #1 cam #3") );
	previewSnapshot_5->installEventFilter( this );

    previewSnapshot_6 = new QLabel();
	previewSnapshot_6->setGeometry( 10 + 320 + 10 + 320 + 10, 280, 320, 240 );
	previewSnapshot_6->raise();
	previewSnapshot_6->setObjectName( "previewSnapshot_6" );
	previewSnapshot_6->setWindowTitle( tr("View weighing photo #2 cam #3") );
	previewSnapshot_6->installEventFilter( this );

	//
    previewSnapshot_7 = new QLabel();
	previewSnapshot_7->setGeometry( 10 + 320 + 10 + 320 + 10 + 320 + 10, 22, 320, 240 );
	previewSnapshot_7->raise();
	previewSnapshot_7->setObjectName( "previewSnapshot_7" );
	previewSnapshot_7->setWindowTitle( tr("View weighing photo #1 cam #4") );
	previewSnapshot_7->installEventFilter( this );

	previewSnapshot_8 = new QLabel();
	previewSnapshot_8->setGeometry( 10 + 320 + 10 + 320 + 10 + 320 + 10, 280, 320, 240 );
	previewSnapshot_8->raise();
	previewSnapshot_8->setObjectName( "previewSnapshot_8" );
	previewSnapshot_8->setWindowTitle( tr("View weighing photo #2 cam #4") );
	previewSnapshot_8->installEventFilter( this );

	// Метка "Нет видео"
	labNoSignal_Cam1 = new QLabel( "NO SIGNAL", widget_Cam1 );
	labNoSignal_Cam1->setGeometry( widget_Cam1->size().width() / 2 - 64, widget_Cam1->size().height() / 2-12, 240, 24 );
	labNoSignal_Cam1->setStyleSheet( "color: #e08524; font-size: 24px; text-align: center; border: none;" );
	labNoSignal_Cam1->show();
	labNoSignal_Cam1->raise();

	// Метка "Нет видео"
	labNoSignal_Cam2 = new QLabel( "NO SIGNAL", widget_Cam2 );
	labNoSignal_Cam2->setGeometry( widget_Cam2->size().width() / 2 - 64, widget_Cam2->size().height() / 2-12, 240, 24 );
	labNoSignal_Cam2->setStyleSheet( "color: #e08524; font-size: 24px; text-align: center; border: none;" );

	// Метка "Нет видео"
	labNoSignal_Cam3 = new QLabel( "NO SIGNAL", widget_Cam3 );
	labNoSignal_Cam3->setGeometry( widget_Cam3->size().width() / 2 - 64, widget_Cam3->size().height() / 2-12, 240, 24 );
	labNoSignal_Cam3->setStyleSheet( "color: #e08524; font-size: 24px; text-align: center; border: none;" );

	// Метка "Нет видео"
	labNoSignal_Cam4 = new QLabel( "NO SIGNAL", widget_Cam4 );
	labNoSignal_Cam4->setGeometry( widget_Cam4->size().width() / 2 - 64, widget_Cam4->size().height() / 2-12, 240, 24 );
	labNoSignal_Cam4->setStyleSheet( "color: #e08524; font-size: 24px; text-align: center; border: none;" );

	labNoSignal_Cam1->hide();
	labNoSignal_Cam2->hide();
	labNoSignal_Cam3->hide();
	labNoSignal_Cam4->hide();

	// Таблица приёмки автомобилей
	tableAutoReception = new QTableWidget( w );
	tableAutoReception->setMinimumHeight( screenResolution.height() - 368 );

	tableAutoReception->setVerticalScrollBarPolicy( Qt::ScrollBarAsNeeded ); // ScrollBarAsNeeded );
	tableAutoReception->setHorizontalScrollBarPolicy( Qt::ScrollBarAsNeeded ); //  ScrollBarAsNeeded );
	tableAutoReception->setEditTriggers( QAbstractItemView::NoEditTriggers ); // запрещаем редактировать ячейки

	cntColumns = getColumnsHeadersTableAutoAcception().count(); // количество колонок в таблице
	headersText = getColumnsHeadersTableAutoAcception(); // считать все заглавия  таблицы  из списка
	tableAutoReception->setColumnCount( cntColumns );

	tableAutoReception->setStyleSheet( "font-weight: bold; font-size: 12px;" );

	QSettings settingsT( "settings.ini", QSettings::IniFormat );
	settingsT.setIniCodec("Windows-1251");

	for(int i = 0; i < tableAutoReception->model()->columnCount() - 1; i++ )
	{
		tableAutoReception->setColumnWidth( i, settingsT.value( QString( "columnSizes/c%1" ).arg( i ), 120 ).toInt() );
	}
	tableAutoReception->setSortingEnabled( false );
	tableAutoReception->horizontalHeader()->setStretchLastSection( true );

	// Заголовки колонок в таблице приёмки/отправки автомобилей
	for( int j = 0; j < cntColumns; j++ )
	{
		tableAutoReception->setHorizontalHeaderLabels( headersText );
		tableAutoReception->setAlternatingRowColors( true );
	}
	connect( tableAutoReception, SIGNAL( cellClicked( int, int ) ),
		this, SLOT( selectAutoReception_Record_ForDelete( int, int ) ) );
	connect( tableAutoReception, SIGNAL( itemSelectionChanged() ),
		this, SLOT( unselectAutoReception_Record_Item() ) );
	connect( tableAutoReception, SIGNAL( cellDoubleClicked( int, int ) ),
		this, SLOT( shippingAuto( int, int ) ) );
	connect( tableAutoReception->horizontalHeader(), SIGNAL( sectionDoubleClicked( int ) ),
		this, SLOT( headerShippingAuto_Clicked( int ) ) );

	acceptform->setupUI_AcceptionForm( this ); // инициализация формы приёмки/отправки автомобиля
    acceptform->acceptionFormWindow->installEventFilter( acceptform ); // настроить на приём событий от окна приёмки/отправки
	acceptform->acceptionFormWindow->installEventFilter( this );



	// компоновка
	QVBoxLayout *mainLayout = new QVBoxLayout( w );
	mainLayout->setSpacing(5);
	mainLayout->setContentsMargins(3, 3, 3, 3);

	QHBoxLayout *top1 = new QHBoxLayout;
	top1->addWidget(buttonAutoComeIn);
	top1->addWidget(buttonAutoComeOut);
	top1->addWidget(reportViewButton);

	QVBoxLayout *top2 = new QVBoxLayout;
	top2->addLayout(top1);
	top2->addWidget(groupBoxFindAuto);

	QGridLayout *topWeight = new QGridLayout;

	if( screenResolution.width() <= 1024 )
	{
	    topWeight->addWidget(tabScalesIndicator, 0, 1, 1,1);
	}
	else
	{
		topWeight->addWidget(tabScalesIndicator, 0, 0, 1,1);
	}

	topWeight->addWidget(labelKg, 0,1);

	if( screenResolution.width() <= 1024 )
	{
	    topWeight->addWidget( buttonZeroing, 1, 1 );
	}
	else
	{
	    topWeight->addWidget( buttonZeroing, 1, 0 );
	}

	topWeight->setSpacing(2);
	topWeight->addColSpacing( 0, 65 );

	QVBoxLayout *top3 = new QVBoxLayout;
	top3->addLayout(topWeight);
	top3->setSpacing(1);

	QHBoxLayout *topLayout = new QHBoxLayout;
	topLayout->addLayout(top2);
	topLayout->addLayout(top3);

	QVBoxLayout *top4 = new QVBoxLayout;
	QHBoxLayout *top5 = new QHBoxLayout;

	label_number_fone = new QLabel();
	label_number_fone->setGeometry( 10, 10, 316, 30 );
	//label_number_fone->setPixmap( QPixmap( "image/s_black.png", "PNG" ) );
	label_number_fone->setText(tr("Customer Service. Maintenance, repair, calibration of scales. 067-754-96-76"));
	label_number_fone->setStyleSheet( "color: #006985; font-weight: bold; font-size: 14px; " );
	label_number_fone->raise();

	label_picture_vesi = new QLabel();
	label_picture_vesi->setGeometry(QRect(0, 0, 160, 116));
    label_picture_vesi->setPixmap(QPixmap("image/logo.png", "PNG"));
	label_picture_vesi->raise();
    //label_picture_vesi->setScaledContents(true);

	//Платформа

	//hLayoutPlatform = new QHBoxLayout;
    //hLayoutPlatform->setSpacing(6);
    //hLayoutPlatform->setContentsMargins(11, 11, 11, 11);
	//hLayoutPlatform->setContentsMargins(0, 0, 0, 0);
    groupBoxPlatform = new QGroupBox;
	groupBoxPlatform->setFixedSize(316, 116);
    labelPlatform = new QLabel(groupBoxPlatform);
    labelPlatform->setGeometry(QRect(0, 90, 301, 22));
    labelPlatform->setPixmap(QPixmap("image/platform.png"));
    labelPlatform->setScaledContents(true);
    labelPosLeft = new QLabel(groupBoxPlatform);
    labelPosLeft->setGeometry(QRect(35, 5, 10, 91));
    labelPosLeft->setPixmap(QPixmap("image/pos_ok.png"));
    labelPosLeft->setScaledContents(true);
	labelPosRight = new QLabel(groupBoxPlatform);
    labelPosRight->setGeometry(QRect(250, 5, 10, 91));
    labelPosRight->setPixmap(QPixmap("image/pos_ok.png"));
    labelPosRight->setScaledContents(true);
	labelPos2Left = new QLabel(groupBoxPlatform);
    labelPos2Left->setGeometry(QRect(89, 5, 10, 91));
    labelPos2Left->setPixmap(QPixmap("image/pos_ok.png"));
    labelPos2Left->setScaledContents(true);
	labelPos2Right = new QLabel(groupBoxPlatform);
    labelPos2Right->setGeometry(QRect(197, 5, 10, 91));
    labelPos2Right->setPixmap(QPixmap("image/pos_ok.png"));
	labelPos2Right->setScaledContents(true);
    labelAuto = new QLabel(groupBoxPlatform);
    labelAuto->setGeometry(QRect(50, 5, 196, 83));
    labelAuto->setPixmap(QPixmap("image/auto.png"));
    labelAuto->setScaledContents(true);
	labelAuto->hide();

	if( settings->check_PositionUseS1->isChecked() )
	{
		if( ! settings->check_PositionUse4ch->isChecked() )
		{
			labelPos2Left->hide();
			labelPos2Right->hide();
		}
	}
	else
	{
		labelPos2Left->hide();
		labelPos2Right->hide();
	}
    
	//topLayout->addWidget( groupBoxPlatform );
	top5->addWidget( groupBoxPlatform );
	groupBoxPlatform->raise();
	//top5->addWidget( label_picture_vesi );

	//top4->addWidget(label_number_fone);
	top4->addLayout(top5);

    topLayout->addLayout(top4);


	if( ! settings->group_PositionS2->isChecked() )
	{
		labelPosLeft->hide();
		labelPosRight->hide();
		labelPos2Left->hide();
		labelPos2Right->hide();
	}
	//Светофор
	label_Svetovor1 = new QLabel();
	label_Svetovor1->setGeometry( 10, 65, 83, 168 );
	label_Svetovor1->setObjectName( "label_Svetovor1" );
	label_Svetovor1->setCursor( Qt::PointingHandCursor );
	label_Svetovor1->setPixmap( QPixmap( "image/s_black.png", "PNG" ) );
	label_Svetovor1->raise();
	label_Svetovor1->installEventFilter( this );

	label_Svetovor2 = new QLabel();
    label_Svetovor2->setGeometry( 10 + 305, 65, 83, 168);
    label_Svetovor2->setObjectName( "label_Svetovor2" );
	label_Svetovor2->setCursor( Qt::PointingHandCursor );
	label_Svetovor2->setPixmap( QPixmap( "image/s_black.png", "PNG" ) );
	label_Svetovor2->raise();
	label_Svetovor2->installEventFilter( this );

	if( !settings->group_SvetoforS2->isChecked() )
	{
		label_Svetovor1->hide();
		label_Svetovor2->hide();
	}

	topLayout->addWidget( label_Svetovor1 );
	topLayout->addWidget( label_Svetovor2 );

	QSpacerItem *spacer1 = new QSpacerItem(20, 10, QSizePolicy::Expanding, QSizePolicy::Minimum);
	topLayout->addItem(spacer1);

	QHBoxLayout *horizCam = new QHBoxLayout;
	QVBoxLayout *verCams = new QVBoxLayout;
	QVBoxLayout *verTables = new QVBoxLayout;


	if( settingsT.value( "main_cameras/isHorizontal", true ).toBool() )
	{
		( strData1 == "0" ) ? widget_Cam1->hide() : horizCam->addWidget(widget_Cam1);
		( strData2 == "0" ) ? widget_Cam2->hide() : horizCam->addWidget(widget_Cam2);
		( strData3 == "0" ) ? widget_Cam3->hide() : horizCam->addWidget(widget_Cam3);
		( strData4 == "0" ) ? widget_Cam4->hide() : horizCam->addWidget(widget_Cam4);

		topLayout->addLayout( horizCam );

		mainLayout->addLayout( topLayout );
		mainLayout->addWidget( tableAutoReception );
		mainLayout->addWidget( tableViewReports );
	}
	else
	{
		//horizCam->addWidget( tableAutoReception );
		verTables->addWidget( tableAutoReception );
		verTables->addWidget( tableViewReports );
		( strData1 == "0" ) ? widget_Cam1->hide() : verCams->addWidget(widget_Cam1);
		( strData2 == "0" ) ? widget_Cam2->hide() : verCams->addWidget(widget_Cam2);
		( strData3 == "0" ) ? widget_Cam3->hide() : verCams->addWidget(widget_Cam3);
		( strData4 == "0" ) ? widget_Cam4->hide() : verCams->addWidget(widget_Cam4);

		horizCam->addLayout( verTables );
		horizCam->addLayout( verCams );

		mainLayout->addLayout( topLayout );
		mainLayout->addLayout( horizCam );
	}

 //   if( ( strData1 == "1" && strData2 == "1" && strData3 == "1" && strData4 == "0" )  ||
	//	( strData1 == "1" && strData2 == "1" && strData3 == "1" && strData4 == "1" ) ) // Три - четыре камеры
	//{
	//	if( screenResolution.width() > 1600 )
	//	{
	//		( strData3 == "0" ) ? widget_Cam3->hide() : topCam->addWidget(widget_Cam3);
	//        ( strData4 == "0" ) ? widget_Cam4->hide() : topCam->addWidget(widget_Cam4);
	//	}
	//	else
	//	{
	//		horizCam->addWidget( tableAutoReception );
	//		verCams = new QVBoxLayout( horizCam );

	//		( strData3 == "0" ) ? widget_Cam3->hide() : verCams->addWidget(widget_Cam3);
	//        ( strData4 == "0" ) ? widget_Cam4->hide() : verCams->addWidget(widget_Cam4);
	//	}
	//}
	//else
	//{
	//	if( screenResolution.width() > 1600 )
	//	{
	//		( strData3 == "0" ) ? widget_Cam3->hide() : topCam->addWidget(widget_Cam3);
	//		( strData4 == "0" ) ? widget_Cam4->hide() : topCam->addWidget(widget_Cam4);
	//	}
	//	else
	//	{
	//		horizCam->addWidget( tableAutoReception );
	//		verCams = new QVBoxLayout( horizCam );

	//		( strData3 == "0" ) ? widget_Cam3->hide() : verCams->addWidget(widget_Cam3);
	//        ( strData4 == "0" ) ? widget_Cam4->hide() : verCams->addWidget(widget_Cam4);

	//	}
	//}
}

//
void MyClass::makeTTN_Button_Slot()
{
	int row = reportFormObject->row;
    int column = reportFormObject->column;

	if( reportFormObject->tableReports->columnCount() == 0  || row == -1 || column == -1 ) // нет записей в таблице
	{
		QMessageBox mess;
	    mess.setText( tr("Select the entry to generate the report!") );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

		return;
	}

	formTTN_Object->loadDataToTTN_Form( reportFormObject->tableReports->item( row, 10 )->text() );
	formTTN_Object->showTTN();
}

//
void MyClass::pushButtonAccept_Slot()
{
	int row = reportFormObject->row;
    int column = reportFormObject->column;

	if( reportFormObject->tableReports->columnCount() == 0  || row == -1 || column == -1 ) // нет записей в таблице

	{
		QMessageBox mess;
	    mess.setText( tr("Select record to start!") );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

		return;
	}

	// Обновить данные из формы в БД ТТН
	formTTN_Object->updateDataFromTTN_Form( reportFormObject->tableReports->item( row, 10 )->text() );
}

// Преобразовать записи жупрнала в таблицу Excel
void MyClass::convertToExcell()
{
    QString val;
	int i = 0, j = 0;
	QList<QVariant> row;

	QSettings settings_file( "settings.ini", QSettings::IniFormat );
    settings_file.setIniCodec("Windows-1251");

	if( reportFormObject->tableReports->rowCount() == 0 )
	{
		// Ошибка запроса в БД
	    QMessageBox mess;
		mess.setText( tr("Data not available") );
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

		return;
	}
	// Создать  документ Excel
	QAxObject *mExcel = new QAxObject( "Excel.Application" ); //получаем указатьтель на excel
	mExcel->setProperty( "DisplayAlerts", 0 ); // отключить предупреждения об  ошибках
	QAxObject *workBooks = mExcel->querySubObject( "Workbooks" );

	if( workBooks == 0x00 )
	{
        // Ошибка запроса в БД
	    QMessageBox mess;
		mess.setText( tr("Microsoft Office - Excel is not installed!") );
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

		workBooks->deleteLater();
	    mExcel->deleteLater();

		return;
	}

	QAxObject *workBook = workBooks->querySubObject( "Add" ); // созадать лист Excel
	QAxObject *sheets = workBook->querySubObject( "Worksheets" );
	QAxObject *sheet1 = sheets->querySubObject( "Item( int )", 1 );

	i = 0;
	j = 0;

	// Вывести имена столбцов из "Шапки таблицы"
	QAxObject *rangeHeaders = sheet1->querySubObject( "Range(const QString&, const QString&)", QString("A%1").arg( 1 ), QString("S%1").arg( 1 ) );
	for( int h = 0; h < reportFormObject->tableReports->columnCount() - 1; h++ )
	{
		row.append( reportFormObject->tableReports->horizontalHeaderItem( h )->text() );
	}

	QAxObject *razmer = rangeHeaders->querySubObject( "Rows" ); //получаю указатель на строку
    razmer->setProperty( "ColumnWidth", 20 ); // устанавливаю её размер.
	razmer->setProperty( "HorizontalAlignment", -4108 ); // устанавливаю её размер.

    QAxObject* font = rangeHeaders->querySubObject("Font");
	// устанавливаем жирность
	font->setProperty("Bold", true);


#define LINE_STYLE 1
#define LINE_WIDTH 2

    // "Borders(xlEdgeLeft)", "Borders(xlEdgeRight)", "Borders(xlEdgeBottom)"); //  xlEdgeTop(верхняя граница) (xlEdgeLeft) левая, (xlEdgeRight) правая,(xlEdgeBottom) нижняя и 2 диагонали (xlDiagonalDown) (xlDiagonalUp)
	//QAxObject *borderTop = rangeHeaders->querySubObject("Borders(xlEdgeTop)" );
 //   borderTop->setProperty( "LineStyle", LINE_STYLE ); //тип линии (там пунктиры,сплошная и так далее)
 //   borderTop->setProperty( "Weight", LINE_WIDTH ); //толщина

	//QAxObject *borderLeft = rangeHeaders->querySubObject("Borders(xlEdgeLeft)" );
 //   borderLeft->setProperty( "LineStyle", LINE_STYLE ); //тип линии (там пунктиры,сплошная и так далее)
 //   borderLeft->setProperty( "Weight", LINE_WIDTH ); //толщина

	QAxObject *borderRight = rangeHeaders->querySubObject("Borders(xlEdgeRight)" );
    borderRight->setProperty( "LineStyle", LINE_STYLE ); //тип линии (там пунктиры,сплошная и так далее)
    borderRight->setProperty( "Weight", LINE_WIDTH ); //толщина


	//QAxObject *borderBottom = rangeHeaders->querySubObject("Borders(xlEdgeBottom)" );
 //   borderBottom->setProperty( "LineStyle", LINE_STYLE ); //тип линии (там пунктиры,сплошная и так далее)
 //   borderBottom->setProperty( "Weight", LINE_WIDTH ); //толщина

	QAxObject* interior = rangeHeaders->querySubObject("Interior");
	// устанавливаем цвет
	interior->setProperty("Color",QColor("yellow"));

	// освобождение памяти
	delete interior;

	rangeHeaders->setProperty("Value", QVariant( row ) );

	// Создаём записи  в  таблице  Excel
	row.clear();
	while( j < reportFormObject->tableReports->rowCount() )
	{
		QAxObject *range = sheet1->querySubObject( "Range(const QString&, const QString&)", QString("A%1").arg( j + 2 ), QString("S%1").arg( j + 2 ) );

		while( i < reportFormObject->tableReports->columnCount() - 3 ) // -1 - ссылка на фото
		{
			if( i == 7 || i == 8 || i == 9 || i == 16 || i == 17 || i == 18 )
			{
				QString temp = reportFormObject->tableReports->item( j, i )->text();

				if( temp.contains( '.' ) )
					row.append( temp.replace( '.', ',' ) );
				else
					row.append( temp );
			}
			else
			if( i == 10 )
			{
				if( !settings->enterExcelDateTimeFormat->text().isEmpty() )
				{
					QAxObject *cell = range->querySubObject( "Range(const QVariant&)", "K1" );
					cell->setProperty( "NumberFormat", settings_file.value( "excel/date_time_format" ).toString() );
				}
				row.append( reportFormObject->tableReports->item( j, i )->text() );
			}
			else
			if( i == 12 )
			{
				if( !settings->enterExcelDateTimeFormat->text().isEmpty() )
				{
					QAxObject *cell = range->querySubObject( "Range(const QVariant&)", "M1" );
					cell->setProperty( "NumberFormat", settings_file.value( "excel/date_time_format" ).toString() );
				}

				row.append( reportFormObject->tableReports->item( j, i )->text() );
			}
			else
			{
				row.append( reportFormObject->tableReports->item( j, i )->text() );
			}

			i++;
		}

		range->setProperty("Value", QVariant( row ) );
		row.clear();
        j++;

        i = 0;
	}

	mExcel->setProperty( "Visible", true );

	workBooks->deleteLater();
	mExcel->deleteLater();
}

void MyClass::convertToExcell_sor()
{
    QString val;
	int i = 0, j = 0;
	QList<QVariant> row;

	QSettings settings_file( "settings.ini", QSettings::IniFormat );
    settings_file.setIniCodec("Windows-1251");

	if( reportFormObject->tableReports->rowCount() == 0 )
	{
		// Ошибка запроса в БД
	    QMessageBox mess;
		mess.setText( tr("Data not available") );
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

		return;
	}
	// Создать  документ Excel
	QAxObject *mExcel = new QAxObject( "Excel.Application" ); //получаем указатьтель на excel
	mExcel->setProperty( "DisplayAlerts", 0 ); // отключить предупреждения об  ошибках
	QAxObject *workBooks = mExcel->querySubObject( "Workbooks" );

	if( workBooks == 0x00 )
	{
        // Ошибка запроса в БД
	    QMessageBox mess;
		mess.setText( tr("Microsoft Office - Excel is not installed!") );
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

		workBooks->deleteLater();
	    mExcel->deleteLater();

		return;
	}

	QAxObject *workBook = workBooks->querySubObject( "Add" ); // созадать лист Excel
	QAxObject *sheets = workBook->querySubObject( "Worksheets" );
	QAxObject *sheet1 = sheets->querySubObject( "Item( int )", 1 );

	i = 0;
	j = 0;

	// Вывести имена столбцов из "Шапки таблицы"
	QAxObject *rangeHeaders = sheet1->querySubObject( "Range(const QString&, const QString&)", QString("A%1").arg( 1 ), QString("U%1").arg( 1 ) );
	for( int h = 0; h < reportFormObject->tableReports->columnCount() - 1; h++ )
	{
		row.append( reportFormObject->tableReports->horizontalHeaderItem( h )->text() );
		if( h == 7 || h == 8 ) row.append( reportFormObject->tableReports->horizontalHeaderItem( h )->text() + tr("(REAL)") );
	}

	QAxObject *razmer = rangeHeaders->querySubObject( "Rows" ); //получаю указатель на строку
    razmer->setProperty( "ColumnWidth", 20 ); // устанавливаю её размер.
	razmer->setProperty( "HorizontalAlignment", -4108 ); // устанавливаю её размер.

    QAxObject* font = rangeHeaders->querySubObject("Font");
	// устанавливаем жирность
	font->setProperty("Bold", true);


#define LINE_STYLE 1
#define LINE_WIDTH 2

    // "Borders(xlEdgeLeft)", "Borders(xlEdgeRight)", "Borders(xlEdgeBottom)"); //  xlEdgeTop(верхняя граница) (xlEdgeLeft) левая, (xlEdgeRight) правая,(xlEdgeBottom) нижняя и 2 диагонали (xlDiagonalDown) (xlDiagonalUp)
	//QAxObject *borderTop = rangeHeaders->querySubObject("Borders(xlEdgeTop)" );
 //   borderTop->setProperty( "LineStyle", LINE_STYLE ); //тип линии (там пунктиры,сплошная и так далее)
 //   borderTop->setProperty( "Weight", LINE_WIDTH ); //толщина

	//QAxObject *borderLeft = rangeHeaders->querySubObject("Borders(xlEdgeLeft)" );
 //   borderLeft->setProperty( "LineStyle", LINE_STYLE ); //тип линии (там пунктиры,сплошная и так далее)
 //   borderLeft->setProperty( "Weight", LINE_WIDTH ); //толщина

	QAxObject *borderRight = rangeHeaders->querySubObject("Borders(xlEdgeRight)" );
    borderRight->setProperty( "LineStyle", LINE_STYLE ); //тип линии (там пунктиры,сплошная и так далее)
    borderRight->setProperty( "Weight", LINE_WIDTH ); //толщина


	//QAxObject *borderBottom = rangeHeaders->querySubObject("Borders(xlEdgeBottom)" );
 //   borderBottom->setProperty( "LineStyle", LINE_STYLE ); //тип линии (там пунктиры,сплошная и так далее)
 //   borderBottom->setProperty( "Weight", LINE_WIDTH ); //толщина

	QAxObject* interior = rangeHeaders->querySubObject("Interior");
	// устанавливаем цвет
	interior->setProperty("Color",QColor("yellow"));

	// освобождение памяти
	delete interior;

	rangeHeaders->setProperty("Value", QVariant( row ) );

	// Создаём записи  в  таблице  Excel
	row.clear();

	QVector<int> realBrutto;
	QVector<int> realTara;
	for( int i_row = 0; i_row < reportFormObject->tableReports->rowCount(); ++i_row )
	{
	    QString date_time;
		date_time = reportFormObject->tableReports->item( i_row, 10 )->text();
		QDateTime dt = QDateTime::fromString( date_time, "dd.MM.yyyy hh:mm:ss" );
		date_time = dt.toString( "yyyy-MM-dd hh:mm:ss" );

		mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		QVector<int> b_t = mysqlProccessObject->getRealBruttoAndTara( date_time );
		realBrutto.append( b_t[0] );
		realTara.append( b_t[1] );
	}

	while( j < reportFormObject->tableReports->rowCount() )
	{
		QAxObject *range = sheet1->querySubObject( "Range(const QString&, const QString&)", QString("A%1").arg( j + 2 ), QString("U%1").arg( j + 2 ) );

		while( i < reportFormObject->tableReports->columnCount() - 3 ) // -1 - ссылка на фото
		{
			if( i == 7 || i == 8 || i == 9 || i == 16 || i == 17 || i == 18 )
			{
				QString temp = reportFormObject->tableReports->item( j, i )->text();

				if( temp.contains( '.' ) )
					row.append( temp.replace( '.', ',' ) );
				else
					row.append( temp );

				if( i == 7 )
					row.append( QString::number( realBrutto[j] ) );
				if( i == 8 )
					row.append( QString::number( realTara[j] ) );
			}
			else
			if( i == 10 )
			{
				if( !settings->enterExcelDateTimeFormat->text().isEmpty() )
				{
					QAxObject *cell = range->querySubObject( "Range(const QVariant&)", "K1" );
					cell->setProperty( "NumberFormat", settings_file.value( "excel/date_time_format" ).toString() );
				}
				row.append( reportFormObject->tableReports->item( j, i )->text() );
			}
			else
			if( i == 12 )
			{
				if( !settings->enterExcelDateTimeFormat->text().isEmpty() )
				{
					QAxObject *cell = range->querySubObject( "Range(const QVariant&)", "M1" );
					cell->setProperty( "NumberFormat", settings_file.value( "excel/date_time_format" ).toString() );
				}

				row.append( reportFormObject->tableReports->item( j, i )->text() );
			}
			else
			{
				row.append( reportFormObject->tableReports->item( j, i )->text() );
			}

			i++;
		}

		range->setProperty("Value", QVariant( row ) );
		row.clear();
        j++;

        i = 0;
	}

	mExcel->setProperty( "Visible", true );

	workBooks->deleteLater();
	mExcel->deleteLater();
}

// Новое заначение веса для Демо - версии
void MyClass::setNewValue( int value )
{
	weightScales_01 = QString( "%1" ).arg( value );

	this->vectorLCD_Indicator.at( 0 )->setText( weightScales_01 );

	if( settings->group_Sornost->isChecked() )
	{
		int w = vectorLCD_Indicator.at( 0 )->text().toInt();
		double koef = settings->spinKoefSor->value();

		weightScales_sor = w * koef;
		this->vectorLCD_Indicator.at( 0 )->setText( QString::number( weightScales_sor ) );

		if( settings->checkSorWeightShow->isChecked() )
		{
			tabScalesIndicator->show();
			labelKg->show();
		}
		else
		{
			tabScalesIndicator->hide();
			labelKg->hide();
		}
	}
	else
	{
		weightScales_sor = weightScales_01.toInt();
	}

	emit setTaraWeight_Signal( ( int )weightScales_01.toDouble() );

}
//
void MyClass::showDataBaseContent_Slot()
{
	bool result;
	QSqlError error;
	int type_operation;

	if( radioButtAutoComeIn->isChecked() == true && radioButtAutoComeOut->isChecked() == false )
	    type_operation = 0;
	else if( radioButtAutoComeOut->isChecked() == true && radioButtAutoComeIn->isChecked() == false )
		type_operation = 1;

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();
	result = query.exec( QString( "SELECT * FROM AutoReception WHERE type_operation=%1;" ).arg( type_operation ) );

	int cnt = query.record().count();

	QString val;
	int i = 0, j = 0;
	while( query.next() )
	{
	    tableAutoReception->setRowCount( j + 1 );

		while( i < tableAutoReception->columnCount() )
		{
		    if( i + 3 == DATETIME_AUTORECEPTION_COLUMN )
                val = query.value( i + 3 ).asDateTime().toString( "dd.MM.yyyy hh:mm:ss" ); // ячейка времени/даты
			else if( i + 3 == ENTERTARA_AUTORECEPTION_COLUMN )
			{
				int tara_type = query.record().value( "tara_type" ).toInt();
                if( tara_type == 0 )
					val = tr("From scales");
				else if( tara_type == 1 )
					val = tr("Manually");
				else if( tara_type == 2 )
					val = tr("Saved tara");
			}
			else
                val = query.value( i + 3 ).toString();

			tableAutoReception->setItem( j, i, new QTableWidgetItem( val ) );
			i++;
        }
        j++;

		i = 0;
    }
}
// Отметить строку  для подготовки удаления записи из таблицы приёмки/отправки автомобилей
void MyClass::selectAutoReception_Record_ForDelete( int row, int column )
{
    this->rowAutoRecept = row;
	this->columnAutoRecept = column;
    if( loginWindowObject->role == loginWindowObject->ADMIN ) // выбрана роль
       deleteRecord->setEnabled( true ); // разрешить кнопку удаления записи
}
// Отменить пометку строки для подготовки удаления записи из таблицы приёмки/отправки автомобилей
void MyClass::unselectAutoReception_Record_Item()
{
	this->rowAutoRecept = -1;
    this->columnAutoRecept = -1;
}

// Отобразить данные по принятым автомобилям
void MyClass::show_InAutoAcception()
{
	bool result;
	QSqlError error;
	int type_operation;

	if( radioButtAutoComeIn->isChecked() == true )
	{
		type_operation = 0;
	}
	else if( radioButtAutoComeOut->isChecked() == true )
	{
		type_operation = 1;
	}

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

	result = query.exec( QString( "SELECT * FROM AutoReception WHERE type_operation=%1;" ).arg( type_operation ) );

	QString val;
	int i = 0, j = 0;
	while( query.next() )
	{
	    tableAutoReception->setRowCount( j + 1 );

        while( i < tableAutoReception->columnCount() )
		{
		    if( i + 3 == DATETIME_AUTORECEPTION_COLUMN )
                val = query.value( i + 3 ).asDateTime().toString( "dd.MM.yyyy hh:mm:ss" ); // ячейка времени/даты
			else if( i + 3 == ENTERTARA_AUTORECEPTION_COLUMN )
			{
				int tara_type = query.record().value( "tara_type" ).toInt();
                if( tara_type == 0 )
					val = tr("From scales");
				else if( tara_type == 1 )
					val = tr("Manually");
				else if( tara_type == 2 )
					val = tr("Saved tara");
			}
			else
                val = query.value( i + 3 ).toString();

			tableAutoReception->setItem( j, i, new QTableWidgetItem( val ) );
			i++;
        }
		j++;
		i = 0;
    }

    // Удаляем строки в таблице приёмки/отправки автомобилей
	int row = tableAutoReception->rowCount();
	while( row > 0 )
	{
	    tableAutoReception->removeRow( row - 1 );
	    row--;
	}
	showDataBaseContent_Slot(); // отобразить записи в таблице приёмки/отправки автомобилей
}

// Отобразить данные по принятым автомобилям в режиме RFID
void MyClass::show_CardsAcception()
{
	bool result;
	QSqlError error;
	int type_operation;

	if (radioButtAutoComeIn->isChecked() == true)
	{
		type_operation = 0;
	}
	else if (radioButtAutoComeOut->isChecked() == true)
	{
		type_operation = 1;
	}

	// Удаляем строки в таблице приёмки/отправки автомобилей
	int row = tableAutoReception->rowCount();
	while (row > 0)
	{
		tableAutoReception->removeRow(row - 1);
		row--;
	}

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.prepare(QString("USE %1;").arg(dataBaseName));
	result = query.exec();

	result = query.exec(QString("SELECT * FROM acceptcards;")); //  WHERE type_operation=%1;").arg(type_operation));

	QString val;
	int i = 0, j = 0;
	while (query.next())
	{
		tableAutoReception->setRowCount(j + 1);

		while (i < tableAutoReception->columnCount())
		{
			if (i + 1 == DATETIME_AUTORECEPTION_COLUMN_CARDS)
				val = query.value(i + 1).asDateTime().toString("dd.MM.yyyy hh:mm:ss"); // ячейка времени/даты

			else
				val = query.value(i + 1).toString();

			tableAutoReception->setItem(j, i, new QTableWidgetItem(val));
			i++;
		}
		j++;
		i = 0;
	}


	//showDataBaseContent_Slot(); // отобразить записи в таблице приёмки/отправки автомобилей
}


// Вызов окна смены пользователя
void  MyClass::changeUser()
{
	loginWindowObject->show();
}
// Вызов окна добавления пользователя
void MyClass::addUser()
{
	if( loginWindowObject->role != loginWindowObject->ADMIN )
        return;

	loginWindowObject->showCreateAccountWin();
}
// Клик по кнопке "Принять автомобиль"
void MyClass::buttonAutoComeIn_Clicked()
{
	this->rowAutoRecept = -1;

	// Сброс параметров форм приёмки/отправки автомобилей
	acceptform->resetValidation(); // сброс валидации формы приёмки/отпраки

	acceptform->clearControls();
	// acceptFormModelObject->clear();

	if( radioButtAutoComeOut->isChecked() == true )
	{
		acceptform->acceptingAuto->setChecked( false );
		acceptform->shippingAuto->setChecked( true );
		// acceptform->checkBoxPrizepWeightEnable->setChecked( false );

		acceptform->enableDisablePrizepWeight( acceptform->checkBoxPrizepWeightEnable->isChecked() );

		// acceptform->enableShippingFormControls();
	}
	else
	{
		acceptform->acceptingAuto->setChecked( true );
		acceptform->shippingAuto->setChecked( false );
		// acceptform->checkBoxPrizepWeightEnable->setChecked( false );

        acceptform->enableDisablePrizepWeight( acceptform->checkBoxPrizepWeightEnable->isChecked() );

		// acceptform->enableAcceptingFormControls(); // разрешить те контролы, которые присущи данному режиму
	}

	acceptform->receptionDateTime->setText( QDateTime::currentDateTime().toString( "dd.MM.yyyy hh:mm:ss" ) );
	acceptform->operatorUserRecept->setText( loginWindowObject->currentNameUser );
	this->show_AcceptionForm(); // отобразить форму приёмки/отправки автомобилей

	this->rowAutoRecept = -1;
	this->columnAutoRecept = -1;
}

// Клик по кнопке "Отправить автомобиль"
void MyClass::buttonAutoComeOut_Clicked()
{
	acceptform->resetValidation(); // сброс валидации формы приёмки/отпраки

	if( this->rowAutoRecept != -1 )
	{
		QTableWidgetItem *itemData = new QTableWidgetItem;
		int i = 0;

		if( radioButtAutoComeOut->isChecked() == false )
		{
			acceptform->acceptingAuto->setChecked( false );
			acceptform->shippingAuto->setChecked( true );
			// acceptform->checkBoxPrizepWeightEnable->setChecked( false );
		    acceptform->enableDisablePrizepWeight( acceptform->checkBoxPrizepWeightEnable->isChecked() );
			// acceptform->enableShippingFormControls();
		}
		else
		{
			acceptform->acceptingAuto->setChecked( true );
			acceptform->shippingAuto->setChecked( false );
			// acceptform->checkBoxPrizepWeightEnable->setChecked( false );
		    acceptform->enableDisablePrizepWeight( acceptform->checkBoxPrizepWeightEnable->isChecked() );
			// acceptform->enableAcceptingFormControls(); // разрешить те контролы, которые присущи данному режиму
		}
		//
		itemData = tableAutoReception->item( this->rowAutoRecept, i++ );
		acceptform->AutoNumber->setCurrentText( itemData->text() );  //   receptionDateTime->setText( itemData->text() );
        //
		itemData = tableAutoReception->item( this->rowAutoRecept, i++ );
		acceptform->sendersList->setCurrentText( itemData->text() );
        //
		itemData = tableAutoReception->item( this->rowAutoRecept, i++ );
		acceptform->recepientsList->setCurrentText( itemData->text() );
		//
		itemData = tableAutoReception->item( this->rowAutoRecept, i++ );
		acceptform->payersList->setCurrentText( itemData->text() );
		//
		itemData = tableAutoReception->item( this->rowAutoRecept, i++ );
		acceptform->namesCarrierList->setCurrentText( itemData->text() );
		//
		itemData = tableAutoReception->item( this->rowAutoRecept, i++ );
		acceptform->namesGoods->setCurrentText( itemData->text() );
		//
		itemData = tableAutoReception->item( this->rowAutoRecept, i++ );
		acceptform->pricePerUnit->setText( itemData->text() );
        //
		// itemData = tableAutoReception->item( this->rowAutoRecept, i++ );
		// acceptform->enterHumidity->setText( itemData->text() );
        //
		itemData = tableAutoReception->item( this->rowAutoRecept, i++ );
		acceptform->bruttoWeight->setText( itemData->text() );
		//
		itemData = tableAutoReception->item( this->rowAutoRecept, i++ );
		acceptform->taraWeight->setText( itemData->text() );
		//
		itemData = tableAutoReception->item( this->rowAutoRecept, i++ );
		acceptform->nettoWeight->setText( itemData->text() );
		//
		itemData = tableAutoReception->item( this->rowAutoRecept, i++ );
		acceptform->receptionDateTime->setText( itemData->text() );
		//
		itemData = tableAutoReception->item( this->rowAutoRecept, i++ );
		acceptform->documentNumber->setText( itemData->text() );
		//
		itemData = tableAutoReception->item( this->rowAutoRecept, i++ );
		acceptform->operatorUserRecept->setText( itemData->text() );
        //
		itemData = tableAutoReception->item( this->rowAutoRecept, i++ );
		acceptform->AutoPrizepNumber->setCurrentText( itemData->text() );
        //
		itemData = tableAutoReception->item( this->rowAutoRecept, i++ );
		acceptform->bruttoWeightPricep->setText( itemData->text() );
		//
		itemData = tableAutoReception->item( this->rowAutoRecept, i++ );
		acceptform->taraWeightPricep->setText( itemData->text() );
		//
		itemData = tableAutoReception->item( this->rowAutoRecept, i++ );
		acceptform->nettoWeightPricep->setText( itemData->text() );
		// Отправки автомобиля
		acceptform->shippingDateTime->setText( QDateTime::currentDateTime().toString( "dd.MM.yyyy hh:mm:ss" ) );
		acceptform->operatorUserShipping->setText( loginWindowObject->currentNameUser );
		//
		if( acceptform->bruttoWeightPricep->text() != "0" || acceptform->taraWeightPricep->text() != "0" )
		{
		    acceptform->checkBoxPrizepWeightEnable->setChecked( true ); // установить флаг взвешивания прицепа, если брутто или тара прицепа для  записи взвешивания != 0
		}
		else
			acceptform->checkBoxPrizepWeightEnable->setChecked( false );
		//
	    this->show_AcceptionForm(); // отобразить форму приёмки/отправки автомобилей

		// delete itemData;
	}
	else // не выбрана запись по отправки автомобиля
	{
        QMessageBox mess;
	    mess.setText( tr("Select the entry first!") );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();
	}
}

// Клик по кнопке "Регистрация"
void MyClass::buttonRegistration_Clicked()
{
	this->rowAutoRecept = -1;

	// Сброс параметров форм приёмки/отправки автомобилей
	acceptform->resetValidation(); // сброс валидации формы приёмки/отпраки

	acceptform->clearControls();
	// acceptFormModelObject->clear();

	if (radioButtAutoComeOut->isChecked() == true)
	{
		acceptform->acceptingAuto->setChecked(false);
		acceptform->shippingAuto->setChecked(true);
		// acceptform->checkBoxPrizepWeightEnable->setChecked( false );

		acceptform->enableDisablePrizepWeight(acceptform->checkBoxPrizepWeightEnable->isChecked());

		// acceptform->enableShippingFormControls();
	}
	else
	{
		acceptform->acceptingAuto->setChecked(true);
		acceptform->shippingAuto->setChecked(false);
		// acceptform->checkBoxPrizepWeightEnable->setChecked( false );

		acceptform->enableDisablePrizepWeight(acceptform->checkBoxPrizepWeightEnable->isChecked());

		// acceptform->enableAcceptingFormControls(); // разрешить те контролы, которые присущи данному режиму
	}

	acceptform->receptionDateTime->setText(QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss"));
	acceptform->operatorUserRecept->setText(loginWindowObject->currentNameUser);
	//acceptform->
	this->show_AcceptionForm(); // отобразить форму приёмки/отправки автомобилей

	this->rowAutoRecept = -1;
	this->columnAutoRecept = -1;
}

// Подтвердить форму приёмки/отправки
void MyClass::confirmAcceptForm()
{
	bool flagValidation = acceptform->validationAcceptForm(); // валидация вводимых данных

	// Debug
	if( flagValidation == false  ) // если форма неверно  заполнена или нет связи с весами, то запретить подтверждение формы
	{
		return;
	}

    #ifndef DEBUG
	if( isVPConnected == false && settings->group_TCPWeight->isChecked() ) //
	{
			QMessageBox mess;
			mess.setText( tr("Communication with scales missing!") ); //Связь с весами отсутствует!
			mess.setStandardButtons( QMessageBox::Cancel );
			mess.setButtonText( QMessageBox::Cancel, "Ok" );
			mess.setIcon( QMessageBox::Warning );
			mess.setWindowTitle( tr( "Prom-Soft" ) );
			mess.exec();

		return;
	}
	if( !settings->group_TCPWeight->isChecked() )
	{
		if( flagValidation == false || flagsNoUarts_connection[ settings->currentScales ] == false ) // если форма неверно  заполнена или нет связи с весами, то запретить подтверждение формы
		{
			if( flagsNoUarts_connection[ settings->currentScales ] == false )
			{
				QMessageBox mess;
				mess.setText( tr("Communication with scales missing!") ); //Связь с весами отсутствует!
				mess.setStandardButtons( QMessageBox::Cancel );
				mess.setButtonText( QMessageBox::Cancel, "Ok" );
				mess.setIcon( QMessageBox::Warning );
				mess.setWindowTitle( tr( "Prom-Soft" ) );
				mess.exec();
			}

			return;
		}
	}
    #endif

	if( this->rowAutoRecept != -1 && this->columnAutoRecept != -1 )
	{
        QString date_time;
		QString numAuto = tableAutoReception->item( this->rowAutoRecept, 0 )->text();  //Записываем номер авто, чтоб передать на сохранение для бота
		date_time = tableAutoReception->item( this->rowAutoRecept, 10 )->text();

		float bruttoBot = (acceptform->bruttoWeight->text()).toFloat(); // БРУТТО для бота
		float taraBot = acceptform->taraWeight->text().toFloat();     // ТАРА для бота
		float nettoBot = acceptform->nettoWeight->text().toFloat(); // НЕТТО для бота


		QDateTime dt = QDateTime::fromString( date_time, "dd.MM.yyyy hh:mm:ss" );
		date_time = dt.toString( "yyyy-MM-dd hh:mm:ss" );
		
		makeShippingAuto( date_time ); // отправка автомобиля с территории загрузки/разгрузки

		if ( settings->checkBoxSendEachRec->isChecked() )
			timerSendMailEachRec->start();
	    makePhotos();

		stateHit = 4;

	    if( optionsObj->isCheked[optionsObj->modules.indexOf("bot")] )
	      {
		    int count_zaderjka = 0;
		    do
			{
			  count_zaderjka++;
			  QApplication::processEvents(QEventLoop::AllEvents, 100);
			}
			while(count_zaderjka<20);
            //Сохраняем фотки принудительно
		    savePhotoForBot(date_time, numAuto, bruttoBot, nettoBot, taraBot);
	      }
		return;
	}

	//  меняем курсор
	emit cursorChange_Signal( true );

	if( acceptform->acceptingAuto->isChecked() == true )
		acceptFormModelObject->type_operation = 0;
	else if( acceptform->shippingAuto->isChecked() == true )
		acceptFormModelObject->type_operation = 1;

	acceptFormModelObject->Nakladnaya =  acceptform->documentNumber->text(); // накладная
	acceptFormModelObject->Number_Auto = acceptform->AutoNumber->currentText(); // номер авто
	acceptFormModelObject->Num_Prizep = acceptform->AutoPrizepNumber->currentText(); // номер прицепа
	acceptFormModelObject->Sender = acceptform->sendersList->currentText(); // имя отправителя
	acceptFormModelObject->Accepter = acceptform->recepientsList->currentText(); // имя получателя
	acceptFormModelObject->Payer = acceptform->payersList->currentText(); // плательщик
	acceptFormModelObject->Transporter = acceptform->namesCarrierList->currentText(); // перевозчик
	acceptFormModelObject->Goods = acceptform->namesGoods->currentText(); // тип груза
	acceptFormModelObject->Price = acceptform->pricePerUnit->text().toFloat(); // стоимость груза

	if(acceptform->enterTaraManually->isChecked())		acceptFormModelObject->tara_type = 1;
	else if(acceptform->checkTaraFromBD->isChecked())	acceptFormModelObject->tara_type = 2;
	else	acceptFormModelObject->tara_type = 0;

	// Взвешивание без прицепа
	if( acceptform->checkBoxPrizepWeightEnable->isChecked() == false )
	{
		if( acceptform->bruttoWeight->text().toDouble() <= 0 && acceptform->taraWeight->text().toDouble() <= 0 ) // если взвешивание автомобиля и вес  <= 0, то запретить  закрытие формы
		{
		    if( acceptform->validationAcceptForm() == false ) // валидация вводимых данных
		        return;
		}

		acceptFormModelObject->Brutto = acceptform->bruttoWeight->text().toDouble(); // БРУТТО фактический вес
		if( acceptform->enterTaraWeightManualy->text() == "" || acceptform->enterTaraWeightManualy->text() == "0" ) // если поле ввода ТАРЫ пустое, то используем тару измеренную от весов
			acceptFormModelObject->Tara = acceptform->taraWeight->text().toDouble(); // ТАРА
		else // иначе - используем тару, введённую вручную
		{
			acceptform->taraWeight->setText( acceptform->enterTaraWeightManualy->text() ); // ТАРА
			acceptFormModelObject->Tara = acceptform->taraWeight->text().toDouble();
			makeNettoWeight();
		}
		acceptFormModelObject->Netto = acceptform->nettoWeight->text().toDouble(); // НЕТТО
	}

	// Взвесить прицеп отдельно
	if( acceptform->checkBoxPrizepWeightEnable->isChecked() == true )
	{
		if( acceptform->bruttoWeightPricep->text().toDouble() <= 0 && acceptform->taraWeightPricep->text().toDouble() <= 0 ) // если взвешивание прицепа и вес  <= 0, то запретить  закрытие формы
		{
		    if( acceptform->validationAcceptForm() == false ) // валидация вводимых данных
		        return;
		}

		acceptFormModelObject->Brutto_Prizep = acceptform->bruttoWeightPricep->text().toDouble(); // БРУТТО фактический вес
		if( acceptform->enterTaraWeightManualyPricep->text() == "" || acceptform->enterTaraWeightManualyPricep->text() == "0" ) // если поле ввода ТАРЫ пустое, то используем тару измеренную от весов
			acceptFormModelObject->Tara_Prizep = acceptform->taraWeightPricep->text().toDouble(); // ТАРА
		else // иначе - используем тару, введённую вручную
		{
			acceptform->taraWeightPricep->setText( acceptform->enterTaraWeightManualyPricep->text() ); // ТАРА
			acceptFormModelObject->Tara_Prizep = acceptform->taraWeightPricep->text().toDouble();
			makeNettoWeightPricep();
		}
		acceptFormModelObject->Netto_Prizep = acceptform->nettoWeightPricep->text().toDouble(); // НЕТТО

		// машинку в 0
		acceptFormModelObject->Netto = 0;
		acceptFormModelObject->Brutto = 0;
		acceptFormModelObject->Tara = 0;
	}
    //
	acceptFormModelObject->Brutto_Prizep = acceptform->bruttoWeightPricep->text().toDouble(); // Брутто
	acceptFormModelObject->Tara_Prizep = acceptform->taraWeightPricep->text().toDouble(); // Тара
	acceptFormModelObject->Netto_Prizep = acceptform->nettoWeightPricep->text().toDouble(); // Нетто

	acceptFormModelObject->Name_User_Accept = acceptform->operatorUserRecept->text(); // имя пользователя при приёмке авто
	acceptFormModelObject->Date_Time = QDateTime::currentDateTime().toString( "yyyy-MM-dd hh:mm:ss" );

	//
	QSettings settings_data( "settings.ini", QSettings::IniFormat );
    settings_data.setIniCodec("Windows-1251");

	settings_data.setValue( "freqData/numAuto", acceptFormModelObject->Number_Auto );
	settings_data.setValue( "freqData/sender", acceptFormModelObject->Sender );
	settings_data.setValue( "freqData/recepient", acceptFormModelObject->Accepter );
	settings_data.setValue( "freqData/payer", acceptFormModelObject->Payer );
	settings_data.setValue( "freqData/carrier", acceptFormModelObject->Transporter );
	settings_data.setValue( "freqData/goods", acceptFormModelObject->Goods );
	settings_data.sync();

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// Записываем в БД принятый автомобиль
	bool result;
	QSqlError error;

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );
    //
	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

	// Создаём запись в таблице с фотоснимками автомобилей (Штамп Даты/Времени)
	imageSaveModelObject->Date_Time = acceptFormModelObject->Date_Time; // сохраняем  Дату/Время
	imageSaveModelObject->num_auto = acceptFormModelObject->Number_Auto;

	query.prepare( QString( "INSERT INTO images( Date_Time ) VALUES( :Date_Time );" ) );
	query.bindValue( ":Date_Time", imageSaveModelObject->Date_Time );
	query.exec();

	//query.exec( QString( "SELECT ID FROM images WHERE Date_Time = '%1';" ).arg( imageSaveModelObject->Date_Time ) );
	query.exec( QString( "SELECT LAST_INSERT_ID();" ) ); // в отличии от предыдущего запроса выполняется мгновенно

	int id_img = 0;

	if( query.next() )
	{
		id_img = query.value( 0 ).toInt();
	}

	//
	query.prepare( QString( "INSERT INTO sor( is_report, real_weight_brutto, real_weight_tara, IDF ) "
		"VALUES( :is_report, :real_weight_brutto, :real_weight_tara, :IDF );" ) );
	query.bindValue( ":is_report", 0 );
	query.bindValue( ":real_weight_brutto", acceptFormModelObject->type_operation == 0 ? weightScales_last_no_sor : 0 );
	query.bindValue( ":real_weight_tara", acceptFormModelObject->type_operation == 1 ? weightScales_last_no_sor : 0 );
	query.bindValue( ":IDF", id_img );
	query.exec();

    query.prepare( QString( "INSERT INTO AutoReception( type_operation, num_scales, Date_Time, Nakladnaya, Name_User_Accept, Number_Auto, Brutto, Tara, Netto, Num_Prizep, Brutto_Prizep, "
		"Tara_Prizep, Netto_Prizep, Goods, Price, Sender, Accepter, Payer, Transporter, IDF, tara_type ) VALUES( :type_operation, :num_scales, :Date_Time, :Nakladnaya, :Name_User_Accept, :Number_Auto, "
		":Brutto, :Tara, :Netto, :Num_Prizep, :Brutto_Prizep, :Tara_Prizep, :Netto_Prizep, :Goods, :Price, :Sender, :Accepter, :Payer, :Transporter, :IDF, :tara_type );" ) );
	query.bindValue( ":type_operation", acceptFormModelObject->type_operation );
	query.bindValue( ":num_scales", settings->currentScales );
	query.bindValue( ":Date_Time", acceptFormModelObject->Date_Time );
	query.bindValue( ":Nakladnaya", acceptFormModelObject->Nakladnaya );
	query.bindValue( ":Name_User_Accept", acceptFormModelObject->Name_User_Accept );
	query.bindValue( ":Number_Auto", acceptFormModelObject->Number_Auto );
	query.bindValue( ":Brutto", acceptFormModelObject->Brutto );
	query.bindValue( ":Tara", acceptFormModelObject->Tara );
	query.bindValue( ":Netto", acceptFormModelObject->Netto );
	query.bindValue( ":Num_Prizep", acceptFormModelObject->Num_Prizep );
	query.bindValue( ":Brutto_Prizep", acceptFormModelObject->Brutto_Prizep );
	query.bindValue( ":Tara_Prizep", acceptFormModelObject->Tara_Prizep );
	query.bindValue( ":Netto_Prizep", acceptFormModelObject->Netto_Prizep );
	query.bindValue( ":Goods", acceptFormModelObject->Goods );
	query.bindValue( ":Price", acceptFormModelObject->Price );
	// query.bindValue( ":humidity", acceptFormModelObject->humidity );
	query.bindValue( ":Sender", acceptFormModelObject->Sender );
	query.bindValue( ":Accepter", acceptFormModelObject->Accepter );
	query.bindValue( ":Payer", acceptFormModelObject->Payer );
	query.bindValue( ":Transporter", acceptFormModelObject->Transporter );
	query.bindValue( ":IDF", id_img );
	query.bindValue( ":tara_type", acceptFormModelObject->tara_type );

	result = query.exec();
	error = query.lastError();

	imageSaveModelObject->num_measure = 0; // первое взвешивание   ???
	makePhotos();

	if( error.type() == QSqlError::NoError )
	{
		// Если ошибка выполнения запроса, то выйти  их процедуры
		if( error.type() != QSqlError::NoError )
		{
		}
		// Записать событие в лог
		mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		if( acceptform->checkBoxPrizepWeightEnable->isChecked() == true )
		{
			mysqlProccessObject->saveEventToLog( tr("Trailer accepted:") + QString("%1")
				.arg( acceptFormModelObject->Num_Prizep ) );
		}
		else
		{
			mysqlProccessObject->saveEventToLog( tr("Vehicle accepted:") + QString("%1")
				.arg( acceptFormModelObject->Number_Auto ) );
		}

        // Закрыть окна приёмки/отправки автомобилей
		acceptform->acceptionFormWindow->hide();
		showDataBaseContent_Slot(); // отобразить записи в таблице приёмки/отправки автомобилей
		acceptform->clearControls();

		stateHit = 4;
	}

	QTimer::singleShot( 8000, this, SLOT( timerCursorOff_Slot() ) );
}

//
void MyClass::makePhotos()
{
	// прятки
	if( !optionsObj->isCheked[optionsObj->modules.indexOf("camera")] ) return;

	bool cam1;
	bool cam2;
	bool cam3;
	bool cam4;

	settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 0 ); // камера №1
	cam1 = currentSettingsIP_CamPtr->cam_photofix_en;

	settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 1 ); // камера №2
	cam2 = currentSettingsIP_CamPtr->cam_photofix_en;

	settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 2 ); // камера №3
	cam3 = currentSettingsIP_CamPtr->cam_photofix_en;

	settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 3 ); // камера №4
	cam4 = currentSettingsIP_CamPtr->cam_photofix_en;


	if( cam1 == true || cam2 == true || cam3 == true || cam4 == true )
	{
		timerRequestIP_Cams->blockSignals( true );
		timerRequestIP_Cams->stop();
		this->states_ip_cams = SNAPSHOT;
		Sleep( 1000 );
	}

	// Делаем фотоснимки
	if( cam1 == true )
	{
		settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 0 );

		sendRequestIP_Cam( networkAccessManager_01, currentSettingsIP_CamPtr->ip_address,
			currentSettingsIP_CamPtr->port.toInt(), currentSettingsIP_CamPtr->login,
			currentSettingsIP_CamPtr->password, 0 );


	}

	if( cam2 == true )
	{
		settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 1 );

		sendRequestIP_Cam( networkAccessManager_02, currentSettingsIP_CamPtr->ip_address,
			currentSettingsIP_CamPtr->port.toInt(), currentSettingsIP_CamPtr->login,
			currentSettingsIP_CamPtr->password, 1 );


	}

	if( cam3 == true )
	{
		settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 2 );

		sendRequestIP_Cam( networkAccessManager_03, currentSettingsIP_CamPtr->ip_address,
			currentSettingsIP_CamPtr->port.toInt(), currentSettingsIP_CamPtr->login,
			currentSettingsIP_CamPtr->password, 2 );


	}

	if( cam4 == true )
	{
		settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 3 );

		sendRequestIP_Cam( networkAccessManager_04, currentSettingsIP_CamPtr->ip_address,
			currentSettingsIP_CamPtr->port.toInt(), currentSettingsIP_CamPtr->login,
			currentSettingsIP_CamPtr->password, 3 );

	}

	timeOutSnapshotIP_Cams->start();

	timerRequestIP_Cams->blockSignals( false );
	this->states_ip_cams = NO_SNAPSHOT;
	timerRequestIP_Cams->start();
}

//
void MyClass::timeOutSnapshotIP_Cams_Slot()
{
    this->states_ip_cams = NO_SNAPSHOT;
	qDebug( "End of Snapshots" );
}

// start separate thread for saving photos (for email send)
void MyClass::startThreadSendEmail_Slot()
{
	if( ! threadSavePhotos.isRunning() && settings->checkBoxEnableMailSend->isChecked() )
		threadSavePhotos.start();
}

// Отправка автомобиля с территории загрузки/разгрузки
void MyClass::makeShippingAuto( QString date_time )
{
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// Записываем в БД принятый автомобиль
	bool result;
	QSqlError error;

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );
	//
	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();
	//

	if( radioButtAutoComeIn->isChecked() == true && radioButtAutoComeOut->isChecked() == false )
	    acceptFormModelObject->type_operation = 0;
	else if( radioButtAutoComeOut->isChecked() == true && radioButtAutoComeIn->isChecked() == false )
		acceptFormModelObject->type_operation = 1;

	acceptFormModelObject->Name_User_Accept = acceptform->operatorUserRecept->text(); // имя пользователя при приёмке авто

	QString dateTimeString = acceptform->receptionDateTime->text();
	QDateTime dateTime = QDateTime::fromString( dateTimeString, "dd.MM.yyyy hh:mm:ss" );
	dateTimeString = dateTime.toString( "yyyy-MM-dd hh:mm:ss" );

	acceptFormModelObject->Date_Time = dateTimeString; // acceptform->receptionDateTime->text(); //  Дата/Время  приёмки  авто

	acceptFormModelObject->Name_User_Send = acceptform->operatorUserShipping->text(); // имя пользователя при приёмке авто
	acceptFormModelObject->Date_Time_Send = QDateTime::currentDateTime().toString( "yyyy-MM-dd hh:mm:ss" );

	acceptFormModelObject->Nakladnaya =  acceptform->documentNumber->text(); // накладная
	acceptFormModelObject->Number_Auto = acceptform->AutoNumber->currentText(); // номер авто
	acceptFormModelObject->Num_Prizep = acceptform->AutoPrizepNumber->currentText(); // номер прицепа
	acceptFormModelObject->Sender = acceptform->sendersList->currentText(); // имя отправителя
	acceptFormModelObject->Accepter = acceptform->recepientsList->currentText(); // имя получателя
	acceptFormModelObject->Payer = acceptform->payersList->currentText(); // плательщик
	acceptFormModelObject->Transporter = acceptform->namesCarrierList->currentText(); // перевозчик
	acceptFormModelObject->Goods = acceptform->namesGoods->currentText(); // тип груза
	acceptFormModelObject->Price = acceptform->pricePerUnit->text().toFloat(); // стоимость груза

	if( acceptform->shippingAuto->isChecked() == true )
	{
		if(acceptform->enterTaraManually->isChecked())		acceptFormModelObject->tara_type = 1;
		else if(acceptform->checkTaraFromBD->isChecked())	acceptFormModelObject->tara_type = 2;
		else	acceptFormModelObject->tara_type = 0;
	}
	else
	{
		result = query.exec( QString( "SELECT tara_type FROM AutoReception WHERE Date_Time='%1';" ).arg( date_time ) );
		if( query.next() )
		{
			acceptFormModelObject->tara_type = query.value( 0 ).toInt();
		}
	}

	acceptFormModelObject->Brutto = acceptform->bruttoWeight->text().toDouble(); // БРУТТО фактический вес
	if( acceptform->enterTaraWeightManualy->text() == "" || acceptform->enterTaraWeightManualy->text() == "0" ) // если поле ввода ТАРЫ пустое, то используем тару измеренную от весов
		acceptFormModelObject->Tara = acceptform->taraWeight->text().toDouble(); // ТАРА
	else // иначе - используем тару, введённую вручную
	{
		acceptform->taraWeight->setText( acceptform->enterTaraWeightManualy->text() ); // ТАРА
		acceptFormModelObject->Tara = acceptform->taraWeight->text().toDouble();
		makeNettoWeight();
	}

	acceptFormModelObject->Netto = acceptform->nettoWeight->text().toDouble(); // НЕТТО

	// Если НЕТТО отрицательное при отправке автомобиля, то  создать предупреждение об этом
	if( acceptform->nettoWeight->text().toDouble() < 0 ) // && acceptform->enterTaraWeightManualy != "0" && acceptform->enterTaraWeightManualy != ""   )
	{
		if( acceptform->validationAcceptForm() == false ) // валидация вводимых данных
		    return;
	}
    // Если взвешивание автомобиля и вес  <= 0, то запретить  закрытие формы
	if( acceptform->checkBoxPrizepWeightEnable->isChecked() == false )
	{
		if( acceptform->bruttoWeight->text().toDouble() <= 0 && acceptform->taraWeight->text().toDouble() <= 0 )
		{
		    if( acceptform->validationAcceptForm() == false ) // валидация вводимых данных
		        return;
		}
	}
	// Если взвешивание прицепа и вес  <= 0, то запретить  закрытие формы
	if( acceptform->checkBoxPrizepWeightEnable->isChecked() == true )
	{
		if( acceptform->bruttoWeightPricep->text().toDouble() <= 0 && acceptform->taraWeightPricep->text().toDouble() <= 0 )
		{
		    if( acceptform->validationAcceptForm() == false ) // валидация вводимых данных
		        return;
		}
	}
    //
	acceptFormModelObject->Brutto_Prizep = acceptform->bruttoWeightPricep->text().toDouble(); // Брутто
	acceptFormModelObject->Tara_Prizep = acceptform->taraWeightPricep->text().toDouble(); // Тара
	acceptFormModelObject->Netto_Prizep = acceptform->nettoWeightPricep->text().toDouble(); // Нетто

    // Если НЕТТО прицепа отрицательное при отправке автомобиля, то  создать предупреждение об этом
	if( acceptform->nettoWeightPricep->text().toDouble() < 0 )
	{
		acceptform->validationAcceptForm(); // валидация вводимых данных
		return;
	}

	result = query.exec( QString( "SELECT IDF FROM AutoReception WHERE Date_Time='%1';" ).arg( date_time ) );

	int idf = 0;

	if( query.next() )
	{
		idf = query.value( 0 ).toInt();
	}

	imageSaveModelObject->num_measure = 1;
	acceptFormModelObject->Date_Time = date_time;
	acceptFormModelObject->id_photo = idf;

	acceptFormModelObject->Brutto_Prizep = acceptform->bruttoWeightPricep->text().toDouble(); // Брутто
	acceptFormModelObject->Tara_Prizep = acceptform->taraWeightPricep->text().toDouble(); // Тара
	acceptFormModelObject->Netto_Prizep = acceptform->nettoWeightPricep->text().toDouble(); // Нетто

	query.exec( QString( "UPDATE sor SET "
		"is_report = 1, "
		"real_weight_%1 = %2 "
		"WHERE IDF = %3;" )
		.arg( acceptFormModelObject->type_operation == 0 ? "tara" : "brutto" )
		.arg( weightScales_last_no_sor )
		.arg( idf ) );

	// Формируем запрос для записи в таблицу отчётов
	query.prepare( QString( "INSERT INTO ReportsWeight( type_operation, num_scales, Date_Time_Accept, Name_User_Accept, Date_Time_Send, Name_User_Send, Nakladnaya, Number_Auto, Brutto, "
		"Tara, Netto, Num_Prizep, Brutto_Prizep, Tara_Prizep, Netto_Prizep, Goods, Price, Sender, Accepter, Payer, Transporter, IDF, tara_type ) VALUES( :type_operation, :num_scales, :Date_Time_Accept, "
		":Name_User_Accept, :Date_Time_Send, :Name_User_Send, :Nakladnaya, :Number_Auto, :Brutto, :Tara, :Netto, :Num_Prizep, :Brutto_Prizep, :Tara_Prizep, :Netto_Prizep, :Goods, :Price, "
		":Sender, :Accepter, :Payer, :Transporter, :IDF, :tara_type );" ) );

	query.bindValue( ":type_operation", acceptFormModelObject->type_operation );
	query.bindValue( ":num_scales", settings->currentScales );
	query.bindValue( ":Date_Time_Accept", acceptFormModelObject->Date_Time );
	query.bindValue( ":Name_User_Accept", acceptFormModelObject->Name_User_Accept );
	query.bindValue( ":Date_Time_Send", acceptFormModelObject->Date_Time_Send );
	query.bindValue( ":Name_User_Send", acceptFormModelObject->Name_User_Send );
	query.bindValue( ":Nakladnaya", acceptFormModelObject->Nakladnaya );
	query.bindValue( ":Number_Auto", acceptFormModelObject->Number_Auto );
	query.bindValue( ":Brutto", acceptFormModelObject->Brutto );
	query.bindValue( ":Tara", acceptFormModelObject->Tara );
	query.bindValue( ":Netto", acceptFormModelObject->Netto );
	query.bindValue( ":Num_Prizep", acceptFormModelObject->Num_Prizep );
	query.bindValue( ":Brutto_Prizep", acceptFormModelObject->Brutto_Prizep );
	query.bindValue( ":Tara_Prizep", acceptFormModelObject->Tara_Prizep );
	query.bindValue( ":Netto_Prizep", acceptFormModelObject->Netto_Prizep );
	query.bindValue( ":Goods", acceptFormModelObject->Goods );
	query.bindValue( ":Price", acceptFormModelObject->Price );
	// query.bindValue( ":humidity", acceptFormModelObject->humidity );
	query.bindValue( ":Sender", acceptFormModelObject->Sender );
	query.bindValue( ":Accepter", acceptFormModelObject->Accepter );
	query.bindValue( ":Payer", acceptFormModelObject->Payer );
	query.bindValue( ":Transporter", acceptFormModelObject->Transporter );
	query.bindValue( ":IDF", idf );
	query.bindValue( ":tara_type", acceptFormModelObject->tara_type );

	imageSaveModelObject->Date_Time = acceptFormModelObject->Date_Time; // сохраняем  Дату/Время отправки авто
	imageSaveModelObject->Date_Time_Send = acceptFormModelObject->Date_Time_Send;

	imageSaveModelObject->num_auto = acceptFormModelObject->Number_Auto;

	error = query.lastError();

	result = query.exec();
	error = query.lastError();

	if( error.type() == QSqlError::NoError )
	{
		result = query.exec( QString( "DELETE FROM AutoReception WHERE IDF='%1';" ).arg( idf ) );
		error = query.lastError();
		//

		show_InAutoAcception();
		acceptform->acceptionFormWindow->hide();

	    // Записать событие в лог
		mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		if( acceptform->checkBoxPrizepWeightEnable->isChecked() == true )
		{
			mysqlProccessObject->saveEventToLog( tr("Trailer send:") + QString("%1")
				.arg( acceptFormModelObject->Num_Prizep ) );
		}
		else
		{
			mysqlProccessObject->saveEventToLog( tr("Vehicle send:") + QString( "%1")
				.arg( acceptFormModelObject->Number_Auto ) );
		}

		formTTN_Object->saveDataFromTTN_Form( acceptFormModelObject->Nakladnaya, acceptFormModelObject->Date_Time, acceptFormModelObject->Date_Time_Send, acceptFormModelObject->Number_Auto, acceptFormModelObject->Num_Prizep, acceptFormModelObject->Transporter, "",
			acceptFormModelObject->Sender, acceptFormModelObject->Accepter, acceptFormModelObject->Netto, acceptFormModelObject->Brutto, acceptFormModelObject->Price, acceptFormModelObject->Goods ); // сделать запись в ТТН таблице


		acceptform->clearControls();
		// acceptFormModelObject->clear();
		acceptform->checkBoxPrizepWeightEnable->setChecked( false ); // сбросить флаг взвешивания прицепа

	    return;
	}
}

// Отменить строку для отправки автомобиля
void MyClass::shippingAuto( int row, int column )
{
	this->rowAutoRecept = row;
	this->columnAutoRecept = column;

    buttonAutoComeOut_Clicked(); // вызвать форму приёмки/отправки
}
// Клик по заголовку в таблице вызывает метод сортировки колонки
void MyClass::headerShippingAuto_Clicked( int column )
{
	static Qt::SortOrder sortOrder = Qt::DescendingOrder;

	// Выбранные колонки для сортировки
	if( column == 1 || column == 2 || column == 3 || column == 4 || column == 5 || column == 10 )
	{

	    if( sortOrder == Qt::DescendingOrder )
		    sortOrder = Qt::DescendingOrder;
	    else
		    sortOrder = Qt::DescendingOrder;

        tableAutoReception->sortItems( column, sortOrder );
	}
}
// Клик по заголовку в таблице вызывает метод сортировки колонки
void MyClass::headerTableReports_Clicked( int column )
{
    static Qt::SortOrder sortOrder = Qt::DescendingOrder;

	// Выбранные колонки для сортировки
	if( column == 1 || column == 2 || column == 3 || column == 4 || column == 5 || column == 10 || column == 12 )
	{
	    if( sortOrder == Qt::DescendingOrder )
		    sortOrder = Qt::DescendingOrder;
	    else
		    sortOrder = Qt::DescendingOrder;

		reportFormObject->tableReports->sortItems( column, sortOrder );
	}
}
// Отобразить данные по принятым/отправленным автомобилям в записях (отчётах)
void MyClass::show_records()
{
	bool result;
	QSqlError error;
	int type_operation = 0;
	QString params;

	if( reportFormObject->radioButtAutoComeInReports->isChecked() == true && reportFormObject->radioButtAutoComeOutReports->isChecked() == false )
		type_operation = 0;
	else if( reportFormObject->radioButtAutoComeOutReports->isChecked() == true && reportFormObject->radioButtAutoComeInReports->isChecked() == false )
		type_operation = 1;

    reportFormObject->enableDisableControls(); // разрешить/запретить контроллы  фильтрации

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

    // Критерий "По дате (начало)"
	if( reportFormObject->checkBoxDateBegin->isChecked() == false )
	{
		QDate current_date = QDate::currentDate();  //  reportFormObject->beginCalendar->selectedDate();
		QString date = current_date.toString( "yyyy-MM-dd 00:00:00" );
		params.append( QString( "Date_Time_Accept>='%1' AND type_operation=%2" ).arg( date ).arg( type_operation ) ) ;
	}

	result = query.exec( QString( "SELECT * FROM ReportsWeight WHERE %1;" ).arg( params ) );

	QString val;
	int i = 0, j = 0;

	int row_count = reportFormObject->tableReports->rowCount();
	while( row_count > 0 )
	{
		reportFormObject->tableReports->removeRow( row_count - 1 );
		row_count--;
	}

	while( query.next() )
	{
		reportFormObject->tableReports->setRowCount( j + 1 );

		while( i < reportFormObject->tableReports->columnCount() )
		{
		    if( i + 3 == DATETIME_REPORTS_COLUMN1 )
			    val = query.value( i + 3 ).asDateTime().toString( "dd.MM.yyyy hh:mm:ss" ); // ячейка времени/даты приёмки автомобиля на территорию
			else
			if( i + 3 == DATETIME_REPORTS_COLUMN2 )
			    val = query.value( i + 3 ).asDateTime().toString( "dd.MM.yyyy hh:mm:ss" ); // ячейка времени/даты отправки автомобиля с территории
			else if( i + 3 == ENTERTARA_REPORTS_COLUMN )
			{
				int tara_type = query.record().value( "tara_type" ).toInt();
                if( tara_type == 0 )
					val = tr("From scales");
				else if( tara_type == 1 )
					val = tr("Manually");
				else if( tara_type == 2 )
					val = tr("Saved tara");
			}
			else
			    val = query.value( i + 3 ).toString();

			QPixmap pixmap( "image/save_icon_24.png" );

			// Если текущая колонка - это ссылка на фотоснимки  автомобилей - второе взвешивание
			if( i == reportFormObject->tableReports->columnCount() - 3 )
			{
				buttonPhotoShow = new QPushButton( tr("Photos") ); //Фото взвешиваний
				buttonPhotoShow->setFixedHeight( 24 );
				buttonPhotoShow->setFixedWidth( 140 );
				QObject::connect( buttonPhotoShow, SIGNAL( clicked() ), this, SLOT( showPhotoReports() ) );

				QModelIndex index = reportFormObject->tableReports->model()->index( j, i, QModelIndex() );
				reportFormObject->tableReports->setIndexWidget( index, buttonPhotoShow );

				val = query.value( 0 ).toString(); //id
				//i++;
				//continue;
			}

			// Если текущая колонка - это ссылка на кнопку для сохранения фото автомобилей из БД на HDD
			if( i == reportFormObject->tableReports->columnCount() - 2 )
			{
			    buttonPhotoSave = new QPushButton();
				buttonPhotoSave->setFixedHeight( 24 );
				buttonPhotoSave->setFixedWidth( 140 );
				buttonPhotoSave->setPixmap( pixmap );
				QObject::connect( buttonPhotoSave, SIGNAL( clicked() ), this, SLOT( savePhotoReportsTo_HDD() ) );

				QModelIndex index = reportFormObject->tableReports->model()->index( j, i, QModelIndex() );
				reportFormObject->tableReports->setIndexWidget( index, buttonPhotoSave );

				val = query.value( query.record().count() - 2 ).toString();  //idf
				//i++;
				//continue;
			}

			reportFormObject->tableReports->setItem( j, i, new QTableWidgetItem( val ) );
			i++;
        }
        j++;

		i = 0;
    }
	reportFormObject->tableReports->setFocus();
}
//
void MyClass::prepare_showPhotoReports( QModelIndex index )
{
	//this->rowAutoRecept = index.row();
	//this->columnAutoRecept = index.column();
	reportFormObject->row = index.row();
	reportFormObject->column = index.column();
}
// Произвести выборку и  отобразить фотоснимки из отчётов
void MyClass::showPhotoReports()
{
	QMessageBox mess;
	bool result;
	QSqlError error;
	int idf = 0;
	int row, column;

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	QTableView *tableReportCards = reports_rfidObj->getTableReports();

	QString tableNameReception = "AutoReception";
	QString tableNameReport = "ReportsWeight";

	if( CardsMode_action->isChecked() )
	{
		row = tableReportCards->currentIndex().row();
		column = tableReportCards->currentIndex().column();
		tableReportCards->setFocus();
		if( row == -1 && column == -1 )
		{
			// Ошибка!  Не выбрана запись
			mess.setText( tr("Select the entry first!") );
			mess.setStandardButtons( QMessageBox::Cancel );
			mess.setButtonText( QMessageBox::Cancel, "Ok" );
			mess.setIcon( QMessageBox::Warning );
			mess.setWindowTitle( tr( "AV-Control" ) );
			mess.exec();
			return;
		}
		tableNameReception = "AcceptCards";
		tableNameReport = "ReportCards";
		idf = reports_rfidObj->getIdForShowPhoto( *tableReportCards, row, 0 );
	}
	else
	{
		row = reportFormObject->tableReports->currentIndex().row();
		column = reportFormObject->tableReports->currentIndex().column();
		reportFormObject->tableReports->setFocus();
		if( row == -1 && column == -1 )
		{
			// Ошибка!  Не выбрана запись
			mess.setText( tr("Select the entry first!") );
			mess.setStandardButtons( QMessageBox::Cancel );
			mess.setButtonText( QMessageBox::Cancel, "Ok" );
			mess.setIcon( QMessageBox::Warning );
			mess.setWindowTitle( tr( "AV-Control" ) );
			mess.exec();
			return;
		}
		QString str = reportFormObject->tableReports->item( row, 10 )->text();
		QDateTime dateTime = QDateTime::fromString( str, "dd.MM.yyyy hh:mm:ss" );
		str = dateTime.toString( "yyyy-MM-dd hh:mm:ss" );
		//
		query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
		result = query.exec();
		result = query.exec( QString( "SELECT IDF FROM %1 WHERE Date_Time_Accept='%2';" ).arg(tableNameReport).arg( str ) );
		if( query.next() )
		{
			idf = query.value( 0 ).toInt();
		}
	}

	result = query.exec( QString( "SELECT photo_1, photo_2, photo_3, photo_4, photo_5, photo_6, photo_7, photo_8 FROM images WHERE ID=%1;" ).arg( idf ) );
	query.next();

	QByteArray ba = query.record().value( "photo_1" ).toByteArray();
	QPixmap pixmap1;
	pixmap1.loadFromData( ba );

	pixmap1 = pixmap1.scaled( QSize( 1366, 768 ), Qt::KeepAspectRatio, Qt::FastTransformation );
	previewSnapshot_1->setPixmap( pixmap1 );
	previewSnapshot_1->show();
	previewSnapshot_1->raise();
	previewSnapshot_1->setScaledContents(true);
    //
	ba = query.record().value( "photo_2" ).toByteArray();
	QPixmap pixmap2;
	pixmap2.loadFromData( ba );
	pixmap2 = pixmap2.scaled( QSize( 1366, 768 ), Qt::KeepAspectRatio, Qt::FastTransformation );
	previewSnapshot_2->setPixmap( pixmap2 );
	previewSnapshot_2->show();
	previewSnapshot_2->raise();
	previewSnapshot_2->setScaledContents(true);
	//
	ba = query.record().value( "photo_5" ).toByteArray();
	QPixmap pixmap3;
	pixmap3.loadFromData( ba );

	pixmap3 = pixmap3.scaled( QSize( 1366, 768 ), Qt::KeepAspectRatio, Qt::FastTransformation );
	previewSnapshot_3->setPixmap( pixmap3 );
	previewSnapshot_3->show();
	previewSnapshot_3->raise();
	previewSnapshot_3->setScaledContents(true);
    //
	ba = query.record().value( "photo_6" ).toByteArray();
	QPixmap pixmap4;
	pixmap4.loadFromData( ba );
	pixmap4 = pixmap4.scaled( QSize( 1366, 768 ), Qt::KeepAspectRatio, Qt::FastTransformation );
	previewSnapshot_4->setPixmap( pixmap4 );
	previewSnapshot_4->show();
	previewSnapshot_4->raise();
	previewSnapshot_4->setScaledContents(true);

	//

	ba = query.record().value( "photo_3" ).toByteArray();
	QPixmap pixmap5;
	pixmap5.loadFromData( ba );
	pixmap5 = pixmap5.scaled( QSize( 1366, 768 ), Qt::KeepAspectRatio, Qt::FastTransformation );
	previewSnapshot_5->setPixmap( pixmap5 );
	previewSnapshot_5->show();
	previewSnapshot_5->raise();
	previewSnapshot_5->setScaledContents(true);

	//

    ba = query.record().value( "photo_4" ).toByteArray();
	QPixmap pixmap6;
	pixmap6.loadFromData( ba );
	pixmap6 = pixmap6.scaled( QSize( 1366, 768 ), Qt::KeepAspectRatio, Qt::FastTransformation );
	previewSnapshot_6->setPixmap( pixmap6 );
	previewSnapshot_6->show();
	previewSnapshot_6->raise();
	previewSnapshot_6->setScaledContents(true);

	//

    ba = query.record().value( "photo_7" ).toByteArray();
	QPixmap pixmap7;
	pixmap7.loadFromData( ba );
	pixmap7 = pixmap7.scaled( QSize( 1366, 768 ), Qt::KeepAspectRatio, Qt::FastTransformation );
	previewSnapshot_7->setPixmap( pixmap7 );
	previewSnapshot_7->show();
	previewSnapshot_7->raise();
	previewSnapshot_7->setScaledContents(true);

	//

    ba = query.record().value( "photo_8" ).toByteArray();
	QPixmap pixmap8;
	pixmap8.loadFromData( ba );
	pixmap8 = pixmap8.scaled( QSize( 1366, 768 ), Qt::KeepAspectRatio, Qt::FastTransformation );
	previewSnapshot_8->setPixmap( pixmap8 );
	previewSnapshot_8->show();
	previewSnapshot_8->raise();
	previewSnapshot_8->setScaledContents(true);


	//this->rowAutoRecept = -1;
	//this->columnAutoRecept = -1;
	reportFormObject->row = -1;
	reportFormObject->column = -1;
}
// Сохранить фотоснимки из отчётов на HDD
void MyClass::savePhotoReports_Rfid( )
{
    QMessageBox mess;
	bool result;
	QSqlError error;
	int row, column, count_photo = 0;
	QString fileName;
	QMap< int, QMap< QString, QByteArray > > dictionary;
	QMap< QString, QByteArray > map;

	QTableView *tableReportCards = reports_rfidObj->getTableReports();

	row = tableReportCards->currentIndex().row();
	column = tableReportCards->currentIndex().column();
	//bool for_email = (sender()->objectName() == "buttonPhotoSaveAll" || reportFormObject->checkBoxPhotoSaveAll->isChecked() && reportFormObject->tableReports->rowCount() > 1 ) ? true : false;
	bool for_email = false; //(tableReportCards->model()->rowCount() < 1 ) ? true : false;
	tableReportCards->setFocus();

	if( !for_email && row == -1 )
	{
		// Ошибка!  Не выбрана запись
        mess.setText( tr("Select the entry first!") );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "AV-Control" ) );
		mess.exec();

	    return;
	}
	else
	{
		//reportFormObject->tableReports->model()->
	}
	
	QString tableNameReception = "AcceptCards";
	QString tableNameReport = "ReportCards";

	/*if( CardsMode_action->isChecked() )
	{
		row = tableReportCards->currentIndex().row();
		column = tableReportCards->currentIndex().column();
		tableReportCards->setFocus();
		if( !for_email && row == -1 )
		{
			// Ошибка!  Не выбрана запись
			mess.setText( tr("Select the entry first!") );
			mess.setStandardButtons( QMessageBox::Cancel );
			mess.setButtonText( QMessageBox::Cancel, "Ok" );
			mess.setIcon( QMessageBox::Warning );
			mess.setWindowTitle( tr( "AV-Control" ) );
			mess.exec();
			return;
		}
		tableNameReception = "AcceptCards";
		tableNameReport = "ReportCards";
		//idf = reports_rfidObj->getIdForShowPhoto( *tableReportCards, row, 0 );
	}
	else
	{
		row = reportFormObject->tableReports->currentIndex().row();
		column = reportFormObject->tableReports->currentIndex().column();
		reportFormObject->tableReports->setFocus();
		if( !for_email && row == -1 )
		{
			// Ошибка!  Не выбрана запись
			mess.setText( tr("Select the entry first!") );
			mess.setStandardButtons( QMessageBox::Cancel );
			mess.setButtonText( QMessageBox::Cancel, "Ok" );
			mess.setIcon( QMessageBox::Warning );
			mess.setWindowTitle( tr( "AV-Control" ) );
			mess.exec();
			return;
		}
		/*QString str = reportFormObject->tableReports->item( row, 10 )->text();
		QDateTime dateTime = QDateTime::fromString( str, "dd.MM.yyyy hh:mm:ss" );
		str = dateTime.toString( "yyyy-MM-dd hh:mm:ss" );
		//
		query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
		result = query.exec();
		result = query.exec( QString( "SELECT IDF FROM %1 WHERE Date_Time_Accept='%2';" ).arg(tableNameReport).arg( str ) );
		if( query.next() )
		{
			idf = query.value( 0 ).toInt();
		}
	*/

	QFileDialog fileDialog( this );
	fileDialog.setFileMode( QFileDialog::Directory );
	fileDialog.setViewMode( QFileDialog::Detail );

	QSettings settingsLang( "settings.ini", QSettings::IniFormat );
	settingsLang.setIniCodec( "Windows-1251" );

	QString langFromConf;
	settingsLang.beginGroup( "Server" );
	langFromConf = settingsLang.value( "lang", "ua" ).toString();
	settingsLang.endGroup();
	if( langFromConf == "ru" )
	{
		fileDialog.setLocale( QLocale::Russian );
	}
	else if( langFromConf == "ua" )
	{
		fileDialog.setLocale( QLocale::Ukrainian );
	}
	else if( langFromConf == "en" )
	{
		fileDialog.setLocale( QLocale::English );
	}
	if ( ! for_email )
		fileDialog.exec();
	//
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	//int idf = reports_rfidObj->getIdForShowPhoto( *tableReportCards, row, 0 );
	/*if( CardsMode_action->isChecked() )
	{
		row = tableReportCards->currentIndex().row();
		//int rec_count = for_email ? tableReportCards->rowCount() : 1;
     */

	int rec_count = for_email ? tableReportCards->model()->rowCount() : 1;

	for(int i = 0; i < rec_count; i++ )
	{   QString str = tableReportCards->model()->index(tableReportCards->currentIndex().row(), 0).data().toString();
		//QString str = reportFormObject->tableReports->item( for_email ? i : row, 10 )->text();
		QDateTime dateTime = QDateTime::fromString( str, "dd.MM.yyyy hh:mm:ss" );
		str = dateTime.toString( "yyyy-MM-dd hh:mm:ss" );
		//
		query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
		result = query.exec();
		//result = query.exec( QString( "SELECT IDF FROM %1 WHERE Date_Time_Accept='%2';" ).arg(tableNameReport).arg( str ) );
		result = query.exec( QString( "SELECT IDF FROM ReportCards WHERE Date_Time_Accept='%1';" ).arg( str ) );

		int idf = 0;

		if( query.next() )
		{
			idf = query.value( 0 ).toInt();
		}

		result = query.exec( QString( "SELECT photo_1, photo_2, photo_3, photo_4, photo_5, photo_6, photo_7, photo_8 FROM images WHERE ID=%1;" ).arg( idf ) );
		query.next();
		//фото с 1-й и 2-й камер
		//первое взвешивание
		QByteArray ba = query.record().value( "photo_1" ).toByteArray();
		QPixmap pixmap1;
		pixmap1.loadFromData( ba );                                                                     

		if( ba.isEmpty() == false )                                                                          //tableReportCards->model()->index(tableViewReports->currentIndex().row(), 0).data().toString()         reportFormObject->tableReports->item( for_email ? i : row, 0 )->text()          
		{
			fileName = QString( "%1_%2_%3_№%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ) ).arg( tableReportCards->model()->index(tableReportCards->currentIndex().row(), 7).data().toString() ).arg( tableReportCards->model()->index(tableReportCards->currentIndex().row(), 9).data().toString() ).arg( count_photo + 1 );

			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}

		// previewSnapshot_1->setPixmap( pixmap1 );
		// previewSnapshot_1->show();
		// previewSnapshot_1->raise();
		//
		ba = query.record().value( "photo_2" ).toByteArray();
		QPixmap pixmap2;
		pixmap2.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_№%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ) ).arg( tableReportCards->model()->index(tableReportCards->currentIndex().row(), 7).data().toString() ).arg( tableReportCards->model()->index(tableReportCards->currentIndex().row(), 9).data().toString() ).arg( count_photo + 1 );

			QMap< QString, QByteArray > map;
			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}

		// previewSnapshot_2->setPixmap( pixmap2 );
		// previewSnapshot_2->show();
		// previewSnapshot_2->raise();
		//

		//второе взвешивание
		ba = query.record().value( "photo_5" ).toByteArray();
		QPixmap pixmap3;
		pixmap3.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_№%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ) ).arg( tableReportCards->model()->index(tableReportCards->currentIndex().row(), 7).data().toString() ).arg( tableReportCards->model()->index(tableReportCards->currentIndex().row(), 9).data().toString() ).arg( count_photo + 1 );

			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}

		// previewSnapshot_3->setPixmap( pixmap3 );
		// previewSnapshot_3->show();
		// previewSnapshot_3->raise();
		//
		ba = query.record().value( "photo_6" ).toByteArray();
		QPixmap pixmap4;
		pixmap4.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_№%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ) ).arg( tableReportCards->model()->index(tableReportCards->currentIndex().row(), 7).data().toString() ).arg( tableReportCards->model()->index(tableReportCards->currentIndex().row(), 9).data().toString() ).arg( count_photo + 1 );

			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}

		//фото с 3-й и 4-й камер
		//первое взвешивание
		ba = query.record().value( "photo_3" ).toByteArray();
		QPixmap pixmap5;
		pixmap5.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_№%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ) ).arg( tableReportCards->model()->index(tableReportCards->currentIndex().row(), 7).data().toString() ).arg( tableReportCards->model()->index(tableReportCards->currentIndex().row(), 9).data().toString() ).arg( count_photo + 1 );

			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}

		ba = query.record().value( "photo_4" ).toByteArray();
		QPixmap pixmap6;
		pixmap6.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_№%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ) ).arg( tableReportCards->model()->index(tableReportCards->currentIndex().row(), 7).data().toString() ).arg( tableReportCards->model()->index(tableReportCards->currentIndex().row(), 9).data().toString() ).arg( count_photo + 1 );

			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}

		//второе взвешивание
		ba = query.record().value( "photo_7" ).toByteArray();
		QPixmap pixmap7;
		pixmap7.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_№%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ) ).arg( tableReportCards->model()->index(tableReportCards->currentIndex().row(), 7).data().toString() ).arg( tableReportCards->model()->index(tableReportCards->currentIndex().row(), 9).data().toString() ).arg( count_photo + 1 );

			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}

		ba = query.record().value( "photo_8" ).toByteArray();
		QPixmap pixmap8;
		pixmap7.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_№%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ) ).arg( tableReportCards->model()->index(tableReportCards->currentIndex().row(), 7).data().toString() ).arg( tableReportCards->model()->index(tableReportCards->currentIndex().row(), 9).data().toString() ).arg( count_photo + 1 );

			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}
	}

	if( dictionary.size() > 0 )
	{
		QMapIterator< int, QMap< QString, QByteArray > > dictionaryData( dictionary );
		QMap< QString, QByteArray > value;

		while( dictionaryData.hasNext() )
		{
			dictionaryData.next();
			value = dictionaryData.value();

			QMapIterator< QString, QByteArray > fileData( value );
			while( fileData.hasNext() )
			{
				fileData.next();
				QString path;
				QString fileName = fileData.key();
				if (! for_email )
					path = fileDialog.directory().path().replace( '/', "\\", Qt::CaseInsensitive ).append( "\\" ); // ???  for Windows only
				else
					path = "photo_tmp\\";
				saveImageFromDB_ToFile( fileData.value(), QString( "%1" ).arg( fileName.prepend( path ) ) );
			}

			// saveImageFromDB_ToFile( fileData.value(), fileData.key() );

		}
	}
	else
	{
		// Ошибка!  Нет фотографий для сохранения
		mess.setText( tr("Error. There are no photos to save!") );
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "AV-Control" ) );
		if ( ! for_email )
			mess.exec();
	}
	
	//this->rowAutoRecept = -1;
	//this->columnAutoRecept = -1;
	//reportFormObject->row = -1;
	//reportFormObject->column = -1;
	//tableReportCards->model()->row = -1;
    //tableReportCards->column = -1;


}


// Сохранить фотоснимки из отчётов на HDD
void MyClass::savePhotoReportsTo_HDD( )
{
    QMessageBox mess;
	bool result;
	QSqlError error;
	int row, column, count_photo = 0;
	QString fileName;
	QMap< int, QMap< QString, QByteArray > > dictionary;
	QMap< QString, QByteArray > map;

	row = reportFormObject->tableReports->currentIndex().row();
	column = reportFormObject->tableReports->currentIndex().column();
	bool for_email = (sender()->objectName() == "buttonPhotoSaveAll" || reportFormObject->checkBoxPhotoSaveAll->isChecked() && reportFormObject->tableReports->rowCount() > 1 ) ? true : false;

	reportFormObject->tableReports->setFocus();

	if( !for_email && row == -1 )
	{
		// Ошибка!  Не выбрана запись
        mess.setText( tr("Select the entry first!") );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

	    return;
	}
	else
	{
		//reportFormObject->tableReports->model()->
	}

	QFileDialog fileDialog( this );
	fileDialog.setFileMode( QFileDialog::Directory );
	fileDialog.setViewMode( QFileDialog::Detail );

	QSettings settingsLang( "settings.ini", QSettings::IniFormat );
	settingsLang.setIniCodec( "Windows-1251" );

	QString langFromConf;
	settingsLang.beginGroup( "Server" );
	langFromConf = settingsLang.value( "lang", "ua" ).toString();
	settingsLang.endGroup();
	if( langFromConf == "ru" )
	{
		fileDialog.setLocale( QLocale::Russian );
	}
	else if( langFromConf == "ua" )
	{
		fileDialog.setLocale( QLocale::Ukrainian );
	}
	else if( langFromConf == "en" )
	{
		fileDialog.setLocale( QLocale::English );
	}
	if ( ! for_email )
		fileDialog.exec();
	//
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	int rec_count = for_email ? reportFormObject->tableReports->rowCount() : 1;
	//row = for_email ? 0 : row;

	for(int i = 0; i < rec_count; i++ )
	{
		QString str = reportFormObject->tableReports->item( for_email ? i : row, 10 )->text();
		QDateTime dateTime = QDateTime::fromString( str, "dd.MM.yyyy hh:mm:ss" );
		str = dateTime.toString( "yyyy-MM-dd hh:mm:ss" );
		//
		query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
		result = query.exec();

		result = query.exec( QString( "SELECT IDF FROM ReportsWeight WHERE Date_Time_Accept='%1';" ).arg( str ) );

		int idf = 0;

		if( query.next() )
		{
			idf = query.value( 0 ).toInt();
		}

		result = query.exec( QString( "SELECT photo_1, photo_2, photo_3, photo_4, photo_5, photo_6, photo_7, photo_8 FROM images WHERE ID=%1;" ).arg( idf ) );
		query.next();
		//фото с 1-й и 2-й камер
		//первое взвешивание
		QByteArray ba = query.record().value( "photo_1" ).toByteArray();
		QPixmap pixmap1;
		pixmap1.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_№%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ) ).arg( reportFormObject->tableReports->item( for_email ? i : row, 14 )->text() ).arg( reportFormObject->tableReports->item( for_email ? i : row, 0 )->text() ).arg( count_photo + 1 );

			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}

		// previewSnapshot_1->setPixmap( pixmap1 );
		// previewSnapshot_1->show();
		// previewSnapshot_1->raise();
		//
		ba = query.record().value( "photo_2" ).toByteArray();
		QPixmap pixmap2;
		pixmap2.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_№%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ) ).arg( reportFormObject->tableReports->item( for_email ? i : row, 14 )->text() ).arg( reportFormObject->tableReports->item( for_email ? i : row, 0 )->text() ).arg( count_photo + 1 );

			QMap< QString, QByteArray > map;
			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}

		// previewSnapshot_2->setPixmap( pixmap2 );
		// previewSnapshot_2->show();
		// previewSnapshot_2->raise();
		//

		//второе взвешивание
		ba = query.record().value( "photo_5" ).toByteArray();
		QPixmap pixmap3;
		pixmap3.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_№%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ) ).arg( reportFormObject->tableReports->item( for_email ? i : row, 14 )->text() ).arg( reportFormObject->tableReports->item( for_email ? i : row, 0 )->text() ).arg( count_photo + 1 );

			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}

		// previewSnapshot_3->setPixmap( pixmap3 );
		// previewSnapshot_3->show();
		// previewSnapshot_3->raise();
		//
		ba = query.record().value( "photo_6" ).toByteArray();
		QPixmap pixmap4;
		pixmap4.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_№%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ) ).arg( reportFormObject->tableReports->item( for_email ? i : row, 14 )->text() ).arg( reportFormObject->tableReports->item( for_email ? i : row, 0 )->text() ).arg( count_photo + 1 );

			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}

		//фото с 3-й и 4-й камер
		//первое взвешивание
		ba = query.record().value( "photo_3" ).toByteArray();
		QPixmap pixmap5;
		pixmap5.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_№%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ) ).arg( reportFormObject->tableReports->item( for_email ? i : row, 14 )->text() ).arg( reportFormObject->tableReports->item( for_email ? i : row, 0 )->text() ).arg( count_photo + 1 );

			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}

		ba = query.record().value( "photo_4" ).toByteArray();
		QPixmap pixmap6;
		pixmap6.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_№%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ) ).arg( reportFormObject->tableReports->item( for_email ? i : row, 14 )->text() ).arg( reportFormObject->tableReports->item( for_email ? i : row, 0 )->text() ).arg( count_photo + 1 );

			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}

		//второе взвешивание
		ba = query.record().value( "photo_7" ).toByteArray();
		QPixmap pixmap7;
		pixmap7.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_№%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ) ).arg( reportFormObject->tableReports->item( for_email ? i : row, 14 )->text() ).arg( reportFormObject->tableReports->item( for_email ? i : row, 0 )->text() ).arg( count_photo + 1 );

			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}

		ba = query.record().value( "photo_8" ).toByteArray();
		QPixmap pixmap8;
		pixmap7.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_№%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ) ).arg( reportFormObject->tableReports->item( for_email ? i : row, 14 )->text() ).arg( reportFormObject->tableReports->item( for_email ? i : row, 0 )->text() ).arg( count_photo + 1 );

			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}
	}

	if( dictionary.size() > 0 )
	{
		QMapIterator< int, QMap< QString, QByteArray > > dictionaryData( dictionary );
		QMap< QString, QByteArray > value;

		while( dictionaryData.hasNext() )
		{
			dictionaryData.next();
			value = dictionaryData.value();

			QMapIterator< QString, QByteArray > fileData( value );
			while( fileData.hasNext() )
			{
				fileData.next();
				QString path;
				QString fileName = fileData.key();
				if (! for_email )
					path = fileDialog.directory().path().replace( '/', "\\", Qt::CaseInsensitive ).append( "\\" ); // ???  for Windows only
				else
					path = "photo_tmp\\";
				qDebug()<<path;
				saveImageFromDB_ToFile( fileData.value(), QString( "%1" ).arg( fileName.prepend( path ) ) );
			}

			// saveImageFromDB_ToFile( fileData.value(), fileData.key() );

		}
	}
	else
	{
		// Ошибка!  Нет фотографий для сохранения
		mess.setText( tr("Error. There are no photos to save!") );
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		if ( ! for_email )
			mess.exec();
	}

	//this->rowAutoRecept = -1;
	//this->columnAutoRecept = -1;
	reportFormObject->row = -1;
	reportFormObject->column = -1;
}


// Сохранить фотоснимки из отчётов на HDD
void MyClass::savePhotoForBot(QString date_time, QString numAuto, float Brutto, float Netto, float Tara )
{
    qDebug()<<date_time;
    qDebug()<<numAuto;
	numAuto.replace( '-', "_" ).replace(' ', "_");
    qDebug()<<numAuto;
	QMessageBox mess;
	bool result;
	QSqlError error;
	int row, column, count_photo = 0;
	QString fileName;
	QString pathPhoto2; //путь к первой фотке 2-го взвешивания и ее имя
	QString pathPhoto4;
	QString pathPhoto6;
	QString pathPhoto8;
	QString userId = "623218679";
	QString pathBot;
	
	QString dataForBot = QString("Auto = %1\nBrutto = %2\nTara = %3\nNetto = %4" ).arg(numAuto).arg(Brutto).arg(Tara).arg(Netto);
    QByteArray ba = dataForBot.toLatin1();
	char *textBot = ba.data();

	qDebug()<<dataForBot;
	qDebug()<<textBot;

	int resultSendNewWght = -1; // Результат отправки данных пользователю в бот
	int resultSetUserId = -1; // Результат установки id пользователя для бота

	QMap< int, QMap< QString, QByteArray > > dictionary;
	QMap< QString, QByteArray > map;

	row = reportFormObject->tableReports->currentIndex().row();
	column = reportFormObject->tableReports->currentIndex().column();
	bool for_email = (sender()->objectName() == "buttonPhotoSaveAll" || reportFormObject->checkBoxPhotoSaveAll->isChecked() && reportFormObject->tableReports->rowCount() > 1 ) ? true : false;

	reportFormObject->tableReports->setFocus();

	QLibrary lib( "TelSend" );
    if( !lib.load() ) {
        qDebug() << "Loading failed!";
           QMessageBox::warning(this, tr("Error"),
                                     tr("Not found TelSend.dll"),
                                     QMessageBox::Ok);
    }

	//
	typedef int ( *MyType2 )(char* uid, char* stext);
	MyType2 SendTelTextP = ( MyType2 ) lib.resolve( "SendTelTextP" );
	if(SendTelTextP){
        qDebug()<< "Hello SendTelTextP!";   
	}

	typedef int ( *MyType3 )(char* uid, char* fname);
	MyType3 SendTelPhotoP = ( MyType3 ) lib.resolve( "SendTelPhotoP" );
	if(SendTelPhotoP){
        qDebug()<< "Hello SendTelPhotoP!";   
	}

	QFileDialog fileDialog( this );
	fileDialog.setFileMode( QFileDialog::Directory );
	fileDialog.setViewMode( QFileDialog::Detail );

	QSettings settingsLang( "settings.ini", QSettings::IniFormat );
	settingsLang.setIniCodec( "Windows-1251" );

	QString langFromConf;
	settingsLang.beginGroup( "Server" );
	langFromConf = settingsLang.value( "lang", "ua" ).toString();
	settingsLang.endGroup();
	if( langFromConf == "ru" )
	{
		fileDialog.setLocale( QLocale::Russian );
	}
	else if( langFromConf == "ua" )
	{
		fileDialog.setLocale( QLocale::Ukrainian );
	}
	else if( langFromConf == "en" )
	{
		fileDialog.setLocale( QLocale::English );
	}
	//if ( ! for_email )
	//	fileDialog.exec();
	//
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );
	QSqlQuery queryDel( *mysqlProccessObject->getQSqlInstance() );

	int rec_count = for_email ? reportFormObject->tableReports->rowCount() : 1;
	//row = for_email ? 0 : row;
//================
if( optionsObj->isCheked[optionsObj->modules.indexOf("camera")] )
  {
	for(int i = 0; i < rec_count; i++ )  //Вытаскиваем фото из БД
	{/*
		QString str = reportFormObject->tableReports->item( for_email ? i : row, 10 )->text();
		QDateTime dateTime = QDateTime::fromString( str, "dd.MM.yyyy hh:mm:ss" );
		str = dateTime.toString( "yyyy-MM-dd hh:mm:ss" );
		*/
		QString str = date_time;
		//
		query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
		result = query.exec();

		result = query.exec( QString( "SELECT IDF FROM ReportsWeight WHERE Date_Time_Accept='%1';" ).arg( str ) );

		int idf = 0;

		if( query.next() )
		{
			idf = query.value( 0 ).toInt();
		}

		result = query.exec( QString( "SELECT photo_1, photo_2, photo_3, photo_4, photo_5, photo_6, photo_7, photo_8 FROM images WHERE ID=%1;" ).arg( idf ) );
		query.next();
		//фото с 1-й и 2-й камер
		//первое взвешивание
		QByteArray ba = query.record().value( "photo_1" ).toByteArray();
		QPixmap pixmap1;
		pixmap1.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ).replace(' ', "_") ).arg( /*reportFormObject->tableReports->item( for_email ? i : row, 14 )->text()*/ "" ).arg( /*reportFormObject->tableReports->item( for_email ? i : row, 0 )->text()*/ numAuto ).arg( count_photo + 1 );
			
			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}

		// previewSnapshot_1->setPixmap( pixmap1 );
		// previewSnapshot_1->show();
		// previewSnapshot_1->raise();
		//
		ba = query.record().value( "photo_2" ).toByteArray();
		QPixmap pixmap2;
		pixmap2.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ).replace(' ', "_") ).arg( /*reportFormObject->tableReports->item( for_email ? i : row, 14 )->text()*/ "" ).arg( /*reportFormObject->tableReports->item( for_email ? i : row, 0 )->text()*/ numAuto ).arg( count_photo + 1 );
			pathPhoto2 = fileName;
			qDebug()<<pathPhoto2;

			QMap< QString, QByteArray > map;
			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}

		// previewSnapshot_2->setPixmap( pixmap2 );
		// previewSnapshot_2->show();
		// previewSnapshot_2->raise();
		//

		//второе взвешивание
		ba = query.record().value( "photo_5" ).toByteArray();
		QPixmap pixmap3;
		pixmap3.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ).replace(' ', "_") ).arg( /*reportFormObject->tableReports->item( for_email ? i : row, 14 )->text()*/ "" ).arg( /*reportFormObject->tableReports->item( for_email ? i : row, 0 )->text()*/ numAuto).arg( count_photo + 1 );

			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}

		// previewSnapshot_3->setPixmap( pixmap3 );
		// previewSnapshot_3->show();
		// previewSnapshot_3->raise();
		//
		ba = query.record().value( "photo_6" ).toByteArray();
		QPixmap pixmap4;
		pixmap4.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ).replace( ' ', "_" ) ).arg( /*reportFormObject->tableReports->item( for_email ? i : row, 14 )->text()*/ "").arg( /*reportFormObject->tableReports->item( for_email ? i : row, 0 )->text()*/ numAuto ).arg( count_photo + 1 );
			pathPhoto6 = fileName;
			qDebug()<<pathPhoto6;

			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}

		//фото с 3-й и 4-й камер
		//первое взвешивание
		ba = query.record().value( "photo_3" ).toByteArray();
		QPixmap pixmap5;
		pixmap5.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ).replace(' ', "_") ).arg(/*reportFormObject->tableReports->item( for_email ? i : row, 14 )->text()*/ "").arg( /*reportFormObject->tableReports->item( for_email ? i : row, 0 )->text()*/ numAuto ).arg( count_photo + 1 );

			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}

		ba = query.record().value( "photo_4" ).toByteArray();
		QPixmap pixmap6;
		pixmap6.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ).replace( ' ', "_" ) ).arg( /*reportFormObject->tableReports->item( for_email ? i : row, 14 )->text()*/ "" ).arg( /*reportFormObject->tableReports->item( for_email ? i : row, 0 )->text()*/ numAuto ).arg( count_photo + 1 );
			pathPhoto4 = fileName;
			qDebug()<<pathPhoto4;

			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}

		//второе взвешивание
		ba = query.record().value( "photo_7" ).toByteArray();
		QPixmap pixmap7;
		pixmap7.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ).replace(' ', "_") ).arg( /*reportFormObject->tableReports->item( for_email ? i : row, 14 )->text()*/ "" ).arg( /*reportFormObject->tableReports->item( for_email ? i : row, 0 )->text()*/ numAuto ).arg( count_photo + 1 );
			pathPhoto8 = fileName;
			qDebug()<<pathPhoto8;

			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}

		ba = query.record().value( "photo_8" ).toByteArray();
		QPixmap pixmap8;
		pixmap7.loadFromData( ba );

		if( ba.isEmpty() == false )
		{
			fileName = QString( "%1_%2_%3_%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ).replace(' ', "_") ).arg( /*reportFormObject->tableReports->item( for_email ? i : row, 14 )->text()*/ "" ).arg( /*reportFormObject->tableReports->item( for_email ? i : row, 0 )->text()*/ numAuto ).arg( count_photo + 1 );

			map.insert( fileName, ba );
			dictionary.insert( count_photo, map );
			count_photo++;
		}
	}

	if( dictionary.size() > 0 )
	{
		QMapIterator< int, QMap< QString, QByteArray > > dictionaryData( dictionary );
		QMap< QString, QByteArray > value;

		while( dictionaryData.hasNext() )
		{
			dictionaryData.next();
			value = dictionaryData.value();

			QMapIterator< QString, QByteArray > fileData( value );
			while( fileData.hasNext() )
			{
				fileData.next();
				QString path;
				QString fileName = fileData.key();
				if (! for_email ){
					path = fileDialog.directory().path().replace( '/', "\\", Qt::CaseInsensitive ).append( "\\photo_bot\\" ); // ???  for Windows only
				}
				else
					path = "photo_tmp\\";
			
				//добавим папку для сохранения фоток
				//path.append("photo");

				QString sss;
				sss.append( "\\" ).append( "\\" );
				pathBot = fileDialog.directory().path().replace( '/', sss, Qt::CaseInsensitive ).append( "\\" ).append( "\\" ).append("photo_bot\\\\");

				// функция сохранения фото
				saveImageFromDB_ToFile( fileData.value(), QString( "%1" ).arg( fileName.prepend( path ) ) );
			}
		}
	}
	else
	{
		// Ошибка!  Нет фотографий для сохранения
		mess.setText( tr("Error. There are no photos to save!") );
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		if ( ! for_email )
			mess.exec();
	}
}
		//ОТПРАВКА НА БОТА
			 userId = "623218679";  //655901241
			 //Считать айди получателя для бота
			   QSettings settings_bot( "settings.ini", QSettings::IniFormat );
               settings_bot.setIniCodec( "Windows-1251" );
//========Вставляем проверку наличия инета=========
			  if(isInetConnect() == false) //Если инета нет, записываем в БД
			  {
			  		QString dt = QDateTime::currentDateTime().toString( "yyyy-MM-dd hh:mm:ss" );
	                QString data = QString("%1\n%2" ).arg(dataForBot).arg(dt);
	                query.prepare( QString( "INSERT INTO dataBot ( data_bot, date_time_bot ) VALUES( :data_bot, :date_time_bot );" ) );
	                query.bindValue( ":data_bot", data );
	                query.bindValue( ":date_time_bot", dt );
	                query.exec();
				//break;
			  }
			  else
			  {
			   result = query.exec(QString( "SELECT * FROM dataBot;" )); //Вычитываем данные, не отправленные вовремя
			   //отправляем данные, которые вычитали 
			   while( query.next() )
	           {
				   QString dataForBotOld = query.record().value( "data_bot" ).toString();
				   qDebug()<<dataForBotOld;

				   QString dateTimeString = ( QString( query.record().value( "date_time_bot" ).toDateTime().toString( "yyyy-MM-dd hh:mm:ss" ) ) );
	               qDebug()<<dateTimeString;
	              
                   QByteArray oldData = dataForBotOld.toLatin1();
	               char *oldTextBot = oldData.data();
				   int resultat = 1; //результат отправки на бота
				   for(int j = 1; j <= 2; j++)
	                {
				     QString id_bot = settings_bot.value(QString("Bot/user%1").arg(j)).toString();
				     qDebug()<<id_bot;
				     if(id_bot.isEmpty())
					   {
				        continue;
				       }
			         QByteArray botid = id_bot.toLatin1();

				     char *usId = botid.data();
				     qDebug()<<usId;
			         resultat = SendTelTextP(usId,oldTextBot);
			        }
			   //тут будем удалять данные
               if (resultat == 0)
			      {
			        bool resDel = queryDel.exec(QString("DELETE FROM databot WHERE date_time_bot = '%1';").arg(dateTimeString));
			      }
			   }
			//  }
//=================================================

	 for(int j = 1; j <= 2; j++)
	    {
				QString id_bot = settings_bot.value(QString("Bot/user%1").arg(j)).toString();
				qDebug()<<id_bot;
				if(id_bot.isEmpty()){
				continue;
				}
			    QByteArray botid = id_bot.toLatin1();

				char *usId = botid.data();
				qDebug()<<usId;

			    int resultat = SendTelTextP(usId,textBot);

				if (resultat != 0)  //Если ошибка при отправке, то сохранить пока что данные, для отправки позже
				{
					QString dt = QDateTime::currentDateTime().toString( "yyyy-MM-dd hh:mm:ss" );
	                QString data = QString("%1\n%2" ).arg(dataForBot).arg(dt);
	                query.prepare( QString( "INSERT INTO dataBot ( data_bot, date_time_bot ) VALUES( :data_bot, :date_time_bot );" ) );
	                query.bindValue( ":data_bot", data );
	                query.bindValue( ":date_time_bot", dt );
	                query.exec();
					break;
				}
				
        if( optionsObj->isCheked[optionsObj->modules.indexOf("camera")] )
          {
	         //Если фото есть, то отправляем их на телеграмм бот
			 if( dictionary.size() > 0 )
	           {
				QString pPhoto2 = (pathBot + pathPhoto2);
				qDebug()<<pathPhoto2;
				QString pPhoto4 = (pathBot + pathPhoto4);
				qDebug()<<pathPhoto4;
                QString pPhoto6 = (pathBot + pathPhoto6);
				qDebug()<<pathPhoto6;
                QString pPhoto8 = (pathBot + pathPhoto8);
				qDebug()<<pathPhoto8;

				qDebug()<<pathBot;
            
				QString presPathFoto;  //Строка содержащая путь к фото. Если путь отсутствует, то фото не отправляется

                QByteArray bf = pPhoto2.toLatin1();
			    char *pathFoto2 = bf.data();
			    qDebug()<<pathFoto2;
				if(!pathPhoto2.isEmpty()){
			    int resFoto2 = SendTelPhotoP(usId,pathFoto2);
				}

			    bf = pPhoto4.toLatin1();
			    char *pathFoto4 = bf.data();
			    qDebug()<<pathFoto4;
				if(!pathPhoto4.isEmpty()){
			    int resFoto4 = SendTelPhotoP(usId,pathFoto4);
				}

			    bf = pPhoto6.toLatin1();
			    char *pathFoto6 = bf.data();
			    qDebug()<<pathFoto6;
				if(!pathPhoto6.isEmpty()){
			    int resFoto6 = SendTelPhotoP(usId,pathFoto6);
				}

			    bf = pPhoto8.toLatin1();
			    char *pathFoto8 = bf.data();
			    qDebug()<<pathFoto8;
				if(!pathPhoto8.isEmpty()){
			    int resFoto8 = SendTelPhotoP(usId,pathFoto8);
				}
			 }
          }
		}
	 }
	reportFormObject->row = -1;
	reportFormObject->column = -1;
}

//Проверка наличия интернета при отправке данных на бота
bool MyClass::isInetConnect()
{
bool inetConnect;
QNetworkAccessManager nam;
QNetworkRequest req(QUrl("http://www.google.com"));
QNetworkReply *reply = nam.get(req);
QEventLoop loop;
connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
loop.exec();
if(reply->bytesAvailable())
{
	inetConnect = true;
	qDebug()<<("Inet Yes");
}
else
{
	inetConnect = false;
	qDebug()<<("Inet Not");
}
return inetConnect;
}
///////////////
void MyClass::sendLastRecToEmail( )
{
   int idf;
   QString fileName;
   QList<int> idf_list;
   QStringList fileNames;
   mysqlProccessObject->getLastRecForSend( idf, fileName );
   if( idf )
   {
	   idf_list << idf;
	   fileNames  << fileName;
	   mysqlProccessObject->getPhotosbyIdAndSave( idf_list, fileNames, true );
   }
}
void MyClass::changeDirectory()
{
}
// Созраняем (преобразовываем байты данных) фото из БД  в  файл
void MyClass::saveImageFromDB_ToFile( QByteArray ba, QString fileName )
{
	try
	{
		QImage img( ba );
	    img.save( fileName );
	}
	catch( ... )
	{
	}
}
// Отметить строку  для подготовки просмотра фотоснимков из журнала записей
void MyClass::selectTableReports_Record( int row, int column )
{
	reportFormObject->row = row;
	reportFormObject->column = column;
}
// Начать процесс выборки данных по фильтру в журнале отчётов
void MyClass::reportsAcceptFiltering()
{
	reportFormObject->opacityEffect->setOpacity( 0.1 );
	reportFormObject->tableReports->setGraphicsEffect( reportFormObject->opacityEffect );

	reportFormObject->timerStartFilteringReports->start();
}
// Фильтрация отчётов
void MyClass::timerStartFilteringReports_Slots() // применить фильтр к отчётам
{
	int num_records, percent;

	reportFormObject->tableReports->setUpdatesEnabled( false );
	reportFormObject->tableReports->hide();

	QString params = "";
	bool result;
	QSqlError error;
	int type_operation = 0, r = 0;

    reportFormObject->reportsShowValidation->hide();

	if( reportFormObject->radioButtAutoComeInReports->isChecked() == true && reportFormObject->radioButtAutoComeOutReports->isChecked() == false )
		type_operation = 0;
	else if( reportFormObject->radioButtAutoComeOutReports->isChecked() == true && reportFormObject->radioButtAutoComeInReports->isChecked() == false )
		type_operation = 1;

	// Критерий "По типу тперации"
	params.append( QString( "type_operation=%1 AND num_scales=%2" ).arg( type_operation ).arg( reportFormObject->comboBoxNumScales->currentIndex() ) );
    r++;

	// Критерий "По номеру автомобиля"
	if( reportFormObject->checkBoxNumAuto->isChecked() == true )
	{
		if( r != 0 )
		{
			params.append( " AND " );
		}

		params.append( QString( "Number_Auto='%1'" ).arg( reportFormObject->enterNumAuto->currentText() ) );
		r++;
	}
    // Критерий "По дате (начало)"
	if( reportFormObject->checkBoxDateBegin->isChecked() == true && reportFormObject->checkBoxTimeBegin->isChecked() == false )
	{
		if( r != 0 )
		{
			params.append( " AND " );
		}

		QDate current_date = reportFormObject->beginCalendar->selectedDate();

		QString date = current_date.toString( "yyyy-MM-dd 00:00:00" );

		params.append( QString( "Date_Time_Accept>='%1'" ).arg( date ) ) ;

	    r++;
	}
    // Критерий "По дате (конец)"
	if( reportFormObject->checkBoxDateEnd->isChecked() == true && reportFormObject->checkBoxTimeEnd->isChecked() == false )
	{
		if( r != 0 )
		{
			params.append( " AND " );
		}

		QDate current_date = reportFormObject->endCalendar->selectedDate();

		QString date = current_date.toString( "yyyy-MM-dd 23:59:59" );

		params.append( QString( "Date_Time_Accept<='%1'" ).arg( date ) ) ;

	    r++;
	}
	// Критерий "По времени (начало)"
	if( reportFormObject->checkBoxTimeBegin->isChecked() == true )
	{
		if( r != 0 )
		{
			params.append( " AND " );
		}

		QDate current_date = reportFormObject->beginCalendar->selectedDate();
		QTime current_time = reportFormObject->beginTime->time();

		QString date = current_date.toString( "yyyy-MM-dd %1" ).arg( current_time.toString() );

		params.append( QString( "Date_Time_Accept>='%1'" ).arg( date ) ) ;

	    r++;
	}
	// Критерий "По времени (конец)"
	if( reportFormObject->checkBoxTimeEnd->isChecked() == true )
	{
		if( r != 0 )
		{
			params.append( " AND " );
		}

		QDate current_date = reportFormObject->endCalendar->selectedDate();
		QTime current_time = reportFormObject->endTime->time();

		QString date = current_date.toString( "yyyy-MM-dd %1" ).arg( current_time.toString() );

		params.append( QString( "Date_Time_Accept<='%1'" ).arg( date ) ) ;

	    r++;
	}
	// Критерий "По отправителю"
	if( reportFormObject->checkBoxSender->isChecked() == true )
	{
		if( r != 0 )
		{
			params.append( " AND " );
		}

		params.append( QString( "Sender='%1'" ).arg( reportFormObject->selectSender->currentText() ) ) ;

	    r++;
	}
	// Критерий "По получателю"
	if( reportFormObject->checkBoxAccepter->isChecked() == true )
	{
		if( r != 0 )
		{
			params.append( " AND " );
		}

		params.append( QString( "Accepter='%1'" ).arg( reportFormObject->selectAccepter->currentText() ) ) ;

	    r++;
	}
	// Критерий "По перевозщику"
	if( reportFormObject->checkBoxCarrier->isChecked() == true )
	{
		if( r != 0 )
		{
			params.append( " AND " );
		}

		params.append( QString( "Transporter='%1'" ).arg( reportFormObject->selectCarrier->currentText() ) ) ;

	    r++;
	}
    // Критерий "По типу груза"
	if( reportFormObject->checkBoxTypeOfGoods->isChecked() == true )
	{
		if( r != 0 )
		{
			params.append( " AND " );
		}

		params.append( QString( "Goods='%1'" ).arg( reportFormObject->selectTypeOfGoods->currentText() ) ) ;

	    r++;
	}

	// Критерий "По плательщику"
	if( reportFormObject->checkBoxPayer->isChecked() == true )
	{
		if( r != 0 )
		{
			params.append( " AND " );
		}

		params.append( QString( "Payer='%1'" ).arg( reportFormObject->selectPayer->currentText() ) ) ;

	    r++;
	}

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();
    //
	if( r == 1 ) // не заданы дополнительные критерии фильтрации, кроме выбранного типа операции  - приёмка/отправка автомобиля
	{
	    params = "";

		QDate current_date = QDate::currentDate();  // reportFormObject->beginCalendar->selectedDate();
	    QString date = current_date.toString( "yyyy-MM-dd 00:00:00" );
	    params.append( QString( "SELECT * FROM ReportsWeight WHERE Date_Time_Accept>='%1' AND type_operation=%2;" ).arg( date ).arg( type_operation ) );
	    result = query.exec( params );
	}
	else
	{
		result = query.exec( QString( "SELECT * FROM ReportsWeight WHERE %1;" ).arg( params ) );
	}

	int cnt = query.record().count();

	// Если не найдено записей, то отобразить сообщение об этом
	if( query.size() == 0 )
	{
		reportFormObject->reportsShowValidation->setText( tr("Data according to the filters was not found") );
		reportFormObject->reportsShowValidation->setStyleSheet( "color: Orange; font-size: 18px; wont-weight: bold;" );
		reportFormObject->reportsShowValidation->show();
	}

	reportFormObject->tableReports->setUpdatesEnabled( false );
	reportFormObject->loadIndicatorReports->show();
	reportFormObject->loadIndicatorReports->setEnabled( true );
    reportFormObject->loadProgressBarReports->show();

	num_records = query.size();

	QString val;
	int i = 0, j = 0;

	int row_count = reportFormObject->tableReports->rowCount();
	while( row_count > 0 )
	{
		reportFormObject->tableReports->removeRow( row_count - 1 );
		row_count--;
	}

	while( query.next() )
	{
		reportFormObject->tableReports->setRowCount( j + 1 );

		percent = ( int )( ( float )( ( float )j / ( float )num_records ) * 100.0 );

		while( i < reportFormObject->tableReports->columnCount() )
		{
            reportFormObject->loadIndicatorReports->setText( QString( "%1%" ).arg( percent ) );
			reportFormObject->loadIndicatorReports->repaint();

			reportFormObject->loadProgressBarReports->setValue( percent );
			reportFormObject->loadProgressBarReports->repaint();

		    if( i + 3 == DATETIME_REPORTS_COLUMN1 )
			    val = query.value( i + 3 ).asDateTime().toString( "dd.MM.yyyy hh:mm:ss" ); // ячейка времени/даты приёмки автомобиля на территорию
			else
			if( i + 3 == DATETIME_REPORTS_COLUMN2 )
			    val = query.value( i + 3 ).asDateTime().toString( "dd.MM.yyyy hh:mm:ss" ); // ячейка времени/даты отправки автомобиля с территории
			else if( i + 3 == ENTERTARA_REPORTS_COLUMN )
			{
				int tara_type = query.record().value( "tara_type" ).toInt();
                if( tara_type == 0 )
					val = tr("From scales");
				else if( tara_type == 1 )
					val = tr("Manually");
				else if( tara_type == 2 )
					val = tr("Saved tara");
			}
			else
			    val = query.value( i + 3 ).toString();

			QPixmap pixmap( "image/save_icon_24.png" );

			// Если текущая колонка - это ссылка на фотоснимки  автомобилей - второе взвешивание
			if( i == reportFormObject->tableReports->columnCount() - 3 )
			{
				buttonPhotoShow = new QPushButton( tr("Photos") );
				buttonPhotoShow->setFixedHeight( 24 );
				buttonPhotoShow->setFixedWidth( 140 );
				QObject::connect( buttonPhotoShow, SIGNAL( clicked() ), this, SLOT( showPhotoReports() ) );

				QModelIndex index = reportFormObject->tableReports->model()->index( j, i, QModelIndex() );
				reportFormObject->tableReports->setIndexWidget( index, buttonPhotoShow );

				val = query.value( 0 ).toString(); //id
				//i++;
				//continue;
			}

			// Если текущая колонка - это ссылка на кнопку для сохранения фото автомобилей из БД на HDD
			if( i == reportFormObject->tableReports->columnCount() - 2 )
			{
			    buttonPhotoSave = new QPushButton();
				buttonPhotoSave->setFixedHeight( 24 );
				buttonPhotoSave->setFixedWidth( 124 );
				buttonPhotoSave->setPixmap( pixmap );
				QObject::connect( buttonPhotoSave, SIGNAL( clicked() ), this, SLOT( savePhotoReportsTo_HDD() ) );

				QModelIndex index = reportFormObject->tableReports->model()->index( j, i, QModelIndex() );
				reportFormObject->tableReports->setIndexWidget( index, buttonPhotoSave );

				 val = query.value( query.record().count() - 2 ).toString();  //idf

				//i++;
				//continue;
			}

			reportFormObject->tableReports->setItem( j, i, new QTableWidgetItem( val ) );
			i++;
        }
        j++;

		i = 0;
    }

	// Отменить скрытие таблицы
	reportFormObject->tableReports->setUpdatesEnabled( true );
    reportFormObject->loadIndicatorReports->hide();
	reportFormObject->loadProgressBarReports->hide();
	reportFormObject->tableReports->setEnabled( true );
	reportFormObject->tableReports->show();

	reportFormObject->opacityEffect->setOpacity( 1.0 );
	reportFormObject->tableReports->setGraphicsEffect( reportFormObject->opacityEffect );
}
// Сбросить фильтры формирования отчётов
void MyClass::reportsResetFiltering()
{
	bool result;
	QSqlError error;
	int type_operation;
	QString params;

    reportFormObject->reportsShowValidation->hide();
	reportFormObject->enableDisableControls(); // сбросить  состояние  контроллов

	if( reportFormObject->radioButtAutoComeInReports->isChecked() == true && reportFormObject->radioButtAutoComeOutReports->isChecked() == false )
		type_operation = 0;
	else if( reportFormObject->radioButtAutoComeOutReports->isChecked() == true && reportFormObject->radioButtAutoComeInReports->isChecked() == false )
		type_operation = 1;

	reportFormObject->tableReports->setUpdatesEnabled( false );
	reportFormObject->tableReports->hide();

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();
	if( result == false )
    {
	    // Предупреждение об ошибке работы с БД
	}
	//
	QDate current_date = QDate::currentDate();  // reportFormObject->beginCalendar->selectedDate();
	QString date = current_date.toString( "yyyy-MM-dd 00:00:00" );
	params.append( QString( "SELECT * FROM ReportsWeight WHERE Date_Time_Accept>='%1' AND type_operation=%2;" ).arg( date ).arg( type_operation ) );
	result = query.exec( params );

	QString val;
	int i = 0, j = 0;

	int row_count = reportFormObject->tableReports->rowCount();
	while( row_count > 0 )
	{
		reportFormObject->tableReports->removeRow( row_count - 1 );
		row_count--;
	}

	while( query.next() )
	{
		reportFormObject->tableReports->setRowCount( j + 1 );

		while( i < reportFormObject->tableReports->columnCount() )
		{
		    if( i + 3 == DATETIME_REPORTS_COLUMN1 )
			    val = query.value( i + 3 ).asDateTime().toString( "dd.MM.yyyy hh:mm:ss" ); // ячейка времени/даты приёмки автомобиля на территорию
			else
			if( i + 3 == DATETIME_REPORTS_COLUMN2 )
			    val = query.value( i + 3 ).asDateTime().toString( "dd.MM.yyyy hh:mm:ss" ); // ячейка времени/даты отправки автомобиля с территории
			else if( i + 3 == ENTERTARA_REPORTS_COLUMN )
			{
				int tara_type = query.record().value( "tara_type" ).toInt();
                if( tara_type == 0 )
					val = tr("From scales");
				else if( tara_type == 1 )
					val = tr("Manually");
				else if( tara_type == 2 )
					val = tr("Saved tara");
			}
			else
			    val = query.value( i + 3 ).toString();


			QPixmap pixmap( "image/save_icon_24.png" );

			// Если текущая колонка - это ссылка на фотоснимки  автомобилей - второе взвешивание
			if( i == reportFormObject->tableReports->columnCount() - 3 )
			{
				buttonPhotoShow = new QPushButton( tr("Photos") );
				buttonPhotoShow->setFixedHeight( 26 );
				buttonPhotoShow->setFixedWidth( 138 );
				QObject::connect( buttonPhotoShow, SIGNAL( clicked() ), this, SLOT( showPhotoReports() ) );

				QModelIndex index = reportFormObject->tableReports->model()->index( j, i, QModelIndex() );
				reportFormObject->tableReports->setIndexWidget( index, buttonPhotoShow );

				i++;
				continue;
			}

			// Если текущая колонка - это ссылка на кнопку для сохранения фото автомобилей из БД на HDD
			if( i == reportFormObject->tableReports->columnCount() - 2 )
			{
			    buttonPhotoSave = new QPushButton();
				buttonPhotoSave->setFixedHeight( 24 );
				buttonPhotoSave->setFixedWidth( 24 );
				buttonPhotoSave->setPixmap( pixmap );
				QObject::connect( buttonPhotoSave, SIGNAL( clicked() ), this, SLOT( savePhotoReportsTo_HDD() ) );

				QModelIndex index = reportFormObject->tableReports->model()->index( j, i, QModelIndex() );
				reportFormObject->tableReports->setIndexWidget( index, buttonPhotoSave );

				i++;
				continue;
			}

			reportFormObject->tableReports->setItem( j, i, new QTableWidgetItem( val ) );
			i++;
        }
        j++;

		i = 0;
    }

	reportFormObject->tableReports->setUpdatesEnabled( true );
	reportFormObject->tableReports->show();
}
// Отобразить окно настроек
void MyClass::showSettingsWindow()
{
    // Процедура считывания настроек  для  IP-камер пользователем
	settings->Load_IP_CAMs_Settings();

    settings->showSettingsDialog(); // открыть  форму настроек

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	mysqlProccessObject->saveEventToLog( tr("Entered the settings") + QString("%1")
		.arg( loginWindowObject->currentNameUser ) );
}
// Отобразить окно отчётов
void MyClass::showReportsWindow()
{
	if( loginWindowObject->role == loginWindowObject->ADMIN && optionsObj->check(optionsObj->getWasteKey(), "sornost" ) )
	{
		reportFormObject->exportExccellButton_sor->show();
	}
	else
	{
		reportFormObject->exportExccellButton_sor->hide();
	}

	// Запретить обновление таблицы
	reportFormObject->tableReports->setUpdatesEnabled( false );
	reportFormObject->tableReports->hide();

	//reportFormObject->radioButtAutoComeInReports->setChecked( true ); // поставить  выбор для принятых  автомобилях
    reportFormObject->radioButtAutoComeOutReports->setChecked( true );
	show_records();
	reportFormObject->reportForm->show();

	// Разрешить обновление таблицы
	reportFormObject->tableReports->setUpdatesEnabled( true );
	reportFormObject->tableReports->show();

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	mysqlProccessObject->saveEventToLog( tr("Entered the registry:") + QString("%1")
		.arg( loginWindowObject->currentNameUser ) );
}
// Скрыть окно отчётов
void MyClass::hideReportsWindow()
{
	//QGraphicsOpacityEffect *opacityEffect = new QGraphicsOpacityEffect();
	//opacityEffect->setOpacity( 1.0 );
	// this->setGraphicsEffect( opacityEffect );
	reportFormObject->reportForm->hide();

	//this->rowAutoRecept = -1;
	//this->columnAutoRecept = -1;
	reportFormObject->row = -1;
	reportFormObject->column = -1;

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	mysqlProccessObject->saveEventToLog( tr("Closed the registry:") + QString("%1")
		.arg( loginWindowObject->currentNameUser ) );
}
// Событие закрытия окна настроек
void MyClass::closeSettingsWin()
{
	QVector< QString > *log_messages_vec = new QVector< QString >();

	settings->dialogSettings->hide();
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	mysqlProccessObject->saveEventToLog( tr("Closed the settings:") + QString("%1")
		.arg( loginWindowObject->currentNameUser ) );
}
// Закрыть форму приёмки/отправки
void MyClass::closeAcceptForm()
{
	acceptform->acceptionFormWindow->hide();
}
// Отоборажать/скрывать камеры наблюдения
void MyClass::showIP_CamsPanel( int state )
{
}
// Записать вес  БРУТТО  в окноприёмки/отправки автомобиля
void MyClass::makeBruttoWeightSlot() // получить вес
{
    QMessageBox mess;

	#ifndef DEBUG
	if( isVPConnected == false && settings->group_TCPWeight->isChecked() ) //
	{
		mess.setText( tr("Communication with scales missing!") ); //Связь с весами отсутствует!
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

		return;
	}
	if( flagsNoUarts_connection[ settings->currentScales ] == false && !settings->group_TCPWeight->isChecked() )
	{
		mess.setText( tr("Communication with scales missing!") );
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();
		return;
	}

	if( verifyAutoPosition( weightScales_01.toFloat() ) == false || getPosState() == false )
	{
		if( loginWindowObject->role == loginWindowObject->ADMIN )
		{
			int result = QMessageBox::question( NULL,
				tr("by Admin"),
				tr("Do you want to weigh the vehicle?"),
				QMessageBox::Ok, QMessageBox::Cancel );

			if( result != QMessageBox::Ok )
				return;
		}
		else
		    return;
	}
    #endif

	if (settings->check_PositionIgnore->isChecked() == true)
	{
		mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		mysqlProccessObject->saveEventToLog(tr("Weighing Brutto ignoring auto position!"));
	}
	weightScales_last_no_sor = weightScales_01.toInt();

	if( settings->group_Sornost->isChecked() )
	{
		acceptform->bruttoWeight->setText( QString( "%1" ).arg( weightScales_sor ) );
	}
	else
	{
		acceptform->bruttoWeight->setText( QString( "%1" ).arg( weightScales_01 ) );
	}

	if( acceptform->checkSor->isChecked() )
	{
		int w = weightScales_01.toInt() * acceptform->spinSor->value();
		acceptform->bruttoWeight->setText( QString( "%1" ).arg( w ) );
	}

    makeNettoWeight();
}

// Записать вес  НЕТТО  в окноприёмки/отправки автомобиля
void MyClass::makeTaraWeightSlot() // получить вес
{
    QMessageBox mess;

	#ifndef DEBUG
	if( isVPConnected == false && settings->group_TCPWeight->isChecked() ) //
	{
		mess.setText( tr("Communication with scales missing!") ); //Связь с весами отсутствует!
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

		return;
	}
	if( flagsNoUarts_connection[ settings->currentScales ] == false && !settings->group_TCPWeight->isChecked() )
	{
		mess.setText( tr("Communication with scales missing!") );
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();
		return;
	}

	if( verifyAutoPosition( weightScales_01.toFloat() ) == false || getPosState() == false )
	{
		if( loginWindowObject->role == loginWindowObject->ADMIN )
		{
			int result = QMessageBox::question( NULL,
				tr("by Admin"),
				tr("Do you want to weigh the vehicle?"),
				QMessageBox::Ok, QMessageBox::Cancel );

			if( result != QMessageBox::Ok )
				return;
		}
		else
		    return;
	}
    #endif

	if (settings->check_PositionIgnore->isChecked() == true)
	{
		mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		mysqlProccessObject->saveEventToLog(tr("Weighing Tara ignoring auto position!"));
	}
	weightScales_last_no_sor = weightScales_01.toInt();

	if( settings->group_Sornost->isChecked() && !settings->checkSorUseOnlyBrutto->isChecked() )
	{
		acceptform->taraWeight->setText( QString( "%1" ).arg( weightScales_sor ) );
	}
	else
	{
		acceptform->taraWeight->setText( QString( "%1" ).arg( weightScales_01 ) );
	}

	if( acceptform->checkSor->isChecked() )
	{
		int w = weightScales_01.toInt() * acceptform->spinSor->value();
		acceptform->taraWeight->setText( QString( "%1" ).arg( w ) );
	}

    makeNettoWeight();
}
// Вычислить нетто
void MyClass::makeNettoWeight()
{
	acceptform->nettoWeight->setText( QString( "%1" )
		.arg( acceptform->bruttoWeight->text().toDouble() - acceptform->taraWeight->text().toDouble() ) );
}
// Получить вес БРУТТО прицепа
void MyClass::makeBruttoWeightPricepSlot()
{
	QMessageBox mess;

#ifndef DEBUG
	if (isVPConnected == false && settings->group_TCPWeight->isChecked()) //
	{
		mess.setText(tr("Communication with scales missing!")); //Связь с весами отсутствует!
		mess.setStandardButtons(QMessageBox::Cancel);
		mess.setButtonText(QMessageBox::Cancel, "Ok");
		mess.setIcon(QMessageBox::Warning);
		mess.setWindowTitle(tr("Prom-Soft"));
		mess.exec();

		return;
	}
	if (flagsNoUarts_connection[settings->currentScales] == false && !settings->group_TCPWeight->isChecked())
	{
		mess.setText(tr("Communication with scales missing!"));
		mess.setStandardButtons(QMessageBox::Cancel);
		mess.setButtonText(QMessageBox::Cancel, "Ok");
		mess.setIcon(QMessageBox::Warning);
		mess.setWindowTitle(tr("Prom-Soft"));
		mess.exec();
		return;
	}

	if (verifyAutoPosition(weightScales_01.toFloat()) == false || getPosState() == false)
	{
		if (loginWindowObject->role == loginWindowObject->ADMIN)
		{
			int result = QMessageBox::question(NULL,
				tr("by Admin"),
				tr("Do you want to weigh the vehicle?"),
				QMessageBox::Ok, QMessageBox::Cancel);

			if (result != QMessageBox::Ok)
				return;
		}
		else
			return;
	}
#endif

	if (settings->check_PositionIgnore->isChecked() == true)
	{
		mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		mysqlProccessObject->saveEventToLog(tr("Weighing Brutto trailer ignoring auto position!"));
	}
	weightScales_last_no_sor = weightScales_01.toInt();

	if( settings->group_Sornost->isChecked() )
	{
		acceptform->bruttoWeightPricep->setText( QString( "%1" ).arg( weightScales_sor ) );
	}
	else
	{
		acceptform->bruttoWeightPricep->setText( QString( "%1" ).arg( weightScales_01 ) );
	}

	if( acceptform->checkSor->isChecked() )
	{
		int w = weightScales_01.toInt() * acceptform->spinSor->value();
		acceptform->bruttoWeightPricep->setText( QString( "%1" ).arg( w ) );
	}

    makeNettoWeightPricep();
}
// Записать вес  НЕТТО  в окно приёмки/отправки автомобиля для прицепа
void MyClass::makeTaraWeightPricepSlot()
{
    QMessageBox mess;

	#ifndef DEBUG
	if( isVPConnected == false && settings->group_TCPWeight->isChecked() ) //
	{
		mess.setText( tr("Communication with scales missing!") ); //Связь с весами отсутствует!
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

		return;
	}
	if( flagsNoUarts_connection[ settings->currentScales ] == false && !settings->group_TCPWeight->isChecked() )
	{
		mess.setText( tr("Communication with scales missing!") );
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();
		return;
	}

	if( verifyAutoPosition( weightScales_01.toFloat() ) == false || getPosState() == false )
	{
		if( loginWindowObject->role == loginWindowObject->ADMIN )
		{
			int result = QMessageBox::question( NULL,
				tr("by Admin"),
				tr("Do you want to weigh the vehicle?"),
				QMessageBox::Ok, QMessageBox::Cancel );

			if( result != QMessageBox::Ok )
				return;
		}
		else
		    return;
	}
	#endif

	if (settings->check_PositionIgnore->isChecked() == true)
	{
		mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		mysqlProccessObject->saveEventToLog(tr("Weighing Tara trailer ignoring auto position!"));
	}
	weightScales_last_no_sor = weightScales_01.toInt();

	if( settings->group_Sornost->isChecked() && !settings->checkSorUseOnlyBrutto->isChecked() )
	{
		acceptform->taraWeightPricep->setText( QString( "%1" ).arg( weightScales_sor ) );
	}
	else
	{
		acceptform->taraWeightPricep->setText( QString( "%1" ).arg( weightScales_01 ) );
	}

	if( acceptform->checkSor->isChecked() )
	{
		int w = weightScales_01.toInt() * acceptform->spinSor->value();
		acceptform->taraWeightPricep->setText( QString( "%1" ).arg( w ) );
	}

    makeNettoWeightPricep();
}
// Вычислить нетто прицепа
void MyClass::makeNettoWeightPricep()
{
	acceptform->nettoWeightPricep->setText( QString( "%1" )
		.arg( acceptform->bruttoWeightPricep->text().toDouble() - acceptform->taraWeightPricep->text().toDouble() ) );
}
// Обнуление показания весов
void MyClass::zeroing()
{
    QByteArray bytesRequest;

	if( !settings->serPortObjectsList.at( settings->currentScales )->isOpen() && !settings->vectorGroupUDP_Settings.at( settings->currentScales )->isChecked() )
	{
		QMessageBox::critical( NULL, tr("Zero error"), tr("COM-port closed"), QMessageBox::Ok );
		return;
	}

	// Запись лога
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	mysqlProccessObject->saveEventToLog( tr("Set to zero: ") + QString( "%1, %2")
		.arg( settings->currentScales + 1 ).arg( loginWindowObject->currentNameUser ) );

	// Прочитать текущие настройки COM-прорта  из  Ini-файла
    settings->getCurrentSettingsCOM_PortIniFile( currSettingsComPortPtr, settings->currentScales );

    QString str = currSettingsComPortPtr->type_scales;
    int idx = settings->vectorTypesVP_main.at( settings->currentScales )->currentIndex();  // определить тип весов, выбранный  в comboBox выбора весов

	switch( idx )
	{
	    case 0: // ВП-05А
		{
			QByteArray ba = settings->hashTypesEndsOfFrames.value( currSettingsComPortPtr->endoframe );

			// Разрешено обнуление по UDP
			if( settings->vectorGroupUDP_Settings.at( settings->currentScales )->isChecked() == true )
			{
			    udp_socket->writeDatagram( QByteArray().append( "\2AH09\3" ), strlen( "\2AH09\3" ),
				    QHostAddress( settings->vectorEnterIP_UDP_Settings.at( settings->currentScales )->text() ),
					settings->vectorEnterDstPort_UDP_Settings.at( settings->currentScales )->text().toInt() );

			    return;
			}
			else // Через COM-порт
			{
				if( settings->vectorcheckBoxUSB_En.at( 0 )->isChecked() )
				{
					if( settings->serPortObjectsList.at( settings->currentScales )->isOpen() == true )
					{
						settings->serPortObjectsList.at( settings->currentScales )->close();
					}
					settings->serPortObjectsList.at( settings->currentScales )->open( QIODevice::ReadWrite );
				}

				bytesRequest.append( "\2AH09\3" ).append( ba ); // ВП-05А
				// serialPortClassObject->writeData2Port( settings->serPortObjectsList.at( settings->currentScales ), bytesRequest, bytesRequest.length() );

				// bytesRequest.append( "\2A000\3" ).append( ba ); // ВП-10
				// serialPortClassObject->writeData2Port( settings->serPortObjectsList.at( settings->currentScales ), bytesRequest, bytesRequest.length() );
			}

		} break;
        case 1: // ВП-01А/Ц
		{
			QByteArray ba = settings->hashTypesEndsOfFrames.value( currSettingsComPortPtr->endoframe );
			bytesRequest.append( "T\r" ).append( ba );
		}break;
		case 20: //
		{
			// 01 06 00 01 00 17 98 04
			bytesRequest.append( 0x01 );
			bytesRequest.append( 0x06 );
			bytesRequest.append( (char)0x00 );
			bytesRequest.append( 0x01 );
			bytesRequest.append( (char)0x00 );
			bytesRequest.append( 0x17 );
			bytesRequest.append( 0x98 );
			bytesRequest.append( 0x04 );
		} break;
		case 21: // Техноваги
		{
			QString addrVP = QString::number(settings->vectorAddrVP.at( settings->currentScales)->value());
			if ( addrVP.size() == 1 )
				addrVP.prepend("0");
			QByteArray ba = "G" + addrVP + "S";
			// G01S0d0a
			bytesRequest.append( ba ).append("\r\n");
		} break;
	}

	serialPortClassObject->writeData2Port( settings->serPortObjectsList.at( settings->currentScales ), bytesRequest, bytesRequest.length() );
}

// Вызвать окно справочников
void MyClass::showDictionaryWindow()
{
	dictionaryFormObject->showDictionaryWindow();
	// Запись лога
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	mysqlProccessObject->saveEventToLog( tr("Entered dictionaries: ") + QString("%1")
		.arg( loginWindowObject->currentNameUser ) );
}
// Скрыть окно справочников
void MyClass::closeDictionaryWindow()
{
	dictionaryFormObject->namesSenders->setCurrentText( "" );
	dictionaryFormObject->namesReceivers->setCurrentText( "" );
	dictionaryFormObject->namesPayers->setCurrentText( "" );
	dictionaryFormObject->namesTransporters->setCurrentText( "" );
	dictionaryFormObject->namesGoods->setCurrentText( "" );
	dictionaryFormObject->namesAuto->setCurrentText( "" );
	dictionaryFormObject->namesPrizep->setCurrentText( "" );

	dictionaryFormObject->resetValidation();
	dictionaryFormObject->dictionaryWindow->hide();

	// Запись лога
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	mysqlProccessObject->saveEventToLog( tr("Dictionaries closed: ") + QString("%1")
		.arg( loginWindowObject->currentNameUser ) );
}

// Добавить запись в спавочник
void MyClass::addToDB_Slot()
{
	bool result;
	QSqlError error;

	QRegExp regExp;
	regExp.setPattern( "[a-z,0-9,а-я,А-Я]" );

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

    // Обработка выбора вкладки справочников, выбираем из БД соответствующие данные
	switch( dictionaryFormObject->current_tab )
	{
	    case dictionaryFormObject->SENDERS: // "Отправители"
		{
			if( dictionaryFormObject->namesSenders->currentText().isEmpty() == true ||
				!dictionaryFormObject->namesSenders->currentText().contains( regExp ) )
			{
				dictionaryFormObject->validationDictionaries->setText( tr("Enter the name") );
				dictionaryFormObject->validationDictionaries->show();
				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
				dictionaryFormObject->namesSenders->setStyleSheet( "border-color: Red;" );
				return;
			}

			query.prepare( QString( "SELECT id,senders FROM dictionaries;" ) );
			result = query.exec();

			// Если записей в БД нет, то производим первую запись в БД (справолчники)
			if( query.next() == false )
			{
				query.prepare( QString( "INSERT INTO dictionaries (senders) VALUES( :senders );" ) );
	            query.bindValue( ":senders", dictionaryFormObject->namesSenders->currentText() );
			    result = query.exec();

				if( result == false )
				{
					dictionaryFormObject->validationDictionaries->setText( tr("Database error!") );
                    dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
					dictionaryFormObject->namesSenders->setStyleSheet( "border-color: Red;" );
					dictionaryFormObject->validationDictionaries->show();
				}
				else
				{
					dictionaryFormObject->validationDictionaries->hide();
                    // return;
				}
			}
			else
			{
				query.prev();
				int i = 0;

				while( query.next() )
				{
					QString str = query.value( 1 ).toString();
					if( str == dictionaryFormObject->namesSenders->currentText() )
					{
						// Validation failed (такое наименование уже есть)
						dictionaryFormObject->validationDictionaries->setText( tr("The name is already exist!") );
						dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
						dictionaryFormObject->namesSenders->setStyleSheet( "border-color: Red;" );
						dictionaryFormObject->validationDictionaries->show();
						return;
					}

					if( query.value( 1 ).isNull() )
					{
						query.prepare( QString( "UPDATE dictionaries SET senders=:senders WHERE id='%1';" ).arg( query.value( 0 ).toInt() ) );
						query.bindValue( ":senders", dictionaryFormObject->namesSenders->currentText() );
						result = query.exec();

						if( result == true )
					    {
							dictionaryFormObject->validationDictionaries->setText( tr("The record is successfully saved.") );
							dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
							dictionaryFormObject->namesSenders->setStyleSheet( "border-color: Green;" );
							dictionaryFormObject->validationDictionaries->show();

							// Запись лога
							mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	                        mysqlProccessObject->saveEventToLog( tr("Added to dictionary: ") + QString( "\"%1\" - %2")
								.arg( dictionaryFormObject->namesSenders->currentText() ).arg( loginWindowObject->currentNameUser ) );
					    }

					    i++;
					}
				}
				//
				if( i == 0 ) // если Значения NULL не встретились, то созадём отдельную запись
				{
					query.prepare( QString( "INSERT INTO dictionaries (senders) VALUES( :senders );" ) );
					query.bindValue( ":senders", dictionaryFormObject->namesSenders->currentText() );
					result = query.exec();

					if( result == false )
					{
						dictionaryFormObject->validationDictionaries->setText( tr("Database error!") );
						dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
						dictionaryFormObject->namesSenders->setStyleSheet( "border-color: Red;" );
						dictionaryFormObject->validationDictionaries->show();
					}
					else
					{
						dictionaryFormObject->validationDictionaries->setText( tr("The record is successfully saved.") );
						dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
						dictionaryFormObject->namesSenders->setStyleSheet( "border-color: Green;" );
					    dictionaryFormObject->validationDictionaries->show();

						// Запись лога
						mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	                    mysqlProccessObject->saveEventToLog( tr("Added to dictionary: ") + QString("\"%1\" - %2")
							.arg( dictionaryFormObject->namesSenders->currentText() ).arg( loginWindowObject->currentNameUser ) );
					}
				}
			}

		} break;
		case dictionaryFormObject->RECEIVERS: // "Получатели"
		{
			if( dictionaryFormObject->namesReceivers->currentText().isEmpty() == true ||
				!dictionaryFormObject->namesReceivers->currentText().contains( regExp ) )
			{
				dictionaryFormObject->validationDictionaries->setText( tr("Enter the name") );
				dictionaryFormObject->validationDictionaries->show();
				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
				dictionaryFormObject->namesReceivers->setStyleSheet( "border-color: Red;" );
				return;
			}

			query.prepare( QString( "SELECT id,receivers FROM dictionaries;" ) );
			result = query.exec();

			// Если записей в БД нет, то производим первую запис в БД (справолчники)
			if( query.next() == false )
			{
				query.prepare( QString( "INSERT INTO dictionaries (receivers) VALUES( :receivers );" ) );
	            query.bindValue( ":receivers", dictionaryFormObject->namesReceivers->currentText() );
			    result = query.exec();

				if( result == false )
				{
					dictionaryFormObject->validationDictionaries->setText( tr("Database error!") );
					dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
					dictionaryFormObject->namesReceivers->setStyleSheet( "border-color: Red;" );
					dictionaryFormObject->validationDictionaries->show();
				}
				else
				{
					dictionaryFormObject->validationDictionaries->hide();
                    // return;
				}
			}
			else
			{
				query.prev();
				int i = 0;

				while( query.next() )
				{
					QString str = query.value( 1 ).toString();
					if( str == dictionaryFormObject->namesReceivers->currentText() )
					{
						// Validation failed (такое наименование уже есть)
						dictionaryFormObject->validationDictionaries->setText( tr("The name is already exist!") );
						dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
						dictionaryFormObject->namesReceivers->setStyleSheet( "border-color: Red;" );
					    dictionaryFormObject->validationDictionaries->show();
						return;
					}

					if( query.value( 1 ).isNull() )
					{
						query.prepare( QString( "UPDATE dictionaries SET receivers=:receivers WHERE id='%1';" ).arg( query.value( 0 ).toInt() ) );
						query.bindValue( ":receivers", dictionaryFormObject->namesReceivers->currentText() );
						result = query.exec();

						if( result == true )
					    {
							dictionaryFormObject->validationDictionaries->setText( tr("The record is successfully saved.") );
							dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
							dictionaryFormObject->namesReceivers->setStyleSheet( "border-color: Green;" );
							dictionaryFormObject->validationDictionaries->show();

							// Запись лога
							mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	                        mysqlProccessObject->saveEventToLog( tr("Added to dictionary: ") + QString( "\"%1\" - %2")
								.arg( dictionaryFormObject->namesReceivers->currentText() ).arg( loginWindowObject->currentNameUser ) );
					    }

					    i++;
					}
				}
				//
				if( i == 0 ) // если Значения NULL не встретились, то созадём отдельную запись
				{
					query.prepare( QString( "INSERT INTO dictionaries (receivers) VALUES( :receivers );" ) );;
					query.bindValue( ":receivers", dictionaryFormObject->namesReceivers->currentText() );
					result = query.exec();

					if( result == false )
					{
						dictionaryFormObject->validationDictionaries->setText( tr("Database error!") );
						dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
						dictionaryFormObject->namesReceivers->setStyleSheet( "border-color: Red;" );
					    dictionaryFormObject->validationDictionaries->show();
					}
					else
					{
						dictionaryFormObject->validationDictionaries->setText( tr("The record is successfully saved.") );
						dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
						dictionaryFormObject->namesReceivers->setStyleSheet( "border-color: Green;" );
					    dictionaryFormObject->validationDictionaries->show();

					    // Запись лога
						mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	                    mysqlProccessObject->saveEventToLog( tr("Added to dictionary: ") + QString("\"%1\" пользователем: %2")
							.arg( dictionaryFormObject->namesReceivers->currentText() ).arg( loginWindowObject->currentNameUser ) );
					}
				}
			}

		} break;
		case dictionaryFormObject->PAYERS: // "Плательщики"
		{
			if( dictionaryFormObject->namesPayers->currentText().isEmpty() == true ||
				!dictionaryFormObject->namesPayers->currentText().contains( regExp ) )
			{
				dictionaryFormObject->validationDictionaries->setText( tr("Enter the name") );
				dictionaryFormObject->validationDictionaries->show();
				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
				dictionaryFormObject->namesPayers->setStyleSheet( "border-color: Red;" );
				return;
			}

			query.prepare( QString( "SELECT id,payers FROM dictionaries;" ) );
			result = query.exec();

			// Если записей в БД нет, то производим первую запис в БД (справолчники)
			if( query.next() == false )
			{
				query.prepare( QString( "INSERT INTO dictionaries (payers) VALUES( :payers );" ) );
	            query.bindValue( ":payers", dictionaryFormObject->namesPayers->currentText() );
			    result = query.exec();

				if( result == false )
				{
					dictionaryFormObject->validationDictionaries->setText( tr("Database error!") );
					dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
					dictionaryFormObject->namesPayers->setStyleSheet( "border-color: Red;" );
					dictionaryFormObject->validationDictionaries->show();
				}
				else
				{
					dictionaryFormObject->validationDictionaries->hide();
                    // return;
				}
			}
			else
			{
				query.prev();
				int i = 0;

				while( query.next() )
				{
					QString str = query.value( 1 ).toString();
					if( str == dictionaryFormObject->namesPayers->currentText() )
					{
						// Validation failed (такое наименование уже есть)
						dictionaryFormObject->validationDictionaries->setText( tr("The name is already exist!") );
						dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
						dictionaryFormObject->namesPayers->setStyleSheet( "border-color: Red;" );
					    dictionaryFormObject->validationDictionaries->show();
						return;
					}

					if( query.value( 1 ).isNull() )
					{
						query.prepare( QString( "UPDATE dictionaries SET payers=:payers WHERE id='%1';" ).arg( query.value( 0 ).toInt() ) );
						query.bindValue( ":payers", dictionaryFormObject->namesPayers->currentText() );
						result = query.exec();

						if( result == true )
					    {
							dictionaryFormObject->validationDictionaries->setText( tr("The record is successfully saved.") );
							dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
							dictionaryFormObject->namesPayers->setStyleSheet( "border-color: Green;" );
							dictionaryFormObject->validationDictionaries->show();

							// Запись лога
							mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	                        mysqlProccessObject->saveEventToLog( tr("Added to dictionary: ") + QString( "\"%1\" - %2")
								.arg( dictionaryFormObject->namesPayers->currentText() ).arg( loginWindowObject->currentNameUser ) );
					    }

					    i++;
					}
				}
				//
				if( i == 0 ) // если Значения NULL не встретились, то созадём отдельную запись
				{
					query.prepare( QString( "INSERT INTO dictionaries (payers) VALUES( :payers );" ) );
					query.bindValue( ":payers", dictionaryFormObject->namesPayers->currentText() );
					result = query.exec();

					if( result == false )
					{
						dictionaryFormObject->validationDictionaries->setText( tr("Database error!") );
						dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
						dictionaryFormObject->namesPayers->setStyleSheet( "border-color: Red;" );
					    dictionaryFormObject->validationDictionaries->show();
					}
					else
					{
						dictionaryFormObject->validationDictionaries->setText( tr("The record is successfully saved.") );
						dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
						dictionaryFormObject->namesPayers->setStyleSheet( "border-color: Green;" );
					    dictionaryFormObject->validationDictionaries->show();

						// Запись лога
						mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	                    mysqlProccessObject->saveEventToLog( tr("Added to dictionary: ") + QString( "\"%1\" - %2")
							.arg( dictionaryFormObject->namesPayers->currentText() ).arg( loginWindowObject->currentNameUser ) );
					}
				}
			}

		} break;
		case dictionaryFormObject->TRANSPORTERS: // "Перевозики"
		{
			if( dictionaryFormObject->namesTransporters->currentText().isEmpty() == true ||
				!dictionaryFormObject->namesTransporters->currentText().contains( regExp ) )
			{
				dictionaryFormObject->validationDictionaries->setText( tr("Enter the name") );
				dictionaryFormObject->validationDictionaries->show();
				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
				dictionaryFormObject->namesTransporters->setStyleSheet( "border-color: Red;" );
				return;
			}

			query.prepare( QString( "SELECT id,transporters FROM dictionaries;" ) );
			result = query.exec();

			// Если записей в БД нет, то производим первую запис в БД (справолчники)
			if( query.next() == false )
			{
				query.prepare( QString( "INSERT INTO dictionaries (transporters) VALUES( :transporters );" ) );
	            query.bindValue( ":transporters", dictionaryFormObject->namesTransporters->currentText() );
			    result = query.exec();

				if( result == false )
				{
					dictionaryFormObject->validationDictionaries->setText( tr("Database error!") );
					dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
					dictionaryFormObject->namesTransporters->setStyleSheet( "border-color: Red;" );
					dictionaryFormObject->validationDictionaries->show();
				}
				else
				{
					dictionaryFormObject->validationDictionaries->hide();
                    // return;
				}
			}
			else
			{
				query.prev();
				int i = 0;

				while( query.next() )
				{
					QString str = query.value( 1 ).toString();
					if( str == dictionaryFormObject->namesTransporters->currentText() )
					{
						// Validation failed (такое наименование уже есть)
						dictionaryFormObject->validationDictionaries->setText( tr("The name is already exist!") );
						dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
						dictionaryFormObject->namesTransporters->setStyleSheet( "border-color: Red;" );
					    dictionaryFormObject->validationDictionaries->show();
						return;
					}

					if( query.value( 1 ).isNull() )
					{
						query.prepare( QString( "UPDATE dictionaries SET transporters=:transporters WHERE id='%1';" )
							.arg( query.value( 0 ).toInt() ) );
						query.bindValue( ":transporters", dictionaryFormObject->namesTransporters->currentText() );
						result = query.exec();

						if( result == true )
					    {
							dictionaryFormObject->validationDictionaries->setText( tr("The record is successfully saved.") );
							dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
							dictionaryFormObject->namesTransporters->setStyleSheet( "border-color: Green;" );
							dictionaryFormObject->validationDictionaries->show();

							// Запись лога
							mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	                        mysqlProccessObject->saveEventToLog( tr("Added to dictionary: ") + QString("\"%1\" - %2")
								.arg( dictionaryFormObject->namesTransporters->currentText() ).arg( loginWindowObject->currentNameUser ) );
					    }

					    i++;
					}
				}
				//
				if( i == 0 ) // если Значения NULL не встретились, то созадём отдельную запись
				{
					query.prepare( QString( "INSERT INTO dictionaries (transporters) VALUES( :transporters );" ) );
					query.bindValue( ":transporters", dictionaryFormObject->namesTransporters->currentText() );
					result = query.exec();

					if( result == false )
					{
						dictionaryFormObject->validationDictionaries->setText( tr("Database error!") );
						dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
						dictionaryFormObject->namesTransporters->setStyleSheet( "border-color: Red;" );
					    dictionaryFormObject->validationDictionaries->show();
					}
					else
					{
						dictionaryFormObject->validationDictionaries->setText( tr("The record is successfully saved.") );
						dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
						dictionaryFormObject->namesTransporters->setStyleSheet( "border-color: Green;" );
					    dictionaryFormObject->validationDictionaries->show();

						// Запись лога
						mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	                    mysqlProccessObject->saveEventToLog( tr("Added to dictionary: ") + QString("\"%1\" - %2")
							.arg( dictionaryFormObject->namesTransporters->currentText() ).arg( loginWindowObject->currentNameUser ) );
					}
				}
			}

		} break;
		case dictionaryFormObject->GOODS: // "Грузы"
		{
			if( dictionaryFormObject->namesGoods->currentText().isEmpty() == true ||
				!dictionaryFormObject->namesGoods->currentText().contains( regExp ) )
			{
				dictionaryFormObject->validationDictionaries->setText( tr("Enter the name") );
				dictionaryFormObject->validationDictionaries->show();
				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
				dictionaryFormObject->namesGoods->setStyleSheet( "border-color: Red;" );
				return;
			}

			query.prepare( QString( "SELECT id,goods FROM dictionaries;" ) );
			result = query.exec();

			// Если записей в БД нет, то производим первую запис в БД (справолчники)
			if( query.next() == false )
			{
				query.prepare( QString( "INSERT INTO dictionaries (goods) VALUES( :goods );" ) );
	            query.bindValue( ":goods", dictionaryFormObject->namesGoods->currentText() );
			    result = query.exec();

				if( result == false )
				{
					dictionaryFormObject->validationDictionaries->setText( tr("Database error!") );
					dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
					dictionaryFormObject->namesGoods->setStyleSheet( "border-color: Red;" );
					dictionaryFormObject->validationDictionaries->show();
				}
				else
				{
					dictionaryFormObject->validationDictionaries->hide();
                    // return;
				}
			}
			else
			{
				query.prev();
				int i = 0;

				while( query.next() )
				{
					QString str = query.value( 1 ).toString();
					if( str == dictionaryFormObject->namesGoods->currentText() )
					{
						// Validation failed (такое наименование уже есть)
						dictionaryFormObject->validationDictionaries->setText( tr("The name is already exist!") );
						dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
						dictionaryFormObject->namesGoods->setStyleSheet( "border-color: Red;" );
					    dictionaryFormObject->validationDictionaries->show();
						return;
					}

					if( query.value( 1 ).isNull() )
					{
						query.prepare( QString( "UPDATE dictionaries SET goods=:goods WHERE id='%1';" ).arg( query.value( 0 ).toInt() ) );
						query.bindValue( ":goods", dictionaryFormObject->namesGoods->currentText() );
						result = query.exec();

						if( result == true )
					    {
							dictionaryFormObject->validationDictionaries->setText( tr("The record is successfully saved.") );
							dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
							dictionaryFormObject->namesGoods->setStyleSheet( "border-color: Green;" );
							dictionaryFormObject->validationDictionaries->show();

							// Запись лога
							mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	                        mysqlProccessObject->saveEventToLog( tr("Added to dictionary: ") + QString("\"%1\" - %2")
								.arg( dictionaryFormObject->namesGoods->currentText() ).arg( loginWindowObject->currentNameUser ) );
					    }

					    i++;
					}
				}
				//
				if( i == 0 ) // если Значения NULL не встретились, то созадём отдельную запись
				{
					query.prepare( QString( "INSERT INTO dictionaries (goods) VALUES( :goods );" ) );
					query.bindValue( ":goods", dictionaryFormObject->namesGoods->currentText() );
					result = query.exec();

					if( result == false )
					{
						dictionaryFormObject->validationDictionaries->setText( tr("Database error!") );
						dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
						dictionaryFormObject->namesGoods->setStyleSheet( "border-color: Red;" );
					    dictionaryFormObject->validationDictionaries->show();
					}
					else
					{
						dictionaryFormObject->validationDictionaries->setText( tr("The record is successfully saved.") );
						dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
						dictionaryFormObject->namesGoods->setStyleSheet( "border-color: Green;" );
					    dictionaryFormObject->validationDictionaries->show();

						// Запись лога
						mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	                    mysqlProccessObject->saveEventToLog( tr("Added to dictionary: ") + QString( "\"%1\" - %2")
							.arg( dictionaryFormObject->namesGoods->currentText() ).arg( loginWindowObject->currentNameUser ) );
					    // return;
					}
				}
			}

		} break;
		////////////////
		case dictionaryFormObject->AUTO: // "Авто"
		{
			if( dictionaryFormObject->namesAuto->currentText().isEmpty() == true ||
				!dictionaryFormObject->namesAuto->currentText().contains( regExp ) )
			{
				dictionaryFormObject->validationDictionaries->setText( tr("Enter the name") );
				dictionaryFormObject->validationDictionaries->show();
				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
				dictionaryFormObject->namesAuto->setStyleSheet( "border-color: Red;" );
				return;
			}

			query.prepare( QString( "SELECT id,Number_Auto FROM dictionaries;" ) );
			result = query.exec();

			// Если записей в БД нет, то производим первую запись в БД (справолчники)
			if( query.next() == false )
			{
				query.prepare( QString( "INSERT INTO dictionaries (Number_Auto) VALUES( :Number_Auto );" ) );
	            query.bindValue( ":Number_Auto", dictionaryFormObject->namesAuto->currentText() );
			    result = query.exec();

				if( result == false )
				{
					dictionaryFormObject->validationDictionaries->setText( tr("Database error!") );
                    dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
					dictionaryFormObject->namesAuto->setStyleSheet( "border-color: Red;" );
					dictionaryFormObject->validationDictionaries->show();
				}
				else
				{
					dictionaryFormObject->validationDictionaries->hide();
                    // return;
				}
			}
			else
			{
				query.prev();
				int i = 0;

				while( query.next() )
				{
					QString str = query.value( 1 ).toString();
					if( str == dictionaryFormObject->namesAuto->currentText() )
					{
						// Validation failed (такое наименование уже есть)
						dictionaryFormObject->validationDictionaries->setText( tr("The name is already exist!") );
						dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
						dictionaryFormObject->namesAuto->setStyleSheet( "border-color: Red;" );
						dictionaryFormObject->validationDictionaries->show();
						return;
					}

					if( query.value( 1 ).isNull() )
					{
						query.prepare( QString( "UPDATE dictionaries SET Number_Auto=:Number_Auto WHERE id='%1';" ).arg( query.value( 0 ).toInt() ) );
						query.bindValue( ":Number_Auto", dictionaryFormObject->namesAuto->currentText() );
						result = query.exec();

						if( result == true )
					    {
							dictionaryFormObject->validationDictionaries->setText( tr("The record is successfully saved.") );
							dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
							dictionaryFormObject->namesAuto->setStyleSheet( "border-color: Green;" );
							dictionaryFormObject->validationDictionaries->show();

							// Запись лога
							mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	                        mysqlProccessObject->saveEventToLog( tr("Added to dictionary: ") + QString( "\"%1\" - %2")
								.arg( dictionaryFormObject->namesAuto->currentText() ).arg( loginWindowObject->currentNameUser ) );
					    }
					    i++;
					}
				}
				//
				if( i == 0 ) // если Значения NULL не встретились, то созадём отдельную запись
				{
					query.prepare( QString( "INSERT INTO dictionaries (Number_Auto) VALUES( :Number_Auto );" ) );
					query.bindValue( ":Number_Auto", dictionaryFormObject->namesAuto->currentText() );
					result = query.exec();

					if( result == false )
					{
						dictionaryFormObject->validationDictionaries->setText( tr("Database error!") );
						dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
						dictionaryFormObject->namesAuto->setStyleSheet( "border-color: Red;" );
						dictionaryFormObject->validationDictionaries->show();
					}
					else
					{
						dictionaryFormObject->validationDictionaries->setText( tr("The record is successfully saved.") );
						dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
						dictionaryFormObject->namesAuto->setStyleSheet( "border-color: Green;" );
					    dictionaryFormObject->validationDictionaries->show();
						// Запись лога
						mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	                    mysqlProccessObject->saveEventToLog( tr("Added to dictionary: ") + QString("\"%1\" - %2")
							.arg( dictionaryFormObject->namesAuto->currentText() ).arg( loginWindowObject->currentNameUser ) );
					}
				}
			}
		} break;
	    case dictionaryFormObject->PRIZEP: // "Прицепы"
		{
			if( dictionaryFormObject->namesPrizep->currentText().isEmpty() == true ||
				!dictionaryFormObject->namesPrizep->currentText().contains( regExp ) )
			{
				dictionaryFormObject->validationDictionaries->setText( tr("Enter the name") );
				dictionaryFormObject->validationDictionaries->show();
				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
				dictionaryFormObject->namesPrizep->setStyleSheet( "border-color: Red;" );
				return;
			}

			query.prepare( QString( "SELECT id,Num_Prizep FROM dictionaries;" ) );
			result = query.exec();

			// Если записей в БД нет, то производим первую запись в БД (справолчники)
			if( query.next() == false )
			{
				query.prepare( QString( "INSERT INTO dictionaries (Num_Prizep) VALUES( :Num_Prizep );" ) );
	            query.bindValue( ":Num_Prizep", dictionaryFormObject->namesPrizep->currentText() );
			    result = query.exec();

				if( result == false )
				{
					dictionaryFormObject->validationDictionaries->setText( tr("Database error!") );
                    dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
					dictionaryFormObject->namesPrizep->setStyleSheet( "border-color: Red;" );
					dictionaryFormObject->validationDictionaries->show();
				}
				else
				{
					dictionaryFormObject->validationDictionaries->hide();
                    // return;
				}
			}
			else
			{
				query.prev();
				int i = 0;

				while( query.next() )
				{
					QString str = query.value( 1 ).toString();
					if( str == dictionaryFormObject->namesPrizep->currentText() )
					{
						// Validation failed (такое наименование уже есть)
						dictionaryFormObject->validationDictionaries->setText( tr("The name is already exist!") );
						dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
						dictionaryFormObject->namesPrizep->setStyleSheet( "border-color: Red;" );
						dictionaryFormObject->validationDictionaries->show();
						return;
					}

					if( query.value( 1 ).isNull() )
					{
						query.prepare( QString( "UPDATE dictionaries SET Num_Prizep=:Num_Prizep WHERE id='%1';" ).arg( query.value( 0 ).toInt() ) );
						query.bindValue( ":Num_Prizep", dictionaryFormObject->namesPrizep->currentText() );
						result = query.exec();

						if( result == true )
					    {
							dictionaryFormObject->validationDictionaries->setText( tr("The record is successfully saved.") );
							dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
							dictionaryFormObject->namesPrizep->setStyleSheet( "border-color: Green;" );
							dictionaryFormObject->validationDictionaries->show();

							// Запись лога
							mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	                        mysqlProccessObject->saveEventToLog( tr("Added to dictionary: ") + QString( "\"%1\" - %2")
								.arg( dictionaryFormObject->namesPrizep->currentText() ).arg( loginWindowObject->currentNameUser ) );
					    }
					    i++;
					}
				}
				//
				if( i == 0 ) // если Значения NULL не встретились, то созадём отдельную запись
				{
					query.prepare( QString( "INSERT INTO dictionaries (Num_Prizep) VALUES( :Num_Prizep );" ) );
					query.bindValue( ":Num_Prizep", dictionaryFormObject->namesPrizep->currentText() );
					result = query.exec();

					if( result == false )
					{
						dictionaryFormObject->validationDictionaries->setText( tr("Database error!") );
						dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
						dictionaryFormObject->namesPrizep->setStyleSheet( "border-color: Red;" );
						dictionaryFormObject->validationDictionaries->show();
					}
					else
					{
						dictionaryFormObject->validationDictionaries->setText( tr("The record is successfully saved.") );
						dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
						dictionaryFormObject->namesPrizep->setStyleSheet( "border-color: Green;" );
					    dictionaryFormObject->validationDictionaries->show();

						// Запись лога
						mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	                    mysqlProccessObject->saveEventToLog( tr("Added to dictionary: ") + QString("\"%1\" - %2")
							.arg( dictionaryFormObject->namesPrizep->currentText() ).arg( loginWindowObject->currentNameUser ) );
					}
				}
			}

		} break;
		//case dictionaryFormObject->MARK_AUTO: // "Марка автомобиля"
		//{
		//	if( dictionaryFormObject->typesMarkAuto->currentText().isEmpty() == true ||  !dictionaryFormObject->typesMarkAuto->currentText().contains( regExp ) )
		//	{
		//		dictionaryFormObject->validationDictionaries->setText( "Введите наименование!" );
		//		dictionaryFormObject->validationDictionaries->show();
		//		dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
		//		dictionaryFormObject->typesMarkAuto->setStyleSheet( "border-color: Red;" );
		//		return;
		//	}

		//	query.prepare( QString( "SELECT id, mark_auto FROM dictionaries;" ) );
		//	result = query.exec();

		//	// Если записей в БД нет, то производим первую запис в БД (справочники)
		//	if( query.next() == false )
		//	{
		//		query.prepare( QString( "INSERT INTO dictionaries (mark_auto) VALUES( :mark_auto );" ) ); // .arg( dictionaryFormObject->mark_auto->currentText() ) );
		//		query.bindValue( ":mark_auto", dictionaryFormObject->typesMarkAuto->currentText() );
		//	    result = query.exec();

		//		if( result == false )
		//		{
		//			dictionaryFormObject->validationDictionaries->setText( "Ошибка записи в БД!" );
		//			dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
		//			dictionaryFormObject->typesMarkAuto->setStyleSheet( "border-color: Red;" );
		//			dictionaryFormObject->validationDictionaries->show();
		//		}
		//		else
		//		{
		//			dictionaryFormObject->validationDictionaries->hide();
  //                  // return;
		//		}
		//	}
		//	else
		//	{
		//		query.prev();
		//		int i = 0;

		//		while( query.next() )
		//		{
		//			QString str = query.value( 1 ).toString();
		//			if( str == dictionaryFormObject->typesMarkAuto->currentText() )
		//			{
		//				// Validation failed (такое наименование уже есть)
		//				dictionaryFormObject->validationDictionaries->setText( "Такое наименование уже есть!" );
		//				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
		//				dictionaryFormObject->typesMarkAuto->setStyleSheet( "border-color: Red;" );
		//			    dictionaryFormObject->validationDictionaries->show();
		//				return;
		//			}

		//			if( query.value( 1 ).isNull() )
		//			{
		//				query.prepare( QString( "UPDATE dictionaries SET mark_auto=:mark_auto WHERE id='%1';" ).arg( query.value( 0 ).toInt() ) );
		//				query.bindValue( ":mark_auto", dictionaryFormObject->typesMarkAuto->currentText() );
		//				result = query.exec();
		//
		//				if( result == true )
		//			    {
		//					dictionaryFormObject->validationDictionaries->setText( "Запись успешно сохранена в БД!" );
		//					dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
		//					dictionaryFormObject->typesMarkAuto->setStyleSheet( "border-color: Green;" );
		//					dictionaryFormObject->validationDictionaries->show();

		//					// Запись лога
						  //mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		//					mysqlProccessObject->saveEventToLog( QString( "Добавление в \"Справочник марки автомобилей\" \"%1\" пользователем: %2").arg( dictionaryFormObject->typesMarkAuto->currentText() ).arg( loginWindowObject->currentNameUser ) );
		//			    }

		//			    i++;
		//			}
		//		}
		//		//
		//		if( i == 0 ) // если Значения NULL не встретились, то созадём отдельную запись
		//		{
		//			query.prepare( QString( "INSERT INTO dictionaries (mark_auto) VALUES( :mark_auto );" ) ); // .arg( dictionaryFormObject->mark_auto->currentText() ) );
		//			query.bindValue( ":mark_auto", dictionaryFormObject->typesMarkAuto->currentText() );
		//			result = query.exec();

		//			if( result == false )
		//			{
		//				dictionaryFormObject->validationDictionaries->setText( "Ошибка записи в БД!" );
		//				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
		//				dictionaryFormObject->typesMarkAuto->setStyleSheet( "border-color: Red;" );
		//			    dictionaryFormObject->validationDictionaries->show();
		//			}
		//			else
		//			{
		//				dictionaryFormObject->validationDictionaries->setText( "Запись успешно сохранена в БД!" );
		//				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
		//				dictionaryFormObject->typesMarkAuto->setStyleSheet( "border-color: Green;" );
		//			    dictionaryFormObject->validationDictionaries->show();

		//				// Запись лога
		//				mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		//				mysqlProccessObject->saveEventToLog( QString( "Добавление в \"Справочник марок актомобиля\" \"%1\" пользователем: %2").arg( dictionaryFormObject->typesMarkAuto->currentText() ).arg( loginWindowObject->currentNameUser ) );
		//			    // return;
		//			}
		//		}
		//	}

		//} break;
	}
	// Обновить список выбранного справочника
    readDictionariesFromDB_ALL(); // прочитать  все словари из БД
}
// Удалить запись из спавочника
void MyClass::deleteRecordDB_Slot()
{
    bool result;
	QSqlError error;

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

    // Обработка выбора вкладки справочников, выбираем из БД соответствующие данные
	switch( dictionaryFormObject->current_tab )
	{
	    case dictionaryFormObject->SENDERS: // "Отправители"
		{
			query.prepare( QString( "UPDATE dictionaries SET senders=NULL WHERE senders='%1';" )
				.arg( dictionaryFormObject->namesSenders->currentText() ) );
			result = query.exec();

			if( result == true )
			{
				dictionaryFormObject->validationDictionaries->setText( tr("The entry was successfully deleted from the database!") );
				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
				dictionaryFormObject->namesSenders->setStyleSheet( "border-color: Green;" );
				dictionaryFormObject->validationDictionaries->show();

				// Запись лога
				mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	            mysqlProccessObject->saveEventToLog( tr("Deleted from dictionaries: ") + QString("\"%1\" - %2")
					.arg( dictionaryFormObject->namesSenders->currentText() ).arg( loginWindowObject->currentNameUser ) );
			}
			else
			{
				dictionaryFormObject->validationDictionaries->setText( tr("Database error!") );
				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
				dictionaryFormObject->namesSenders->setStyleSheet( "border-color: Red;" );
				dictionaryFormObject->validationDictionaries->show();
			}

		} break;
		case dictionaryFormObject->RECEIVERS: // "Получатели"
		{
			query.prepare( QString( "UPDATE dictionaries SET receivers=NULL WHERE receivers='%1';" )
				.arg( dictionaryFormObject->namesReceivers->currentText() ) );
			result = query.exec();

			if( result == true )
			{
				dictionaryFormObject->validationDictionaries->setText( tr("The entry was successfully deleted from the database!") );
				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
				dictionaryFormObject->namesReceivers->setStyleSheet( "border-color: Green;" );
				dictionaryFormObject->validationDictionaries->show();

				// Запись лога
				mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	            mysqlProccessObject->saveEventToLog( tr("Deleted from dictionaries: ") + QString("\"%1\" - %2")
					.arg( dictionaryFormObject->namesReceivers->currentText() ).arg( loginWindowObject->currentNameUser ) );
			}
			else
			{
				dictionaryFormObject->validationDictionaries->setText( tr("Database error!") );
				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
				dictionaryFormObject->namesReceivers->setStyleSheet( "border-color: Red;" );
				dictionaryFormObject->validationDictionaries->show();
			}

		} break;
		case dictionaryFormObject->PAYERS: // "Плательщики"
		{
			query.prepare( QString( "UPDATE dictionaries SET payers=NULL WHERE payers='%1';" )
				.arg( dictionaryFormObject->namesPayers->currentText() ) );
			result = query.exec();

			if( result == true )
			{
				dictionaryFormObject->validationDictionaries->setText( tr("The entry was successfully deleted from the database!") );
				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
				dictionaryFormObject->namesPayers->setStyleSheet( "border-color: Green;" );
				dictionaryFormObject->validationDictionaries->show();

				// Запись лога
				mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	            mysqlProccessObject->saveEventToLog( tr("Deleted from dictionaries: ") + QString("\"%1\" - %2")
					.arg( dictionaryFormObject->namesPayers->currentText() ).arg( loginWindowObject->currentNameUser ) );
			}
			else
			{
				dictionaryFormObject->validationDictionaries->setText( tr("Database error!") );
				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
				dictionaryFormObject->namesPayers->setStyleSheet( "border-color: Red;" );
				dictionaryFormObject->validationDictionaries->show();
			}

		} break;
		case dictionaryFormObject->TRANSPORTERS: // "Перевозщики"
		{
			query.prepare( QString( "UPDATE dictionaries SET transporters=NULL WHERE transporters='%1';" )
				.arg( dictionaryFormObject->namesTransporters->currentText() ) );
			result = query.exec();

			if( result == true )
			{
				dictionaryFormObject->validationDictionaries->setText( tr("The entry was successfully deleted from the database!") );
				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
				dictionaryFormObject->namesTransporters->setStyleSheet( "border-color: Green;" );
				dictionaryFormObject->validationDictionaries->show();

				// Запись лога
				mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	            mysqlProccessObject->saveEventToLog( tr("Deleted from dictionaries: ") + QString( "\"%1\" - %2")
					.arg( dictionaryFormObject->namesTransporters->currentText() ).arg( loginWindowObject->currentNameUser ) );
			}
			else
			{
				dictionaryFormObject->validationDictionaries->setText( tr("Database error!") );
				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
				dictionaryFormObject->namesTransporters->setStyleSheet( "border-color: Red;" );
				dictionaryFormObject->validationDictionaries->show();
			}

		} break;
		case dictionaryFormObject->GOODS: // "Грузы"
		{
			query.prepare( QString( "UPDATE dictionaries SET goods=NULL WHERE goods='%1';" )
				.arg( dictionaryFormObject->namesGoods->currentText() ) );
			result = query.exec();

			if( result == true )
			{
				dictionaryFormObject->validationDictionaries->setText( tr("The entry was successfully deleted from the database!") );
				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
				dictionaryFormObject->namesGoods->setStyleSheet( "border-color: Green;" );
				dictionaryFormObject->validationDictionaries->show();

				// Запись лога
				mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	            mysqlProccessObject->saveEventToLog( tr("Deleted from dictionaries: ") + QString( "\"%1\" - %2")
					.arg( dictionaryFormObject->namesGoods->currentText() ).arg( loginWindowObject->currentNameUser ) );
			}
			else
			{
				dictionaryFormObject->validationDictionaries->setText( tr("Database error!") );
				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
				dictionaryFormObject->namesGoods->setStyleSheet( "border-color: Red;" );
				dictionaryFormObject->validationDictionaries->show();
			}

		} break;
		/////////
        case dictionaryFormObject->AUTO: // "Авто"
		{
			query.prepare( QString( "UPDATE dictionaries SET Number_Auto=NULL WHERE Number_Auto='%1';" )
				.arg( dictionaryFormObject->namesAuto->currentText() ) );
			result = query.exec();

			if( result == true )
			{
				dictionaryFormObject->validationDictionaries->setText( tr("The entry was successfully deleted from the database!") );
				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
				dictionaryFormObject->namesAuto->setStyleSheet( "border-color: Green;" );
				dictionaryFormObject->validationDictionaries->show();

				// Запись лога
				mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	            mysqlProccessObject->saveEventToLog( tr("Deleted from dictionaries: ") + QString("\"%1\" - %2")
					.arg( dictionaryFormObject->namesAuto->currentText() ).arg( loginWindowObject->currentNameUser ) );
			}
			else
			{
				dictionaryFormObject->validationDictionaries->setText( tr("Database error!") );
				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
				dictionaryFormObject->namesAuto->setStyleSheet( "border-color: Red;" );
				dictionaryFormObject->validationDictionaries->show();
			}
		} break;
		case dictionaryFormObject->PRIZEP: // "Прицепы"
		{
			query.prepare( QString( "UPDATE dictionaries SET Num_Prizep=NULL WHERE Num_Prizep='%1';" )
				.arg( dictionaryFormObject->namesPrizep->currentText() ) );
			result = query.exec();

			if( result == true )
			{
				dictionaryFormObject->validationDictionaries->setText( tr("The entry was successfully deleted from the database!") );
				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Green;" );
				dictionaryFormObject->namesPrizep->setStyleSheet( "border-color: Green;" );
				dictionaryFormObject->validationDictionaries->show();

				// Запись лога
				mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	            mysqlProccessObject->saveEventToLog( tr("Deleted from dictionaries: ") + QString( "\"%1\" - %2")
					.arg( dictionaryFormObject->namesPrizep->currentText() ).arg( loginWindowObject->currentNameUser ) );
			}
			else
			{
				dictionaryFormObject->validationDictionaries->setText( tr("Database error!") );
				dictionaryFormObject->validationDictionaries->setStyleSheet( "color: Red;" );
				dictionaryFormObject->namesPrizep->setStyleSheet( "border-color: Red;" );
				dictionaryFormObject->validationDictionaries->show();
			}
		} break;
	}
    // Обновить список выбранного справочника
    readDictionariesFromDB_ALL(); // прочитать  все словари из БД
}

// Удалить запись, выбранную  мышкой
void MyClass::deleteAutoReception_Record()
{
	QMessageBox mess;
	int row = this->rowAutoRecept;
    int column = this->columnAutoRecept;

	if( tableAutoReception->rowCount() == 0 || this->rowAutoRecept == -1 || this->columnAutoRecept == -1 ) // нет записей в таблице
	{
		deleteRecord->setEnabled( false ); // запретить кнопку удаления записи

	    mess.setText( tr("Select the entry to delete!") );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Delete Record" ) );
		mess.exec();

		return;
	}

	mess.setText( tr("Do you want to remove the entry?") );
	mess.setStandardButtons( QMessageBox::Ok | QMessageBox::Cancel );
	mess.setButtonText( QMessageBox::Ok, tr("Yes") );
	mess.setButtonText( QMessageBox::Cancel, tr("No") );
	mess.setWindowTitle( tr( "Delete record" ) );

	//QPixmap iconChumBacket;
	//iconChumBacket.load( "image/delete-icon_64.png", "PNG", QPixmap::Auto );
	//mess.setIconPixmap( iconChumBacket );
	mess.exec();

	if(  mess.result() == QMessageBox::Cancel )
	{
        this->rowAutoRecept = -1;
	    this->columnAutoRecept = -1;

		return;
	}


	QString tableName = "AutoReception";
	if( CardsMode_action->isChecked() )
		tableName = "AcceptCards";
    bool result;
	QSqlError error;

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

	QString str = tableAutoReception->item( row, tableName == "AutoReception" ? DATETIME_AUTORECEPTION_COLUMN_SHOWN : 0 )->text();
	QString strDateTime = str;
	QDateTime dateTime = QDateTime::fromString( str, "dd.MM.yyyy hh:mm:ss" );
	qDebug()<< dateTime;
	str = dateTime.toString( "yyyy-MM-dd hh:mm:ss" );

	query.exec( QString( "SELECT IDF FROM %1 WHERE Date_Time='%2';" ).arg( tableName ).arg( str ) );
	int idf_ = 0;
	if( query.next() )
	{
		idf_ = query.value( 0 ).toInt();
	}

	result = query.exec( QString( "DELETE FROM %1 WHERE Date_Time='%2';" ).arg( tableName ).arg( str ) );
	result = query.exec( QString( "DELETE FROM images WHERE ID='%1';" ).arg( idf_ ) );

	result = query.exec( QString( "SELECT * FROM %1;" ).arg( tableName ) );

    // Записать событие в лог
	QString numAuto;
	QTableWidgetItem *itemData = tableAutoReception->item( row, DATETIME_AUTORECEPTION_COLUMN );
	numAuto = itemData->text();

	if( numAuto != "" && numAuto != " " ) // если номер прицепа не пустая строка, то
	{
		mysqlProccessObject->saveEventToLog( tr("Removed the record. Trailer num:") + QString("%1, %2").arg( numAuto ).arg( strDateTime ) );
	}
	else
	{
		itemData = tableAutoReception->item( row, 0 );
	    numAuto = itemData->text();
		mysqlProccessObject->saveEventToLog( tr("Removed the record. Vehicle num:") + QString("%1, %2").arg( numAuto ).arg( strDateTime ) );
	}

	//
	if( query.record().count() != 0 )
	{
		tableAutoReception->removeRow( row );

		int j = 0;
        while( query.next() )
		{
			j++;
			tableAutoReception->setRowCount( j );
		}
	}
	else
	{
	}

    // Удаляем строки в таблице приёмки/отправки автомобилей
    int rowCnt = tableAutoReception->rowCount();
	while( rowCnt > 0 )
	{
	    tableAutoReception->removeRow( rowCnt - 1 );
	    rowCnt--;
	}

	showDataBaseContent_Slot(); // отобразить записи в таблице приёмки/отправки автомобилей

	if( CardsMode_action->isChecked() )
		CardsMode_Slot( true );

	this->rowAutoRecept = -1;
	this->columnAutoRecept = -1;
}

// Вызов слота при смене вкладки справочника
void MyClass::currDictionaryChanged( int num_tab )
{
	dictionaryFormObject->resetValidation(); // сброс валидации
	dictionaryFormObject->current_tab = num_tab; // сохраняем номер вкладки справочника
	readDictionariesFromDB();

	// dictionaryFormObject->namesSenders->setCurrentText( acceptform->sendersList->currentText() ); // поместить данные из QComboBox  приёмки/отправки автомобилей в QComboBox  справочника
	// dictionaryFormObject->namesReceivers->setCurrentText( acceptform->recepientsList->currentText() ); // поместить данные из QComboBox  приёмки/отправки автомобилей в QComboBox  справочника
}
// Прочитать записи справочников из БД
void MyClass::readDictionariesFromDB()
{
	bool result;
	QSqlError error;
	QString tempDictionary_item;

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

	// Обработка выбора вкладки справочников, выбираем из БД соответствующие данные
	switch( dictionaryFormObject->current_tab )
	{
	    case dictionaryFormObject->SENDERS: // "Отправители"
		{
			query.prepare( QString( "SELECT senders FROM dictionaries;" ) );
			result = query.exec();

			tempDictionary_item = acceptform->sendersList->currentText();
			dictionaryFormObject->namesSenders->clear();
			acceptform->sendersList->clear(); // из формы приёмки/отправки автомобиля
			reportFormObject->selectSender->clear();

			while( query.next() )
			{
				if( query.value( 0 ).isNull() == true )
					continue;

				dictionaryFormObject->namesSenders->addItem( query.value( 0 ).toString() );
			    acceptform->sendersList->addItem( query.value( 0 ).toString() );
				reportFormObject->selectSender->addItem( query.value( 0 ).toString() );
			}
			dictionaryFormObject->namesSenders->setCurrentText("");
			acceptform->sendersList->setCurrentText( tempDictionary_item );
		} break;
		case dictionaryFormObject->RECEIVERS: // "Получатели"
		{
			query.prepare( QString( "SELECT receivers FROM dictionaries;" ) );
			result = query.exec();

			tempDictionary_item = acceptform->recepientsList->currentText();
			dictionaryFormObject->namesReceivers->clear();
			acceptform->recepientsList->clear(); // из формы приёмки/отправки автомобиля
			reportFormObject->selectAccepter->clear();

			while( query.next() )
			{
				if( query.value( 0 ).isNull() == true )
					continue;

				dictionaryFormObject->namesReceivers->addItem( query.value( 0 ).toString() );
			    acceptform->recepientsList->addItem( query.value( 0 ).toString() );
				reportFormObject->selectAccepter->addItem( query.value( 0 ).toString() );
			}
			dictionaryFormObject->namesReceivers->setCurrentText("");
			acceptform->recepientsList->setCurrentText( tempDictionary_item );
		} break;
		case dictionaryFormObject->PAYERS: // "Плательщики"
		{
			query.prepare( QString( "SELECT payers FROM dictionaries;" ) );
			result = query.exec();

			tempDictionary_item = acceptform->payersList->currentText();
			dictionaryFormObject->namesPayers->clear();
			acceptform->payersList->clear(); // из формы приёмки/отправки автомобиля
			reportFormObject->selectPayer->clear();

			while( query.next() )
			{
				if( query.value( 0 ).isNull() == true )
					continue;

				dictionaryFormObject->namesPayers->addItem( query.value( 0 ).toString() );
				acceptform->payersList->addItem( query.value( 0 ).toString() );
				reportFormObject->selectPayer->addItem( query.value( 0 ).toString() );
			}
			acceptform->payersList->setCurrentText( tempDictionary_item );
			dictionaryFormObject->namesPayers->setCurrentText("");
		} break;
		case dictionaryFormObject->TRANSPORTERS: // "Перевозики"
		{
			query.prepare( QString( "SELECT transporters FROM dictionaries;" ) );
			result = query.exec();

			tempDictionary_item = acceptform->namesCarrierList->currentText();
			dictionaryFormObject->namesTransporters->clear();
			acceptform->namesCarrierList->clear(); // из формы приёмки/отправки автомобиля
			reportFormObject->selectCarrier->clear();

			while( query.next() )
			{
				if( query.value( 0 ).isNull() == true )
					continue;

				dictionaryFormObject->namesTransporters->addItem( query.value( 0 ).toString() );
				acceptform->namesCarrierList->addItem( query.value( 0 ).toString() );
				reportFormObject->selectCarrier->addItem( query.value( 0 ).toString() );
			}

			acceptform->namesCarrierList->setCurrentText( tempDictionary_item );
			dictionaryFormObject->namesTransporters->setCurrentText("");
		} break;
		case dictionaryFormObject->GOODS: // "Грузы"
		{
            query.prepare( QString( "SELECT goods FROM dictionaries;" ) );
			result = query.exec();

			tempDictionary_item = acceptform->namesGoods->currentText();
			dictionaryFormObject->namesGoods->clear();
			acceptform->namesGoods->clear(); // из формы приёмки/отправки автомобиля
			reportFormObject->selectTypeOfGoods->clear();

			while( query.next() )
			{
				if( query.value( 0 ).isNull() == true )
					continue;

				dictionaryFormObject->namesGoods->addItem( query.value( 0 ).toString() );
				acceptform->namesGoods->addItem( query.value( 0 ).toString() );
				reportFormObject->selectTypeOfGoods->addItem( query.value( 0 ).toString() );
			}
			acceptform->namesGoods->setCurrentText( tempDictionary_item );
			dictionaryFormObject->namesGoods->setCurrentText("");
		} break;

		case dictionaryFormObject->AUTO: // "Авто"
		{
            query.prepare( QString( "SELECT Number_Auto FROM dictionaries;" ) );
			result = query.exec();

			tempDictionary_item = acceptform->AutoNumber->currentText();
			dictionaryFormObject->namesAuto->clear();
			acceptform->AutoNumber->clear(); // из формы приёмки/отправки автомобиля
			reportFormObject->enterNumAuto->clear();

			while( query.next() )
			{
				if( query.value( 0 ).isNull() == true )
					continue;

				dictionaryFormObject->namesAuto->addItem( query.value( 0 ).toString() );
				acceptform->AutoNumber->addItem( query.value( 0 ).toString() );
				reportFormObject->enterNumAuto->addItem( query.value( 0 ).toString() );
			}
			acceptform->AutoNumber->setCurrentText( tempDictionary_item );
			dictionaryFormObject->namesAuto->setCurrentText("");
		} break;

		case dictionaryFormObject->PRIZEP: // "Прицепы"
		{
            query.prepare( QString( "SELECT Num_Prizep FROM dictionaries;" ) );
			result = query.exec();

			tempDictionary_item = acceptform->AutoPrizepNumber->currentText();
			dictionaryFormObject->namesPrizep->clear();
			acceptform->AutoPrizepNumber->clear(); // из формы приёмки/отправки автомобиля
			//reportFormObject->selectTypeOfGoods->clear();

			while( query.next() )
			{
				if( query.value( 0 ).isNull() == true )
					continue;

				dictionaryFormObject->namesPrizep->addItem( query.value( 0 ).toString() );
				acceptform->AutoPrizepNumber->addItem( query.value( 0 ).toString() );
				//reportFormObject->selectTypeOfGoods->addItem( query.value( 0 ).toString() );
			}
			acceptform->AutoPrizepNumber->setCurrentText( tempDictionary_item );
			dictionaryFormObject->namesPrizep->setCurrentText("");
		} break;
	}
}
// Прочитать  все словари из БД
void MyClass::readDictionariesFromDB_ALL()
{
	static int prev_num_tab;
	prev_num_tab = dictionaryFormObject->current_tab;

	// Заполняем ComboBoxes справочников
	for( dictionaryFormObject->current_tab = 0; dictionaryFormObject->current_tab < MAX_NUM_DICTIONARIES; dictionaryFormObject->current_tab++ )
	{
		readDictionariesFromDB();
	}

	dictionaryFormObject->current_tab = prev_num_tab;
}
//
void MyClass::selectSender_Slot() // вызов справочника с вкладкой "Отправитель"
{
	QString data = acceptform->sendersList->currentText();

	dictionaryFormObject->tabbedWindow->setCurrentIndex( 0 );
	dictionaryFormObject->namesSenders->setCurrentText( data ); // поместить данные из QComboBox  приёмки/отправки автомобилей в QComboBox  справочника
	dictionaryFormObject->showDictionaryWindow();
}
//
void MyClass::selectRecepient_Slot()
{
	QString data = acceptform->recepientsList->currentText();

    dictionaryFormObject->tabbedWindow->setCurrentIndex( 1 );
	dictionaryFormObject->namesReceivers->setCurrentText( data ); // поместить данные из QComboBox  приёмки/отправки автомобилей в QComboBox  справочника
	dictionaryFormObject->showDictionaryWindow();
	acceptform->recepientsList->setCurrentText( data );
}
//
void MyClass::selectPayer_Slot()
{
	QString data = acceptform->payersList->currentText();

	dictionaryFormObject->tabbedWindow->setCurrentIndex( 2 );
	dictionaryFormObject->namesPayers->setCurrentText( data ); // поместить данные из QComboBox  приёмки/отправки автомобилей в QComboBox  справочника
	dictionaryFormObject->showDictionaryWindow();
	acceptform->payersList->setCurrentText( data );
}
//
void MyClass::selectCarrier_Slot()
{
	QString data = acceptform->namesCarrierList->currentText();

    dictionaryFormObject->tabbedWindow->setCurrentIndex( 3 );
	dictionaryFormObject->namesTransporters->setCurrentText( data ); // поместить данные из QComboBox  приёмки/отправки автомобилей в QComboBox  справочника
	dictionaryFormObject->showDictionaryWindow();
	acceptform->namesCarrierList->setCurrentText( data );
}
//
void MyClass::selectGoods_Slot()
{
	QString data = acceptform->namesGoods->currentText();

	dictionaryFormObject->tabbedWindow->setCurrentIndex( 4 );
	dictionaryFormObject->namesGoods->setCurrentText( data ); // поместить данные из QComboBox  приёмки/отправки автомобилей в QComboBox  справочника
	dictionaryFormObject->showDictionaryWindow();
	acceptform->namesGoods->setCurrentText( data );
}
//
void MyClass::selectAuto_Slot()
{
	QString data = acceptform->AutoNumber->currentText();

	dictionaryFormObject->tabbedWindow->setCurrentIndex( 5 );
	dictionaryFormObject->namesAuto->setCurrentText( data ); // поместить данные из QComboBox  приёмки/отправки автомобилей в QComboBox  справочника
	dictionaryFormObject->showDictionaryWindow();
	acceptform->AutoNumber->setCurrentText( data );
}
//
void MyClass::selectPrizep_Slot()
{
	QString data = acceptform->AutoPrizepNumber->currentText();

	dictionaryFormObject->tabbedWindow->setCurrentIndex( 6 );
	dictionaryFormObject->namesPrizep->setCurrentText( data ); // поместить данные из QComboBox  приёмки/отправки автомобилей в QComboBox  справочника
	dictionaryFormObject->showDictionaryWindow();
	acceptform->AutoPrizepNumber->setCurrentText( data );
}
// Создать документ - накладную для распечатки
void MyClass::makeNakladnaya_SLOT() // создать документ - накладную
{
	int row = reportFormObject->row;
    int column = reportFormObject->column;
	QMessageBox mess;

	if( reportFormObject->tableReports->rowCount() == 0 || row == -1 || column == -1 ) // нет записей в таблице
	{
	    mess.setText( tr("Select the entry first!") );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

		return;
	}

	//reportFormObject->row = this->rowAutoRecept;
    reportFormObject->showPrintDialog();
}
// Распечатать таблицу с данными из журнала взвешивания
void MyClass::printRecords_Slot()
{
    QMessageBox mess;

	if( reportFormObject->tableReports->rowCount() == 0 )
	{
        mess.setText( tr("Select the entry first!") );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.setWindowIcon( *global_Icon );
		mess.exec();

		return;
	}

	QSettings settingsLang( "settings.ini", QSettings::IniFormat );
	settingsLang.setIniCodec( "Windows-1251" );

	QString langFromConf;
	settingsLang.beginGroup( "Server" );
	langFromConf = settingsLang.value( "lang", "ua" ).toString();
	settingsLang.endGroup();


	QRect screenResolution = qApp->desktop()->screenGeometry();
	QPrinter printer( QPrinter::HighResolution );
	printer.setOrientation( QPrinter::Landscape );
	QString html;

	webView = new QWebView; // объект для создания отчёта
	webView->setStyleSheet( "background-color: #fff;" );

	QPrintPreviewDialog preview( &printer, this );
	preview.setWindowFlags( Qt::Window );
	if( langFromConf == "ru" )
	{
		preview.setLocale( QLocale::Russian );
	}
	else if( langFromConf == "ua" )
	{
		preview.setLocale( QLocale::Ukrainian );
	}
	else if( langFromConf == "en" )
	{
		preview.setLocale( QLocale::English );
	}

	QObject::connect( &preview, SIGNAL( paintRequested( QPrinter* ) ), webView, SLOT( print( QPrinter* ) ) );

	preview.setModal( true );
	preview.setOrientation( Qt::Horizontal );
	preview.setMinimumSize( screenResolution.width(), screenResolution.height() ); // развернуть на весь размер экрана
	preview.setWindowTitle( tr ( "Preview" ) ); //Предварительный просмотр

	QList<QString> rowHeader;
	int row;

	html = QString( "<h2 style='text-align: center;'>" );
	html +=	tr("Weight data from  ");
	html +=	QString( "%1г.</h2>" ).arg( QDate::currentDate().toString( "dd.MM.yyyy" ) )
		.append( " <table border='1'cellspacing='0' cellpadding='4' width=100% style='text-align: center; font-size: 18px; font-weight: bold; border: 3px solid #000; "
        "width: 1000px; margin-left: 20px;'>" );

	html.append( QString( "<p style='font-size: 18px; font-weight: bold; margin-left: 60px;'>" ) +
		tr( "Operating mode:" ) + " " +
		( reportFormObject->radioButtAutoComeInReports->isChecked() ? tr("Accepting") : tr( "Sending" ) ) +
		QString( "</p>" ) );

	html.append( "<tr>" );
	for( int h = 0; h < 15 /* reportFormObject->tableReports->columnCount() - 1 */ ; h++ )
	{
		rowHeader.append( reportFormObject->tableReports->horizontalHeaderItem( h )->text() );

		html.append( QString( "<th>%1</th>" ).arg( rowHeader.at( h ) ) );
	}
	html.append( "</tr>" );

	double brutto_sum = 0;
	double tara_sum = 0;
	double netto_sum = 0;

	QList<QString> rowData;
	for( row = 0; row < reportFormObject->tableReports->rowCount(); row++ )
	{
		html.append( "<tr>" );
		for( int c = 0; c < 15 /* reportFormObject->tableReports->columnCount() - 1 */ ; c++ )
	    {
		    rowData.append( reportFormObject->tableReports->item( row, c )->text() );
		    html.append( QString( "<td>%1</td>" ).arg( rowData.at( c ) ) );
		}

		brutto_sum += reportFormObject->tableReports->item( row, 7 )->text().toDouble(); // сумма по БРУТТО
		tara_sum += reportFormObject->tableReports->item( row, 8 )->text().toDouble(); // сумма по ТАРЕ
		netto_sum += reportFormObject->tableReports->item( row, 9 )->text().toDouble(); // сумма по НЕТТО

		rowData.clear();
        html.append( "</tr>" );
	}
	html.append( "</table><br />" );

	html.append( QString( "<p style='font-size: 18px; font-weight: bold; margin-left: 60px;'>" ) +
		tr("Total gross: ") +
		QString( "<span style='font-size: 22px; font-weight: bolder;'> %1 кг </span></p>" ).arg( brutto_sum ) );
	html.append( QString( "<p style='font-size: 18px; font-weight: bold; margin-left: 60px;'>" ) +
		tr("Total tare: ") +
		QString( "<span style='font-size: 22px; font-weight: bolder; '> %1 кг </span></p>" ).arg( tara_sum ) );
	html.append( QString( "<p style='font-size: 18px; font-weight: bold;  margin-left: 60px;'>" ) +
		tr("Total net: ") +
		QString( "<span style='font-size: 22px; font-weight: bolder;'> %1 кг </span></p>" ).arg( netto_sum ) );
	//
	html.append( QString( "<br /><span style='font-size: 18px; text-align: center;'><p><b>" ) +
		tr("Responsible: ") +
		QString("_______________________________________</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>" ) +
		tr("Date/time ") +
		QString("%1 </b></p></span><br /><br /><br /><br /><br /><br />" ).arg( QDateTime().currentDateTime().toString( "dd.MM.yyyy hh:mm:ss" ) ) );
	html.prepend( "<html style=\"background-color: #fff; padding-botom: 30px;\"><body>" );
	html.append( "</body></html>" );
	webView->setHtml( html );

	preview.exec();
	webView->deleteLater();
}
// Отменить строку двойным кликом для формирования накладной
void MyClass::makeNakladnayaDoubleClick_SLOT( int row, int column )
{
	reportFormObject->row = row;
	reportFormObject->column = column;

    makeNakladnaya_SLOT(); // создать документ - накладную

	//this->rowAutoRecept = -1;
	//this->columnAutoRecept = -1;
}
//
// Слоты для работы с формой окна для логов
void MyClass::showLogEventsWindow_Slot()
{
	QGraphicsOpacityEffect *opacityEffect = new QGraphicsOpacityEffect();
	opacityEffect->setOpacity( 0.1 );
	// this->setGraphicsEffect( opacityEffect );

	// Отобразить в окне логов данные логов
	logEventsFormObject->logWindow->clear();

	QDateTime startDateTime;
	startDateTime.currentDateTime();
	startDateTime.setTime( QTime( 0, 0, 0 ) );
	startDateTime.setDate( QDate::currentDate() );
	logEventsFormObject->timeBeginEdit->setTime( startDateTime.time() );
	logEventsFormObject->dateBeginEdit->setDate( startDateTime.date() );

	QDateTime endDateTime;
	endDateTime.currentDateTime();
	endDateTime.setTime( QTime( 23, 59, 59 ) );
	endDateTime.setDate( QDate::currentDate() );
	logEventsFormObject->timeEndEdit->setTime( endDateTime.time() );
	logEventsFormObject->dateEndEdit->setDate( endDateTime.date() );

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	logEventsFormObject->logWindow->append( mysqlProccessObject->loadEventFromLog( startDateTime, endDateTime ) ); // считать записи логов в диапазоне времени и даты

	logEventsFormObject->showWindow();
}
//
void MyClass::closeLogEventsWindow_Slot()
{
	logEventsFormObject->closeWindow();

	QGraphicsOpacityEffect *opacityEffect = new QGraphicsOpacityEffect();
	opacityEffect->setOpacity( 1.0 );
	this->setGraphicsEffect( opacityEffect );
}
// Применить фильтры логов событий отчётов
void MyClass::logEventsAcceptFiltering()
{
    QDateTime startDateTime;
	startDateTime.currentDateTime();
	startDateTime.setTime( logEventsFormObject->timeBeginEdit->time() );
	startDateTime.setDate( logEventsFormObject->dateBeginEdit->date() );

	QDateTime endDateTime;
	endDateTime.currentDateTime();
	endDateTime.setTime( logEventsFormObject->timeEndEdit->time() );
	endDateTime.setDate( logEventsFormObject->dateEndEdit->date() );

	logEventsFormObject->logWindow->clear();
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	logEventsFormObject->logWindow->append( mysqlProccessObject->loadEventFromLog( startDateTime, endDateTime ) ); // считать записи логов в диапазоне времени и даты

}
// Сбросить фильтры логов событий (устанавливаем текущие сутки в фильтре)
void MyClass::logEventsResetFiltering()
{
    QDateTime startDateTime;
	startDateTime.currentDateTime();
	startDateTime.setTime( QTime( 0, 0, 0 ) );
	startDateTime.setDate( QDate::currentDate() );
	logEventsFormObject->timeBeginEdit->setTime( startDateTime.time() );
	logEventsFormObject->dateBeginEdit->setDate( startDateTime.date() );

	QDateTime endDateTime;
	endDateTime.currentDateTime();
	endDateTime.setTime( QTime( 23, 59, 59 ) );
	endDateTime.setDate( QDate::currentDate() );
	logEventsFormObject->timeEndEdit->setTime( endDateTime.time() );
	logEventsFormObject->dateEndEdit->setDate( endDateTime.date() );

	logEventsFormObject->logWindow->clear();
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	logEventsFormObject->logWindow->append( mysqlProccessObject->loadEventFromLog( startDateTime, endDateTime ) ); // считать записи логов в диапазоне времени и даты
}

//
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void MyClass::readPendingDatagramDataRTP() // приём данных от IP-камеры
{
}
//
void MyClass::readPendingDatagramControlRTCP() // приём данных контроля качества от IP-камеры
{
}
//
// Обработка сигнала соеинения клиента с Сервером (канал передачи медиаданных)
void MyClass::connectedToServerRTP()
{
}

// Обработка сигнала соеинения клиента с Сервером (канал контроля передачи медиаданных)
void MyClass::connectedToServerRTCP()
{
}
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

void MyClass::errorSocket()
{
}

void MyClass::hostFoundSlot()
{
}

void MyClass::proxyAuthenticationRequiredSlot( const QNetworkProxy & proxy, QAuthenticator * authenticator )
{
}

void MyClass::stateChangedSlot( QAbstractSocket::SocketState socketState )
{
}

void MyClass::aboutToCloseSlot()
{
}

void MyClass::bytesWrittenSlot( qint64 bytes )
{
}

void MyClass::readChannelFinishedSlot()
{
}

void MyClass::destroyedTCP_Socket()
{
}

// Вызвать окно настроек
void MyClass::openSettingsWindow( bool state )
{
	settings->showSettingsDialog(); // открыть  форму настроек
}
// Выбор COM-порта из списка дорступных портов
void MyClass::selectedNewComPort( int port )  //
{
    QString p = settings->comPortsList->itemText( port );
	int b = settings->baudRatesList->currentIndex();

    QSerialPort *serialPortObj = settings->serPortObjectsList.at( settings->currentScales ); // извлекаем  объект COM-порта для соответствующего экземпляра весов

}

// Обработчик выбора скоростей COM-порта в ComboBox
void MyClass::selectedNewComPortSpeed( int idx )
{
	int settNum = settings->currentScales; // ???
}

// Состояние checkBox - а работы с БК поменялось
// "0" - unckecked, "2" - checked
void MyClass::chkBoxBK_Work_stateChanged( bool state )
{
}
// Состояние checkBox - а работы с RM поменялось
// "0" - unckecked, "2" - checked
void MyClass::chkBoxRM_Work_stateChanged( int state )
{
}

//
void MyClass::mainWinVP_tabChanged( int idx ) // сигнал-слот для срабатывания на смену вкладки весов (главное окно)
{
	settings->currentScales = idx;
}
//
void MyClass::selectedNewTypeVP( int idx )
{

}
// Заголовки колонок таблицы приёмки автомобилей
QList<QString> MyClass::getColumnsHeadersTableAutoAcception()
{
	QList<QString> result = QList<QString>();
	result.append( tr("Vehicle\nnumber") ); //"Номер автомобиля"
	result.append( tr("Sender") ); //"Отправитель"
	result.append( tr("Recipient") ); //"Получатель"
	result.append( tr("Payer") ); //"Плательщик"
	result.append( tr("Carrier") ); //"Перевозчик"
	result.append( tr("Cargo") ); //"Груз"
    result.append( tr("Cargo value") ); //"Стоимость груза"
	// result.append( "Влажность" );
	result.append( tr("Gross") ); //"Брутто"
	result.append( tr("Tare") ); //"Тара"
	result.append( tr("Net") ); //"Нетто"
	result.append( tr("Date\ntime") ); // дата приёмки ??? Resources
	result.append( tr("Waybill") ); //"Накладная"
	result.append( tr("User") ); //"Принял"
	result.append( tr("Trailer\nnumber") ); //"Номер прицепа"
	result.append( tr("Gross\n(trailer)") ); //"Брутто (прицеп)"
	result.append( tr("Tare\n(trailer)") ); //"Тара (прицеп)"
	result.append( tr("Net\n(trailer)") ); //"Нетто (прицеп)"
	result.append( tr("Tara\nenter") ); //"Ввод тары"

	return result;
}

QList<QString> MyClass::getColumnsHeadersTableAcceptCards()
{
	QList<QString> result = QList<QString>();
	result.append(tr("Date\ntime")); // дата приёмки
	result.append(tr("Scales")); // номер весов
	result.append(tr("RFID Code")); //"код карты"
	result.append(tr("Weight 1")); // Взвешивание 1
	result.append(tr("Weight 2")); // Взвешивание 2
	result.append(tr("Net")); //"Нетто"
	result.append(tr("Supplier")); //"Поставщик"
	result.append(tr("Material")); //"Материал"
	result.append(tr("Vehicle\nnumber")); //"Номер автомобиля"
	result.append(tr("Trailer\nnumber")); //"Номер прицепа"
	result.append(tr("Driver")); //"Водитель"
	result.append(tr("Order number")); //"порядк номер карты"
	result.append(tr("Blocked")); //"блокирована"
	result.append(tr("Photo")); //"Фото"
	return result;
}

// Обработка нажатия на checkBox
// Отображение панели светофоров
// параметр state = 0 -  галочка снята
// параметр state = 2 -  галочка поставлена
void MyClass::showTrafficLightsPanel( int state )
{
	switch( state )
	{
        case 0: // выключить светофоры
        {

        } break;
        case 2: // включить светофоры
        {

        } break;
	}
}

// Фильтр событий
bool MyClass::eventFilter( QObject *object, QEvent *e ) // вызвать метод при клике по изображению камеры, для его детального рассмотрения
{
    static bool flagBlocked = false;
    static QString prevObjectName = "";

	if( e->type() == QEvent::Close ) // срабатывание на закрытие окон просмотра фотографий из журнала взвешиваний
	{
		if( object->objectName() == "previewSnapshot_1" )
		{
			previewSnapshot_1->hide();
			previewSnapshot_2->hide();
			previewSnapshot_3->hide();
			previewSnapshot_4->hide();
			previewSnapshot_5->hide();
			previewSnapshot_6->hide();
			previewSnapshot_7->hide();
			previewSnapshot_8->hide();
		}
		if( object->objectName() == "previewSnapshot_2" )
		{
            previewSnapshot_1->hide();
			previewSnapshot_2->hide();
			previewSnapshot_3->hide();
			previewSnapshot_4->hide();
			previewSnapshot_5->hide();
			previewSnapshot_6->hide();
			previewSnapshot_7->hide();
			previewSnapshot_8->hide();
		}
		if( object->objectName() == "previewSnapshot_3" )
		{
            previewSnapshot_1->hide();
			previewSnapshot_2->hide();
			previewSnapshot_3->hide();
			previewSnapshot_4->hide();
			previewSnapshot_5->hide();
			previewSnapshot_6->hide();
			previewSnapshot_7->hide();
			previewSnapshot_8->hide();
		}
		if( object->objectName() == "previewSnapshot_4" )
		{
            previewSnapshot_1->hide();
			previewSnapshot_2->hide();
			previewSnapshot_3->hide();
			previewSnapshot_4->hide();
			previewSnapshot_5->hide();
			previewSnapshot_6->hide();
			previewSnapshot_7->hide();
			previewSnapshot_8->hide();
		}
		if( object->objectName() == "previewSnapshot_5" )
		{
            previewSnapshot_1->hide();
			previewSnapshot_2->hide();
			previewSnapshot_3->hide();
			previewSnapshot_4->hide();
			previewSnapshot_5->hide();
			previewSnapshot_6->hide();
			previewSnapshot_7->hide();
			previewSnapshot_8->hide();
		}
		if( object->objectName() == "previewSnapshot_6" )
		{
            previewSnapshot_1->hide();
			previewSnapshot_2->hide();
			previewSnapshot_3->hide();
			previewSnapshot_4->hide();
			previewSnapshot_5->hide();
			previewSnapshot_6->hide();
			previewSnapshot_7->hide();
			previewSnapshot_8->hide();
		}
		if( object->objectName() == "previewSnapshot_7" )
		{
            previewSnapshot_1->hide();
			previewSnapshot_2->hide();
			previewSnapshot_3->hide();
			previewSnapshot_4->hide();
			previewSnapshot_5->hide();
			previewSnapshot_6->hide();
			previewSnapshot_7->hide();
			previewSnapshot_8->hide();
		}
		if( object->objectName() == "previewSnapshot_8" )
		{
            previewSnapshot_1->hide();
			previewSnapshot_2->hide();
			previewSnapshot_3->hide();
			previewSnapshot_4->hide();
			previewSnapshot_5->hide();
			previewSnapshot_6->hide();
			previewSnapshot_7->hide();
			previewSnapshot_8->hide();
		}

        return false;
	}

	//
	if( e->type() == QEvent::MouseButtonRelease  /* && Qt::LeftButton */ )
	{
		// Обработка нажатия на кнопку Меню "Настройки"
        if( object->objectName() == "menuSettings" )
        {
            settings->showSettingsDialog(); // открыть  форму настроек
		}
		return false;
	}
	//
	if( e->type() == QEvent::KeyPress ) // события "Нажатие кнопки"
	{
        if( object->objectName() == "loginWin" ) // события "Нажатие кнопки Enter или ESC" для окна входа в ситсему
	    {
            QKeyEvent *keyEvent = ( QKeyEvent *)e;
            int keyNum = keyEvent->key();

            if( keyNum == Qt::Key_Escape || keyNum == Qt::Key_Delete ) // нажата кнопка ESC
            {
                loginWindowObject->loginWin->hide(); // закрыть форму для авторизации в системе
            }
			else if( keyNum == Qt::Key_Enter || keyNum == Qt::Key_Return ) // нажата кнопка Enter
		    {
				showMainWindow(); // ввод данных для входа в систему
		    }
			return false;
	    }

	    // Форма справочников
		if( object->objectName() == "dictionaryWindow" )
		{
			QKeyEvent *keyEvent = ( QKeyEvent *)e;
	        int keyNum = keyEvent->key();

            if( keyNum == Qt::Key_Escape || keyNum == Qt::Key_Delete ) // нажата кнопка ESC
            {
				closeDictionaryWindow(); // закрыть форму справочников
            }

			return false;
		}

		// Форма отчётов
		if( object->objectName() == "reportForm" )
		{
			QKeyEvent *keyEvent = ( QKeyEvent *)e;
	        int keyNum = keyEvent->key();

            if( keyNum == Qt::Key_Escape || keyNum == Qt::Key_Delete ) // нажата кнопка ESC
            {
				hideReportsWindow(); // закрыть форму отчётов
            }

			return false;
		}

		if( object->objectName() == "acceptionFormWindow" )
		{
			QKeyEvent *keyEvent = ( QKeyEvent *)e;
	        int keyNum = keyEvent->key();

            if( keyNum == Qt::Key_Escape || keyNum == Qt::Key_Delete )
            {
				acceptform->acceptionFormWindow->hide();
            }

			return false;
		}

		if( object->objectName() == "centralWidget" )
		{
			QKeyEvent *keyEvent = ( QKeyEvent *)e;
	        int keyNum = keyEvent->key();

			if( keyNum == Qt::Key_F1 ) // нажата кнопка F1 - вызов помощи
            {
				launchHelp(); // вызов справочного документа (Help)
				return false;
            }

			QSettings settingsCams( "settings.ini", QSettings::IniFormat );
	        settingsCams.setIniCodec( "Windows-1251" );

			settingsCams.beginGroup( "Cam1" );
			QString strData1 = settingsCams.value( "cam_en" ).toString();
			settingsCams.endGroup();
			settingsCams.beginGroup( "Cam2" );
			QString strData2 = settingsCams.value( "cam_en" ).toString();
			settingsCams.endGroup();
			settingsCams.beginGroup( "Cam3" );
			QString strData3 = settingsCams.value( "cam_en" ).toString();
			settingsCams.endGroup();
			settingsCams.beginGroup( "Cam4" );
			QString strData4 = settingsCams.value( "cam_en" ).toString();
			settingsCams.endGroup();

			int keyValue = keyEvent->key();
			if( keyValue == Qt::Key_Escape )
			{
				labelMaxSize->hide();

				if( strData1 == "1" )
				{
					widget_Cam1->show();
					widget_Cam1->raise();
				}

				if( strData2 == "1" )
				{
					widget_Cam2->show();
					widget_Cam2->raise();
				}

				if( strData3 == "1" )
				{
				    widget_Cam3->show();
				    widget_Cam3->raise();
				}

				if( strData4 == "1" )
				{
					widget_Cam4->show();
					widget_Cam4->raise();
				}

				flagBlocked = false;

				return false;
			}
			return false;
		}
	}

    // Обработчик нажатия на  левую кнопку мыши
    if( e->type() == QEvent::MouseButtonPress && Qt::LeftButton )
    {
		// Обработка "кликов"   по изображениям IP-камер
		if( object->objectName() == "imageLabel1" ||
			object->objectName() == "imageLabel2" ||
			object->objectName() == "imageLabel3" ||
			object->objectName() == "imageLabel4" ||
			object->objectName() == "imageLabel_max" )  // || object->objectName() == "imageLabel3" || object->objectName() == "imageLabel4" )
		{
			QString obj_name = object->objectName();

			QLabel *label = NULL;

			QLabel *widget = qobject_cast< QLabel* >( object );

            if( flagBlocked == false )
			{
				if( obj_name == "imageLabel1" )
				{
					if( labNoSignal_Cam1->text() == tr("Cam is turn off") )
						return false;

					if( widget->pixmap() != 0 )
					{
						labelMaxSize->setPixmap( *widget->pixmap() );
						labelMaxSize->show();
						labelMaxSize->raise();

						widget_Cam1->hide();

						focus_cam = 1;
					}
				}
				if( obj_name == "imageLabel2" )
				{
                    if( labNoSignal_Cam2->text() == tr("Cam is turn off") )
						return false;

					if( widget->pixmap() != 0 )
					{
						labelMaxSize->setPixmap( *widget->pixmap() );
						labelMaxSize->show();
						labelMaxSize->raise();

						widget_Cam2->hide();

						focus_cam = 2;
					}
				}
				if( obj_name == "imageLabel3" )
				{
					if( labNoSignal_Cam3->text() == tr("Cam is turn off") )
						return false;

					if( widget->pixmap() != 0 )
					{
						labelMaxSize->setPixmap( *widget->pixmap() );
						labelMaxSize->show();
						labelMaxSize->raise();

						widget_Cam3->hide();

						focus_cam = 3;
					}
				}
				if( obj_name == "imageLabel4" )
				{
                    if( labNoSignal_Cam4->text() == tr("Cam is turn off") )
						return false;

					if( widget->pixmap() != 0 )
					{
						labelMaxSize->setPixmap( *widget->pixmap() );
						labelMaxSize->show();
						labelMaxSize->raise();

						widget_Cam4->hide();

						focus_cam = 4;
					}
				}
				prevObjectName = obj_name;
				flagBlocked = true;
			}
			else
			{
			    if( prevObjectName == "imageLabel1" &&  obj_name == "imageLabel_max" && flagBlocked == true )
                {
					labelMaxSize->hide();
					widget_Cam1->show();

					flagBlocked = false;
					focus_cam = 0;
                }
			    if( prevObjectName == "imageLabel2" &&  obj_name == "imageLabel_max" && flagBlocked == true )
                {
					labelMaxSize->hide();
					widget_Cam2->show();

					flagBlocked = false;
					focus_cam = 0;
                }
				if( prevObjectName == "imageLabel3" &&  obj_name == "imageLabel_max" && flagBlocked == true )
                {
					labelMaxSize->hide();
					widget_Cam3->show();

					flagBlocked = false;
					focus_cam = 0;
                }
				if( prevObjectName == "imageLabel4" &&  obj_name == "imageLabel_max" && flagBlocked == true )
                {
					labelMaxSize->hide();
					widget_Cam4->show();

					flagBlocked = false;
					focus_cam = 0;
                }
            }
			return false;
		}


		// Обработка кликов по левому светофору
		if( object->objectName() == "label_Svetovor1" )
		{
			QPoint point;
			QMouseEvent *mouseEvent = static_cast< QMouseEvent* >( e );
			point = mouseEvent->pos();

			// Координаты красного левого светофора
			if( point.x() >= 0 && point.x() <= 75 && point.y() >= 42 && point.y() <= 114 )
			{
				set_Svetofor( 0, 0 );
			}
			// Координаты зелёного левого светофора
			if( point.x() >= 0 && point.x() <= 75 && point.y() >= 99 && point.y() <= 200 )
			{
				set_Svetofor( 0, 1 );
			}

			return true;
		}

		// Обработка кликов по правому светофору
		if( object->objectName() == "label_Svetovor2" )
		{
			QPoint point;
			QMouseEvent *mouseEvent = static_cast<QMouseEvent*>( e );
			point = mouseEvent->pos();

			// Координаты красного правого светофора
			if( point.x() >= 0 && point.x() <= 75 && point.y() >= 42 && point.y() <= 114 )
			{
				set_Svetofor( 1, 0 );
			}
			// Координаты зелёного правого светофора
			if( point.x() >= 0 && point.x() <= 75 && point.y() >= 99 && point.y() <= 200 )
			{
                set_Svetofor( 1, 1 );
			}

			return true;
		}
	}

	return false;
}
// Распечатать таблицу приёмки/отправки грузов
void MyClass::printAutoReception_Slot()
{
    QMessageBox mess;

    if( tableAutoReception->rowCount() == 0 )
	{
        mess.setText( tr("There is no data to print!") );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

		return;
	}

	QSettings settingsLang( "settings.ini", QSettings::IniFormat );
	settingsLang.setIniCodec( "Windows-1251" );
	QString langFromConf;
	settingsLang.beginGroup( "Server" );
	langFromConf = settingsLang.value( "lang", "ua" ).toString();
	settingsLang.endGroup();

	QRect screenResolution = qApp->desktop()->screenGeometry();
	QPrinter printer( QPrinter::HighResolution );
	printer.setOrientation( QPrinter::Landscape );
	QString html;

	webView = new QWebView; // объект для создания отчёта
	webView->setStyleSheet( "background-color: #fff;" );

	QPrintPreviewDialog preview( &printer, this );
	preview.setWindowFlags( Qt::Window );

	if( langFromConf == "ru" )
	{
		preview.setLocale( QLocale::Russian );
	}
	else if( langFromConf == "ua" )
	{
		preview.setLocale( QLocale::Ukrainian );
	}
	else if( langFromConf == "en" )
	{
		preview.setLocale( QLocale::English );
	}

	QObject::connect( &preview, SIGNAL( paintRequested( QPrinter* ) ), webView, SLOT( print( QPrinter* ) ) );

	preview.setModal( true );
	preview.setOrientation( Qt::Horizontal );
	preview.setMinimumSize( screenResolution.width(), screenResolution.height() ); // развернуть на весь размер экрана
	preview.setWindowTitle( tr ( "Preview" ) ); //Предварительный просмотр

	QList<QString> rowHeader;
	int row;
	QString modeAutoReception;

	if( radioButtAutoComeIn->isChecked() == true && radioButtAutoComeOut->isChecked() == false )
	    modeAutoReception = tr("vehicles accepted");
	else if( radioButtAutoComeOut->isChecked() == true && radioButtAutoComeIn->isChecked() == false )
		modeAutoReception = tr("vehicles sended");

	html = QString( "<h2 style='text-align: center;'>") +
		tr("Weight data - ") +
		QString("%1 - %2г.</h2><br /><br /><br />" ).arg( modeAutoReception ).arg( QDate::currentDate().toString( "dd.MM.yyyy" ) )
		.append( " <table border='1'cellspacing='0' cellpadding='4' width=100% style='text-align: center; font-size: 18px; font-weight: bold; border: 3px solid #000; width: 1000px; margin-right: 0px;'>" );

	html.append( "<tr>" );
	for( int h = 0; h < 13 /* reportFormObject->tableAutoReception->columnCount() - 1 */ ; h++ )
	{
		rowHeader.append( tableAutoReception->horizontalHeaderItem( h )->text() );

		html.append( QString( "<th>%1</th>" ).arg( rowHeader.at( h ) ) );
	}
	html.append( "</tr>" );

	QList<QString> rowData;
	for( row = 0; row < tableAutoReception->rowCount(); row++ )
	{
		html.append( "<tr>" );
		for( int c = 0; c < 13 /* reportFormObject->tableAutoReception->columnCount() - 1 */ ; c++ )
	    {
		    rowData.append( tableAutoReception->item( row, c )->text() );
		    html.append( QString( "<td>%1</td>" ).arg( rowData.at( c ) ) );

		}
		rowData.clear();
        html.append( "</tr>" );
	}
	html.append( "</table><br /><br /><br /><br />" );
	html.append( QString( "<span style='text-align: left; font-size: 18px; '><p><b>") +
		tr("Responsible: ") +
		QString("_______________________________________</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>") +
		tr("Date time ") +
		QString("%1 </b></p></span><br /><br /><br /><br /><br /><br />" ).arg( QDateTime().currentDateTime().toString( "dd.MM.yyyy hh:mm:ss" ) ) );
	html.prepend( "<html style=\"background-color: #fff; \"><body>" );
	html.append( "</body></html>" );
	webView->setHtml( html );

	preview.exec();
	webView->deleteLater();
}
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Форма приёмки/отправки автомобилей
void MyClass::show_AcceptionForm() // отобразить форму приёмки/отправки автомобилей
{
	//acceptform->enterTaraManually->hide();
	//if( loginWindowObject->currentNameUser == "ADMIN" ) acceptform->enterTaraManually->show();

	if( !(loginWindowObject->role == loginWindowObject->ADMIN && optionsObj->check(optionsObj->getWasteKey(), "sornost" ) ) )
	{
		acceptform->checkSor->hide();
		acceptform->spinSor->hide();
	}
	int num_nakl;

	acceptform->setHeaderName( settings->currentScales ); // имя операции (отобразить номер весов)

	if( this->rowAutoRecept == -1 )
	{
		mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		num_nakl = mysqlProccessObject->getLastNumNakladnaya();
        acceptform->documentNumber->setText( QString( "%1" ).arg( num_nakl ) );
	}

    acceptform->acceptionFormWindow->show();
}
//
void MyClass::hide_AcceptionForm() // скрыть форму приёмки/отправки автомобилей
{
	acceptform->acceptionFormWindow->hide();
}
// Выбор режима приёмки груза
void MyClass::selectedAcceptingAutoMode()
{
	acceptform->enableDisablePrizepWeight( acceptform->checkBoxPrizepWeightEnable->isChecked() );
	acceptform->enterTaraManually->setChecked( false );
	acceptform->enterTaraManually->setEnabled( false );

	acceptform->enterTaraManuallyPricep->setChecked( false );
	acceptform->enterTaraManuallyPricep->setEnabled( false );

	// Переключиться в режим отправки автомобилей
	radioButtAutoComeOut->setChecked( false );
	radioButtAutoComeIn->setChecked( true );

	acceptform->clearControls(); // очитить контролы формы приёмки
}
// Выбор режима отправки  груза
void MyClass::selectedShippingAutoMode()
{
    acceptform->enableDisablePrizepWeight( acceptform->checkBoxPrizepWeightEnable->isChecked() );

	acceptform->enterTaraManually->setChecked( false );
    acceptform->enterTaraManuallyPricep->setChecked( false );

	// Если выбрано взвешивание автомобиля
	if( acceptform->checkBoxPrizepWeightEnable->isChecked() == false )
	{
		acceptform->enterTaraManually->setEnabled( true );
		acceptform->enterTaraManuallyPricep->setEnabled( false );
	}
	else // иначе - прицеп
	{
		acceptform->enterTaraManually->setEnabled( false );
		acceptform->enterTaraManuallyPricep->setEnabled( true );
	}

	// Переключиться в режим отправки автомобилей
	radioButtAutoComeOut->setChecked( true );
	radioButtAutoComeIn->setChecked( false );

	acceptform->clearControls(); // очитить контролы формы приёмки
}
// Разрешить взвешивание прицепа отдельно
void MyClass::prizepWeightEnable( int state )
{
	// Разрешить/запретить контроллы для взвешивания прицепа отдельно
    acceptform->enableDisablePrizepWeight( ( state == 0 ? false : true ) );

	switch( state )
	{
	    case 0: // unckecked
        {
			acceptform->enterTaraManuallyPricep->setChecked( false );
			acceptform->enterTaraManuallyPricep->setEnabled( false );
		} break;
	    case 2: // ckecked
        {
			acceptform->enterTaraManually->setChecked( false );

		} break;
	}

	acceptform->resetValidation(); // сброс валидации

	acceptform->enterTaraManuallyPricep->setChecked( false );
	acceptform->enterTaraManually->setChecked( false );
}

void MyClass::serialPort0_dataReceived()
{
	QSerialPort *sp = settings->serPortObjectsList.at( 0 );

	// Прочитать текущие настройки COM-прорта  из  Ini-файла
    settings->getCurrentSettingsCOM_PortIniFile( currSettingsComPortPtr, 0 );

	if( sp->bytesAvailable()  )
	{
		parserVP( sp->readAll() );
		groupBoxPlatform->show();
		verifyAutoPosition( weightScales_01.toFloat(), true); 
	}
}

void MyClass::parserVP( QByteArray ba )
{
	#ifdef DEBUG
	    return;
    #endif

	static QString data = "";
	static QByteArray byteData;
	char *startPtr = 0;
	char *endPtr = 0;

	byteData += ba;
	data = byteData;

	QTextCodec *textCodec = QTextCodec::codecForName("Windows-1251");


        //QString str = currSettingsComPortPtr->type_scales;
		int idx = settings->vectorTypesVP_main.at( 0 )->currentIndex();  // определить тип весов, выбранный  в comboBox выбора весов

		float koef = settings->vectorMuxKoef.at( 0 )->text().toFloat();

		switch( idx )
		{
		    case 0: // ВП-05А
			case 6: // со светофором тоже
			{
				if( ( data.contains( '\02' )
					&& data.contains( 'A' )
					&& data.contains( 'O' )
					&& data.contains( 'K' )
					&& data.contains( '4' )
					&& data.contains( '5' )
					&& data.contains( '\03' ) ) )
				{
					if( settings->typesVPList->currentText() != textCodec->toUnicode("ВП-01 (светофор)") ) // тоже окей, но нам не интересен
					{
						if( !zeroBlock ) // тупо но быстро (mess вызывается 1 раз)
						{
							zeroBlock = true;
							emit sayAboutZero_Signal( true );
						}
					}
					data.clear();
					byteData.clear();
					return;
				}
				else if( ( data.contains( '\02' )
					&& data.contains( 'A' )
					&& data.contains( 'N' )
					&& data.contains( 'O' )
					&& data.contains( 'K' )
					&& data.contains( '0' )
					&& data.contains( 'B' )
					&& data.contains( '\03' ) ) )
				{
					if( settings->typesVPList->currentText() != textCodec->toUnicode("ВП-01 (светофор)" )) // тоже окей, но нам не интересен
					{
						if( !zeroBlock ) // тупо но быстро (mess вызывается 1 раз)
						{
							zeroBlock = true;
							emit sayAboutZero_Signal( false );
						}
					}
					data.clear();
					byteData.clear();
					return;
				}
				else if( ( data.contains( '\02' ) // запрос по каждому датчику
					&& data.contains( 'A' )
					&& data.contains( 'W' )
					&& data.contains( ',' )
					&& data.contains( '\03' ) ) )
				{
					// пример ответа
					//02 41 57 2c 30 30 30 33 31 30 32 2c 30 30 31 32 	.AW,0003102,0012
					//37 37 36 2c 30 30 30 38 36 30 33 2c 30 30 32 32 	776,0008603,0022
					//30 30 35 2c 30 30 31 35 33 37 30 2c 30 30 31 35 	005,0015370,0015
					//31 38 30 2c 30 30 32 37 37 38 36 2c 30 30 31 36 	180,0027786,0016
					//37 30 32 2c 30 31 32 31 35 32 34 2c 32 39 03    	702,0121524,29.

					QStringList aw_data = data.split( "," );
					aw.clear();
					if( aw_data.count() < 9 )
					{
						data.clear();
						byteData.clear();
						qDebug() << "serialPort0_dataReceived: parser error";
						return;
					}
					for( int i = 1; i <= 8; i++ ) // всегда ли восемь?
					{
						aw.append( aw_data[i].toInt() );
					}

					data.clear();
					byteData.clear();
				}
				else if( ( data.contains( '\02' ) // запрос по каждому датчику
					&& data.contains( 'A' )
					&& data.contains( 'C' )
					&& data.contains( ',' )
					&& data.contains( '\03' ) ) )
				{
					// пример ответа
					//02 41 43 2c 31 63 34 34 2c 37 34 36 39 2c 34 65 	.AC,1c44,7469,4e
					//36 33 2c 63 38 38 32 2c 38 63 30 62 2c 38 61 35 	63,c882,8c0b,8a5
					//31 2c 66 64 32 64 2c 39 38 33 30 2c 37 37 03    	1,fd2d,9830,77.

					QStringList ac_data = data.split( "," );
					ac.clear();
					if( ac_data.count() < 9 )
					{
						data.clear();
						byteData.clear();
						qDebug() << "serialPort0_dataReceived: parser error";
						return;
					}
					for( int i = 1; i <= 8; i++ ) // всегда ли восемь?
					{
						ac.append( ac_data[i].toInt( 0, 16 ) );
					}
					emit writeSensors_Signal( aw, ac );

					data.clear();
					byteData.clear();
				}
				else if( data.startsWith( '\02' ) && data.endsWith( '\03' ) )
				{
					QByteArray ba = data.toUtf8();
					char *chararray = ba.data();

					startPtr = strchr( chararray, '\02' ) + 4;
					endPtr = strchr( chararray, '\03' ) - 2;

					if( startPtr == 0 || endPtr == 0 )
					{
						data.clear();
						byteData.clear();
						return;
					}

					if( ( *( strchr( chararray, '\02' ) + 2 ) == ' ' || *( strchr( chararray, '\02' ) + 2 ) == 'B' || *( strchr( chararray, '\02' ) + 2 ) == 'n' ) && startPtr != 0x00 && endPtr != 0x00  )
					{
						*( char* )( endPtr ) = 0;

						if( *( chararray + 3 ) == '-' && *( chararray + 4 ) != '-' )
						{
							weightScales_01 = QString( "%1" ).arg( atol( startPtr ) * -1 * koef );
						}
						else
						if( *( chararray + 3 ) == '-' && *( chararray + 4 ) == '-' )
						{
							weightScales_01 = QString( "%1" ).arg( atol( startPtr + 1 ) * -1 * koef );
						}
						else
						{
							weightScales_01 = QString( "%1" ).arg( atol( startPtr ) * koef );
						}

						this->vectorLCD_Indicator.at( 0 )->setText( weightScales_01 );
					}
					data.clear();
					byteData.clear();
				}
				else if( ( data.contains( '\02' ) && data.contains( '\03' ) ) && data.length() > 14 )
				{
					data.clear();
					byteData.clear();
				}

			} break;
			case 1: // esit ART
		    {
              /*  if( data.startsWith( '@' ) || data.startsWith('\xFF') && data.endsWith( '\r' ) )
		        {
			        QByteArray ba = data.toUtf8();
			        char *chararray = ba.data();

					startPtr = strchr( chararray, '@' );
					if( ! startPtr )
						startPtr = strchr(chararray, '\xFF');
			        endPtr = strchr( chararray, '\r' );


					if( startPtr != 0x00 && endPtr != 0x00  )
			        {
						*( unsigned char* )&endPtr = 0;
						if (*(startPtr + 1) == '-')
							koef = 0 - koef;
						weightScales_01 = QString( "%1" ).arg( atol( startPtr + 2 ) * koef );
						this->vectorLCD_Indicator.at(0)->setText(weightScales_01);
					}

			        data.clear();
					byteData.clear();
		        }
		        else if( data.contains( '@' ) || data.contains('\xFF') && data.contains( '\r' ) && data.length() > 20 )
		        {
			        data.clear();
					byteData.clear();
		        }
				else if ( data.length() > 20 )
				{
					data.clear();
					byteData.clear();
				}*/


				if ( byteData.size() >= 16 &&  byteData.contains("\r"))
				{
					QByteArray ba = byteData;
					char *chararray = ba.data();

					static QString debugString; // = chararray;

					bool aChar = false;
					char ourChar = '@';

					char *ptr = NULL;

					QList< QByteArray > ba_1 = byteData.split(0x0d);
					for (int i = 0; i < ba_1.count(); i++)
					{
						if (ba_1.at(i).size() == 7 || ba_1.at(i).size() == 8 )
						{
							if (*(ba_1.at(i).data() + 1) == '-')
								koef = 0 - koef;
							if ( ba_1.at(i).startsWith( '@' ) || ba_1.at(i).startsWith('\xFF') )
							{
								weightScales_01 = QString("%1").arg(atoi(ba_1.at(i).data() + 2) * koef);
								this->vectorLCD_Indicator.at(0)->setText(weightScales_01);
								break;
							}
						}
					}
					ba_1.clear();
					data.clear();
					byteData.clear();
				}
				else if (byteData.size() >= 30)
				{
					data.clear();
					byteData.clear();
				}

			} break;
			case 2: // Элва (CAS 3I)
		    {
				QMap< QString, QString > mapEdIsm;
				mapEdIsm.insert( "GR", "граммы" );
				mapEdIsm.insert( "KG", "кг" );
				mapEdIsm.insert( "TN", "Т" );

                if( ( data.startsWith( "ST" ) || data.startsWith( "US" ) || data.startsWith( "OL" ) ) && data.endsWith( "\r\n" ) )
		        {
					data.replace( '\r', "\0", false );
					data.replace( '\n', "\0", false );

					int length = data.length();
					QStringList list = data.split( ',' );

					if( list.count() < 4 )
						return;

					list.at( 0 ); // режимы работы ( стабильно/нестабильно/перегрузка )
					list.at( 1 ); // режим БРУТТО/НЕТТО ("GS/NT")

					/* Битово-упакованный байт статуса индикаторов состояния весов. Для разных моделей – свой.
                    Бит 0– стабильность, бит 1- блокировка
					*/
					list.at( 2 ); // байт ID индикатора

					QStringList list_weight = list.at( 3 ).split( ' ' ); // поля - вес ( "123.45, -123.45, 12" ) и ед.измерения ( "GR/KG/TN" )
					if( list_weight.count() < 2 )
						return;

					QString temp_weight = list_weight.at( 0 );
					float weight;
					int idx;

					if( temp_weight.contains( "--" ) ) // вес от ВП-10
					{
					    idx = temp_weight.indexOf( '-', 0 );
					    weight = temp_weight.replace( idx, 1, "" ).toFloat();
					}
					// else if( temp_weight.contains( "-" ) ) // вес от ВП-05А
					// {
						// idx = temp_weight.indexOf( '-', 0 );
					    // weight = temp_weight.replace( idx, 1, "" ).toFloat();

					// }
					else
					{

					}
					weightScales_01.sprintf( "%3.3f", temp_weight.toFloat() ); // вес


					labelKg->setText( mapEdIsm.value( list_weight.at( 1 ) ) ); // ед. измерени ( "GR/KG/TN" )

					this->vectorLCD_Indicator.at( 0 )->setText( weightScales_01 );
			        data.clear();
					byteData.clear();
		        }
				else if( ( data.contains( "ST" ) || data.contains( "US" ) || data.contains( "OL" ) ) && data.contains( "\r\n" ) && data.length() > 22 )
		        {
			        data.clear();
					byteData.clear();
		        }

			} break;
			case 3: // XK-3190-A12E(реж.P5 №4)
		    {
                if( data.startsWith( "ww" ) && data.endsWith( "\r\n" ) )
		        {
			        QByteArray ba = data.toUtf8();
			        char *chararray = ba.data();

					startPtr = strchr( chararray, 'w' );
			        endPtr = strchr( chararray, '\r' ) - 2;

					if( startPtr != 0x00 && endPtr != 0x00  )
			        {
						*( unsigned char* )&endPtr = 0;

						if( ba.contains( "." ) == true )
						{
							QString ves = startPtr + 2;
							ves = ves.left(6);

							weightScales_01 = QString::number(ves.toFloat() * koef);
							this->vectorLCD_Indicator.at( 0 )->setText( QString("%1").arg(ves.toFloat() * koef) );
						}
						else
						{
							QString ves = startPtr + 2;
							ves = ves.left(6);

							weightScales_01 = QString::number(ves.toInt() * koef);
							this->vectorLCD_Indicator.at( 0 )->setText( weightScales_01 );
						}
					}
					data.clear();
					byteData.clear();
		        }
				else if( ( data.startsWith( "ww" ) && data.endsWith( "\r\n" ) ) || data.length() > 20 )
		        {
			        data.clear();
					byteData.clear();
		        }

			} break;
			case 4: // Элва (режим 1)
		    {
				static QByteArray ba;
				static short step = 0;

				ba = byteData;

				switch( step )
				{
				    case 0:
					{
						if( ba.contains( 'R' ) )
						{
							ba.clear();
							step++;
						}

						if( ba.length() > 10 )
							ba.clear();

					} break;

					case 1:
					{
						if( ba.startsWith( QByteArray().append( 0xAA ).append( 0x55 ) ) && ba.length() == 13 )
						{
							step = 0;
							unsigned char checksum = 0;
							unsigned char num_decimal_digits = 0;

							for( int i = 0; i < ba.length() - 1; i++ )
							{
								checksum += *( unsigned char* )( ba.data() + i );
							}

							if( *( unsigned char* )( ba.data() + 2 ) != 13 )
							{
								ba.clear();
								step = 0;
								return;
							}

							if( checksum != *( unsigned char* )( ba.data() + ba.length() - 1 ) )
							{
								ba.clear();
								step = 0;
								return;
							}
							// *( unsigned char* )( data.data() + data.length() - 1 ) = 0; // обнуляем контрольную сумму

							// Признак стабильного веса
							if( *( unsigned char* )( ba.data() + 3 ) == 0 )
							{
							    buttonZeroing->setText( "Стабильно" );
								buttonZeroing->setStyleSheet( "color: Green; font-size: 18px;" );
							}
							else // инче - нестабильные показания веса
							{
							    buttonZeroing->setText( "Не стабильно" );
								buttonZeroing->setStyleSheet( "color: Yellow; font-size: 18px;" );
							}

							num_decimal_digits = *( unsigned char* )( ba.data() + 5 );

							if( num_decimal_digits == 3 ) // num_decimal_digits >= 0 && num_decimal_digits <= 3 )
							{
								int weight_int;

								unsigned char buff[ 100 ];

								for( int i = 0; i < 13; i++ )
								{
									buff[ i ] = ba.at( i );
								}

								if( ( buff[ 8 ] & 0x80 ) == 0x80 ) // если знак отрицательный
								{
								    weight_int = ( ( ( int )( buff[ 8 ] << 16 ) | ( int )( buff[ 7 ] << 8 ) | ( int )( buff[ 6 ] ) ) << 8 ) / 256;
									weight_int -= 1 * koef;
									// weight_int != weight_int;

								}
								else // иначе - положительный
								{
								    weight_int = ( int )( buff[ 8 ] << 16 ) | ( int )( buff[ 7 ] << 8 ) | ( int )( buff[ 6 ] );
								    weight_int *= koef;
								}

                                weightScales_01.sprintf( "%3.3f", ( float )weight_int / 1000.0 );
							}
							else
							{
								step = 0;
								ba.clear();
								return;
							}

							labelKg->setText( "т" ); // ед. измерени ( "GR/KG/TN" )

							this->vectorLCD_Indicator.at( 0 )->setText( weightScales_01 );
							ba.clear();
							step = 0;
						}
						else if( ba.contains( QByteArray().append( 0xAA ).append( 0x55 ) ) && ba.length() > 14 )
						{
							ba.clear();
							step = 0;
						}

					} break;
				}

			} break;
			case 5: // БК-01
		    {

			} break;

			case 7: // WE2108
			{
				if( data.length() >= 13 && data.length() <= 20 && data.endsWith( "\r\n" ) )
				{
					bool ok = false;
					short sign = 1;
					if (data.startsWith( "0\r\n" ) )  // Ответ на команду формата COF (типа ОК)
					{
						data = data.mid(3);
					}
					if (data.startsWith( '-' ) )
					{
						sign = -1;
						data.replace('-'," ",false);
					}
					if (data.startsWith( ' ' ) )
					{
						data = data.trimmed();
						int temp_weight = data.toInt(&ok) * sign * koef * 10;
						if ( temp_weight || ok == true )
						{
							weightScales_01 = QString( "%1" ).arg(temp_weight);
							this->vectorLCD_Indicator.at( 0 )->setText(  weightScales_01 );
						}
						else
						{
							data.clear();
							byteData.clear();
						}
					}
					else
					{
						data.clear();
						byteData.clear();
					}
				}
				else
				{
					//if( data.length() < 13 )
					//	data+=sp->readAll();
					//else
						if ( data.length() > 20 )
						{
							data.clear();
							byteData.clear();
						}
				}
			} break;

			case 8: // XK3118T1
				//3d 2d 30 30 31 33 32 30 28 6b 67 29 0d 0a  вроде 4 режим
			{
				if( data.length() >= 14 && data.endsWith( '\n' ) )
				{
					QByteArray ba = data.toUtf8();
					char *chararray = ba.data();

					startPtr = strstr(chararray, "=");
					endPtr = strstr(chararray, "(kg)");
					if ((startPtr != NULL) && (endPtr != NULL))
					{
						startPtr++;
						*(char*)endPtr = '\0';

						qDebug() << startPtr;

						weightScales_01 = QString("%1").arg( atoi(startPtr) * koef );


						//this->vectorLCD_Indicator.at( 0 )->setText( weightScales_01 );

						data.clear();
						byteData.clear();
					}
				}
				else if( data.length() > 29 )
				{
					qDebug() << "clear";
				    data.clear();
					byteData.clear();
				}
			} break;

			case 9: // ESIT_PWI
			{
				if( data.length() <= 20  && data.endsWith( "\r" ) )
				{
					QByteArray ba = byteData;
					char *chararray = ba.data();

					 if(strstr(chararray, "+") != NULL)
					 {
						  startPtr = strstr(chararray, "+");
						endPtr = startPtr + 7;
						*(char*)endPtr = '\0';
							while( *startPtr != 0  )
							{
								if( *startPtr++ > 0x39 )
									break;
							}
							startPtr--;
						*(char*)startPtr -= 0x80;
						weightScales_01 = QString( "%1" ).arg( atoi( strstr(chararray, "+") + 1 ) * 10 * koef );
						this->vectorLCD_Indicator.at( 0 )->setText(  weightScales_01 );
					 }
					 else if(strstr(chararray, "-") != NULL)
					 {
						  startPtr = strstr(chararray, "-");
						endPtr = startPtr + 7;
						*(char*)endPtr = '\0';
						while( *startPtr != 0  )
						{
							if( *startPtr++ > 0x39 )
								break;
						}
						startPtr--;
						*(char*)startPtr -= 0x80;
						weightScales_01 = QString( "%1" ).arg( atoi( strstr(chararray, "-") + 1 ) *-10 * koef );
						this->vectorLCD_Indicator.at( 0 )->setText(  weightScales_01 );
					}
					 else if(ba.count() == 8)
					 {
						ba.remove(0,1);
						ba.remove(ba.count()-1,1);

						int w = ba.toInt();
						weightScales_01 = QString( "%1" ).arg( (int) w * koef );
						this->vectorLCD_Indicator.at( 0 )->setText(  weightScales_01 );
					 }

					data.clear();
					byteData.clear();
				}
				else
				    data.clear();
					byteData.clear();
			} break;

			case 10: // PULSAR
			{
				if( data.length() <= 10 ) // && data.endsWith( "\r\n" ) )
				{
					 QByteArray ba = data.toUtf8();
					 char *chararray = ba.data();

					 endPtr = strstr(chararray, " ");
					 if (endPtr != NULL)
					 {
						*(char*)endPtr = '\0';
						weightScales_01 = QString("%1").arg( (int)( atof(chararray)* 1000 * koef ));
						this->vectorLCD_Indicator.at( 0 )->setText( weightScales_01 );
					 }
					 else
					 {
						data.clear();
						byteData.clear();
					 }
				}
				else
				{
				    data.clear();
					byteData.clear();
				}
			} break;

			case 11: // RiceLake
			{
				if( data.length() <= 20 ) // && data.endsWith( "\r\n" ) )
				{
					 QByteArray ba = data.toUtf8();
					 char *chararray = ba.data();

					 startPtr = strstr(chararray, "-");
					 endPtr = strstr(chararray, "kg");
					 if (endPtr != NULL)
						 if (startPtr != NULL)
						 {
							*(char*)endPtr = '\0';
							weightScales_01 = QString("%1").arg(startPtr + 1);
							weightScales_01.trimmed();
							weightScales_01 = QString("%1").arg( -atoi( weightScales_01 ) * koef );
							this->vectorLCD_Indicator.at( 0 )->setText(  weightScales_01 );
						 }
						 else
						 {
							*(char*)endPtr = '\0';
							weightScales_01 = QString("%1").arg(chararray);
							weightScales_01.trimmed();
							weightScales_01 = QString("%1").arg( atoi( weightScales_01 ) * koef );
							this->vectorLCD_Indicator.at( 0 )->setText(  weightScales_01 );
						 }
					 else
					 {
						data.clear();
						byteData.clear();
					 }
				}
				else
				{
				    data.clear();
					byteData.clear();
				}
			} break;

		 ///////////////////////////////////

			case 12: //AXIS SE01
			{
				if( data.length() == 16 /*&& data.endsWith( '\0A' )*/ )
				{
					QByteArray ba = data.toUtf8();

					//Byte 1 - sign or space
					//Byte 2 - space
					//Byte 3÷4 - digit or space
					//Byte 5÷9 - digit, decimal point or space
					//Byte 10 - digit
					//Byte 11 - space
					//Byte 12 - k, l, c, p or space
					//Byte 13 - g, b, t, c or %
					//Byte 14 - space
					//Byte 15 - CR
					//Byte 16 - LF

					QList<QByteArray> data_split = ba.split( ' ' );
					double weight = atof( data_split.at( data_split.count()-3 ).data() ) ;
					weightScales_01 = QString( "%1" ).arg( weight * 1000 );

					this->vectorLCD_Indicator.at( 0 )->setText( weightScales_01 );
					data.clear();
					byteData.clear();
				}
				else if( data.length() > 16 )
				{
					data.clear();
					byteData.clear();
				}

			} break;
			case 13: // какой-то есит
		    {
                if( data.startsWith( 'G' ) && data.endsWith( '\r' ) )
		        {
			        QByteArray ba = data.toUtf8();
			        char *chararray = ba.data();

					startPtr = strchr( chararray, 'G' );
			        endPtr = strchr( chararray, '\r' );

					if( startPtr != 0x00 && endPtr != 0x00  )
			        {
						*( unsigned char* )&endPtr = 0;
						char znak = *(startPtr + 1);
						weightScales_01 = QString( "%1" ).arg( atol( startPtr + 2 ) * koef *
							( znak == '-' ? -1 : 1 ) );

					}
		            this->vectorLCD_Indicator.at( 0 )->setText( weightScales_01 );
			        data.clear();
					byteData.clear();
		        }
		        else if( data.contains( 'G' ) && data.contains( '\r' ) && data.length() > 20 )
		        {
			        data.clear();
					byteData.clear();
		        }

			} break;
			case 14: // T7E
		    {
				//QString data = "=00.0050"; //=00.005-
				const int size = 7;
				if( data.startsWith( '=' ) && data.length() == size+1 )
		        {
					// result = reverse data string
					char result[size];
					for( int i = 0; i < size; i++ )
					{
						result[i] = data[size-i].toAscii();
					}
					int weight = atol( result );

					weightScales_01 = QString::number( weight * koef );

		            this->vectorLCD_Indicator.at( 0 )->setText( weightScales_01 );
			        data.clear();
					byteData.clear();
		        }
		        else if( data.length() > 9 )
		        {
			        data.clear();
					byteData.clear();
		        }
			} break;


			case 15: // SMART ABS (F9)
			{
				if( data.length() >= 8 && data.endsWith( "\r" ) )
				{
					data.replace( '\r', "" );

					int temp_weight = data.toInt();
					weightScales_01 = QString("%1").arg( temp_weight );
					this->vectorLCD_Indicator.at( 0 )->setText(  weightScales_01 );
				    data.clear();
					byteData.clear();
				}
				else if( data.length() >= 10 && data.contains( "\r" ) )
				{
					data.clear();
					byteData.clear();
				}

			} break;

			case 16: // SMART ABS (без запроса)
			//02 2d 30 30 30 30 30 36 30 4b 47 20
			//02 20 30 30 30 30 31 31 30 4b 47 20
			{
				if( data.startsWith("\0x2") && data.length() >= 12 && data.endsWith( " " ) )
				{
					int znak = 1;
					data.remove(0, 1);
					if (data.startsWith( '-' ) )
					{
						znak = -1;
						data.remove(0, 1);
					}
					else
						if (data.startsWith( ' ' ) )
						{
							znak = 1;
							data.remove(0, 1);
						}


					data.truncate( 7 );
					bool ok;
					int temp_weight = data.toInt(&ok) * znak;
					if(ok)
						weightScales_01 = QString("%1").arg( temp_weight );
					this->vectorLCD_Indicator.at( 0 )->setText(  weightScales_01 );
				    data.clear();
					byteData.clear();
				}
				else if( data.length() >= 30 )
				{
					data.clear();
					byteData.clear();
				}

			} break;

			//40 2b 30 30 30 31 32 33 0d    -
			case 17: // BK05
			{
				if( data.startsWith( '@' ) && data.endsWith( '\r' ) )
		        {
			        QByteArray ba = data.toUtf8();
			        char *chararray = ba.data();

					startPtr = strchr( chararray, '@' );
			        endPtr = strchr( chararray, '\r' );

					if( startPtr != 0x00 && endPtr != 0x00  )
			        {
						*( unsigned char* )&endPtr = 0;
						char znak = *(startPtr + 1);
						weightScales_01 = QString( "%1" ).arg( atol( startPtr + 2 ) * koef * ( znak == '-' ? -1 : 1 ) );
					}
		            this->vectorLCD_Indicator.at( 0 )->setText( weightScales_01 );
			        data.clear();
					byteData.clear();
		        }
		        else if( data.length() > 20 )
		        {
			        data.clear();
					byteData.clear();
		        }
			} break;

			case 18: // ESIT_PWI (с точкой)
			{
				if( /*byteData.size() >= 16 && */ byteData.contains( "\r" ) )
				{
					QByteArray ba = byteData;
					char *chararray = ba.data();

					static QString debugString; // = chararray;

					bool aChar = false;
					char ourChar = '@';

					char *ptr = NULL;

					QList< QByteArray > ba_1 = byteData.split( 0x0d );
					for( int i = 0; i < ba_1.count(); i++ )
					{
						if( ba_1.at( i ).size() >= 7 )
						{
							weightScales_01 = QString("%1").arg( atoi( ba_1.at( i ).data() + 1 ) * koef );
							this->vectorLCD_Indicator.at( 0 )->setText(  weightScales_01 );
						}
					}
					byteData.clear();
				}
				else if( byteData.size() >= 30 )
				{
				    data.clear();
					byteData.clear();
				}
			} break;

			case 19: // WE2107(без запроса)
			{
				if( data.startsWith( '\02' ) && data.endsWith( '\n' ) )
				{
					QByteArray ba = data.toUtf8();
					ba[7] = 0;
					char *chararray = ba.data();

					weightScales_01 = QString( "%1" ).arg( atoi( chararray+1 ) * koef );
					this->vectorLCD_Indicator.at( 0 )->setText( weightScales_01 );

					data.clear();
					byteData.clear();
				}
				else if( ( data.contains( '\02' ) && data.contains( '\n' ) ) && data.length() > 21 )
				{
					data.clear();
					byteData.clear();
				}
			} break;

			case 20: // WE2110(без запроса)
			{
				if( data.count() == 8 )
				{
					weightScales_01 = QString::number( data.toInt() * koef );
					this->vectorLCD_Indicator.at( 0 )->setText( weightScales_01 );

					data.clear();
					byteData.clear();
				}
				else if( data.count() > 8 )
				{
					data.clear();
					byteData.clear();
				}
			} break;

			case 21: // D12
				//01 03 08 30 30 30 30 30 37 34 30 4b 2e
			{
				if( data.count() == 13 )
				{
					if( data[1] == 0x03 ) // ves
					{
						QString ves = data.mid( 3, 7 );
						weightScales_01 = QString::number( ves.toInt() * koef );
						this->vectorLCD_Indicator.at( 0 )->setText( weightScales_01 );
					}
					data.clear();
					byteData.clear();
				}
				else if( data.count() > 13 )
				{
					data.clear();
					byteData.clear();
				}
			} break;

			case 22: // Техноваги
				//R01W000.000d0a //01 - адрес
		    {
                if( data.startsWith( 'R' ) && data.endsWith( "\r\n" ) )
		        {
			        QByteArray ba = data.toUtf8();
			        char *chararray = ba.data();

					startPtr = strchr( chararray, 'R' ) + 4;
			      endPtr = strstr( chararray, "\r\n" );
					//endPtr = startPtr + 6;

					if( startPtr != 0x00 && endPtr != 0x00  )
			        {
						*endPtr = 0;
						//char znak = *(startPtr + 1);  ??
						if(strchr(startPtr,'.') != NULL)
							startPtr[strchr(startPtr,'.') - startPtr] = ',';
						weightScales_01 = QString::number( (double)atof( startPtr ) * 1000 * koef, 'f', 0);
							//( znak == '-' ? -1 : 1 ) );

					}
		            this->vectorLCD_Indicator.at( 0 )->setText( weightScales_01 );
			        data.clear();
					byteData.clear();
		        }
		        else if( data.contains( 'R' ) && data.contains( "\r\n" ) && data.length() > 12 )
		        {
			        data.clear();
					byteData.clear();
		        }

			} break;

			case 23: // Мика КВ-1
		    {
				if( data.endsWith( '\r' ) && data.size() >= 7 )
		        {
			        QByteArray ba = data.toUtf8();
			        char *chararray = ba.data();


			        endPtr = strchr( chararray, '\r' );
					startPtr = endPtr - 6;

					if( startPtr != 0x00 && endPtr != 0x00  )
			        {
						*( unsigned char* )&endPtr = 0;
						weightScales_01 = QString( "%1" ).arg( atol( startPtr ) * koef );
					}
		            this->vectorLCD_Indicator.at( 0 )->setText( weightScales_01 );
			        data.clear();
					byteData.clear();
		        }
		        else if( data.contains( '\r' ) && data.length() > 20 )
		        {
			        data.clear();
					byteData.clear();
		        }

			} break;
			case 24: // Zemic A9 (без запроса, tf0) // шлет вес на 10 больше (koef д.б. 0.1)
			//02 2d 30 30 30 30 30 36 30 4b 47 20
			//02 20 30 30 30 30 31 31 30 4b 47 20
			{
				if( data.startsWith("\0x2") && data.length() >= 12 && data.endsWith( "\0x3" ) )
				{
					int znak = 1;
					data.remove(0, 1);
					if (data.startsWith( '-' ) )
					{
						znak = -1;
						data.remove(0, 1);
					}
					else
						if (data.startsWith( ' ' ) || data.startsWith( '+' ))
						{
							znak = 1;
							data.remove(0, 1);
						}


					data.truncate( 7 );
					bool ok;
					int temp_weight = data.toInt(&ok) * znak * koef;
					if(ok)
						weightScales_01 = QString("%1").arg( temp_weight );
					this->vectorLCD_Indicator.at( 0 )->setText(  weightScales_01 );
				    data.clear();
					byteData.clear();
				}
				else if( data.length() >= 30 )
				{
					data.clear();
					byteData.clear();
				}

			} break;

			case 25: //  Keli D39 (реж2)
			//02 2b 30 30 30 30 30 30 30 31 42 03
			
			{
				if((data.startsWith("\0x2") || data.startsWith(' '))  && data.length() >= 12 && data.endsWith( "\0x3" ) )
				{
					int znak = 1;
					data.remove(0, 1);
					if (data.startsWith( '-' ) || data.startsWith(' ') )
					{
						znak = -1;
						data.remove(0, 1);
					}
					else
						if (data.startsWith( '+' ) )
						{
							znak = 1;
							data.remove(0, 1);
						}


					data.truncate( 7 );
					bool ok;
					int temp_weight = data.toInt(&ok) * znak * koef;
					if(ok)
						weightScales_01 = QString("%1").arg( temp_weight );
					this->vectorLCD_Indicator.at( 0 )->setText(  weightScales_01 );
				    data.clear();
					byteData.clear();
				}
				else if( data.length() >= 30 )
				{
					data.clear();
					byteData.clear();
				}

			} break;

			case 26: // XK3118T1  с дробной частью
				//3d 2d 30 30 31 33 32 30 28 6b 67 29 0d 0a  вроде 4 режим
				{
				 qDebug()<< data;
				if( data.length() >= 14 && data.endsWith( '\n' ) )
				{
					QByteArray ba = data.toUtf8();
					char *chararray = ba.data();

					startPtr = strstr(chararray, "=");
					endPtr = strstr(chararray, "(kg)");
					if ((startPtr != NULL) && (endPtr != NULL))
					{
						startPtr++;
						*(char*)endPtr = '\0';

						QString ves_str = startPtr;
					    weightScales_01 = ves_str.remove( QRegExp("^[0]*") );
                        qDebug() << weightScales_01;
						//weightScales_01 = QString("%1").arg( atoi(startPtr) * koef );
						//this->vectorLCD_Indicator.at( 0 )->setText( weightScales_01 );

						data.clear();
						byteData.clear();
					}
				}
				else if( data.length() > 29 )
				{
					qDebug() << "clear";
				    data.clear();
					byteData.clear();
				}
				} break;

		}

		if( settings->group_Sornost->isChecked() && radioButtAutoComeIn->isChecked() )
		{
			int w = weightScales_01.toInt();
			double koef = settings->spinKoefSor->value();

			if( isDiscret )
			{
				weightScales_sor = roundTo( (w * koef), 20 );
			}
			else
			{
				weightScales_sor = w * koef;
			}

			this->vectorLCD_Indicator.at( 0 )->setText( QString::number( weightScales_sor ) );

			if( settings->checkSorWeightShow->isChecked() )
			{
				tabScalesIndicator->show();
				labelKg->show();
			}
			else
			{
				tabScalesIndicator->hide();
				labelKg->hide();
			}
		}
		else
		{
			weightScales_sor = weightScales_01.toInt();
			this->vectorLCD_Indicator.at( 0 )->setText( weightScales_01 );
		}

		flagsNoUarts_connection[ 0 ] = true;
		vectorFlagsNoUARTs_Connection.insert( 0, &flagsNoUarts_connection[ 0 ] ); // установить флаг наличия связи ВП с ПК
		timeoutsNoUartConnection[ 0 ] = 0;

	emit setTaraWeight_Signal( ( int )weightScales_01.toDouble() );

	if( settings->groupTraffLight1->isChecked() || settings->groupTraffLight2->isChecked() )
	{
		QString w = weightScales_01;
		if( settings->group_Sornost->isChecked() ) w = QString::number( weightScales_sor );

		if( stateHit == 1 && w.toInt() > settings->enterMinWeightTablo->text().toInt() )
		{
			stateHit = 2;
			tabloTraff = 0;
			tabloColor = 0;
			QTimer::singleShot( settings->enterTimerTablo->text().toInt() * 1000, this, SLOT( tabloStateTimer_Slot() ) );
		}

		if( stateHit == 3 )
		{
			tabloTraff = 0;
			tabloColor = 1;
		}

		if( stateHit == 4 )
		{
			tabloTraff = 1;
			tabloColor = 1;
		}

		if( w.toInt() < 40 )
		{
			tabloColor = 1;
			tabloTraff = 1;
			stateHit = 1;
		}

		QString tabloWeight = w;
		while(tabloWeight.count() < 6) tabloWeight.push_front(' ');

		if( !settings->checkSorWeightShow->isChecked() )
		{
			tabloWeight = "      ";
		}

		QByteArray tabloSend;
		tabloSend.append( QString::number( tabloTraff ) );
		tabloSend.append( ' ' );
		tabloSend.append( tabloWeight );
		tabloSend.append( ' ' );
		tabloSend.append( QString::number( tabloColor ) );
		tabloSend.append( '\r' );

		tabloData = tabloSend;
		/*
		static int time_to_send = 0;  // разрежаем посылки на табло
		time_to_send++;
		if ( time_to_send == 4 )
		{
			if( settings->groupTraffLight1->isChecked() )
			{
				if( settings->radioTablo_COM1->isChecked() )
				{
					serialPortClassObject->writeData2Port( settings->serialPortTablo1, tabloSend, tabloSend.length() );
				}
				else
				{
					tcpSocket_Tablo1->write( tabloSend );
				}
			}

			if( settings->groupTraffLight2->isChecked() )
			{
				if( settings->radioTablo_COM2->isChecked() )
				{
					serialPortClassObject->writeData2Port( settings->serialPortTablo2, tabloSend, tabloSend.length() );
				}
				else
				{
					tcpSocket_Tablo2->write( tabloSend );
				}
			}

			time_to_send = 0;
		}
		*/
	}
	else if( settings->typesVPList->currentText() == textCodec->toUnicode("ВП-01 (светофор)") )
	{
		if( stateHit == 1 && weightScales_01.toInt() > settings->enterMinWeightTablo->text().toInt() )
		{
			stateHit = 2;
			tabloTraff = 0;
			tabloColor = 0;
			QTimer::singleShot( settings->enterTimerTablo->text().toInt() * 1000, this, SLOT( tabloStateTimer_Slot() ) );
		}

		if( stateHit == 3 )
		{
			tabloTraff = 0;
			tabloColor = 1;
		}

		if( stateHit == 4 )
		{
			tabloTraff = 1;
			tabloColor = 1;
		}

		if( weightScales_01.toInt() < 40 )
		{
			tabloColor = 1;
			tabloTraff = 1;
			stateHit = 1;
		}

		QString tabloWeight = weightScales_01;
		while(tabloWeight.count() < 6) tabloWeight.push_front(' ');

		QByteArray tabloSend;
		tabloSend.append( 'A' );
		tabloSend.append( 'L' );
		tabloSend.append( tabloTraff == 0 ? 'R' : 'G' );
		tabloSend.append( tabloColor == 0 ? 'R' : 'G' );
		tabloSend.append( tabloWeight );

		unsigned char checksum = 0; // считается неправильно, но один фиг прибор не проверяет
		for( int i = 0; i < tabloSend.count(); i++ )
		{
			checksum ^= tabloSend[i];
		}
		tabloSend.append( checksum );
		tabloSend.push_front( '\02' );
		tabloSend.push_back( '\03' );

		VP01_svetofor_request = tabloSend;
	}
	qDebug()<<settings->enterMinWeightTablo->text();
	if( hiddenWeighing->hiddenWeightEnable )
	{
		// скрытые взвешивания
		int workinWeight = vectorLCD_Indicator.at( 0 )->text().toInt();

		if( workinWeight > settings->enterMinWeightTablo->text().toInt() ) // todo вес берем как с табло
		{
			emit onBoard_Signal( workinWeight ); // сигналим о заезде на платформу
		}
		else if( workinWeight < 41 ) // 2 дискрета
		{
			emit offBoard_Signal(); // о сьезде
		}
	}
}

double MyClass::roundTo( int value, int div )
{
	if( ( value % div ) > ( div / 2.0 ) )
	{
		return ( value / div + 1 ) * div;
	}
	return ( value / div ) * div;
}

void MyClass::tablo_timer_Slot()
{
	QByteArray tabloSend = tabloData;
	if( settings->groupTraffLight1->isChecked() )
	{
		if( settings->radioTablo_COM1->isChecked() )
		{
			serialPortClassObject->writeData2Port( settings->serialPortTablo1, tabloSend, tabloSend.length() );
		}
		else
		{
			tcpSocket_Tablo1->write( tabloSend );
		}
	}

	if( settings->groupTraffLight2->isChecked() )
	{
		if( settings->radioTablo_COM2->isChecked() )
		{
			serialPortClassObject->writeData2Port( settings->serialPortTablo2, tabloSend, tabloSend.length() );
		}
		else
		{
			tcpSocket_Tablo2->write( tabloSend );
		}
	}

	//serialPortClassObject->writeData2Port( settings->serialPort, VP01_svetofor_request, VP01_svetofor_request.length() );
}

// Обработчик принятых данных по COM-порту
void MyClass::serialPort1_dataReceived()
{
	static QString data = "";
	const char *startPtr;
	const char *endPtr;

	QSerialPort *sp = settings->serPortObjectsList.at( 1 );


	if( sp->bytesAvailable() )
	{
		if( data.startsWith( '\02' ) && data.endsWith( '\03' ) )
		{
			QByteArray ba = data.toUtf8();
			char *charray = ba.data();

			startPtr = strchr( charray, '\02' ) + 4;
			endPtr = strchr( charray, '\03' ) - 2;

			if( startPtr != 0x00 && endPtr != 0x00  )
			{
			    *( char* )( endPtr ) = 0;

			    weightScales_02 = atol( startPtr );
			    //this->vectorLCD_Indicator.at( 1 )->setText( weightScales_02 );
			}
            data = "";
		}
		else if( data.contains( '\02' ) && data.contains( '\03' ) && data.length() > 12 )
		{
			data.clear();
		}
		else
			data += sp->readAll();
	}
}
// Обработчик принятых данных по COM-порту
void MyClass::serialPort2_dataReceived()
{
	static QString data = "";
	const char *startPtr;
	const char *endPtr;

	QSerialPort *sp = settings->serPortObjectsList.at( 2 );

	if( sp->bytesAvailable()  )
	{
		if( data.startsWith( '\02' ) && data.endsWith( '\03' ) )
		{
			QByteArray ba = data.toUtf8();
			char *charray = ba.data();

			startPtr = strchr( charray, '\02' ) + 4;
			endPtr = strchr( charray, '\03' ) - 2;

			if( startPtr != 0x00 && endPtr != 0x00  )
			{
			    *( char* )( endPtr ) = 0;

			    weightScales_03 = atol( startPtr );
			    //this->vectorLCD_Indicator.at( 2 )->setText( weightScales_03 );
			}
            data = "";
		}
		else if( data.contains( '\02' ) && data.contains( '\03' ) )
		{
			data.clear();
		}
		else
			data += sp->readAll();
	}
}
// Обработчик принятых данных по COM-порту
void MyClass::serialPort3_dataReceived()
{
	static QString data = "";
	const char *startPtr;
	const char *endPtr;

	QSerialPort *sp = settings->serPortObjectsList.at( 3 );

	if( sp->bytesAvailable() )
	{
		if( data.startsWith( '\02' ) && data.endsWith( '\03' ) )
		{
			QByteArray ba = data.toUtf8();
			char *charray = ba.data();

			startPtr = strchr( charray, '\02' ) + 4;
			endPtr = strchr( charray, '\03' ) - 2;

			if( startPtr != 0x00 && endPtr != 0x00  )
			{
			    *( char* )( endPtr ) = 0;

			    weightScales_04 = atol( startPtr );
			    //this->vectorLCD_Indicator.at( 3 )->setText( weightScales_04 );
			}
            data = "";
		}
		else if( data.contains( '\02' ) && data.contains( '\03' ) )
		{
			data.clear();
		}
		else
			data += sp->readAll();
	}
}
// Обработчик принятых данных по COM-порту
void MyClass::serialPort4_dataReceived()
{
	int A = 10000;
	// QString str =
	// settings->vectorComPortsVP.at( settings->currentScales );
}
// Обработчик принятых данных по COM-порту
void MyClass::serialPort5_dataReceived()
{
	int A = 10000;
}
// Обработчик принятых данных по COM-порту
void MyClass::serialPort6_dataReceived()
{
	int A = 10000;
}

// Обработчик принятых данных по COM-порту
void MyClass::serialPort7_dataReceived()
{
	int A = 10000;
}
// Обработчик таймерного события запросов в COM-порт
void MyClass::timerRequestCOM_Port_elapsed()
{
    int p, cntPorts;
	QString request; // запрос в ВП
    static int cnt_params = 0;

    if( vector_type_command.count() == 0 )
	{
	    cntPorts = settings->serPortObjectsList.length();

		for( p = 0; p < cntPorts; p++ )
		{
			// Прочитать текущие настройки COM-прорта  из  Ini-файла
			settings->getCurrentSettingsCOM_PortIniFile( currSettingsComPortPtr, p );

			// Если БК-01 ещё не инициализированна, то производим её инициализацию
			if( currSettingsComPortPtr->bk == true && settings->initialised_bk01 == false )
			{
				QByteArray bytes;
				QString request;
				int idx, type_device;

				switch( cnt_params )
				{
					case 0: // тип ВП-xx
					{
						QList<int> typesVP = settings->load_BK_VP_REquests();

						idx = settings->vectorTypesVP_BK.at( p )->currentIndex();  // определить тип весов, выбранный  в comboBox выбора весов
						type_device = typesVP.at( idx ); // выбор каомманды для запроса в ВП,  для его передачи

						request = QString( "P 1 %1" ).arg( type_device );
						bytes = request.toAscii(); // toUtf8(); // преобразовать QString в char*

					} break;
					case 1: // фильтр
					{
						request = QString( "P 0 %1" ).arg( settings->vectorInputDelay_BK.at( p )->text().toInt() );
						bytes = request.toAscii(); // toUtf8(); // преобразовать QString в char*

						// cnt_params = 0;
					} break;
				}

				QByteArray ba = settings->hashTypesEndsOfFrames.value( currSettingsComPortPtr->endoframe );

				if( currSettingsComPortPtr->rm == true ) // если выбрана работа через РМ
					bytes.prepend( "R" );

				char *reqChars = bytes.append( ba ).data();
				serialPortClassObject->writeData2Port( settings->serPortObjectsList.at( p ), bytes, bytes.length() );
			}
			else // Если разрешена  работа  экземпляра  весов
			if( currSettingsComPortPtr->scales_enable == true )
			{
				int index = settings->vectorTypesVP_main.at( 0 )->currentIndex();  // определить тип весов, выбранный  в comboBox выбора весов

				if( index == 2 ) // если выбранны весы "Элва CAS I3", то перестаём делать опрос весов - весы сами посылают вес
					return;

				if( index == 4 ) // если выбранны весы "Элва (протокол №1)"
				{
                    static short num_step = 0;

					switch( num_step )
					{
					    case 0:
						{
							serialPortClassObject->writeData2Port( settings->serPortObjectsList.at( p ), QByteArray().append( "\01" ), 1 );
							num_step++;

						} break;
						case 1:
						{
							serialPortClassObject->writeData2Port( settings->serPortObjectsList.at( p ), QByteArray().append( 97 ), 1 ); // 97 - hex = 0x61
							num_step = 0;

						} break;
					}
				}
				else // запросы в ВП
				{
					if( settings->vectorcheckBoxUSB_En.at( 0 )->isChecked() )
					{
						if( settings->serPortObjectsList.at( settings->currentScales )->isOpen() == true )
						{
							settings->serPortObjectsList.at( settings->currentScales )->close();
						}
						settings->serPortObjectsList.at( settings->currentScales )->open( QIODevice::ReadWrite );
					}

					if( index == 0 ) // для опроса датчиков ВП-01
					{
						QByteArray requestData;

						static int askSec = 1;
						if( askSec == ( secondsAW * 4 - 5 ) ) // INTERVAL_REQUEST , запрос раз в секунду, TODO переписать с переменными
						{
							requestData = "\2AW16\3";
							askSec = -1;
						}
						else if( askSec == -1 ) // просто фантазии не хватило
						{
							requestData = "\2AC04\3";
							askSec = 1;
						}
						else
						{
							requestData = "\2AB03\3";
							askSec++;
						}

						serialPortClassObject->writeData2Port( settings->serPortObjectsList.at( p ), requestData, requestData.count() );
						return;
					}

					//////////// Добавленные протоколы из БК ///////////////////////
					if (index == 7) // если выбраны весы WE2108, устанавливаем формат
					{
						serialPortClassObject->writeData2Port( settings->serPortObjectsList.at( p ), QByteArray().append("COF3\n"), 6 );
					}
					///////////////////////////////////////////////////////////////

					QString str = currSettingsComPortPtr->type_scales;
					int idx = settings->vectorTypesVP_main.at( p )->currentIndex();  // определить тип весов, выбранный  в comboBox выбора весов
					request = settings->hashTypesVP_RequestNum.key( idx ); // выбор комманды для запроса в ВП, для его передачи

					QByteArray bytes = request.toAscii();
					QByteArray ba = settings->hashTypesEndsOfFrames.value( currSettingsComPortPtr->endoframe );

					// UDP
					if( settings->vectorGroupUDP_Settings.at( p )->isChecked() )
					{
						if( flag_UDP_listening_state == true )
						{
		                    qint64 len = udp_socket->writeDatagram( QByteArray().append( "AB" ), strlen( "AB" ),
							QHostAddress( settings->vectorEnterIP_UDP_Settings.at( p )->text() ), settings->vectorEnterDstPort_UDP_Settings.at( p )->text().toInt() );
						}

					    return;
					}

					if( currSettingsComPortPtr->rm == true ) // если выбрана работа через РМ
						bytes.prepend( "R" );

					char *reqChars = bytes.append( ba ).data();

					if( idx == 21 ) // в d12 есть нуль символы, строка не годится
					{
						QByteArray d12;
						d12.append( 0x01 );
						d12.append( 0x03 );
						d12.append( (char)0x00 );
						d12.append( 0x03 );
						d12.append( (char)0x00 );
						d12.append( 0x04 );
						d12.append( 0xB4 );
						d12.append( 0x09 );
						serialPortClassObject->writeData2Port( settings->serPortObjectsList.at( p ), d12, d12.count() );
					}
					else
					{
						serialPortClassObject->writeData2Port( settings->serPortObjectsList.at( p ), bytes, bytes.length() );
					}
				}
			}
		}
		//
		if( cnt_params < NUM_BK_01_PARAMS && settings->initialised_bk01 == false )
		{
			++cnt_params;
		}
		else if( cnt_params >= NUM_BK_01_PARAMS )
		{
			settings->initialised_bk01 = true;
			cnt_params = 0;
		}
	}
}

//
void MyClass::UDP_DataAvalable_Slot()
{
	static QString data = "";
	const char *startPtr;
	const char *endPtr;

	if( udp_socket->pendingDatagramSize() > 0 )
	{
		char buffer[ 1024 ];
		memset( buffer, 0x00, sizeof( buffer ) );

		int len = udp_socket->readDatagram( buffer, 1024 ); // readAll();

		if( len >= 4 )
		{
			char weight[ 16 ];
			memset( weight, 0x00, sizeof( weight ) );

			QString weight_str;

			float koef = settings->vectorMuxKoef.at( 0 )->text().toFloat();

			char* ptr = strstr( buffer, "AB" );
			char* ptr_space = strstr( ptr + 2, " " );

			if( ptr != 0 && ptr_space != 0 )
			{
				strcpy( weight, ptr + 3 );
				weight_str = QString( "%1" ).arg( atoi( weight ) * koef );

				this->vectorLCD_Indicator.at( 0 )->setText( weight_str );
				weightScales_01 = weight_str;

				QByteArray ba = weight_str.toUtf8();
				char *weight_debug = ba.data();
				qDebug( weight_debug );
			}

			flagsNoUarts_connection[ 0 ] = true;
		    vectorFlagsNoUARTs_Connection.insert( 0, &flagsNoUarts_connection[ 0 ] ); // установить флаг наличия связи ВП с ПК
		    timeoutsNoUartConnection[ 0 ] = 0;
		}
	}

	/*
    if( udp_socket->pendingDatagramSize() > 0 )
	{
		char buffer[ 1024 ];
		memset( buffer, 0x00, sizeof( buffer ) );

		int len = udp_socket->readDatagram( buffer, 1024 ); // readAll();

		if( len >= 14 )
		{
			char weight[ 16 ];
			memset( weight, 0x00, sizeof( weight ) );

			QString weight_str;

			float koef = settings->vectorMuxKoef.at( 0 )->text().toFloat();

			char* ptr = strstr( buffer, "AB" );
			char* ptr_space = strstr( ptr + 4, " " );

			memcpy( weight, ptr + 3, ptr_space - ptr - 3 );
			weight_str = QString( "%1" ).arg( atoi( weight ) * koef );

			this->vectorLCD_Indicator.at( 0 )->setText( weight_str );
			weightScales_01 = weight_str;

			QByteArray ba = weight_str.toUtf8();
			char *weight_debug = ba.data();
			qDebug( weight_debug );

			flagsNoUarts_connection[ 0 ] = true;
		    vectorFlagsNoUARTs_Connection.insert( 0, &flagsNoUarts_connection[ 0 ] ); // установить флаг наличия связи ВП с ПК
		    timeoutsNoUartConnection[ 0 ] = 0;
		}
	}
	*/

}

//
void MyClass::timer_sysTick_elapsed() // создать  обработчик системного таймера
{
    static bool flagActivity = false;

	if( settings->getServerFlagActivity() == true ) // если Сервер  активен
	{
        if( flagActivity == false )
		{
			bool statusOpen;
            int cntPorts, p, statusPort;
			QString com; // имя COM-порта для открытия

            int cnt_port = 0;
		    static bool initialised_bk01 = false; // флаг инициализации БК-01
            static int cnt_params = 0;

            cntPorts = settings->serPortObjectsList.length();

			for( p = 0; p < cntPorts; p++ )
			{
				// Прочитать текущие настройки COM-прорта  из  Ini-файла
                settings->getCurrentSettingsCOM_PortIniFile( currSettingsComPortPtr, p );
				//
				if( currSettingsComPortPtr->scales_enable == true ) // если разрешена  работа  экземпляра  весов,  то  открыть  экземпляр COM-порта
				{
					if( !settings->vectorGroupUDP_Settings.at( p )->isChecked() && !settings->group_TCPWeight->isChecked() )
					{
						statusOpen = serialPortClassObject->OpenSerialPort( settings->serPortObjectsList.at( p ), currSettingsComPortPtr->com, currSettingsComPortPtr->baud.toInt() ); // массив  объектов для открытия COM-порта
						QSerialPort *sp = settings->serPortObjectsList.at( p );
						QString str = sp->objectName();

						if( str == "serialPort 0" && sp->isOpen() == true ) // COM №1
						{
							QObject::connect( sp, SIGNAL( readyRead() ), this , SLOT( serialPort0_dataReceived() ) );
						}
					}

					if( settings->vectorGroupUDP_Settings.at( p )->isChecked() == true )
					{
						if( udp_socket != 0 )
						{
							udp_socket->close();
							bool flag = udp_socket->bind( QHostAddress::Any, settings->vectorEnterSrcPort_UDP_Settings.at( settings->currentScales )->text().toInt() );
								flag_UDP_listening_state = true;
						}
					}
				}
			}

			// Запись лога
			mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
			mysqlProccessObject->saveEventToLog( tr("Start server") );
            flagActivity = true;
		}

		if( !settings->group_TCPWeight->isChecked() ) startServerProcess( flagActivity );
	}
	else
	{
	    if( flagActivity == true ) // если Сервер запущен,
		{                          // то остановить его
			int cntPorts, p;
            cntPorts = settings->serPortObjectsList.length();

			for( p = 0; p < cntPorts; p++ )
			{
				settings->serPortObjectsList.at( p )->disconnect();
                serialPortClassObject->CloseSerialPort( settings->serPortObjectsList.at( p ) ); // массив  объектов для закрытия COM-порта
            }

			flagActivity = false;
			settings->initialised_bk01 = false; // если флаг инициализации БК-01 сброшен, то  начать процесс инициализации

			startServerProcess( false );

		    // Запись лога
			mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
			mysqlProccessObject->saveEventToLog( tr("Stop server") );
		}
	}

    // Обработка timeOut-ов  для  определния отсутствия связи с весами
	if( timeoutsNoUartConnection[ 0 ]++ >= TIMEOUT_NO_SCALES_CONNECTION ) // таймаут весов №1
	{
		//
		#ifndef DEBUG
		this->vectorLCD_Indicator.at( 0 )->setText( "------" );
	    flagsNoUarts_connection[ 0 ] = false;
		timeoutsNoUartConnection[ 0 ] = 0;
		groupBoxPlatform->hide();
        #endif
	}
	

	if( settings->group_TCPWeight->isChecked() )
	{
		if( isVPConnected )
		{
			lcdScalesConnection->setText( tr("Connected") );
		}
		else
		{
			lcdScalesConnection->setText( tr("No connection") );
		}
	}
	else
	{
		if( flagsNoUarts_connection[ settings->currentScales ] )
		{
			lcdScalesConnection->setText( tr("Connected") );
		}
		else
		{
			lcdScalesConnection->setText( tr("No connection") );
		}
	}


	// Вывод времени в footer
	//this->statusBar()->showMessage( QDateTime::currentDateTime().toString("hh:mm dd.MM.yyyy") + " | " + tr("User: ")
	//	+ QString("%1" ).arg( loginWindowObject->currentNameUser ) + " | " + tr("Scales link status ")
	//	+ QString("%1 : %2" ).arg( settings->currentScales + 1 )
	//	.arg( ( flagsNoUarts_connection[ settings->currentScales ] == true ? tr("Connected") : tr("Disconnected") ) )
	//	);
}

// http - запрос в IP-камеру №1
void MyClass::sendRequestIP_Cam( QNetworkAccessManager *mngr,  QString url_string, int port, QString login, QString password, int num_cam ) // http - запрос в IP-камеру
{
    //QUrl url( QString( "http://%1:%2@%3/Streaming/channels/102/picture/" ).arg( login ).arg( password ).arg( url_string ) );
	
	//как на ростке
	//QUrl url( QString( "http://%1:%2@%3/ISAPI/ContentMgmt/StreamingProxy/channels/102/picture/" ).arg( login ).arg( password ).arg( url_string ) );
	
	//3-й вариант
	QUrl url( QString( "http://%1:%2@%3/ISAPI/Streaming/channels/102/picture/" ).arg( login ).arg( password ).arg( url_string ) );

	
	QNetworkReply *reply = mngr->get( QNetworkRequest( url ) );
}

// Сигнал закрытия главного окна
void MyClass::closeEvent( QCloseEvent * e )
{
	QMessageBox mess;

	mess.setText( tr("Do you want to exit the program?") );
	mess.setStandardButtons( QMessageBox::Ok | QMessageBox::Cancel );
	mess.setButtonText( QMessageBox::Ok, tr("Yes") );
	mess.setButtonText( QMessageBox::Cancel, tr("No") );
	mess.setIcon( QMessageBox::Question );
	mess.setWindowTitle( tr( "Prom-Soft" ) );
	mess.exec();

	if( mess.result() != QMessageBox::Ok )
	{
		e->ignore();
	}
	else
	{
		clean_1C();
		delete waitW;
		// Записать событие в лог
		QSettings settings( "settings.ini", QSettings::IniFormat );
		settings.setIniCodec("Windows-1251");

		for(int i = 0; i < tableAutoReception->model()->columnCount() - 1; i++ )
		{
			if ( CardsMode_action->isChecked() )
			{
				settings.setValue( "mode/rfid", 1 );
				settings.setValue( QString( "columnSizes/cc%1" ).arg( i ), tableAutoReception->columnWidth( i ) );
			}
			else
			{
				settings.setValue( "mode/rfid", 0 );
				settings.setValue( QString( "columnSizes/c%1" ).arg( i ), tableAutoReception->columnWidth( i ) );
			}
		}

		for(int i = 0; i < reportFormObject->tableReports->model()->columnCount() - 1; i++ )
		{
			settings.setValue( QString( "columnSizes/r%1" ).arg( i ), reportFormObject->tableReports->columnWidth( i ) );
		}
		settings.sync();

		mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		mysqlProccessObject->saveEventToLog( tr("Exit program") + QString( "%1").arg( loginWindowObject->currentNameUser ) );
	}
}
// Вызов справочного документа (Help)
void MyClass::launchHelp()
{
	QDesktopServices::openUrl( QUrl( "Help\\Help.pdf" ) );
}

// О программе
void MyClass::aboutSoft()
{
	QString styleSheet;
	styleSheet = "text-size: 24; border-radius: 5px; color: #004B7F; background-color: rgb(144, 238, 144);";//#fff

    /*QFile styleFile("Resources/styles/styleSheet.qss");
	if( styleFile.open( QFile::ReadOnly ) )
	{
		styleSheet = QLatin1String( styleFile.readAll() );
	}*/

	QRect screenResolution = QApplication::desktop()->availableGeometry();
	const QRect helpWindowSizePos = QRect(
		( screenResolution.width() / 2 ) - ( screenResolution.width() / 5 ),
		( screenResolution.height() / 5 ) - ( screenResolution.height() / 30 ),
		600, 150 );
	const QSize helpWindowSize = QSize( 640, 148 );

	dialogHelp = new QDialog;
    dialogHelp->setWindowTitle( tr("About") );

	dialogHelp->setGeometry( helpWindowSizePos );
	dialogHelp->setModal( true );
	dialogHelp->setFixedSize( helpWindowSize );
	//dialogHelp->setWindowIcon( QPixmap( "image/6.png", "PNG" ) );
	dialogHelp->setStyleSheet( "text-size: 24;border: 2px solid #000; border-radius: 5px; color: #004B7F; background-color: #fff; " );


	QFrame *frame = new QFrame( dialogHelp );
	frame->setGeometry( 160, 5, helpWindowSize.width() - 173, helpWindowSize.height() - 9 );
	frame->setFrameStyle( QFrame::Panel | QFrame::Raised );
	frame->setFrameShape( QFrame::Box );//StyledPanel
	frame->setFrameShadow( QFrame::Raised );
	frame->setLineWidth( 2 );

	QLabel *promsoft = new QLabel( dialogHelp );
	promsoft->setGeometry( 3, 1, 145, 145 );
	QPixmap pixmap_promsoft;
	pixmap_promsoft.load( "image/promsoft200.png");
	promsoft->setPixmap( pixmap_promsoft.scaled( promsoft->size() ) );

	QLabel *aboutText = new QLabel( frame );
	aboutText->setText(
		tr("This software is an automatic workstation \nweighing system wich is intended to control and account \nauto weighing with assistance of operator-weigher. \n\n\nTechnical support - 093 009 9990 \nProm-Soft") );

	aboutText->setStyleSheet( "float: center; text-align: center; text-size: 18px; margin: 5px; border: 0px;" );

	QPushButton *closeButton = new QPushButton( tr("Close"), dialogHelp );
	closeButton->setGeometry( ( helpWindowSize.width() / 3) + 70, helpWindowSize.height() - 40, helpWindowSize.width() / 3, 32 );
	closeButton->setStyleSheet( styleSheet );
	connect( closeButton, SIGNAL( clicked() ), dialogHelp, SLOT( hide() ) );

	dialogHelp->show();
}
//
MyClass::~MyClass()
{
	delete sensorsPasswordDialog;
	delete loginWindowObject;
	delete spravochnikTaraObj;
	delete serialPortClassObject;
	delete dialogVPSensorsObj;
	delete settings;
	delete imageSaveModelObject;
	delete acceptFormModelObject;
	delete acceptform;
	delete mysqlProccessObject;
	//delete dialogHelp;
}

// Инициализация  Timer - а запросов в COM-порт
void MyClass::initTimerRequestCOM_Port()
{
	char n[ 24 ];

	// Таймер опроса  ВП
	timerRequestCOM_Port = new QTimer( this ); //( object ); // this ); // объект таймера запросов в COM-порт
	timerRequestCOM_Port->setInterval( INTERVAL_REQUEST );
	timerRequestCOM_Port->setSingleShot( true );
	QObject::connect( timerRequestCOM_Port, SIGNAL( timeout() ), this, SLOT( timerRequestCOM_Port_elapsed() ) ); // создать  обработчик таймерного события запросов в COM-порт

	// Таймер системный
	sysTickTimer = new QTimer( this );
	sysTickTimer->setInterval( SYS_TYCK_TIME );
	QObject::connect( sysTickTimer, SIGNAL( timeout() ), this, SLOT( timer_sysTick_elapsed() ) ); // создать  обработчик системного таймера
	sysTickTimer->start();

	// SettingsForm *sett = new SettingsForm();

    // Таймаут определения отсутствия связи с ВП
	for( int i = 0; i < settings->getNumVP(); i++ ) // ??? количество ВП
	{
	    // Таймер TimeOut-а для определения отсутствия связи с ВП
  //      ticksNoConnection = new QTimer( this );
	 //   ticksNoConnection->setInterval( TIMEOUT_NO_SCALES_CONNECTION ); // ???  настраиваемая величина
		//memset( n, 0x00, sizeof( n ) );
		//sprintf( n, "ticksNoConnection%2d", i + 1 );

		// ticksNoConnection->setObjectName( QString( n ) );
		// ticksNoConnection->start();
        // ticksNoConnection->installEventFilter( object );
		// vectorTimeOut_COM_Port.append( ticksNoConnection ); // помещаем объект timer-а timeOut - а в массив таймеров
	}
}
// Начать процесс опроса ВП
bool MyClass::startServerProcess( bool command )
{
	bool result = false;

	if( timerRequestCOM_Port->isActive() == false && command == true )
	{
		timerRequestCOM_Port->setInterval( INTERVAL_REQUEST );
		timerRequestCOM_Port->start();
		result = true;
	}
	else if( timerRequestCOM_Port->isActive() == true &&  command == false )
	{
		timerRequestCOM_Port->stop();
		result = true;
	}

	return result;
}

//
void MyClass::groupUDP_Settings_Slot( bool state )
{
	if( state == true )
	{
		bool flag;

		settings->vectorComPortsVP.at( settings->currentScales )->setEnabled( false );
		settings->vectorBaudsVP.at( settings->currentScales )->setEnabled( false );
		settings->vectorEOFrameVP.at( settings->currentScales )->setEnabled( true );
	}
	else
	{
		settings->vectorComPortsVP.at( settings->currentScales )->setEnabled( true );
		settings->vectorBaudsVP.at( settings->currentScales )->setEnabled( true );
		settings->vectorEOFrameVP.at( settings->currentScales )->setEnabled( true );
	}
}

// Слот закрытия окна приложения
void MyClass::closeApp_SLOT()
{


}

//// Ответ на HTTP-запрос для IP-камеры №1
//void MyClass::finishedHTTP_Request_Cam_01( QNetworkReply *reply )
//{
//	if( reply->error() == QNetworkReply::NoError  )
//	{
//	    mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
//		QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );
//		bool result;
//		QSqlError error;
//
//		// Преобразовать screenshot изображения от IP-камеры в массив байт
//		QByteArray ba = reply->readAll();
//		QByteArray jpegData;
//
//        // Прочитать текущие настройки IP-камер  из  Ini-файла
//	    settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 0 ); // камера №1
//		if( currentSettingsIP_CamPtr->cam_photofix_preview_en == true )
//		{
//			jpegData = makeStampPhoto( ba,
//				( imageSaveModelObject->num_measure == 0 ? imageSaveModelObject->Date_Time : imageSaveModelObject->Date_Time_Send),
//				imageSaveModelObject->num_auto,
//				settings->getNumPosStampPhoto(),
//				 QString::number(imageSaveModelObject->num_measure ^ acceptFormModelObject->type_operation ? acceptFormModelObject->Tara :  acceptFormModelObject->Brutto ) );
//
//			QPixmap pixmap;
//			pixmap.loadFromData( jpegData );
//
//			// IMAGE_1000
//			pixmap = pixmap.scaled( QSize( previewSnapshot_1->size() ), Qt::IgnoreAspectRatio, Qt::SmoothTransformation );
//
//			previewSnapshot_1->setPixmap( pixmap ); // отобразить картинку в окне IP-камеры №1
//			previewSnapshot_1->show();
//		}
//
//		// Сохраняем картинку в виде массива байт
//		imageSaveModelObject->image1.clear();
//		imageSaveModelObject->image1.append( jpegData );
//		//
//		query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
//	    result = query.exec();
//
//		int idf = 0;
//
//		if( imageSaveModelObject->num_measure == 0 )
//		{
//			result = query.exec( QString( "SELECT IDF FROM AutoReception WHERE Date_Time='%1';" ).arg( imageSaveModelObject->Date_Time ) );
//
//			if( query.next() )
//			{
//				idf = query.value( 0 ).toInt();
//			}
//
//		    query.prepare( QString( "UPDATE images SET photo_1=:photo_1 WHERE ID='%1';" ).arg( idf ) );
//	        query.bindValue( ":photo_1", imageSaveModelObject->image1 );
//		}
//		else
//		{
//			result = query.exec( QString( "SELECT IDF FROM ReportsWeight WHERE Date_Time_Accept='%1';" ).arg( imageSaveModelObject->Date_Time ) );
//
//			if( query.next() )
//			{
//				idf = query.value( 0 ).toInt();
//			}
//
//			query.prepare( QString( "UPDATE images SET photo_2=:photo_2 WHERE ID='%1';" ).arg( idf ) );
//	        query.bindValue( ":photo_2", imageSaveModelObject->image1 );
//		}
//
//		result = query.exec();
//		//
//		// Если ошибка  записи в БД,  то  выйти из  метода
//	    if( error.type() != QSqlError::NoError )
//	    {
//            //  Ошибка записи в БД
//        }
//
//		timeSnapShotShow_1->start();
//	}
//	else
//	{
//		char buff[ 255 ];
//		strcpy( buff, ( char* )reply->errorString().toAscii().data() );
//		qDebug( buff );
//	}
//
//	reply->deleteLater();
//}
//


// Ответ на HTTP-запрос для IP-камеры №1


void MyClass::finishedHTTP_Request_Cam_01( QNetworkReply *reply )
{
	if( reply->error() == QNetworkReply::NoError  )
	{
	    mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );
		bool result;
		QSqlError error;

		// Преобразовать screenshot изображения от IP-камеры в массив байт
		QByteArray ba = reply->readAll();
		QByteArray jpegData;

        // Прочитать текущие настройки IP-камер  из  Ini-файла
	    settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 0 ); // камера №1
		if( currentSettingsIP_CamPtr->cam_photofix_preview_en == true )
		{
			jpegData = makeStampPhoto( ba,
				( imageSaveModelObject->num_measure == 0 ? imageSaveModelObject->Date_Time : imageSaveModelObject->Date_Time_Send),
				imageSaveModelObject->num_auto,
				settings->getNumPosStampPhoto(),
				 QString::number(imageSaveModelObject->num_measure ^ acceptFormModelObject->type_operation ? acceptFormModelObject->Tara :  acceptFormModelObject->Brutto ) );

			QPixmap pixmap;
			pixmap.loadFromData( jpegData );

			// IMAGE_1000
			pixmap = pixmap.scaled( QSize( previewSnapshot_1->size() ), Qt::IgnoreAspectRatio, Qt::SmoothTransformation );

			previewSnapshot_1->setPixmap( pixmap ); // отобразить картинку в окне IP-камеры №1
			previewSnapshot_1->show();
		}

		// Сохраняем картинку в виде массива байт
		imageSaveModelObject->image1.clear();
		imageSaveModelObject->image1.append( jpegData );
		//
		query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	    result = query.exec();

		int idf = 0;

		QString tableNameReception = "AutoReception";
		QString tableNameReport = "ReportsWeight";
		if( CardsMode_action->isChecked() )
		{
			tableNameReception = "AcceptCards";
			tableNameReport = "ReportCards";
		}
		if( imageSaveModelObject->num_measure == 0 )
		{
			result = query.exec( QString( "SELECT IDF FROM %1 WHERE Date_Time='%2';" ).arg(tableNameReception).arg( imageSaveModelObject->Date_Time ) );

			if( query.next() )
			{
				idf = query.value( 0 ).toInt();
			}

		    query.prepare( QString( "UPDATE images SET photo_1=:photo_1 WHERE ID='%1';" ).arg( idf ) );
	        query.bindValue( ":photo_1", imageSaveModelObject->image1 );
		}
		else
		{
			result = query.exec( QString( "SELECT IDF FROM %1 WHERE Date_Time_Accept='%2';" ).arg(tableNameReport).arg( imageSaveModelObject->Date_Time ) );

			if( query.next() )
			{
				idf = query.value( 0 ).toInt();
			}

			query.prepare( QString( "UPDATE images SET photo_2=:photo_2 WHERE ID='%1';" ).arg( idf ) );
	        query.bindValue( ":photo_2", imageSaveModelObject->image1 );
		}

		result = query.exec();
		//
		// Если ошибка  записи в БД,  то  выйти из  метода
	    if( error.type() != QSqlError::NoError )
	    {
            //  Ошибка записи в БД
        }

		timeSnapShotShow_1->start();
	}
	else
	{
		char buff[ 255 ];
		strcpy( buff, ( char* )reply->errorString().toAscii().data() );
		qDebug( buff );
	}

	reply->deleteLater();
}





// Таймер отображения фотоснимка от  камеры  №1
void MyClass::timeSnapShotShow_1_slot()
{
	previewSnapshot_1->hide();
	emit cursorChange_Signal( false );
}
//// Ответ на HTTP-запрос для IP-камеры №2
//void MyClass::finishedHTTP_Request_Cam_02( QNetworkReply *reply )
//{
//	if( reply->error() == QNetworkReply::NoError  )
//	{
//	    mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
//		QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );
//		bool result;
//		QSqlError error;
//
//        // Преобразовать screenshot изображения от IP-камеры в массив байт
//		QByteArray ba = reply->readAll();
//		QByteArray jpegData;
//
//		// Прочитать текущие настройки IP-камер  из  Ini-файла
//	    settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 1 ); // камера №2
//		if( currentSettingsIP_CamPtr->cam_photofix_preview_en == true )
//		{
//			jpegData = makeStampPhoto( ba,
//				( imageSaveModelObject->num_measure == 0 ? imageSaveModelObject->Date_Time : imageSaveModelObject->Date_Time_Send ),
//				imageSaveModelObject->num_auto,
//				settings->getNumPosStampPhoto(),
//				QString::number(imageSaveModelObject->num_measure ^ acceptFormModelObject->type_operation ? acceptFormModelObject->Tara :  acceptFormModelObject->Brutto ) );
//
//			QPixmap pixmap;
//			pixmap.loadFromData( jpegData );
//			pixmap = pixmap.scaled( QSize( previewSnapshot_2->size()  /* *imageLabel_maxSize2 */ ), Qt::IgnoreAspectRatio, Qt::SmoothTransformation );
//
//			previewSnapshot_2->setPixmap( pixmap ); // отобразить картинку в окне IP-камеры №2
//			previewSnapshot_2->show();
//		}
//
//		// Сохраняем картинку в виде массива байт
//		imageSaveModelObject->image2.clear();
//		imageSaveModelObject->image2.append( jpegData );
//        //
//        query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
//	    result = query.exec();
//
//        int idf = 0;
//
//		if( imageSaveModelObject->num_measure == 0 )
//		{
//			result = query.exec( QString( "SELECT IDF FROM AutoReception WHERE Date_Time='%1';" ).arg( imageSaveModelObject->Date_Time ) );
//
//			if( query.next() )
//			{
//				idf = query.value( 0 ).toInt();
//			}
//
//		    query.prepare( QString( "UPDATE images SET photo_5=:photo_5 WHERE ID='%1';" ).arg( idf ) );
//	        query.bindValue( ":photo_5", imageSaveModelObject->image2 );
//		}
//		else
//		{
//			result = query.exec( QString( "SELECT IDF FROM ReportsWeight WHERE Date_Time_Accept='%1';" ).arg( imageSaveModelObject->Date_Time ) );
//
//			if( query.next() )
//			{
//				idf = query.value( 0 ).toInt();
//			}
//
//			query.prepare( QString( "UPDATE images SET photo_6=:photo_6 WHERE ID='%1';" ).arg( idf ) );
//	        query.bindValue( ":photo_6", imageSaveModelObject->image2 );
//		}
//
//		result = query.exec();
//		//
//		// Если ошибка  записи в БД,  то  выйти из  метода
//	    if( error.type() != QSqlError::NoError )
//	    {
//            //  Ошибка записи в БД
//        }
//
//		timeSnapShotShow_2->start();
//	}
//	else
//	{
//		char buff[ 255 ];
//		strcpy( buff, ( char* )reply->errorString().toAscii().data() );
//		qDebug( buff );
//	}
//
//	reply->deleteLater();
//}
//

// Таймер отображения фотоснимка от  камеры  №2

// Ответ на HTTP-запрос для IP-камеры №2
void MyClass::finishedHTTP_Request_Cam_02( QNetworkReply *reply )
{
	if( reply->error() == QNetworkReply::NoError  )
	{
	    mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );
		bool result;
		QSqlError error;

        // Преобразовать screenshot изображения от IP-камеры в массив байт
		QByteArray ba = reply->readAll();
		QByteArray jpegData;

		// Прочитать текущие настройки IP-камер  из  Ini-файла
	    settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 1 ); // камера №2
		if( currentSettingsIP_CamPtr->cam_photofix_preview_en == true )
		{
			jpegData = makeStampPhoto( ba,
				( imageSaveModelObject->num_measure == 0 ? imageSaveModelObject->Date_Time : imageSaveModelObject->Date_Time_Send ),
				imageSaveModelObject->num_auto,
				settings->getNumPosStampPhoto(),
				QString::number(imageSaveModelObject->num_measure ^ acceptFormModelObject->type_operation ? acceptFormModelObject->Tara :  acceptFormModelObject->Brutto ) );

			QPixmap pixmap;
			pixmap.loadFromData( jpegData );
			pixmap = pixmap.scaled( QSize( previewSnapshot_2->size()  /* *imageLabel_maxSize2 */ ), Qt::IgnoreAspectRatio, Qt::SmoothTransformation );

			previewSnapshot_2->setPixmap( pixmap ); // отобразить картинку в окне IP-камеры №2
			previewSnapshot_2->show();
		}

		// Сохраняем картинку в виде массива байт
		imageSaveModelObject->image2.clear();
		imageSaveModelObject->image2.append( jpegData );
        //
        query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	    result = query.exec();

        int idf = 0;

		QString tableNameReception = "AutoReception";
		QString tableNameReport = "ReportsWeight";
		if( CardsMode_action->isChecked() )
		{
			tableNameReception = "AcceptCards";
			tableNameReport = "ReportCards";
		}

		if( imageSaveModelObject->num_measure == 0 )
		{
			result = query.exec( QString( "SELECT IDF FROM %1 WHERE Date_Time='%2';" ).arg(tableNameReception).arg( imageSaveModelObject->Date_Time ) );

			if( query.next() )
			{
				idf = query.value( 0 ).toInt();
			}

		    query.prepare( QString( "UPDATE images SET photo_5=:photo_5 WHERE ID='%1';" ).arg( idf ) );
	        query.bindValue( ":photo_5", imageSaveModelObject->image2 );
		}
		else
		{
			result = query.exec( QString( "SELECT IDF FROM %1 WHERE Date_Time_Accept='%2';" ).arg(tableNameReport).arg( imageSaveModelObject->Date_Time ) );

			if( query.next() )
			{
				idf = query.value( 0 ).toInt();
			}

			query.prepare( QString( "UPDATE images SET photo_6=:photo_6 WHERE ID='%1';" ).arg( idf ) );
	        query.bindValue( ":photo_6", imageSaveModelObject->image2 );
		}

		result = query.exec();
		//
		// Если ошибка  записи в БД,  то  выйти из  метода
	    if( error.type() != QSqlError::NoError )
	    {
            //  Ошибка записи в БД
        }

		timeSnapShotShow_2->start();
	}
	else
	{
		char buff[ 255 ];
		strcpy( buff, ( char* )reply->errorString().toAscii().data() );
		qDebug( buff );
	}

	reply->deleteLater();
}


void MyClass::timeSnapShotShow_2_slot()
{
	previewSnapshot_2->hide();
	emit cursorChange_Signal( false );
}
//// Ответ на HTTP-запрос для IP-камеры №3
//void MyClass::finishedHTTP_Request_Cam_03( QNetworkReply *reply )
//{
//	if( reply->error() == QNetworkReply::NoError  )
//	{
//	    mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
//		QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );
//		bool result;
//		QSqlError error;
//
//        // Преобразовать screenshot изображения от IP-камеры в массив байт
//		QByteArray ba = reply->readAll();
//		QByteArray jpegData;
//
//		// Прочитать текущие настройки IP-камер  из  Ini-файла
//	    settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 2 ); // камера №3
//		if( currentSettingsIP_CamPtr->cam_photofix_preview_en == true )
//		{
//			jpegData = makeStampPhoto( ba,
//				( imageSaveModelObject->num_measure == 0 ? imageSaveModelObject->Date_Time : imageSaveModelObject->Date_Time_Send ),
//				imageSaveModelObject->num_auto,
//				settings->getNumPosStampPhoto(),
//				QString::number(imageSaveModelObject->num_measure ^ acceptFormModelObject->type_operation ? acceptFormModelObject->Tara :  acceptFormModelObject->Brutto ) );
//
//			QPixmap pixmap;
//			pixmap.loadFromData( jpegData );
//			pixmap = pixmap.scaled( QSize( previewSnapshot_3->size()  /* *imageLabel_maxSize2 */ ), Qt::IgnoreAspectRatio, Qt::SmoothTransformation );
//
//			previewSnapshot_3->setPixmap( pixmap ); // отобразить картинку в окне IP-камеры №2
//			previewSnapshot_3->show();
//		}
//
//		// Сохраняем картинку в виде массива байт
//		imageSaveModelObject->image3.clear();
//		imageSaveModelObject->image3.append( jpegData );
//        //
//        query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
//	    result = query.exec();
//
//        int idf = 0;
//
//		if( imageSaveModelObject->num_measure == 0 )
//		{
//			result = query.exec( QString( "SELECT IDF FROM AutoReception WHERE Date_Time='%1';" ).arg( imageSaveModelObject->Date_Time ) );
//
//			if( query.next() )
//			{
//				idf = query.value( 0 ).toInt();
//			}
//
//		    query.prepare( QString( "UPDATE images SET photo_3=:photo_3 WHERE ID='%1';" ).arg( idf ) );
//	        query.bindValue( ":photo_3", imageSaveModelObject->image3 );
//		}
//		else
//		{
//			result = query.exec( QString( "SELECT IDF FROM ReportsWeight WHERE Date_Time_Accept='%1';" ).arg( imageSaveModelObject->Date_Time ) );
//
//			if( query.next() )
//			{
//				idf = query.value( 0 ).toInt();
//			}
//
//			query.prepare( QString( "UPDATE images SET photo_4=:photo_4 WHERE ID='%1';" ).arg( idf ) );
//			query.bindValue( ":photo_4", imageSaveModelObject->image3 );
//		}
//
//		result = query.exec();
//		//
//		// Если ошибка  записи в БД,  то  выйти из  метода
//	    if( error.type() != QSqlError::NoError )
//	    {
//            //  Ошибка записи в БД
//        }
//
//		timeSnapShotShow_3->start();
//	}
//	else
//	{
//		char buff[ 255 ];
//		strcpy( buff, ( char* )reply->errorString().toAscii().data() );
//		qDebug( buff );
//	}
//
//	reply->deleteLater();
//}
//


// Таймер отображения фотоснимка от  камеры  №3

// Ответ на HTTP-запрос для IP-камеры №3
void MyClass::finishedHTTP_Request_Cam_03( QNetworkReply *reply )
{
	if( reply->error() == QNetworkReply::NoError  )
	{
	    mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );
		bool result;
		QSqlError error;

        // Преобразовать screenshot изображения от IP-камеры в массив байт
		QByteArray ba = reply->readAll();
		QByteArray jpegData;

		// Прочитать текущие настройки IP-камер  из  Ini-файла
	    settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 2 ); // камера №3
		if( currentSettingsIP_CamPtr->cam_photofix_preview_en == true )
		{
			jpegData = makeStampPhoto( ba,
				( imageSaveModelObject->num_measure == 0 ? imageSaveModelObject->Date_Time : imageSaveModelObject->Date_Time_Send ),
				imageSaveModelObject->num_auto,
				settings->getNumPosStampPhoto(),
				QString::number(imageSaveModelObject->num_measure ^ acceptFormModelObject->type_operation ? acceptFormModelObject->Tara :  acceptFormModelObject->Brutto ) );

			QPixmap pixmap;
			pixmap.loadFromData( jpegData );
			pixmap = pixmap.scaled( QSize( previewSnapshot_3->size()  /* *imageLabel_maxSize2 */ ), Qt::IgnoreAspectRatio, Qt::SmoothTransformation );

			previewSnapshot_3->setPixmap( pixmap ); // отобразить картинку в окне IP-камеры №2
			previewSnapshot_3->show();
		}

		// Сохраняем картинку в виде массива байт
		imageSaveModelObject->image3.clear();
		imageSaveModelObject->image3.append( jpegData );
        //
        query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	    result = query.exec();

        int idf = 0;

		QString tableNameReception = "AutoReception";
		QString tableNameReport = "ReportsWeight";
		if( CardsMode_action->isChecked() )
		{
			tableNameReception = "AcceptCards";
			tableNameReport = "ReportCards";
		}

		if( imageSaveModelObject->num_measure == 0 )
		{
			result = query.exec( QString( "SELECT IDF FROM %1 WHERE Date_Time='%2';" ).arg(tableNameReception).arg( imageSaveModelObject->Date_Time ) );

			if( query.next() )
			{
				idf = query.value( 0 ).toInt();
			}

		    query.prepare( QString( "UPDATE images SET photo_3=:photo_3 WHERE ID='%1';" ).arg( idf ) );
	        query.bindValue( ":photo_3", imageSaveModelObject->image3 );
		}
		else
		{
			result = query.exec( QString( "SELECT IDF FROM %1 WHERE Date_Time_Accept='%2';" ).arg(tableNameReport).arg( imageSaveModelObject->Date_Time ) );

			if( query.next() )
			{
				idf = query.value( 0 ).toInt();
			}

			query.prepare( QString( "UPDATE images SET photo_4=:photo_4 WHERE ID='%1';" ).arg( idf ) );
			query.bindValue( ":photo_4", imageSaveModelObject->image3 );
		}

		result = query.exec();
		//
		// Если ошибка  записи в БД,  то  выйти из  метода
	    if( error.type() != QSqlError::NoError )
	    {
            //  Ошибка записи в БД
        }

		timeSnapShotShow_3->start();
	}
	else
	{
		char buff[ 255 ];
		strcpy( buff, ( char* )reply->errorString().toAscii().data() );
		qDebug( buff );
	}

	reply->deleteLater();
}



void MyClass::timeSnapShotShow_3_slot()
{
	previewSnapshot_3->hide();
	emit cursorChange_Signal( false );
}

//// Ответ на HTTP-запрос для IP-камеры №4
//void MyClass::finishedHTTP_Request_Cam_04( QNetworkReply *reply )
//{
//	if( reply->error() == QNetworkReply::NoError  )
//	{
//	    mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
//		QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );
//		bool result;
//		QSqlError error;
//
//        // Преобразовать screenshot изображения от IP-камеры в массив байт
//		QByteArray ba = reply->readAll();
//		QByteArray jpegData;
//
//		// Прочитать текущие настройки IP-камер  из  Ini-файла
//	    settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 3 ); // камера №4
//		if( currentSettingsIP_CamPtr->cam_photofix_preview_en == true )
//		{
//			jpegData = makeStampPhoto( ba,
//				( imageSaveModelObject->num_measure == 0 ? imageSaveModelObject->Date_Time : imageSaveModelObject->Date_Time_Send ),
//				imageSaveModelObject->num_auto,
//				settings->getNumPosStampPhoto(),
//				QString::number(imageSaveModelObject->num_measure ^ acceptFormModelObject->type_operation ? acceptFormModelObject->Tara :  acceptFormModelObject->Brutto ) );
//
//			QPixmap pixmap;
//			pixmap.loadFromData( jpegData );
//			pixmap = pixmap.scaled( QSize( previewSnapshot_4->size()  /* *imageLabel_maxSize2 */ ), Qt::IgnoreAspectRatio, Qt::SmoothTransformation );
//
//			previewSnapshot_4->setPixmap( pixmap ); // отобразить картинку в окне IP-камеры №2
//			previewSnapshot_4->show();
//		}
//
//		// Сохраняем картинку в виде массива байт
//		imageSaveModelObject->image4.clear();
//		imageSaveModelObject->image4.append( jpegData );
//        //
//        query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
//	    result = query.exec();
//
//        int idf = 0;
//
//		if( imageSaveModelObject->num_measure == 0 )
//		{
//			result = query.exec( QString( "SELECT IDF FROM AutoReception WHERE Date_Time='%1';" ).arg( imageSaveModelObject->Date_Time ) );
//
//			if( query.next() )
//			{
//				idf = query.value( 0 ).toInt();
//			}
//
//		    query.prepare( QString( "UPDATE images SET photo_7=:photo_7 WHERE ID='%1';" ).arg( idf ) );
//	        query.bindValue( ":photo_7", imageSaveModelObject->image4 );
//		}
//		else
//		{
//			result = query.exec( QString( "SELECT IDF FROM ReportsWeight WHERE Date_Time_Accept='%1';" ).arg( imageSaveModelObject->Date_Time ) );
//
//			if( query.next() )
//			{
//				idf = query.value( 0 ).toInt();
//			}
//
//			query.prepare( QString( "UPDATE images SET photo_8=:photo_8 WHERE ID='%1';" ).arg( idf ) );
//	        query.bindValue( ":photo_8", imageSaveModelObject->image4 );
//		}
//
//		result = query.exec();
//		//
//		// Если ошибка  записи в БД,  то  выйти из  метода
//	    if( error.type() != QSqlError::NoError )
//	    {
//            //  Ошибка записи в БД
//        }
//
//		timeSnapShotShow_4->start();
//	}
//	else
//	{
//		char buff[ 255 ];
//		strcpy( buff, ( char* )reply->errorString().toAscii().data() );
//		qDebug( buff );
//	}
//
//	reply->deleteLater();
//}
//

// Ответ на HTTP-запрос для IP-камеры №4
void MyClass::finishedHTTP_Request_Cam_04( QNetworkReply *reply )
{
	if( reply->error() == QNetworkReply::NoError  )
	{
	    mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );
		bool result;
		QSqlError error;

        // Преобразовать screenshot изображения от IP-камеры в массив байт
		QByteArray ba = reply->readAll();
		QByteArray jpegData;

		// Прочитать текущие настройки IP-камер  из  Ini-файла
	    settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 3 ); // камера №4
		if( currentSettingsIP_CamPtr->cam_photofix_preview_en == true )
		{
			jpegData = makeStampPhoto( ba,
				( imageSaveModelObject->num_measure == 0 ? imageSaveModelObject->Date_Time : imageSaveModelObject->Date_Time_Send ),
				imageSaveModelObject->num_auto,
				settings->getNumPosStampPhoto(),
				QString::number(imageSaveModelObject->num_measure ^ acceptFormModelObject->type_operation ? acceptFormModelObject->Tara :  acceptFormModelObject->Brutto ) );

			QPixmap pixmap;
			pixmap.loadFromData( jpegData );
			pixmap = pixmap.scaled( QSize( previewSnapshot_4->size()  /* *imageLabel_maxSize2 */ ), Qt::IgnoreAspectRatio, Qt::SmoothTransformation );

			previewSnapshot_4->setPixmap( pixmap ); // отобразить картинку в окне IP-камеры №2
			previewSnapshot_4->show();
		}

		// Сохраняем картинку в виде массива байт
		imageSaveModelObject->image4.clear();
		imageSaveModelObject->image4.append( jpegData );
        //
        query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	    result = query.exec();

        int idf = 0;

		QString tableNameReception = "AutoReception";
		QString tableNameReport = "ReportsWeight";
		if( CardsMode_action->isChecked() )
		{
			tableNameReception = "AcceptCards";
			tableNameReport = "ReportCards";
		}

		if( imageSaveModelObject->num_measure == 0 )
		{
			result = query.exec( QString( "SELECT IDF FROM %1 WHERE Date_Time='%2';" ).arg(tableNameReception).arg( imageSaveModelObject->Date_Time ) );

			if( query.next() )
			{
				idf = query.value( 0 ).toInt();
			}

		    query.prepare( QString( "UPDATE images SET photo_7=:photo_7 WHERE ID='%1';" ).arg( idf ) );
	        query.bindValue( ":photo_7", imageSaveModelObject->image4 );
		}
		else
		{
			result = query.exec( QString( "SELECT IDF FROM %1 WHERE Date_Time_Accept='%2';" ).arg(tableNameReport).arg( imageSaveModelObject->Date_Time ) );

			if( query.next() )
			{
				idf = query.value( 0 ).toInt();
			}

			query.prepare( QString( "UPDATE images SET photo_8=:photo_8 WHERE ID='%1';" ).arg( idf ) );
	        query.bindValue( ":photo_8", imageSaveModelObject->image4 );
		}

		result = query.exec();
		//
		// Если ошибка  записи в БД,  то  выйти из  метода
	    if( error.type() != QSqlError::NoError )
	    {
            //  Ошибка записи в БД
        }

		timeSnapShotShow_4->start();
	}
	else
	{
		char buff[ 255 ];
		strcpy( buff, ( char* )reply->errorString().toAscii().data() );
		qDebug( buff );
	}

	reply->deleteLater();
}


// Таймер отображения фотоснимка от  камеры  №4
void MyClass::timeSnapShotShow_4_slot()
{
	previewSnapshot_4->hide();
	emit cursorChange_Signal( false );
}

// Добавить поток для работы с отправкой почты
void MyClass::addMailThread()
{
	threadMail = new QThread( this );
/*	mailThreadObject = new MailThread();

	bool flag = QObject::connect( mailThreadObject, SIGNAL( mailSendStatus_Signal( bool, bool ) ), this, SLOT( mailSendStatus_Slot( bool, bool ) ) );
	flag = QObject::connect( this, SIGNAL( loadMailData_Signal( bool, QString, int, QString, QString, QString, QString, QString, QString, bool, QString, QStringList ) ),
		mailThreadObject, SLOT( loadMailData_Slot( bool, QString, int, QString, QString, QString, QString, QString, QString, bool, QString, QStringList ) ) );

	flag = QObject::connect( reportFormObject, SIGNAL( loadMailData_Signal( bool, QString, int, QString, QString, QString, QString, QString, QString, bool, QString, QStringList ) ),
		mailThreadObject, SLOT( loadMailData_Slot( bool, QString, int, QString, QString, QString, QString, QString, QString, bool, QString, QStringList ) ) );

	flag = QObject::connect( dialogVPSensorsObj, SIGNAL( loadMailData_Signal( bool, QString, int, QString, QString, QString, QString, QString, QString, bool, QString, QStringList ) ),
		mailThreadObject, SLOT( loadMailData_Slot( bool, QString, int, QString, QString, QString, QString, QString, QString, bool, QString, QStringList ) ) );

	mailThreadObject->start();
	mailThreadObject->setPriority( QThread::NormalPriority );
	*/
}

//
void MyClass::mailSendStatus_Slot( bool status, bool manual_send )
{
	QMessageBox mess;
	mess.setWindowTitle( tr( "Prom-Soft" ) );
	mess.setStandardButtons( QMessageBox::Cancel );
	mess.setButtonText( QMessageBox::Cancel, "Ok" );

	QSettings settings( "settings.ini", QSettings::IniFormat );
    settings.setIniCodec("Windows-1251");

	// Если отправка сообщения идёт  ручная,  то  отобразить статус  отпрака  сообщения
	if( manual_send == true )
	{
		if( status == true )
		{
			mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
			mysqlProccessObject->saveEventToLog( tr("Attempt to send e-mail successfully ") +
				QString( "%1" ).arg( settings.value( "mail/mailToSend" ).toString() ) );

			mess.setIcon( QMessageBox::Information );
			mess.setText( tr("Sended to ") + QString( "%1" ).arg( settings.value( "mail/mailToSend" ).toString() ) );
			mess.exec();
		}
		else
		{
			mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
			mysqlProccessObject->saveEventToLog( tr("Attempt to send e-mail unsuccessful ") +
				QString( "%1" ).arg( settings.value( "mail/mailToSend" ).toString() ) );

			mess.setIcon( QMessageBox::Critical );
			mess.setText( tr("The letter is not sent to destination. Check your mail settings and internet connection.") );
			mess.exec();
		}
	}
	else
	if( status == true )
	{
		settings.setValue( "mail/last_date_time", QDateTime::currentDateTime().toString( "yyyy-MM-dd hh:mm:ss" ) );
		settings.sync();
	}
}

//
void MyClass::finishedHTTP_Request_CamView_01( QNetworkReply *reply )
{
	if( reply->error() == QNetworkReply::NoError )
	{
		QByteArray jpegData = reply->readAll();

		if( settings->vectorChkboxEnableIP_Cam.at( 0 )->isChecked() == true )
		{
			widget_Cam1->clear();

			QPixmap pixmap;
			pixmap.loadFromData( jpegData );
			pixmap = pixmap.scaled( QSize( widget_Cam1->size() ), Qt::IgnoreAspectRatio, Qt::SmoothTransformation );
			widget_Cam1->setPixmap( pixmap );
			widget_Cam1->repaint();

			QPixmap pixmap2;
			pixmap2.loadFromData( jpegData );
			if(focus_cam == 1)
			{
				labelMaxSize->setPixmap( pixmap2.scaled( QSize( imageLabel_maxGeometry1->size() ),
					Qt::IgnoreAspectRatio, Qt::SmoothTransformation ) );
			}
		}
	}

	reply->abort();
    reply->close();
	reply->deleteLater();
}

//
void MyClass::finishedHTTP_Request_CamView_02( QNetworkReply *reply )
{
	if( reply->error() == QNetworkReply::NoError  )
	{
		QByteArray jpegData = reply->readAll();

		if( settings->vectorChkboxEnableIP_Cam.at( 1 )->isChecked() == true )
		{
			widget_Cam2->clear();

			QPixmap pixmap;
			pixmap.loadFromData( jpegData );
			pixmap = pixmap.scaled( QSize( widget_Cam2->size() ), Qt::IgnoreAspectRatio, Qt::SmoothTransformation );
			widget_Cam2->setPixmap( pixmap );
			widget_Cam2->repaint();

			QPixmap pixmap2;
			pixmap2.loadFromData( jpegData );
			if(focus_cam == 2)
			{
				labelMaxSize->setPixmap( pixmap2.scaled( QSize( imageLabel_maxGeometry1->size() ),
					Qt::IgnoreAspectRatio, Qt::SmoothTransformation ) );
			}
		}
	}

	reply->abort();
    reply->close();
	reply->deleteLater();
}

//
void MyClass::finishedHTTP_Request_CamView_03( QNetworkReply *reply )
{
	if( reply->error() == QNetworkReply::NoError  )
	{
		QByteArray jpegData = reply->readAll();

		if( settings->vectorChkboxEnableIP_Cam.at( 2 )->isChecked() == true )
		{
			widget_Cam3->clear();

			QPixmap pixmap;
			pixmap.loadFromData( jpegData );
			pixmap = pixmap.scaled( QSize( widget_Cam3->size() ), Qt::IgnoreAspectRatio, Qt::SmoothTransformation );
			widget_Cam3->setPixmap( pixmap );
			widget_Cam3->repaint();

			QPixmap pixmap2;
			pixmap2.loadFromData( jpegData );
			if(focus_cam == 3)
			{
				labelMaxSize->setPixmap( pixmap2.scaled( QSize( imageLabel_maxGeometry1->size() ),
					Qt::IgnoreAspectRatio, Qt::SmoothTransformation ) );
			}
		}
	}

	reply->abort();
    reply->close();
	reply->deleteLater();
}

//
void MyClass::finishedHTTP_Request_CamView_04( QNetworkReply *reply )
{
	if( reply->error() == QNetworkReply::NoError  )
	{
		QByteArray jpegData = reply->readAll();

		if( settings->vectorChkboxEnableIP_Cam.at( 3 )->isChecked() == true )
		{
			widget_Cam4->clear();

			QPixmap pixmap;
			pixmap.loadFromData( jpegData );
			pixmap = pixmap.scaled( QSize( widget_Cam4->size() ), Qt::IgnoreAspectRatio, Qt::SmoothTransformation );
			widget_Cam4->setPixmap( pixmap );
			widget_Cam4->repaint();

			QPixmap pixmap2;
			pixmap2.loadFromData( jpegData );
			if(focus_cam == 4)
			{
				labelMaxSize->setPixmap( pixmap2.scaled( QSize( imageLabel_maxGeometry1->size() ),
					Qt::IgnoreAspectRatio, Qt::SmoothTransformation ) );
			}
		}
	}

	reply->abort();
    reply->close();
	reply->deleteLater();
}

//
void MyClass::timerRequestIP_Cams_Slot()
{
	static int num_cam = 0;

	timerRequestIP_Cams->stop();

	if( this->states_ip_cams != SNAPSHOT )
	{
		/*switch( num_cam )
		{
			case 0:
			{*/
				if( settings->vectorChkboxEnableIP_Cam.at( 0 )->isChecked() == true )
				{
					sendRequestIP_Cam( networkAccessManager_CamView01, settings->vectorEnterIP_Address.at( 0 )->text(),
						settings->vectorEnterPort_Address.at( 0 )->text().toInt(),
						settings->vectorEnterLogin.at( 0 )->text(), Crypt::decrypt( settings->vectorEnterPass.at( 0 )->text() ), 0 );
				}

			/*} break;
			case 1:
			{*/
				if( settings->vectorChkboxEnableIP_Cam.at( 1 )->isChecked() == true )
				{
					sendRequestIP_Cam( networkAccessManager_CamView02, settings->vectorEnterIP_Address.at( 1 )->text(),
						settings->vectorEnterPort_Address.at( 1 )->text().toInt(),
						settings->vectorEnterLogin.at( 1 )->text(), Crypt::decrypt( settings->vectorEnterPass.at( 1 )->text() ), 1 );
			    }
			/*	} break;
			case 2:
			{*/
				if( settings->vectorChkboxEnableIP_Cam.at( 2 )->isChecked() == true )
				{

					sendRequestIP_Cam( networkAccessManager_CamView03, settings->vectorEnterIP_Address.at( 2 )->text(),
						settings->vectorEnterPort_Address.at( 2 )->text().toInt(),
						settings->vectorEnterLogin.at( 2 )->text(), Crypt::decrypt( settings->vectorEnterPass.at( 2 )->text() ), 2 );
				}
			/*} break;
			case 3:
			{*/
				if( settings->vectorChkboxEnableIP_Cam.at( 3 )->isChecked() == true )
				{
					sendRequestIP_Cam( networkAccessManager_CamView04, settings->vectorEnterIP_Address.at( 3 )->text(),
						settings->vectorEnterPort_Address.at( 3 )->text().toInt(),
						settings->vectorEnterLogin.at( 3 )->text(), Crypt::decrypt( settings->vectorEnterPass.at( 3 )->text() ), 3 );
				}
			/*} break;

			default:
			{
				num_cam = 0;
			}
		}*/

		/*if( ++num_cam >= 4 )
		{
			num_cam = 0;
		}*/
	}

    /* else
	{
	    qDebug( "Start of Snapshots" );
	}*/

	timerRequestIP_Cams->start();
}


void MyClass::timerCursorOff_Slot()
{
	emit cursorChange_Signal( false );
}

void MyClass::cursorChange_Slot( bool a )
{
	if( a )
	{
		//setCursor(Qt::WaitCursor);
	}
	else
	{
		//setCursor(Qt::ArrowCursor);
	}
}

void MyClass::lang_ua_Slot()
{
	QSettings settings( "settings.ini", QSettings::IniFormat );
	settings.setIniCodec( "Windows-1251" );
	settings.beginGroup( "Server" );
	settings.setValue( "lang", "ua" );
	settings.endGroup();

	QMessageBox mess;
	mess.setText( "Перезавантажте програму для того, щоб прийняти зміни." );
	mess.setStandardButtons( QMessageBox::Cancel );
	mess.setButtonText( QMessageBox::Cancel, "Ok" );
	mess.setIcon( QMessageBox::Warning );
	mess.setWindowTitle( tr( "Prom-Soft" ) );
	mess.exec();
}
void MyClass::lang_ru_Slot()
{
	QSettings settings( "settings.ini", QSettings::IniFormat );
	settings.setIniCodec( "Windows-1251" );
	settings.beginGroup( "Server" );
	settings.setValue( "lang", "ru" );
	settings.endGroup();

	QMessageBox mess;
	mess.setText( "Перезапустите программу, чтобы изменения вступили в силу." );
	mess.setStandardButtons( QMessageBox::Cancel );
	mess.setButtonText( QMessageBox::Cancel, "Ok" );
	mess.setIcon( QMessageBox::Warning );
	mess.setWindowTitle( tr( "Prom-Soft" ) );
	mess.exec();
}
void MyClass::lang_en_Slot()
{
	QSettings settings( "settings.ini", QSettings::IniFormat );
	settings.setIniCodec( "Windows-1251" );
	settings.beginGroup( "Server" );
	settings.setValue( "lang", "en" );
	settings.endGroup();

	QMessageBox mess;
	mess.setText( "Restart program for changes to take effect." );
	mess.setStandardButtons( QMessageBox::Cancel );
	mess.setButtonText( QMessageBox::Cancel, "Ok" );
	mess.setIcon( QMessageBox::Warning );
	mess.setWindowTitle( tr( "Prom-Soft" ) );
	mess.exec();
}

void MyClass::socketsDialog_Slot()
{
	timerSocket2_1->start();
	timerSocket2_2->start();
	timerSocket2_3->start();

	socketDevices->show();
}

void MyClass::readSocket2_1_Slot()
{
	QByteArray message;
	forever
	{
		if(nextBlockSize1 == 0)
		{
			if(tcpSocket2_1->bytesAvailable() < 9) break;

			message = tcpSocket2_1->readAll();
		}
		if(nextBlockSize1 > tcpSocket2_1->bytesAvailable()) break;

		char data[8] = {0};
		data[0] = (char)message.at(0); // 15
		data[1] = (char)message.at(1); // 0
		data[2] = (char)message.at(2); // 1
		data[3] = (char)message.at(3); // 35
		data[4] = (char)message.at(4); // in 0
		data[5] = (char)message.at(5); // in 1
		data[6] = (char)message.at(6); // relay 0
		data[7] = (char)message.at(7); // relay 1

		if( data[3] == 35 ) socketDevices->setLink( 1, true );
		else return;
		socketDevices->setSocket2( 1, 0, (data[6] == 1) ? true : false );
		socketDevices->setSocket2( 1, 1, (data[7] == 1) ? true : false );
	}
}

void MyClass::readSocket2_2_Slot()
{
	QByteArray message;
	forever
	{
		if(nextBlockSize2 == 0)
		{
			if(tcpSocket2_2->bytesAvailable() < 9) break;

			message = tcpSocket2_2->readAll();
		}
		if(nextBlockSize2 > tcpSocket2_2->bytesAvailable()) break;

		char data[8] = {0};
		data[0] = (char)message.at(0); // 15
		data[1] = (char)message.at(1); // 0
		data[2] = (char)message.at(2); // 1
		data[3] = (char)message.at(3); // 35
		data[4] = (char)message.at(4); // in 0
		data[5] = (char)message.at(5); // in 1
		data[6] = (char)message.at(6); // relay 0
		data[7] = (char)message.at(7); // relay 1

		if( data[3] == 35 ) socketDevices->setLink( 2, true );
		else return;
		socketDevices->setSocket2( 2, 0, (data[6] == 1) ? true : false );
		socketDevices->setSocket2( 2, 1, (data[7] == 1) ? true : false );
	}
}

void MyClass::readSocket2_3_Slot()
{
	QByteArray message;
	forever
	{
		if(nextBlockSize3 == 0)
		{
			if(tcpSocket2_3->bytesAvailable() < 9) break;

			message = tcpSocket2_3->readAll();
		}
		if(nextBlockSize3 > tcpSocket2_3->bytesAvailable()) break;

		char data[8] = {0};
		data[0] = (char)message.at(0); // 15
		data[1] = (char)message.at(1); // 0
		data[2] = (char)message.at(2); // 1
		data[3] = (char)message.at(3); // 35
		data[4] = (char)message.at(4); // in 0
		data[5] = (char)message.at(5); // in 1
		data[6] = (char)message.at(6); // relay 0
		data[7] = (char)message.at(7); // relay 1

		if( data[3] == 35 ) socketDevices->setLink( 3, true );
		else return;
		socketDevices->setSocket2( 3, 0, (data[6] == 1) ? true : false );
		socketDevices->setSocket2( 3, 1, (data[7] == 1) ? true : false );
	}
}

void MyClass::timerSocket2_1_Slot()
{
	static int i = 0;

	if( i < 10 )
	{
		getSocket2State( 1 );
	}
	else
	{
		// if ON - 60sec signal
		if( socketDevices->getRelayState( 1, 0 ) )	setSocket2State_Slot( 1, 0, 1 );
		if( socketDevices->getRelayState( 1, 1 ) )	setSocket2State_Slot( 1, 1, 1 );
	}

	i++;
	if( i > 10 )	i = 0;
}
void MyClass::timerSocket2_2_Slot()
{
	static int i = 0;

	if( i < 10 )
	{
		getSocket2State( 2 );
	}
	else
	{
		// if ON - 60sec signal
		if( socketDevices->getRelayState( 2, 0 ) )	setSocket2State_Slot( 2, 0, 1 );
		if( socketDevices->getRelayState( 2, 1 ) )	setSocket2State_Slot( 2, 1, 1 );
	}

	i++;
	if( i > 10 )	i = 0;
}
void MyClass::timerSocket2_3_Slot()
{
	static int i = 0;

	if( i < 10 )
	{
		getSocket2State( 3 );
	}
	else
	{
		// if ON - 60sec signal
		if( socketDevices->getRelayState( 3, 0 ) )	setSocket2State_Slot( 3, 0, 1 );
		if( socketDevices->getRelayState( 3, 1 ) )	setSocket2State_Slot( 3, 1, 1 );
	}

	i++;
	if( i > 10 )	i = 0;
}

// sockets 1 or 2
void MyClass::getSocket2State( int socket_num )
{
	QByteArray block;
	QDataStream out( &block, QIODevice::WriteOnly );
	out.setVersion( QDataStream::Qt_4_0);
	unsigned char c0 = 35; //socket 2

	out << (quint16)0;
	out << c0;
	out.device()->seek(0);
	out << (quint16)(block.size() - sizeof(quint16) );

	if( socket_num == 1 )
	{
		//tcpSocket2_1->reset();
		tcpSocket2_1->write( block );
	}
	else if( socket_num == 2 )
	{
		//tcpSocket2_2->reset();
		tcpSocket2_2->write( block );
	}
	else if( socket_num == 3 )
	{
		//tcpSocket2_3->reset();
		tcpSocket2_3->write( block );
	}
}

// socket num 1,2,3; relay 0,1; setTo 0,1; time in ms
void MyClass::setSocket2State_Slot( int socket_num, int relay, int setTo )
{
	QByteArray block;
	QDataStream out( &block, QIODevice::WriteOnly );
	out.setVersion( QDataStream::Qt_4_0);
	unsigned char c0 = 34;		//socket 2
	unsigned char c1 = relay;	//relay
	unsigned char c2 = setTo;	//on-off
	unsigned char c3 = 0xFF;	//time

	out << (quint16)0;
	out << c0; out << c1; out << c2; out << c3;
	out.device()->seek(0);
	out << (quint16)(block.size() - sizeof(quint16) );

	if( socket_num == 1 )		tcpSocket2_1->write( block );
	else if( socket_num == 2 )	tcpSocket2_2->write( block );
	else if( socket_num == 3 )	tcpSocket2_3->write( block );
}

void MyClass::setSocket2State_Slot( int socket_num, int relay, int setTo, int time )
{
	QByteArray block;
	QDataStream out( &block, QIODevice::WriteOnly );
	out.setVersion( QDataStream::Qt_4_0);
	unsigned char c0 = 34;		//socket 2
	unsigned char c1 = relay;	//relay
	unsigned char c2 = setTo;	//on-off
	unsigned char c3 = time;	//time

	out << (quint16)0;
	out << c0; out << c1; out << c2; out << c3;
	out.device()->seek(0);
	out << (quint16)(block.size() - sizeof(quint16) );

	if( socket_num == 1 )		tcpSocket2_1->write( block );
	else if( socket_num == 2 )	tcpSocket2_2->write( block );
	else if( socket_num == 3 )	tcpSocket2_3->write( block );
}
void MyClass::drawPositionAuto(bool leftOk, bool rightOk)
{
	labelPosLeft->setPixmap(QPixmap(leftOk ? "image/pos_ok.png" : "image/pos_bad.png"));
	labelPosRight->setPixmap(QPixmap(rightOk ? "image/pos_ok.png" : "image/pos_bad.png"));
	if ( leftOk == false && rightOk == false )
		labelAuto->setGeometry(QRect(35, 5, 226, 83));
	else
		labelAuto->setGeometry(QRect(50, 5, 196, 83));
	{
		if(  leftOk == false )
			labelAuto->setGeometry(QRect(35, 5, 196, 83));
		if( rightOk == false )
			labelAuto->setGeometry(QRect(65, 5, 196, 83));
	}
}

void MyClass::drawPosition2Auto(bool left2Ok, bool right2Ok)
{
	if ( show2ndPos )
	{
		labelPos2Left->show();
		labelPos2Right->show();
	}

	labelPos2Left->setPixmap(QPixmap(left2Ok ? "image/pos_ok.png" : "image/pos_bad.png"));
	labelPos2Right->setPixmap(QPixmap(right2Ok ? "image/pos_ok.png" : "image/pos_bad.png"));

}
void MyClass::socketError_Slot( QAbstractSocket::SocketError err )
{
    strError1 = (err == QAbstractSocket::HostNotFoundError ?
                     "The host was not found." :
                     err == QAbstractSocket::RemoteHostClosedError ?
                     "The remote host is closed." :
                     err == QAbstractSocket::ConnectionRefusedError ?
                     "The connection was refused." :
					 QString( tcpSocket2_1->errorString() ) );

	strError2 = (err == QAbstractSocket::HostNotFoundError ?
					"The host was not found." :
					err == QAbstractSocket::RemoteHostClosedError ?
					"The remote host is closed." :
					err == QAbstractSocket::ConnectionRefusedError ?
					"The connection was refused." :
					QString( tcpSocket2_2->errorString() ) );

	strError3 = (err == QAbstractSocket::HostNotFoundError ?
					"The host was not found." :
					err == QAbstractSocket::RemoteHostClosedError ?
					"The remote host is closed." :
					err == QAbstractSocket::ConnectionRefusedError ?
					"The connection was refused." :
					QString( tcpSocket2_3->errorString() ) );

	if( !strError1.isEmpty() || strError1 == "Unknown error" )
	{
		socketDevices->setLink( 1, false );
		if( settings->chkboxEnableVk1->isChecked() )
		{
			tcpSocket2_1->disconnectFromHost();
			tcpSocket2_1->connectToHost( settings->enterVk1->text(), 9761 );
		}
	}
	if( !strError2.isEmpty() || strError2 == "Unknown error" )
	{
		socketDevices->setLink( 2, false );
		if( settings->chkboxEnableVk2->isChecked() )
		{
			tcpSocket2_2->disconnectFromHost();
			tcpSocket2_2->connectToHost( settings->enterVk2->text(), 9761 );
		}
	}
	if( !strError3.isEmpty() || strError3 == "Unknown error" )
	{
		socketDevices->setLink( 3, false );
		if( settings->chkboxEnableVk3->isChecked() )
		{
			tcpSocket2_3->disconnectFromHost();
			tcpSocket2_3->connectToHost( settings->enterVk3->text(), 9761 );
		}
	}
	//QMessageBox::information( NULL, textCodec->toUnicode( "socketErr" ),
	//strError, QMessageBox::Ok );
    //m_ptxtInfo->append(strError);
	qDebug( strError1 );
	qDebug( strError2 );
	qDebug( strError3 );
}

void MyClass::tabloStateTimer_Slot()
{
	stateHit = 3;
}

void MyClass::sayAboutZero_Slot(bool a)
{
	QMessageBox mess;
	if(a)
	{
		mess.setText( tr("Setting of zero successful.\nPress 'Ok' to continue.") );
	}
	else
	{
		mess.setText( tr("Setting of zero unsuccessful.\nPress 'Ok' to continue.") );
	}
	mess.setStandardButtons( QMessageBox::Cancel );
	mess.setButtonText( QMessageBox::Cancel, "Ok" );
	mess.setIcon( QMessageBox::Warning );
	mess.setWindowTitle( tr( "Prom-Soft" ) );
	if( mess.exec() == QMessageBox::Cancel )
	{
		zeroBlock = false;
	}
}

//
QByteArray MyClass::makeStampPhoto( QByteArray srcBa, QString date_time, QString num_auto, int num_pos, QString weight, int num_pos2 )
{
	QBuffer buf( &srcBa );
	QImage img( srcBa );
	NumberAndTimestamp numberAndTimestamp;
	QImage image = numberAndTimestamp.updatePhoto( date_time, img, num_auto, num_pos, weight, num_pos2 );
	image.save( &buf, "JPEG" );

	return buf.buffer();
}
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Резервное копирование БД
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void MyClass::dbBackupDialog_Slot()
{
	dialogDBdump->show();
}

void MyClass::dbBackup_Slot()
{
	dialogDBdump->hide();
	waitW->show();

	QTimer::singleShot( 500, this, SLOT( dumpGo_Slot() ) );
}

void MyClass::dumpGo_Slot()
{
	QSettings settings_file( "settings.ini", QSettings::IniFormat );
    settings_file.setIniCodec( "Windows-1251" );
	QString address = "-h" + settings_file.value( "MySQL/address" ).toString();
	QString port = "-P" + settings_file.value( "MySQL/port" ).toString();
	//enc-dec pass
	QString pass = settings_file.value("MySQL/password").toString();
	QString pass_enc;
	if (!pass.startsWith("#"))
		pass_enc = Crypt::crypt(pass);
	else
	{
		pass_enc = pass.remove(0, 1);
		pass = Crypt::decrypt(pass_enc);
	}
	//

	pass = "-p" + pass;
	QStringList tables;
	tables.append( dataBaseName );
	if( !dbPhoto->isChecked() )
	{
		tables.append( "autoreception" );
		tables.append( "dictionaries" );
		tables.append( "logs" );
		tables.append( "reportsweight" );
		tables.append( "taraauto" );
		tables.append( "ttn_data" );
		tables.append( "ttn_data_2" );
		tables.append( "ttn_data_3" );
		tables.append( "ttn_data_4" );
		tables.append( "users" );
	}
	if(!QDir("backup").exists())//проверяем, существует ли директория
	{
		QMessageBox *mb = new QMessageBox();
		mb->setText( tr("Folder to save the dump file can not be found. A new folder will be created."));
		mb->setStandardButtons(QMessageBox::Cancel);
		mb->setButtonText(QMessageBox::Cancel, "Ok");
		mb->setIcon(QMessageBox::Warning);
		mb->setWindowTitle( tr("Prom-Soft"));
		mb->exec();
		delete mb;
		QDir().mkdir("backup");// если нет - предупреждаем пользователя и создаём.
	}
	dumpProcess = new QProcess(this);
	QStringList args;
	args << "-uroot" << address << port << pass << tables;
	lastNameOfDump = QString( "dump_%1.sql" ).arg( QDateTime::currentDateTime().toString( "yyyy-MM-dd_hh-mm-ss" ) );
	dumpProcess->setStandardOutputFile( "backup//" + lastNameOfDump );	//создаём файл с дампом в папке backup

	connect( dumpProcess, SIGNAL( finished(int, QProcess::ExitStatus) ), this, SLOT( dbPackingFinished_Slot(int, QProcess::ExitStatus) ) );

	dumpProcess->start( "mysqldump", args );
}

void MyClass::dbPackingFinished_Slot(int exitCode, QProcess::ExitStatus exitStatus)//завершение резервирования данных
{
	waitW->hide();

	if( exitCode == 0 && exitStatus == QProcess::NormalExit )//если успешно
	{
		QMessageBox mess;
		mess.setText( tr( "The dump was successfully created. Filename: " ) + lastNameOfDump );
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Information );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();//вывод сообщения с именем файла
	}
	else//если нет - ошибка
	{
		QMessageBox mess;
		mess.setText( tr( "Creating dump failed." ) );
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Critical );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();
	}

	lastNameOfDump.clear();
}

void MyClass::dbRestoreDialog_Slot()//восстановление - формирование диалогового окна
{
	listWidget->clear();
	QDir dir( "backup" );
    dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
    QFileInfoList list = dir.entryInfoList();
	if(!list.isEmpty())//проверка наличия файла дампа
	{
        for( int i = 0; i < list.size(); ++i )
		{
            QFileInfo fileInfo = list.at(i);
		    listWidget->addItem( QString( "%1" ).arg( fileInfo.fileName() ) );
        }
	    listWidget->setCurrentRow( 0 );
	    dialogDB->show();//если да - отображаем список найденных файлов в окне
	}
	else//если нет - ошибка
	{
		QMessageBox *mb = new QMessageBox();
		mb->setText( tr("Dump file can not be found."));
		mb->setStandardButtons(QMessageBox::Cancel);
		mb->setButtonText(QMessageBox::Cancel, "Ok");
		mb->setIcon(QMessageBox::Critical);
		mb->setWindowTitle( tr("Prom-Soft"));
		mb->exec();
		delete mb;
	}

}

void MyClass::dbRestore_Slot()
{
	QString nameOfDump = listWidget->currentItem()->text();

	if( !nameOfDump.isEmpty() )
	{
		dialogDB->hide();
		waitW->show();

		QTimer::singleShot( 500, this, SLOT( restoreGo_Slot() ) );
	}
}

void MyClass::restoreGo_Slot()//восстановление - подключение к БД, передача файла дампа потоку
{
	QString nameOfDump = listWidget->currentItem()->text();

	if( !nameOfDump.isEmpty() )
	{
		QSettings settings_file( "settings.ini", QSettings::IniFormat );
		settings_file.setIniCodec( "Windows-1251" );
		QString address = "-h" + settings_file.value( "MySQL/address" ).toString();
		QString port = "-P" + settings_file.value( "MySQL/port" ).toString();

		//enc-dec pass
		QString pass = settings_file.value("MySQL/password").toString();
		QString pass_enc;
		if (!pass.startsWith("#"))
			pass_enc = Crypt::crypt(pass);
		else
		{
			pass_enc = pass.remove(0, 1);
			pass = Crypt::decrypt(pass_enc);
		}
		//

		pass = "-p" + pass;

		restoreProcess = new QProcess(this);
		QStringList args;
		args << "-uroot" << address << port << pass << dataBaseName;
		restoreProcess->setStandardInputFile( "backup//" + nameOfDump );

		connect( restoreProcess, SIGNAL( finished(int, QProcess::ExitStatus) ), this, SLOT( dbUnpackingFinished_Slot(int, QProcess::ExitStatus) ) );

		restoreProcess->start( "mysql", args );
	}
}

void MyClass::dbUnpackingFinished_Slot(int exitCode, QProcess::ExitStatus exitStatus)// завершение восстановления
{
	waitW->hide();

	if( exitCode == 0 && exitStatus == QProcess::NormalExit )// если успешно - сообщение с просьбой перезапуска
	{
		QMessageBox mess;
		mess.setText( tr( "The database is restored. Please restart the program." ) );
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Information );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();
	}
	else// если нет - ошибка.
	{
		QMessageBox mess;
		mess.setText( tr( "Unexpected error." ) );
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Critical );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();
	}
}

void MyClass::dbOpenFolder_Slot()// просмотр папки backup
{
	QDir dir("backup");
	if(dir.exists())//если существет - открыть
	    QDesktopServices::openUrl( QUrl( "backup" ) );
	else//если нет - ошибка
	{
		QMessageBox *mb = new QMessageBox();
		mb->setText( tr("Dump folder can not be found. Make your first dump for creation it."));
		mb->setStandardButtons(QMessageBox::Cancel);
		mb->setButtonText(QMessageBox::Cancel, "Ok");
		mb->setIcon(QMessageBox::Critical);
		mb->setWindowTitle( tr("Prom-Soft"));
		mb->exec();
		delete mb;
	}
}

//
void MyClass::showTaraDictionary_Slot()
{
	spravochnikTaraObj->showWindow();
}

void MyClass::VP01_svetofor_timer_Slot()
{
	serialPortClassObject->writeData2Port( settings->serialPort, VP01_svetofor_request, VP01_svetofor_request.length() );
}

void MyClass::askSensorPassword()
{
	sensorsPasswordDialog->show();
}

void MyClass::okButtonDC_Slot()
{
	if( enterPasswordDC->text() == "Pr0m-S0ftse" )
	{
		dialogVPSensorsObj->show();
		sensorsPasswordDialog->hide();
	}
	else
	{
		QToolTip::showText( enterPasswordDC->mapToGlobal(QPoint()), "Неверный пароль."  );
	}
	enterPasswordDC->clear();
}

void MyClass::cancelButtonDC_Slot()
{
	sensorsPasswordDialog->hide();
	enterPasswordDC->clear();
}

void MyClass::timer_PositionS2_Slot()
{
	QByteArray data;
	data.append(settings->check_PositionUseS1->isChecked() ? (char)0x32 : (char)0x23);
	qDebug()<<"send"<<data;	//s2
	tcpSocket_PositionS2->write( data );
}

void MyClass::readSocket_PositionS2_Slot()
{
	QByteArray message;
	forever
	{
		if( tcpSocket_PositionS2->bytesAvailable() < 5 ) break;

		message = tcpSocket_PositionS2->readAll();

		char data[5] = {0};
		//data[0] = (char)message.at(0); // 15 // получена неизвестная команда
		//data[1] = (char)message.at(1); // 0
		//data[2] = (char)message.at(2); // 1

		data[0] = (char)message.at(0); // 35
		data[1] = (char)message.at(1); // in 0
		data[2] = (char)message.at(2); // in 1
		data[3] = (char)message.at(3); // relay 0
		data[4] = (char)message.at(4); // relay 1

		//0x32
		//Назначение: Состояние входов.
		//Данные: 0 байт Цифровой вход №0: 0–Замкнут; 1–Разомкнут;
		//data[0] = (char)message.at(0); // 35
		//data[1] = (char)message.at(1); // in 0
		//data[2] = (char)message.at(2); // in 1
		//data[3] = (char)message.at(3); // in 2
		//data[4] = (char)message.at(4); // in 3
		qDebug() <<"in"<<message;
		qDebug()<<" -data"<<data[0];
		qDebug()<<" 1 -"<<data[1];
		qDebug()<<" 2 -"<<data[2];
		qDebug()<<" "<<data[1];
		if( data[0] != (settings->check_PositionUseS1->isChecked() ? (char)0x32 : (char)0x23) )
		{
			qDebug() << "tcpSocket_PositionS2, unknown package:" << data;
			return;
		}

		int in_1 = data[1];
		int in_2 = data[2];
		int in_3 = data[3];
		int normal = 1;

		if( settings->check_PositionS2->isChecked() ) normal = 0;

		isPositionOK = false;
		show2ndPos = false;
		isPositionLeftOK = true;
		isPositionRightOK  = true;

		if( in_1 == normal && in_2 == normal ) // лучи не пересечены
		{
			isPositionOK = true;
		}
		else
		{
			if( in_1 != normal ) isPositionLeftOK = false;
			if( in_2 != normal ) isPositionRightOK = false;
		}

		drawPositionAuto( isPositionLeftOK, isPositionRightOK );

		if( settings->check_PositionUseS1->isChecked() )
		{
			if( settings->check_PositionUse4ch->isChecked() )
			{

				int in_4 = data[4];
				show2ndPos = true;
				isPositionLeft2OK = true;
				isPositionRight2OK  = true;
				if( in_3 != normal )
				{
					isPositionLeft2OK = false;
					isPositionOK = false;
				}
				if( in_4 != normal )
				{
					isPositionRight2OK = false;
					isPositionOK = false;
				}
				drawPosition2Auto( isPositionLeft2OK, isPositionRight2OK );
			}
			else
			{

				if( in_1 == normal && in_2 == normal ) // лучи не пересечены
				{
					isPositionOK = true;
				}
				else
				{
					if( in_1 != normal ) isPositionLeftOK = false;
					if( in_2 != normal ) isPositionRightOK = false;
				}

				static int prev_state_cabinet = 0;
				if( in_3 != prev_state_cabinet ) // шкаф открыт
				{
					mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
					mysqlProccessObject->saveEventToLog(tr("CABINET IS ") + (in_3 ? tr("CLOSED") : tr("OPENED")) );
					prev_state_cabinet = in_3;
				}
			}
		}
		if( settings->check_PositionIgnore->isChecked() )
		{
			isPositionOK = true;
		}
	}
}

void MyClass::socketError_PositionS2_Slot( QAbstractSocket::SocketError  err)
{
	QString strErrorMain1 = (err == QAbstractSocket::HostNotFoundError ?
					"The host was not found." :
					err == QAbstractSocket::RemoteHostClosedError ?
					"The remote host is closed." :
					err == QAbstractSocket::ConnectionRefusedError ?
					"The connection was refused." :
					QString( tcpSocket_PositionS2->errorString() ) );
	if( !strErrorMain1.isEmpty() || strErrorMain1 == "Unknown error" )
	{
		if( settings->group_PositionS2->isChecked() )
		{
			static int max_recons = 0;
			++max_recons;
			if(max_recons > 100) return;

			tcpSocket_PositionS2->disconnectFromHost();
			tcpSocket_PositionS2->connectToHost( settings->lineEdit_PositionS2->text(), 9761 );
			isPositionOK = false;
		}
	}
}

bool MyClass::getPosState()
{
	if( settings->group_PositionS2->isChecked() )
	{
		if( !isPositionOK )
		{
			QMessageBox::warning( NULL, tr("Positioning"),
				tr("The vehicle is not correctly standing on the weighing platform!"), QMessageBox::Ok );
			return false;
		}
	}
	return true;
}

bool MyClass::verifyAutoPosition( int weight, bool silent )
{
	bool result = false;
	QMessageBox mess;

	if( weight < MIN_WEIGHT_PLATFORM )
	{
		if (!silent)
		{
			mess.setText( tr("The weight on the weighing platform is less than the minimum ")
				+ QString( "%1 kg !" ).arg( MIN_WEIGHT_PLATFORM ) );
			mess.setStandardButtons( QMessageBox::Cancel );
			mess.setButtonText( QMessageBox::Cancel, "Ok" );
			mess.setIcon( QMessageBox::Warning );
			mess.setWindowTitle( tr( "Prom-Soft" ) );
			mess.exec();
		}
		labelAuto->hide();
		result = false;
	}
	else
	{
		groupBoxPlatform->show();
		labelAuto->show();
		result = true;
	}

	return result;
}

//Lamps Socket2
void MyClass::timer_Lamps_Slot()
{
	QByteArray data;
    data.append((char)0x23);	//s2
	tcpSocket_Lamps->write( data );
}

void MyClass::readSocket_Lamps_Slot()
{
	QByteArray message;
	forever
	{
		if( tcpSocket_Lamps->bytesAvailable() < 5 ) break;

		message = tcpSocket_Lamps->readAll();

		char data[5] = {0};
		data[0] = (char)message.at(0); // 35
		data[1] = (char)message.at(1); // in 0
		data[2] = (char)message.at(2); // in 1
		data[3] = (char)message.at(3); // relay 0
		data[4] = (char)message.at(4); // relay 1

		if( data[0] != 0x23 )
		{
			qDebug() << "tcpSocket_Lamps, unknown package:" << data;
			return;
		}

		int rele_0 = data[3];
		int rele_1 = data[4];

		static int cur_color_0 = -1;
		static int cur_color_1 = -1;
	}
}

void MyClass::socketError_Lamps_Slot( QAbstractSocket::SocketError  err)
{
	QString strErrorMain1 = (err == QAbstractSocket::HostNotFoundError ?
					"The host was not found." :
					err == QAbstractSocket::RemoteHostClosedError ?
					"The remote host is closed." :
					err == QAbstractSocket::ConnectionRefusedError ?
					"The connection was refused." :
					QString( tcpSocket_Lamps->errorString() ) );
	if( !strErrorMain1.isEmpty() || strErrorMain1 == "Unknown error" )
	{
		if( settings->group_Rfid->isChecked() )
		{
			static int max_recons = 0;
			++max_recons;
			if(max_recons > 100) return;

			tcpSocket_Lamps->disconnectFromHost();
			tcpSocket_Lamps->connectToHost( settings->lineEdit_Lamps->text(), 9761 );
		}
	}
}

void MyClass::timer_SvetoforS2_Slot()
{
	QByteArray data;
    data.append((char)0x23);	//s2
	tcpSocket_SvetoforS2->write( data );
}

void MyClass::readSocket_SvetoforS2_Slot()
{
	QByteArray message;
	forever
	{
		if( tcpSocket_SvetoforS2->bytesAvailable() < 5 ) break;

		message = tcpSocket_SvetoforS2->readAll();

		char data[5] = {0};
		data[0] = (char)message.at(0); // 35
		data[1] = (char)message.at(1); // in 0
		data[2] = (char)message.at(2); // in 1
		data[3] = (char)message.at(3); // relay 0
		data[4] = (char)message.at(4); // relay 1

		if( data[0] != 0x23 )
		{
			qDebug() << "tcpSocket_SvetoforS2, unknown package:" << data;
			return;
		}

		int rele_0 = data[3];
		int rele_1 = data[4];

		static int cur_color_0 = -1;
		static int cur_color_1 = -1;

		if( rele_0 != cur_color_0 ) // изменился цвет 1 светофора
		{
			if( rele_0 == 0 ) // красный
			{
				QPixmap pix;
				pix.load( "image/s_red.png", "PNG", Qt::AutoColor );
				label_Svetovor1->setPixmap( pix );
				label_Svetovor1->raise();
			}
			else if( rele_0 == 1 ) // зеленый
			{
				QPixmap pix;
				pix.load( "image/s_green.png", "PNG", Qt::AutoColor );
				label_Svetovor1->setPixmap( pix );
				label_Svetovor1->raise();
			}
			cur_color_0 = rele_0;
		}

		if( rele_1 != cur_color_1 ) // изменился цвет 2 светофора
		{
			if( rele_1 == 0 ) // красный
			{
				QPixmap pix;
				pix.load( "image/s_red.png", "PNG", Qt::AutoColor );
				label_Svetovor2->setPixmap( pix );
				label_Svetovor2->raise();
			}
			else if( rele_1 == 1 ) // зеленый
			{
				QPixmap pix;
				pix.load( "image/s_green.png", "PNG", Qt::AutoColor );
				label_Svetovor2->setPixmap( pix );
				label_Svetovor2->raise();
			}
			cur_color_1 = rele_1;
		}
	}
}

void MyClass::socketError_SvetoforS2_Slot( QAbstractSocket::SocketError  err)
{
	QString strErrorMain1 = (err == QAbstractSocket::HostNotFoundError ?
					"The host was not found." :
					err == QAbstractSocket::RemoteHostClosedError ?
					"The remote host is closed." :
					err == QAbstractSocket::ConnectionRefusedError ?
					"The connection was refused." :
					QString( tcpSocket_SvetoforS2->errorString() ) );
	if( !strErrorMain1.isEmpty() || strErrorMain1 == "Unknown error" )
	{
		if( settings->group_SvetoforS2->isChecked() )
		{
			static int max_recons = 0;
			++max_recons;
			if(max_recons > 100) return;

			tcpSocket_SvetoforS2->disconnectFromHost();
			tcpSocket_SvetoforS2->connectToHost( settings->lineEdit_SvetoforS2->text(), 9761 );
		}
	}
}

void MyClass::timer_BarrierS2_Slot()
{
	QByteArray data;
    data.append((char)0x23);	//s2
	tcpSocket_BarrierS2->write( data );
}

void MyClass::readSocket_BarrierS2_Slot()
{
	QByteArray message;
	forever
	{
		if( tcpSocket_BarrierS2->bytesAvailable() < 5 ) break;

		message = tcpSocket_BarrierS2->readAll();

		char data[5] = {0};
		data[0] = (char)message.at(0); // 35
		data[1] = (char)message.at(1); // in 0
		data[2] = (char)message.at(2); // in 1
		data[3] = (char)message.at(3); // relay 0
		data[4] = (char)message.at(4); // relay 1

		if( data[0] != 0x23 )
		{
			qDebug() << "tcpSocket_BarrierS2, unknown package:" << data;
			return;
		}

		int rele_0 = data[3];
		int rele_1 = data[4];

	}
}

void MyClass::socketError_BarrierS2_Slot( QAbstractSocket::SocketError  err)
{
	QString strErrorMain1 = (err == QAbstractSocket::HostNotFoundError ?
					"The host was not found." :
					err == QAbstractSocket::RemoteHostClosedError ?
					"The remote host is closed." :
					err == QAbstractSocket::ConnectionRefusedError ?
					"The connection was refused." :
					QString( tcpSocket_BarrierS2->errorString() ) );
	if( !strErrorMain1.isEmpty() || strErrorMain1 == "Unknown error" )
	{
		if( settings->group_BarrierS2->isChecked() )
		{
			static int max_recons = 0;
			++max_recons;
			if(max_recons > 100) return;

			tcpSocket_BarrierS2->disconnectFromHost();
			tcpSocket_BarrierS2->connectToHost( settings->lineEdit_BarrierS2->text(), 9761 );
		}
	}
}

void MyClass::set_Svetofor( int rele, int setTo )
{
	unsigned char c0 = 34;		//socket 2
	unsigned char c1 = rele;	//relay
	unsigned char c2 = setTo;	//on-off
	unsigned char c3 = 0;		//time

	QByteArray block;
	block.append( c0 );
	block.append( c1 );
	block.append( c2 );
	block.append( c3 );

	tcpSocket_SvetoforS2->write( block );
}

void MyClass::set_Barrier( int rele, int setTo )
{
	unsigned char c0 = 34;		//socket 2
	unsigned char c1 = rele;	//relay
	unsigned char c2 = setTo;	//on-off
	unsigned char c3 = 0;		//time

	QByteArray block;
	block.append( c0 );
	block.append( c1 );
	block.append( c2 );
	block.append( c3 );

	tcpSocket_BarrierS2->write( block );
}

void MyClass::set_Lamp( int rele, int setTo )
{
	unsigned char c0 = 34;		//socket 2
	unsigned char c1 = rele;	//relay
	unsigned char c2 = setTo;	//on-off
	unsigned char c3 = 0;		//time

	QByteArray block;
	block.append( c0 );
	block.append( c1 );
	block.append( c2 );
	block.append( c3 );

	tcpSocket_Lamps->write( block );
}

void MyClass::set_Lamp1_off()
{
	set_Lamp(0, 0);
}

void MyClass::set_Lamp1_on()
{
	set_Lamp(0, 1);
}

void MyClass::set_Lamp2_off()
{
	set_Lamp(1, 0);
}

void MyClass::set_Lamp2_on()
{
	set_Lamp(1, 1);
}

void MyClass::socketError_tcpSocket_Tablo1_Slot( QAbstractSocket::SocketError  err)
{
	QString strErrorMain1 = (err == QAbstractSocket::HostNotFoundError ?
					"The host was not found." :
					err == QAbstractSocket::RemoteHostClosedError ?
					"The remote host is closed." :
					err == QAbstractSocket::ConnectionRefusedError ?
					"The connection was refused." :
					QString( tcpSocket_Tablo1->errorString() ) );
	if( !strErrorMain1.isEmpty() || strErrorMain1 == "Unknown error" )
	{
		if( settings->radioTablo_IP1->isChecked() )
		{
			static int max_recons = 0;
			++max_recons;
			if(max_recons > 100) return;

			tcpSocket_Tablo1->disconnectFromHost();
			tcpSocket_Tablo1->connectToHost( settings->enterTablo_IP1->text(), 9761 );
		}
	}
}

void MyClass::socketError_tcpSocket_Tablo2_Slot( QAbstractSocket::SocketError  err)
{
	QString strErrorMain1 = (err == QAbstractSocket::HostNotFoundError ?
					"The host was not found." :
					err == QAbstractSocket::RemoteHostClosedError ?
					"The remote host is closed." :
					err == QAbstractSocket::ConnectionRefusedError ?
					"The connection was refused." :
					QString( tcpSocket_Tablo2->errorString() ) );
	if( !strErrorMain1.isEmpty() || strErrorMain1 == "Unknown error" )
	{
		if( settings->radioTablo_IP2->isChecked() )
		{
			static int max_recons = 0;
			++max_recons;
			if(max_recons > 100) return;

			tcpSocket_Tablo2->disconnectFromHost();
			tcpSocket_Tablo2->connectToHost( settings->enterTablo_IP2->text(), 9761 );
		}
	}
}

void MyClass::timerWeight_Slot()
{
	QStringList rl = settings->loadVP_REquests();
	int idx = settings->vectorTypesVP_main.at( 0 )->currentIndex();
	tcpWeight->write( rl[idx] );
}

void MyClass::timerConnection_Slot()
{
#ifndef DEBUG
	if( isON_flag == false )
    {

		isVPConnected = false;
		weightScales_01 = QString::number( -1 );
		this->vectorLCD_Indicator.at( 0 )->setText( "------" );
		groupBoxPlatform->hide();
    }
	else
#endif
	{
		isVPConnected = true;
		groupBoxPlatform->show();
	}

    if( isON_flag == true )
        isON_flag = false;
}

void MyClass::readWeight_Slot()
{
	//test2->setText( "r: "+ QTime::currentTime().toString( "hh:mm:ss:zzz" ) );
    // AB+00001071e
    isON_flag = true;

    QTcpSocket* pClientSocket = (QTcpSocket*)sender();
	groupBoxPlatform->show();
	parserVP( pClientSocket->readAll() );
	if (verifyAutoPosition( weightScales_01.toFloat(), true) == true )
	{
		labelAuto->show();
		set_Lamp(0, 1);
	}
	else
		labelAuto->hide();

}

void MyClass::socketErrorWeight_Slot( QAbstractSocket::SocketError err )
{
	QString strErrorMain1 = (err == QAbstractSocket::HostNotFoundError ?
					"The host was not found." :
					err == QAbstractSocket::RemoteHostClosedError ?
					"The remote host is closed." :
					err == QAbstractSocket::ConnectionRefusedError ?
					"The connection was refused." :
					QString( tcpWeight->errorString() ) );
	if( !strErrorMain1.isEmpty() || strErrorMain1 == "Unknown error" )
	{
		if( settings->group_TCPWeight->isChecked() )
		{
			static int max_recons = 0;
			++max_recons;
			if(max_recons > 100) return;

			tcpWeight->disconnectFromHost();
			tcpWeight->connectToHost( settings->lineEditTCP_IP->text(), 9761 );
		}
	}
}

void MyClass::makeHiddenPhotos_Slot()
{
	// прятки
	if( !optionsObj->isCheked[optionsObj->modules.indexOf("camera")] ) return;

	bool cam1;
	bool cam2;
	bool cam3;
	bool cam4;

	settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 0 ); // камера №1
	cam1 = currentSettingsIP_CamPtr->cam_photofix_en;

	settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 1 ); // камера №2
	cam2 = currentSettingsIP_CamPtr->cam_photofix_en;

	settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 2 ); // камера №3
	cam3 = currentSettingsIP_CamPtr->cam_photofix_en;

	settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 3 ); // камера №4
	cam4 = currentSettingsIP_CamPtr->cam_photofix_en;


	if( cam1 == true || cam2 == true || cam3 == true || cam4 == true )
	{
		timerRequestIP_Cams->blockSignals( true );
		timerRequestIP_Cams->stop();
		this->states_ip_cams = SNAPSHOT;
		//Sleep( 1000 );
	}

	// Делаем фотоснимки
	if( cam1 == true )
	{
		settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 0 );

		sendRequestIP_Cam( networkManagersPhotofixHidden[0], currentSettingsIP_CamPtr->ip_address,
			currentSettingsIP_CamPtr->port.toInt(), currentSettingsIP_CamPtr->login,
			currentSettingsIP_CamPtr->password, 0 );
	}

	if( cam2 == true )
	{
		settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 1 );

		sendRequestIP_Cam( networkManagersPhotofixHidden[1], currentSettingsIP_CamPtr->ip_address,
			currentSettingsIP_CamPtr->port.toInt(), currentSettingsIP_CamPtr->login,
			currentSettingsIP_CamPtr->password, 1 );
	}

	if( cam3 == true )
	{
		settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 2 );

		sendRequestIP_Cam( networkManagersPhotofixHidden[2], currentSettingsIP_CamPtr->ip_address,
			currentSettingsIP_CamPtr->port.toInt(), currentSettingsIP_CamPtr->login,
			currentSettingsIP_CamPtr->password, 2 );
	}

	if( cam4 == true )
	{
		settings->getCurrentSettings_IP_CamsIniFile( currentSettingsIP_CamPtr, 3 );

		sendRequestIP_Cam( networkManagersPhotofixHidden[3], currentSettingsIP_CamPtr->ip_address,
			currentSettingsIP_CamPtr->port.toInt(), currentSettingsIP_CamPtr->login,
			currentSettingsIP_CamPtr->password, 3 );
	}

	timeOutSnapshotIP_Cams->start();

	timerRequestIP_Cams->blockSignals( false );
	this->states_ip_cams = NO_SNAPSHOT;
	timerRequestIP_Cams->start();
}

void MyClass::finishedHTTP_Hidden_Cams( QNetworkReply *reply )
{
	QString name = sender()->objectName();
	int camNum = name.right( 1 ).toInt(); // сигнал пришел от камеры 0..3

	if( reply->error() == QNetworkReply::NoError )
	{
		// Преобразовать screenshot изображения от IP-камеры в массив байт
		QByteArray ba = reply->readAll();

		if( name == networkManagersPhotofixHidden[camNum]->objectName() ) // скрытое взвеш
		{
			updateHiddenImages( &ba, camNum );
		}
	}
	else
	{
		qDebug() << reply->errorString();
	}

	reply->deleteLater();
}

void MyClass::updateHiddenImages( QByteArray *ba, int camNum )
{
	if( hiddenWeighing->hidden_id > 0 )
	{
		mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );
		query.prepare( QString( "UPDATE images SET photo_%1=:photo WHERE ID='%2';" ).arg( camNum+1 ).arg( hiddenWeighing->hidden_id ) );
	    query.bindValue( ":photo", *ba );
		query.exec();
	}
}

void MyClass::write_1C()
{
	int w = vectorLCD_Indicator.at( 0 )->text().toInt();
	if( w >= MIN_WEIGHT_PLATFORM )
	{
		QString w_string = QString( "%1" ).arg( w );

		//Шифруем )  ------------
		int i = 1;
		for(QString::iterator it = w_string.begin(); it != w_string.end(); ++it,++i)
		{
			char ch = it->toLatin1();
			if ( i%2 )
				ch += i * 2  + 12;
			else
				ch -= i * 2  - 48;
			*it = QChar::fromLatin1(ch);
		}

		QFile textFile( pathFile1C + "\\1Cv" );

		if( textFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
		{
			QTextStream writeStream( &textFile );
			writeStream << w_string;
			writeStream.flush();
			textFile.flush();
			textFile.close();
		}
	}
	else
	{
		clean_1C();
	}
}

void MyClass::clean_1C()
{
	QFile textFile( pathFile1C + "\\1Cv" );
	if( textFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
	{
		QTextStream writeStream( &textFile );
		writeStream << "";
		writeStream.flush();
		textFile.flush();
		textFile.close();
		//textFile.remove();
	}
}

void MyClass::timer_Trial_Enable()
{
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	if( mysqlProccessObject->timer_trial() == false )
	{
		if( mysqlProccessObject->add_trial() == false )
		{
			if( mysqlProccessObject->check_trial() == false )
			{
				int result = QMessageBox::question( NULL,
				tr("Software License"),
				tr("The trial version has expired!\nPlease activate the software\nTechnical support - 093 009 9990 \n 'Prom-Soft'"),
				QMessageBox::Ok, QMessageBox::Cancel );

			if( result == QMessageBox::Ok )
			{
				buttonAutoComeIn->setEnabled( false );
				buttonAutoComeOut->setEnabled( false );
				optionsObj->show();
			}
			else
			{
				buttonAutoComeIn->setEnabled( false );
				buttonAutoComeOut->setEnabled( false );
			}
			}
		}
	}
}


void MyClass::split_contacts(QString path)
{
	QFile file(path);
	file.open( QIODevice::ReadOnly | QIODevice::Text );
    QTextStream fileStream(&file);

	QFileInfo fileInfo(file.fileName());
	QString path1(fileInfo.absoluteDir().absolutePath());
	QString str = "";
	int i = 0;

	do
	{
		QFile file_out(path1 + "/" + "contact" + ( i + 1 > 9 ? "0" : ""  ) + QString::number(++i) + ".vcf" );
		file_out.open( QIODevice::Append | QIODevice::Text );
		QTextStream fileStreamOut(&file_out);
		while ( (str = fileStream.readLine(250)) != "END:VCARD" )
		{
			fileStreamOut << str << endl;
		}
		fileStreamOut << str << endl;
		file_out.close();

	}
	while(! fileStream.atEnd() );
	file.close();
}

void MyClass::CardsMode_Slot(bool checked)
{
	int cntColumns;
	QList<QString> headersText;

	QByteArray buttInGeom;
	QByteArray buttOutGeom;

	QSettings settingsT("settings.ini", QSettings::IniFormat);
	settingsT.setIniCodec("Windows-1251");

	if (checked)  // show table in cards mode
	{
		cntColumns = getColumnsHeadersTableAcceptCards().count(); // количество колонок в таблице
		headersText = getColumnsHeadersTableAcceptCards(); // считать все заглавия  таблицы  из списка
		tableAutoReception->setColumnCount(cntColumns);
		tableAutoReception->setColumnHidden(1, true);
		tableAutoReception->setColumnHidden(4, true);
		tableAutoReception->setColumnHidden(5, true);
		tableAutoReception->setColumnHidden(12, true);
		tableAutoReception->setColumnHidden(13, true);

		buttInGeom = buttonAutoComeIn->saveGeometry();
		buttOutGeom = buttonAutoComeOut->saveGeometry();
		radioButtAutoComeIn->setVisible(false);
		radioButtAutoComeOut->setVisible(false);
		disconnect(buttonAutoComeIn, SIGNAL(clicked()), this, SLOT(buttonAutoComeIn_Clicked()));
		disconnect( tableAutoReception, SIGNAL( cellDoubleClicked( int, int ) ), this, SLOT( shippingAuto( int, int ) ) );
		disconnect( tableAutoReception->horizontalHeader(), SIGNAL( sectionDoubleClicked( int ) ), this, SLOT( headerShippingAuto_Clicked( int ) ) );
		connect(buttonAutoComeIn, SIGNAL(clicked()), registration_rfidObj, SLOT(showForm()));
		buttonAutoComeIn->setMinimumWidth(180);
		buttonAutoComeIn->setText(tr("Registration"));
		buttonAutoComeOut->hide();
		buttonAutoComeIn->update();


		QRect screenResolution = qApp->desktop()->screenGeometry();
		tableViewReports->setMinimumHeight(screenResolution.height()/2 - 340 );
		tableViewReports->setMaximumHeight(screenResolution.height()/2);
		//tableAutoReception->resizeColumnsToContents();
		tableAutoReception->setMinimumHeight(screenResolution.height()/2 - 380 );
		tableAutoReception->setMaximumHeight(screenResolution.height()/2 - 150 );
		tableViewReports->resizeColumnsToContents();

		tableViewReports->setColumnHidden(5, true);
		tableViewReports->setColumnHidden(6, true);

		tableViewReports->setVisible( true );
		tableViewReports->update();

		disconnect( reportViewButton, SIGNAL( clicked() ), this, SLOT( showReportsWindow() ) );
		connect( reportViewButton, SIGNAL( clicked() ), reports_rfidObj, SLOT( showForm() ) );
		cardReaderRfidObj->mode_active = true;

	}

	else  // show accepttable
	{
		cntColumns = getColumnsHeadersTableAutoAcception().count(); // количество колонок в таблице
		headersText = getColumnsHeadersTableAutoAcception(); // считать все заглавия  таблицы  из списка
		tableAutoReception->setColumnCount(cntColumns);
		tableAutoReception->setColumnHidden(0, false);
		tableAutoReception->setColumnHidden(1, false);
		tableAutoReception->setColumnHidden(4, false);
		tableAutoReception->setColumnHidden(5, false);
		tableAutoReception->setColumnHidden(12, false);
		tableAutoReception->setColumnHidden(13, false);

		if (!buttInGeom.isEmpty()) buttonAutoComeIn->restoreGeometry(buttInGeom);
		if (!buttOutGeom.isEmpty()) buttonAutoComeOut->restoreGeometry(buttOutGeom);
		disconnect(buttonAutoComeIn, SIGNAL(clicked()), registration_rfidObj, SLOT(showForm()));
		connect(buttonAutoComeIn, SIGNAL(clicked()), this, SLOT(buttonAutoComeIn_Clicked()));
		radioButtAutoComeIn->setVisible(true);
		radioButtAutoComeOut->setVisible(true);
		buttonAutoComeIn->setMinimumWidth(120);
		buttonAutoComeIn->setText(tr("Check in"));
		buttonAutoComeIn->update();
		buttonAutoComeOut->show();

		tableViewReports->setVisible( false );
		QRect screenResolution = qApp->desktop()->screenGeometry();
		tableAutoReception->setMinimumHeight(screenResolution.height()/2 - 280 );
		tableAutoReception->setMaximumHeight(screenResolution.height() - 250 );

		disconnect( reportViewButton, SIGNAL( clicked() ), reports_rfidObj, SLOT( showForm() ) );
		connect( reportViewButton, SIGNAL( clicked() ), this, SLOT( showReportsWindow() ) );
		connect( tableAutoReception, SIGNAL( cellDoubleClicked( int, int ) ), this, SLOT( shippingAuto( int, int ) ) );
		connect( tableAutoReception->horizontalHeader(), SIGNAL( sectionDoubleClicked( int ) ), this, SLOT( headerShippingAuto_Clicked( int ) ) );

		cardReaderRfidObj->mode_active = false;
	}

	for (int j = 0; j < cntColumns; j++)
	{
		tableAutoReception->setHorizontalHeaderLabels(headersText);
		tableAutoReception->setAlternatingRowColors(true);
	}

	if (checked)
	{
		tableAutoReception->setColumnWidth(0, 190); //дата
		tableAutoReception->setColumnWidth(12, 60); //порядк. номер
		for (int i = 0; i < tableAutoReception->model()->columnCount() - 1; i++)
		{
			tableAutoReception->setColumnWidth(i, settingsT.value(QString("columnSizes/cc%1").arg(i), 120).toInt());
		}
		show_CardsAcception();
	}
	else
	{

		for (int i = 0; i < cntColumns; i++)
		{
			tableAutoReception->setColumnWidth(i, settingsT.value(QString("columnSizes/c%1").arg(i), 120).toInt());
		}
		show_InAutoAcception();
	}
	//tableAutoReception->update();


}

void MyClass::reopenRfid_Slot()
{
	cardReaderRfidObj->reopenRfid();
}

//
void MyClass::getRfidCode_Slot(QString rfid_code)
{
	static QString rfid_code1 = "";
	//serialPortFunc *serialPortFunc = serialPortFunc::instance();
	mysqlProccess *mysqlObj = mysqlProccess::instance();
	QByteArray dataSocket;

	if (verifyAutoPosition(weightScales_01.toFloat(), false) == false)
	{

		mysqlObj->saveEventToLog(QString(tr("The vehicle is not correctly standing on the weighing platform!")));

		set_Lamp(1, 1);
		QTimer::singleShot(5000, this, SLOT(set_Lamp2_off()));

		return;
	}

	// read spravochniki
	QList< QString > data = mysqlObj->getRegistrationData(rfid_code);
	
	if (data.size() > 0) // если данные есть
	{
		bool chkAutoTara = data.at(8) == "1" ? true : false;
		QList< QString > data2;

			   dataSocket[0] = 0x22; // 34
		       dataSocket[1] = 0; // in 0 or in 1
		       dataSocket[2] = 1;                                    
		       dataSocket[3] = 50;	
	           tcpSocket_Lamps->write( dataSocket );


		if (mysqlObj->hasRecordMainTable(data2, rfid_code)) // второе взвешивание
		{
			if (rfid_code1 != rfid_code)
			{
				timerRepeatedRFid->start();
				set_Lamp(0, 0);

			}
			else if (rfid_code1 == rfid_code)
				if (timerRepeatedRFid->isActive())
				{
					mysqlObj->saveEventToLog(QString(tr("Repeated reading of RFID card %1 !")
						.arg(rfid_code)));
					set_Lamp(1, 1);
					QTimer::singleShot(5000, this, SLOT(set_Lamp2_off()));
					return;
				}
			qDebug() << "Второе взвешивание \r\n";
			qDebug() << "rfid_code1= " << rfid_code1 << "\r\n";
			qDebug() << "rfid_code= " << rfid_code << "\r\n";
			// --- WriteCode2File(rfid_code, 2);
			rfid_code1 = rfid_code;

			//flag_updated1C = false;
			record.dateTimeAccept = data2.at(0);
			record.dateTimeSend = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
			record.code = data2.at(1);
			record.weight_1 = data2.at(2).toDouble();
			record.weight_2 = weightScales_01.toDouble();// getWeight();
			record.netto = abs(record.weight_2 - record.weight_1);

			record.supplier = data2.at(5);
			record.material = data2.at(6);
			record.num_auto = data2.at(7);
			record.num_prizep = data2.at(8);
			record.fio_driver = data2.at(9);
			record.num_card_poryadk = data2.at(10);
			record.blocked = data2.at(11).toInt();

			QString dateT = QDateTime::fromString(record.dateTimeAccept, "dd.MM.yyyy hh:mm:ss").toString("yyyy-MM-dd hh:mm:ss");
			int idf = mysqlObj->getIdfForRecord(dateT);
			record.dateTimeAccept = dateT;
			//File1C_data file1C( settingsObj->getFileName() );
			//QStringList strDataFile;
			//file1C = new File1C_data( settingsObj->getFileName() );
			if (mysqlObj->saveReportsTableData(record, idf))
			{
				/*	if( file1C->file_valid == true && file1C->compareFileAndRecord( record ) )
				{
				//file1C->clearFileStruct();
				//file1C->fillFileStruct( strDataFile );

				//save
				mysqlObj->add1CRecordReportTable( file_struct, record, rfid_code );
				}
				*/
				photofix_struct.id = idf;
				photofix_struct.num_measure = 2; // второе взвешивание

				QString strEvent = tr("Second weighing for card # ").append(record.code).append(
					tr(", auto number ").append(record.num_auto).append(
						tr(", material ").append(record.material).append(
							tr(" and supplier ").append(record.supplier).append(tr(" is done.")))));

				mysqlObj->saveEventToLog(strEvent);

				// Создаём запись в таблице с фотоснимками автомобилей (Штамп Даты/Времени)
				imageSaveModelObject->Date_Time = dateT; // сохраняем  Дату/Время
				imageSaveModelObject->num_measure = 1;
				imageSaveModelObject->id_photo = idf;

			//	if (settings->checkBoxSendEachRec->isChecked())
			//		timerSendMailEachRec->start();
				makePhotos();

				reports_rfidObj->updateReportsTable_Slot();
				mysqlObj->getMainTableData(getHeadersMainTable(), tableViewMain, progrBarMainTable, QList< QString >());

				mysqlObj->getReportsTableData(reports_rfidObj->getHeadersReportsTable(), tableViewReports, progrBarReportTable, QList< QString >());

				// Удаляем строки в таблице приёмки/отправки автомобилей
				int rowCnt = tableAutoReception->rowCount();
				while( rowCnt > 0 )
				{
					tableAutoReception->removeRow( rowCnt - 1 );
					rowCnt--;
				}


				if( CardsMode_action->isChecked() )
					QTimer::singleShot(1000, this, SLOT(show_CardsAcception()));
				CardsMode_Slot(cardReaderRfidObj->mode_active);

				//копировать на сервер
				//mysqlObj->CopyOnServer();
			}

			else // mysql error
			{

			}

			// было так
			//serialPortClassObj->writeTo_1C( QString( "AB%1\r" ).arg( serialPortFunc->getWeight() ) );
			// сделал по таймеру
			//---bigTruckIsHere = true;
			//---timer1C->start();

			//---PhotoFix *photoFix = PhotoFix::getInstance();
			//---photoFix->makePhotos();
		}
		else // первое взвешивание или автотара - сразу два
		{
			if (!timerRepeatedRFid->isActive())
			{
				timerRepeatedRFid->start();
			}
			else	 if (rfid_code1 == rfid_code)
			{
				mysqlObj->saveEventToLog(QString(tr("Repeated reading of RFID card %1 !")
					.arg(rfid_code)));
				return;
			}
			qDebug() << "Первое взвешивание \r\n";
			qDebug() << "rfid_code1= " << rfid_code1 << "\r\n";
			qDebug() << "rfid_code= " << rfid_code << "\r\n";
			//file1C = new File1C_data( settingsObj->getFileName() );
			record.code = rfid_code;
			record.supplier = data.at( 1 );
			record.weight_1 = weightScales_01.toDouble();
			record.weight_2 = 0;
			record.netto = 0;
			record.material = data.at(2);
			record.num_auto = data.at(3);
			record.num_prizep = data.at(4);
			record.fio_driver = data.at(5);
			record.num_card_poryadk = data.at(6);
			record.blocked = 0;

			// Дата/время сохранения записи
			QString dateTime = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
			int id = mysqlObj->saveRecordsPhotos(dateTime);

			photofix_struct.id = id;
			photofix_struct.num_measure = 0; // первое взвешивание

			//QString dateT = QDateTime::fromString(record.dateTimeAccept, "dd.MM.yyyy hh:mm:ss").toString("yyyy-MM-dd hh:mm:ss");
			QString dateT = dateTime;
			record.dateTimeAccept = dateT;

			// Создаём запись в таблице с фотоснимками автомобилей (Штамп Даты/Времени)
			imageSaveModelObject->Date_Time = dateT; // сохраняем  Дату/Время
			imageSaveModelObject->num_measure = 0;
			imageSaveModelObject->id_photo = id;

			mysqlObj->saveMainTableData(dateTime, record, id);
			mysqlObj->getMainTableData(getHeadersMainTable(), tableViewMain, progrBarMainTable, QList< QString >());
	

			makePhotos();
			if(chkAutoTara == false)
			{
				QString strEvent = tr("First weighing for card # ").append(record.code).append(
					tr(", auto number ").append(record.num_auto).append(
						tr(", material ").append(record.material).append(
							tr(" and supplier ").append(record.supplier).append(tr(" is done.")))));

				mysqlObj->saveEventToLog(strEvent);
			}
			//---WriteCode2File(rfid_code, 1);
			rfid_code1 = rfid_code;
			// было так
			//serialPortClassObj->writeTo_1C( QString( "AB%1\r" ).arg( serialPortFunc->getWeight() ) );

			// сделал по таймеру
			//---bigTruckIsHere = true;
			//---timer1C->start();

			statesWeight = RFID_ACTIVATED;
			stateHit = 4;

			/*if( CardsMode_action->isChecked() )
					CardsMode_Slot( true );
				else
					CardsMode_Slot( false );*/

			if( CardsMode_action->isChecked() )
				QTimer::singleShot(1000, this, SLOT(show_CardsAcception()));
			CardsMode_Slot(cardReaderRfidObj->mode_active);
			
			if(chkAutoTara) // автотара - сразу два взвешивания (типа второе)
			{
				if (mysqlObj->hasRecordMainTable(data2, rfid_code))
				{
					double weight = mysqlObj->getTaraAuto(data2.at(7));
					if(weight == -1)
					{
						return;
					}
					record.dateTimeAccept = data2.at(0);
					record.dateTimeSend = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
					record.code = data2.at(1);
					record.weight_1 = weight; // ТАРА!
					record.weight_2 = weightScales_01.toDouble();// getWeight();
					record.netto = abs(record.weight_2 - record.weight_1);

					record.supplier = data2.at(5);
					record.material = data2.at(6);
					record.num_auto = data2.at(7);
					record.num_prizep = data2.at(8);
					record.fio_driver = data2.at(9);
					record.num_card_poryadk = data2.at(10);
					record.blocked = data2.at(11).toInt();
					
					QString dateT = QDateTime::fromString(record.dateTimeAccept, "dd.MM.yyyy hh:mm:ss").toString("yyyy-MM-dd hh:mm:ss");
					int idf = mysqlObj->getIdfForRecord(dateT);
					record.dateTimeAccept = dateT;
			
					if (mysqlObj->saveReportsTableData(record, idf))
					{
						photofix_struct.id = idf;
						photofix_struct.num_measure = 2; // типа второе взвешивание

						QString strEvent = tr("First and Second weighing for card # ").append(record.code).append(
							tr(", auto tare ").append(QString::number(record.weight_1)).append(
								tr(", auto number ").append(record.num_auto).append(
									tr(", material ").append(record.material).append(
									tr(" and supplier ").append(record.supplier).append(tr(" is done."))))));

						mysqlObj->saveEventToLog(strEvent);

						// Создаём запись в таблице с фотоснимками автомобилей (Штамп Даты/Времени)
						imageSaveModelObject->Date_Time = dateT; // сохраняем  Дату/Время
						imageSaveModelObject->num_measure = 1;
						imageSaveModelObject->id_photo = idf;

						//makePhotos();

						reports_rfidObj->updateReportsTable_Slot();
						mysqlObj->getMainTableData(getHeadersMainTable(), tableViewMain, progrBarMainTable, QList< QString >());

						mysqlObj->getReportsTableData(reports_rfidObj->getHeadersReportsTable(), tableViewReports, progrBarReportTable, QList< QString >());

						// Удаляем строки в таблице приёмки/отправки автомобилей
						int rowCnt = tableAutoReception->rowCount();
						while( rowCnt > 0 )
						{
							tableAutoReception->removeRow( rowCnt - 1 );
							rowCnt--;
						}


						if( CardsMode_action->isChecked() )
							QTimer::singleShot(1000, this, SLOT(show_CardsAcception()));
						CardsMode_Slot(cardReaderRfidObj->mode_active);

					  //копировать на сервер
				     // mysqlObj->CopyOnServer();
					}
				}
			}
		}
	}

	else // данных для данной RFID карты нет
	{
		statesWeight = AUTO_ON_PLATFORM;
		mysqlObj->saveEventToLog(tr("Weighing is not possible! Card with code %1 is not registered!")
			.arg(rfid_code));

			   dataSocket[0] = 0x22; // 34
		       dataSocket[1] = 1; // in 0 or in 1
		       dataSocket[2] = 1;                                    
		       dataSocket[3] = 50;	
	           tcpSocket_Lamps->write( dataSocket );
	}

}



QList< QString > MyClass::getHeadersMainTable()
{
	QList< QString > list;

	/*list.append( textCodec->toUnicode( "Дата/Время\n прибытия" ) );
	list.append( textCodec->toUnicode( "Масса 1,\n кг" ) );
	list.append( textCodec->toUnicode( "Масса 2,\n кг" ) );
	list.append( textCodec->toUnicode( "Нетто,\n кг" ) );
	list.append( textCodec->toUnicode( "Поставщик" ) );
	list.append( textCodec->toUnicode( "Материал" ) );
	list.append( textCodec->toUnicode( "Номер авто" ) );
	list.append( textCodec->toUnicode( "Ф.И.О. водителя" ) );
	list.append( textCodec->toUnicode( "№ карточки" ) );
	list.append( textCodec->toUnicode( "Статус" ) );
	list.append( textCodec->toUnicode( "Фото" ) );
	list.append( textCodec->toUnicode( "Код RFID" ) );*/

	//--list.append(textCodec->toUnicode("Дата/Время\n прибытия"));
	//--list.append(textCodec->toUnicode("Масса 1,\n кг"));
	//--list.append(textCodec->toUnicode("Масса 2,\n кг"));
	//--list.append(textCodec->toUnicode("Нетто,\n кг"));
	//--list.append(textCodec->toUnicode("Фото"));
	//--list.append(textCodec->toUnicode("Код RFID"));



	list.append(tr("Date\ntime")); // дата приёмки
	//list.append(tr("Scales")); // номер весов
	list.append(tr("Weight 1")); // Взвешивание 1
	list.append(tr("Weight 2")); // Взвешивание 2
	list.append(tr("Net")); //"Нетто"
	list.append(tr("Supplier")); //"Поставщик"
	list.append(tr("Material")); //"Материал"
	list.append(tr("Vehicle\nnumber")); //"Номер автомобиля"
	list.append(tr("Trailer\nnumber")); //"Номер прицепа"
	list.append(tr("Driver")); //"Водитель"
	list.append(tr("Order number")); //"порядк номер карты"
	list.append(tr("Blocked")); //"блокирована"
	list.append(tr("Photo")); //"Фото"
	list.append(tr("RFID Code")); //"код карты"

	// добавленные колонки из DATA-file
	/*list.append( textCodec->toUnicode( "Номер авто" ) );
	list.append( textCodec->toUnicode( "Отправитель" ) );
	list.append( textCodec->toUnicode( "Получатель" ) );
	list.append( textCodec->toUnicode( "Перевозчик" ) );
	list.append( textCodec->toUnicode( "Груз" ) );
	list.append( textCodec->toUnicode( "Принял" ) );*/
	//list.append( textCodec->toUnicode( "Дата/Время\n отправки" ) );
	//list.append( textCodec->toUnicode( "Отправил" ) );
	/*list.append( textCodec->toUnicode( "№ накладной" ) );
	list.append( textCodec->toUnicode( "№ прицепа" ) );
	list.append( textCodec->toUnicode( "Ввод тары вручную" ) );*/


	// list.append( textCodec->toUnicode( "Сохранить на диск" ) );

	return list;
}
void MyClass::StartTimerWeight()
{
	timerWeight->start();
}