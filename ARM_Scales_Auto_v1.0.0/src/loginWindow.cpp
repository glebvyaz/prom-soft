#include "stdafx.h"
#include "loginWindow.h"
#include "DB_Driver.h"

//extern class mysqlProccess *mysqlProccessObject; // ������ ��� ������ � �� MySQL

loginWindowClass::loginWindowClass()
{
	QRect screenResolution = qApp->desktop()->screenGeometry();
	const QRect loginWindowSizePos = QRect( ( screenResolution.width() / 2 ) - 195,
		( screenResolution.height() / 2 ) - 115,
		390, 230 );
	const QSize comboBoxSize = QSize( 190, 32 ); // ������ comboBox - � 
	const QSize lineEditSize = QSize( 81, 32 ); // ������ lineEdit - �
	const QSize pushButtonSize = QSize( 104, 48 ); // ������  pushButton - �
    // 
	// ���� ����� � ������� 
    loginWin = new QWidget();
	loginWin->setGeometry( loginWindowSizePos );
	loginWin->setFixedWidth( 390 );
	loginWin->setFixedHeight( 230 );
	loginWin->setWindowTitle( tr("Login") ); 
	loginWin->setWindowModality(  Qt::ApplicationModal );
	loginWin->setWindowFlags(Qt::FramelessWindowHint); //Set a frameless window 
	loginWin->setObjectName( "loginWin" );
	// 
	nameOfUser = new QLabel( tr("Username"), loginWin );
	nameOfUser->setGeometry( 22, loginWindowSizePos.height() / 5, 180, 24 );
	nameOfUser->setStyleSheet( "font-size: 18px; border: none; " );
	// 
	nameOfUserValidation = new QLabel( tr("Incorrect name"), loginWin );
	nameOfUserValidation->setGeometry( 20 + 150, loginWindowSizePos.height() / 5 - 28, comboBoxSize.width() + 16, 18 );
	nameOfUserValidation->setStyleSheet( "color: Red;" );
	nameOfUserValidation->hide();
	// 
	enterName = new QComboBox( loginWin );
	enterName->setGeometry( 20 + 150, loginWindowSizePos.height() / 5 - 4, comboBoxSize.width(), lineEditSize.height() );
	enterName->setEditable( true );
	enterName->setStyleSheet( "font-size: 14px; background-color: #fff;" );
	enterName->addItem( NAME_OF_ADMIN_USER );
	// 
    password = new QLabel( tr("Password"), loginWin );
	password->setGeometry( 22, loginWindowSizePos.height() / 10 + lineEditSize.height() + 54, comboBoxSize.width() , 24 );
	password->setStyleSheet( "font-size: 18px; border: none; " );
	// 
	passwordOfUserValidation= new QLabel( tr("Incorrect password"), loginWin );
	passwordOfUserValidation->setGeometry( 20 + 150, loginWindowSizePos.height() / 10 + lineEditSize.height() + 32, comboBoxSize.width() + 24, 18 );
	passwordOfUserValidation->setStyleSheet( "color: Red;" );
	passwordOfUserValidation->hide();
	// 
	enterPassword = new QLineEdit( loginWin );
	enterPassword->setGeometry( 20 + 150, loginWindowSizePos.height() / 10 + lineEditSize.height() + 51, comboBoxSize.width(), lineEditSize.height() );
	enterPassword->setEchoMode( QLineEdit::Password );
	// 
	confirmButton = new QPushButton( tr("Enter"), loginWin );
	confirmButton->setGeometry( 20, loginWindowSizePos.height() / 10 + lineEditSize.height() + 100,
		pushButtonSize.width() + 64 , pushButtonSize.height() );
	confirmButton->setStyleSheet( "font-size: 14px; color: Black;" );
	// 
	cancelButton = new QPushButton( tr("Cancel"), loginWin );
	cancelButton->setGeometry( 20 + pushButtonSize.width() + 64 + 5, loginWindowSizePos.height() / 10 + lineEditSize.height() + 100,
		pushButtonSize.width() + 64 , pushButtonSize.height() );
	cancelButton->setStyleSheet( "font-size: 14px; color: Black;" );
	connect( cancelButton, SIGNAL( clicked() ), this, SLOT( hideLoginWindow() ) );
	// 
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ����������/�������� ������������ 
	createAccountWin = new QWidget();
	createAccountWin->setGeometry( loginWindowSizePos );
	createAccountWin->setFixedWidth( loginWindowSizePos.width() + 180 );
	createAccountWin->setFixedHeight( 320 );
	createAccountWin->setWindowTitle( tr("Users") ); 
	createAccountWin->setWindowModality(  Qt::ApplicationModal );
	createAccountWin->setWindowFlags(Qt::FramelessWindowHint); //Set a frameless window
	// 
	user = new QLabel( tr("Username"), createAccountWin ); 
	user->setGeometry( 20, loginWindowSizePos.height() / 10 + 8, 180, 18 );
	user->setStyleSheet( "color: Green; border: none; " );
	// 
	labelPassValidation = new QLabel( "", createAccountWin );
	labelPassValidation->setGeometry( 210 , loginWindowSizePos.height() / 10 - 24, comboBoxSize.width() + 100 , lineEditSize.height() );
	labelPassValidation->setStyleSheet( "color: Red;" );
	// 
	name = new QComboBox( createAccountWin );
	name->addItem( NAME_OF_ADMIN_USER ); 
	name->setGeometry( 210, loginWindowSizePos.height() / 10 + 4, comboBoxSize.width(), lineEditSize.height() );
	name->setEditable( true );
	name->setStyleSheet( "background-color: #fff;" );
	//  ������ ����� ����� ������������ 
	labelUser = new QLabel( createAccountWin );
	labelUser->setGeometry( 410, loginWindowSizePos.height() / 10 - 0, 72, 72 );
	QPixmap pixmapUser;
	pixmapUser.load( "image/first-name-icon.jpg", "JPEG", QPixmap::Auto );
	labelUser->setPixmap( pixmapUser );
	// 
	pass = new QLabel( tr("Password"), createAccountWin ); 
	pass->setGeometry( 20, loginWindowSizePos.height() / 10 + 8 + 64, 180, 18 );
	pass->setStyleSheet( "color: Green; border: none; " );
	// 
	newPass = new QLineEdit( createAccountWin );
	newPass->setGeometry( 210 , loginWindowSizePos.height() / 10 + 4 + 64 , comboBoxSize.width(), lineEditSize.height() );
	newPass->setEchoMode( QLineEdit::Password );
	// 
	reenterNewPass = new QLabel( tr("Repeat"), createAccountWin );
	reenterNewPass->setGeometry( 20, loginWindowSizePos.height() / 10 + 8 + 64 + 64, 180, 18 );
	reenterNewPass->setStyleSheet( "color: Green; border: none; " );
	//
	reenterNewPassValidation = new QLabel( "", createAccountWin );
	reenterNewPassValidation->setGeometry( 210, loginWindowSizePos.height() / 10 + 4 + 64 + 38, 240, 18 );
	reenterNewPassValidation->setStyleSheet( "color: Red;" );
	// 
	reenterNewPassword = new QLineEdit( createAccountWin );
	reenterNewPassword->setGeometry( 210 , loginWindowSizePos.height() / 10 + 4 + 64 + 64 , comboBoxSize.width(), lineEditSize.height() );
	reenterNewPassword->setEchoMode( QLineEdit::Password );
	// 
	newUserConfirmButton = new QPushButton( tr("Confirm"), createAccountWin );
	newUserConfirmButton->setGeometry( 20, loginWindowSizePos.height() / 10 + 8 + 64 + 64 + 48, pushButtonSize.width() + 32 ,pushButtonSize.height() );
	connect( newUserConfirmButton, SIGNAL( clicked() ), this, SLOT( confirmNewUserData() ) );
    // 
	newUserCancelButton = new QPushButton( tr("Cancel"), createAccountWin );
	newUserCancelButton->setGeometry( 20 + pushButtonSize.width() + 32 + 5, loginWindowSizePos.height() / 10 + 8 + 64 + 64 + 48, pushButtonSize.width() + 32 ,pushButtonSize.height() );
	connect( newUserCancelButton, SIGNAL( clicked() ), this, SLOT( hideCreateAccountWin() ) );
	// 
	deleteUserButton = new QPushButton( tr("Delete user"), createAccountWin );
	deleteUserButton->setGeometry( 20 + ( pushButtonSize.width() + 32 + 5 ) * 2 + 5, loginWindowSizePos.height() / 10 + 8 + 64 + 64 + 48, pushButtonSize.width() + 72 ,pushButtonSize.height() );
	connect( deleteUserButton, SIGNAL( clicked() ), this, SLOT( deleteRecord() ) );
    // 
	exitCrossNewUser = new QPushButton( createAccountWin ); // ������ ������ 
	exitCrossNewUser->setGeometry( loginWindowSizePos.width() / 2 + 95 + 180 , 2, 32, 32 );
	//icon.setPixmap( "image/exit_crosspng", QIcon::Large, QIcon::Normal, QIcon::On );
	//exitCrossNewUser->setIcon( icon );
	exitCrossNewUser->setStyleSheet( "color: Green; border: none; background: none; " );
	exitCrossNewUser->setObjectName( "#exitCross" );
	QObject::connect( exitCrossNewUser, SIGNAL( clicked() ), this, SLOT( hideCreateAccountWin() ) );

	updateLogin = new QCheckBox( tr("Change user data"), createAccountWin );
	updateLogin->setGeometry( 20, loginWindowSizePos.height() / 10 + 8 + 64 + 64 + 48 + 64, pushButtonSize.width() + 240 , 18 );

    loadUsers();
};
// ���������� ���� ����� ������ 
void loginWindowClass::show()
{
	loadUsers();
    loginWin->show();
}
// ������ ���� ����� ������
void loginWindowClass::hide()
{
	// enterPassword->setText( "" );
	// loginWin->hide();
}
// ������� ���� ����������� 
void loginWindowClass::hideLoginWindow()
{
	enterPassword->setText( "" );
	loginWin->hide();

	if( currentNameUser == "" )
		qApp->quit();
}
// �������� ������������� ����� � ������ 
// ���� ��������� �������,  ��  ���������� true, ����� - false
bool loginWindowClass::confirmLogin( QString nameUser, QString passwordUser )
{
	bool result = false;
 
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

	query.prepare( QString( "SELECT Name, Password, Role FROM users;" ) );
	result = query.exec();

	int i = 0;

	// ���� ������� ������ �������� �����������
	if( nameUser == NAME_OF_ADMIN_USER && passwordUser == PASSWORD_OF_SUPERVISOR_USER )
	{
	    role = ADMIN;
		return true;
	}

    while( query.next() )
	{
		if( nameUser == query.value( 0 ).toString().trimmed() && passwordUser == query.value( 1 ).toString().trimmed() )
		{
			i++;
			role = query.value( 2 ).toInt(); // ���� ������������ = 

			currentNameUser = query.value( 0 ).toString().trimmed(); // ������� ��� ������������ 
			currentPasswordUser = query.value( 1 ).toString().trimmed(); // ������� ������ ������������ 
		}
		else if( nameUser != query.value( 0 ).toString() && passwordUser != query.value( 1 ).toString() )
		{
			
		}
	}

	if( i == 0 )
	{
		passwordOfUserValidation->show();
		enterPassword->setStyleSheet( "border: 3px solid  #8B0000;" );
		result = false;
	}
	else
	{
        // nameOfUserValidation->hide();
		passwordOfUserValidation->hide();
		enterPassword->setStyleSheet( ":focus::border: 3px solid #006400; " );
		result = true;
	}

    return result;
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ���������� ���� �������� ������������� 
void loginWindowClass::showCreateAccountWin()
{
	createAccountWin->show();
}
// ����������� ���� �����x 
bool loginWindowClass::confirmNewUserData()
{
	bool result = false;

	if( newPass->text().contains( " " ) || newPass->text() != reenterNewPassword->text() )
	{
		reenterNewPassword->setStyleSheet( "border-color: #8B0000; " ); // �������� ������ ����� ������ ������ 
		if( newPass->text().contains( " " ) )
		{
			reenterNewPassValidation->setText( tr("without spaces!") );
		}
		else
		{
			reenterNewPassValidation->setText( tr("re-enter password incorrect") );
		}
	}
	else
	{
		reenterNewPassword->setStyleSheet( ":focus::border: 3px solid #006400; " ); // ������������ ������� ����� ���� ����� 
        reenterNewPassValidation->setText( "" );
		currentNameUser = name->currentText().trimmed();
	    currentPasswordUser = newPass->text().trimmed();

		if( currentNameUser == NAME_OF_ADMIN_USER )
		    role = ADMIN; // ���� ��� ����� � ������� ����� ����� 
		else
			role = USER;

		if( updateLogin->isChecked() == true ) // ���� ������ ����� ��������� �������� 
		{
			// �������� ������� ������ 
            result = updateUserRecord( name->currentText(), newPass->text(), 0 );

			if( result == true )
			{
				mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
				QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );
			    mysqlProccessObject->saveEventToLog( tr("User password changed: ") + QString( "%1")
				.arg( name->currentText() ) ); // �������� ������� � ��� 
			}
			return result;
		}

		// ����  ����  ������� ������� ������� 
		if( currentNameUser == NAME_OF_ADMIN_USER )
		{
            QMessageBox mess;

	        mess.setText( tr("Cannot create a second administrator account!") );
	        mess.setStandardButtons( QMessageBox::Cancel );
		    mess.setButtonText( QMessageBox::Cancel, "Ok" );
		    mess.setIcon( QMessageBox::Warning );
		    mess.setWindowTitle( tr( "Prom-Soft" ) );
		    mess.exec();

			result = false;
		}

		// 
		result = createUserRecord( currentNameUser, currentPasswordUser, role ); // ������ ������ ����� � ������ � �� 
		if( result == false ) // ������ ������ � �� 
		{
		}
		else
		{
            // �������� ������� � ��� 
			mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
			QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );
            mysqlProccessObject->saveEventToLog( tr("Add user: ") + QString("%1").arg( currentNameUser ) );
            hideCreateAccountWin();
		}
	}

	return result;
}
// ������ ���� �������� ������������� 
void loginWindowClass::hideCreateAccountWin()
{
	reenterNewPassword->setStyleSheet( ":focus::border: 3px solid #006400; " ); // ������������ ������� ����� ���� �����
    name->setStyleSheet( "background-color: #fff;" ); // ������������ ������� ����� ���� ����� ����� 
	//  :focus::border: 3px solid #006400; :editable::border: 2px solid #66CDAA; 
	// nameUser = "";
    name->setCurrentText( "" );
	// passwordUser = "";
	newPass->setText( "" );
	reenterNewPassword->setText( "" );
	reenterNewPassValidation->setText( "" );
	labelPassValidation->setText( "" );
	updateLogin->setChecked( false );

	createAccountWin->hide();
}
// 
bool loginWindowClass::loadUsers()
{
    bool result = false;
	
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

	query.prepare( QString( "SELECT Name FROM users;" ) );
	result = query.exec();

    name->clear();
	enterName->clear();
	while( query.next() )
	{
		name->addItem( query.value( 0 ).toString() );
		enterName->addItem( query.value( 0 ).toString() );
	}

	enterPassword->clear();
		
	return result;
}
// ������� ������� ������ 
bool loginWindowClass::createUserRecord( QString nameUser, QString password, int role )
{
	bool result = false;

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

	query.prepare( QString( "SELECT Name FROM users;" ) );
	result = query.exec();

    while( query.next() )
	{
		if( nameUser == query.value( 0 ) || nameUser == NAME_OF_ADMIN_USER  )
		{
			labelPassValidation->setText( tr("Such a name is already exist") );
			name->setStyleSheet( "border-color: #8B0000; background-color: #fff;" ); // �������� ������ ����� ������ ������ 
			return false;
		}
		else
		{
			labelPassValidation->setText( "" );
			name->setStyleSheet( ":focus::border: 3px solid #006400; :editable::border: 2px solid #66CDAA; background-color: #fff;" ); // ������������ ������� ����� ���� ����� �����
		}
	}

	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

	query.prepare( QString( "INSERT INTO users ( Name, Password, Role ) VALUES( :Name, :password, :Role );" ) );
	query.bindValue( ":Name", nameUser );
	query.bindValue( ":Password", password );
	query.bindValue( ":Role", role );
	result = query.exec();

	loadUsers();

	return result;
}
// �������� ������� ������ 
bool loginWindowClass::updateUserRecord( QString nameUser, QString password, int role )
{
	bool result = false;
	
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

	query.prepare( QString( "UPDATE users SET Password='%1' WHERE Name='%2';" ).arg( password ).arg( nameUser ) );
	// query.bindValue( ":Name", nameUser );
	// query.bindValue( ":Password", password );
	// query.bindValue( ":Role", role );
	result = query.exec();

	loadUsers();

	return result;
}
void loginWindowClass::deleteRecord()
{
	QMessageBox mess;

	mess.setText( tr("Do you want to delete record?") );
	mess.setStandardButtons( QMessageBox::Ok | QMessageBox::Cancel );
	mess.setButtonText( QMessageBox::Ok, tr("Yes") );
	mess.setButtonText( QMessageBox::Cancel, tr("No") );
	mess.setWindowTitle( tr( "Prom-Soft" ) );
	mess.exec();

	if( mess.result() == QMessageBox::Cancel )
		return;

	if( name->currentText() == NAME_OF_ADMIN_USER )
	{
        mess.setText( tr("You cannot delete Admin user!") );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();
		return;
	}
	else
	{
		if( removeUser( name->currentText() ) == false )
		{
            mess.setText( tr("Delete error.") );
	        mess.setStandardButtons( QMessageBox::Cancel );
		    mess.setButtonText( QMessageBox::Cancel, "Ok" );
		    mess.setIcon( QMessageBox::Warning );
		    mess.setWindowTitle( tr( "Prom-Soft" ) );
		    mess.exec();
		}
		else
		{
		    mess.setText( tr("Deleted") );
	        mess.setStandardButtons( QMessageBox::Ok );
			mess.setIcon( QMessageBox::Information );
		    mess.setWindowTitle( tr( "Prom-Soft" ) );
		    mess.exec();

			// �������� ������� � ��� 
			mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
			QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );
            mysqlProccessObject->saveEventToLog( tr("User deleted: ") + QString( "%1").arg( name->currentText() ) );
            hideCreateAccountWin();

			loadUsers();
		}
	}
}

// ������� ������������ �� �� 
bool loginWindowClass::removeUser( QString nameUser )
{
    bool result = false;

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );
	
	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

	query.prepare( QString( "DELETE FROM users WHERE Name='%1';" ).arg( nameUser ) );
	result = query.exec();

    while( query.next() )
	{
		
	}

	return result;
}

loginWindowClass::~loginWindowClass()
{
}