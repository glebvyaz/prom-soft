#include "stdafx.h"
#include "options.h"
#include "ui_options.h"
#include "Crypt.h"

#include "DB_Driver.h"
//extern mysqlProccess *mysqlProccessObject; // ������ ��� ������ � �� MySQL

options::options(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::options)
{
    ui->setupUi(this);

	QSettings settingsComPort( "settings.ini", QSettings::IniFormat );
	settingsComPort.setIniCodec( "Windows-1251" );

	textCodec = QTextCodec::codecForName("Windows-1251");

	QString cpu = QString::number( Crypt::getMacHash() );//*getCpuHash() */).rightJustified(7, '0').right(7);
	ui->label_yourKey->setText( cpu );

	modules.append( "camera" );
	modules.append( "sornost" );
	modules.append( "svetofor" );
	modules.append( "shlagbaum" );
	modules.append( "datchik" );
	modules.append( "tablo" );
	modules.append( "rfid" );
	modules.append( "bot" );
	modules.append( "PO" );

	pushButtons.append( ui->pushButton_camera );
	pushButtons.append( ui->pushButton_sornost );
	pushButtons.append( ui->pushButton_svetofor );
	pushButtons.append( ui->pushButton_shlagbaum );
	pushButtons.append( ui->pushButton_datchik );
	pushButtons.append( ui->pushButton_tablo );
	pushButtons.append( ui->pushButton_rfid );
	pushButtons.append( ui->pushButton_bot );
	pushButtons.append( ui->pushButton_PO );

	lineEdits.append( ui->lineEdit_camera );
	lineEdits.append( ui->lineEdit_sornost );
	lineEdits.append( ui->lineEdit_svetofor );
	lineEdits.append( ui->lineEdit_shlagbaum );
	lineEdits.append( ui->lineEdit_datchik );
	lineEdits.append( ui->lineEdit_tablo );
	lineEdits.append( ui->lineEdit_rfid );
	lineEdits.append( ui->lineEdit_bot );
	lineEdits.append( ui->lineEdit_PO );
	
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance(); 

	QSqlQuery query( /* *mysqlProccessObject->connect() */ *mysqlProccessObject->getQSqlInstance() );
	query.exec( QString( "USE %1;" ).arg( dataBaseName ) );

	for( int i = 0; i < modules.count(); ++i )
	{
		isCheked.append( false );

		query.exec( QString( "SELECT pass FROM modules WHERE module='%1';" ).arg( modules[i] ) );
		query.next();
		lineEdits[i]->setText( query.value( 0 ).toString() );

		connect( pushButtons[i], SIGNAL( clicked() ), this, SLOT( push() ) );
		pushButtons[i]->click();
	}
}

options::~options()
{
    delete ui;
}

QString options::getCamerasKey() {return ui->lineEdit_camera->text(); };
QString options::getWasteKey() {return ui->lineEdit_sornost->text(); };
QString options::getTrafficKey() {return ui->lineEdit_svetofor->text(); };
QString options::getBarrierKey() {return ui->lineEdit_shlagbaum->text(); };
QString options::getPositioningKey() {return ui->pushButton_datchik->text(); };
QString options::getScoreboardsKey() {return ui->lineEdit_tablo->text(); };
QString options::getRfidKey() {return ui->lineEdit_rfid->text(); };
QString options::getBotKey() {return ui->lineEdit_bot->text(); };
QString options::getProgramKey() {return ui->lineEdit_PO->text(); };

unsigned short options::getCpuHash()
{
   int cpuinfo[4] = { 0, 0, 0, 0 };
   __cpuid( cpuinfo, 0 );
   unsigned short hash = 0;
   unsigned short* ptr = (unsigned short*)(&cpuinfo[0]);
   for ( int i = 0; i < 9; ++i )
      hash += ptr[i];
  /* QStringList Macs = Crypt::getMAClist();
   hash = Crypt::getMacHash(Macs);*/
   return hash;
}

void options::push()
{
	QString module = sender()->objectName();
	module = module.mid( 11 );
	int x = modules.indexOf( module );

	if( check( lineEdits[x]->text(), module ) )
	{
		pushButtons[x]->setText( textCodec->toUnicode("�����������") );
		pushButtons[x]->setEnabled( false );
		lineEdits[x]->setEnabled( false );
		isCheked[x] = true;

		mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

		query.exec( QString( "USE %1;" ).arg( dataBaseName ) );
		query.prepare( "UPDATE modules SET pass=:pass WHERE module=:module;" );
		query.bindValue( ":pass", lineEdits[x]->text() );
		query.bindValue( ":module", module );
		query.exec();
	}
	else
	{
		QToolTip::showText( lineEdits[x]->mapToGlobal(QPoint()), textCodec->toUnicode("���� �� ������!") );
	}
}

bool options::check( QString key, QString module )
{
	if( key.isEmpty() )	return false;
	if( module.isEmpty() )	return false;
	
	enum {camera = 0, sornost, svetofor, shlagbaum, datchik, tablo, rfid, bot, po};
	
	QByteArray ba;
	ba.append( key ).append("=");
	ba = QByteArray::fromBase64( ba );
	ba = QByteArray::fromBase64( ba );
	unsigned long code = ui->label_yourKey->text().toLong() / (ba.toLong() ? ba.toLong() : 1 );                          // ��� ����

	if( code == 111 && module == modules[camera] ) return true;    //code == 344               code == 123               code == 111         
	if( code == 246 && module == modules[sornost] ) return true;   //code == 787               code == 215               code == 246
	if( code == 312 && module == modules[svetofor] ) return true;  //code == 879               code == 745               code == 312
	if( code == 464 && module == modules[shlagbaum] ) return true; //code == 320               code == 553               code == 464
	if( code == 525 && module == modules[datchik] ) return true;   //code == 945               code == 669               code == 525
	if( code == 646 && module == modules[tablo] ) return true;     //code == 253               code == 487               code == 646
	if( code == 784 && module == modules[rfid] ) return true;      //code == 478               code == 358               code == 784
	if( code == 994 && module == modules[bot] ) return true;                                                          // code == 994
	if( code == 472 && module == modules[po] ) return true;        //code == 635 ����� ����    ������ ��� 867            code == 472
	

	return false;
}

void options::set_Trial_Time( QString stt)
{
	ui->label_start->setText( stt );
}