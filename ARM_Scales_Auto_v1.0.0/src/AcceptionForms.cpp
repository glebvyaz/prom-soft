#include "stdafx.h"
#include "AcceptionForms.h"
#include "settings.h"
#include "DB_Driver.h"

#define MAX_INPUT_TEXT_LENGTH 48

class SettingsForm;
extern SettingsForm *settings;
//extern mysqlProccess *mysqlProccessObject;

// ������� ��������� ���� ������/ �������� ���������� 
void acceptionForm::setupUI_AcceptionForm( QObject *object )
{
	QRect screenResolution = qApp->desktop()->screenGeometry();
	const QRect settWindowSizePos = QRect( ( screenResolution.width() / 2 ) - 262,
		( screenResolution.height() <= 768 ? ( -4 + 32 ) : ( screenResolution.height() / 6 ) - ( screenResolution.height() / 30 ) ),
		screenResolution.width() / 3, 770 );
	const QSize comboBoxSize = QSize( 100, 24 ); // ������ comboBox - � 
	const QSize lineEditSize = QSize( 72, 24 ); // ������ lineEdit - �
	const QSize pushButtonSize = QSize( 48, 24 ); // ������  pushButton - �

	// 
	acceptionFormWindow = new QWidget(); // ���� ������/�������� ���������� 
	acceptionFormWindow->setGeometry( settWindowSizePos );

	acceptionFormWindow->setFixedHeight( 684 );
	acceptionFormWindow->setFixedWidth( 524 );
	acceptionFormWindow->raise(); // ������ ���� ���� 
	acceptionFormWindow->setWindowModality ( Qt::ApplicationModal ); // ������� ����  ��������� 
	acceptionFormWindow->setWindowTitle( tr("Accept/Send") );
	
	acceptionFormWindow->setObjectName( "acceptionFormWindow" );

	operationName = new QFrame( acceptionFormWindow ); // ����� � �������� �������� 
	operationName->setFrameShape( QFrame::Box );
	operationName->setFrameShadow( QFrame::Sunken );
	operationName->setGeometry( 0, 0, 522, 28 );
	// 
	headerName = new QLabel( tr("Scales"), operationName );
	headerName->setGeometry( 10, operationName->height() / 10 + 2, 365, 18 );
	headerName->setStyleSheet( "font-weight: bold; font-size: 16px; " );
	// 
	shippingAuto = new QRadioButton( acceptionFormWindow );
	shippingAuto->setGeometry( 10, operationName->height() + 4, 240, 24 );
	shippingAuto->setStyleSheet( "color: Orange; width: auto;" );
	shippingAuto->setText( tr("Empty vehicle") );
	shippingAuto->setStyleSheet( "color: #000; font-weight: bold; font-size: 14px; " );

	shippingAuto->setEnabled( false );

    connect( shippingAuto, SIGNAL( clicked() ), object, SLOT( selectedShippingAutoMode() ) );
	// 
	acceptingAuto = new QRadioButton( acceptionFormWindow ); // ??? ����� ������ ���������� 
	acceptingAuto->setGeometry( 140 + 48 + 32, operationName->height() + 4, 240, 24 );
    acceptingAuto->setStyleSheet( "width: auto;" );
	acceptingAuto->setText( tr("Loaded vehicle") ); 
	acceptingAuto->setStyleSheet( "color: #000; font-weight: bold; font-size: 14px; " );

	acceptingAuto->setEnabled( false );

	connect( acceptingAuto, SIGNAL( clicked() ), object, SLOT( selectedAcceptingAutoMode() ) );

	frameDocumentName = new QFrame( acceptionFormWindow ) ; // ����� ������������ ���������
	frameDocumentName->setFrameShape( QFrame::Box );
	frameDocumentName->setFrameShadow( QFrame::Sunken );
	frameDocumentName->setGeometry( 0, 60, 522, 24 );

	labelDocumentName = new QLabel( tr("Waybill "), frameDocumentName );
	labelDocumentName->setGeometry( frameDocumentName->width() / 3 + 32, frameDocumentName->height() / 12, 295, 18 );
	labelDocumentName->setStyleSheet( "color: #000; font-weight: bold; font-size: 16px; " );
    // 
	labelDocumentNumber = new QLabel( tr("Waybill number"), acceptionFormWindow );
	labelDocumentNumber->setGeometry( 16, 90, 180, 18 );
	labelDocumentNumber->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	documentNumber = new QLineEdit( acceptionFormWindow );
	documentNumber->setGeometry( 16 + 120, 90, lineEditSize.width() + 32, lineEditSize.height() );
	documentNumber->setStyleSheet("color: #000; font-weight: bold; font-size: 12px; " );
	documentNumber->setMaxLength( 32 ); 
	// 
	labelAutoNumber = new QLabel( tr("Vehicle number"), acceptionFormWindow ); //����� (�������) "������ �����������"
	labelAutoNumber->setGeometry( 16, 150, 180, 18 );
	labelAutoNumber->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	AutoNumber = new QComboBox( acceptionFormWindow );    //������ ������� �����������
	AutoNumber->setGeometry( 16 + 120, 150, 240, 24 );
	AutoNumber->setEditable( true );
	AutoNumber->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px;" );
	
	selectAutoNumber = new QPushButton( tr("Add"), acceptionFormWindow ); // �����(������) ���������� ������ ���������� 
	selectAutoNumber->setGeometry( 16 + 380, 150, lineEditSize.width() + 32 , lineEditSize.height() );
	selectAutoNumber->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	connect( selectAutoNumber, SIGNAL( clicked() ), object, SLOT( selectAuto_Slot() ) );
	// 
	checkBoxPrizepWeightEnable = new QCheckBox( tr("Trailer separately"), acceptionFormWindow ); // ����� ����������� ������� ��������
	checkBoxPrizepWeightEnable->setGeometry( 16 + 140 + lineEditSize.width() + 48 + 16 + 53, 118 - 20, 200, 18 );
	QObject::connect( checkBoxPrizepWeightEnable, SIGNAL( stateChanged( int ) ), object, SLOT( prizepWeightEnable( int ) ) );
    // 
    labelAutoPrizepNumber = new QLabel( tr("Trailer number"), acceptionFormWindow ); // ???
	labelAutoPrizepNumber->setGeometry( 16, 150 - 30, 180, 18 );
	labelAutoPrizepNumber->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	AutoPrizepNumber = new QComboBox( acceptionFormWindow );
	AutoPrizepNumber->setGeometry( 16 + 120, 150 - 30, 240, lineEditSize.height() );
	AutoPrizepNumber->setEditable( true );
	AutoPrizepNumber->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	selectPrizepNumber = new QPushButton( tr("Add"), acceptionFormWindow ); // �����(������) ���������� ������ ���������� 
	selectPrizepNumber->setGeometry( 16 + 380, 120, lineEditSize.width() + 32 , lineEditSize.height() );
	selectPrizepNumber->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	connect( selectPrizepNumber, SIGNAL( clicked() ), object, SLOT( selectPrizep_Slot() ) );
	// 
	labelSender = new QLabel( tr("Sender"), acceptionFormWindow );
	labelSender->setGeometry( 16, 180, 180, 18 );
	labelSender->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	sendersList = new QComboBox( acceptionFormWindow ); // ������ ������������ 
	sendersList->setGeometry( 16 + 120, 180, 240, lineEditSize.height() );
	sendersList->setEditable( true );
	sendersList->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
     
	selectSender = new QPushButton( tr("Add"), acceptionFormWindow ); // ����� ����������� 
	selectSender->setGeometry( 16 + 380, 180, lineEditSize.width() + 32, lineEditSize.height() );
	selectSender->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	connect( selectSender, SIGNAL( clicked() ), object, SLOT( selectSender_Slot() ) );
	// 
    labelRecepient = new QLabel( tr("Recipient"), acceptionFormWindow );
	labelRecepient->setGeometry( 16, 210, 180, 18 );
	labelRecepient->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	recepientsList = new QComboBox( acceptionFormWindow ); // ������ ����������� 
	recepientsList->setGeometry( 16 + 120, 210, 240, lineEditSize.height() );
	recepientsList->setEditable( true );
	recepientsList->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	selectRecepient = new QPushButton( tr("Add"), acceptionFormWindow ); // ����� ���������� 
	selectRecepient->setGeometry( 16 + 380, 210, lineEditSize.width() + 32, lineEditSize.height() );
	selectRecepient->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	connect( selectRecepient, SIGNAL( clicked() ), object, SLOT( selectRecepient_Slot() ) );
    // 
	labelPayer = new QLabel( tr("Payer"), acceptionFormWindow ); 
	labelPayer->setGeometry( 16, 240, 180, 18 );
	labelPayer->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	payersList = new QComboBox( acceptionFormWindow ); // ������ ������������ 
	payersList->setGeometry( 16 + 120, 240, 240, lineEditSize.height() );
	payersList->setEditable( true );
	payersList->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	selectPayer = new QPushButton( tr("Add"), acceptionFormWindow ); // ����� ����������� 
	selectPayer->setGeometry( 16 + 380, 240, lineEditSize.width() + 32, lineEditSize.height() );
	selectPayer->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	connect( selectPayer, SIGNAL( clicked() ), object, SLOT( selectPayer_Slot() ) );
    // 
    labelNameCarrier = new QLabel( tr("Carrier"), acceptionFormWindow ); 
	labelNameCarrier->setGeometry( 16, 270, 180, 18 );
	labelNameCarrier->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	namesCarrierList = new QComboBox( acceptionFormWindow ); // ������ ������������ 
	namesCarrierList->setGeometry( 16 + 120, 270, 240, lineEditSize.height() );
	namesCarrierList->setEditable( true );
	namesCarrierList->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	selectCarrier = new QPushButton( tr("Add"), acceptionFormWindow ); // ����� ����������� 
	selectCarrier->setGeometry( 16 + 380, 270, lineEditSize.width() + 32, lineEditSize.height() );
	selectCarrier->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	connect( selectCarrier, SIGNAL( clicked() ), object, SLOT( selectCarrier_Slot() ) );
	// 
	labelGoods = new QLabel( tr("Cargo"), acceptionFormWindow ); 
	labelGoods->setGeometry( 16, 300, 180, 18 );
	labelGoods->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	namesGoods = new QComboBox( acceptionFormWindow ); // ������ ������ 
	namesGoods->setGeometry( 16 + 120, 300, 240, lineEditSize.height() );
	namesGoods->setEditable( true );
	namesGoods->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	selectGoods = new QPushButton( tr("Add"), acceptionFormWindow ); // ����� ������������ ����� 
	selectGoods->setGeometry( 16 + 380, 300, lineEditSize.width() + 32, lineEditSize.height() );
	selectGoods->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	connect( selectGoods, SIGNAL( clicked() ), object, SLOT( selectGoods_Slot() ) );
	// 
	labelPricePerUnit = new QLabel( tr("Cargo price"), acceptionFormWindow );
	labelPricePerUnit->setGeometry( 16, 330, 180, 18 );
	labelPricePerUnit->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	pricePerUnit = new QLineEdit( acceptionFormWindow ); // ���� �� ����� 
	pricePerUnit->setGeometry( 16 + 120, 330, 65, lineEditSize.height() );
	pricePerUnit->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	pricePerUnit->setInputMask( "999999999" );
	pricePerUnit->setEdited( true );
	pricePerUnit->setText( "0" );
	pricePerUnit->setCursorPosition( 0 );
	// 
	buttonGetLastData = new QPushButton( tr("Latest data")+" [F8]", acceptionFormWindow ); // ����� ������������ ����� 
	buttonGetLastData->setGeometry( 16 + 260, 330, lineEditSize.width() + 32+120, lineEditSize.height() );
	buttonGetLastData->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	connect( buttonGetLastData, SIGNAL( clicked() ), this, SLOT( buttonGetLastData_Slot() ) );
	// 
	horisontalLine2 = new QFrame( acceptionFormWindow );
	horisontalLine2->setGeometry( QRect( 10, 360, acceptionFormWindow->width() - 20, 3 ) );
	horisontalLine2->setFrameShape( QFrame::HLine );
	horisontalLine2->setFrameShadow( QFrame::Sunken );
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	labelFactWeight = new QLabel( tr("Actual weight"), acceptionFormWindow ); // ����������� ��� 
	labelFactWeight->setGeometry( 16, 370, 180, 18 );
	labelFactWeight->setStyleSheet( "font-weight: bold; font-size: 14px; " );
    // 
    labelBrutto = new QLabel( tr("Gross"), acceptionFormWindow ); 
	labelBrutto->setGeometry( 48 - 32, 400, 180, 18 );
	labelBrutto->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	bruttoWeight = new QLineEdit( acceptionFormWindow ); // ����������� ������ 
	bruttoWeight->setGeometry( 48 + 48 - 32, 400, lineEditSize.width(), lineEditSize.height() );
	bruttoWeight->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	bruttoWeight->setReadOnly( true );

	makeBruttoWeight = new QPushButton( tr("Weigh") + " [F6]", acceptionFormWindow ); // �������� ��� ������ 
	makeBruttoWeight->setGeometry( 48 + 48 + 100 + 5 - 60, 400, lineEditSize.width() + 32, lineEditSize.height() );
	makeBruttoWeight->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	connect( makeBruttoWeight, SIGNAL( pressed() ), object, SLOT( makeBruttoWeightSlot() ) );
	// 
    labelTara = new QLabel( tr("Tare"), acceptionFormWindow ); 
	labelTara->setGeometry( 48 - 32, 430, 180, 18 );
	labelTara->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	taraWeight = new QLineEdit( acceptionFormWindow ); // ����������� ���� 
	taraWeight->setGeometry( 48 + 48 - 32, 430, lineEditSize.width(), lineEditSize.height() );
	taraWeight->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	taraWeight->setReadOnly( true );

	makeTaraWeigh = new QPushButton( tr("Weigh") + " [F5]", acceptionFormWindow ); // �������� ��� ���� 
	makeTaraWeigh->setGeometry( 48 + 48 + 100 + 5 - 60 , 430, lineEditSize.width() + 32, lineEditSize.height() );
	makeTaraWeigh->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	QObject::connect( makeTaraWeigh, SIGNAL( pressed() ), object, SLOT( makeTaraWeightSlot() ) );
	// 
	labelNetto = new QLabel( tr("Net"), acceptionFormWindow );
	labelNetto->setGeometry( 48 - 32, 460, 180, 18 );
	labelNetto->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	nettoWeight = new QLineEdit( acceptionFormWindow ); // ����������� ����� 
	nettoWeight->setGeometry( 48 + 48 - 32, 460, lineEditSize.width(), lineEditSize.height() );
	nettoWeight->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	nettoWeight->setReadOnly( true );

	checkSor = new QCheckBox( tr("Soreness"), acceptionFormWindow );
	checkSor->setGeometry( 16, 496 + 20, 70, 24 );
	checkSor->hide();

	spinSor = new QDoubleSpinBox( acceptionFormWindow );
	spinSor->setGeometry( 16+70, 496+24, 60, 18 );
	spinSor->setRange( 0, 1 );
	spinSor->setDecimals( 4 );
	spinSor->setSingleStep( 0.0001 );
	spinSor->hide();

	enterTaraManually = new QCheckBox( tr("Manual tare"), acceptionFormWindow ); 
	enterTaraManually->setGeometry( 16, 496, 180, 18 );
	enterTaraManually->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	enterTaraManually->setEnabled( false );
	enterTaraManually->setChecked( false );
	enterTaraManually->setObjectName( "enterTaraManually" );
	enterTaraManually->installEventFilter( this );

	enterTaraFromDB = new QPushButton( tr("Tare from database"), acceptionFormWindow ); 
	enterTaraFromDB->setGeometry( 16+180, 496 + 20, 160, 24 );
	enterTaraFromDB->setStyleSheet( "color: black; font-weight: bold; font-size: 12px; " );
	enterTaraFromDB->setEnabled( false );
	enterTaraFromDB->setObjectName( "enterTaraFromDatabase" );
	connect( enterTaraFromDB, SIGNAL( clicked() ), this, SLOT( selectedTareFromDataBase_Slot() ) );

	checkTaraFromBD = new QCheckBox(acceptionFormWindow);
	checkTaraFromBD->setChecked(false);
	checkTaraFromBD->hide();
	
	enterTaraWeightManualy = new QLineEdit( acceptionFormWindow ); // ���� ���� ������� 
	enterTaraWeightManualy->setGeometry( 48 + 48 + 100 + 5 - 60, 490, lineEditSize.width() + 32 + 2, lineEditSize.height() );
	enterTaraWeightManualy->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	// enterTaraWeightManualy->setEdited( true );
	enterTaraWeightManualy->setInputMask( "999999" );
	bool flag = connect( enterTaraWeightManualy, SIGNAL( textChanged( const QString& ) ), this, SLOT( textChanged_Slot( const QString& ) ) );

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// 
	labelFactWeightPricep = new QLabel( tr("Actual trailer weight"), acceptionFormWindow ); // ����������� ��� 
	labelFactWeightPricep->setGeometry( acceptionFormWindow->width() / 3 - 100 + 210, 370, 240, 18 );
	labelFactWeightPricep->setStyleSheet( "font-weight: bold; font-size: 14px; " );
    // 
    labelBruttoPricep = new QLabel( tr("Gross"), acceptionFormWindow ); 
	labelBruttoPricep->setGeometry( 48 + 238, 400, 180, 18 );
	labelBruttoPricep->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	bruttoWeightPricep = new QLineEdit( acceptionFormWindow ); // ����������� ������ 
	bruttoWeightPricep->setGeometry( 48 + 48 + 238, 400, lineEditSize.width(), lineEditSize.height() );
	bruttoWeightPricep->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	bruttoWeightPricep->setReadOnly( true );

	makeBruttoWeightPricep = new QPushButton( tr("Weigh") + " [F6]", acceptionFormWindow ); // �������� ��� ������ 
	makeBruttoWeightPricep->setGeometry( 48 + 48 + 100 + 5 + 210 , 400, lineEditSize.width() + 32, lineEditSize.height() );
	makeBruttoWeightPricep->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	QObject::connect( makeBruttoWeightPricep, SIGNAL( pressed() ), object, SLOT( makeBruttoWeightPricepSlot() ) );
	// 
    labelTaraPricep = new QLabel( tr("Tare"), acceptionFormWindow ); 
	labelTaraPricep->setGeometry( 48 + 238, 430, 180, 18 );
	labelTaraPricep->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	taraWeightPricep = new QLineEdit( acceptionFormWindow ); // ����������� ���� 
	taraWeightPricep->setGeometry( 48 + 48 + 238, 430, lineEditSize.width(), lineEditSize.height() );
	taraWeightPricep->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	taraWeightPricep->setReadOnly( true );

	makeTaraWeighPricep = new QPushButton( tr("Weigh") + " [F5]", acceptionFormWindow ); // �������� ��� ���� 
	makeTaraWeighPricep->setGeometry( 48 + 48 + 100 + 5 + 210 , 430, lineEditSize.width() + 32, lineEditSize.height() );
	makeTaraWeighPricep->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	connect( makeTaraWeighPricep, SIGNAL( pressed() ), object, SLOT( makeTaraWeightPricepSlot() ) );
	// 
	labelNettoPricep = new QLabel( tr("Net"), acceptionFormWindow ); 
	labelNettoPricep->setGeometry( 48 + 238, 460, 180, 18 );
	labelNettoPricep->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	nettoWeightPricep = new QLineEdit( acceptionFormWindow ); // ����������� ����� 
	nettoWeightPricep->setGeometry( 48 + 48 + 238, 460, lineEditSize.width(), lineEditSize.height() );
	nettoWeightPricep->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	nettoWeightPricep->setReadOnly( true );
	//
	enterTaraManuallyPricep = new QCheckBox( tr("Manual tare"), acceptionFormWindow );
	enterTaraManuallyPricep->setGeometry( 48 + 238, 496, 180, 18 );
	enterTaraManuallyPricep->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	enterTaraManuallyPricep->setEnabled( false );
	enterTaraManuallyPricep->setChecked( false );
	enterTaraManuallyPricep->setObjectName( "enterTaraManuallyPricep" );
	enterTaraManuallyPricep->installEventFilter( this );
	
	enterTaraWeightManualyPricep = new QLineEdit( acceptionFormWindow ); // ���� ���� ������� 
	enterTaraWeightManualyPricep->setGeometry( 48 + 48 + 100 + 5 + 210 , 490, lineEditSize.width() + 32 + 2, lineEditSize.height() );
	enterTaraWeightManualyPricep->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	enterTaraWeightManualyPricep->setInputMask( "999999" );
	connect( enterTaraWeightManualyPricep, SIGNAL( textChanged( const QString& ) ), this, SLOT( textChangedPricep_Slot( const QString& ) ) );
	

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// 
	horisontalLine3 = new QFrame( acceptionFormWindow );
	horisontalLine3->setGeometry( QRect( 10, 540, acceptionFormWindow->width() - 20, 3 ) );
	horisontalLine3->setFrameShape( QFrame::HLine );
	horisontalLine3->setFrameShadow( QFrame::Sunken );

	// 
	labelOperatorReception = new QLabel( tr("Accepted:"), acceptionFormWindow ); // ???  ������� "������������" (�����������)
	labelOperatorReception->setGeometry( 10, 545, 180, 18 );
	labelOperatorReception->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	labelOperatorUserRecept = new QLabel( tr("User:"), acceptionFormWindow ); // ???  ������� "������" (��������)
	labelOperatorUserRecept->setGeometry( 10, 575, 180, 18 );
	labelOperatorUserRecept->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

    operatorUserRecept = new QLineEdit( acceptionFormWindow ); // ����������� �������� 
	operatorUserRecept->setGeometry( 10 + 104, 575, 140, lineEditSize.height() );
	operatorUserRecept->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	operatorUserRecept->setReadOnly( true );

	labelReceptionDateTime = new QLabel( tr("Date/time"), acceptionFormWindow ); // ???
	labelReceptionDateTime->setGeometry( 10, 605, 180, 18 );
	labelReceptionDateTime->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

    receptionDateTime = new QLineEdit( acceptionFormWindow ); // ����/����� ������ ���������� 
	receptionDateTime->setGeometry( 10 + 104, 605, 140, lineEditSize.height() );
	receptionDateTime->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	receptionDateTime->setReadOnly( true );
	// 
    labelOperatorShipping = new QLabel( tr("Sended:"), acceptionFormWindow ); // ???  ������� "��������" (��������)
	labelOperatorShipping->setGeometry( 10 + 256, 545, 180, 18 );
	labelOperatorShipping->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

	labelOperatorUserShipping = new QLabel( tr("User:"), acceptionFormWindow ); // ???  ������� "������������" (������������)
	labelOperatorUserShipping->setGeometry( 10 + 256, 575, 180, 18 );
	labelOperatorUserShipping->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

    operatorUserShipping = new QLineEdit( acceptionFormWindow ); // ����������� �������� 
	operatorUserShipping->setGeometry( 10 + 104 + 256, 575, 140, lineEditSize.height() );
	operatorUserShipping->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	operatorUserShipping->setReadOnly( true );

	labelShippingDateTime = new QLabel( tr("Date/time"), acceptionFormWindow ); 
	labelShippingDateTime->setGeometry( 10 + 256, 605, 180, 18 );
	labelShippingDateTime->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );

    shippingDateTime = new QLineEdit( acceptionFormWindow ); // ����/����� ������ ���������� 
	shippingDateTime->setGeometry( 10 + 104 + 256, 605, 140, lineEditSize.height() );
	shippingDateTime->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
    shippingDateTime->setReadOnly( true );
	// 
	QIcon okIcon;
	confirmForm = new QPushButton( tr("Confirm"), acceptionFormWindow ); // ����������� ����� 
	confirmForm->setGeometry( acceptionFormWindow->width() / 4, 640,
		pushButtonSize.width() + 64, pushButtonSize.height() + 16 );
	confirmForm->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px;" );
	connect( confirmForm, SIGNAL( pressed() ), object, SLOT( confirmAcceptForm() ) ); // slot ��� ������������� ����� 

	QIcon cancelIcon;
	cancelForm = new QPushButton( tr("Cancel"), acceptionFormWindow ); // ������� ����� 
	cancelForm->setGeometry( acceptionFormWindow->width() / 3 + pushButtonSize.width() + 48 + 5, 640,
		pushButtonSize.width() + 64, pushButtonSize.height() + 16 );
	cancelForm->setStyleSheet( "color: #000; font-weight: bold; font-size: 12px; " );
	connect( cancelForm, SIGNAL( pressed() ), object, SLOT( closeAcceptForm() ) ); // slot ��� �������� ����� 

    enableDisablePrizepWeight( false ); // ���������/��������� ��������� ��� ����������� ������� �������� 
}

// 
void acceptionForm::textChanged_Slot( const QString & text )
{
	taraWeight->setText( text );
	nettoWeight->setText( QString( "%1" ).arg( bruttoWeight->text().toDouble() - taraWeight->text().toDouble() ) );
}

// 
void acceptionForm::textChangedPricep_Slot( const QString & text )
{
	taraWeightPricep->setText( text );
	nettoWeightPricep->setText( QString( "%1" ).arg( bruttoWeightPricep->text().toDouble() - taraWeightPricep->text().toDouble() ) );
}

// ��������  ���� ��� ����� ������ 
void acceptionForm::clearControls() 
{
	documentNumber->clear(); // ��������� 
	AutoNumber->setCurrentText( "" ); // ����� ���� 
	AutoPrizepNumber->setCurrentText( "" ); // ����� ������� 
	sendersList->setCurrentText( "" ); // ��� ����������� 
	recepientsList->setCurrentText( "" ); // ��� ���������� 
	payersList->setCurrentText( "" ); // ���������� 
	namesCarrierList->setCurrentText( "" ); // ���������� 
	namesGoods->setCurrentText( "" ); // ��� ����� 
	pricePerUnit->setText( "0" ); // ��������� ����� 
	// enterHumidity->setText( "" ); // ��������� ����� 

	bruttoWeight->setText( "0" ) ; // ������ ����������� ���
    taraWeight->setText( "0" ); // ���� 
	nettoWeight->setText( "0" ); // �����

	bruttoWeightPricep->setText( "0" ) ; // ������ ����������� ��� ������� 
    taraWeightPricep->setText( "0" ); // ���� ������� 
	nettoWeightPricep->setText( "0" ); // ����� ������� 

	enterTaraManually->setChecked( false ); // ����� ����� ���� ������� 
	enterTaraManuallyPricep->setChecked( false ); // ����� ����� ���� ������� ������� 
	checkTaraFromBD->setChecked( false );

	checkSor->setChecked( false );
	spinSor->setValue( 1 );

	operatorUserShipping->clear();
	shippingDateTime->clear();
}
// ����������� ������� ��� ����� 
bool acceptionForm::eventFilter( QObject *object, QEvent *e ) // ������ �������
{
	QKeyEvent *keyEvent = ( QKeyEvent *)e;
	int keyNum = keyEvent->key();

	if( keyNum == Qt::Key_F5 || keyNum == Qt::Key_Return )
	{
		if( makeTaraWeigh->isEnabled() )
		{
			makeTaraWeigh->animateClick();
			return true;
		}
		if( makeTaraWeighPricep->isEnabled() )
		{
			makeTaraWeighPricep->animateClick();
			return true;
		}
	}
	if( keyNum == Qt::Key_F6 || keyNum == Qt::Key_Return )
	{
		if( makeBruttoWeight->isEnabled() )
		{
			makeBruttoWeight->animateClick();
			return true;
		}
		if( makeBruttoWeightPricep->isEnabled() )
		{
			makeBruttoWeightPricep->animateClick();
			return true;
		}
	}
	if( keyNum == Qt::Key_F8 || keyNum == Qt::Key_Return )
	{
		buttonGetLastData_Slot();
		return true;
	}

	

	if( e->type() == QEvent::MouseButtonPress && Qt::LeftButton )
	{		
		if( object->objectName() == "enterTaraManually" && checkBoxPrizepWeightEnable->isChecked() == false ) // t��� ������� ����������� ����������, ����� ����� ���� ���� �������
		{
			if( shippingAuto->isChecked() == true ) // ���� ���������� ����� "������ ����������", �.�. ���������� �������� ���� 
			{
			    QCheckBox *chBox = qobject_cast< QCheckBox *>( object );

                if( chBox->isChecked() == false )
				{
					chBox->setChecked( true );
                    makeTaraWeigh->setEnabled( false ); // ��.�������� ���� 
					taraWeight->setText( "0" );
					nettoWeight->setText( "0" );

					enterTaraWeightManualy->setText( "0" );
					enterTaraWeightManualy->setEnabled( true );
					enterTaraWeightManualy->setFocus();
					enterTaraWeightManualy->setCursorPosition( 0 );
				}
			    else
				{
					chBox->setChecked( false );
				    makeTaraWeigh->setEnabled( true );

					enterTaraWeightManualy->clear();
					enterTaraWeightManualy->setEnabled( false );
				}
			}

			return true;
		}
		if( object->objectName() == "enterTaraManuallyPricep" && checkBoxPrizepWeightEnable->isChecked() == true ) // ����� ����� ���� ���� ������� ������� 
		{
			if( shippingAuto->isChecked() == true ) // ���� ���������� ����� "������ ����������", �.�. ���������� �������� ���� 
			{
				QCheckBox *chBox = qobject_cast< QCheckBox *>( object );

			    if( chBox->isChecked() == false )
				{
					chBox->setChecked( true );
			        makeTaraWeighPricep->setEnabled( false );
					taraWeightPricep->setText( "0" );
					nettoWeightPricep->setText( "0" );

					enterTaraWeightManualyPricep->setText( "0" );
					enterTaraWeightManualyPricep->setEnabled( true );
					enterTaraWeightManualyPricep->setFocus();
					enterTaraWeightManualyPricep->setCursorPosition( 0 );
				}
			    else
				{
					chBox->setChecked( false );
				    makeTaraWeighPricep->setEnabled( true );

					enterTaraWeightManualyPricep->clear();
					enterTaraWeightManualyPricep->setEnabled( false );
				}
			}

			return true;
		}
    }

    return false;
}
// ��������� ��������� � ������ ������ ��������� 
void acceptionForm::enableAcceptingFormControls()
{
    makeTaraWeigh->setEnabled( false ); // ��.�������� ����
	makeTaraWeighPricep->setEnabled( false );

	enterTaraWeightManualy->clear();
	enterTaraWeightManualyPricep->clear();

	makeBruttoWeight->setEnabled( true );
   	makeBruttoWeightPricep->setEnabled( true );

	enterTaraFromDB->setEnabled( false );
}
// ��������� ��������� � ������ �������� ��������� 
void acceptionForm::enableShippingFormControls()
{
    makeTaraWeigh->setEnabled( true ); // ��.�������� ���� 
	makeTaraWeighPricep->setEnabled( true );
	
	enterTaraWeightManualy->clear();
	enterTaraWeightManualyPricep->clear();

	makeBruttoWeight->setEnabled( false );
	makeBruttoWeightPricep->setEnabled( false );

	enterTaraFromDB->setEnabled( true );
}
// ���������/��������� ��������� ��� ����������� ������� �������� 
void acceptionForm::enableDisablePrizepWeight( bool state )
{
	if( state == true ) // ����������� ������� 
	{
		if( acceptingAuto->isChecked() == true ) // ������  ���������� 
		{
			enableAcceptingFormControls();
			makeTaraWeigh->setEnabled( false ); // ��.�������� ����
			enterTaraWeightManualy->setEnabled( false );
			enterTaraWeightManualy->setStyleSheet( "background-color: DarkGray;" );
			enterTaraWeightManualy->clear();
			makeBruttoWeight->setEnabled( false );
		}
		else
		{
			enableShippingFormControls();
			makeTaraWeigh->setEnabled( false ); // ��.�������� ����
			enterTaraWeightManualy->setEnabled( false );
			enterTaraWeightManualy->setStyleSheet( "background-color: DarkGray;" );
			enterTaraWeightManualy->clear();
			enterTaraWeightManualyPricep->clear();
			makeBruttoWeight->setEnabled( false );
		}

		AutoNumber->setEnabled( false );
		//AutoNumber->setStyleSheet( "background-color: DarkGray;" );
		//AutoPrizepNumber->setEnabled( true );
        //AutoPrizepNumber->setStyleSheet( "background-color: White;" );
	}
	else // ����������� ���������� 
	{
		if( acceptingAuto->isChecked() == true )
		{
			enableAcceptingFormControls();

			makeTaraWeighPricep->setEnabled( false );
			enterTaraWeightManualy->setReadOnly( false );

            enterTaraWeightManualy->setEnabled( false );
			enterTaraWeightManualy->setStyleSheet( "background-color: DarkGray;" );

			enterTaraWeightManualyPricep->setEnabled( false );
			enterTaraWeightManualyPricep->setStyleSheet( "background-color: DarkGray;" );
			enterTaraWeightManualy->clear();
			enterTaraWeightManualyPricep->clear();
   			makeBruttoWeightPricep->setEnabled( false );
		}
		else
		{
			enableShippingFormControls();
			makeTaraWeighPricep->setEnabled( false );
			enterTaraWeightManualyPricep->setEnabled( false );

            enterTaraWeightManualy->setEnabled( false );
			enterTaraWeightManualy->setStyleSheet( "background-color: DarkGray;" );

			enterTaraWeightManualyPricep->setStyleSheet( "background-color: DarkGray;" );
			enterTaraWeightManualy->clear();
			enterTaraWeightManualyPricep->clear();
   			makeBruttoWeightPricep->setEnabled( false );
		}
		AutoNumber->setEnabled( true );
		//AutoNumber->setStyleSheet( "background-color: White;" );
        //AutoPrizepNumber->setEnabled( true ); // false );
	    //AutoPrizepNumber->setStyleSheet( "background-color: DarkGray;" );
	}
}
// ��������� �������� ������ � ����� ������/�������� ����������� 
bool acceptionForm::validationAcceptForm()
{
	bool result = true;
	QString styleSheetEdit = documentNumber->styleSheet();
	QString styleSheetList = sendersList->styleSheet();
	QString styleSheetAlarm = " border-color: Red; border-style: outset; border-width: 1px;";

	if( documentNumber->text().isEmpty() == true ) // ����� ��������� 
	{
		documentNumber->setToolTip( tr("Enter waybill number") );
		documentNumber->setStyleSheet( styleSheetEdit + styleSheetAlarm );
		result = false;
	}
	else
	{
        documentNumber->setStyleSheet( styleSheetEdit );
		result = true;
	}
	// ??? ��������� ��������� ������ ���������� 
	if( checkBoxPrizepWeightEnable->isChecked() == false ) // ���� ������  ��  ������������ 
	{
	    if( AutoNumber->currentText().isEmpty() )
	    {
		    AutoNumber->setToolTip( tr("Enter vehicle number") );
		    AutoNumber->setStyleSheet( styleSheetList + styleSheetAlarm );
		    result = false;
	    }
	    else
	    {
		    AutoNumber->setStyleSheet( styleSheetList );
	    }
	}
	else
    if( AutoPrizepNumber->currentText().isEmpty() )
	{
        AutoPrizepNumber->setToolTip( tr("Enter trailer number") );
		AutoPrizepNumber->setStyleSheet( styleSheetEdit + styleSheetAlarm );
		result = false;
	}
	else
	{
		AutoNumber->setStyleSheet( styleSheetList );
		AutoPrizepNumber->setStyleSheet( styleSheetList ); //styleSheetEdit
	}
	// 
	if( sendersList->currentText().isEmpty() )
	{
		sendersList->setStyleSheet( styleSheetList + styleSheetAlarm );
		result = false;
	}
	else
	{
		sendersList->setStyleSheet( styleSheetList );
	}
	// 
	if( recepientsList->currentText().isEmpty() )
	{
		recepientsList->setStyleSheet( styleSheetList + styleSheetAlarm );
		result = false;
	}
	else
	{
		recepientsList->setStyleSheet( styleSheetList );
	}
    // 
	if( payersList->currentText().isEmpty() )
	{
		payersList->setStyleSheet( styleSheetList + styleSheetAlarm );
		result = false;
	}
	else
	{
		payersList->setStyleSheet( styleSheetList );
	}
	// 
	if(namesCarrierList->currentText().isEmpty() )
	{
		namesCarrierList->setStyleSheet( styleSheetList + styleSheetAlarm );
		result = false;
	}
	else
	{
		namesCarrierList->setStyleSheet( styleSheetList );
	}
    // 
	if(  acceptingAuto->isChecked() && namesGoods->currentText().isEmpty() )
	{
		namesGoods->setStyleSheet( styleSheetList + styleSheetAlarm );
		result = false;
	}
	else
	{
		namesGoods->setStyleSheet( styleSheetList );
	}
	//////////////////
	if( AutoNumber->currentText().isEmpty() )
	{
		AutoNumber->setStyleSheet( styleSheetList + styleSheetAlarm );
		result = false;
	}
	else
	{
		AutoNumber->setStyleSheet( styleSheetList );
	}

    // 
	// ���� �� ����� 
	if( pricePerUnit->text().isEmpty() )
	{
	}
	// �������� ����� ������ �� ������������� ���� 
	if( checkBoxPrizepWeightEnable->isChecked() == true ) 
	{
		if( shippingAuto->isChecked() == true && enterTaraManuallyPricep->isChecked() == true
			&& enterTaraWeightManualyPricep->text() != "" && enterTaraWeightManualyPricep->text() != "0.000" )
		{
			if( result != false )
                result = true;
		}
		else
		{
			if( bruttoWeightPricep->text().toDouble() == 0 && 
				taraWeightPricep->text().toDouble() == 0 && 
				nettoWeightPricep->text().toDouble() == 0 )
			{
		        bruttoWeightPricep->setStyleSheet( styleSheetEdit + styleSheetAlarm );
				taraWeightPricep->setStyleSheet( styleSheetEdit + styleSheetAlarm );
		        nettoWeightPricep->setStyleSheet( styleSheetEdit + styleSheetAlarm );
				result = false;
			}
			else
			if( bruttoWeightPricep->text().toDouble() > 0 && 
				taraWeightPricep->text().toDouble() > 0 && 
				nettoWeightPricep->text().toDouble() == 0 )
			{
		        bruttoWeightPricep->setStyleSheet( styleSheetEdit );
				taraWeightPricep->setStyleSheet( styleSheetEdit );
		        nettoWeightPricep->setStyleSheet( styleSheetEdit + styleSheetAlarm );
				result = false;
			}
			else
			if( bruttoWeightPricep->text().toDouble() > 0 && 
				taraWeightPricep->text().toDouble() > 0 && 
				nettoWeightPricep->text().toDouble() <= 0 )
			{
		        bruttoWeightPricep->setStyleSheet( styleSheetEdit );
				taraWeightPricep->setStyleSheet( styleSheetEdit );
		        nettoWeightPricep->setStyleSheet( styleSheetEdit + styleSheetAlarm );
				result = false;
			}
			else
			if( shippingAuto->isChecked() && bruttoWeightPricep->text().toDouble() != 0 && 
				taraWeightPricep->text().toDouble() == 0 && 
				nettoWeightPricep->text().toDouble() != 0 )
			{
		        bruttoWeightPricep->setStyleSheet( styleSheetEdit );
				taraWeightPricep->setStyleSheet( styleSheetEdit + styleSheetAlarm );
		        nettoWeightPricep->setStyleSheet( styleSheetEdit );
				result = false;
			}
			else
			if( !shippingAuto->isChecked() && bruttoWeightPricep->text().toDouble() == 0 && 
				taraWeightPricep->text().toDouble() != 0 && 
				nettoWeightPricep->text().toDouble() != 0 )
			{
		        bruttoWeightPricep->setStyleSheet( styleSheetEdit + styleSheetAlarm );
				taraWeightPricep->setStyleSheet( styleSheetEdit );
		        nettoWeightPricep->setStyleSheet( styleSheetEdit );
				result = false;
			}
			else
			{
                taraWeightPricep->setToolTip( "Push \"Weigh\" to take Tare" );
                bruttoWeightPricep->setToolTip( "Push \"Weigh\" to take Gross" );
	            nettoWeightPricep->setToolTip( "Push \"Weigh\" to take Net" );

				nettoWeightPricep->setStyleSheet( styleSheetEdit );
			}
		}

		bruttoWeight->setStyleSheet( styleSheetEdit );
		taraWeight->setStyleSheet( styleSheetEdit );
		nettoWeight->setStyleSheet( styleSheetEdit );
	}
	else
	if( checkBoxPrizepWeightEnable->isChecked() == false )
	{
		if( shippingAuto->isChecked() == true && enterTaraManually->isChecked() == true && enterTaraWeightManualy->text() != "" && enterTaraWeightManualy->text() != "0.000" )
		{
			if( result != false )
                result = true;
		}
		else
		{
			if( bruttoWeight->text().toDouble() == 0 && 
				taraWeight->text().toDouble() == 0 && 
				nettoWeight->text().toDouble() == 0 )
			{
		        bruttoWeight->setStyleSheet( styleSheetEdit + styleSheetAlarm );
				taraWeight->setStyleSheet( styleSheetEdit + styleSheetAlarm );
		        nettoWeight->setStyleSheet( styleSheetEdit + styleSheetAlarm );
				result = false;
			}
			else
			if( bruttoWeight->text().toDouble() > 0 && 
				taraWeight->text().toDouble() > 0 && 
				nettoWeight->text().toDouble() == 0 )
			{
		        bruttoWeight->setStyleSheet( styleSheetEdit );
				taraWeight->setStyleSheet( styleSheetEdit );
		        nettoWeight->setStyleSheet( styleSheetEdit + styleSheetAlarm );
				result = false;
			}
			else
			if( bruttoWeight->text().toDouble() > 0 && 
				taraWeight->text().toDouble() > 0 && 
				nettoWeight->text().toDouble() <= 0 )
			{
		        bruttoWeight->setStyleSheet( styleSheetEdit );
				taraWeight->setStyleSheet( styleSheetEdit );
		        nettoWeight->setStyleSheet( styleSheetEdit + styleSheetAlarm );
				result = false;
			}
			else
			if( shippingAuto->isChecked() && bruttoWeight->text().toDouble() != 0 && 
				taraWeight->text().toDouble() == 0 && 
				nettoWeight->text().toDouble() != 0 )
			{
		        bruttoWeight->setStyleSheet( styleSheetEdit );
				taraWeight->setStyleSheet( styleSheetEdit + styleSheetAlarm );
		        nettoWeight->setStyleSheet( styleSheetEdit );
				result = false;
			}
			else
			if( !shippingAuto->isChecked() && bruttoWeight->text().toDouble() == 0 && 
				taraWeight->text().toDouble() != 0 && 
				nettoWeight->text().toDouble() != 0 )
			{
		        bruttoWeight->setStyleSheet( styleSheetEdit + styleSheetAlarm );
				taraWeight->setStyleSheet( styleSheetEdit );
		        nettoWeight->setStyleSheet( styleSheetEdit );
				result = false;
			}
			else
			{
				taraWeightPricep->setToolTip( "Push \"Weigh\" to take Tare" );
	            bruttoWeightPricep->setToolTip( "Push \"Weigh\" to take Gross" );
	            nettoWeightPricep->setToolTip( "Push \"Weigh\" to take Net" );

				nettoWeight->setStyleSheet( styleSheetEdit );
			}
		}

		bruttoWeightPricep->setStyleSheet( styleSheetEdit );
		taraWeightPricep->setStyleSheet( styleSheetEdit );
		nettoWeightPricep->setStyleSheet( styleSheetEdit );
	}

	// ���� � ComboBox ������� ����� MAX ��������� ��������, �� ���������� ������
	//AutoNumber
	if( AutoNumber->currentText().length() > MAX_INPUT_TEXT_LENGTH )
	{
        AutoNumber->setStyleSheet( styleSheetList + styleSheetAlarm );
		AutoNumber->setToolTip( tr("The number of characters entered is greater than MAX possible!") );
		result = false;
	}

	if( sendersList->currentText().length() > MAX_INPUT_TEXT_LENGTH )
	{
        sendersList->setStyleSheet( styleSheetList + styleSheetAlarm );
		sendersList->setToolTip( tr("The number of characters entered is greater than MAX possible!") );
		result = false;
	}
	if( recepientsList->currentText().length() > MAX_INPUT_TEXT_LENGTH )
	{
		recepientsList->setStyleSheet( styleSheetList + styleSheetAlarm );
		recepientsList->setToolTip( tr("The number of characters entered is greater than MAX possible!") );
		result = false;
	}
	if( payersList->currentText().length() > MAX_INPUT_TEXT_LENGTH )
	{
		payersList->setStyleSheet( styleSheetList + styleSheetAlarm );
		payersList->setToolTip( tr("The number of characters entered is greater than MAX possible!") );
		result = false;
	}
	if( namesCarrierList->currentText().length() > MAX_INPUT_TEXT_LENGTH )
	{
		namesCarrierList->setStyleSheet( styleSheetList + styleSheetAlarm );
		namesCarrierList->setToolTip( tr("The number of characters entered is greater than MAX possible!") );
		result = false;
	}
	if( namesGoods->currentText().length() > MAX_INPUT_TEXT_LENGTH )
	{
		namesGoods->setStyleSheet( styleSheetList + styleSheetAlarm );
		namesGoods->setToolTip( tr("The number of characters entered is greater than MAX possible!") );
		result = false;
	}
	//result=true;
	return result;
}
// ����� ��������� ����� ������/������� 
void acceptionForm::resetValidation()
{
	QString styleSheet =  "color: #000; font-weight: bold; font-size: 18px; ";

	documentNumber->setStyleSheet( styleSheet );
	documentNumber->setToolTip( tr("Enter waybill number") );
	AutoPrizepNumber->setStyleSheet( styleSheet );
	AutoPrizepNumber->setToolTip( tr("Enter trailer number") );
    AutoNumber->setStyleSheet( styleSheet );
	AutoNumber->setToolTip( tr("Enter vehicle number") );
	sendersList->setStyleSheet( styleSheet );
	sendersList->setToolTip( tr("Enter sender name") );
	recepientsList->setStyleSheet( styleSheet );
	recepientsList->setToolTip( tr("Enter recipient name") );
	payersList->setStyleSheet( styleSheet );
	payersList->setToolTip( tr("Enter payer name") );
	namesCarrierList->setStyleSheet( styleSheet );
	namesCarrierList->setToolTip( tr("Enter carrier name") );
	namesGoods->setStyleSheet( styleSheet );
	namesGoods->setToolTip( tr("Enter cargo name") );
	pricePerUnit->setStyleSheet( styleSheet );

	taraWeight->setStyleSheet( styleSheet );
	taraWeight->setToolTip( "Push \"Weigh\" to take Tare" );
	bruttoWeight->setStyleSheet( styleSheet );
	bruttoWeight->setToolTip( "Push \"Weigh\" to take Gross" );
	nettoWeight->setStyleSheet( styleSheet );
	nettoWeight->setToolTip( "Push \"Weigh\" to take Net" );

	taraWeightPricep->setStyleSheet( styleSheet );
    taraWeightPricep->setToolTip( "Push \"Weigh\" to take Tare" );
	bruttoWeightPricep->setStyleSheet( styleSheet );
	bruttoWeightPricep->setToolTip( "Push \"Weigh\" to take Gross" );
	nettoWeightPricep->setStyleSheet( styleSheet );
	nettoWeightPricep->setToolTip( "Push \"Weigh\" to take Net" );

	//enterTaraManually->setStyleSheet( styleSheet );
	enterTaraManually->setToolTip( tr("Set tare manually") );
	enterTaraWeightManualy->setStyleSheet( styleSheet );
	enterTaraWeightManualy->setToolTip( tr("Enter tare") );
	//enterTaraManuallyPricep->setStyleSheet( styleSheet );
	enterTaraManuallyPricep->setToolTip( tr("Set trailer tare manually") );
	enterTaraWeightManualyPricep->setStyleSheet( styleSheet );
	enterTaraWeightManualyPricep->setToolTip( tr("Enter tare") );
}
// 
void acceptionForm::buttonGetLastData_Slot()
{
	QSettings settings_data( "settings.ini", QSettings::IniFormat );
    settings_data.setIniCodec("Windows-1251");

	AutoNumber->setCurrentText( settings_data.value( "freqData/numAuto", "" ).toString() );   
	sendersList->setCurrentText( settings_data.value( "freqData/sender", "" ).toString() );
	recepientsList->setCurrentText( settings_data.value( "freqData/recepient", "" ).toString() );
	payersList->setCurrentText( settings_data.value( "freqData/payer", "" ).toString() );
	namesCarrierList->setCurrentText( settings_data.value( "freqData/carrier", "" ).toString() );
	namesGoods->setCurrentText( settings_data.value( "freqData/goods", "" ).toString() );
	//AutoNumber->setCurrentText( settings_data.value( "freqData/Auto", "" ).toString() );
}

// ��������� ��������� � ������ �������� ��������� 
void acceptionForm::disableShippingFormControls()
{
}
// ��������� ��������� � ������ ������ ��������� 
void acceptionForm::disableAcceptingFormControls()
{
}
// ������� ������ � ������� ������ ����������� 
bool acceptionForm::insertDataIntoAcceptTable()
{
	bool result = false;

	return result;
}
// ����������� 
acceptionForm::acceptionForm()
{
}
// ���������� 
acceptionForm::~acceptionForm()
{
}
// ���������� ������� (����� �����) ��� ����� ������/�������� ����������� 
void acceptionForm::setHeaderName( int num_scales )
{
    headerName->setText( tr("Scales - ") + QString( "%1" ).arg( num_scales + 1 ) ); 
}

void acceptionForm::selectedTareFromDataBase_Slot()
{
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	double weight = mysqlProccessObject->getTaraAuto( AutoNumber->currentText() );

	if( weight == -1 )
	{
		taraWeight->clear();
		nettoWeight->clear();

		QMessageBox mess;
		mess.setText( tr("Data for today is not available.") );
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();
		return;
	}

	checkTaraFromBD->setChecked(true);
	taraWeight->setText( weight == -1 ? "0" : QString::number( weight ) );
	nettoWeight->setText( QString::number( bruttoWeight->text().toDouble() - taraWeight->text().toDouble() ) );
}

// 
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void acceptFormModel::clear() // �������� ��������� �����
{
    this->type_operation = 0;
	this->Nakladnaya = ""; // ��������� 
	this->Number_Auto = ""; // ����� ���� 
	this->Num_Prizep = ""; // ����� ������� 
	this->Sender = ""; // ��� ����������� 
	this->Accepter = ""; // ��� ���������� 
	this->Payer = ""; // ���������� 
	this->Transporter = ""; // ���������� 
	this->Goods = ""; // ��� ����� 
	this->Price = 0.0; // ��������� ����� 
	// this->humidity = 0.0; // ��������� 
	
	this->Brutto = 0; // ������ ����������� ���
	this->Tara = 0; // ���� 
	this->Netto = 0; // ����� 

	this->Brutto_Prizep = 0; // ������ 
	this->Tara_Prizep = 0; // ���� 
	this->Netto_Prizep = 0; // �����

	this->Name_User_Accept = ""; // ��� ������������ ��� ������ ���� 
	this->Date_Time = "";
}
// ����������� 
acceptFormModel::acceptFormModel()
{
}
// ���������� 
acceptFormModel::~acceptFormModel()
{
}
// ������ ������ ��� �������� ����������� 
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void imageSaveModel::clear() // �������� ��������� �����
{
}
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
imageSaveModel::imageSaveModel()
{
}
imageSaveModel::~imageSaveModel()
{
}