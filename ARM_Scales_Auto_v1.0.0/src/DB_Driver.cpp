#include "stdafx.h"
#include "DB_Driver.h"
#include "Crypt.h"
#include "cardsAcceptDataStruct.h"

// ���� ������ � ������� �� �� 

mysqlProccess* mysqlProccess::m_Instance = 0;


mysqlProccess::mysqlProccess()
{

	QStringList list = QSqlDatabase::drivers();

	m_sqlDatabase = new QSqlDatabase( QSqlDatabase::addDatabase( "QMYSQL" ) );
	mySqlDBRemout = new QSqlDatabase( QSqlDatabase::addDatabase( "QMYSQL", "connectionDBR" ) );

	if( connect() )
	   createMySQL_DataBase();

	taraDictionaryModel_obj = NULL;
	registration_Obj = NULL;
	reportsTableModel_Obj = NULL;
	mainTableModel_Obj = NULL;
}

// 
mysqlProccess::~mysqlProccess()
{
	//mySqlDatabase->close();
	//delete mySqlDatabase;
	m_Instance->disconnect();
	delete m_Instance;
}

//================

mysqlProccess* mysqlProccess::instance()
{
    static QMutex mutex;
    if (!m_Instance)
    {
        mutex.lock();
 
        if (!m_Instance)
		{
            m_Instance = new mysqlProccess;
		}

        mutex.unlock();
    }
 
    return m_Instance;
}

// 
QSqlDatabase *mysqlProccess::getQSqlInstance()
{
	return m_sqlDatabase;
}

// 
void mysqlProccess::drop()
{
    static QMutex mutex;
    mutex.lock();
	m_Instance->disconnect();
    delete m_Instance;
    m_Instance = 0;
    mutex.unlock();
}

// ������������ � ��, ���������� true � ������ ������ 
bool mysqlProccess::connect() // ����������� � �� 
{
	bool result = false;

	QSettings settings( "settings.ini", QSettings::IniFormat );
    settings.setIniCodec("Windows-1251");

	m_sqlDatabase->setHostName( settings.value( "MySQL/address" ).toString() );
	m_sqlDatabase->setPort( settings.value( "MySQL/port" ).toInt() );
	m_sqlDatabase->setUserName( settings.value( "MySQL/login" ).toString() );
   
	//enc-dec pass
		QString pass = settings.value( "MySQL/password" ).toString();
		QString pass_enc;
		if ( ! pass.startsWith("#") )
			pass_enc = Crypt::crypt( pass );
		else
		{
			pass_enc = pass.remove( 0, 1 );
			pass = Crypt::decrypt( pass_enc );
		}
		// 
		
	
	m_sqlDatabase->setPassword( pass );

	QString str1 = settings.value( "MySQL/address" ).toString();
	int port = settings.value( "MySQL/port" ).toInt();
	QString str3 = settings.value( "MySQL/login" ).toString();
    QString str4 = settings.value( "MySQL/password" ).toString();

	//mySqlDatabase = m_sqlDatabase;
    return m_sqlDatabase->open();
}

//��������� ��
//����������� � ��������� ��
QSqlDatabase *mysqlProccess::connectDBR()
{
	bool result = false;

	static QString addressDBR;
	static int portDBR;
	static QString nameDBR;
	static QString loginDBR;
	static QString passwordDBR;

		QSettings settings( "settings.ini", QSettings::IniFormat );
		settings.setIniCodec("Windows-1251");
		//settings.setIniCodec("UTF-8");

	//if( !settings.value( "Remote_Database/Remote_Database_on" ).toInt() == 0 )
	//{
	//	if( loginDBR.isEmpty() )
	//	{
		addressDBR = settings.value( "Remote_Database/addressRDB" ).toString();       qDebug()<<addressDBR;
		portDBR = settings.value( "Remote_Database/portRDB" ).toInt();                qDebug()<<portDBR;
		nameDBR = settings.value( "Remote_Database/nameRDB" ).toString();             qDebug()<<nameDBR;
		loginDBR = settings.value( "Remote_Database/loginRDB" ).toString();           qDebug()<<loginDBR;
		passwordDBR = settings.value( "Remote_Database/passwordRDB" ).toString();     qDebug()<<passwordDBR;
	
		mySqlDBRemout->setHostName( addressDBR );	
		mySqlDBRemout->setPort( portDBR );
		mySqlDBRemout->setDatabaseName( nameDBR );
		mySqlDBRemout->setUserName( loginDBR );
		mySqlDBRemout->setPassword(passwordDBR );
		//}
	
		if( !mySqlDBRemout->isOpen() )
		{
			for( int i = 0; i < 3; i++ )
			{
				if( mySqlDBRemout->open() )
				{
					//emit red_green_connect(1);
					break;
				}
				else
				{
					qDebug( "___Open database failed!    DB_Driver.cpp-connect()" );

					QMessageBox mess;
					mess.setText(tr("Error connect with Remote Data Base") );  //������ ����������� �  ��������� ���� ������!
					mess.setStandardButtons( QMessageBox::Cancel );
					mess.setButtonText( QMessageBox::Cancel, "Ok" );
					mess.setIcon( QMessageBox::Warning );
					mess.setWindowTitle( tr( "AV-CONTROL PLUS" ) );
					mess.exec();
				}
			}		
		}
	//	else
	//	{
	//	  //emit red_green_connect(1);
	//	}
	//}
    return mySqlDBRemout;
}

// 
void mysqlProccess::disconnect() // ���������� �� �� 
{
	if( m_sqlDatabase != 0 )
		m_sqlDatabase->close();
}

// 
void mysqlProccess::disconnect( QSqlDatabase *sqlDatabase, QString db_name ) // ���������� �� �� 
{
	sqlDatabase->close();
}




// ���� �� ������������, �� ���������� true
bool mysqlProccess::isConnected()
{
	QSqlQuery query( *getQSqlInstance() );

	bool result = query.exec( QString( "USE %1; SELECT id FROM users LIMIT 1;" ).arg( dataBaseName ) );
	return result;
}

// 
bool mysqlProccess::hasAutoNumberOrCode( QString code, QString num_auto )
{
	bool result;
	QSqlError error;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	result = query.exec( QString( "SELECT * FROM registration WHERE code = '%1' OR num_auto = '%2'; " )
		.arg( code )
		.arg( num_auto ) );

	int r = query.size();

	if( r > 0 )
		result = true;
	else
		result = false;

	return result;
}

// ����� ������������������ ����� � ����������� 
QList< QString > mysqlProccess::getRegistrationData( QString code )
{
    bool result;
	QSqlError error;
	QList< QString > data;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	QString strDeb;
	result = query.exec( strDeb = QString( "SELECT * FROM registration WHERE code = '%1';" ).arg( code ) );	qDebug() << "Query result = " << strDeb << endl << code << endl;

	while( query.next() )
	{
		data.append( query.record().value( "code" ).toString() );
		data.append( query.record().value( "supplier" ).toString() );
		data.append( query.record().value( "material" ).toString() );
		data.append( query.record().value( "num_auto" ).toString() );
		data.append( query.record().value( "num_prizep" ).toString() );
		data.append( query.record().value( "fio_driver" ).toString() );
		data.append( query.record().value( "num_card_poryadk" ).toString() );
		data.append( query.record().value( "blocked" ).toString() );
		data.append( query.record().value( "auto_tara" ).toString() );

	}

	return data;
}
// ������ � ������� � �������� ������ 

// �������� �������� ��  ������� ������ �� ������ 
bool mysqlProccess::getMainTableData(QList< QString > headers_names, QTableView *tableWidget, QProgressBar *progrBar, QList< QString > filters)
{
	bool result;
	QSqlError error;
	int num_records, percent;
	QVector< QString > data;
	QString params;

	if (!isConnected())
	{
		for (int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN; i++)
		{
			if (connect())
				break;
		}
	}

	QSqlQuery query(*getQSqlInstance());

	if (filters.count() >= 3)  //  NUM_FILTERS_PARAMS )
	{
		/*if( !filters.at( 0 ).isEmpty() )
		params.append( QString( " date_time >= '%1' " ).arg( filters.at( 0 ) ) );

		if( !filters.at( 1 ).isEmpty() )
		params.append( QString( " AND date_time <= '%1' " ).arg( filters.at( 1 ) ) );

		if( !filters.at( 2 ).isEmpty() )
		params.append( QString( " AND numScales=%1 " ).arg( filters.at( 2 ) ) );*/

	}

	result = query.exec(QString("SELECT * FROM acceptcards; ")); // WHERE %1; " ).arg( params ) );
	//progrBar->show();

	num_records = query.size();
	percent = 0;

	tableWidget->setUpdatesEnabled(false);
	tableWidget->setEnabled(false);
	tableWidget->setVisible(false);
	tableWidget->blockSignals(true);
	tableWidget->setSortingEnabled(false);

	int r = query.size();
	int c = query.record().count() - 4;

	if (r < 0 || c < 0)
	{
		r = 0;
		c = headers_names.length();
	}

	// ������ ������ ������ ��� ������������� � � QTableView
	if (mainTableModel_Obj == NULL)
	{
		mainTableModel_Obj = new MainTableModel(r, c, headers_names);
	}
	else
	{
		mainTableModel_Obj->clear();
		mainTableModel_Obj->addParams(r, c, headers_names);
	}

	while (query.next())
	{
		data.append(query.record().value("date_time").toDateTime().toString("dd.MM.yyyy hh:mm:ss"));
		data.append(query.record().value("weight_1").toString());
		data.append(query.record().value("weight_2").toString());
		data.append(query.record().value("netto").toString());
		data.append(QTextCodec::codecForName("Windows-1251")->toUnicode("����"));
		data.append(query.record().value("code").toString());
		data.append( query.record().value( "material" ).toString() );
		data.append( query.record().value( "num_auto" ).toString() );
		data.append( query.record().value( "num_prizep" ).toString() );
		data.append( query.record().value( "fio_driver" ).toString() );
		data.append( query.record().value( "num_card_poryadk" ).toString() );

		// int blocked = query.record().value( "blocked" ).toInt();
		// data.append( ( blocked == 0 ? QTextCodec::codecForName("Windows-1251")->toUnicode( "���" ) : ( blocked == 1 ? QTextCodec::codecForName("Windows-1251")->toUnicode( "��" ) : QTextCodec::codecForName("Windows-1251")->toUnicode( "" ) ) ) );

		

		mainTableModel_Obj->update(&data);
		data.clear();
	}

	tableWidget->setModel(mainTableModel_Obj);
	tableWidget->setUpdatesEnabled(false);
	tableWidget->setStyleSheet("font-weight: bold; font-size: 12px;"); // ???

	for (int i = 0; i < headers_names.length(); i++)
	{
		if (i == (headers_names.length() - 2))
			tableWidget->setColumnWidth(i, 120);
		else
			tableWidget->setColumnWidth(i, 180);
	}

	tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers); // ��������� ������������� ������
	tableWidget->setAlternatingRowColors(true);

	tableWidget->setUpdatesEnabled(true);
	progrBar->hide();
	tableWidget->setEnabled(true);
	//tableWidget->setVisible(true);
	tableWidget->blockSignals(false);
	tableWidget->setSortingEnabled(true);

	return result;
}

// 
bool mysqlProccess::saveMainTableData(QString dateTime, struct Record & record, int idf)
{
	bool result = false;
	QSqlError error;

	if (!isConnected())
	{
		for (int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN; i++)
		{
			if (connect())
				break;
		}
	}

	QSqlQuery query(*getQSqlInstance());

	result = query.prepare(QString("INSERT INTO acceptcards ( date_time, code, weight_1, weight_2, netto, supplier, material, num_auto, num_prizep, fio_driver, num_card_poryadk, blocked, idf ) "
		"VALUES ( :date_time, :code, :weight_1, :weight_2, :netto, :supplier, :material, :num_auto, :num_prizep, :fio_driver, :num_card_poryadk, :blocked, :idf );"));

	query.bindValue(":date_time", dateTime);
	query.bindValue(":code", record.code);
	query.bindValue(":weight_1", record.weight_1);
	query.bindValue(":weight_2", record.weight_2);
	query.bindValue(":netto", record.netto);

	query.bindValue(":supplier", record.supplier);
	query.bindValue(":material", record.material);
	query.bindValue(":num_auto", record.num_auto);
	query.bindValue(":num_prizep", record.num_prizep);
	query.bindValue(":fio_driver", record.fio_driver);
	query.bindValue(":num_card_poryadk", record.num_card_poryadk);
	query.bindValue(":blocked", 0);
	query.bindValue(":idf", idf);

	result = query.exec();
	error = query.lastError();
	QString error_text = error.text();

	return result;
}

// 

// �������� �������� ��  ������� ������� 
bool mysqlProccess::getReportsTableData(QList< QString > headers_names, QTableView *tableWidget, QProgressBar *progrBar, QList< QString > filters)
{
	bool result;
	QSqlError error;
	int num_records, percent;
	QVector< QString > data;
	QString params = "";

	if (!isConnected())
	{
		for (int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN; i++)
		{
			if (connect())
				break;
		}
	}

	QSqlQuery query(*getQSqlInstance());
	
	if( filters.isEmpty() )
	{
		filters.append( QString( "%1 %2" ).arg( QDate::currentDate().toString( "yyyy-MM-dd" )).arg("00:00:00"));
		filters.append( QString( "%1 %2" ).arg( QDate::currentDate().toString( "yyyy-MM-dd" )).arg("23:59:59"));
		for (int i = 0; i < 6; i++)
			filters.append( "" );
	}

	params.append(QString(" date_time_accept >= '%1' AND date_time_accept <= '%2' ")
		.arg(filters.at(0))
		.arg(filters.at(1)));

	if (!filters.at(2).isEmpty())
		params.append(QString(" AND num_auto = '%1' ").arg(filters.at(2)));

	if (!filters.at(3).isEmpty())
		params.append(QString(" AND supplier = '%1' ").arg(filters.at(3)));

	if (!filters.at(4).isEmpty())
		params.append(QString(" AND num_card_poryadk = '%1' ").arg(filters.at(4)));

	if (!filters.at(5).isEmpty())
		params.append(QString(" AND fio_driver = '%1' ").arg(filters.at(5)));

	if (!filters.at(6).isEmpty())
		params.append(QString(" AND material = '%1' ").arg(filters.at(6)));

	if (filters.at(7).toInt() > 0) // index of combo box > 0
		params.append(QString(" AND blocked = %1 ").arg(filters.at(7).toInt() - 1));

	result = query.exec(QString("SELECT * FROM reportcards WHERE %1; ").arg(params));
	error = query.lastError();

	progrBar->show();

	num_records = query.size();
	percent = 0;

	tableWidget->setUpdatesEnabled(false);
	tableWidget->setEnabled(false);
	tableWidget->setVisible(false);
	tableWidget->blockSignals(true);
	tableWidget->setSortingEnabled(false);

	int r = query.size();
	int c = query.record().count()  - 3  /*- 10 */;
	QString type_tara;  //��������. "���� �� �����������" ��� "���� � ��"
	bool priznak_tara;  // ��� ����� ������� ���� 0 ��� 1 �� ���� auto_tara 

	if (r < 0 || c < 0)
	{
		r = 0;
		c = headers_names.length();
	}
	reportsTableModel_Obj = NULL;
	// ������ ������ ������ ��� ������������� � � QTableView
	if (reportsTableModel_Obj == NULL)
	{
		reportsTableModel_Obj = new ReportsTableModel(r, c, headers_names);
	}
	else
	{
		reportsTableModel_Obj->clear();
		reportsTableModel_Obj->addParams(r, c, headers_names);
	}

	reportsTableModel_Obj->clearColoredLinesSign();
	int i = 0;
	while (query.next())
	{
		data.append(query.record().value("date_time_accept").toDateTime().toString("dd.MM.yyyy hh:mm:ss"));
		data.append(query.record().value("date_time_send").toDateTime().toString("dd.MM.yyyy hh:mm:ss"));
		data.append(query.record().value("weight_1").toString());
		data.append(query.record().value("weight_2").toString());
		data.append(query.record().value("netto").toString());

		data.append(query.record().value("IDF").toString());
		data.append("save");
		//data.append(QTextCodec::codecForName("Windows-1251")->toUnicode("����"));
		data.append(query.record().value("code").toString());
		data.append( query.record().value( "supplier" ).toString() );
		data.append( query.record().value( "material" ).toString() );
		data.append( query.record().value( "num_auto" ).toString() );
		data.append( query.record().value( "num_prizep" ).toString() );
		data.append( query.record().value( "fio_driver" ).toString() );
		data.append( query.record().value( "num_card_poryadk" ).toString() );

		     priznak_tara = ( query.record().value( "auto_tara" ).toBool() );
			    if ( priznak_tara == true)
			    	{
				      type_tara = "���� �� �����������";
				    }
				else {type_tara = "���� � �������� �������";}
        data.append(type_tara);
		//data.append( query.record().value( "auto_tara" ).toString() );

		// int blocked = query.record().value( "blocked" ).toInt();
		// data.append( ( blocked == 0 ? QTextCodec::codecForName("Windows-1251")->toUnicode( "��" ) : ( blocked == 1 ? QTextCodec::codecForName("Windows-1251")->toUnicode( "���" ) : QTextCodec::codecForName("Windows-1251")->toUnicode( "" ) ) ) );

		/*if( blocked == 1 )
		{
		reportsTableModel_Obj->setColoredLinesSign( ReportsTableModel::Colors::GRAY );
		}
		else
		{
		reportsTableModel_Obj->setColoredLinesSign( ReportsTableModel::Colors::WHITE );
		}*/

		

		//data.append(query.record().value("num_auto").toString());
		//data.append(query.record().value("sender").toString());
		//data.append(query.record().value("accepter").toString());
		//data.append(query.record().value("transporter").toString());
		//data.append(query.record().value("goods").toString());
		//data.append(query.record().value("name_user_accept").toString());
		//data.append(query.record().value("name_user_send").toString());
		//data.append(query.record().value("nakladnaya").toString());
		//data.append(query.record().value("num_prizep").toString());
		//data.append(query.record().value("tara_type").toString());


		reportsTableModel_Obj->update(&data);

		

		data.clear();
	}

	tableWidget->setModel(reportsTableModel_Obj);
	tableWidget->setUpdatesEnabled(false);

//	tableWidget->setStyleSheet("font-weight: bold; font-size: 12px;");

	for (int i = 0; i < headers_names.length(); i++)
	{
		tableWidget->setColumnWidth(i, 180);

	}

	QPixmap pixmap( "image/save_icon_24.png" );
	for(int i = 0; i < r; i++)
		{
			
			buttonPhotoShow = new QPushButton( tr("Photos") ); //���� �����������
			buttonPhotoShow->setFixedHeight( 24 );
			buttonPhotoShow->setFixedWidth( 140 );
			QObject::connect( buttonPhotoShow, SIGNAL( clicked() ), this, SLOT( showPhotoReports() ) );

			QModelIndex index = reportsTableModel_Obj->index( i, 5, QModelIndex() );
			tableWidget->setIndexWidget( index, buttonPhotoShow );

			    buttonPhotoSave = new QPushButton();
				buttonPhotoSave->setFixedHeight( 24 );
				buttonPhotoSave->setFixedWidth( 140 );
				buttonPhotoSave->setPixmap( pixmap );
				QObject::connect( buttonPhotoSave, SIGNAL( clicked() ), this, SLOT( savePhotoReports() ) );

				QModelIndex index1 = reportsTableModel_Obj->index( i, 6, QModelIndex() );
				tableWidget->setIndexWidget( index1, buttonPhotoSave );

		}

	tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers); // ��������� ������������� ������
	tableWidget->setAlternatingRowColors(true);

	tableWidget->setUpdatesEnabled(true);
	progrBar->hide();
	tableWidget->setEnabled(true);
	tableWidget->setVisible(true);
	tableWidget->blockSignals(false);
	tableWidget->setSortingEnabled(true);

	return result;
}

// ������� ������ �� �� ��� ������������ ������ �� ������ ������� 
bool mysqlProccess::getDataForHourlyCardsReports( QString last_dateTime, QString now_dateTime, QVector< QList< QString > > *data )
{
	bool result;
	QSqlError error;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	result = query.exec( QString( "SELECT * FROM reportcards WHERE date_time_accept >= '%1' AND date_time_accept <= '%2';" )
		.arg( now_dateTime )
		.arg( last_dateTime ) );

	int size = query.size();
	int num_columns = query.record().count();

	QList< QString > row_data;

	if( size != 0 )
	{
		for( int j = 0; j < size; j++ )
		{
			query.next();

			row_data.append( query.record().value( "date_time_accept" ).toDateTime().toString( "dd.MM.yyyy hh:mm:ss" ) );
			row_data.append( query.record().value( "date_time_send" ).toDateTime().toString( "dd.MM.yyyy hh:mm:ss" ) );
			
			row_data.append( query.record().value( "weight_1" ).toString() );
			row_data.append( query.record().value( "weight_2" ).toString() );
			row_data.append( query.record().value( "netto" ).toString() );

			row_data.append( query.record().value( "" ).toString() );
			row_data.append( query.record().value( "code" ).toString() );
			row_data.append( query.record().value( "supplier" ).toString() );
			row_data.append( query.record().value( "material" ).toString() );
			row_data.append( query.record().value( "num_auto" ).toString() );
			row_data.append( query.record().value( "num_prizep" ).toString() );
			row_data.append( query.record().value( "fio_driver" ).toString() );
			row_data.append( query.record().value( "num_card_poryadk" ).toString() );

			data->append( row_data );
			row_data.clear();
		}
	}

	return result;
}

// 
bool mysqlProccess::saveReportsTableData(struct Record & record, int idf)
{
	bool result = false;
	QSqlError error;

	if (!isConnected())
	{
		for (int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN; i++)
		{
			if (connect())
				break;
		}
	}

	QSqlQuery query(*getQSqlInstance());
	QString str;
	result = query.prepare(str = QString("INSERT INTO reportcards ( date_time_accept, date_time_send, "
		"code, weight_1, weight_2, netto, supplier, material, num_auto, num_prizep, fio_driver, num_card_poryadk, blocked, idf ) "
		"VALUES ( :date_time_accept, :date_time_send, "
		":code, :weight_1, :weight_2, :netto, :supplier, :material, :num_auto, :num_prizep, :fio_driver, :num_card_poryadk, :blocked, :idf );"));
	qDebug()<<str;
	query.bindValue(":date_time_accept", record.dateTimeAccept);
	query.bindValue(":date_time_send", QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
	query.bindValue(":code", record.code);

	query.bindValue(":weight_1", record.weight_1);
	query.bindValue(":weight_2", record.weight_2);
	query.bindValue(":netto", record.netto);

	 query.bindValue( ":supplier", record.supplier );
	 query.bindValue( ":material", record.material );
	 query.bindValue( ":num_auto", record.num_auto );
	 query.bindValue( ":num_prizep", record.num_prizep );
	 query.bindValue( ":fio_driver", record.fio_driver );
	 query.bindValue( ":num_card_poryadk", record.num_card_poryadk );
	 query.bindValue( ":blocked", record.blocked );
	 query.bindValue( ":idf", idf );

	result = query.exec();
	error = query.lastError();
	QString error_text = error.text();

	if (result == true)
	{
		if (!query.exec(QString("DELETE FROM acceptcards WHERE date_time = '%1';")
			.arg(record.dateTimeAccept)))
		{
			result = false;
		}
	}

	return result;
}

bool mysqlProccess::hasRecordMainTable(QList< QString > & data, QString code)
{
	bool result;
	QSqlError error;

	if (!isConnected())
	{
		for (int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN; i++)
		{
			if (connect())
				break;
		}
	}

	QSqlQuery query(*getQSqlInstance());

	result = query.exec(QString("SELECT * FROM acceptcards WHERE code = '%1' AND blocked = 0;")
		.arg(code));

	int r = query.size();

	if (r > 0)
		result = true;
	else
		result = false;

	while (query.next())
	{
		data.append(query.record().value("date_time").toDateTime().toString("dd.MM.yyyy hh:mm:ss"));
		data.append(query.record().value("code").toString());
		data.append(query.record().value("weight_1").toString());
		data.append(query.record().value("weight_2").toString());
		data.append(query.record().value("netto").toString());
		data.append(query.record().value("supplier").toString());
		data.append(query.record().value("material").toString());
		data.append(query.record().value("num_auto").toString());
		data.append(query.record().value("num_prizep").toString());
		data.append(query.record().value("fio_driver").toString());
		data.append(query.record().value("num_card_poryadk").toString());
		data.append(query.record().value("blocked").toString());
	}

	return result;
}

int mysqlProccess::getIdfForRecord(QString dateTime)
{
	bool result;
	QSqlError error;

	if (!isConnected())
	{
		for (int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN; i++)
		{
			if (connect())
				break;
		}
	}

	QSqlQuery query(*getQSqlInstance());

	result = query.exec(QString("SELECT idf FROM acceptcards WHERE date_time = '%1';")
		.arg(dateTime));

	int idf = 0;

	if (query.next())
	{
		idf = query.value(0).toInt();
	}

	return idf;
}

// 
int mysqlProccess::getIdfInTableForPhoto(QString date_time)
{
	int idf = 0;

	QString logEventDataList;
	bool result;
	QSqlError error;

	if (!isConnected())
	{
		for (int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN; i++)
		{
			if (connect())
				break;
		}
	}

	QSqlQuery query(*getQSqlInstance());

	result = query.exec(QString("SELECT ID FROM images WHERE date_time = '%1';").
		arg(date_time));
	result = query.exec();

	error = query.lastError();
	QString temp = error.text();

	if (result == true)
	{
		if (query.next())
			idf = query.value(0).toInt();
	}

	return idf;
}

// 
int mysqlProccess::saveRecordsPhotos(QString dateTime)
{
	bool result = false;
	QSqlError error;

	if (!isConnected())
	{
		for (int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN; i++)
		{
			if (connect())
				break;
		}
	}

	QSqlQuery query(*getQSqlInstance());
	result = query.prepare(QString("INSERT INTO images ( date_time ) "
		"VALUES ( :date_time );"));
	query.bindValue(":date_time", dateTime);
	result = query.exec();
	error = query.lastError();
	QString error_text = error.text();

	int id = 0;
	result = query.exec(QString("SELECT MAX(id) FROM images; "));
	error = query.lastError();
	error_text = error.text();

	if (query.next())
	{
		id = query.value(0).toInt();
	}

	return id;
}

// num_measure - 0 - first, 1 - second
bool mysqlProccess::updateRecordsPhotos(int id, int num_cam, QByteArray photo, int num_measure)
{
	bool result = false;
	QSqlError error;
	QString error_text;

	if (!isConnected())
	{
		for (int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN; i++)
		{
			if (connect())
				break;
		}
	}

	QSqlQuery query(*getQSqlInstance());

	if (num_measure == 0) // first measure
	{
		switch (num_cam)
		{
		case 0:
		{
			result = query.prepare(QString("UPDATE images SET photo_1=:photo_1 WHERE id=%1;")
				.arg(id));
			query.bindValue(":photo_1", photo);
		} break;
		case 1:
		{
			result = query.prepare(QString("UPDATE images SET photo_2=:photo_2 WHERE id=%1;")
				.arg(id));
			query.bindValue(":photo_2", photo);
		} break;
		case 2:
		{
			result = query.prepare(QString("UPDATE images SET photo_3=:photo_3 WHERE id=%1;")
				.arg(id));
			query.bindValue(":photo_3", photo);
		} break;
		case 3:
		{
			result = query.prepare(QString("UPDATE images SET photo_4=:photo_4 WHERE id=%1;")
				.arg(id));
			query.bindValue(":photo_4", photo);
		} break;
		}

		result = query.exec();
		error = query.lastError();
		error_text = error.text();
	}
	else // second measure
	{
		switch (num_cam)
		{
		case 0:
		{
			result = query.prepare(QString("UPDATE images SET photo_5=:photo_5 WHERE id=%1;")
				.arg(id));
			query.bindValue(":photo_5", photo);
		} break;
		case 1:
		{
			result = query.prepare(QString("UPDATE images SET photo_6=:photo_6 WHERE id=%1;")
				.arg(id));
			query.bindValue(":photo_6", photo);
		} break;
		case 2:
		{
			result = query.prepare(QString("UPDATE images SET photo_7=:photo_7 WHERE id=%1;")
				.arg(id));
			query.bindValue(":photo_7", photo);
		} break;
		case 3:
		{
			result = query.prepare(QString("UPDATE images SET photo_8=:photo_8 WHERE id=%1;")
				.arg(id));
			query.bindValue(":photo_8", photo);
		} break;
		}

		result = query.exec();
		error = query.lastError();
		error_text = error.text();
	}

	return result;
}

// 
bool mysqlProccess::removeRecordsPhotos(int id)
{
	bool result = false;
	QSqlError error;

	if (!isConnected())
	{
		for (int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN; i++)
		{
			if (connect())
				break;
		}
	}

	QSqlQuery query(*getQSqlInstance());
	result = query.exec(QString("DELETE FROM images WHERE id=%1;")
		.arg(id));

	result = query.exec();
	error = query.lastError();
	QString error_text = error.text();

	return result;
}

// 
 
QVector< QByteArray > mysqlProccess::getSavedPhoto(int id)
{
	QVector< QByteArray > data;

	bool result;
	QSqlError error;
	int i = 0;

	if (!isConnected())
	{
		for (int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN; i++)
		{
			if (connect())
				break;
		}
	}

	QSqlQuery query(*getQSqlInstance());

	// result = query.prepare( QString( "SET GLOBAL max_allowed_packet=32000000;" ) );
	result = query.prepare(QString("SELECT photo_1 FROM images WHERE ID = %1;").
		arg(id));
	result = query.exec();

	if (query.next())
	{
		data.append(query.record().value("photo_1").toByteArray());
		i++;
	}

	// result = query.prepare( QString( "SET GLOBAL max_allowed_packet=32000000;" ) );
	result = query.prepare(QString("SELECT photo_2 FROM images WHERE ID = %1;").
		arg(id));
	result = query.exec();

	while (query.next())
	{
		data.append(query.record().value("photo_2").toByteArray());
		i++;
	}

	// result = query.prepare( QString( "SET GLOBAL max_allowed_packet=32000000;" ) );
	result = query.prepare(QString("SELECT photo_3 FROM images WHERE ID = %1;").
		arg(id));
	result = query.exec();

	while (query.next())
	{
		data.append(query.record().value("photo_3").toByteArray());
		i++;
	}

	// 
	result = query.prepare(QString("SELECT photo_4 FROM images WHERE ID = %1;").
		arg(id));
	result = query.exec();

	while (query.next())
	{
		data.append(query.record().value("photo_4").toByteArray());
		i++;
	}

	// result = query.prepare( QString( "SET GLOBAL max_allowed_packet=32000000;" ) );
	result = query.prepare(QString("SELECT photo_5 FROM images WHERE ID = %1;").
		arg(id));
	result = query.exec();

	while (query.next())
	{
		data.append(query.record().value("photo_5").toByteArray());
		i++;
	}

	// 6
	result = query.prepare(QString("SELECT photo_6 FROM images WHERE ID = %1;").
		arg(id));
	result = query.exec();

	while (query.next())
	{
		data.append(query.record().value("photo_6").toByteArray());
		i++;
	}

	// 7
	result = query.prepare(QString("SELECT photo_7 FROM images WHERE ID = %1;").
		arg(id));
	result = query.exec();

	while (query.next())
	{
		data.append(query.record().value("photo_7").toByteArray());
		i++;
	}

	// 8
	result = query.prepare(QString("SELECT photo_8 FROM images WHERE ID = %1;").
		arg(id));
	result = query.exec();

	while (query.next())
	{
		data.append(query.record().value("photo_8").toByteArray());
		i++;
	}

	error = query.lastError();
	QString temp = error.text();

	return data;
}


// 
bool mysqlProccess::removeRecordFromSpravochniki( QString dateTime )
{
	bool result;
	QSqlError error;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	result = query.exec( QString( "DELETE FROM registration WHERE date_time = '%1'; " )
		.arg( dateTime ) );

	return result;
}

// 

// 
//mysqlProccess::~mysqlProccess()
//{
//	m_Instance->disconnect2();
//	delete m_Instance;
//}

//================


// ������� ��
void mysqlProccess::createMySQL_DataBase()
{
	bool result = false, flag;
	QMessageBox mess;


	QSqlQuery query( *m_sqlDatabase );
	QSqlError error;

	// query.prepare( "USE %1; DROP DATABASE %1;" );
	// result = query.exec();

	QString str = m_sqlDatabase->databaseName();

	result = query.exec( QString( "CREATE DATABASE IF NOT EXISTS %1 DEFAULT CHARACTER SET utf8;" ).arg( dataBaseName ) );
	error = query.lastError();

	// ������� ��������� 
	result = query.exec( QString( "SELECT id FROM %1.users;" ).arg( dataBaseName ) );
	if( result == false )
	{
		result = query.exec( QString( "USE %1; CREATE TABLE IF NOT EXISTS users( id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, Name CHAR( 30 ), Password CHAR( 30 ), Role INT );" ).
			arg( dataBaseName )  );
		error = query.lastError();
		query.exec( QString( "INSERT INTO users ( Name, Password, Role ) VALUES( 'ADMIN', '', 1 );" ) );
		error = query.lastError();
	}

	// ������� ����������� 
	result = query.exec( QString( "USE %1; CREATE TABLE IF NOT EXISTS images( ID INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, Date_Time DATETIME default '0000-00-00 00:00:00', "
		"photo_1 MEDIUMBLOB, photo_2 MEDIUMBLOB, photo_3 MEDIUMBLOB, photo_4 MEDIUMBLOB, photo_5 MEDIUMBLOB, photo_6 MEDIUMBLOB, photo_7 MEDIUMBLOB, photo_8 MEDIUMBLOB );" ).arg( dataBaseName ) );
	error = query.lastError();

	// ������� ������������ 
	result = query.exec( QString( "USE %1; CREATE TABLE IF NOT EXISTS dictionaries( id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, senders CHAR( 48 ), receivers CHAR( 48 ), "
		"payers CHAR( 48 ), transporters CHAR( 48 ), goods CHAR( 48 ), Number_Auto CHAR( 20 ), Num_Prizep CHAR( 30 ) );" ).arg( dataBaseName ) );
	error = query.lastError();
	// ���������� ���� ��� ������ ���� � ������������ ����
	result = query.exec( QString( "USE %1; alter table dictionaries add Number_Auto CHAR( 20 ) after goods;" ).arg( dataBaseName ) );
	error = query.lastError();
	// ���������� ���� ��� ������ ������� � ������������ ����
	result = query.exec( QString( "USE %1; alter table dictionaries add Num_Prizep CHAR( 30 ) after Number_Auto;" ).arg( dataBaseName ) );
	error = query.lastError();


	// 
	result = query.exec( QString( "USE %1; CREATE TABLE IF NOT EXISTS logs( id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, Date_Time DATETIME default '0000-00-00 00:00:00', "
		"event CHAR( 200 ) );" ).arg( dataBaseName ) );
	error = query.lastError();

	result = query.exec( QString( "USE %1; CREATE TABLE IF NOT EXISTS ttn_data( id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, date_time DATETIME default '0000-00-00 00:00:00',"
		"nakladnaya CHAR(50), date_nakl CHAR(30), month_nakl CHAR(30), year_nakl CHAR(30), auto_info CHAR(100), pricep CHAR(100), type_of_transporting CHAR(100), transporter CHAR(100), "
		"driver_name CHAR(50), orderer_name CHAR(100), sender_info CHAR(100), accepter_info CHAR(100), point_loading CHAR(100), point_unloading CHAR(100), readress_of_goods_info CHAR(100), "
		"serial_serial_dov CHAR(100), num_dov CHAR(100), date_dov CHAR(30), month_dov CHAR(30), year_dov CHAR(30), vidano CHAR(100), state_of_goods CHAR(100), rules_of_goods CHAR(100), "
		"netto CHAR(68), brutto CHAR(68), expeditor_info CHAR(100), buhgalter_info CHAR(100), vidpusk_dozvoliv CHAR(100), total_price VARCHAR(100), nds INT, suprovodni_docs CHAR(100), "
		"transport_service CHAR(100), others CHAR(200)"
		");" ).arg( dataBaseName ) );

	error = query.lastError();

	result = query.exec( QString( "USE %1; CREATE TABLE IF NOT EXISTS ttn_data_2( id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, date_time DATETIME default '0000-00-00 00:00:00', "
		"Num INT, NameOfGoods CHAR(200), edIsm CHAR(30), Netto DOUBLE, PriceWithOutPDV DOUBLE, ZagalnaSummaZ_PDV DOUBLE, VidPakuvannya CHAR(100), Docs_Z_Vantagem CHAR(200), Brutto Double);" ).
		arg( dataBaseName ) );
	error = query.lastError();

	result = query.exec( QString( "USE %1; CREATE TABLE IF NOT EXISTS ttn_data_3( id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, date_time DATETIME default '0000-00-00 00:00:00',"
		"Operation CHAR(100), Operation_2 CHAR(100), Brutto DOUBLE, Brutto_2 DOUBLE, Chas_pributtya CHAR(30), Chas_pributtya_2 CHAR(30), Chas_vibuttya CHAR(30), Chas_vibuttya_2 CHAR(30), "
		"Chas_prostoyu CHAR(30), Chas_prostoyu_2 CHAR(30), Pidpis CHAR(30), Pidpis_2 CHAR(30) );" ).arg( dataBaseName ) );

	error = query.lastError();

	result = query.exec( QString( "USE %1; CREATE TABLE IF NOT EXISTS ttn_data_4( id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, date_time DATETIME default '0000-00-00 00:00:00',"
		"vidpovidalnaOsoba CHAR(100), priynyavExpeditor CHAR(100), zdavExpeditor CHAR(100), prinyalVidpovOsoba CHAR(100) );" ).arg( dataBaseName ) );
	error = query.lastError();

	// ������� ������/�������� ����������� 
	result = query.prepare( QString( "USE %1; CREATE TABLE IF NOT EXISTS AutoReception( id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, type_operation INT, num_scales INT, "
		"Number_Auto CHAR(14), Sender CHAR(48), Accepter CHAR(48),  Payer CHAR(48), Transporter CHAR(48), Goods CHAR(48), Price DOUBLE, Brutto DOUBLE, Tara DOUBLE, Netto DOUBLE, "
		"Date_Time DATETIME default '0000-00-00 00:00:00', Nakladnaya CHAR(30), Name_User_Accept CHAR(30), Num_Prizep CHAR(30), Brutto_Prizep DOUBLE, Tara_Prizep DOUBLE, "
		"Netto_Prizep DOUBLE, IDF INT, FOREIGN KEY (IDF) REFERENCES images (ID) ON UPDATE CASCADE, tara_type INT );" ).arg( dataBaseName ) );

	result = query.exec();
	error = query.lastError();

	// ������� ������� ����������� 
	result = query.prepare( QString( "USE %1; CREATE TABLE IF NOT EXISTS ReportsWeight( id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, type_operation INT, num_scales INT, "
		"Number_Auto CHAR(30), Sender CHAR(48), Accepter CHAR(48), Payer CHAR(48), Transporter CHAR(48), Goods CHAR(48), Price DOUBLE, Brutto DOUBLE, Tara DOUBLE, Netto DOUBLE, "
		"Date_Time_Accept DATETIME default '0000-00-00 00:00:00', Name_User_Accept CHAR(30), Date_Time_Send DATETIME default '0000-00-00 00:00:00', Name_User_Send CHAR(30), "
		"Nakladnaya CHAR(30), Num_Prizep CHAR(30), Brutto_Prizep DOUBLE, Tara_Prizep DOUBLE, Netto_Prizep DOUBLE, IDF INT, FOREIGN KEY (IDF) REFERENCES images (ID) ON UPDATE CASCADE, tara_type INT );" ).
		arg( dataBaseName ) );

	result = query.exec();
	error = query.lastError();

	// ������� ������ �� ������
	result = query.prepare(QString("USE %1; CREATE TABLE IF NOT EXISTS acceptcards(id INTEGER NOT NULL AUTO_INCREMENT, date_time datetime DEFAULT '0000-00-00 00:00:00', "
		"numScales int(11), code varchar(30), weight_1 double, weight_2 double, netto double, supplier varchar(100), material varchar(100), "
		"num_auto varchar(30), num_prizep varchar(30), fio_driver varchar(100), num_card_poryadk varchar(30), blocked int(11), IDF int(11), "
		"PRIMARY KEY (id), KEY IDF (IDF), CONSTRAINT acceptcards_ibfk_1 FOREIGN KEY (IDF) REFERENCES images (ID) ON UPDATE CASCADE );").
		arg(dataBaseName));
	result = query.exec();
	error = query.lastError();

	// ������� ������� �� ������
	result = query.prepare(QString("USE %1; CREATE TABLE IF NOT EXISTS reportcards(id INTEGER NOT NULL AUTO_INCREMENT, "
		"date_time_accept datetime DEFAULT '0000-00-00 00:00:00', date_time_send datetime DEFAULT '0000-00-00 00:00:00', "
		"numScales int(11), code varchar(30), weight_1 double, weight_2 double, netto double, supplier varchar(100), material varchar(100), "
		"num_auto varchar(30), num_prizep varchar(30), fio_driver varchar(100), num_card_poryadk varchar(30), blocked int(11), IDF int(11), "
		"PRIMARY KEY (id), KEY IDF (IDF), CONSTRAINT reportcards_ibfk_1 FOREIGN KEY (IDF) REFERENCES images (ID) ON UPDATE CASCADE );").
		arg(dataBaseName));
	result = query.exec();
	error = query.lastError();

	// ������� ����������� ���� 
	result = query.prepare( QString( "USE %1; CREATE TABLE IF NOT EXISTS registration( id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, date_time DATETIME default '0000-00-00 00:00:00', "
		"code VARCHAR(30), supplier VARCHAR(100), material VARCHAR(100), num_auto VARCHAR(30), num_prizep VARCHAR(30), fio_driver VARCHAR(100), num_card_poryadk VARCHAR(30), blocked INT, auto_tara BOOL);" ).
		arg( dataBaseName ) );
	result = query.exec();
	error = query.lastError();

	// ���������� ���� ��� �������� ���� �� ����������� � ������������ ����
	result = query.exec( QString( "USE %1; alter table registration add auto_tara BOOL after blocked;" ).arg( dataBaseName ) );
	error = query.lastError();

    // ������� ����������� ���� ���� 
	result = query.prepare( QString( "USE %1; CREATE TABLE IF NOT EXISTS taraAuto( id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, date_time DATETIME default '0000-00-00 00:00:00', "
		"number_auto VARCHAR(48), mark_auto VARCHAR(48), weight_tara DOUBLE );" )
		.arg( dataBaseName ) );
	result = query.exec();

	result = query.exec( QString( "USE %1; CREATE TABLE IF NOT EXISTS sensorsdataforservice( id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, date_time DATETIME default '0000-00-00 00:00:00', "
		"sensor1 INT, "
		"sensor2 INT, "
		"sensor3 INT, "
		"sensor4 INT, "
		"sensor5 INT, "
		"sensor6 INT, "
		"sensor7 INT, "
		"sensor8 INT, "
		"code1 INT, "
		"code2 INT, "
		"code3 INT, "
		"code4 INT, "
		"code5 INT, "
		"code6 INT, "
		"code7 INT, "
		"code8 INT );"
		).arg( dataBaseName ) );
	error = query.lastError();

	// �������� ��� ��� �������� �� ���� ����������� ��� �������
	result = query.exec( QString( "USE %1; CREATE TABLE IF NOT EXISTS sor( "
		"id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, "
		"is_report INT, "
		"real_weight_brutto INT, "
		"real_weight_tara INT, "
		"IDF INT );" ).arg( dataBaseName ) );

	result = query.exec();
	error = query.lastError();

	result = query.exec( QString( "SELECT id FROM %1.modules;" ).arg( dataBaseName ) );
	if( result == false )
	{
		query.exec( QString( "USE %1; CREATE TABLE IF NOT EXISTS modules( "
			"id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, "
			"module CHAR(15), "
			"pass CHAR(30) );" ).arg( dataBaseName ) );
		
		query.exec( QString( "INSERT INTO modules ( module ) VALUES( 'camera' );" ) );
		query.exec( QString( "INSERT INTO modules ( module ) VALUES( 'sornost' );" ) );
		query.exec( QString( "INSERT INTO modules ( module ) VALUES( 'svetofor' );" ) );
		query.exec( QString( "INSERT INTO modules ( module ) VALUES( 'shlagbaum' );" ) );
		query.exec( QString( "INSERT INTO modules ( module ) VALUES( 'datchik' );" ) );
		query.exec( QString( "INSERT INTO modules ( module ) VALUES( 'tablo' );" ) );
		query.exec( QString( "INSERT INTO modules ( module ) VALUES( 'rfid' );" ) );
		query.exec( QString( "INSERT INTO modules ( module ) VALUES( 'bot' );" ) );
		query.exec( QString( "INSERT INTO modules ( module ) VALUES( 'PO' );" ) );
	}
	// ���������� ���� ��� ������ ���� � ������������ ����
	result = query.exec( QString( "USE %1; alter table modules add bot CHAR( 30 ) after rfid;" ).arg( dataBaseName ) );
	error = query.lastError();

	//������ ��� ������ ������ ����� ����� rfid
	result = query.exec( QString( "SELECT module FROM %1.modules WHERE module='rfid';" ).arg( dataBaseName ) );
	if( result == false || query.next() == false )
		query.exec( QString( "INSERT INTO modules ( module ) VALUES( 'rfid' );" ) );

	// ������������ ������� ����
	result = query.exec( QString( "SELECT id FROM %1.trial_work;" ).arg( dataBaseName ) );
	if( result == false )
	{
		result = query.exec( QString( "USE %1; CREATE TABLE IF NOT EXISTS trial_work( "
		"id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,"
		"data_start DATETIME default '0000-00-00 00:00:00', "
		"data_of_work INT, "
		"data_tmp_1 INT, "
		"data_tmp_2 INT, "
		"checker INT, "
		"data_end DATETIME default '0000-00-00 00:00:00' );" ).arg( dataBaseName ) );

		error = query.lastError();
	
	    QString strDateTime = QDateTime::currentDateTime().toString( "yyyy-MM-dd hh:mm:ss" );
		int dw = 123;
		int dt1 = 65;
		int dt2 = 36;
		int ch = 87;
		qDebug()<<strDateTime;
		query.prepare( QString("INSERT INTO trial_work ( data_start, data_of_work, data_tmp_1, data_tmp_2, checker )"
			"VALUES( :data_start, :data_of_work, :data_tmp_1, :data_tmp_2, :checker );" ) );
		query.bindValue( ":data_start", strDateTime );
		query.bindValue( ":data_of_work", dw );
		query.bindValue( ":data_tmp_1", dt1 );
		query.bindValue( ":data_tmp_2", dt2 );
		query.bindValue( ":checker", ch );
		query.exec();
		error = query.lastError();
	}

	// use Scales_Auto_v2;
	// alter table ReportsWeight add tara_type INT After IDF;
	// describe ReportsWeight;

	query.exec( QString( "USE %1; CREATE TABLE IF NOT EXISTS totalReports( "
		"ID INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, "
		"Date_Time DATETIME default '0000-00-00 00:00:00', "
		"weight_moment INT, "
		"weight_max INT, "
		"registered INT, "	// 1 - in reports; 0 - not, so its hidden
		"IDF INT, FOREIGN KEY (IDF) REFERENCES images (ID) ON UPDATE CASCADE );" )
		.arg( dataBaseName ) );
	error = query.lastError();

		// ������� ������ ��� ����

		result = query.exec( QString( "USE %1; CREATE TABLE IF NOT EXISTS dataBot( "
		"id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,"
		"data_bot varchar(200), "
		"date_time_bot DATETIME default '0000-00-00 00:00:00' );" ).arg( dataBaseName ) );
	error = query.lastError();
	qDebug()<<error.text();
	//flag = sqlDatabase->isOpen();
	//return result;
}

//QSqlDatabase *mysqlProccess::connect()
//{
//	bool result = false;
//
//	static QString login;
//	static QString password;
//	static QString address;
//	static int port;
//
//	if( login.isEmpty() )
//	{
//		QSettings settings( "settings.ini", QSettings::IniFormat );
//		settings.setIniCodec("Windows-1251");
//
//		address = settings.value( "MySQL/address" ).toString();
//		login = settings.value( "MySQL/login" ).toString();
//
//		//enc-dec pass
//		QString pass = settings.value( "MySQL/password" ).toString();
//		QString pass_enc;
//		if ( ! pass.startsWith("#") )
//			pass_enc = Crypt::crypt( pass );
//		else
//		{
//			pass_enc = pass.remove( 0, 1 );
//			pass = Crypt::decrypt( pass_enc );
//		}
//		// 
//		password = pass;
//		port = settings.value( "MySQL/port" ).toInt();
//
//		m_sqlDatabase->setHostName( address );	
//		m_sqlDatabase->setUserName( login );
//		m_sqlDatabase->setPassword(password );
//		m_sqlDatabase->setPort( port );
//	}
//	
//	if( !m_sqlDatabase->isOpen() )
//	{
//		for( int i = 0; i < 3; i++ )
//		{
//			if( mySqlDatabase->open() ) break;
//			else
//			{
//				qDebug( "___Open database failed!    DB_Driver.cpp-connect()" );
//
//				QMessageBox mess;
//				mess.setText( "������ ����������� � ���� ������!" );
//				mess.setStandardButtons( QMessageBox::Cancel );
//				mess.setButtonText( QMessageBox::Cancel, "Ok" );
//				mess.setIcon( QMessageBox::Warning );
//				mess.setWindowTitle( tr( "Prom-Soft" ) );
//				mess.exec();
//			}
//		}		
//	}
//	mySqlDatabase = m_sqlDatabase;
//    return m_sqlDatabase;
//}

// ������������ � ��
// ���������� true � ������ ������ 
bool mysqlProccess::connectToDB_MySql( QSqlDatabase *sqlDatabase, QString db_name ) // ����������� � �� 
{
	bool result = false;

	static QString login;
	static QString password;
	static QString address;
	static int port;

	if( login.isEmpty() )
	{
		QSettings settings( "settings.ini", QSettings::IniFormat );
		settings.setIniCodec("Windows-1251");

		address = settings.value( "MySQL/address" ).toString();
		login = settings.value( "MySQL/login" ).toString();
		//enc-dec pass
		QString pass = settings.value( "MySQL/password" ).toString();
		QString pass_enc;
		if ( ! pass.startsWith("#") )
			pass_enc = Crypt::crypt( pass );
		else
		{
			pass_enc = pass.remove( 0, 1 );
			pass = Crypt::decrypt( pass_enc );
		}
		// 
		password = pass;
		port = settings.value( "MySQL/port" ).toInt();
	}

	sqlDatabase->setHostName( address );	
	sqlDatabase->setUserName( login );
	sqlDatabase->setPassword(password );
	sqlDatabase->setPort( port );	

	result = sqlDatabase->open();
    return result;
}
// ���������� �� ��
// ���������� true � ������ ������ 
void mysqlProccess::disconnectFromDB_MySql( QSqlDatabase *sqlDatabase, QString db_name ) // ���������� �� �� 
{
	sqlDatabase->close();
}
// �������� ������� � ��� ������ ������ (200 ����)
void mysqlProccess::saveEventToLog( QString logEventData )
{
    bool result;
	QSqlError error;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );
    // 
	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

	QString strDateTime = QDateTime::currentDateTime().toString( "yyyy-MM-dd hh:mm:ss" );

    query.prepare( "INSERT INTO logs( Date_Time, event ) VALUES( :Date_Time, :event );" );
	query.bindValue( ":Date_Time", strDateTime );
	query.bindValue( ":event", logEventData );

	query.exec();
	error = query.lastError();

}
// ��������� ������� �� ��� 
QString mysqlProccess::loadEventFromLog( QDateTime startDateTime, QDateTime endDateTime )
{
	QString logEventDataList;
    bool result;
	QSqlError error;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );
    // 
	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

	QString dateTimeStart = startDateTime.toString( "yyyy-MM-dd hh:mm:ss" );
	QString dateTimeEnd = endDateTime.toString( "yyyy-MM-dd hh:mm:ss" );

	// int i = 0;
	query.exec( QString( "SELECT Date_Time, event FROM logs WHERE Date_Time>='%1' AND Date_Time<='%2';" ).arg( dateTimeStart ).arg( dateTimeEnd ) );
	error = query.lastError();

	logEventDataList.clear();

    while( query.next() )
	{
		logEventDataList.append( query.value( 0 ).asDateTime().toString( "dd.MM.yyyy hh:mm:ss" ) ).append( "    " ).append( query.value( 1 ).toString() ).append( "\r\n\r\n" ); // ������ �������/���� ������ ���������� �� ���������� 
    }
	
    return logEventDataList;
}

// ������� ������ �� �� ��� ������������ ������ �� ������ ������� 
bool mysqlProccess::getDataForHourlyReports( QString last_dateTime, QString now_dateTime, typesTables table, QVector< QList< QString > > *data,  bool bSaveEachRec )
{
	bool result;
	QSqlError error;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	result = query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

	if( table == MAIN_TABLE )
	    result = query.exec( QString( "SELECT * FROM AutoReception WHERE DATE_TIME >= '%1' AND DATE_TIME <= '%2';" ).arg( last_dateTime ).arg( now_dateTime ) );
	else
		if ( bSaveEachRec )
			result = query.exec( QString( "SELECT * FROM reportsweight WHERE id > 0 ORDER BY id DESC LIMIT 1;" ) );
		else
			result = query.exec( QString( "SELECT * FROM ReportsWeight WHERE Date_Time_Send >= '%1' AND Date_Time_Send <= '%2';" ).arg( last_dateTime ).arg( now_dateTime ) );

	int size = query.size();
	int num_columns = query.record().count();

	QList< QString > row_data;

	if( size != 0 )
	{
		for( int j = 0; j < size; j++ )
		{
			query.next();

			if( table == MAIN_TABLE )
			{
				// � ������ ���������� 
			}
			else
			{
				row_data.append( query.record().value( "Number_Auto" ).toString() );
				row_data.append( query.record().value( "Sender" ).toString() );
				row_data.append( query.record().value( "Accepter" ).toString() );
				row_data.append( query.record().value( "Payer" ).toString() );
				row_data.append( query.record().value( "Transporter" ).toString() );
				row_data.append( query.record().value( "Goods" ).toString() );
				row_data.append( query.record().value( "Price" ).toString() );
                // row_data.append( query.record().value( "humidity" ).toString() );
				row_data.append( query.record().value( "Brutto" ).toString() );
				row_data.append( query.record().value( "Tara" ).toString() );
				row_data.append( query.record().value( "Netto" ).toString() );
				row_data.append( query.record().value( "Date_Time_Accept" ).asDateTime().toString( "dd.MM.yyyy hh:mm:ss" ) ); // ������ �������/���� �������� ���������� 
				row_data.append( query.record().value( "Name_User_Accept" ).toString() );
				row_data.append( query.record().value( "Date_Time_Send" ).asDateTime().toString( "dd.MM.yyyy hh:mm:ss" ) ); // ������ �������/���� �������� ����������
				row_data.append( query.record().value( "Name_User_Send" ).toString() );
				row_data.append( query.record().value( "Nakladnaya" ).toString() );
				row_data.append( query.record().value( "Num_Prizep" ).toString() );
				row_data.append( query.record().value( "Brutto_Prizep" ).toString() );
				row_data.append( query.record().value( "Tara_Prizep" ).toString() );
				row_data.append( query.record().value( "Netto_Prizep" ).toString() );
				row_data.append( query.record().value( "tara_type" ).toString() );
				row_data.append( query.record().value( "idf" ).toString() );
				
				QString str = row_data.at( 10 );
				QDateTime dateTime = QDateTime::fromString( str, "dd.MM.yyyy hh:mm:ss" );
				str = dateTime.toString( "yyyy-MM-dd hh:mm:ss" );
				
				QString fileName = QString( "%1_%2_%3_�%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ) ).arg( row_data.at( 14 ) ).arg( row_data.at( 0 ) ).arg( j + 1 );
				row_data.append( fileName ); // file_name
			}

			data->append( row_data );
			row_data.clear();
		}
	}
	
	return result;

}

void mysqlProccess::getPhotosbyIdAndSave(QList<int> id_list, QStringList fileNames, bool autosend)
{
	bool result;
	QSqlError error;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	result = query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

	QString fname = fileNames.at(0);
	fname.prepend( autosend ? "photo_tmp_auto/" : "photo_tmp/" );
	QString fname1 = fname;
	QFileInfo fileInfo(fname);
	QString path1(fileInfo.absoluteDir().absolutePath());
				
	QDir dir1( path1 );
	if (! dir1.exists() )
		dir1.mkdir( path1 );
	dir1.setNameFilters(QStringList() << "*.*");
	dir1.setFilter(QDir::Files);
	foreach(QString dirFile, dir1.entryList())
	{
		dir1.remove(dirFile);
	}

	for( int i = 0; i < id_list.size(); i++ )
	{
		result = query.prepare( QString( "SELECT * FROM images WHERE id = %1;" ).arg( id_list.at( i ) ) );
		result = query.exec();
		if( query.next() )
		{
			QByteArray ba1_0, ba1_1, ba2_0, ba2_1, ba3_0, ba3_1, ba4_0, ba4_1;
			QString fn1_0, fn1_1, fn2_0, fn2_1, fn3_0, fn3_1, fn4_0, fn4_1;
			ba1_0 = query.record().value( "photo_1" ).asByteArray();   fn1_0 = "cam1_1";
			ba1_1 = query.record().value( "photo_4" ).asByteArray();   fn1_1 = "cam1_2";
			ba2_0 = query.record().value( "photo_2" ).asByteArray();   fn2_0 = "cam2_1";
			ba2_1 = query.record().value( "photo_5" ).asByteArray();   fn2_1 = "cam2_2";
			ba3_0 = query.record().value( "photo_3" ).asByteArray();   fn3_0 = "cam3_1";
			ba3_1 = query.record().value( "photo_7" ).asByteArray();   fn3_1 = "cam3_2";
			ba4_0 = query.record().value( "photo_6" ).asByteArray();   fn4_0 = "cam4_1";
			ba4_1 = query.record().value( "photo_8" ).asByteArray();   fn4_1 = "cam4_2";
			try
			{	
				QString fname = fileNames.at(i);
				fname.prepend( autosend ? "photo_tmp_auto/" : "photo_tmp/" );
				QString fname1 = fname;
				QApplication::processEvents(QEventLoop::AllEvents, 100);

				QImage img1_0( ba1_0 );
				img1_0.save( fname.replace( ".jpg", fn1_0.prepend("-").append(".jpg"), false) );
				QApplication::processEvents(QEventLoop::AllEvents, 100);
				
				fname = fname1;
				QImage img1_1( ba1_1 );
				img1_1.save( fname.replace( ".jpg", fn1_1.prepend("-").append(".jpg"), false) );
				QApplication::processEvents(QEventLoop::AllEvents, 100);
				
				fname = fname1;
				QImage img2_0( ba2_0 );
				img2_0.save( fname.replace( ".jpg", fn2_0.prepend("-").append(".jpg"), false) );
				QApplication::processEvents(QEventLoop::AllEvents, 100);
				
				fname = fname1;
				QImage img2_1( ba2_1 );
				img2_1.save( fname.replace( ".jpg", fn2_1.prepend("-").append(".jpg"), false) );
				QApplication::processEvents(QEventLoop::AllEvents, 100);
				
				fname = fname1;
				QImage img3_0( ba3_0 );
				img3_0.save( fname.replace( ".jpg", fn3_0.prepend("-").append(".jpg"), false) );
				QApplication::processEvents(QEventLoop::AllEvents, 100);
				
				fname = fname1;
				QImage img3_1( ba3_1 );
				img3_1.save( fname.replace( ".jpg", fn3_1.prepend("-").append(".jpg"), false) );
				QApplication::processEvents(QEventLoop::AllEvents, 100);
				
				fname = fname1;
				QImage img4_0( ba4_0 );
				img4_0.save( fname.replace( ".jpg", fn4_0.prepend("-").append(".jpg"), false) );
				QApplication::processEvents(QEventLoop::AllEvents, 100);
				
				fname = fname1;
				QImage img4_1( ba4_1 );
				img4_1.save( fname.replace( ".jpg", fn4_1.prepend("-").append(".jpg"), false) );
				QApplication::processEvents(QEventLoop::AllEvents, 100);
				
			}
			catch( ... )
			{
			}
		}
		QApplication::processEvents();
	}
}

void mysqlProccess::getLastRecForSend(int &idf, QString &fileName)
{
	bool result;
	QSqlError error;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	result = query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();
	
	result = query.exec( QString( "SELECT id, idf, Number_Auto, Date_Time_Accept, Nakladnaya FROM reportsweight WHERE id > 0 ORDER BY id DESC LIMIT 1;" ) );
	if ( query.next() )
	{
		idf = query.value( 1 ).toInt();
		QString str = query.value( 3 ).toString();
		fileName = QString( "%1_%2_%3.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ) ).arg(  query.value( 4 ).toString() ).arg( query.value( 4 ).toString() );
	}
	else
	{
		idf = 0;
		fileName = "";
	}
}	
// 
int mysqlProccess::getLastNumNakladnaya()
{
    int num_nakl = -1;
    bool result;
	QSqlError error;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	result = query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();
	if( result == false )
    {
	    // �������������� �� ������ ������ � �� 
	}

	int id1 = 0, id2 = 0, num_n1 = 0, num_n2 = 0;

	result = query.exec( QString( "SELECT MAX(ID) FROM AutoReception;" ) );
	if( query.next() )
	{
		id1 = query.value( 0 ).toInt();
	}

	result = query.exec( QString( "SELECT MAX(ID) FROM ReportsWeight;" ) );
	if( query.next() )
	{
		id2 = query.value( 0 ).toInt();
	}

	if( id1 > 0 && id2 > 0 )
	{
		result = query.exec( QString( "SELECT MAX(CAST(Nakladnaya AS UNSIGNED)) FROM AutoReception;" ) );
		if( query.next() )
		{
			num_n1 = query.value( 0 ).toInt();
		}

		result = query.exec( QString( "SELECT MAX(CAST(Nakladnaya AS UNSIGNED)) FROM ReportsWeight;" ) );
		if( query.next() )
		{
			num_n2 = query.value( 0 ).toInt();
		}

		if( num_n1 >= num_n2 )
		{
			num_nakl = num_n1;
		}
		else
			num_nakl = num_n2;
	}
	else
	{
		if( id1 == 0 )
		{
		    result = query.exec( QString( "SELECT MAX(CAST(Nakladnaya AS UNSIGNED)) FROM ReportsWeight;" ) );
			if( query.next() )
			{
				num_nakl = query.value( 0 ).toInt();
			}
		}

		if( id2 == 0 )
		{
		    result = query.exec( QString( "SELECT MAX(CAST(Nakladnaya AS UNSIGNED)) FROM AutoReception;" ) );
			if( query.next() )
			{
				num_nakl = query.value( 0 ).toInt();
			}
		}
	}
	
	return int( num_nakl + 1 );
}

//
QList< QString > mysqlProccess::getNakladnayaRecord( QString date_time, int num_table )
{
    QString logEventDataList;
	bool result;
	QSqlError error;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	if( num_table == 0 )
	{
	    result = query.exec( QString( "SELECT * FROM acceptcards WHERE date_time='%1';" ).arg( date_time ) );
	}
	else
	{
	    result = query.exec( QString( "SELECT * FROM reportcards WHERE date_time_accept='%1';" ).arg( date_time ) );
	}

	error = query.lastError();

	int cnt = query.record().count();
	int i = 0;
	QList< QString > row_data;

	if( num_table == 0 )
	{
		if( query.next() )
		{
			row_data.append( query.record().value( "date_time" ).toDateTime().toString( "dd.MM.yyyy hh:mm:ss" ) );
			
			row_data.append( query.record().value( "weight_1" ).toString() );
			row_data.append( query.record().value( "weight_2" ).toString() );
			row_data.append( query.record().value( "netto" ).toString() );

			row_data.append( query.record().value( "supplier" ).toString() );
			row_data.append( query.record().value( "material" ).toString() );
			row_data.append( query.record().value( "num_auto" ).toString() );
			row_data.append( query.record().value( "fio_driver" ).toString() );
			row_data.append( query.record().value( "num_card_poryadk" ).toString() );
			
			int blocked = query.record().value( "blocked" ).toInt();
			row_data.append( ( blocked == 0 ? QTextCodec::codecForName("Windows-1251")->toUnicode( "��" ) : ( blocked == 1 ? QTextCodec::codecForName("Windows-1251")->toUnicode( "���" ) : QTextCodec::codecForName("Windows-1251")->toUnicode( "" ) ) ) );

			row_data.append( query.record().value( "code" ).toString() );
		}
	}
	else
	{
		if( query.next() )
		{
			row_data.append( query.record().value( "date_time_accept" ).toDateTime().toString( "dd.MM.yyyy hh:mm:ss" ) );
			row_data.append( query.record().value( "date_time_send" ).toDateTime().toString( "dd.MM.yyyy hh:mm:ss" ) );
			
			row_data.append( query.record().value( "weight_1" ).toString() );
			row_data.append( query.record().value( "weight_2" ).toString() );
			row_data.append( query.record().value( "netto" ).toString() );

			row_data.append( query.record().value( "supplier" ).toString() );
			row_data.append( query.record().value( "material" ).toString() );
			row_data.append( query.record().value( "num_auto" ).toString() );
			row_data.append( query.record().value( "fio_driver" ).toString() );
			row_data.append( query.record().value( "num_card_poryadk" ).toString() );

			int blocked = query.record().value( "blocked" ).toInt();
			row_data.append( ( blocked == 0 ? QTextCodec::codecForName("Windows-1251")->toUnicode( "��" ) : ( blocked == 1 ? QTextCodec::codecForName("Windows-1251")->toUnicode( "���" ) : QTextCodec::codecForName("Windows-1251")->toUnicode( "" ) ) ) );
            row_data.append( query.record().value( "code" ).toString() );
		}
	}

	return row_data;
}


// 
bool mysqlProccess::getTaraAuto( QList< QString > headers_names, QTableView *tableWidget )
{
	bool result;
	QSqlError error;
	QVector< QString > row_data;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	result = query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();
	if( result == false )
    {
	    // �������������� �� ������ ������ � �� 
	}

    result = query.exec( QString( "SELECT * FROM taraAuto;" ) );

	int r = query.size();
	int c = query.record().count() - 1;

	// ������ ������ ������ ��� ������������� � � QTableView
	if( taraDictionaryModel_obj == NULL )
	{
		taraDictionaryModel_obj = new taraDictionaryModel( r, c, headers_names );
    }
	else
	{
		delete taraDictionaryModel_obj;
		taraDictionaryModel_obj = NULL;
		taraDictionaryModel_obj = new taraDictionaryModel( r, c, headers_names );
	}

	while( query.next() )
	{
		row_data.append( query.record().value( "date_time" ).asDateTime().toString( "dd.MM.yyyy hh:mm:ss" ) ); // ������ �������/���� ������ ���� ���������� 
		row_data.append( query.record().value( "number_auto" ).toString() );
		row_data.append( query.record().value( "mark_auto" ).toString() );
		row_data.append( query.record().value( "weight_tara" ).toString() );
		
		taraDictionaryModel_obj->update( &row_data );
		row_data.clear();
	}

	tableWidget->setModel( taraDictionaryModel_obj );
	tableWidget->setEditTriggers( QAbstractItemView::NoEditTriggers ); // ��������� ������������� ������
	tableWidget->setAlternatingRowColors( true );

	for( int i = 0; i < headers_names.length(); i++ )
	{
        tableWidget->setColumnWidth( i, 180 );
	}

	tableWidget->setUpdatesEnabled( true );
	tableWidget->setEnabled( true );
	//tableWidget->setVisible( true );
	tableWidget->blockSignals( false );
	tableWidget->setSortingEnabled( true );

	return result;
}

// ���������� ���� 
bool mysqlProccess::insertTaraAuto( QString number_auto, QString mark_auto, float weight_tara )
{
	bool result;
	QSqlError error;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	result = query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();
	if( result == false )
    {
	    // �������������� �� ������ ������ � �� 
	}

	result = query.exec( QString( "SELECT number_auto FROM taraAuto WHERE number_auto = '%1';" )
		.arg( number_auto ) );

	bool isExists = false;
	if( query.next() )
	{
		isExists = true;
	}

	if( !isExists )
	{
		result = query.prepare( QString( "INSERT INTO taraAuto ( date_time, number_auto, mark_auto, weight_tara ) "
			" VALUES(:date_time, :number_auto, :mark_auto, :weight_tara);" ) );
		query.bindValue( ":date_time", QDateTime::currentDateTime().toString( "yyyy-MM-dd hh:mm:ss" ) );
		query.bindValue( ":number_auto", number_auto );
		query.bindValue( ":mark_auto", mark_auto );
		query.bindValue( ":weight_tara", weight_tara );
		result = query.exec();
	}
	else
	{
		result = false;
	}

	return result;
}

bool mysqlProccess::updateTaraAuto( QString dateTime, QString number_auto, QString mark_auto, float weight_tara )
{
	bool result;
	QSqlError error;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	result = query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();
	if( result == false )
    {
	    // �������������� �� ������ ������ � �� 
	}

	result = query.exec( QString( "SELECT number_auto FROM taraAuto WHERE number_auto = '%1';" )
		.arg( number_auto ) );

	bool isExists = false;
	if( query.next() )
	{
		isExists = true;
	}

	// if( !isExists )
	// {
		result = query.prepare( QString( "UPDATE taraAuto SET date_time=:date_time, number_auto=:number_auto, mark_auto=:mark_auto, weight_tara=:weight_tara WHERE date_time='%1';" )
			.arg( dateTime ) );
		query.bindValue( ":date_time", QDateTime::currentDateTime().toString( "yyyy-MM-dd hh:mm:ss" ) );
		query.bindValue( ":number_auto", number_auto );
		query.bindValue( ":mark_auto", mark_auto );
		query.bindValue( ":weight_tara", weight_tara );
		result = query.exec();
	/*}
	else
	{
		result = false;
	}*/

	return result;
}

bool mysqlProccess::deleteTaraAuto( QString dateTime )
{
	bool result;
	QSqlError error;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	result = query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();
	if( result == false )
    {
	    // �������������� �� ������ ������ � �� 
	}

    result = query.exec( QString( "DELETE FROM taraAuto WHERE date_time = '%1';" )
		.arg( dateTime ) );
	
	return result;
}

// 
double mysqlProccess::getTaraAuto( QString numAuto )
{
	double resultWeight = -1;
	bool result;
	QSqlError error;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	result = query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();
	if( result == false )
    {
	    // �������������� �� ������ ������ � �� 
	}

    result = query.exec( QString( "SELECT weight_tara FROM taraAuto WHERE number_auto = '%1';" )// AND date_time >= '%2 00:00:00';" )
		.arg( numAuto ));
		//.arg(QDateTime::currentDateTime().toString("yyyy-MM-dd") ));

	if( query.next() )
	{
		resultWeight = query.record().value( "weight_tara" ).toDouble();
	}

	return resultWeight;
}

void mysqlProccess::writeSensors_Slot( QVector<int> aw, QVector<int> ac )
{	
	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	query.exec( QString( "USE %1;" ).arg( dataBaseName ) );

    query.prepare( "INSERT INTO sensorsdataforservice ( date_time, "
		"sensor1, sensor2, sensor3, sensor4, sensor5, sensor6, sensor7, sensor8, code1, code2, code3, code4, code5, code6, code7, code8 ) "
		"VALUES ( :date_time, "
		":sensor1, :sensor2, :sensor3, :sensor4, :sensor5, :sensor6, :sensor7, :sensor8, :code1, :code2, :code3, :code4, :code5, :code6, :code7, :code8 )" 
		);

	query.bindValue( ":Date_Time", QDateTime::currentDateTime().toString( "yyyy-MM-dd hh:mm:ss" ) );

	int max_aw = aw.count();
	int max_ac = ac.count();
	for( int i = 0; i < 8; i++)
	{
		if( i < max_aw )	query.bindValue( QString (":sensor%1" ).arg(i+1), aw[i] );
		else				query.bindValue( QString (":sensor%1" ).arg(i+1), 0 );

		if( i < max_ac )	query.bindValue( QString (":code%1" ).arg(i+1), ac[i] );
		else				query.bindValue( QString (":code%1" ).arg(i+1), 0 );
	}	

	query.exec();
}

void mysqlProccess::stable_Slot()
{
	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );
	// ����������� ������ ��� ������������ ����������, �� ������� MySQL ������ 8 ����� �������� ����������
	query.exec( QString( "USE %1; SELECT * FROM users;" ).arg( dataBaseName ) );
}

QVector<int> mysqlProccess::getRealBruttoAndTara( QString accept_time )
{
	QVector<int> v;
	v.append( 0 );
	v.append( 0 );

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	query.exec( QString( "USE %1;" ).arg( dataBaseName ) );

	query.exec( QString( "SELECT idf FROM reportsweight WHERE Date_Time_Accept = '%1';" )
		.arg( accept_time ) );
	int idf = 0;
	if( query.next() )
	{
		idf = query.record().value( 0 ).toInt();
	}

    query.exec( QString( "SELECT real_weight_brutto, real_weight_tara FROM sor WHERE idf = %1;" )
		.arg( idf ) );
	if( query.next() )
	{
		v[0] = query.record().value( 0 ).toInt();
		v[1] = query.record().value( 1 ).toInt();
	}

	return v;
}

// filters: time from, time to
QSqlTableModel* mysqlProccess::getHiddenWeighingTableModel( QStringList headers, QStringList filters )
{
    if(filters.count() < 2)
    {
        Q_ASSERT(filters.count() < 2);
    }

    QString params = "";
	// �����
    params.append( QString( " Date_Time >= '%1' AND Date_Time <= '%2' " )
        .arg( filters.at( 0 ) )
        .arg( filters.at( 1 ) ) );


	QSqlTableModel* model = new QSqlTableModel( this, *getQSqlInstance() );
    model->setTable( "totalReports" );
    model->setFilter( params );
    model->select();
	model->removeColumn(5);
	model->removeColumn(4);
    for (int col = 0; col < headers.count(); ++col)
    {
        model->setHeaderData(col, Qt::Horizontal, headers[col]);
    }

	return model;
}

int mysqlProccess::addHiddenWeighing( int weight )
{
	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	QString dt = QDateTime::currentDateTime().toString( "yyyy-MM-dd hh:mm:ss" );

	query.prepare( QString( "INSERT INTO images( Date_Time ) VALUES( :Date_Time );" ) );
	query.bindValue( ":Date_Time", dt );
	query.exec();
			
	query.exec( QString( "SELECT LAST_INSERT_ID();" ) );

	int id_img = 0;

	if( query.next() )
	{
		id_img = query.value( 0 ).toInt();
	}

	query.prepare( QString( "INSERT INTO totalReports "
		"( Date_Time, weight_moment, weight_max, registered, IDF ) "
		" VALUES( :Date_Time, :weight_moment, :weight_max, :registered, :IDF );" ) );
	query.bindValue( ":Date_Time", dt );
	query.bindValue( ":weight_moment", weight );
	query.bindValue( ":weight_max", 0 );
	query.bindValue( ":registered", 0 );
	query.bindValue( ":IDF", id_img );
	query.exec();	

	return id_img;
}

void mysqlProccess::updHiddenWeighing( int max_weight )
{
	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	query.exec( "SELECT MAX(id) FROM totalReports;" );

	int id = 0;
	if( query.next() )
	{
		id = query.value( 0 ).toInt();
	}

	query.prepare( QString( "UPDATE totalReports SET weight_max=:weight_max WHERE id = %1;" )
		.arg( id ) );
	query.bindValue( ":weight_max", max_weight );
	query.exec();
}

void mysqlProccess::updHiddenWeighing( bool registered )
{
	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	query.exec( "SELECT MAX(id) FROM totalReports;" );

	int id = 0;
	if( query.next() )
	{
		id = query.value( 0 ).toInt();
	}

	query.prepare( QString( "UPDATE totalReports SET registered=:registered WHERE id = %1;" )
		.arg( id ) );
	query.bindValue( ":registered", registered ? 1 : 0 );
	query.exec();
}

//�������� ����� ������
bool mysqlProccess::read_trial()
{
	bool result;
	QSqlError error;
	int idf = 1;
	int data_of_work = 1;
	int data_tmp_1 = 1;
	int checker = 1;
	QString date_time;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	result = query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();
	if( result == false )
    {
	    qDebug() << "������ ��";// �������������� �� ������ ������ � �� 
		return false;
	}
	
	query.exec( QString( "SELECT * FROM trial_work WHERE id = %1;" ).arg( idf ) );
	query.next();
	data_of_work = query.value( 2 ).toInt();
	data_tmp_1 = query.value( 3 ).toInt();
	checker = query.value( 5 ).toInt();

	if( data_of_work == 123 || data_tmp_1 == 65 || checker == 87 )
	{
		result = query.prepare( QString( "UPDATE trial_work SET data_of_work=:data_of_work, data_tmp_1=:data_tmp_1,"
			"data_tmp_2=:data_tmp_2, checker=:checker WHERE id='%1';" ).arg( idf ) );
		query.bindValue( ":data_of_work", 0);
		query.bindValue( ":data_tmp_1", 0);
		query.bindValue( ":data_tmp_2", 0);
		query.bindValue( ":checker", 0);
		query.exec();
		return true;
	}
	else if( data_of_work >= 80 * 3600 ) //80 ����� � ��������
	{
		return false;
	}
	return true;
}
//���������� 3���� � �������� �� ������ ������
bool mysqlProccess::check_trial()
{
	bool result;
	QSqlError error;
	int idf = 1;
	int data_of_work = 1;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	result = query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();
	if( result == false )
    {
	    qDebug() << "������ ��";// �������������� �� ������ ������ � �� 
		return false;
	}
	
	query.exec( QString( "SELECT * FROM trial_work WHERE id = %1;" ).arg( idf ) );
	query.next();
	data_of_work = query.value( 2 ).toInt();
	
	if( data_of_work >= 80 * 3600 ) //80 ����� � ��������
	{
		return false;	
	}
	else  
	{
		result = query.prepare( QString( "UPDATE trial_work SET data_of_work=:data_of_work WHERE id='%1';" ).arg( idf ) );
		query.bindValue( ":data_of_work", data_of_work + 10800);
		query.exec();
		return true;
	}
}
//�������� 3 � � ���������� ����
bool mysqlProccess::add_trial()
{
	bool result;
	QSqlError error;
	int idf = 1;
	int data_tmp_1 = 1;

	QString date_time;
	
	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	result = query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();
	if( result == false )
    {
	    qDebug() << "������ ��";// �������������� �� ������ ������ � �� 
		return false;
	}
	
	query.exec( QString( "SELECT * FROM trial_work WHERE id = %1;" ).arg( idf ) );
	query.next();
	data_tmp_1 = query.value( 3 ).toInt();
	
	if(  data_tmp_1 >= 10800 ) //3����
	{
		result = query.prepare( QString( "UPDATE trial_work SET data_tmp_1=:data_tmp_1 WHERE id='%1';" ).arg( idf ) );
		query.bindValue( "data_tmp_1", 0 );
		query.exec();
		return false;
	}
	else
	{
		result = query.prepare( QString( "UPDATE trial_work SET data_tmp_1=:data_tmp_1 WHERE id='%1';" ).arg( idf ) );
		query.bindValue( "data_tmp_1", data_tmp_1 + 3600 );
		query.exec();
		return true;	
	}
}
//�������� 1 � � ���������� 15���
bool mysqlProccess::timer_trial()
{
	bool result;
	QSqlError error;
	int idf = 1;
	int data_tmp_2 = 1;

	QString date_time;
	
	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	result = query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();
	if( result == false )
    {
	    qDebug() << "������ ��";// �������������� �� ������ ������ � �� 
		return false;
	}
	
	query.exec( QString( "SELECT data_tmp_2 FROM trial_work WHERE id = %1;" ).arg( idf ) );
	query.next();
	data_tmp_2 = query.value( 0 ).toInt();

	if(  data_tmp_2 >= 3600  ) //1���
	{
		result = query.prepare( QString( "UPDATE trial_work SET data_tmp_2 =:data_tmp_2 WHERE id='%1';" ).arg( idf ) );
		query.bindValue( ":data_tmp_2", 0 );
		result = query.exec();
		return false;
	}
	else
	{
		result = query.prepare( QString( "UPDATE trial_work SET data_tmp_2 =:data_tmp_2 WHERE id='%1';" ).arg( idf ) );
		query.bindValue( ":data_tmp_2", data_tmp_2 + 900 );
		result = query.exec();
		return true;	
	}
}

//
QString mysqlProccess::set_time_leable()
{
	bool result;
	QSqlError error;
	int idf = 1;
	QString date_time;

/*	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}
	*/
	QSqlQuery query( *getQSqlInstance() );

	result = query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();
	if( result == false )
    {
	    qDebug() << "������ ��";// �������������� �� ������ ������ � �� 
		return false;
	}
	
	query.exec( QString( "SELECT data_start FROM trial_work WHERE id = %1;" ).arg( idf ) );
	query.next();
	date_time = query.value( 0 ).asDateTime().toString( "dd.MM.yyyy hh:mm:ss" );
	return date_time;
}

// 
bool mysqlProccess::saveToRegisterTable( QString code, QString supplier, QString material, QString num_auto, QString num_prizep, QString fio_driver, QString num_card_poryadk, bool auto_tara )
{
	bool result = false;
	QSqlError error;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	result = query.prepare( QString( "INSERT INTO registration ( date_time, code, supplier, material, num_auto, num_prizep, fio_driver, num_card_poryadk, auto_tara ) "
		"VALUES ( :date_time, :code, :supplier, :material, :num_auto, :num_prizep, :fio_driver, :num_card_poryadk, :auto_tara );" ) );

	query.bindValue( ":date_time", QDateTime::currentDateTime().toString( "yyyy-MM-dd hh:mm:ss" ) );
	query.bindValue( ":code", code );
	query.bindValue( ":supplier", supplier );
	query.bindValue( ":material", material );
	query.bindValue( ":num_auto", num_auto );
	query.bindValue( ":num_prizep", num_prizep );
	query.bindValue( ":fio_driver", fio_driver );
	query.bindValue( ":num_card_poryadk", num_card_poryadk );
	query.bindValue( ":auto_tara", auto_tara ? 1 : 0 );

	result = query.exec();
	error = query.lastError();

	QString error_text = error.text();
	qDebug()<<error_text;
	return result;
}

bool mysqlProccess::updateRegisterTable( QString date_time, QString code, QString supplier, QString material, QString num_auto, QString num_prizep, QString fio_driver, QString num_card_poryadk, bool auto_tara )
{
	bool result = false;
	QSqlError error;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	result = query.prepare( QString( "UPDATE registration SET date_time=:date_time, code=:code, supplier=:supplier, material=:material, "
		"num_auto=:num_auto, num_prizep=:num_prizep, fio_driver=:fio_driver, num_card_poryadk=:num_card_poryadk, auto_tara=:auto_tara WHERE date_time = '%1' ;" )
		.arg( date_time ) );

	query.bindValue( ":date_time", QDateTime::currentDateTime().toString( "yyyy-MM-dd hh:mm:ss" ) );
	query.bindValue( ":code", code );
	query.bindValue( ":supplier", supplier );
	query.bindValue( ":material", material );
	query.bindValue( ":num_auto", num_auto );
	query.bindValue( ":num_prizep", num_prizep );
	query.bindValue( ":fio_driver", fio_driver );
	query.bindValue( ":num_card_poryadk", num_card_poryadk );
	query.bindValue( ":auto_tara", auto_tara ? 1 : 0 );

	result = query.exec();
	error = query.lastError();

	QString error_text = error.text();

	return result;
}

// �������� ������������������ ������ �� ������������ 
bool mysqlProccess::getRegistrationData( QList< QString > headers_names, QTableView *tableWidget, QList< QString > filters )
{
    bool result;
	QSqlError error;
	int num_records, percent;
	QVector< QString > data;
	QString params;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );

	//if( filters.count() >= 3 )  //  NUM_FILTERS_PARAMS )
	//{
	//	if( !filters.at( 0 ).isEmpty() )
	//		params.append( QString( " date_time >= '%1' " ).arg( filters.at( 0 ) ) );

	//	if( !filters.at( 1 ).isEmpty() )
	//		params.append( QString( " AND date_time <= '%1' " ).arg( filters.at( 1 ) ) );

	//	if( !filters.at( 2 ).isEmpty() )
	//		params.append( QString( " AND numScales=%1 " ).arg( filters.at( 2 ) ) );
	//}

	result = query.exec( QString( "SELECT * FROM registration; " ) ); //  WHERE %1 .arg( params ) );
    num_records = query.size();
    percent = 0;

	tableWidget->setUpdatesEnabled( false );
	tableWidget->setEnabled( false );
	tableWidget->setVisible( false );
	tableWidget->blockSignals( true );
	tableWidget->setSortingEnabled( false );

	int r = query.size();
	int c = query.record().count() - 1; //��� id

	if( r < 0 || c < 0 )
	{
		r = 0;
		c = headers_names.length();
	}

	// ������ ������ ������ ��� ������������� � � QTableView
	if( registration_Obj == NULL )
	{
		registration_Obj = new RegistrationModel( r, c, headers_names );
    }
	else
	{
		registration_Obj->clear();
		registration_Obj->addParams( r, c, headers_names );
	}

	while( query.next() )
	{
		data.append( query.record().value( "date_time" ).toDateTime().toString( "dd.MM.yyyy hh:mm:ss" ) );
		data.append( query.record().value( "code" ).toString() );
		data.append( query.record().value( "supplier" ).toString() );
		data.append( query.record().value( "material" ).toString() );
		data.append( query.record().value( "num_auto" ).toString() );
		data.append( query.record().value( "num_prizep" ).toString() );
		data.append( query.record().value( "fio_driver" ).toString() );
		data.append( query.record().value( "num_card_poryadk" ).toString() );
		data.append( query.record().value( "blocked" ).toString() );
		data.append( query.record().value( "auto_tara" ).toString() );

		registration_Obj->update( &data );
	    data.clear();
	}
	tableWidget->setModel( registration_Obj );
	tableWidget->setUpdatesEnabled( false );

	tableWidget->setStyleSheet( "font-weight: bold; font-size: 12px;" ); // ???

	for( int i = 0; i < headers_names.length(); i++ )
	{
		tableWidget->setColumnWidth( i, 180 );
	}
	tableWidget->setColumnHidden(8, true);
	tableWidget->setEditTriggers( QAbstractItemView::NoEditTriggers ); // ��������� ������������� ������
	tableWidget->setAlternatingRowColors( true );

	tableWidget->setUpdatesEnabled( true );
	tableWidget->setEnabled( true );
	tableWidget->setVisible( true );
	tableWidget->blockSignals( false );
	tableWidget->setSortingEnabled( true );

	return result;
}

// 
// get data for filter controls.
// 0 - suppliersm
// 1 - num_card
// 2 - ���
// 3 - ���� 
QList< QString > mysqlProccess::getRegisteredDataToFilterControls( int type )
{
	bool result;
	QSqlError error;

	QList< QString > data;

	if( !isConnected() )
	{
		for( int i = 0; i < NUM_ATTEMPTS_MYSQL_CONN;  i++ )
		{
			if( connect() )
		        break;
		}
	}

	QSqlQuery query( *getQSqlInstance() );
	switch( type )
	{
	    case 0:
		{
	        result = query.exec( QString( "SELECT DISTINCT supplier FROM registration; " ) );
		} break;
		case 1:
		{
	        result = query.exec( QString( "SELECT DISTINCT num_card_poryadk FROM registration; " ) );
		} break;
		case 2:
		{
	        result = query.exec( QString( "SELECT DISTINCT fio_driver FROM registration; " ) );
		} break;
		case 3:
		{
	        result = query.exec( QString( "SELECT DISTINCT material FROM registration; " ) );
		} break;
	}

	int r = query.size();
	if( r > 0 )
	{
		int i = 0;
		while( query.next() )
		{
			data.append( query.value( 0 ).toString() );
		}
	}
	else
	{
		data.append( "" );
	}

	return data;
}

void mysqlProccess::showPhotoReports()
{
	emit showPhotoReports_Signal();
}

void mysqlProccess::savePhotoReports()
{
	emit savePhotoReports_Signal();
}

void mysqlProccess::CopyOnServer(){
		QSqlError error;
	    bool result;
		QSqlQuery query( *getQSqlInstance() );
		QSqlQuery query_RDB( *connectDBR() );
		int size_rc, size_img;
		QList<QString> row_data;
		QVector<QList<QString>> *data_rc = new QVector< QList< QString > >();
		QVector<QList<QString>> *data_img = new QVector< QList< QString > >();

		//����������� ������� ����
		result = query_RDB.exec( QString( "SELECT MAX(Date_Time) FROM images ;"));
	    query_RDB.next();
	    qDebug()<<query_RDB.value(0);
	    QString max_dt_img_server = ( QString( query_RDB.value( 0 ).toDateTime().toString( "yyyy-MM-dd hh:mm:ss" ) ) );
	    qDebug()<<max_dt_img_server;
	    QDateTime dt_img_server = QDateTime::fromString( max_dt_img_server, "yyyy-MM-dd hh:mm:ss" );

		query.exec( QString( "USE %1;" ).arg( dataBaseName ) );
		result = query.exec( QString( "SELECT MAX(Date_Time) FROM images ;"));
	    query.next();
	    qDebug()<<query.value(0);
	    QString max_dt_img_client = ( QString( query.value( 0 ).toDateTime().toString( "yyyy-MM-dd hh:mm:ss" ) ) );
	    qDebug()<<max_dt_img_client;
	    QDateTime dt_img_client = QDateTime::fromString( max_dt_img_client, "yyyy-MM-dd hh:mm:ss" );
	    qDebug()<<dt_img_client;

		   do
		{
	        result = query.exec( QString( "SELECT * FROM images WHERE Date_Time> '%1' LIMIT 1;").arg(max_dt_img_server));
			error = query.lastError();
	        qDebug()<<error;
	        size_img = query.size();
	        qDebug()<<"size"<<size_img;
			
	        if( size_img != 0 )
            {
               for( int j = 0; j < size_img; j++ )
               {
                 query.next();
			     row_data.append( query.record().value( "ID" ).toString() );
			     qDebug()<<query.record().value( "ID" ).toString();
				 qDebug()<<row_data;
                 row_data.append( query.record().value( "Date_Time" ).toDateTime().toString("yyyy-MM-dd hh:mm:ss")); //.toString( "dd.MM.yyyy hh:mm:ss" ) ); // ������ �������/���� �
				 qDebug()<<row_data;

	             qDebug()<<row_data.value(1);
           	     data_img->append( row_data );
			     row_data.clear();
               }
            }

	        for( int j = 0; j < size_img; j++ )
              {
	            qDebug()<<data_img->at(j);
		        row_data.append( data_img->at(j));
		        query_RDB.prepare( QString("INSERT INTO images ( ID, Date_Time )"
                                       "VALUES( :ID, :Date_Time );" ) );
                query_RDB.bindValue( ":ID", row_data.value(0) );
                query_RDB.bindValue( ":Date_Time", row_data.value(1) );
                result = query_RDB.exec();
                error = query_RDB.lastError();
				qDebug()<<error;
				row_data.clear();
	          }
		data_img->clear();

		result = query_RDB.exec( QString( "SELECT MAX(Date_Time) FROM images ;"));
	    query_RDB.next();
	    qDebug()<<query_RDB.value(0);
	    max_dt_img_server = ( QString( query_RDB.value( 0 ).toDateTime().toString( "yyyy-MM-dd hh:mm:ss" ) ) );
	    qDebug()<<max_dt_img_server;
	    dt_img_server = QDateTime::fromString( max_dt_img_server, "yyyy-MM-dd hh:mm:ss" );

	    QApplication::processEvents(QEventLoop::AllEvents, 1000);
		}
		while(dt_img_server < dt_img_client);

		//����������� ������� ����������� �� ������ 
		result = query_RDB.exec( QString( "SELECT MAX(date_time_accept) FROM reportcards ;"));
	    query_RDB.next();
	    qDebug()<<query_RDB.value(0);
	    QString max_dt_rc_server = ( QString( query_RDB.value( 0 ).toDateTime().toString( "yyyy-MM-dd hh:mm:ss" ) ) );
	    qDebug()<<max_dt_rc_server;
	    QDateTime dt_rc_server = QDateTime::fromString( max_dt_rc_server, "yyyy-MM-dd hh:mm:ss" );

		query.exec( QString( "USE %1;" ).arg( dataBaseName ) );
		result = query.exec( QString( "SELECT MAX(date_time_accept) FROM reportcards ;"));
	    query.next();
	    qDebug()<<query.value(0);
	    QString max_dt_rc_client = ( QString( query.value( 0 ).toDateTime().toString( "yyyy-MM-dd hh:mm:ss" ) ) );
	    qDebug()<<max_dt_rc_client;
	    QDateTime dt_rc_client = QDateTime::fromString( max_dt_rc_client, "yyyy-MM-dd hh:mm:ss" );
	    qDebug()<<dt_rc_client;
      do
	   {    
	    qDebug()<<max_dt_rc_server;
		query.exec( QString( "USE %1;" ).arg( dataBaseName ) );
		result = query.exec( QString( "SELECT * FROM reportcards WHERE date_time_accept>'%1' LIMIT 10;").arg(max_dt_rc_server));
	    error = query.lastError();
        qDebug()<<error;
	    size_rc = query.size();
	    qDebug()<<"size"<<size_rc;

        if( size_rc != 0 )
          {
           for( int j = 0; j < size_rc; j++ )
           {
            query.next();
			row_data.append( query.record().value( "id" ).toString() );                                                  qDebug()<< query.record().value( "id" ).toString();
			row_data.append( query.record().value( "date_time_accept" ).toDateTime().toString("yyyy-MM-dd hh:mm:ss") );  qDebug()<< query.record().value( "date_time_accept" ).toString();
			row_data.append( query.record().value( "date_time_send" ).toDateTime().toString("yyyy-MM-dd hh:mm:ss") );    qDebug()<< query.record().value( "date_time_send" ).toString();
			//row_data.append( query.record().value( "numScales" ).toString() );
            row_data.append( query.record().value( "code" ).toString() );
            row_data.append( query.record().value( "weight_1" ).toString() );
            row_data.append( query.record().value( "weight_2" ).toString() );
            row_data.append( query.record().value( "netto" ).toString() );
            row_data.append( query.record().value( "supplier" ).toString() );
			row_data.append( query.record().value( "material" ).toString() );
            row_data.append( query.record().value( "num_auto" ).toString() );
            row_data.append( query.record().value( "num_prizep" ).toString() );
            row_data.append( query.record().value( "fio_driver" ).toString() );
            row_data.append( query.record().value( "num_card_poryadk" ).toString() );
            row_data.append( query.record().value( "blocked" ).toString() );
            row_data.append( query.record().value( "IDF" ).toString() );
           
			data_rc->append( row_data );
			qDebug()<<data_rc->at(j);
			row_data.clear();
           }    
          }

	      // ��������� ������ ��� ������ ��������� ��
          for( int j = 0; j < size_rc; j++ )
          {
            query_RDB.exec( QString( "SELECT MAX(id) FROM reportcards;" ) );
	        query_RDB.next();
            int max_id_rc = query_RDB.value(0).toInt();
			QString max_id = QString( "%1" ).arg(max_id_rc + 1);
           qDebug()<<max_id;
			//row_data.append( query_RDB.record().value( "id" ).toString() );

           qDebug()<<data_rc->at(j);
		   row_data.append( data_rc->at(j));

	       query_RDB.prepare( QString("INSERT INTO reportcards ( id, date_time_accept, date_time_send, "
		   "code, weight_1, weight_2, netto, supplier, material, num_auto, num_prizep, fio_driver, num_card_poryadk, blocked, idf ) "
		   "VALUES ( :id, :date_time_accept, :date_time_send, "
		   ":code, :weight_1, :weight_2, :netto, :supplier, :material, :num_auto, :num_prizep, :fio_driver, :num_card_poryadk, :blocked, :idf );"));
           query_RDB.bindValue( ":id", row_data.value(0).toInt() ); 
	       query_RDB.bindValue(":date_time_accept", row_data.value(1));
	       query_RDB.bindValue(":date_time_send", row_data.value(2));
	       query_RDB.bindValue(":code", row_data.value(3));
	       query_RDB.bindValue(":weight_1", row_data.value(4).toDouble());
	       query_RDB.bindValue(":weight_2", row_data.value(5).toDouble());
	       query_RDB.bindValue(":netto", row_data.value(6).toDouble());
	       query_RDB.bindValue( ":supplier", row_data.value(7) );
	       query_RDB.bindValue( ":material", row_data.value(8) );
	       query_RDB.bindValue( ":num_auto", row_data.value(9) );
	       query_RDB.bindValue( ":num_prizep", row_data.value(10) );
	       query_RDB.bindValue( ":fio_driver", row_data.value(11) );
	       query_RDB.bindValue( ":num_card_poryadk", row_data.value(12) );
	       query_RDB.bindValue( ":blocked", row_data.value(13).toInt() );
	       query_RDB.bindValue( ":idf", row_data.value(14).toInt() );       qDebug()<< row_data.value(14).toInt();
	       result = query_RDB.exec();
	       error = query_RDB.lastError();
	       qDebug()<<error;
           row_data.clear();
		  }
          data_rc->clear();

		  result = query_RDB.exec( QString( "SELECT MAX(date_time_accept) FROM reportcards ;"));
	      query_RDB.next();
	      qDebug()<<query_RDB.value(0);
	      max_dt_rc_server = ( QString( query_RDB.value( 0 ).toDateTime().toString( "yyyy-MM-dd hh:mm:ss" ) ) );
	      qDebug()<<max_dt_rc_server;
	      dt_rc_server = QDateTime::fromString( max_dt_rc_server, "yyyy-MM-dd hh:mm:ss" );
		  QApplication::processEvents(QEventLoop::AllEvents, 1000);
       }
       while(dt_rc_server < dt_rc_client);
}
