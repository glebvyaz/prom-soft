#include "StdAfx.h"
#include "MailSend.h"
#include "DB_Driver.h"
#include "Crypt.h"
#include "reportFormClass.h"
#include <QSettings>


//extern  SmtpClient *smtpClient;
//extern  mysqlProccess *mysqlProccessObject;
extern  reportFormClass *reportFormObject;

EmailAddress* MailSend::stringToEmail(const QString &str)
{
    int p1 = str.indexOf("<");
    int p2 = str.indexOf(">");

    if (p1 == -1)
    {
        // no name, only email address
        return new EmailAddress(str);
    }
    else
    {
        return new EmailAddress(str.mid(p1 + 1, p2 - p1 - 1), str.left(p1));
    }

}

bool MailSend::SendMail( QString file, bool showStatus1 )
{
	QSettings settings_file( "settings.ini", QSettings::IniFormat );
	settings_file.setIniCodec( "Windows-1251" );
	bool flag_autosend = settings_file.value( "mail/enable_send_report" ).toInt() ? true : false;
	bool flag_send_photos = settings_file.value( "mail/enable_send_photos" ).toInt() ? true : false;
	if( file.size() > 0 )
	{		
		QString host = settings_file.value( "mail/host" ).toString();
		int port = settings_file.value( "mail/port" ).toInt();
		bool ssl = settings_file.value( "mail/ssl" ).toBool();
		bool auth = true;
		QString user = settings_file.value( "mail/login" ).toString();
		QString password = settings_file.value( "mail/password" ).toString();
		if(password.startsWith("#"))
		{
			password = Crypt::decrypt(password);
		}
		QString nameSender = ""; //settings_file.value( "mail/mailToSend" ).toString();
		QString subject = m_subject; //settings_file.value( "mail/report_data", m_subject ).toString();
		QString htmlstr = m_subject + " " + //settings_file.value( "mail/report_data", m_subject ).toString() + " " + 
			QString( "%1" ).arg( QDateTime::currentDateTime().toString( "dd.MM.yyyy hh:mm:ss" ) );

		qDebug() << "user " << user << "\r\npassword = " <<  password;
		qDebug() << "\r\nnameSender " << nameSender << "\r\nsubject = " <<  subject;
		qDebug() << "\r\nhtmlstr " << htmlstr << "\r\n";

		EmailAddress *sender = stringToEmail( nameSender + "<" + user + ">" );

		QStringList rcptStringList;
		rcptStringList.append( settings_file.value( "mail/mailToSend" ).toString() );
		if( settings_file.value( "mail/check_Adr_1" ).toBool() )
		{
			rcptStringList.append( settings_file.value( "mail/enterAdress_1" ).toString() );
		}
		if( settings_file.value( "mail/check_Adr_2" ).toBool() )
		{
			rcptStringList.append( settings_file.value( "mail/enterAdress_2" ).toString() );
		}
		if( settings_file.value( "mail/check_Adr_3" ).toBool() )
		{
			rcptStringList.append( settings_file.value( "mail/enterAdress_3" ).toString() );
		}
		qDebug() <<"rcptStringList"<<rcptStringList;
		SmtpClient smtp (host, port, ssl ? SmtpClient::SslConnection : SmtpClient::TcpConnection);

		MimeMessage message;

		message.setSender(sender);
		message.setSubject(subject);

		for (int i = 0; i < rcptStringList.size(); ++i)
			message.addRecipient(stringToEmail(rcptStringList.at(i)));

		MimeHtml content;
		content.setHtml(htmlstr);

		message.addPart(&content);
		message.addPart(new MimeAttachment(new QFile(file)));

		if( reportFormObject->checkBoxPhotoSaveAll->isChecked() && m_autosend == false )
		{
			QFileInfo fileInfo(file);
			QString path1(fileInfo.absoluteDir().absolutePath().replace("/hourly", "") + "/photo_tmp/");
			QDir dir1( path1 );
			if (! dir1.exists() )
				dir1.mkdir( path1 );
			dir1.setFilter(QDir::Files | QDir::NoSymLinks);
			dir1.setSorting(QDir::Name | QDir::Reversed);

			QFileInfoList list1 = dir1.entryInfoList();

			for (int i = 0; i < list1.size(); ++i) 
			{
				QFileInfo fileInfo = list1.at(i);
				message.addPart(new MimeAttachment(new QFile(fileInfo.absFilePath())));
			}
		}

		else if( flag_autosend && flag_send_photos && m_autosend == true )
		{
			QFileInfo fileInfo(file);
			QString path1(fileInfo.absoluteDir().absolutePath().replace("/hourly", "") + "/photo_tmp_auto/");
			QDir dir1( path1 );
			if (! dir1.exists() )
				dir1.mkdir( path1 );
			
			//mysqlProccessObject->getPhotosbyIdAndSave();

			dir1.setFilter(QDir::Files | QDir::NoSymLinks);
			dir1.setSorting(QDir::Name | QDir::Reversed);

			QFileInfoList list1 = dir1.entryInfoList();

			for (int i = 0; i < list1.size(); ++i) 
			{
				QFileInfo fileInfo = list1.at(i);
				message.addPart(new MimeAttachment(new QFile(fileInfo.absFilePath())));
			}
		}

		/*QFile file1(file);
		file1.open(QIODevice::ReadOnly);
		QByteArray ba = file1.readAll();
		file1.close();
		MimeAttachment MimeAtt = MimeAttachment(ba, file);
		message.addPart(&MimeAtt);*/
		QString str="";
		QMessageBox msgBox;
		msgBox.setText(tr("Sending mail status")); 
		msgBox.setIcon(QMessageBox::Information); 
		
		
		
		//smtp.setConnectionTimeout(10000);
		//smtp.setHost("smtp.gmail.com"); 
		if (!smtp.connectToHost())
		{
			qDebug()<<("Connection Failed");
			smtp.quit();
			mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
			mysqlProccessObject->saveEventToLog(str = tr("SMTP client: Connection Failed"));
			msgBox.setIcon( QMessageBox::Critical ); 
			msgBox.setInformativeText( str ); 
			if( !flag_autosend || showStatus1 ) msgBox.exec(); 
			return false;
		}

		if (auth)
			if (!smtp.login(user, password))
			{
				qDebug()<<("Authentification Failed");
				smtp.quit();
				mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
				mysqlProccessObject->saveEventToLog(str = tr("SMTP client: Authentification Failed"));
				msgBox.setIcon( QMessageBox::Critical ); 
				msgBox.setInformativeText( str ); 
				if( !flag_autosend || showStatus1 ) msgBox.exec(); 
				return false;
			}


		if (!smtp.sendMail(message))
		{
			if ( smtp.getResponseCode() != 354 )
			{
				qDebug()<<("Mail sending FAILED");
				smtp.quit();
				mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
				mysqlProccessObject->saveEventToLog(str = QString(tr("SMTP client: Mail sending failed, Server response:")).append(smtp.getResponseText() ).append(QString(tr(" Code: ") + QString::number(smtp.getResponseCode())  ) ));
				msgBox.setIcon( QMessageBox::Critical ); 
				msgBox.setInformativeText( str ); 
				if( !flag_autosend || showStatus1 ) msgBox.exec(); 
				return false;
			}
			else
			{
			   qDebug()<<("Mail sending successful");
			   qDebug()<<(QString("SMTP client: Mail sending successful, Server response: ").append(smtp.getResponseText() ).append(QString(" Code: " + QString::number(smtp.getResponseCode())  ) ));
			   mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
			   mysqlProccessObject->saveEventToLog(str = QString(tr("SMTP client: Mail sending successful, Server response:")).append(smtp.getResponseText() ).append(QString(tr(" Code: ") + QString::number(smtp.getResponseCode())  ) ));
			   msgBox.setInformativeText( str ); 
			   if( !flag_autosend || showStatus1 ) msgBox.exec(); 
			}
		}
		else
		{
		   qDebug()<<("Mail sending successful");
		   qDebug()<<(QString("SMTP client: Mail sending successful, Server response: ").append(smtp.getResponseText() ).append(QString(" Code: " + QString::number(smtp.getResponseCode())  ) ));
		   mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		   mysqlProccessObject->saveEventToLog(str = QString(tr("SMTP client: Mail sending successful, Server response:")).append(smtp.getResponseText() ).append(QString(tr(" Code: ") + QString::number(smtp.getResponseCode())  ) ));
		   msgBox.setInformativeText( str ); 
		   if( !flag_autosend || showStatus1 ) msgBox.exec(); 
		}

		smtp.quit();
		return true;

	}
	else
	{
		QString str;
		QMessageBox msgBox;
		qDebug()<<("Mail sending FAILED");
		mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		mysqlProccessObject->saveEventToLog(str = QString(tr("SMTP client: Mail sending failed, No file to send!")));
		msgBox.setText(tr("Sending mail status")); 
		msgBox.setIcon( QMessageBox::Critical ); 
		msgBox.setInformativeText( str ); 
		if( !flag_autosend || showStatus1 ) msgBox.exec(); 
		return false;
	}
}
