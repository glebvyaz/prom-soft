#include "stdafx.h"
#include "spravochnik_tara.h"
#include "ui_spravochnik_tara.h"
#include "DB_Driver.h"

extern mysqlProccess *mysqlProccessObject;

spravochnik_tara::spravochnik_tara(QWidget *parent) : 
    QDialog( parent ), m_weight_tara( 0 ),
		ui( new Ui::spravochnik_tara )
{
	ui->setupUi( this );

	connect( ui->buttonClose, SIGNAL( clicked() ), this, SLOT( hide() ) );
	connect( ui->buttonAdd, SIGNAL( clicked() ), this, SLOT( addRecord_Slot() ) );
	connect( ui->buttonModify, SIGNAL( clicked() ), this, SLOT( modifyRecord_Slot() ) );
	connect( ui->buttonRemove, SIGNAL( clicked() ), this, SLOT( removeRecord_Slot() ) );

	bool flag = connect( ui->tableView, SIGNAL( clicked( const QModelIndex & ) ), this, SLOT( tableView_Clicked( const QModelIndex & ) ) );
}

// 
void spravochnik_tara::showWindow()
{
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	mysqlProccessObject->getTaraAuto( getHeadersTaraDictionary(), ui->tableView );
	this->show();
}

void spravochnik_tara::addRecord_Slot()
{
	QDialog dialog;
	dialog.resize( 320, 365 );
	dialog.setWindowTitle( tr("Add") );
	// 
	QLabel labelNumAuto( tr("Vehicle number"), &dialog );
	labelNumAuto.setGeometry( 5, 5, 310, 18 );
	labelNumAuto.setAlignment( Qt::AlignCenter );
	labelNumAuto.setStyleSheet( "font-size: 18px; font-weight: bold;" );

	QLineEdit enterNumAuto( &dialog );
	enterNumAuto.setStyleSheet( "font-size: 18px; font-weight: bold;" );
	enterNumAuto.setGeometry( 5, 25, 310, 28 );
	// 
	QLabel labelMarkAuto( tr("Brand"), &dialog );
	labelMarkAuto.setGeometry( 5, 55, 310, 18 );
	labelMarkAuto.setAlignment( Qt::AlignCenter );
	labelMarkAuto.setStyleSheet( "font-size: 18px; font-weight: bold;" );
	labelMarkAuto.show();

	QLineEdit enterMarkAuto( &dialog );
	enterMarkAuto.setGeometry( 5, 75, 310, 28 );
	enterMarkAuto.setStyleSheet( "font-size: 18px; font-weight: bold;" );
	enterMarkAuto.show();
	// 
	QLabel labelWeightTaraAuto( tr("Tara weight"), &dialog );
	labelWeightTaraAuto.setGeometry( 5, 105, 310, 18 );
	labelWeightTaraAuto.setAlignment( Qt::AlignCenter );
	labelWeightTaraAuto.setStyleSheet( "font-size: 18px; font-weight: bold;" );
	labelWeightTaraAuto.show();

	enterWeightTaraAuto = new QLineEdit;
	enterWeightTaraAuto->setStyleSheet( "font-size: 18px; font-weight: bold;" );
	enterWeightTaraAuto->setReadOnly( true );
	enterWeightTaraAuto->setParent( &dialog );

	enterWeightTaraAuto->show();
	enterWeightTaraAuto->setGeometry( 5, 125, 310, 28 );
	enterWeightTaraAuto->setText( QString::number( m_weight_tara ) );

	QPushButton btnRefresh( tr("Refresh"), &dialog );
	btnRefresh.setGeometry( 5, 155, 100, 48 );
	connect( &btnRefresh, SIGNAL( clicked() ), this, SLOT( update_tara_weight_Slot() ) );

	QPushButton btnOk( tr("Confirm"), &dialog );
	btnOk.setGeometry( 5, 255, 310, 48 );
	connect( &btnOk, SIGNAL( clicked() ), dialog.window(), SLOT( accept() ) );

	QPushButton btnCancel( tr("Close"), &dialog );
	btnCancel.setGeometry( 5, 305, 310, 48 );
	connect( &btnCancel, SIGNAL( clicked() ), dialog.window(), SLOT( hide() ) );

	int result = dialog.exec();
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();

	if( result == QDialog::Accepted )
	{
		if( enterNumAuto.text() == "" || enterMarkAuto.text() == "" || enterWeightTaraAuto->text() == "" )
		{
			QMessageBox::critical( NULL, tr("Error"), tr("Not all fields are filled"), QMessageBox::Ok );
		}
		else
		{
			if( mysqlProccessObject->insertTaraAuto( enterNumAuto.text(), enterMarkAuto.text(), enterWeightTaraAuto->text().toFloat() ) )
			{
				QMessageBox::information( NULL, tr("Data entry"), tr("Data is written to the directory!"), QMessageBox::Ok );
			}
			else
			{
				QMessageBox::critical( NULL, tr("Error"), tr("The car number already entered!"), QMessageBox::Ok );
			}
		}
	}
	
	mysqlProccessObject->getTaraAuto( getHeadersTaraDictionary(), ui->tableView );

	this->row = -1;
	this->column = -1;

	delete enterWeightTaraAuto;
}

void spravochnik_tara::modifyRecord_Slot()
{
	QDialog dialog;
	dialog.resize( 320, 365 );
	dialog.setWindowTitle( tr("Modify") );
		
	QString dateTime = QDateTime::fromString( ui->tableView->model()->index( this->row, 0 ).data().toString(), "dd.MM.yyyy hh:mm:ss" )
		.toString( "yyyy-MM-dd hh:mm:ss" );

	// 
	QLabel labelNumAuto( tr("Vehicle number"), &dialog );
	labelNumAuto.setGeometry( 5, 5, 310, 18 );
	labelNumAuto.setAlignment( Qt::AlignCenter );
	labelNumAuto.setStyleSheet( "font-size: 18px; font-weight: bold;" );

	QLineEdit enterNumAuto( &dialog );
	enterNumAuto.setStyleSheet( "font-size: 18px; font-weight: bold;" );
	enterNumAuto.setGeometry( 5, 25, 310, 28 );
	enterNumAuto.setText( ui->tableView->model()->index( this->row, 1 ).data().toString() );
	enterNumAuto.setReadOnly( true );

	// 
	QLabel labelMarkAuto( tr("Brand"), &dialog );
	labelMarkAuto.setGeometry( 5, 55, 310, 18 );
	labelMarkAuto.setAlignment( Qt::AlignCenter );
	labelMarkAuto.setStyleSheet( "font-size: 18px; font-weight: bold;" );
	labelMarkAuto.show();

	QLineEdit enterMarkAuto( &dialog );
	enterMarkAuto.setGeometry( 5, 75, 310, 28 );
	enterMarkAuto.setStyleSheet( "font-size: 18px; font-weight: bold;" );
	enterMarkAuto.show();
	enterMarkAuto.setText( ui->tableView->model()->index( this->row, 2 ).data().toString() );

	// 
	QLabel labelWeightTaraAuto( tr("Tara weight"), &dialog );
	labelWeightTaraAuto.setGeometry( 5, 105, 310, 18 );
	labelWeightTaraAuto.setAlignment( Qt::AlignCenter );
	labelWeightTaraAuto.setStyleSheet( "font-size: 18px; font-weight: bold;" );
	labelWeightTaraAuto.show();

	enterWeightTaraAuto = new QLineEdit;
	enterWeightTaraAuto->setStyleSheet( "font-size: 18px; font-weight: bold;" );
	enterWeightTaraAuto->setReadOnly( true );
	enterWeightTaraAuto->setParent( &dialog );

	enterWeightTaraAuto->show();
	enterWeightTaraAuto->setGeometry( 5, 125, 310, 28 );
	enterWeightTaraAuto->setText( QString::number( m_weight_tara ) );

	QPushButton btnRefresh( tr("Refresh"), &dialog );
	btnRefresh.setGeometry( 5, 155, 100, 48 );
	connect( &btnRefresh, SIGNAL( clicked() ), this, SLOT( update_tara_weight_Slot() ) );

	// 
	QPushButton btnOk( tr("Confirm"), &dialog );
	btnOk.setGeometry( 5, 255, 310, 48 );
	connect( &btnOk, SIGNAL( clicked() ), dialog.window(), SLOT( accept() ) );

	QPushButton btnCancel( tr("Close"), &dialog );
	btnCancel.setGeometry( 5, 305, 310, 48 );
	connect( &btnCancel, SIGNAL( clicked() ), dialog.window(), SLOT( hide() ) );

	int result = dialog.exec();
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	if( result == QDialog::Accepted )
	{
		if( enterNumAuto.text() == "" || enterMarkAuto.text() == "" || enterWeightTaraAuto->text() == "" )
		{
			QMessageBox::critical( NULL, tr("Error"), tr("Not all fields are filled"), QMessageBox::Ok );
		}
		else
		{
			if( mysqlProccessObject->updateTaraAuto( dateTime, enterNumAuto.text(), enterMarkAuto.text(), enterWeightTaraAuto->text().toFloat() ) )
			{
				QMessageBox::information( NULL, tr("Data entry"), tr("Data is written to the directory!"), QMessageBox::Ok );
			}
			else
			{
				QMessageBox::critical( NULL, tr("Error"), tr("The car number already entered!"), QMessageBox::Ok );
			}
		}
	}
	
	mysqlProccessObject->getTaraAuto( getHeadersTaraDictionary(), ui->tableView );

	this->row = -1;
	this->column = -1;

	delete enterWeightTaraAuto;
}

void spravochnik_tara::removeRecord_Slot()
{
	QString dateTime = QDateTime::fromString( ui->tableView->model()->index( this->row, 0 ).data().toString(), "dd.MM.yyyy hh:mm:ss" )
		.toString( "yyyy-MM-dd hh:mm:ss" );

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	mysqlProccessObject->deleteTaraAuto( dateTime );
	mysqlProccessObject->getTaraAuto( getHeadersTaraDictionary(), ui->tableView );

	this->row = -1;
	this->column = -1;
}

void spravochnik_tara::tableView_Clicked( const QModelIndex & index )
{
	this->row = index.row();
	this->column = index.column();
}

void spravochnik_tara::getTaraWeight_Slot( double weight )
{
	m_weight_tara = weight;
}

void spravochnik_tara::update_tara_weight_Slot()
{
	enterWeightTaraAuto->setText( QString::number( m_weight_tara ) );
	enterWeightTaraAuto->update();
	enterWeightTaraAuto->repaint();
}

// 
spravochnik_tara::~spravochnik_tara()
{
	delete ui;
}
