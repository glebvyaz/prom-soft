#include "StdAfx.h"
#include "cardReaderRfid.h"


cardReaderRfid::~cardReaderRfid()
{
	tcpSocketCardReader_1->disconnectFromHost();
	tcpSocketCardReader_2->disconnectFromHost();
	tcpSocketCardReader_3->disconnectFromHost();
}


cardReaderRfid::cardReaderRfid (QObject *parent)
					: mode_active(false), QTcpSocket(parent), tcpSocketCardReader_1( this ), tcpSocketCardReader_2( this ), tcpSocketCardReader_3( this )
{
	//QSettings settingsParams( "settings.ini", QSettings::IniFormat );
	//settingsParams.setIniCodec( "Windows-1251" );

	//// ��� ethernet ����������� �1
	//tcpSocketCardReader_1 = new QTcpSocket( this );
	//tcpSocketCardReader_1->disconnectFromHost();
	//connect( tcpSocketCardReader_1, SIGNAL( readyRead() ), this, SLOT( readCard_1_Slot() ) );
	//connect( tcpSocketCardReader_1, SIGNAL( error( QAbstractSocket::SocketError ) ), 
 //           this, SLOT( slotError( QAbstractSocket::SocketError ) ) );
	//tcpSocketCardReader_1->connectToHost( settingsParams.value( "rfid/IP1" ).toString() , settingsParams.value( "rfid/PORT1" ).toInt() );
	//nextBlockSize_1 = 0;

	//// ��� ethernet ����������� �2
	//tcpSocketCardReader_2 = new QTcpSocket( this );
	//tcpSocketCardReader_2->disconnectFromHost();
	//connect( tcpSocketCardReader_2, SIGNAL( readyRead() ), this, SLOT( readCard_2_Slot() ) );
	//connect( tcpSocketCardReader_2, SIGNAL( error( QAbstractSocket::SocketError ) ), 
 //           this, SLOT( slotError( QAbstractSocket::SocketError ) ) );
	//tcpSocketCardReader_2->connectToHost( settingsParams.value( "rfid/IP2" ).toString() , settingsParams.value( "rfid/PORT2" ).toInt() );
	//nextBlockSize_2 = 0;

	//// ��� ethernet ����������� �3
	//tcpSocketCardReader_3 = new QTcpSocket( this );
	//tcpSocketCardReader_3->disconnectFromHost();
	//connect( tcpSocketCardReader_3, SIGNAL( readyRead() ), this, SLOT( readCard_3_Slot() ) );
	//connect( tcpSocketCardReader_3, SIGNAL( error( QAbstractSocket::SocketError ) ), 
 //           this, SLOT( slotError( QAbstractSocket::SocketError ) ) );
	//tcpSocketCardReader_3->connectToHost( settingsParams.value( "rfid/IP3" ).toString() , settingsParams.value( "rfid/PORT3" ).toInt() );
	//nextBlockSize_3 = 0;

	reopenRfid();

}

void cardReaderRfid::data_rfid_received_Slot()
{
	static QByteArray ba;
	QString code;

	if( ba.length() > 11 )
		ba.clear();

/*	ba.append( serialPort->readAll() );
	if( ba.length() >= 11 )
	{
		if( ( ba.at( 0 ) != 0x01 && ba.at( 1 ) != 0x0b ) )
		{
			serialPort->flush();
			serialPort->clear();
			
			emit 

			ba.clear();
			return;
		}

		// spravochnik_obj->setRFID_Code( "" );
		for( int i = 0; i < ba.length(); i++ )
		{
			if( ( uchar )ba[ i ] < 16 )
				code.append( QString::number( ( uchar )ba[ i ], 16 ).prepend( "0" ) );
			else
				code.append( QString::number( ( uchar )ba[ i ], 16 ) );
		}
		// spravochnik_obj->setRFID_Code( code.toUpper() );

		ba.clear();
	}
	*/
}

// 
void cardReaderRfid::readCard_1_Slot()
{
	qDebug() << "ready read card reader 1";
    QByteArray message;
 
    forever
	{
        if(nextBlockSize_1 == 0)
		{
             if ( tcpSocketCardReader_1->bytesAvailable() < 6)
                break;
 
            bool ok = true;
            message = tcpSocketCardReader_1->readAll();
        }
 
        if (nextBlockSize_1 > tcpSocketCardReader_1->bytesAvailable())
            break;

		char buff[ 100 ];
		memset( buff, 0, sizeof( buff ) );
		sprintf( buff, "%02x %02x %02x %02x %02x %02x" , 
			( unsigned char )message.at( 0 ),
			( unsigned char )message.at( 1 ),
			( unsigned char )message.at( 2 ), 
			( unsigned char )message.at( 3 ),
			( unsigned char )message.at( 4 ),
			( unsigned char )message.at( 5 )
			); 
        
		qDebug( buff );
		
		if( message.length() == 6 )
		{
			QString code;
			for( int i = 3; i < message.length(); i++ )
			{
				if( ( uchar )message[ i ] < 16 )
					code.append( QString::number( ( uchar )message[ i ], 16 ).prepend( "0" ) );
				else
					code.append( QString::number( ( uchar )message[ i ], 16 ) );
			}

			emit sendCodeRfid_Signal( code );
		}
    } 
}

// 
void cardReaderRfid::readCard_2_Slot()
{
	qDebug() << "ready read card reader 2";
    QByteArray message;

    forever
	{
        if(nextBlockSize_2 == 0)
		{
             if ( tcpSocketCardReader_2->bytesAvailable() < 6)
                break;
 
            bool ok = true;
            message = tcpSocketCardReader_2->readAll();
        }
 
        if (nextBlockSize_2 > tcpSocketCardReader_2->bytesAvailable())
            break;

		char buff[ 100 ];
		memset( buff, 0, sizeof( buff ) );
		sprintf( buff, "%02x %02x %02x %02x %02x %02x" , 
			( unsigned char )message.at( 0 ),
			( unsigned char )message.at( 1 ),
			( unsigned char )message.at( 2 ), 
			( unsigned char )message.at( 3 ),
			( unsigned char )message.at( 4 ),
			( unsigned char )message.at( 5 )
			); 

		qDebug( buff );

		if( message.length() == 6 )
		{
			QString code;
			for( int i = 3; i < message.length(); i++ )
			{
				if( ( uchar )message[ i ] < 16 )
					code.append( QString::number( ( uchar )message[ i ], 16 ).prepend( "0" ) );
				else
					code.append( QString::number( ( uchar )message[ i ], 16 ) );
			}

			QSettings settings_file("settings.ini", QSettings::IniFormat);
			settings_file.setIniCodec("Windows-1251");
			bool registrationEnable = settings_file.value("Rfid/registration").toInt();

			if (registrationEnable == true)
				emit sendCodeRfid_Signal( code );
			else
				emit sendCodeRfid_Make_Record_Signal(code);
		}
    } 
}

// 
void cardReaderRfid::readCard_3_Slot()
{
	qDebug() << "ready read card reader 3";
    QByteArray message;

    forever
	{
        if(nextBlockSize_3 == 0)
		{
             if ( tcpSocketCardReader_3->bytesAvailable() < 6)
                break;
 
            bool ok = true;
            message = tcpSocketCardReader_3->readAll();
        }
 
        if (nextBlockSize_3 > tcpSocketCardReader_3->bytesAvailable())
            break;

		char buff[ 100 ];
		memset( buff, 0, sizeof( buff ) );
		sprintf( buff, "%02x %02x %02x %02x %02x %02x" , 
			( unsigned char )message.at( 0 ),
			( unsigned char )message.at( 1 ),
			( unsigned char )message.at( 2 ), 
			( unsigned char )message.at( 3 ),
			( unsigned char )message.at( 4 ),
			( unsigned char )message.at( 5 )
			);

		qDebug( buff );

		if( message.length() == 6 )
		{
			QString code;
			for( int i = 3; i < message.length(); i++ )
			{
				if( ( uchar )message[ i ] < 16 )
					code.append( QString::number( ( uchar )message[ i ], 16 ).prepend( "0" ) );
				else
					code.append( QString::number( ( uchar )message[ i ], 16 ) );
			}

			emit sendCodeRfid_Make_Record_Signal( code );
		}
    } 
}

// TCP_IP Errors
void cardReaderRfid::slotError_1( QAbstractSocket::SocketError err )
{
	QSettings settingsParams( "settings.ini", QSettings::IniFormat );
	settingsParams.setIniCodec( "Windows-1251" );

    QString strError = (err == QAbstractSocket::HostNotFoundError ? 
		"The host was not found." :
		err == QAbstractSocket::RemoteHostClosedError ? 
		"The remote host is closed." :
		err == QAbstractSocket::ConnectionRefusedError ? 
		"The connection was refused." :
		QString( tcpSocketCardReader_1->errorString())
	);

	tcpSocketCardReader_1->disconnectFromHost();
	tcpSocketCardReader_1->connectToHost( settingsParams.value( "rfid/IP1" ).toString() , settingsParams.value( "rfid/PORT1" ).toInt() );
	nextBlockSize_1 = 0;
}

// TCP_IP Errors
void cardReaderRfid::slotError_2( QAbstractSocket::SocketError err )
{
	QSettings settingsParams( "settings.ini", QSettings::IniFormat );
	settingsParams.setIniCodec( "Windows-1251" );

    QString strError = (err == QAbstractSocket::HostNotFoundError ? 
		"The host was not found." :
		err == QAbstractSocket::RemoteHostClosedError ? 
		"The remote host is closed." :
		err == QAbstractSocket::ConnectionRefusedError ? 
		"The connection was refused." :
		QString( tcpSocketCardReader_2->errorString())
	);

	tcpSocketCardReader_2->disconnectFromHost();
	tcpSocketCardReader_2->connectToHost( settingsParams.value( "rfid/IP2" ).toString() , settingsParams.value( "rfid/PORT2" ).toInt() );
	nextBlockSize_2 = 0;
}

// TCP_IP Errors
void cardReaderRfid::slotError_3( QAbstractSocket::SocketError err )
{
	QSettings settingsParams( "settings.ini", QSettings::IniFormat );
	settingsParams.setIniCodec( "Windows-1251" );

    QString strError = (err == QAbstractSocket::HostNotFoundError ? 
		"The host was not found." :
		err == QAbstractSocket::RemoteHostClosedError ? 
		"The remote host is closed." :
		err == QAbstractSocket::ConnectionRefusedError ? 
		"The connection was refused." :
		QString( tcpSocketCardReader_3->errorString())
	);

	tcpSocketCardReader_3->disconnectFromHost();
	tcpSocketCardReader_3->connectToHost( settingsParams.value( "rfid/IP3" ).toString() , settingsParams.value( "rfid/PORT3" ).toInt() );
	nextBlockSize_3 = 0;

}

void cardReaderRfid::reopenRfid()
{
	QSettings settingsParams( "settings.ini", QSettings::IniFormat );
	settingsParams.setIniCodec( "Windows-1251" );

	// ��� ethernet ����������� �1
	tcpSocketCardReader_1 = new QTcpSocket(this);
	tcpSocketCardReader_1->disconnectFromHost();
	connect( tcpSocketCardReader_1, SIGNAL( readyRead() ), this, SLOT( readCard_1_Slot() ) );
	connect( tcpSocketCardReader_1, SIGNAL( error( QAbstractSocket::SocketError ) ), 
            this, SLOT( slotError( QAbstractSocket::SocketError ) ) );
	tcpSocketCardReader_1->connectToHost( settingsParams.value( "rfid/IP1" ).toString() , settingsParams.value( "rfid/PORT1" ).toInt() );
	nextBlockSize_1 = 0;

	// ��� ethernet ����������� �2
	tcpSocketCardReader_2 = new QTcpSocket(this);
	tcpSocketCardReader_2->disconnectFromHost();
	bool dsgdfggdr = connect( tcpSocketCardReader_2, SIGNAL( readyRead() ), this, SLOT( readCard_2_Slot() ) );
	bool asdhgfsdgh = connect( tcpSocketCardReader_2, SIGNAL( error( QAbstractSocket::SocketError ) ), 
            this, SLOT( slotError_2( QAbstractSocket::SocketError ) ) );
	if( tcpSocketCardReader_2->state() ==  QTcpSocket::UnconnectedState ) 
		tcpSocketCardReader_2->connectToHost( settingsParams.value( "rfid/IP2" ).toString() , settingsParams.value( "rfid/PORT2" ).toInt() );
	nextBlockSize_2 = 0;

	// ��� ethernet ����������� �3
	tcpSocketCardReader_3 = new QTcpSocket(this);
	tcpSocketCardReader_3->disconnectFromHost();
	connect( tcpSocketCardReader_3, SIGNAL( readyRead() ), this, SLOT( readCard_3_Slot() ) );
	connect( tcpSocketCardReader_3, SIGNAL( error( QAbstractSocket::SocketError ) ), 
            this, SLOT( slotError( QAbstractSocket::SocketError ) ) );
	tcpSocketCardReader_3->connectToHost( settingsParams.value( "rfid/IP3" ).toString() , settingsParams.value( "rfid/PORT3" ).toInt() );
	nextBlockSize_3 = 0;
}

