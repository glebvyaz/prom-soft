#include "stdafx.h"
#include "optionaldevices.h"
#include "ui_optionaldevices.h"

optionalDevices::optionalDevices(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::optionalDevices)
{
    ui->setupUi(this);

	QSettings settingsComPort( "settings.ini", QSettings::IniFormat );
	settingsComPort.setIniCodec( "Windows-1251" );
	
	ui->groupBox_s1_rele0->setTitle( 
		QString (ui->groupBox_s1_rele0->title() + " (" + settingsComPort.value( "Devices/socket1_name1" ).toString() + ")" ) );
	ui->groupBox_s1_rele1->setTitle( 
		QString (ui->groupBox_s1_rele1->title() + " (" + settingsComPort.value( "Devices/socket1_name2" ).toString() + ")" ) );
	ui->groupBox_s2_rele0->setTitle( 
		QString (ui->groupBox_s2_rele0->title() + " (" + settingsComPort.value( "Devices/socket2_name1" ).toString() + ")" ) );
	ui->groupBox_s2_rele1->setTitle( 
		QString (ui->groupBox_s2_rele1->title() + " (" + settingsComPort.value( "Devices/socket2_name2" ).toString() + ")" ) );
	ui->groupBox_s3_rele0->setTitle( 
		QString (ui->groupBox_s3_rele0->title() + " (" + settingsComPort.value( "Devices/socket3_name1" ).toString() + ")" ) );
	ui->groupBox_s3_rele1->setTitle( 
		QString (ui->groupBox_s3_rele1->title() + " (" + settingsComPort.value( "Devices/socket3_name2" ).toString() + ")" ) );

	connect( ui->radioButton_s1_rele0_ON, SIGNAL( clicked() ), this, SLOT( radioClick_Slot() ) );
	connect( ui->radioButton_s1_rele0_OFF, SIGNAL( clicked() ), this, SLOT( radioClick_Slot() ) );
	connect( ui->radioButton_s1_rele1_ON, SIGNAL( clicked() ), this, SLOT( radioClick_Slot() ) );
	connect( ui->radioButton_s1_rele1_OFF, SIGNAL( clicked() ), this, SLOT( radioClick_Slot() ) );

	connect( ui->radioButton_s2_rele0_ON, SIGNAL( clicked() ), this, SLOT( radioClick_Slot() ) );
	connect( ui->radioButton_s2_rele0_OFF, SIGNAL( clicked() ), this, SLOT( radioClick_Slot() ) );
	connect( ui->radioButton_s2_rele1_ON, SIGNAL( clicked() ), this, SLOT( radioClick_Slot() ) );
	connect( ui->radioButton_s2_rele1_OFF, SIGNAL( clicked() ), this, SLOT( radioClick_Slot() ) );

	connect( ui->radioButton_s3_rele0_ON, SIGNAL( clicked() ), this, SLOT( radioClick_Slot() ) );
	connect( ui->radioButton_s3_rele0_OFF, SIGNAL( clicked() ), this, SLOT( radioClick_Slot() ) );
	connect( ui->radioButton_s3_rele1_ON, SIGNAL( clicked() ), this, SLOT( radioClick_Slot() ) );
	connect( ui->radioButton_s3_rele1_OFF, SIGNAL( clicked() ), this, SLOT( radioClick_Slot() ) );
}

optionalDevices::~optionalDevices()
{
    delete ui;
}

// set radiobuttons, nums 1,2,3; relay 0,1; setTo true
void optionalDevices::setSocket2( int num, int relay, bool setTo )
{
	switch( num )
	{
	case 1:
		if( relay == 1 )
		{
			ui->radioButton_s1_rele1_ON->setChecked( setTo );
			ui->radioButton_s1_rele1_OFF->setChecked( !setTo );
		}
		else if( relay == 0 )
		{
			ui->radioButton_s1_rele0_ON->setChecked( setTo );
			ui->radioButton_s1_rele0_OFF->setChecked( !setTo );
		}
		break;
	case 2:
		if( relay == 1 )
		{
			ui->radioButton_s2_rele1_ON->setChecked( setTo );
			ui->radioButton_s2_rele1_OFF->setChecked( !setTo );
		}
		else if( relay == 0 )
		{
			ui->radioButton_s2_rele0_ON->setChecked( setTo );
			ui->radioButton_s2_rele0_OFF->setChecked( !setTo );
		}
		break;
	case 3:
		if( relay == 1 )
		{
			ui->radioButton_s3_rele1_ON->setChecked( setTo );
			ui->radioButton_s3_rele1_OFF->setChecked( !setTo );
		}
		else if( relay == 0 )
		{
			ui->radioButton_s3_rele0_ON->setChecked( setTo );
			ui->radioButton_s3_rele0_OFF->setChecked( !setTo );
		}
		break;
	}	
}

void optionalDevices::setLink( int num, bool setTo )
{
	switch( num )
	{
	case 1:
		if( setTo )
		{
			ui->label_link_s1->setText( tr("Connected") );
			ui->label_link_s1->setStyleSheet( "color:green;" );
		}
		else	
		{
			ui->label_link_s1->setText( tr("Not connected") );
			ui->label_link_s1->setStyleSheet( "color:red;" );
		}
		break;
	case 2:
		if( setTo ) 
		{
			ui->label_link_s2->setText( tr("Connected") );
			ui->label_link_s2->setStyleSheet( "color:green;" );
		}
		else	
		{
			ui->label_link_s2->setText( tr("Not connected") );
			ui->label_link_s2->setStyleSheet( "color:red;" );
		}
		break;
	case 3:
		if( setTo )
		{
			ui->label_link_s3->setText( tr("Connected") );
			ui->label_link_s3->setStyleSheet( "color:green;" );
		}
		else	
		{
			ui->label_link_s3->setText( tr("Not connected") );
			ui->label_link_s3->setStyleSheet( "color:red;" );
		}
		break;
	}	
}

// s 1,2,3; r 0,1
bool optionalDevices::getRelayState( int s, int r )
{
	switch( s )
	{
	case 1:
		if( r == 1 ) return ui->radioButton_s1_rele1_ON->isChecked();
		else if( r == 0 ) return ui->radioButton_s1_rele0_ON->isChecked();
		break;
	case 2:
		if( r == 1 ) return ui->radioButton_s2_rele1_ON->isChecked();
		else if( r == 0 ) return ui->radioButton_s2_rele0_ON->isChecked();
		break;
	case 3:
		if( r == 1 ) return ui->radioButton_s3_rele1_ON->isChecked();
		else if( r == 0 ) return ui->radioButton_s3_rele0_ON->isChecked();
		break;
	}
	return false;
}

void optionalDevices::radioClick_Slot()
{
	QRadioButton* button = static_cast< QRadioButton* >( sender() ); 
	QString buttonName = button->name();

	// ����� ������ ������ - ������� ���
	if( buttonName == "radioButton_s1_rele0_ON" )		emit radioClicked_Signal( 1, 0, 1 );
	else if( buttonName == "radioButton_s1_rele0_OFF" )	emit radioClicked_Signal( 1, 0, 0 );
	else if( buttonName == "radioButton_s1_rele1_ON" )	emit radioClicked_Signal( 1, 1, 1 );
	else if( buttonName == "radioButton_s1_rele1_OFF" )	emit radioClicked_Signal( 1, 1, 0 );

	else if( buttonName == "radioButton_s2_rele0_ON" )	emit radioClicked_Signal( 2, 0, 1 );
	else if( buttonName == "radioButton_s2_rele0_OFF" )	emit radioClicked_Signal( 2, 0, 0 );
	else if( buttonName == "radioButton_s2_rele1_ON" )	emit radioClicked_Signal( 2, 1, 1 );
	else if( buttonName == "radioButton_s2_rele1_OFF" )	emit radioClicked_Signal( 2, 1, 0 );

	else if( buttonName == "radioButton_s3_rele0_ON" )	emit radioClicked_Signal( 3, 0, 1 );
	else if( buttonName == "radioButton_s3_rele0_OFF" )	emit radioClicked_Signal( 3, 0, 0 );
	else if( buttonName == "radioButton_s3_rele1_ON" )	emit radioClicked_Signal( 3, 1, 1 );
	else if( buttonName == "radioButton_s3_rele1_OFF" )	emit radioClicked_Signal( 3, 1, 0 );
}
