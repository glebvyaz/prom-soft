#include "stdafx.h"
#include "logEventsForm.h"
#include "DB_Driver.h"
// 
logEventsFormClass::logEventsFormClass( QWidget *w, QObject *object )
{
	const QSize comboBoxSize = QSize( 190, 32 ); // ������ comboBox - � 
	const QSize lineEditSize = QSize( 81, 32 ); // ������ lineEdit - �
	const QSize pushButtonSize = QSize( 131, 28 ); // ������  pushButton - �

	//QPixmap global_Icon;
	//QIcon icon;

	//global_Icon.load( "image/logo_vis2png", "PNG", QPixmap::Auto );
	// 
	// ����������  ������� �������� ����, � ������������� �� 
	QRect screenResolution = qApp->desktop()->screenGeometry();
    // 
	// ����� ������� 
	logEventsFormWindow = new QDialog( w, Qt::CustomizeWindowHint|Qt::WindowTitleHint );
	logEventsFormWindow->setObjectName( "reportForm" );
	logEventsFormWindow->installEventFilter( object );
	logEventsFormWindow->setGeometry( ( screenResolution.width() <= 1440 ? ( screenResolution.width() - 1340 ) / 2 : ( ( screenResolution.width() - 1500 ) )  / 2 ), 100, 0, 0 );
	logEventsFormWindow->setFixedWidth( ( screenResolution.width() <= 1440 ? 1364 : 1500 ) );
	logEventsFormWindow->setFixedHeight( screenResolution.height() - 240 );
	logEventsFormWindow->setWindowTitle( tr("Logging") );
	// 
	//logoVIS = new QLabel( logEventsFormWindow );
	//logoVIS->setGeometry( 24, 5, 110, 151 );
	//logoVIS->setPixmap( global_Icon );
	// 
	sortGroupBox = new QGroupBox( logEventsFormWindow );
	sortGroupBox->setGeometry( 10 + 120, 20, 1350, 124 );
	sortGroupBox->setTitle( tr("Datetime") );
	// 
	acceptFilterButton = new QPushButton( tr("Confirm\nfilter"), sortGroupBox ); // ������ ���������� �������(��)
	acceptFilterButton->setGeometry( 10, 20, pushButtonSize.width(), pushButtonSize.height() + 16 );
   // QPixmap acceptReportsPixmap;
	//acceptReportsPixmap.load( "image/filter_acceptpng", "PNG", QPixmap::Auto );
	//acceptFilterButton->setPixmap( acceptReportsPixmap );
	connect( acceptFilterButton, SIGNAL( clicked() ), object, SLOT( logEventsAcceptFiltering() ) ); // ��������� ������� ����� ������� 
	// 
	resetFilterButton = new QPushButton( tr("Reset\nfilter"), sortGroupBox ); // ������ ������ �������(��)
	resetFilterButton->setGeometry( 10, 20 + pushButtonSize.height() + 16 + 5, pushButtonSize.width(), pushButtonSize.height() + 16 );
    //QPixmap resettReportsPixmap;
	//resettReportsPixmap.load( "image/filter_resetpng", "PNG", QPixmap::Auto );
	//resetFilterButton->setPixmap( resettReportsPixmap );
	connect( resetFilterButton, SIGNAL( clicked() ), object, SLOT( logEventsResetFiltering() ) ); // �������� ������� ����� ������� 
	// 
	dateBeginLabel = new QLabel( tr("Date (start)"), sortGroupBox );
	dateBeginLabel->setGeometry( 10 + pushButtonSize.width() + 7, 20, 140, 18 );
	dateBeginEdit = new QDateEdit( sortGroupBox );
	dateBeginEdit->setGeometry( 10 + pushButtonSize.width() + 7, 39, 104, 24  );
	dateBeginEdit->setDate( QDate::currentDate() ); 

    dateEndLabel = new QLabel( tr("Date (end)"), sortGroupBox );
	dateEndLabel->setGeometry( 10 + pushButtonSize.width() + 7, 70, 140, 18 );
	dateEndEdit = new QDateEdit( sortGroupBox );
	dateEndEdit->setGeometry( 10 + pushButtonSize.width() + 7, 89, 104, 24  );
	dateEndEdit->setDate( QDate::currentDate() ); 

	timeBeginLabel = new QLabel( tr("Time(start)"), sortGroupBox );
	timeBeginLabel->setGeometry( 10 + pushButtonSize.width() + 7 + 112, 20, 140, 18 );
	timeBeginEdit = new QTimeEdit( sortGroupBox );
	timeBeginEdit->setGeometry( 10 + pushButtonSize.width() + 7 + 112, 39, 104, 24  );
	timeBeginEdit->setTime( QTime( 0, 0, 0 ) );

    timeEndLabel = new QLabel( tr("Time(end)"), sortGroupBox );
	timeEndLabel->setGeometry( 10 + pushButtonSize.width() + 7 + 112, 70, 140, 18 );
	timeEndEdit = new QTimeEdit( sortGroupBox );
	timeEndEdit->setGeometry( 10 + pushButtonSize.width() + 7 + 112, 89, 104, 24  );
	timeEndEdit->setTime( QTime( 23, 59, 59 ) );

	// ������ ������� ����� �������� "�����"
	closeButton = new QPushButton( tr("Close"), logEventsFormWindow );
	closeButton->setGeometry( 7, 143, pushButtonSize.width() - 16, pushButtonSize.height() + 16 );
	
	//QIcon iconCloseButton;
	//iconCloseButton.setPixmap( "image/cancel_32_32_bigpng", QIcon::Large, QIcon::Normal, QIcon::On );
	//closeButton->setIcon( iconCloseButton );
	connect( closeButton, SIGNAL( clicked() ), object, SLOT( closeLogEventsWindow_Slot() ) ); // ������� ���� ����� 
	// 
	logWindow = new QTextEdit( logEventsFormWindow ); // ���� ����������� �����
	logWindow->setGeometry( 10 + 120, 20 + 124 + 7, ( screenResolution.width() <= 1440 ? 1232 : 1350 ), logEventsFormWindow->geometry().height() - sortGroupBox->height() - 32 );
	logWindow->setReadOnly( true );

	logEventsFormWindow->hide();
}
// ���������� ���� 
void logEventsFormClass::showWindow()
{
    logEventsFormWindow->show();
}
// ������ ���� 
void logEventsFormClass::closeWindow()
{
	logEventsFormWindow->hide();
}

logEventsFormClass::~logEventsFormClass()
{
}
