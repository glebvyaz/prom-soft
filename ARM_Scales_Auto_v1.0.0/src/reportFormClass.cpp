#include "stdafx.h"
#include "reportFormClass.h"
#include "settings.h"
#include "DB_Driver.h"

#include <ActiveQt/qaxobject.h>
#include <ActiveQt/qaxbase.h>

class SettingsForm;
extern SettingsForm *settings;
//extern mysqlProccess *mysqlProccessObject; // ������ ��� ������ � �� MySQL

// 
reportFormClass::reportFormClass( QWidget *w, QObject *object )
{
	const QSize comboBoxSize = QSize( 190, 32 ); // ������ comboBox - � 
	const QSize lineEditSize = QSize( 81, 32 ); // ������ lineEdit - �
	const QSize pushButtonSize = QSize( 131, 28 ); // ������  pushButton - �

	row = -1;

	QIcon icon;
	// 
	// ����������  ������� �������� ����, � ������������� �� 
	QRect screenResolution = qApp->desktop()->screenGeometry();
    // 
	// ����� ������� 
	reportForm = new QDialog( w, Qt::CustomizeWindowHint|Qt::WindowTitleHint|Qt::WindowCloseButtonHint|Qt::WindowMinMaxButtonsHint);
	reportForm->setObjectName( "reportForm" );
	reportForm->installEventFilter( object );

	// 
	if( screenResolution.width() <= 1024 )
	{
		reportForm->setGeometry( 10, 10, 1006, 758 );
		reportForm->resize( 1006, 758 );
	}
	else
	{
		reportForm->setGeometry( ( screenResolution.width() <= 1440 ? 
			( screenResolution.width() - 1346 ) / 2 : ( ( screenResolution.width() - 1500 ) )  / 2 ), 100, 0, 0 );
		reportForm->setFixedWidth( ( screenResolution.width() <= 1440 ? 1356 : 1500 ) );
		reportForm->setFixedHeight( screenResolution.height() - 240 );
	}

	reportForm->setWindowTitle( tr("Registry") );

	// 
	sortGroupBox = new QGroupBox( reportForm );
	sortGroupBox->setGeometry( 10 + 120, 20, 1350, 280 );
	sortGroupBox->setTitle( tr("Filter params") );
	// 
	checkBoxNumAuto = new QCheckBox( tr("Vehicle num"), sortGroupBox );
	checkBoxNumAuto->setGeometry( 20, 32, 95, 18 );
	connect( checkBoxNumAuto, SIGNAL( stateChanged( int ) ), this, SLOT( checkBoxNumAutoSlot( int ) ) );
    // 
	enterNumAuto = new QComboBox( sortGroupBox );
	enterNumAuto->setGeometry( 20, 32 + 24, 131, 24 );
	enterNumAuto->setEditable( true );
	//enterNumAuto->setStyleSheet( "font-size: 32px;" );  
	// 
	acceptFilterButton = new QPushButton( tr("Confirm\nfilter"), sortGroupBox ); // ������ ���������� �������(��)
	acceptFilterButton->setGeometry( 20, 32 + 24 + lineEditSize.height() + 10, pushButtonSize.width(), pushButtonSize.height() + 16 );
    //QPixmap acceptReportsPixmap;
	//acceptReportsPixmap.load( "image/ng", "PNG", QPixmap::Auto );
	//acceptFilterButton->setPixmap( acceptReportsPixmap );
	connect( acceptFilterButton, SIGNAL( clicked() ), object, SLOT( reportsAcceptFiltering() ) ); // ��������� ������� ������������ ������� 
	// 
	resetFilterButton = new QPushButton( tr("Reset\nfilter"), sortGroupBox ); // ������ ������ �������(��)
	resetFilterButton->setGeometry( 20, 32 + 24 + lineEditSize.height() + 10 + pushButtonSize.height() + 16 + 5, pushButtonSize.width(), pushButtonSize.height() + 16 );
    //QPixmap resettReportsPixmap;
	//resettReportsPixmap.load( "image/ng", "PNG", QPixmap::Auto );
	//resetFilterButton->setPixmap( resettReportsPixmap );
	connect( resetFilterButton, SIGNAL( clicked() ), object, SLOT( reportsResetFiltering() ) ); // �������� ������� ������������ ������� 
	// 
    radioButtAutoComeInReports = new QRadioButton( tr("Accepting"), sortGroupBox );
	radioButtAutoComeInReports->setGeometry( 20, 32 + 24 + lineEditSize.height() + 10 + pushButtonSize.height() + 16 + 24 + pushButtonSize.height() + 5, pushButtonSize.width(), 18 );
	 // radioButtAutoComeInReports
	// 
	radioButtAutoComeOutReports = new QRadioButton( tr("Sending"), sortGroupBox );
	radioButtAutoComeOutReports->setGeometry( 20, 32 + 24 + lineEditSize.height() + 10 + pushButtonSize.height() + 16 + 24 + pushButtonSize.height() + 5 + 24, pushButtonSize.width(), 18 );
	// 
	checkBoxDateBegin = new QCheckBox( tr("Date (start)"), sortGroupBox );
	checkBoxDateBegin->setGeometry( 20 + lineEditSize.width() + 16 + 48 , 32, 180, 18 );
	connect( checkBoxDateBegin, SIGNAL( stateChanged( int ) ), this, SLOT( checkBoxDateBeginSlot( int ) ) );
    // 
	QSettings settingsLang( "settings.ini", QSettings::IniFormat );
	settingsLang.setIniCodec( "Windows-1251" );
	
	QString langFromConf;
	settingsLang.beginGroup( "Server" );
	langFromConf = settingsLang.value( "lang", "ua" ).toString();
	settingsLang.endGroup();

	beginCalendar = new QCalendarWidget( sortGroupBox );
	beginCalendar->setGeometry( 20 + lineEditSize.width() + 16 + 48 , 32 + 24, ( screenResolution.width() <= 1024 ? 185 : 265 ), 180 );
	beginCalendar->setGridVisible( true );
	beginCalendar->setFirstDayOfWeek(Qt::Monday);
	if( langFromConf == "ru" )
	{
		beginCalendar->setLocale( QLocale::Russian );
	}
	else if( langFromConf == "ua" )
	{
		beginCalendar->setLocale( QLocale::Ukrainian );
	}
	else if( langFromConf == "en" )
	{
		beginCalendar->setLocale( QLocale::English );
	}
	// 
	checkBoxDateEnd = new QCheckBox( tr("Date (end)"), sortGroupBox );
	checkBoxDateEnd->setGeometry( 20 + lineEditSize.width() + 16 + 285 + 48 , 32, 180, 18 );
	connect( checkBoxDateEnd, SIGNAL( stateChanged( int ) ), this, SLOT( checkBoxDateEndSlot( int ) ) );
    // 
	endCalendar = new QCalendarWidget( sortGroupBox );
	endCalendar->setGeometry( ( screenResolution.width() <= 1024 ? 350 : 450 ), 32 + 24, ( screenResolution.width() <= 1024 ? 185 : 265 ), 180 );
	endCalendar->setGridVisible( true );
	endCalendar->setFirstDayOfWeek(Qt::Monday);
	if( langFromConf == "ru" )
	{
		endCalendar->setLocale( QLocale::Russian );
	}
	else if( langFromConf == "ua" )
	{
		endCalendar->setLocale( QLocale::Ukrainian );
	}
	else if( langFromConf == "en" )
	{
		endCalendar->setLocale( QLocale::English );
	}
    // 
	checkBoxTimeBegin = new QCheckBox( tr("Time(start)"), sortGroupBox );
	checkBoxTimeBegin->setGeometry( ( screenResolution.width() <= 1024 ? 543 : 734 ), 32, 180, 24 );
	connect( checkBoxTimeBegin, SIGNAL( stateChanged( int ) ), this, SLOT( checkBoxTimeBeginSlot( int ) ) );

	beginTime = new QTimeEdit( sortGroupBox );
	beginTime->setGeometry( ( screenResolution.width() <= 1024 ? 543 : 734 ), 32 + 32,	95, 32 );
	// 
	checkBoxTimeEnd = new QCheckBox( tr("Time(end)"), sortGroupBox );
	checkBoxTimeEnd->setGeometry( ( screenResolution.width() <= 1024 ? 543 + 104 : 734 + 104 ), 32, 180, 24 );
	connect( checkBoxTimeEnd, SIGNAL( stateChanged( int ) ), this, SLOT( checkBoxTimeEndSlot( int ) ) );

	endTime = new QTimeEdit( sortGroupBox );
	endTime->setGeometry( ( screenResolution.width() <= 1024 ? 543 + 104 : 734 + 100 ), 32 + 32,	95, 32 );
    // 
    checkBoxSender = new QCheckBox( tr("Sender"), sortGroupBox );
	checkBoxSender->setGeometry( ( screenResolution.width() <= 1024 ? 543 : 734 ), 32 + 64, 180, 24 );
	connect( checkBoxSender, SIGNAL( stateChanged( int ) ), this, SLOT( checkBoxSenderSlot( int ) ) );

	selectSender = new QComboBox( sortGroupBox );
	selectSender->setGeometry( ( screenResolution.width() <= 1024 ? 543 : 734 ), 32 + 64 + 32 , 180, 24 );
	selectSender->setEditable( true );
	// 
	checkBoxAccepter = new QCheckBox( tr("Recipient"), sortGroupBox );;
	checkBoxAccepter->setGeometry( ( screenResolution.width() <= 1024 ? 543 : 734 ), 32 + 64 + 32 + 32, 180, 24 );
	connect( checkBoxAccepter, SIGNAL( stateChanged( int ) ), this, SLOT( checkBoxAccepterSlot( int ) ) );

	selectAccepter = new QComboBox( sortGroupBox );
	selectAccepter->setGeometry( ( screenResolution.width() <= 1024 ? 543 : 734 ), 32 + 64 + 32 + 32 + 32, 180, 24 );
	selectAccepter->setEditable( true );
	// 
	checkBoxCarrier = new QCheckBox( tr("Carrier"), sortGroupBox );
	checkBoxCarrier->setGeometry( ( screenResolution.width() <= 1024 ? 543 + 204 : 734 + 95 + 114 ), 32, 180, 24 );
	connect( checkBoxCarrier, SIGNAL( stateChanged( int ) ), this, SLOT( checkBoxCarrierSlot( int ) ) );

	selectCarrier = new QComboBox( sortGroupBox );
	selectCarrier->setGeometry( ( screenResolution.width() <= 1024 ? 543 + 204 : 734 + 95 + 100 ), 32 + 32 , ( screenResolution.width() <= 1024 ? 140 : 180 ), 24 );
	selectCarrier->setEditable( true );
	// 
	labelNumScales = new QLabel( tr("Scales num"), sortGroupBox );
	labelNumScales->setGeometry( ( screenResolution.width() <= 1024 ? 543 + 204 + 204 : 734 + 95 + 95 + 95 + 95 ), ( screenResolution.width() <= 1024 ? 380 : 32 ), 81, 24 );

	comboBoxNumScales = new QComboBox( sortGroupBox ); // ����� ������ �����
	comboBoxNumScales->setGeometry( ( screenResolution.width() <= 1024 ? 543 + 204 : 734 + 100 + 95 + 95 + 95 ), ( screenResolution.width() <= 1024 ? 380 : 32 + 32 ) , 95, 24 );
	comboBoxNumScales->addItems( settings->load_list_Scales() ); // ������� � ������ ����� 
	// 
	checkBoxTypeOfGoods = new QCheckBox( tr("Cargo"), sortGroupBox );
	checkBoxTypeOfGoods->setGeometry( ( screenResolution.width() <= 1024 ? 543 + 204 : 734 + 95 + 100 ), 32 + 32 + 32, 180, 24 );
	connect( checkBoxTypeOfGoods, SIGNAL( stateChanged( int ) ), this, SLOT( checkBoxTypeOfGoodsSlot( int ) ) );

	selectTypeOfGoods = new QComboBox( sortGroupBox );
	selectTypeOfGoods->setGeometry( ( screenResolution.width() <= 1024 ? 543 + 204 : 734 + 95 + 100 ), 32 + 32 + 32 + 32 , ( screenResolution.width() <= 1024 ? 140 : 180 ), 24 );
    selectTypeOfGoods->setEditable( true );
	// 
	checkBoxPayer = new QCheckBox( tr("Payer"), sortGroupBox );
	checkBoxPayer->setGeometry( ( screenResolution.width() <= 1024 ? 543 + 204 : 734 + 95 + 100 ), 32 + 32 + 32 + 32 + 32, 180, 24 );
	connect( checkBoxPayer, SIGNAL( stateChanged( int ) ), this, SLOT( checkBoxPayerSlot( int ) ) );

	selectPayer = new QComboBox( sortGroupBox );
	selectPayer->setGeometry( ( screenResolution.width() <= 1024 ? 543 + 204 : 734 + 95 + 100 ), 32 + 32 + 32 + 32 + 32 + 32 , ( screenResolution.width() <= 1024 ? 140 : 180 ), 24 );
	selectPayer->setEditable( true );
	//// 
	int cntColumns;
	QList<QString> headersText;
	// �������  ����������� ������� 
	tableReports = new QTableWidget( reportForm );

	if( screenResolution.width() <= 1024 )
	{
		tableReports->setGeometry( 130, 268, 886, 445 );
	}
	else
	{
	    tableReports->setGeometry( 10 + 120, 20 + 248 , ( screenResolution.width() <= 1440 ? 1232 : 1350 ), reportForm->geometry().height() - 248 - 32 );
	}    
	
	// 
	tableReports->setVerticalScrollBarPolicy( Qt::ScrollBarAsNeeded ); // ScrollBarAsNeeded );
	tableReports->setHorizontalScrollBarPolicy( Qt::ScrollBarAsNeeded ); //  ScrollBarAsNeeded );
	tableReports->setEditTriggers( QAbstractItemView::NoEditTriggers ); // ��������� ������������� ������ 

	cntColumns = getColumnsHeadersTableAutoReports().count(); // ���������� ������� � ������� 
	headersText = getColumnsHeadersTableAutoReports(); // ������� ��� ��������  �������  �� ������ 
	tableReports->setColumnCount( cntColumns ); // ???

	tableReports->setStyleSheet( "font-weight: bold; font-size: 12px;" );

	QSettings settingsT( "settings.ini", QSettings::IniFormat );
	settingsT.setIniCodec("Windows-1251");
	for(int i = 0; i < tableReports->model()->columnCount() - 1; i++ )
	{
		tableReports->setColumnWidth( i, settingsT.value( QString( "columnSizes/r%1" ).arg( i ), 150 ).toInt() );
	}
	tableReports->horizontalHeader()->setStretchLastSection( true );

    // 
	// ��������� ������� � ������� ������/�������� ����������� 
	for( int j = 0; j < cntColumns; j++ )
	{
		tableReports->setHorizontalHeaderLabels( headersText ); 
		tableReports->setAlternatingRowColors( true );
	}
	connect( tableReports, SIGNAL( cellClicked( int, int ) ), object, SLOT( selectTableReports_Record( int, int ) ) ); // �������� ������  ��� ���������� ��������� ����������� �� ������� ������� 
	connect( tableReports, SIGNAL( pressed( QModelIndex ) ), object, SLOT( prepare_showPhotoReports( QModelIndex ) ) ); // SLOT ��� ����������� ��������� ��� ����� ���� �� ������ � ������ ������� 
	connect( tableReports, SIGNAL( itemSelectionChanged() ), object, SLOT( unselectAutoReception_Record_Item() ) ); // �������� ������� ������ ������� �������  ������/�������� 
    connect( tableReports, SIGNAL( cellDoubleClicked( int, int ) ), object, SLOT( makeNakladnayaDoubleClick_SLOT( int, int ) ) ); // �������� ������ ������� ������ ��� ������������ ��������� 
	connect( tableReports->horizontalHeader(), SIGNAL( sectionDoubleClicked( int ) ), object, SLOT( headerTableReports_Clicked( int ) ) ); // ���� �� ��������� � ������� �������� ����� ���������� ������� 
	// 
    // ��������� ������� ������ �� ������� ������� 
	loadIndicatorReports = new QLabel( "TEST", tableReports );
	loadIndicatorReports->setGeometry( 0, 0, tableReports->size().width(), tableReports->size().height() );
    loadIndicatorReports->setText( "<img style=\"background-position: center center;background-attachment: fixed;background-repeat: no-repeat;\" src=\"image/loader-database.gif\" alt=\"Wait please\"></img>" );
	loadIndicatorReports->hide();
	// 
	makeTTN_Button = new QPushButton( tr("Create TTN"), reportForm ); // ������� �������� ������/�������� 
	makeTTN_Button->setGeometry( 5,
		20 + 180 - pushButtonSize.height() - 11 - 8,
		pushButtonSize.width() - 11,
		pushButtonSize.height() + 16 );
	connect( makeTTN_Button, SIGNAL( clicked() ), object, SLOT( makeTTN_Button_Slot() ) );

	//makeTTN_Button->hide();
	// 
	makeNakladnayaButton = new QPushButton( tr("Create\nwaybill"), reportForm ); // ������� �������� ������/�������� 
	makeNakladnayaButton->setGeometry( 5,
		20 + 180,
		pushButtonSize.width() - 11,
		pushButtonSize.height() + 16 );
	//QPixmap createReportsPixmap;
	//createReportsPixmap.load( "image/report-icon_32_32png", "PNG", QPixmap::Auto );
	//makeNakladnayaButton->setPixmap( createReportsPixmap );
	connect( makeNakladnayaButton, SIGNAL( clicked() ), object, SLOT( makeNakladnaya_SLOT() ) );
	//
	exportExccellButton = new QPushButton( tr("Export to\nExcel"), reportForm ); // ������� ������ � Excel
	exportExccellButton->setGeometry( 5,
		20 + 180 + pushButtonSize.height() + 11 + 8,
		pushButtonSize.width() - 11,
		pushButtonSize.height() + 16 );
	//QPixmap exportEccellPixmap;
	//exportEccellPixmap.load( "image/Excel-icon_32_32png", "PNG", QPixmap::Auto );
	//exportExccellButton->setPixmap( exportEccellPixmap );
	connect( exportExccellButton, SIGNAL( clicked() ), object, SLOT( convertToExcell() ) ); // ������������� ������ �������� � ������� Excell


	exportExccellButton_sor = new QPushButton( tr("Export to\nExcel (+sor)"), reportForm ); // ������� ������ � Excel
	exportExccellButton_sor->setGeometry( 5,
		20 + 180 + pushButtonSize.height() + 11 + 8 + pushButtonSize.height() + 11 + 8 + pushButtonSize.height() + 148,
		pushButtonSize.width() - 11,
		pushButtonSize.height() + 16 );
	connect( exportExccellButton_sor, SIGNAL( clicked() ), object, SLOT( convertToExcell_sor() ) ); // ������������� ������ �������� � ������� Excell
	exportExccellButton_sor->hide();
	// 
	printRecords = new QPushButton( tr("Print\ntable"), reportForm ); // ����������� ���������� ������� ����������� � ��������� ���� 
	printRecords->setGeometry( 5,
		20 + 180 + pushButtonSize.height() + 11 + 8 + pushButtonSize.height() + 11 + 8,
		pushButtonSize.width() - 11,
		pushButtonSize.height() + 16 );
	//QPixmap printTablePixmap;
	//printTablePixmap.load( "image/print_32_32png", "PNG", QPixmap::Auto );
	//printRecords->setPixmap( printTablePixmap );
	connect( printRecords, SIGNAL( clicked() ), object, SLOT( printRecords_Slot() ) ); // ������������� ������ �������� � ������� ��� ���������� 
	// 
	closeButton = new QPushButton( tr("Close"), reportForm );
	closeButton->setGeometry( 5,
		20 + 180 + pushButtonSize.height() + 11 + 8 + pushButtonSize.height() + 11 + 8 + pushButtonSize.height() + 148,
		pushButtonSize.width() - 11,
		pushButtonSize.height() + 16 );
	//icon.setPixmap( "image/cancel_32_32_bigpng", QIcon::Large, QIcon::Normal, QIcon::On );
	//closeButton->setIcon( icon );
	connect( closeButton, SIGNAL( clicked() ), object, SLOT( hideReportsWindow() ) ); // ������� ����  ������� 
	closeButton->hide();

	buttonPhotoSaveAll = new QPushButton(tr("Save all\nphotos"), reportForm);
	buttonPhotoSaveAll->setObjectName("buttonPhotoSaveAll");
	buttonPhotoSaveAll->setGeometry( 5,
		20 + 48 +  180 + pushButtonSize.height() + 11 + 8 + pushButtonSize.height() + 11 + 8 + pushButtonSize.height() + 148,
		pushButtonSize.width() - 11,
		pushButtonSize.height() + 16 );
	//buttonPhotoSave->setPixmap( pixmap );
	 QObject::connect( buttonPhotoSaveAll, SIGNAL( clicked() ), object, SLOT( savePhotoReportsTo_HDD() ) );
	buttonPhotoSaveAll->hide();

	buttonSendReportEmail = new QPushButton(tr("Send\ne-mail"), reportForm );
	buttonSendReportEmail->setGeometry( 5,		
		20 + 180 + pushButtonSize.height() + 11 + 8 + pushButtonSize.height() + 11 + 8 + pushButtonSize.height() + 11 + 8,
		pushButtonSize.width() - 11,
		pushButtonSize.height() + 16 );

	checkBoxPhotoSaveAll = new QCheckBox( tr("Save all photos"), reportForm );
	checkBoxPhotoSaveAll->setGeometry( 5, 20 + 180 + (pushButtonSize.height() + 11 + 8) * 3 + 44, 160, 24 );
	//connect( checkBoxPhotoSaveAll, SIGNAL( stateChanged( int ) ), this, SLOT( checkBoxPhotoSaveAll_Slot( int ) ) );
	//checkBoxPhotoSaveAll->hide();

	// icon.setPixmap( "image/cancel_32_32_bigpng", QIcon::Large, QIcon::Normal, QIcon::On );
	// buttonSendReportEmail->setIcon( icon );
	
	//connect( buttonSendReportEmail, SIGNAL( clicked() ), this, SLOT( buttonSendReportEmail_Slot() ) ); // ��������� �� e-mail ������� �� ������� ������� 
	connect( buttonSendReportEmail, SIGNAL( clicked() ), this, SLOT( startThreadSendEmail_Slot() ) ); // ��������� �� e-mail ������� �� ������� ������� 

	// 
	reportsShowValidation = new QLabel( "", sortGroupBox );
	reportsShowValidation->setGeometry( sortGroupBox->width() / 3, 10, 500, 18 );
	// reportsShowValidation->setStyleSheet( "margin: 10px 0px;" );
	reportsShowValidation->hide();
	// 
	// ��������� ������� ������ �� ������� ������� 
	loadIndicatorReports = new QLabel( reportForm );
	loadIndicatorReports->setGeometry( 568 + 180, 430, 100, 48 );
	loadIndicatorReports->setStyleSheet( "font-size: 32px; border: 3px solid #004B7F;" );
	loadIndicatorReports->hide();

	loadProgressBarReports = new QProgressBar( reportForm );
	loadProgressBarReports->setGeometry( 460 + 180, 530, 320, 23 );
	loadProgressBarReports->setTextVisible( false );
	loadProgressBarReports->setValue( 0 );
	loadProgressBarReports->setRange( 0, 100 );
	loadProgressBarReports->setStyleSheet( "border: 1px solid Gray;" );
	loadProgressBarReports->hide();
	// loadProgressBarReports->show();

	opacityEffect = new QGraphicsOpacityEffect();

	// ������ ����� ������� ��� ������� ����������� 
	timerStartFilteringReports = new QTimer( object );
	timerStartFilteringReports->setInterval( 1400 );
	timerStartFilteringReports->setSingleShot( true );
	bool flag12 = connect( timerStartFilteringReports, SIGNAL( timeout() ), object, SLOT( timerStartFilteringReports_Slots() ) );

	reportForm->hide();
    enableDisableControls();

	bool flag1 =  QObject::connect(&threadSavePhotos, SIGNAL(started()), this, SLOT(buttonSendReportEmail_Slot() ));
    bool flag2 =  QObject::connect(this, SIGNAL(finishedSavePhotos()), &threadSavePhotos, SLOT(quit()));

	//  ���� ������  ���������� ��������� �� ������� A4
	dialonNumNakladn = new QDialog();
	dialonNumNakladn->setWindowTitle( tr("The number of prints on the A4") );
	dialonNumNakladn->setModal( true );
	dialonNumNakladn->resize( 212, 93 );
	dialonNumNakladn->setFixedSize( 212, 93 );

	QLabel *labelNumNalk = new QLabel( tr("Set number"), dialonNumNakladn );
	labelNumNalk->setGeometry( 10, 10, 180, 18 );

	selectNumNakl = new QComboBox( dialonNumNakladn );
	selectNumNakl->setGeometry( 5, 33, 195, 24 );

	selectNumNakl->addItem( "1" );
	selectNumNakl->addItem( "2" );
	selectNumNakl->addItem( "3" );
	selectNumNakl->addItem( "4" );

	buttonPrint = new QPushButton( tr("Print"), dialonNumNakladn );
	buttonPrint->setGeometry( 5, 59, 195, 32 );
	connect( buttonPrint, SIGNAL( clicked() ), this, SLOT( buttonPrint_Slot() ) );

	dialonNumNakladn->hide();
}
// 
void reportFormClass::showPrintDialog()
{
    dialonNumNakladn->show();
}
// ��������� ���������  ���������� 
void reportFormClass::enableDisableControls()
{
	QGraphicsOpacityEffect *opacityEffect = new QGraphicsOpacityEffect();
	QGraphicsOpacityEffect *opacityEffect2 = new QGraphicsOpacityEffect();

	enterNumAuto->setEnabled( false );
    /*enterNumAuto->setStyleSheet( "background-color: Gray;" );*/
	checkBoxNumAuto->setChecked( false );
    // 
	checkBoxDateBegin->setChecked( false );
    opacityEffect->setOpacity( 0.2 );
	beginCalendar->setGraphicsEffect( opacityEffect );
	beginCalendar->setEnabled( false );
	
	// beginCalendar->hide();
	// 
	checkBoxDateEnd->setChecked( false );
    opacityEffect2->setOpacity( 0.2 );
	endCalendar->setGraphicsEffect( opacityEffect2 );
	endCalendar->setEnabled( false );
    // endCalendar->hide();
	// 
	QTime time1( 0, 0, 0, 0 );
	beginTime->setTime( time1 );
	beginTime->setEnabled( false );
	// 
	QTime time2( 23, 59, 59, 0 );
	endTime->setTime( time2 );
	endTime->setEnabled( false );
    // 
	selectSender->setEnabled( false );
    //
	selectAccepter->setEnabled( false );
	// 
	selectCarrier->setEnabled( false );
	// 
	selectTypeOfGoods->setEnabled( false );
    // 
	selectPayer->setEnabled( false );
	// 
	checkBoxDateBegin->setChecked( false );
    // 
	checkBoxDateEnd->setChecked( false );
    // 
	checkBoxTimeBegin->setChecked( false );
    // 
	checkBoxTimeEnd->setChecked( false );
	// 
	checkBoxCarrier->setChecked( false );
    // 
	checkBoxTypeOfGoods->setChecked( false );
	// 
	checkBoxSender->setChecked( false );
	// 
	checkBoxAccepter->setChecked( false );
	// 
	checkBoxPayer->setChecked( false );
    // 
	reportsShowValidation->hide();
    // 
	comboBoxNumScales->setCurrentItem( 0 ); // ���������� ���� �1 �� ��������� 
}
// ��������� ���������  ���������� 
void reportFormClass::disableControls()
{
}
// ���������� �������� // "�� ������"
void reportFormClass::checkBoxNumAutoSlot( int state )
{
    if( state == 0 )
	{
		enterNumAuto->setEnabled( false );
		//enterNumAuto->setStyleSheet( "background-color:Gray;" ); 
	}
	else
	{
		enterNumAuto->setEnabled( true );
		//enterNumAuto->setStyleSheet( "background-color: #fff;" );
	}
}
// 
void reportFormClass::checkBoxDateBeginSlot( int state )
{
    QGraphicsOpacityEffect *opacityEffect = new QGraphicsOpacityEffect();

	if( state == 0 )
	{
	    opacityEffect->setOpacity( 0.2 );
	    beginCalendar->setGraphicsEffect( opacityEffect );
	
		beginCalendar->setEnabled( false ); // hide();
	}
	else
	{
	    opacityEffect->setOpacity( 1.0 );
	    beginCalendar->setGraphicsEffect( opacityEffect );

		beginCalendar->setEnabled( true ); // show();
	}
}
// 
void reportFormClass::checkBoxDateEndSlot( int state )
{
    QGraphicsOpacityEffect *opacityEffect = new QGraphicsOpacityEffect();

	if( state == 0 )
	{
		opacityEffect->setOpacity( 0.2 );
	    endCalendar->setGraphicsEffect( opacityEffect );

		endCalendar->setEnabled( false );  // hide();
	}
	else
	{
        opacityEffect->setOpacity( 1.0 );
	    endCalendar->setGraphicsEffect( opacityEffect );

		endCalendar->setEnabled( true );  // show();
	}
}
// 
void reportFormClass::checkBoxTimeBeginSlot( int state )
{
	QTime time( 0, 0, 0, 0 );
	beginTime->setTime( time );

	if( state == 0 )
	{
		beginTime->setEnabled( false );
	}
	else
	{
		beginTime->setEnabled( true );
	}
}
// 
void reportFormClass::checkBoxTimeEndSlot( int state )
{
	QTime time( 23, 59, 59, 0 );
	endTime->setTime( time );

	if( state == 0 )
	{
		endTime->setEnabled( false );
	}
	else
	{
		endTime->setEnabled( true );
	}
}
// 
void reportFormClass::checkBoxSenderSlot( int state )
{
	if( state == 0 )
	{
		selectSender->setEnabled( false );
	}
	else
	{
		selectSender->setEnabled( true );
	}
}
// 
void reportFormClass::checkBoxAccepterSlot( int state )
{
	if( state == 0 )
	{
		selectAccepter->setEnabled( false );
	}
	else
	{
		selectAccepter->setEnabled( true );
	}	
}
// 
void reportFormClass::checkBoxCarrierSlot( int state )
{
	if( state == 0 )
	{
		selectCarrier->setEnabled( false );
	}
	else
	{
		selectCarrier->setEnabled( true );
	}
}
// 
void reportFormClass::checkBoxTypeOfGoodsSlot( int state )
{
	if( state == 0 )
	{
		selectTypeOfGoods->setEnabled( false );
	}
	else
	{
		selectTypeOfGoods->setEnabled( true );
	}
}
// 
void reportFormClass::checkBoxPayerSlot( int state )
{
    if( state == 0 )
	{
		selectPayer->setEnabled( false );
	}
	else
	{
		selectPayer->setEnabled( true );
	}
}
// ������ �������� ������� ������� 
QList<QString> reportFormClass::getColumnsHeadersTableAutoReports()
{
	QList<QString> result = QList<QString>();
	result.append( tr("Vehicle\nnumber") );
	result.append( tr("Sender") );
	result.append( tr("Recipient") );
	result.append( tr("Payer") );
	result.append( tr("Carrier") );
	result.append( tr("Cargo") );
    result.append( tr("Cargo value") );
	result.append( tr("Gross") );
	result.append( tr("Tare") );
	result.append( tr("Net") );
	result.append( tr("Datetime\naccept") ); // ���� ������ 
    result.append( tr("Accept\nuser") ); 
	result.append( tr("Datetime\nsend") );
    result.append( tr("Sender\nuser") ); 
	result.append( tr("Waybill") );
	result.append( tr("Trailer\nnumber") );
	result.append( tr("Gross\n(trailer)") );
	result.append( tr("Tare\n(trailer)") );
	result.append( tr("Net\n(trailer)") );
	result.append( tr("Photo") );
	result.append( tr("Save") );
	result.append( tr("Tara\nenter") );

	return result;
}

// 
void reportFormClass::buttonPrint_Slot()
{
	bool result;
	QSqlError error;
	QMessageBox mess;
	QString html;

	QRect screenResolution = qApp->desktop()->screenGeometry();
	QPrinter printer( QPrinter::HighResolution );

	printer.setPaperSize( QSize( 210, 297 ), QPrinter::Millimeter );
	printer.setOrientation( QPrinter::Portrait );
	printer.setFullPage( true );
	printer.setPageMargins( 8, 8, 8, 8, QPrinter::Millimeter );

	QWebView *webView = new QWebView; // ������ ��� �������� ������ 
	webView->setStyleSheet( "background-color: #fff;" );

	QPrintPreviewDialog preview( &printer );
	preview.setWindowFlags( Qt::Window );

	QObject::connect( &preview, SIGNAL( paintRequested( QPrinter* ) ), webView, SLOT( print( QPrinter* ) ) );

	preview.setModal( true );
	preview.setOrientation( Qt::Horizontal );
	preview.setMinimumSize( screenResolution.width(), screenResolution.height() ); // ���������� �� ���� ������ ������ 
	preview.setWindowTitle( tr ( "Preview" ) ); //��������������� ��������

	QString str = tableReports->item( row, 10 )->text();
	QDateTime dateTime = QDateTime::fromString( str, "dd.MM.yyyy hh:mm:ss" );
	str = dateTime.toString( "yyyy-MM-dd hh:mm:ss" );
	
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

	result = query.exec( QString( "SELECT * FROM ReportsWeight WHERE Date_Time_Accept='%1';" ).arg( str ) );
	error = query.lastError();

	int cnt = query.record().count();
	int i = 0;
	QList<QString> data;

	while( query.next() )
	{
	    while( i < cnt )
		{
		    if( i + 3 == DATETIME_REPORTS_COLUMN1 )
				data.append( query.value( i + 3 ).asDateTime().toString( "dd.MM.yyyy hh:mm:ss" ) ); // ������ �������/���� ������ ���������� �� ���������� 
			else
			if( i + 3 == DATETIME_REPORTS_COLUMN2 )
				data.append( query.value( i + 3 ).asDateTime().toString( "dd.MM.yyyy hh:mm:ss" ) ); // ������ �������/���� �������� ���������� � ���������� 
			else
				data.append( query.value( i + 3 ).toString() );

			i++;
        }
	}

	float weight = ( data.at( 9 ) != "0" ? ( data.at( 9 ).toDouble() / 1000.0 ) : ( data.at( 18 ).toDouble() / 1000.0 ) ); // ��� ����� 
	const float NDS = 0.2; // ��� = 20%
	float prisWithNDS = ( float )( ( weight * data.at( 6 ).toFloat() * NDS ) + ( weight * data.at( 6 ).toFloat() ) );

	// NDS

	int num_nakl = selectNumNakl->currentText().toInt();


    html.append( "<html style=\"background-color: #fff;\"><body>" );

	for( int i = 0; i < num_nakl; i++ )
	{
		html.append( QString( "<span style='text-align: left; font-size: 18px; '><a><b>") + 
			tr("Waybill") + //���������
			QString(" �:</b> %1  ��  %2�.          </a></span><br />" ).arg( data.at( 14 ) ).arg( data.at( 12 ).split( ' ' )[0] ) +
			QString(" <span style='text-align: left; font-size: 18px; '><a><b>" ) +
			tr("Sender") + 
			QString(":  </b> \"%1\"</a></span><br />" ).arg( data.at( 1 ) ) +
			QString(" <span style='text-align: left; font-size: 18px; '><a><b>" ) + 
			tr("Payer") + 
			QString(":   </b> \"%1\"</a></span><br />" ).arg( data.at( 3 ) ) +
			QString(" <span style='text-align: left; font-size: 18px; '><a><b>") + 
			tr("Carrier") + 
			QString(":   </b> \"%1\"</a></span><br />" ).arg( data.at( 4 ) ) +
			QString(" <span style='text-align: left; font-size: 18px; '><a><b>") +
			tr("Recipient") + 
			QString(":   </b> \"%1\"</a></span><br />" ).arg( data.at( 2 ) ) +
			QString(" <span style='text-align: left; font-size: 18px; '><a><b>" ) + 
			tr("Vehicle number") +
			QString(":  </b> %1</a></span>" ).arg( ( data.at( 0 ).isEmpty() ? data.at( 15 ) : data.at( 0 ) ) ) +
			QString(" <h1 style=\"text-align: center; font-size: 18px; font-weight: bold;\">") + 
			tr("Shipment details") + //������ ��������
			QString("</h1> <table border='1'cellspacing='0' cellpadding='4' width=100% style='text-align: center; font-size: 18px;"
			"font-weight: bold; border: 3px solid #000; width: 1000px; margin-right: 0px;'>    <tr><td>" ) + 
			tr("Name") + //������������
			QString("</td><td>") + 
			tr("Unit") + //��.���.
			QString("</td><td>") + 
			tr("Count") + //���-��
			QString("</td><td>") +
			tr("Price, uah") + //����, ���
			QString("</td><td>") + 
			tr("Sum, uah") + //�����, ���
			QString("</td><td>") + 
			tr("Sum +VAT, uah") + //����� c ���, ���
			QString("</td></tr>   <tr><td>%1</td><td>%2</td><td>%3</td><td>%4</td><td>%5</td><td>%6</td></tr>")
			.arg( data.at( 5 ) )
			.arg( tr("tons") ) //�����
			.arg( ( float )( weight ) )
			.arg( data.at( 6 ) )
			.arg( ( float )( weight * data.at( 6 ).toFloat() ) )
			.arg( prisWithNDS ) +
			QString( " </table><br /><div>") +
			tr("VAT") + //���
			QString("=%1%</div><p>").arg( NDS * 100 ) ) +
			tr("Accept user") +  // ������
			QString("________________________<b>%1</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ").arg( data.at( 11 ) ) +
			tr("Sender user") +  //��������
			QString("________________________<b>%1</b></p><br />" ).arg( data.at( 13 ) );
	}
	html.append( "</body></html>" );

	webView->setHtml( html );

	preview.exec();
	// 
	dialonNumNakladn->hide();
	webView->deleteLater();
	row = -1;
}

// 
void reportFormClass::buttonSendReportEmail_Slot()
{
	if( tableReports->rowCount() == 0 )
	{
		QMessageBox::critical( NULL, 
			tr("Report"),
			tr("No data to e-mail"), QMessageBox::Ok );
		return;
	}

	QSettings settings_file( "settings.ini", QSettings::IniFormat );
    settings_file.setIniCodec("Windows-1251");

	const int line_style = 1;
    const int line_width = 2;

	QList<QVariant> row;
    bool flag = false;
	int i = 0;
	int j = 0;

	// �������  �������� Excel 
	QAxObject *mExcel = new QAxObject( "Excel.Application" ); //�������� ����������� �� excel
	mExcel->setProperty( "DisplayAlerts", 0 ); // ��������� �������������� ��  ������� 
	QAxObject *workBooks = mExcel->querySubObject( "Workbooks" );

	if( workBooks == 0x00 )
	{
		// "�� Microsoft Excel �� �����������!"
		workBooks->deleteLater();
		mExcel->deleteLater();

		return;
	}

	QAxObject *workBook = workBooks->querySubObject( "Add" ); // �������� ���� Excel
	QAxObject *sheets = workBook->querySubObject( "Worksheets" );
	QAxObject *sheet1 = sheets->querySubObject( "Item( int )", 1 );

	// ������� ����� �������� �� "����� �������"
	QAxObject *rangeHeaders = sheet1->querySubObject( "Range(const QString&, const QString&)", QString("A%1").arg( 1 ), QString("S%1").arg( 1 ) );
	for( int h = 0; h < tableReports->columnCount() - 2; h++ )
	{
		row.append( tableReports->horizontalHeaderItem( h )->text() );
	}

	QAxObject *razmer = rangeHeaders->querySubObject( "Rows" ); //������� ��������� �� ������
	razmer->setProperty( "ColumnWidth", 20 ); // ������������ � ������.
	razmer->setProperty( "HorizontalAlignment", -4108 ); // ������������ � ������.

	QAxObject *borderRight = rangeHeaders->querySubObject("Borders(xlEdgeRight)" );
	borderRight->setProperty( "LineStyle", line_style ); //��� ����� (��� ��������, �������� � ��� �����)
	borderRight->setProperty( "Weight", line_width ); //�������

	QAxObject* interior = rangeHeaders->querySubObject("Interior");

	// ������������� ����
	interior->setProperty("Color",QColor("yellow"));

	rangeHeaders->setProperty("Value", QVariant( row ) );

	QList< int > idf_list;
	QStringList fileNames;
	idf_list.clear();
	fileNames.clear();

	// ������ ������  �  �������  Excel
	row.clear();
	while( j < tableReports->rowCount() )
	{
		QAxObject *range = sheet1->querySubObject( "Range(const QString&, const QString&)", QString("A%1").arg( j + 2 ), QString("S%1").arg( j + 2 ) );

		while( i < tableReports->columnCount() - 3 ) 
		{
			if( i == 7 || i == 8 || i == 9 || i == 16 || i == 17 || i == 18 )
			{
				QString temp = tableReports->item( j, i )->text();

				if( temp.contains( '.' ) )
					row.append( temp.replace( '.', ',' ) );
				else
					row.append( temp );
			}
			else
			if( i == 10 )
			{
				if( !settings->enterExcelDateTimeFormat->text().isEmpty() )
				{
					QAxObject *cell = range->querySubObject( "Range(const QVariant&)", "K1" );
					cell->setProperty( "NumberFormat", settings_file.value( "excel/date_time_format" ).toString() ); // "��.��.���� �:��"
				}
				row.append( tableReports->item( j, i )->text() );
			}
			else
			if( i == 12 )
			{
				if( !settings->enterExcelDateTimeFormat->text().isEmpty() )
				{
					QAxObject *cell = range->querySubObject( "Range(const QVariant&)", "M1" );
					cell->setProperty( "NumberFormat", settings_file.value( "excel/date_time_format" ).toString() );
				}
				row.append( tableReports->item( j, i )->text() );
			}
			else
			{
				row.append( tableReports->item( j, i )->text() );
			}

			i++;
		}

		QString id1 = tableReports->item( j, i )->text();
		QString idf1 = tableReports->item( j, i + 1 )->text();
		idf_list.append( idf1.toInt() );
		
		QString str = row.at( 10 ).toString();
		QDateTime dateTime = QDateTime::fromString( str, "dd.MM.yyyy hh:mm:ss" );
		str = dateTime.toString( "yyyy-MM-dd hh:mm:ss" );	
		QString fileName1 = QString( "%1_%2_%3_�%4.jpg" ).arg( str.replace( '-', "_" ).replace( ':', "_" ) ).arg( row.at( 14 ).toString() ).arg( row.at( 0 ).toString() ).arg( j + 1 );
		fileNames.append( fileName1 ); // file_name

		range->setProperty("Value", QVariant( row ) );
		row.clear();
		j++;

		i = 0;
	}
	
	if (checkBoxPhotoSaveAll->isChecked())
	{
		mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );
		buttonSendReportEmail->blockSignals( true );
		mysqlProccessObject->getPhotosbyIdAndSave( idf_list, fileNames, false );
		buttonSendReportEmail->blockSignals( false );
	}
	mExcel->setProperty( "Visible", false); // ��������� ���������� �������� Excel

	// ��������� � ���� ��� �������� �� e-mail
	QString filePath;
	QString path = "hourly";

	QDir dir( path );
	if( !dir.exists() )
		dir.mkdir( dir.absolutePath() );

	QString fileName = "Report.xls";
				
	filePath.append( QString( dir.absolutePath().replace( "/", "\\"  ) + "\\" + fileName ) );

	QFile file( filePath );
	file.open( QIODevice::ReadWrite );
	if( file.exists() )
	{
		file.remove();
	}
	file.close();
			
	QVariant var = workBook->dynamicCall( "SaveAs(const QString&, int)", filePath, -4143 );
	workBook->dynamicCall( "Close (Boolean)", false );

	QStringList sl;
	sl.append( filePath );
/*
	emit loadMailData_Signal( true, settings_file.value( "mail/host" ).toString(), settings_file.value( "mail/port" ).toInt(), 
		settings_file.value( "mail/login" ).toString(), settings_file.value( "mail/password" ).toString(), settings_file.value( "mail/ssl" ).toString(), 
		settings_file.value( "mail/mailToSend" ).toString(), tr("Weight mail"), tr("Report - ") + QString( "%1" ).
		arg( QDateTime::currentDateTime().toString( "dd.MM.yyyy hh:mm:ss" ) ), 
		false, "", sl );
*/
	MailSend mail( "�����" );

	mail.moveToThread(&threadSavePhotos);
	
	mail.SendMail(filePath);

	emit finishedSavePhotos();

	// delete range;
	delete sheet1;
	delete sheets;
	delete workBook;
	delete workBooks;
	delete mExcel;
}
 
void reportFormClass::startThreadSendEmail_Slot()
{
	if( ! threadSavePhotos.isRunning() ) 
		threadSavePhotos.start();
}

// 
reportFormClass::~reportFormClass()
{
}