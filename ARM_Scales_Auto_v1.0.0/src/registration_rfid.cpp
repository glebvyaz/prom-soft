#include "stdafx.h"
#include "registration_rfid.h"
#include "ui_registration_rfid.h"
//#include <qtextcodec.h>
//#include <qlineedit.h>
//#include <qlabel.h>
#include <QMessageBox>
#include "db_driver.h"
#include "loginWindow.h"

//extern mysqlProccess *mysqlProccessObject;
extern loginWindowClass *loginWindowObject;

registration_rfid::registration_rfid(QWidget *parent) :
    QDialog(parent),
		ui(new Ui::registration_rfid )
{
	textCodec = QTextCodec::codecForName("Windows-1251");

    ui->setupUi(this);
    setupParamsAndSlots();

	ui->btnAdd->setIconSize( QSize( 43, 43 ) );
	ui->btnAdd->setIcon( QPixmap( "Resources/images/add.png", "PNG" ) );

	ui->btnModify->setIconSize( QSize( 34, 34 ) );
	ui->btnModify->setIcon( QPixmap( "Resources/images/edit.svg", "SVG" ) );

	ui->btnRemove->setIconSize( QSize( 51, 51 ) );
	ui->btnRemove->setIcon( QPixmap( "Resources/images/remove.png", "PNG" ) );

	ui->btnClose->setIconSize( QSize( 41, 41 ) );
	ui->btnClose->setIcon( QPixmap( "Resources/images/close.png", "PNG" ) );

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	mysqlProccessObject->getRegistrationData( getHeadersRegistration(), ui->tableViewRegistration, QList< QString >() );
	dlgRegister = nullptr;
}

// 
void registration_rfid::getRegistrationContent()
{
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	mysqlProccessObject->getRegistrationData( getHeadersRegistration(), ui->tableViewRegistration, QList< QString >() );
}
// 
void registration_rfid::setupParamsAndSlots()
{
	connect( ui->tableViewRegistration, SIGNAL( clicked( const QModelIndex & ) ), this, SLOT( cellClicked_tableSpravochniki_Slot( const QModelIndex & ) ) );

	connect( ui->btnClose, SIGNAL( clicked() ), this, SLOT( hide() ) );
    connect( ui->btnAdd, SIGNAL( clicked() ), this, SLOT( addRecord_Slot() ) );
	connect( ui->btnModify, SIGNAL( clicked() ), this, SLOT( modifyRecord_Slot() ) );
	connect( ui->btnRemove, SIGNAL( clicked() ), this, SLOT( removeRecord_Slot() ) );
	connect( ui->tabsSpravochniki, SIGNAL( currentChanged( int ) ), this, SLOT( tabChanged_Slot( int ) ) );
	tabChanged_Slot( 0 );
	clearIndexes();
}

// 
void registration_rfid::fillAcceptForm()
{
}

// 
void registration_rfid::showForm()
{
	this->show();
}

// 
void registration_rfid::closeForm()
{
	NumAuto = "";
	this->hide();
}

// 
void registration_rfid::addRecord_Slot()
{
	addModifyRecords( true, "" );
}

// 
void registration_rfid::modifyRecord_Slot()
{
	if( this->row == -1 )
	{
		QMessageBox::information( NULL, textCodec->toUnicode( "��������� ������" ), textCodec->toUnicode( "�������� ������ ��� ���������!" ), QMessageBox::Cancel );
		return;
	}

	ui->tableViewRegistration->selectRow( this->row );

	QSettings settingsParams( "settings.ini", QSettings::IniFormat );
	settingsParams.setIniCodec( "Windows-1251" );

    settingsParams.setValue( "register_data/supplier", ui->tableViewRegistration->model()->index( this->row, 2 ).data().toString() ); // ��������� 
	settingsParams.setValue( "register_data/material", ui->tableViewRegistration->model()->index( this->row, 3 ).data().toString() ); // �������� 
	settingsParams.setValue( "register_data/num_auto", ui->tableViewRegistration->model()->index( this->row, 4 ).data().toString() ); // ����� ����������
	settingsParams.setValue( "register_data/num_prizep", ui->tableViewRegistration->model()->index( this->row, 5 ).data().toString() ); // ����� ����������
	settingsParams.setValue( "register_data/fio", ui->tableViewRegistration->model()->index( this->row, 6 ).data().toString() ); // ��� 
	settingsParams.setValue( "register_data/num_card", ui->tableViewRegistration->model()->index( this->row, 7 ).data().toString() ); // ���������� ����� 


	addModifyRecords( false, ui->tableViewRegistration->model()->index( this->row, 1 ).data().toString() ); // code
	clearIndexes();
}

// 
void registration_rfid::removeRecord_Slot()
{
	QTextCodec *textCodec = QTextCodec::codecForName("Windows-1251");

	int row;
	int column;
	int result;
	QString date_time;

	if( loginWindowObject->role != loginWindowObject->ADMIN )
	{
		QMessageBox::critical( NULL, textCodec->toUnicode( "�������� ������ �� ������������" ), 
            textCodec->toUnicode( "� ��� �� ����, ��� �������� ������ �� ������������!" ),
			QMessageBox::Ok );
		
		return;
	}

	if( this->row == -1 )
	{
		QMessageBox::information( NULL, textCodec->toUnicode( "��������� ������" ), textCodec->toUnicode( "�������� ������ ��� ��������!" ), QMessageBox::Cancel );
		return;
	}

	ui->tableViewRegistration->selectRow( this->row );

	result = QMessageBox::question( NULL, textCodec->toUnicode( "�������� ������" ), textCodec->toUnicode( "�� ������������� ������ ������� ������ �� �����������?" ), QMessageBox::Ok, QMessageBox::Cancel );

	if( result == QMessageBox::Cancel )
		return;

	getIndexes( &row, &column );
	
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QString dateTime = QDateTime::fromString( ui->tableViewRegistration->model()->index( this->row, 0 ).data().toString(), "dd.MM.yyyy hh:mm:ss" ).toString( "yyyy-MM-dd hh:mm:ss" );
	if( true == mysqlProccessObject->removeRecordFromSpravochniki( dateTime ) );
	{
		QMessageBox::information( NULL, textCodec->toUnicode( "�������� ������" ), textCodec->toUnicode( "������ �� ����������� ������� �������!" ), QMessageBox::Ok );
	}
	
	mysqlProccessObject->saveEventToLog( textCodec->toUnicode( "�� ����������� ������� ������." ) );

	clearIndexes();
	mysqlProccessObject->getRegistrationData( getHeadersRegistration(), ui->tableViewRegistration, QList< QString >() );
}

// 
void registration_rfid::addModifyRecords( bool save, QString code )
{
	bool flag = false;
	QSettings settingsParams( "settings.ini", QSettings::IniFormat );
	settingsParams.setIniCodec( "Windows-1251" );
	QTextCodec *textCodec = QTextCodec::codecForName("Windows-1251");

	while( true )
	{
		mysqlProccess *mysqlProccessObject = mysqlProccess::instance();

		QDialog dialog;
		dlgRegister = &dialog;
		//dialog.resize( 320, 428 );
		dialog.setFixedSize(320, 430);
		
		if( save )dialog.setWindowTitle( textCodec->toUnicode( "���������� ������." ) );
		else dialog.setWindowTitle( textCodec->toUnicode( "��������� ������." ) );
		// 
		QLabel labelPostavschik( textCodec->toUnicode( "���������" ), &dialog );
		labelPostavschik.setGeometry( 5, 5, 310, 18 );
		labelPostavschik.setAlignment( Qt::AlignCenter );
		labelPostavschik.setStyleSheet( "font-size: 18px; font-weight: bold;" );

		QLineEdit enterPostavschik( &dialog );
		enterPostavschik.setStyleSheet( "font-size: 18px; font-weight: bold;" );
		enterPostavschik.setGeometry( 5, 25, 310, 28 );
		enterPostavschik.setText( settingsParams.value( "register_data/supplier" ).toString() );
		// 
		QLabel labelMaterial( textCodec->toUnicode( "��������" ), &dialog );
		labelMaterial.setGeometry( 5, 55, 310, 18 );
		labelMaterial.setAlignment( Qt::AlignCenter );
		labelMaterial.setStyleSheet( "font-size: 18px; font-weight: bold;" );
		labelMaterial.show();

		QLineEdit enterMaterial( &dialog );
		enterMaterial.setGeometry( 5, 75, 310, 28 );
		enterMaterial.setStyleSheet( "font-size: 18px; font-weight: bold;" );
		enterMaterial.setText( settingsParams.value( "register_data/material" ).toString() );
		enterMaterial.show();
		// 
		QLabel labelNumAuto( textCodec->toUnicode( "����� ����������" ), &dialog );
		labelNumAuto.setGeometry( 5, 105, 310, 18 );
		labelNumAuto.setAlignment( Qt::AlignCenter );
		labelNumAuto.setStyleSheet( "font-size: 18px; font-weight: bold;" );
		labelNumAuto.show();

		QLineEdit enterNumAuto( &dialog );
		enterNumAuto.setObjectName("enterNumAuto");
		enterNumAuto.setGeometry( 5, 125, 310, 28 );
		enterNumAuto.setStyleSheet( "font-size: 18px; font-weight: bold;" );
		enterNumAuto.setText( settingsParams.value( "register_data/num_auto" ).toString() );

		QLabel labelNumPrizep( textCodec->toUnicode( "����� �������" ), &dialog );
		labelNumPrizep.setGeometry( 5, 154, 310, 18 );
		labelNumPrizep.setAlignment( Qt::AlignCenter );
		labelNumPrizep.setStyleSheet( "font-size: 18px; font-weight: bold;" );
		labelNumPrizep.show();

		QLineEdit enterNumPrizep( &dialog );
		enterNumPrizep.setGeometry( 5, 175, 310, 28 );
		enterNumPrizep.setStyleSheet( "font-size: 18px; font-weight: bold;" );
		enterNumPrizep.setText( settingsParams.value( "register_data/num_prizep" ).toString() );

		if( save == false )
			enterNumAuto.setReadOnly( true );
		else
			enterNumAuto.setReadOnly( false );


		connect( &enterNumAuto, SIGNAL( textChanged( const QString & ) ), this, SLOT( autoNumEnter_Slot( const QString & ) ) );

		enterNumAuto.show();
		// 
		QLabel labelFIO( textCodec->toUnicode( "�.�.�. ��������" ), &dialog );
		labelFIO.setGeometry( 5, 204, 310, 18 );
		labelFIO.setAlignment( Qt::AlignCenter );
		labelFIO.setStyleSheet( "font-size: 18px; font-weight: bold;" );
		labelFIO.show();

		QLineEdit enterFIO( &dialog );
		enterFIO.setGeometry( 5, 225, 310, 28 );
		enterFIO.setStyleSheet( "font-size: 18px; font-weight: bold;" );
		enterFIO.setText( settingsParams.value( "register_data/fio" ).toString() );
		enterFIO.show();

		QLabel labelNumOrder( textCodec->toUnicode( "���������� ����� �����" ), &dialog );
		labelNumOrder.setGeometry( 5, 254, 310, 18 );
		labelNumOrder.setAlignment( Qt::AlignCenter );
		labelNumOrder.setStyleSheet( "font-size: 18px; font-weight: bold;" );
		labelNumOrder.show();

		QLineEdit enterNumOrder( &dialog );
		enterNumOrder.setGeometry( 5, 275, 310, 28 );
		enterNumOrder.setStyleSheet( "font-size: 18px; font-weight: bold;" );
		enterNumOrder.setText( settingsParams.value( "register_data/num_card" ).toString() );
		enterNumOrder.show();

		QCheckBox chkBoxAutoTara(textCodec->toUnicode("���� �� �����������"), &dialog);
		chkBoxAutoTara.setObjectName("chkBoxAutoTara");
		chkBoxAutoTara.setGeometry(5, 309, 180, 20);
		chkBoxAutoTara.setChecked(false);
		connect(&chkBoxAutoTara, SIGNAL(toggled(bool)), this, SLOT(AutoTaraCheck(bool)));

		QPushButton btnOk( textCodec->toUnicode( "�����������" ), &dialog );
		btnOk.setGeometry( 5, 335, 310, 48 );
		connect( &btnOk, SIGNAL( clicked() ), dialog.window(), SLOT( accept() ) );

		QPushButton btnCancel( textCodec->toUnicode( "�������" ), &dialog );
		btnCancel.setGeometry( 5, 381, 310, 48 );
		connect( &btnCancel, SIGNAL( clicked() ), dialog.window(), SLOT( hide() ) );

		ui->enterCode->setText( code );
		int result = dialog.exec();
		if( result == QDialog::Accepted )
		{
			enterNumAuto.setText( convertAutoNumToUpperCase( enterNumAuto.text() ) );
			NumAuto = enterNumAuto.text();

			// save params
			// settingsParams.setValue( "register_data/code", ui->enterCode->text() );
			settingsParams.setValue( "register_data/supplier", enterPostavschik.text() );
			settingsParams.setValue( "register_data/material", enterMaterial.text() );
			settingsParams.setValue( "register_data/fio", enterFIO.text() );
			settingsParams.setValue( "register_data/num_auto", enterNumAuto.text() );
			settingsParams.setValue( "register_data/num_prizep", enterNumAuto.text() );
			settingsParams.setValue( "register_data/num_card", enterNumOrder.text() );

			// validation 
			if( save == true && ui->enterCode->text() == "" )
			{
				QMessageBox::critical( NULL, textCodec->toUnicode( "������ ���������� ������ � �����������" ), textCodec->toUnicode( "��������� ����� � ����������!" ), QMessageBox::Ok );
				continue;
			}

			if( save == true && mysqlProccessObject->hasAutoNumberOrCode( ui->enterCode->text(), enterNumAuto.text() ) )
			{
				QMessageBox::critical( NULL, textCodec->toUnicode( "������ ���������� ������ � �����������" ), textCodec->toUnicode( "����� ����� ���������� ��� ID ����� ��� �������!" ), QMessageBox::Ok );
				settingsParams.setValue( "register_data/num_auto", "" );
				continue;
			}
			/*else if( save == false && mysqlProccessObject->hasNumAuto( enterNumAuto.text() ) )
			{
                QMessageBox::critical( NULL, textCodec->toUnicode( "������ ���������� ������ � �����������" ), textCodec->toUnicode( "����� ����� ���������� ��� �����!" ), QMessageBox::Ok );
				settingsParams.setValue( "register_data/num_auto", "" );
				continue;
			}*/

			if( enterPostavschik.text() == "" )
			{
				QMessageBox::critical( NULL, textCodec->toUnicode( "������ ���������� ������ � �����������" ), textCodec->toUnicode( "������� ����������" ), QMessageBox::Ok );
				continue;
			}

			if( enterMaterial.text() == "" )
			{
				QMessageBox::critical( NULL, textCodec->toUnicode( "������ ���������� ������ � �����������" ), textCodec->toUnicode( "������� ��������" ), QMessageBox::Ok );
				continue;
			}

			if( enterNumAuto.text() == "" )
			{
				QMessageBox::critical( NULL, textCodec->toUnicode( "������ ���������� ������ � �����������" ), textCodec->toUnicode( "������� ����� ����" ), QMessageBox::Ok );
				continue;
			}

			if( enterFIO.text() == "" )
			{
				QMessageBox::critical( NULL, textCodec->toUnicode( "������ ���������� ������ � �����������" ), textCodec->toUnicode( "������� �.�.�. ��������" ), QMessageBox::Ok );
				continue;
			}

			if( enterNumOrder.text() == "" )
			{
				QMessageBox::critical( NULL, textCodec->toUnicode( "������ ���������� ������ � �����������" ), textCodec->toUnicode( "������� ����� �� ������� �����" ), QMessageBox::Ok );
				continue;
			}

			// save data
			if( save == true && mysqlProccessObject->saveToRegisterTable( ui->enterCode->text(), enterPostavschik.text(), enterMaterial.text(), enterNumAuto.text(), enterNumPrizep.text(), enterFIO.text(), enterNumOrder.text(), chkBoxAutoTara.isChecked() ) )
			{
				mysqlProccessObject->getRegistrationData( getHeadersRegistration(), ui->tableViewRegistration, QList< QString >() );

				mysqlProccessObject->saveEventToLog( textCodec->toUnicode( "� ����������� ��������� ������: " ) + textCodec->toUnicode( " ���������=%1, ��������=%2, ����� ����=%3, "
					"����� �������=%4, ���=%5, ����� �����=%6, ��� �����=%7." )
					.arg( enterPostavschik.text() )
					.arg( enterMaterial.text() )
					.arg( enterNumAuto.text() )
					.arg( enterNumPrizep.text() )
					.arg( enterFIO.text() )
					.arg( enterNumOrder.text() )
					.arg( ui->enterCode->text() ) );

				break;
			}
			else if( save == false ) // update data
			{
				QString date_time = getDateTimeOfRecord( ui->tableViewRegistration, this->row, 0 );

				if( mysqlProccessObject->updateRegisterTable( date_time, ui->enterCode->text(), enterPostavschik.text(), enterMaterial.text(), enterNumAuto.text(), enterNumPrizep.text(), enterFIO.text(), enterNumOrder.text(), chkBoxAutoTara.isChecked() ) )
				{
					mysqlProccessObject->getRegistrationData( getHeadersRegistration(), ui->tableViewRegistration, QList< QString >() );

					mysqlProccessObject->saveEventToLog( textCodec->toUnicode( "� ����������� �������� ������: " ) + textCodec->toUnicode( " ���������=%1, ��������=%2, ����� ����=%3, "
					"���=%4, ����� �����=%5,��� �����=%6." )
					.arg( enterPostavschik.text() )
					.arg( enterMaterial.text() )
					.arg( enterNumAuto.text() )
					.arg( enterFIO.text() )
					.arg( enterNumOrder.text() )
					.arg( ui->enterCode->text() ) );

					break;
				}
			}
		}
		else
		if( dialog.result() == QDialog::Rejected )
		{
			qDebug( "---" );
			break;

		}
	}

	// save params
	// settingsParams.setValue( "register_data/code", "" );
	settingsParams.setValue( "register_data/supplier", "" );
	settingsParams.setValue( "register_data/material", "" );
	settingsParams.setValue( "register_data/fio", "" );
	settingsParams.setValue( "register_data/num_auto", "" );
	settingsParams.setValue( "register_data/num_prizep", "" );
	settingsParams.setValue( "register_data/num_card", "" );

    clearIndexes();
}

// 
void registration_rfid::tabChanged_Slot( int tab )
{
	m_current_tab = tab;
}

// 
int registration_rfid::getCurrentTab()
{
	return m_current_tab;
}

// 
void registration_rfid::cellClicked_tableSpravochniki_Slot( const QModelIndex & index )
{
	this->row = index.row();
	this->column = index.column();
}

// 
void registration_rfid::getIndexes( int *row, int *column )
{
	*row = this->row;
	*column = this->column;
}

// 
QString registration_rfid::getDateTimeOfRecord( QTableView *table, int row, int column )
{
	QString date_time;
    date_time = table->model()->index( row, column ).data().toString();
	date_time = QDateTime::fromString( date_time, "dd.MM.yyyy hh:mm:ss" ).toString( "yyyy-MM-dd hh:mm:ss" );
	return date_time;
}

// 
void registration_rfid::clearIndexes()
{
	this->row = -1;
	this->column = -1;
}

// 
void registration_rfid::tabSprav_Gruzootpraviteli()
{
	ui->tabsSpravochniki->setCurrentIndex( OTPRAVITELI );
	showForm();
}

// 
void  registration_rfid::tabSprav_Gruzopoluchateli()
{
	ui->tabsSpravochniki->setCurrentIndex( POLUCHATELI );
	showForm();
}

// 
void registration_rfid::tabSprav_Perevoschiki()
{
	ui->tabsSpravochniki->setCurrentIndex( PEREVOSCHIKI );
	showForm();
}

// 
void registration_rfid::tabSprav_Platelschiki()
{
	ui->tabsSpravochniki->setCurrentIndex( PLATELSCHIKI );
    showForm();
}

// 
void registration_rfid::tabSprav_TypesOfGoods()
{
	ui->tabsSpravochniki->setCurrentIndex( TYPE_GRUZ );
    showForm();
}

// 
void registration_rfid::acceptCodeRfid_Slot( QString code )
{
	QSettings settingsParams( "settings.ini", QSettings::IniFormat );
    settingsParams.setIniCodec( "Windows-1251" );

	getRegistrationContent();

	ui->enterCode->setText( code );

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QList< QString > data = mysqlProccessObject->getRegistrationData( code );

	if( data.size() >= 6 ) // ������ �� ���� ����� ������� 
	{
		settingsParams.setValue( "register_data/supplier", data.at( 1 ) ); // ��������� 
		settingsParams.setValue( "register_data/material", data.at( 2 ) ); // �������� 
		settingsParams.setValue( "register_data/num_auto", data.at( 3 ) ); // ����� ���������� 
		settingsParams.setValue( "register_data/fio", data.at( 4 ) ); // ��� 
		settingsParams.setValue( "register_data/num_card", data.at( 5 ) ); // ���������� ����� 

		if( !this->isActiveWindow() )
			this->show();

		addModifyRecords( false, code ); // update
		clearIndexes();
	}
	else
	{
		if( !this->isActiveWindow() )
			this->show();

		addModifyRecords( true, code ); // save
		clearIndexes();
	}
}

// 
registration_rfid::~registration_rfid()
{
    delete ui;
}

// 
QString registration_rfid::convertAutoNumToUpperCase( QString num_auto )
{
	return num_auto.toUpper();
}

void registration_rfid::registerCard( QString code )
{
	addModifyRecords(false, code);
}

QString registration_rfid::getCurrentNumAuto()
{
	return NumAuto;
}

void registration_rfid::AutoTaraCheck(bool checked)
{
	if (checked)
	{
		//enterNumAuto.text();
		QCheckBox *chkBoxAutoTara = nullptr;
		QLineEdit *editNumAuto = nullptr;
		mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		if (dlgRegister != nullptr)
				editNumAuto = dlgRegister->findChild<QLineEdit*>("enterNumAuto");// enterNumAuto;
		if (editNumAuto != nullptr)
			NumAuto = editNumAuto->text();
		double weight = mysqlProccessObject->getTaraAuto(NumAuto);
		if (weight == -1)
		{
			if (dlgRegister != nullptr)
				chkBoxAutoTara = dlgRegister->findChild<QCheckBox*>("chkBoxAutoTara");// chkBoxTaraAuto;
			
			if (chkBoxAutoTara != nullptr)
			{
				disconnect(chkBoxAutoTara);
				chkBoxAutoTara->setChecked(false);
				
			}
			QMessageBox mess;
			mess.setText(tr("Data for today is not available."));
			mess.setStandardButtons(QMessageBox::Cancel);
			mess.setButtonText(QMessageBox::Cancel, "Ok");
			mess.setIcon(QMessageBox::Warning);
			mess.setWindowTitle(tr("Prom-Soft"));
			mess.exec();
			connect(chkBoxAutoTara, SIGNAL(toggled(bool)), this, SLOT(AutoTaraCheck(bool)));
			return;
		}

		//emit setTaraAutoCheck(true);
		if (chkBoxAutoTara != nullptr)
		{
			disconnect(chkBoxAutoTara);
			chkBoxAutoTara->setChecked(true);
			connect(chkBoxAutoTara, SIGNAL(toggled(bool)), this, SLOT(AutoTaraCheck(bool)));
		}
	}
}

void registration_rfid::autoNumEnter_Slot(const QString & numAuto)
{
	NumAuto = numAuto;
}