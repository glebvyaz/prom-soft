#include "stdafx.h"
#include "numberAndTimeStamp.h"

// 
NumberAndTimestamp::NumberAndTimestamp()
{
	textCodec = QTextCodec::codecForName("Windows-1251");
}

// 
QImage NumberAndTimestamp::updatePhoto( QString dateTime, QImage & image, QString num_auto, int num_pos, QString weight, int num_pos2 )
{
	QByteArray ba;
	QBuffer buffer( &ba );
	buffer.open( QIODevice::WriteOnly );

	QImage imageV;
	imageV = image;

	QPainter p, px;
	QPixmap pix( 150, 50 );
	p.begin( &imageV );		

	px.begin( &pix );
	px.fillRect( pix.rect(), QColor( 255, 255, 255 ) );
	px.drawText( 4, 15, QString( QString( "�����: %1" ).arg( dateTime ) ) );
	px.drawText( 4, 30, QString( QString( "� ����: %1" ).arg( num_auto ) ) );
	if( ! weight.isEmpty() ) px.drawText( 4, 45, QString( QString( "���: %1" ).arg( weight ) ) );
	px.end();

	switch( num_pos )
	{
	    case 0:
		{
			p.drawPixmap( 0, 0, pix.width()*3, pix.height()*3, pix );
		} break;
		case 1:
		{
            p.drawPixmap( 0, imageV.height() - pix.height()*3, pix.width()*3, pix.height()*3, pix );
		} break;
		case 2:
		{
            p.drawPixmap( imageV.width() - pix.width()*3, 0, pix.width()*3, pix.height()*3, pix );
		} break;
		case 3:
		{
			p.drawPixmap( imageV.width() - pix.width()*3, imageV.height() - pix.height()*3, pix.width()*3, pix.height()*3, pix );
		} break;
	}

	p.end();

	imageV.save( &buffer, "JPG" );

	return imageV;
}

// 
NumberAndTimestamp::~NumberAndTimestamp()
{
}