#include "stdafx.h"
#include "DB_Driver.h"
#include "dictionaryForm.h"

dictionaryFormClass::dictionaryFormClass( QObject *object )
{
	QIcon icon;

	QRect screenResolution = qApp->desktop()->screenGeometry();
	const QRect dictionaryWindowSizePos = QRect( 
		( screenResolution.width() / 2 ) - ( screenResolution.width() / 5 ),
		( screenResolution.height() / 6 ) - ( screenResolution.height() / 30 ),
		680, 232 );

	const QSize comboBoxSize = QSize( 465, 32 ); // ������ comboBox - � 
	const QSize lineEditSize = QSize( 81, 32 ); // ������ lineEdit - �
	const QSize pushButtonSize = QSize( 104, 48 ); // ������  pushButton - �
	// 
	dictionaryWindow = new QWidget();
	dictionaryWindow->setGeometry( dictionaryWindowSizePos );
	dictionaryWindow->setFixedWidth( 680 );
	dictionaryWindow->setFixedHeight( 232 );
    dictionaryWindow->setWindowTitle( tr("Dictionaries") ); 
	dictionaryWindow->setWindowModality(  Qt::ApplicationModal );
	dictionaryWindow->setWindowFlags(Qt::FramelessWindowHint); //Set a frameless window 
	dictionaryWindow->setObjectName( "dictionaryWindow" );
	dictionaryWindow->installEventFilter( object );
	// 
	nameOfWindow = new QLabel( tr("Dictionaries"), dictionaryWindow );
	nameOfWindow->setGeometry( dictionaryWindow->width() / 3 + 48, 10, 240, 18 );
	// 
	exitCross = new QPushButton( dictionaryWindow ); // ������ ������ 
	exitCross->setGeometry( dictionaryWindowSizePos.width() / 2 + 95 + 56 + 104 , 5, 32, 32 );
	//icon.setPixmap( "image/exit_crosspng", QIcon::Large, QIcon::Normal, QIcon::On );
	exitCross->setIcon( icon );
	exitCross->setStyleSheet( "color: Green; border: none; background: none; " );
	exitCross->setObjectName( "#exitCross" );
	QObject::connect( exitCross, SIGNAL( clicked() ), object, SLOT( closeDictionaryWindow() ) );
	// 

	const QRect sizePosWin = QRect( 0, 38,
	680, 232 - 38 );

	tabbedWindow = new QTabWidget( dictionaryWindow );
	tabbedWindow->setGeometry( sizePosWin );

	tabbedWindowSenders = new QWidget( tabbedWindow ); 
	tabbedWindowSenders->setGeometry( sizePosWin );
	
	tabbedWindowReceivers = new QWidget( tabbedWindow );
	tabbedWindowReceivers->setGeometry( sizePosWin );

	tabbedWindowPayers = new QWidget( tabbedWindow );
	tabbedWindowPayers->setGeometry( sizePosWin );

	tabbedWindowTransporters = new QWidget( tabbedWindow );
	tabbedWindowTransporters->setGeometry( sizePosWin );

	tabbedWindowGoods = new QWidget( tabbedWindow );
	tabbedWindowGoods->setGeometry( sizePosWin );

	tabbedWindowAuto = new QWidget( tabbedWindow );
	tabbedWindowAuto->setGeometry( sizePosWin );

	tabbedWindowPrizep = new QWidget( tabbedWindow );
	tabbedWindowPrizep->setGeometry( sizePosWin );

	tabbedWindow->addTab( tabbedWindowSenders, tr("Senders") ); 
	tabbedWindow->addTab( tabbedWindowReceivers, tr("Recipients") );
	tabbedWindow->addTab( tabbedWindowPayers, tr("Payers") );
	tabbedWindow->addTab( tabbedWindowTransporters, tr("Carriers") );
	tabbedWindow->addTab( tabbedWindowGoods, tr("Cargos") );
	tabbedWindow->addTab( tabbedWindowAuto, tr("Auto") );
	tabbedWindow->addTab( tabbedWindowPrizep, tr("Prizep") );
	// 
	nameOfEntity = new QLabel( tr("Name:"), tabbedWindow );
	nameOfEntity->setGeometry( 16, 74, 165, 18 );

	validationDictionaries = new QLabel( "", tabbedWindow ); // ��������� ��� ����� ������ � ����������
	validationDictionaries->setGeometry( 10 + 165 + 16, 41, 320, 18 );
	validationDictionaries->setStyleSheet( "color: Red;" );
	validationDictionaries->hide();

	namesSenders = new QComboBox( tabbedWindowSenders ); // "�����������"
	namesSenders->setGeometry( 10 + 165 + 16, 20 + 12, comboBoxSize.width(), comboBoxSize.height() );
	namesSenders->setEditable( true );
	
	namesReceivers = new QComboBox( tabbedWindowReceivers ); // "����������"
	namesReceivers->setGeometry( 10 + 165 + 16, 20 + 12, comboBoxSize.width(), comboBoxSize.height() );
	namesReceivers->setEditable( true );

	namesPayers = new QComboBox( tabbedWindowPayers ); // "�����������"
	namesPayers->setGeometry( 10 + 165 + 16, 20 + 12, comboBoxSize.width(), comboBoxSize.height() );
	namesPayers->setEditable( true );

	namesTransporters = new QComboBox( tabbedWindowTransporters ); // "�����������"
	namesTransporters->setGeometry( 10 + 165 + 16, 20 + 12, comboBoxSize.width(), comboBoxSize.height() );
	namesTransporters->setEditable( true );

	namesGoods = new QComboBox( tabbedWindowGoods ); // "�����"
	namesGoods->setGeometry( 10 + 165 + 16, 20 + 12, comboBoxSize.width(), comboBoxSize.height() );
	namesGoods->setEditable( true );

	namesAuto = new QComboBox( tabbedWindowAuto ); // "����"
	namesAuto->setGeometry( 10 + 165 + 16 , 20 + 12, comboBoxSize.width(), comboBoxSize.height() );
	namesAuto->setEditable( true );

	namesPrizep = new QComboBox( tabbedWindowPrizep ); // "������"
	namesPrizep->setGeometry( 10 + 165 + 16 , 20 + 12, comboBoxSize.width(), comboBoxSize.height() );
	namesPrizep->setEditable( true );
	// 
	attToDB_Button = new QPushButton( tr("Add"), dictionaryWindow );
	attToDB_Button->setGeometry( 20, dictionaryWindowSizePos.height() / 10 + lineEditSize.height() + 100, pushButtonSize.width() + 64 , pushButtonSize.height() );
	//QPixmap addDataBasePixmap;
    //addDataBasePixmap.load( "image/add-to-database_32_32png", "PNG", QPixmap::Auto );
	//attToDB_Button->setPixmap( addDataBasePixmap );
	connect( attToDB_Button, SIGNAL( clicked() ), object, SLOT( addToDB_Slot() ) );
	// 
    deleteRecord = new QPushButton( tr("Delete record"), dictionaryWindow );
	deleteRecord->setGeometry( 20 + pushButtonSize.width() + 64 + 16, dictionaryWindowSizePos.height() / 10 + lineEditSize.height() + 100, pushButtonSize.width() + 64 , pushButtonSize.height() );
	//QPixmap removeDataBaseRecordPixmap;
    //removeDataBaseRecordPixmap.load( "image/remove-from-database_32_32png", "PNG", QPixmap::Auto );
	//deleteRecord->setPixmap( removeDataBaseRecordPixmap );
	connect( deleteRecord, SIGNAL( clicked() ), object, SLOT( deleteRecordDB_Slot() ) );
	// 
    cancelButton = new QPushButton( tr("Cancel"), dictionaryWindow );
	cancelButton->setGeometry( 20 + pushButtonSize.width() + 64 + 16 + pushButtonSize.width() + 64 + 16 , dictionaryWindowSizePos.height() / 10 + lineEditSize.height() + 100, pushButtonSize.width() + 72 ,pushButtonSize.height() );
	//icon.setPixmap( "image/cancel_32_32_bigpng", QIcon::Large, QIcon::Normal, QIcon::On );
	//cancelButton->setIcon( icon );
	connect( cancelButton, SIGNAL( clicked() ), object, SLOT( closeDictionaryWindow() ) );
	// 
	//  ������ ��
	dataBaseIcon = new QLabel( dictionaryWindow );
	dataBaseIcon->setGeometry( 20 + pushButtonSize.width() + 64 + 16 + pushButtonSize.width() + 64 + 16 + pushButtonSize.width() + 64 + 28 , dictionaryWindowSizePos.height() / 10 + lineEditSize.height() + 86, 72, 72 );
	//QPixmap pixmapDataBase;
	//pixmapDataBase.load( "image/database_logo_72_72png", "PNG", QPixmap::Auto );
	//dataBaseIcon->setPixmap( pixmapDataBase );
	// 
	current_tab = 0; // ���������� ����� ������� ������� = 0
	connect( tabbedWindow, SIGNAL( currentChanged( int ) ), object, SLOT( currDictionaryChanged( int ) ) ); // ����� ����� ��� ����� ������� ����������� 
}

void dictionaryFormClass::showDictionaryWindow()
{
	dictionaryWindow->show();
}

// Abort validation
void dictionaryFormClass::resetValidation()
{
	validationDictionaries->hide();
	validationDictionaries->setStyleSheet( "color: Red;" );
	
	namesAuto->setStyleSheet( "border: 1px solid #000;" );
	namesGoods->setStyleSheet( "border: 1px solid #000;" );
	namesPayers->setStyleSheet( "border: 1px solid #000;" );
	namesReceivers->setStyleSheet( "border: 1px solid #000;" );
	namesSenders->setStyleSheet( "border: 1px solid #000;" );
	namesTransporters->setStyleSheet( "border: 1px solid #000;" );
}

// Destr
dictionaryFormClass::~dictionaryFormClass()
{
}