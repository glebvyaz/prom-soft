#include "include/reports_form.h"
#include "ui_reports_form.h"
#include "include/db_driver.h"
#include "include/spravochniki_form.h"
#include "include/Form_ttn.h"
#include "include/dialogPreview.h"
#include "include/loginWindow.h"

#include <ActiveQt/qaxobject.h>
#include <ActiveQt/qaxbase.h>

#define NUM_PHOTO_CELL 11

extern mysql *mysqlObj;
extern spravochniki_form *spravochnikiObj;
extern Form_ttn *formTTN_Object;
extern dialogPreview *dialogPreview_Obj;
extern loginWindowClass *loginWindowObject;

reports_form::reports_form(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::reports_form),
	row( -1 ),
	column( -1 )
{
    ui->setupUi(this);
    setupParamsAndSlots();
}

// 
void reports_form::setupParamsAndSlots()
{
	textCodec = QTextCodec::codecForName("Windows-1251");

	progrBar = new QProgressBar( ui->tableViewReports );

	ui->selectNumScales->addItem( "1" ); // ???

	this->setWindowFlags( Qt::WindowMinimizeButtonHint | Qt::WindowFullscreenButtonHint | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint );

	// Get instance of the mysql object
	mysqlObj = mysql::instance();
	// mysqlObj->getReportsTableData( getHeadersReportsTable(), ui->tableViewReports, progrBar, "", ( ui->radioBtnAccept->isChecked() == true ? 0 : 1 ) );
		
	bool flag = connect( ui->btnAcceptFilter, SIGNAL( clicked() ), this, SLOT( btnAcceptFilter_Slot() ) );
	connect( ui->btnResetFilter, SIGNAL( clicked() ), this, SLOT( btnResetFilter_Slot() ) );
	connect( ui->tableViewReports, SIGNAL( doubleClicked( const QModelIndex & ) ), this, SLOT( tableViewReportsDoubleClicked_Slot( const QModelIndex & ) ) );
	connect( ui->tableViewReports, SIGNAL( clicked( const QModelIndex & ) ), this, SLOT( tableViewReportsClicked_Slot( const QModelIndex & ) ) );

	QHeaderView *m_horiz_header = ui->tableViewReports->horizontalHeader();
    flag = connect( m_horiz_header, SIGNAL( sectionClicked ( int ) ), this, SLOT( tableViewReportsHeader_Slot( int ) ) );

	flag = connect( ui->btnPrintNakladnaya, SIGNAL( clicked() ), this, SLOT( btnPrintNakladnaya_Slot() ) );
	flag = connect( ui->btnPrintTable, SIGNAL( clicked() ), this, SLOT( btnPrintTable_Slot() ) );
	flag = connect( ui->btnExcelConv, SIGNAL( clicked() ), this, SLOT( convertToExcell() ) );

	formTTN_Object = new Form_ttn( this );
	connect( ui->btnMakeTTN, SIGNAL( clicked() ), this, SLOT( makeTTN_Button_Slot() ) );

	labelViewPict_1 = new QLabel();
	labelViewPict_1->setGeometry( 10, 30, 640, 480 );
	labelViewPict_1->setWindowTitle( textCodec->toUnicode( "������ 1 - �����" ) );
	labelViewPict_1->setObjectName( "labelViewPict_1" );
	labelViewPict_1->installEventFilter( this );

	labelViewPict_2 = new QLabel();
	labelViewPict_2->setGeometry( 650, 30, 640, 480 );
	labelViewPict_2->setWindowTitle( textCodec->toUnicode( "������ 1 - �����" ) );
	labelViewPict_2->setObjectName( "labelViewPict_2" );
	labelViewPict_2->installEventFilter( this );

	labelViewPict_3 = new QLabel();
	labelViewPict_3->setGeometry( 10, 510, 640, 480 );
	labelViewPict_3->setWindowTitle( textCodec->toUnicode( "������ 2 - �����" ) );
	labelViewPict_3->setObjectName( "labelViewPict_3" );
	labelViewPict_3->installEventFilter( this );

	labelViewPict_4 = new QLabel();
	labelViewPict_4->setGeometry( 650, 510, 640, 480 );
	labelViewPict_4->setWindowTitle( textCodec->toUnicode( "������ 2 - �����" ) );
	labelViewPict_4->setObjectName( "labelViewPict_4" );
	labelViewPict_4->installEventFilter( this );

	ui->btnAcceptFilter->setIconSize( QSize( 43, 43 ) );
	ui->btnAcceptFilter->setIcon( QPixmap( ":/Resources/images/filter_add.png", "PNG" ) );

	ui->btnResetFilter->setIconSize( QSize( 31, 31 ) );
	ui->btnResetFilter->setIcon( QPixmap( ":/Resources/images/filter_remove.png", "PNG" ) );

	ui->btnPrintNakladnaya->setIconSize( QSize( 31, 31 ) );
	ui->btnPrintNakladnaya->setIcon( QPixmap( ":/Resources/images/printer.png", "PNG" ) );

	ui->btnPrintTable->setIconSize( QSize( 31, 31 ) );
	ui->btnPrintTable->setIcon( QPixmap( ":/Resources/images/printer-report.png", "PNG" ) );

	ui->btnExcelConv->setIconSize( QSize( 31, 31 ) );
	ui->btnExcelConv->setIcon( QPixmap( ":/Resources/images/excel.png", "PNG" ) );

	this->installEventFilter( this );

	btnResetFilter_Slot();

	// hided  fields
	ui->label_num_auto->hide();
	ui->selectNumScales->hide();
	ui->btnMakeTTN->hide();

	sort_filter = new QSortFilterProxyModel( this );
}

// 
void reports_form::makeTTN_Button_Slot()
{
	int row = this->row;
    int column = this->column;

	if( this->row == -1 || this->column == -1 ) // ��� ������� � ������� 
	{
		QMessageBox mess;
	    mess.setText( "��� ������ �������� ������ ��� ������������ ������!" );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "������" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( "   ��� ���������  ��� \"����������������� ������� ������\" " );
		mess.exec();

		return;
	}

	formTTN_Object->loadDataToTTN_Form( ui->tableViewReports->model()->index( row, 1 ).data().toString() );
	formTTN_Object->showTTN();

	this->hide();
}

// 
QString reports_form::getDateTimeRecord()
{
	if( ui->tableViewReports->model()->rowCount() > 0 )
		return  QDateTime::fromString( ui->tableViewReports->model()->index( this->row, 1 ).data().toString(), "dd.MM.yyyy hh:mm:ss" ).toString( "yyyy-MM-dd hh:mm:ss" ); // date time sending auto cell
	else
		return "";
}

// 
void reports_form::showForm()
{
	if( loginWindowObject->role != loginWindowObject->ADMIN )
	{
		QMessageBox::critical( NULL, textCodec->toUnicode( "������ �����������" ), textCodec->toUnicode( "� ��� ��� ���� ��� ��������� ������� �����������" ), QMessageBox::Ok );
	    return;
	}

	mysql *mysqlObj = mysql::instance();

	ui->selectSender->clear();
	ui->selectSender->addItem( "" );

	ui->selectNumCard->clear();
	ui->selectNumCard->addItem( "" );

	ui->selectFIO->clear();
	ui->selectFIO->addItem( "" );

	ui->selectTypeOfGoods->clear();
	ui->selectTypeOfGoods->addItem( "" );

	QList< QString > data;

	data = mysqlObj->getRegisteredDataToFilterControls( 0 );
	ui->selectSender->addItems( data );

	data = mysqlObj->getRegisteredDataToFilterControls( 1 );
	ui->selectNumCard->addItems( data );

	data = mysqlObj->getRegisteredDataToFilterControls( 2 );
	ui->selectFIO->addItems( data );

	data = mysqlObj->getRegisteredDataToFilterControls( 3 );
	ui->selectTypeOfGoods->addItems( data );

	this->show();
}

// 
void reports_form::updateReportsTable_Slot()
{
	btnAcceptFilter_Slot();
}

// 
void reports_form::btnAcceptFilter_Slot()
{
	QList< QString > list;

	list.append( QString( "%1 %2" ).arg( ui->calendarStartDate->selectedDate().toString( "yyyy-MM-dd" ) ).arg( ui->timeEditStart->time().toString( "hh:mm:ss" ) ) );
	list.append( QString( "%1 %2" ).arg( ui->calendarFinDate->selectedDate().toString( "yyyy-MM-dd" ) ).arg( ui->timeEditFin->time().toString( "hh:mm:ss" ) ) );
	qDebug() << "StartDate: " << list.at(0) << ", " << "FinDate: " << list.at(1) << "\r\n";

	list.append( ui->enterNumAuto->text() );
	list.append( ui->selectSender->currentText() );
	list.append( ui->selectNumCard->currentText() );
	list.append( ui->selectFIO->currentText() );
	list.append( ui->selectTypeOfGoods->currentText() );
	list.append( QString::number( ui->selectFinRecords->currentIndex() ) );

	mysqlObj->getReportsTableData( getHeadersReportsTable(), ui->tableViewReports, progrBar, list );
}

// 
void reports_form::btnResetFilter_Slot()
{
	QList< QString > list;

    ui->timeEditStart->setTime( QTime( 8, 0, 0 ) );
	ui->timeEditFin->setTime( QTime( 7, 59, 59 ) );
	list.append( QString( "%1 %2" ).arg( QDate::currentDate().toString( "yyyy-MM-dd" ) ).arg( ui->timeEditStart->time().toString( "hh:mm:ss" ) ) );
	list.append( QString( "%1 %2" ).arg( QDate::currentDate().toString( "yyyy-MM-dd" ) ).arg( ui->timeEditFin->time().toString( "hh:mm:ss" ) ) );

	ui->enterNumAuto->clear();
	ui->selectSender->setCurrentText( "" );
	ui->selectNumCard->setCurrentText( "" );
	ui->selectFIO->setCurrentText( "" );
	ui->selectTypeOfGoods->setCurrentText( "" );
	ui->selectFinRecords->setCurrentText( "" );

	list.append( ui->enterNumAuto->text() );
	list.append( ui->selectSender->currentText() );
	list.append( ui->selectNumCard->currentText() );
	list.append( ui->selectFIO->currentText() );
	list.append( ui->selectTypeOfGoods->currentText() );
	list.append( "0" );

	mysqlObj->getReportsTableData( getHeadersReportsTable(), ui->tableViewReports, progrBar, list );
}

// 
void reports_form::radioBtnSend_Slot()
{
	updateReportsTable_Slot();
}

// 
int reports_form::getReportTableColumnsCount()
{
	return ui->tableViewReports->model()->columnCount();
}

// 
void reports_form::setSpravochnikiContent_Slot( QList< QString > data, int tab )
{
}

// 
QTableView *reports_form::getTableView()
{
	return ui->tableViewReports;
}

// 
void reports_form::btnPrintNakladnaya_Slot()
{
	QTextCodec *textCodec = QTextCodec::codecForName("Windows-1251");

	QMessageBox mess;

	if( this->row == -1 || this->column == -1 ) // ��� ������� � ������� 
	{
		mess.setText( textCodec->toUnicode( "��� ������ �������� ������ ��� ������������ ������!" ) );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, textCodec->toUnicode( "������" ) );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( textCodec->toUnicode( "   ��� ���������  ��� \"����������������� ������� ������\" " ) );
		mess.exec();

		return;
	}

	QModelIndex id = ui->tableViewReports->model()->index( this->row, this->column, QModelIndex() );

	QTableView & table = *ui->tableViewReports;
	printRecordWithPhotos( id, table, getHeadersReportsTable(), 1 );

	this->row = -1;
	this->column = -1;
}

// ����������� ������� ������� � ������� �� ������� ����������� � ���������� �� ����� 
void reports_form::printRecordWithPhotos( const QModelIndex & index, QTableView & table, QList< QString > headers, int num_table )
{
	// for( int j = 0; j < 3; j++ )
	// {

	QTextCodec *textCodec = QTextCodec::codecForName("Windows-1251");
	QMessageBox mess;
	QString html;
	QList< QString > data;

	int row = index.row();
	int column = index.column();

	QPrinter printer( QPrinter::HighResolution );
	printer.setPaperSize( QSize( 210, 297 ), QPrinter::Millimeter );
	printer.setOrientation( QPrinter::Landscape );
	printer.setFullPage( true );
	printer.setPageMargins( 8, 8, 8, 8, QPrinter::Millimeter );

	QDir dir( "NakladnayaPrint" );
	if( !dir.exists() )
	{
		dir.mkdir( dir.absolutePath() );
	}

	printer.setOutputFileName( "NakladnayaPrint//nakladnaya.pdf" );
	printer.setOutputFormat( QPrinter::PdfFormat );
	printer.setFullPage( true );

	webView = new QWebView; // ������ ��� �������� ������ 
	webView->setStyleSheet( "background-color: #fff;" );
	webView->setWindowTitle( textCodec->toUnicode( "������ ���������" ) );

    QPrintPreviewDialog preview( &printer ); //, this );
	preview.setWindowFlags( Qt::Window );

	preview.setModal( true );
	preview.setOrientation( Qt::Horizontal );

	QString date_time = table.model()->index( row, 0 ).data().toString();
	date_time = QDateTime::fromString( date_time, "dd.MM.yyyy hh:mm:ss" ).toString( "yyyy-MM-dd hh:mm:ss" );

	if( num_table == 0 )
	{
	    data = mysqlObj->getNakladnayaRecord( date_time, 0 );
	}
	if( num_table == 1 )
	{
	    data = mysqlObj->getNakladnayaRecord( date_time, 1 );
	}
	if( data.count() == 0 )
	{
		QMessageBox::critical( NULL, textCodec->toUnicode( "������������ ���������" ),
			textCodec->toUnicode( "��� ������ ��� ������������!\n��������� ����� � ��." ), QMessageBox::Ok );

		return;
	}

	QFile file( "NakladnayaPrint\\nakladnaya.pdf" );
	file.remove();
	file.close();

	html = "<html><body style=\"background-color: #fff; width: 100%; padding-botom: 0px; display: block;\">";

	html += "<head>";
	html += "<meta http-equiv=\"content-type\" content=\"text/html;charset=Windows-1251\">";
	html += "<meta name=\"robots\" content=\"noindex,nofollow\">";
	html += "<meta name=\"Googlebot\" content=\"noindex,nofollow\">";
	html += "</head>";

	QList<QString> rowHeader;
	// html.append( "<html style='background-color: #ffffff;color: #000000;'><body>" );
	html.append( QString( "<div style='text-align: center; font-size: 15px; '>" + textCodec->toUnicode( "�������� ����������� " ) + "</b>" + textCodec->toUnicode( " �� " ) + "%1" 
		+ textCodec->toUnicode( "�." ) + "</div><br />" ).
		arg( data.at( 0 ).split( ' ' )[0] ) );

	// border='1' 
	html.append( " <table cellspacing='0' cellpadding='4' width=100% style='text-align: center; font-size: 15px; font-weight: normal; border: 1px solid #000000; "
		"width: 99%; margin-right: 0px;'>" );

	// 
    html.append( "<tr>" );
	for( int h = 0; h < table.model()->columnCount() - 2; h++ )
	{
		rowHeader.append( headers.at( h ) );
		html.append( QString( "<th style='font-size: 14px; width: auto; margin: 0px; border-right: 1px solid #000000; '>%1</th>" ).arg( rowHeader.at( h ) ) );
	}

    html.append( "</tr>" );

	QList<QString> rowData;
	html.append( "<tr>" );

	for( int c = 0; c < table.model()->columnCount() - 2; c++ )
	{
		rowData.append( table.model()->index( row, c ).data().toString() );
		html.append( QString( "<td style='font-size: 14px; width: auto; margin: 0px; border-right: 1px solid #000000; border-top: 1px solid #000000; '>%1</td>" ).arg( rowData.at( c ) ) );
	}

    rowData.clear();
	html.append( "</tr>" );
    html.append( "</table>" );

	// get photos
	int idf = getIdForShowPhoto( table, index.row(), 0 );

	mysql *mysqlObj = mysql::instance();
	QVector< QByteArray > ba = mysqlObj->getSavedPhoto( idf );

	html.append( "<div style='width: 100%; display: block; margin-top: 10px; margin-left: 5px;'>" );
	for( int i = 0; i < ba.count(); i++ )
    {
		QByteArray data = ba.at( i );
		
		if( data.size() == 0 )
			continue;

		QBuffer buffer( &data );
		buffer.open( QIODevice::ReadWrite );

		QPixmap pix;
		pix.loadFromData( buffer.data() );
		pix = pix.scaled( 1920, 1080 );
		pix.save( &buffer, "JPG" );

		QByteArray ba_1 = QByteArray( (const char*  )buffer.data(), buffer.size() );
		QString img = ba_1.toBase64(); 
		html.append( "<span style='width=\"1920px\"; height=\"1080\";'>" );
		html.append( QString( "<img id=\"%1\" style='margin: 5px;' src=\"data:image/jpg;base64,%2\" width='1920px' height='1080px' alt=\"����\" />" )
			.arg( i +1 )
			.arg( img ) );
		html.append( "</span>" );
		if( i == 2 )
			html.append( "<br/>" );
	}
	html.append( "</div>" );
	html.append( "</body></html>" );
	
	webView->setHtml( html );
	webView->setHidden( true );

	/*QFile file1( "report.html" );
	file1.open( QIODevice::ReadWrite );
	file1.write( QByteArray( ( const char* )html.data(), html.size() ) ); 
	file1.close();*/
	
	
    QFile file1( "report.html" );
	if ( file1.open( QIODevice::WriteOnly | QIODevice::Text ) )
	{
	    QTextStream out( &file1 );
		out << html;
	    file1.close();
    }
	QDesktopServices::openUrl( QUrl::fromLocalFile( "report.html" ) );

	// connect( &preview, SIGNAL( paintRequested( QPrinter* ) ), webView, SLOT( print( QPrinter* ) ) );
	webView->print( &printer );
    // QDesktopServices::openUrl( QUrl::fromLocalFile( "NakladnayaPrint\\nakladnaya.pdf" ) );

	// }

	this->row = -1;
    this->column = -1;

	webView->deleteLater();
}

// 
void reports_form::tableViewReportsDoubleClicked_Slot( const QModelIndex & index )
{
	QSettings settings( "configure.ini", QSettings::IniFormat );
    settings.setIniCodec("Windows-1251");

	QTextCodec *textCodec = QTextCodec::codecForName("Windows-1251");
	QMessageBox mess;
	QString html;
	QList< QString > data;

	int row = index.row();
	int column = index.column();

	QTableView & table = *ui->tableViewReports;

	if( column == NUM_PHOTO_CELL )
	{
		QVector< QByteArray > ba;
        QImage img;

		int idf = getIdForShowPhoto( table, row, 0 );

		mysql *mysqlObj = mysql::instance();
		ba = mysqlObj->getSavedPhoto( idf );

		QVector< QLabel* > labels = dialogPreview_Obj->getPreviewControls();

		if( showPhotos( ba, labels ) > 0 )
		{
			dialogPreview_Obj->showMaximized();
			dialogPreview_Obj->raise();
		}

		dialogPreview_Obj->getDescriptionArrivedLabel()->setText( QString( "<html><body style='font-size: 18px;font-weight: bold;'>" 
			+ textCodec->toUnicode( "���������� ������ ����" ) 
			+ "</html></body>" ) );

		dialogPreview_Obj->getDescriptionAbandonLabel()->setText( QString( "<html><body style='font-size: 18px;font-weight: bold;'>" 
			+ textCodec->toUnicode( "���������� ������ ����" ) 
			+ "</html></body>" ) );

		dialogPreview_Obj->getDescriptionLabel()->setText( QString( "<html><body style='font-size: 18px;font-weight: bold;'>" 
			+ textCodec->toUnicode( "����/����� ��������:" ) 
			+ "%1, " + textCodec->toUnicode( "����/����� ������: " ) 
			+ " %2</html></body>" )
			.arg( table.model()->index( row, 0 ).data().toString() )
			.arg( table.model()->index( row, 1 ).data().toString() ) );

		return;
	}

	// QTableView & table = *ui->tableViewReports;
	printRecordWithPhotos( index, table, getHeadersReportsTable(), 1 );

	

	//QPrinter printer( QPrinter::HighResolution );
	//printer.setPaperSize( QSize( 210, 297 ), QPrinter::Millimeter );
	//printer.setOrientation( QPrinter::Portrait );
	//printer.setFullPage( true );
	//printer.setPageMargins( 16, 16, 16, 16, QPrinter::Millimeter );

	//QDir dir( "NakladnayaPrint" );
	//if( !dir.exists() )
	//{
	//	dir.mkdir( dir.absolutePath() );
	//}

	//printer.setOutputFileName( "NakladnayaPrint//nakladnaya.pdf" );
	//printer.setOutputFormat( QPrinter::PdfFormat );

	//webView = new QWebView; // ������ ��� �������� ������ 
	//webView->setStyleSheet( "background-color: #fff;" );

	//QPrintPreviewDialog preview( &printer, this );
	//preview.setWindowFlags( Qt::Window );

	//QObject::connect( &preview, SIGNAL( paintRequested( QPrinter* ) ), webView, SLOT( print( QPrinter* ) ) );

	//preview.setModal( true );
	//preview.setOrientation( Qt::Vertical );

	//QString date_time = table.model()->index( row, 0 ).data().toString();
	//date_time = QDateTime::fromString( date_time, "dd.MM.yyyy hh:mm:ss" ).toString( "yyyy-MM-dd hh:mm:ss" );

	//data = mysqlObj->getNakladnayaRecord( date_time );

	//if( data.count() == 0 )
	//{
	//	QMessageBox::critical( NULL, textCodec->toUnicode( "������������ ���������" ),
	//		textCodec->toUnicode( "��� ������ ��� ������������!\n��������� ����� � ��." ), QMessageBox::Ok );

	//	return;
	//}

	//QList<QString> rowHeader;

	//html.append( "<html style='background-color: #ffffff;color: #000000;'><body>" );

	//html.append( QString( "<span style='text-align: left; font-size: 18px; '><a><b>" + textCodec->toUnicode( "����������� " ) + "</b>" + textCodec->toUnicode( " �� " ) + "%1" 
	//	+ textCodec->toUnicode( "�." ) + "</a></span><br />" ).
	//	arg( data.at( 0 ).split( ' ' )[0] ) );

	//html.append( " <table border='1'cellspacing='0' cellpadding='4' width=100% style='text-align: center; font-size: 18px; font-weight: bold; border: 3px solid #000; "
	//	"width: 100%; margin-right: 0px;'>" );

 //   html.append( "<tr>" );
	//QList< QString > headers = getHeadersReportsTable();
	//for( int h = 0; h < table.model()->columnCount() - 3; h++ )
	//{
	//	rowHeader.append( headers.at( h ) );
	//	html.append( QString( "<th>%1</th>" ).arg( rowHeader.at( h ) ) );
	//}

 //   html.append( "</tr>" );

	//QList<QString> rowData;
	//for( row = 0; row < table.model()->rowCount(); row++ )
 //   {
	//	html.append( "<tr>" );

	//	for( int c = 0; c < table.model()->columnCount() - 3; c++ )
	//	{
	//		rowData.append( table.model()->index( row, c ).data().toString() );
	//		html.append( QString( "<td>%1</td>" ).arg( rowData.at( c ) ) );
	//	}

 //       rowData.clear();
	//	html.append( "</tr>" );
	//}
 //   html.append( "</table><br/><br/>" );

	//// get photos
	//int idf = getIdForShowPhoto( table, index.row(), 0 );

	//mysql *mysqlObj = mysql::instance();
	//QVector< QByteArray > ba = mysqlObj->getSavedPhoto( idf );

	//for( int i = 0; i < ba.count(); i++ )
 //   {
	//	QString img = ba.at( i ).toBase64();
	//	html.append( "<span style='width=\"320px\"; height=\"240px\";'>" );
	//	html.append( QString( "<img src=\"data:image/jpg;base64,%1\" width=320px height=240px alt='' />" )
	//		.arg( img ) );
	//	html.append( "</span>" );
	//	if( i == 2 )
	//		html.append( "<br/>" );
	//}

	//html.append( "</body></html>" );

	///*QTextEdit *textEdit = new QTextEdit();
	//textEdit->append( html );
	//textEdit->show();*/

	//webView->setHtml( html );
	//webView->repaint();

	//webView->setHtml( html );

	//webView->print( &printer );

	//// if( preview.exec() == QPrintDialog::Accepted )
	//// {
 //       QDesktopServices::openUrl( QUrl::fromLocalFile( "NakladnayaPrint\\nakladnaya.pdf" ) );
	//// }

	this->row = -1;
    this->column = -1;
	//webView->deleteLater();
}

// 
void reports_form::tableViewReportsClicked_Slot( const QModelIndex & index )
{
	this->row = index.row();
	this->column = index.column();
}

// 
void reports_form::btnPrintTable_Slot()
{
	QTextCodec *textCodec = QTextCodec::codecForName("Windows-1251");

	QTableView & table = *ui->tableViewReports;
	printRecords( table, getHeadersReportsTable(), 1 );
	this->row = -1;
	this->column = -1;
}

// ����������� ������� � ������� �� ������� ����������� 
void reports_form::printRecords( QTableView & table, QList< QString > & headers, int type_table )
{
	QTextCodec *textCodec = QTextCodec::codecForName("Windows-1251");
    QMessageBox mess;

	if( table.model()->rowCount() == 0 )
	{
		mess.setText( textCodec->toUnicode( "��� ������ ���������� ������������ ������ ��� ������!" ) );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, textCodec->toUnicode( "������" ) );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( textCodec->toUnicode( "   ��� ���������  ��� \"����������������� ������� ������\" " ) );
		mess.exec();

		return;
	}

	QPrinter printer( QPrinter::HighResolution );
	printer.setOrientation( QPrinter::Landscape );
	printer.setPaperSize( QPrinter::A4 ); // QPagedPaintDevice::A4 );
	printer.setFullPage( true );
	printer.setPageMargins( 10, 10, 10, 10, QPrinter::Millimeter );

	QDir dir( "ReportsPrint" );
	if( !dir.exists() )
	{
		dir.mkdir( dir.absolutePath() );
	}

	printer.setOutputFileName( "ReportsPrint//report.pdf" );
	printer.setOutputFormat( QPrinter::PdfFormat );

	QString html;

	QWebView *webView = new QWebView; // ������ ��� �������� ������ 
	webView->setStyleSheet( "background-color: #fff;" ); // width: 100%; height: 100%;

	QPrintPreviewDialog preview( &printer, this );
	preview.setWindowFlags( Qt::Window );

	QObject::connect( &preview, SIGNAL( paintRequested( QPrinter* ) ), webView, SLOT( print( QPrinter* ) ) );

	preview.setModal( true );
	preview.setOrientation( Qt::Horizontal );

	QList<QString> rowHeader;
	int row;

	html = "<html style=\"background-color: #fff; padding-botom: 30px; display: block;\"><body>";

	html += "<head>";
	html += "<meta http-equiv=\"content-type\" content=\"text/html;charset=iso-8859-1\">";
	html += "<meta name=\"robots\" content=\"noindex,nofollow\">";
	html += "<meta name=\"Googlebot\" content=\"noindex,nofollow\">";
	html += "</head>";

	html += QString( "<h2 style='text-align: center;'>" + QString( textCodec->toUnicode( "������ �� ������������ �� " ) ) + "%1" + QString( textCodec->toUnicode( "�." ) ) + "</h2><br /><br /><br />" ).
		arg( QDate::currentDate().toString( "dd.MM.yyyy" ) ).
		append( " <table border='1'cellspacing='0' cellpadding='4' width=100% style='text-align: center; font-size: 18px; font-weight: bold; border: 3px solid #000; "
		"width: 100%; margin-right: 0px;'>" );

	double brutto_sum = 0;
	double tara_sum = 0;
	double netto_sum = 0;

    html.append( "<tr>" );

	if( type_table == 0 )
	{
		for( int h = 0; h < table.model()->columnCount() - 3; h++ ) // ???
		{
			rowHeader.append( headers.at( h ) );

			html.append( QString( "<th>%1</th>" ).arg( rowHeader.at( h ) ) );
		}
	}
	else
	{
		for( int h = 0; h < table.model()->columnCount() - 2; h++ ) // ???
		{
			rowHeader.append( headers.at( h ) );

			html.append( QString( "<th>%1</th>" ).arg( rowHeader.at( h ) ) );
		}
	}

    html.append( "</tr>" );

	QList<QString> rowData;
	for( row = 0; row < table.model()->rowCount(); row++ )
    {
		html.append( "<tr>" );

		if( type_table == 0 )
		{
			for( int c = 0; c < table.model()->columnCount() - 3; c++ ) // ???
			{
				rowData.append( table.model()->index( row, c ).data().toString() );
				html.append( QString( "<td>%1</td>" ).arg( rowData.at( c ) ) );
			}

			brutto_sum += table.model()->index( row, 2 ).data().toDouble(); // ����� �� ������ 
			tara_sum += table.model()->index( row, 3 ).data().toDouble(); // ����� �� ���� 
			netto_sum += table.model()->index( row, 4 ).data().toDouble(); // ����� �� ����� 
		}
		else
		{
			for( int c = 0; c < table.model()->columnCount() - 2; c++ ) // ???
			{
				rowData.append( table.model()->index( row, c ).data().toString() );
				html.append( QString( "<td>%1</td>" ).arg( rowData.at( c ) ) );
			}

			brutto_sum += table.model()->index( row, 2 ).data().toDouble(); // ����� �� ������ 
			tara_sum += table.model()->index( row, 3 ).data().toDouble(); // ����� �� ���� 
			netto_sum += table.model()->index( row, 4 ).data().toDouble(); // ����� �� �����
		}

        rowData.clear();
		html.append( "</tr>" );
	}
	html.append( "</table><br /><br />" );

	html.append( QString( "<p style='font-size: 18px; font-weight: bold;'>" + QString( textCodec->toUnicode( "����� �� ����������� �1: " ) )
		+ QString( "<span style='font-size: 22px; font-weight: bolder;'> %1" ).arg( brutto_sum ) + QString( textCodec->toUnicode( " �" ) + "</span></p>" )
			) );
	html.append( QString( "<p style='font-size: 18px; font-weight: bold;'>" + QString( textCodec->toUnicode( "����� �� �� ����������� �2: " ) ) 
		+ QString( "<span style='font-size: 22px; font-weight: bolder;'> %1" ).arg( tara_sum ) + QString( textCodec->toUnicode( " �" ) + "</span></p>" )
			) );
	html.append( QString( "<p style='font-size: 18px; font-weight: bold;'>" + QString( textCodec->toUnicode( "����� �� �����: " ) )
		+ QString( "<span style='font-size: 22px; font-weight: bolder;'> %1" ).arg( netto_sum ) + QString( textCodec->toUnicode( " �" ) + "</span></p>" )
			) );

	html.append( QString( "<br /><br /><span style='text-align: left; font-size: 18px; '><p><b>" + QString( textCodec->toUnicode( "�������������: " )
		+ " _______________________________________</b>"
		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>" + QString( textCodec->toUnicode( "����/�����: " ) ) + "%1 </b></p></span><br /><br /><br /><br /><br /><br />" ).
		arg( QDateTime().currentDateTime().toString( "dd.MM.yyyy hh:mm:ss" ) ) ) );

	html.append( "</body></html>" );
	
	webView->setHtml( html );

	webView->page()->mainFrame()->print( &printer ); //teste 1 //ui->ci_web_view->page()->mainFrame()->print(p); //test 2 }

	// webView->print( &printer );

	// if( preview.exec() == QPrintDialog::Accepted )
	// {
        QDesktopServices::openUrl( QUrl::fromLocalFile( "ReportsPrint\\report.pdf" ) );
	// }

	webView->deleteLater();
}

// ������������� ������ �������� � ������� Excel
void reports_form::convertToExcell()
{
	QTextCodec *textCodec = QTextCodec::codecForName("Windows-1251");

    QString val;
	int i = 0, j = 0;
	QList<QVariant> row;
	QVector< QList< QString > > data;

	const char line_style = 1;
    const char line_width = 2;

	if( getTableView()->model()->rowCount() == 0 )
	{
		// ������ ������� � �� 
	    QMessageBox mess;
		mess.setText( textCodec->toUnicode( "������ ��� �������������� ���!" ) );
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, textCodec->toUnicode( "������" ) );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( textCodec->toUnicode( "   ��� ���������  ��� \"����������������� ������� ������\" " ) );
		mess.exec();

		return;
	}

	// �������� ����������� Exel �������� 
	QAxObject *mExcel = new QAxObject( "Excel.Application" ); //�������� ����������� �� excel
	mExcel->setProperty( "DisplayAlerts", 0 ); // ��������� �������������� ��  ������� 
	QAxObject *workBooks = mExcel->querySubObject( "Workbooks" );

	if( workBooks == 0x00 )
	{
		// "�� Microsoft Excel �� �����������!"
		workBooks->deleteLater();
		mExcel->deleteLater();

		return;
	}

	QAxObject *workBook = workBooks->querySubObject( "Add" ); // �������� ���� Excel
	QAxObject *sheets = workBook->querySubObject( "Worksheets" );
	QAxObject *sheet1 = sheets->querySubObject( "Item( int )", 1 );

	i = 0;
	j = 0;

	// ������� ����� �������� �� "����� �������"
	row.clear();

	QList< QString > list = getHeadersReportsTable();

	QAxObject *rangeHeaders = sheet1->querySubObject( "Range(const QString&, const QString&)", QString("A%1").arg( 1 ), QString("F%1").arg( 1 ) );
	for( int h = 0; h < getReportTableColumnsCount(); h++ )
	{
		if( h == 5 )
		{
			continue;
		}
		row.append( list.at( h ) );
	}

	char letter = 'A';
	int Z = 0;

	QAxObject *borderLeft;
	QAxObject *borderTop;
	QAxObject *borderRight;
	QAxObject *borderBottom;
	QAxObject *cell;

	while( Z < getReportTableColumnsCount() )
	{
		while( letter <= 'F' )
		{
			cell = sheet1->querySubObject( "Range(const QVariant&)", QString("%1").arg( QString( "%1%2" ).arg( letter ).arg( 1 ) ) );

			borderLeft = cell->querySubObject("Borders(xlEdgeLeft)" );
			borderLeft->setProperty( "LineStyle", line_style ); //��� ����� (��� ��������,�������� � ��� �����)
			borderLeft->setProperty( "Weight", line_width ); //�������

			borderTop = cell->querySubObject("Borders(xlEdgeTop)" );
			borderTop->setProperty( "LineStyle", line_style ); //��� ����� (��� ��������,�������� � ��� �����)
			borderTop->setProperty( "Weight", line_width ); //�������

			borderRight = cell->querySubObject("Borders(xlEdgeRight)" );
			borderRight->setProperty( "LineStyle", line_style );
			borderRight->setProperty( "Weight", line_width );

			borderBottom = cell->querySubObject("Borders(xlEdgeBottom)" );
			borderBottom->setProperty( "LineStyle", line_style );
			borderBottom->setProperty( "Weight", line_width );

			++letter;
		}
		++Z;
		letter = 'A';
	}

	QAxObject *razmer = rangeHeaders->querySubObject( "Rows" ); //������� ��������� �� ������
    razmer->setProperty( "ColumnWidth", 30 ); // ������������ � ������.
	razmer->setProperty( "HorizontalAlignment", -4108 ); // ������������ � ������.

	QAxObject* interior = rangeHeaders->querySubObject("Interior");
	// ������������� ����
	interior->setProperty("Color",QColor("yellow"));
	// 
	rangeHeaders->setProperty("Value", QVariant( row ) );

	// ������ ������  �  �������  Excel
	row.clear();

	QString last_date_time = getTableView()->model()->index( getTableView()->model()->rowCount() - 1, 0 ).data().toString();
	QString dateTimeNow = getTableView()->model()->index( 0, 0 ).data().toString();

	bool flag = mysqlObj->getDataForHourlyReports( QDateTime::fromString( last_date_time, "dd.MM.yyyy hh:mm:ss" ).toString( "yyyy-MM-dd hh:mm:ss" ),
        QDateTime::fromString( dateTimeNow, "dd.MM.yyyy hh:mm:ss" ).toString( "yyyy-MM-dd hh:mm:ss" ),
		&data ); // ��������� ������ ��� �������� �� e-mail
	if( flag == false || data.size() == 0 )
	{
		delete sheet1;
		delete sheets;
		delete workBook;
		delete workBooks;
		delete mExcel;

		return;
	}

    QString param;
	while( j <  data.size() )
	{
		QAxObject *range = sheet1->querySubObject( "Range(const QString&, const QString&)", QString("A%1").arg( j + 2 ), QString("F%1").arg( j + 2 ) );

		QSettings settingsIni( "configure.ini", QSettings::IniFormat );
		settingsIni.setIniCodec("Windows-1251");

		while( i < data.at( j ).size() ) 
		{
			if( i == 0 )
			{
				QAxObject *cell = range->querySubObject( "Range(const QVariant&)", "A1" );
				cell->setProperty( "NumberFormat", settingsIni.value( "excel/date_time_format" ).toString() );
				row.append( data.at( j ).at( i ) );
			}
			else
			if( i == 1 )
			{
                QAxObject *cell = range->querySubObject( "Range(const QVariant&)", "B1" );
				cell->setProperty( "NumberFormat", settingsIni.value( "excel/date_time_format" ).toString() );
				row.append( data.at( j ).at( i ) );
			}
			else
			if( i == 5 )
			{
				i++;
				continue;
			}
			else
				row.append( data.at( j ).at( i ) );

			i++;
		}

		range->setProperty("Value", QVariant( row ) );
		row.clear();
		j++;

		i = 0;
	}

	// 
	letter = 'A';
	j = 0;
	while( j < data.size() )
	{
		while( letter <= 'F' )
		{
			cell = sheet1->querySubObject( "Range(const QVariant&)", QString("%1").arg( QString( "%1%2" ).arg( letter ).arg( j + 2 ) ) );

			borderLeft = cell->querySubObject("Borders(xlEdgeLeft)" );
			borderLeft->setProperty( "LineStyle", line_style ); //��� ����� (��� ��������,�������� � ��� �����)
			borderLeft->setProperty( "Weight", line_width ); //�������

			borderTop = cell->querySubObject("Borders(xlEdgeTop)" );
			borderTop->setProperty( "LineStyle", line_style ); //��� ����� (��� ��������,�������� � ��� �����)
			borderTop->setProperty( "Weight", line_width ); //�������

			borderRight = cell->querySubObject("Borders(xlEdgeRight)" );
			borderRight->setProperty( "LineStyle", line_style );
			borderRight->setProperty( "Weight", line_width );

			borderBottom = cell->querySubObject("Borders(xlEdgeBottom)" );
			borderBottom->setProperty( "LineStyle", line_style );
			borderBottom->setProperty( "Weight", line_width );

			++letter;
		}
		++j;
		letter = 'A';
	}

	mExcel->setProperty( "Visible", true ); // ���������� �������� Excel
	mExcel->setProperty( "DisplayAlerts", 0 ); // ��������� �������������� ��  ������� 

	delete rangeHeaders;

	delete borderLeft;
	delete borderTop;
	delete borderRight;
	delete borderBottom;
	delete cell;

	delete sheet1;
	delete sheets;
	delete workBook;
	delete workBooks;

	mExcel->clear();
	delete mExcel;
	mExcel = NULL;
}

// 
int reports_form::getIdForShowPhoto( QTableView & table, int row, int column )
{
	int result = false;

	QString date_time = table.model()->index( row, column ).data().toString();
	date_time = QDateTime::fromString( date_time, "dd.MM.yyyy hh:mm:ss" ).toString( "yyyy-MM-dd hh:mm:ss" );
	result = mysqlObj->getIdfInTableForPhoto( date_time );

	return result;
}

// ������� 
bool reports_form::eventFilter( QObject *object, QEvent *e )
{
	if( e->type() == QEvent::Close ) // ������� "�������� �����"
	{
   //     if( object->objectName() == "labelViewPict_1" ||
			//object->objectName() == "labelViewPict_2" || 
			//object->objectName() == "labelViewPict_3" ||
			//object->objectName() == "labelViewPict_4" || 
			//object->objectName() == "reports_form"
			//) // ������� "������� ������ Enter ��� ESC" ��� ���� ����� � ������� 
	  //  {
			//labelViewPict_1->hide();
			//labelViewPict_2->hide();
			//labelViewPict_3->hide();
			//labelViewPict_4->hide();

			//return false;
	  //  }

		return false;
	}

	return false;
}

// 
void reports_form::updateReportsTable()
{
	mysqlObj->getReportsTableData( getHeadersReportsTable(), getTableView(), getProgressBarReports(), QList< QString >() );
}

// 
QTableView *reports_form::getTableReports()
{
	return ui->tableViewReports;
}

// 
QProgressBar *reports_form::getProgressBarReports()
{
	return progrBar;
}

// returns num photos
int reports_form::showPhotos( QVector< QByteArray > ba, QVector< QLabel* > labels )
{
	int result = ba.size();

	for( int i = 0; i < labels.size(); i++ )
	{
        labels.at( i )->setPixmap( QPixmap() );
	}

	for( int i = 0; i < ba.count(); i++ )
	{
		if( ba.at( i ).size() != 0 )
		{
			switch( i )
			{
				case 0:
				{
					QPixmap pixmap;
					pixmap.loadFromData( ba.at( i ) );
					pixmap = pixmap.scaled( 640, 480 );
					labels.at( i )->setPixmap( pixmap );
					labels.at( i )->show();
				} break;
				case 1:
				{
					QPixmap pixmap;
					pixmap.loadFromData( ba.at( i ) );
					pixmap = pixmap.scaled( 640, 480 );
					labels.at( i )->setPixmap( pixmap );
					labels.at( i )->show();
				} break;
				case 2:
				{
					QPixmap pixmap;
					pixmap.loadFromData( ba.at( i ) );
					pixmap = pixmap.scaled( 640, 480 );
					labels.at( i )->setPixmap( pixmap );
					labels.at( i )->show();
				} break;
				case 3:
				{
					QPixmap pixmap;
					pixmap.loadFromData( ba.at( i ) );
					pixmap = pixmap.scaled( 640, 480 );
					labels.at( i )->setPixmap( pixmap );
					labels.at( i )->show();
				} break;
				case 4:
				{
					QPixmap pixmap;
					pixmap.loadFromData( ba.at( i ) );
					pixmap = pixmap.scaled( 640, 480 );
					labels.at( i )->setPixmap( pixmap );
					labels.at( i )->show();
				} break;
				case 5:
				{
					QPixmap pixmap;
					pixmap.loadFromData( ba.at( i ) );
					pixmap = pixmap.scaled( 640, 480 );
					labels.at( i )->setPixmap( pixmap );
					labels.at( i )->show();
				} break;
				case 6:
				{
					QPixmap pixmap;
					pixmap.loadFromData( ba.at( i ) );
					pixmap = pixmap.scaled( 640, 480 );
					labels.at( i )->setPixmap( pixmap );
					labels.at( i )->show();
				} break;
				case 7:
				{
					QPixmap pixmap;
					pixmap.loadFromData( ba.at( i ) );
					pixmap = pixmap.scaled( 640, 480 );
					labels.at( i )->setPixmap( pixmap );
					labels.at( i )->show();
				} break;
			}
		}
	}

	return result;
}

// 
void reports_form::tableViewReportsHeader_Slot( int logicalIndex )
{
	static bool flag  = false;

	updateReportsTable_Slot();

	QAbstractItemModel *model = ui->tableViewReports->model();

	// sorting
	sort_filter->setSourceModel( model );
	sort_filter->sort( logicalIndex, ( flag == false ? Qt::AscendingOrder : Qt::DescendingOrder ) );
	ui->tableViewReports->setModel( sort_filter );

	if( flag ) flag = false;
	else flag = true;
}

// 
reports_form::~reports_form()
{
    delete ui;
}
