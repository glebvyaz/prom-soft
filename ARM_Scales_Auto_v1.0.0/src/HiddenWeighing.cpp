#include "stdafx.h"
#include "HiddenWeighing.h"
#include "ui_hiddenweighing.h"

#include <ActiveQt/qaxobject.h>
#include <ActiveQt/qaxbase.h>

#include "DB_Driver.h"
//extern mysqlProccess *mysqlProccessObject; // ������ ��� ������ � �� MySQL

HiddenWeighing::HiddenWeighing(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HiddenWeighing)
{
    ui->setupUi(this);
	textCodec = QTextCodec::codecForName( "Windows-1251" );

	QSettings settingsFile( "settings.ini", QSettings::IniFormat );
	settingsFile.setIniCodec( "UTF-8" );
	delayBeforeShoot = settingsFile.value( "photo_timer/msec", 3000 ).toInt();
	hiddenWeightEnable = settingsFile.value( "photo_timer/enabke", true ).toBool();

	hidden_id = 0;
	state = OFF;
	max_weights = 0;

	tableHeaders.append( "id" );
	tableHeaders.append( tr("Time") );
	tableHeaders.append( tr("Weight, kg") );
	tableHeaders.append( tr("Max weight, kg") );

	connect( ui->pushButton_accept,	SIGNAL( clicked() ), this, SLOT( acceptFilter_Slot() ) );
	connect( ui->pushButton_reset,	SIGNAL( clicked() ), this, SLOT( resetFilter_Slot() ) );
	connect( ui->pushButton_photo,	SIGNAL( clicked() ), this, SLOT( photo_Slot() ) );

	resetFilter_Slot();

	ui->tableView->setColumnWidth(0, 100);
	ui->tableView->setColumnWidth(1, 250);
	ui->tableView->setColumnWidth(2, 200);
	ui->tableView->setColumnWidth(3, 200);
	
	// in
	for( int i = 0; i < 4; ++i )
	{
		QLabel *l = new QLabel(); 
		l->setGeometry( 10+i*340, 22, 320, 240 );
		l->raise();
		l->setObjectName( "previewSnapshots" );
		l->setWindowTitle( QString( " -> Cam %1" ).arg(i+1) );
		l->installEventFilter( this );
		previewSnapshotsIn.append(l);
	}
}

HiddenWeighing::~HiddenWeighing()
{
    delete ui;
}

void HiddenWeighing::acceptFilter_Slot()
{
    QStringList filters;

    filters.append( QString( "%1" ).arg( ui->dateTimeEdit_from->dateTime().toString( "yyyy-MM-dd hh:mm:ss" ) ) );
    filters.append( QString( "%1" ).arg( ui->dateTimeEdit_to->dateTime().toString( "yyyy-MM-dd hh:mm:ss" ) ) );
	
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();

    ui->tableView->setModel( mysqlProccessObject->getHiddenWeighingTableModel( tableHeaders, filters ) );
    ui->tableView->setEditTriggers( QAbstractItemView::NoEditTriggers );
}

void HiddenWeighing::resetFilter_Slot()
{
    ui->dateTimeEdit_from->setDate( QDate::currentDate() );
    ui->dateTimeEdit_from->setTime( QTime( 0, 0 ) );
    ui->dateTimeEdit_to->setDate( QDate::currentDate() );
    ui->dateTimeEdit_to->setTime( QTime( 23, 59, 59 ) );

    acceptFilter_Slot();
}

void HiddenWeighing::shoot()
{		
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	hidden_id = mysqlProccessObject->addHiddenWeighing( max_weights );
	emit makeHiddenPhotos();
}

void HiddenWeighing::detectOn_Slot( int weight ) 
{	
	if( state == OFF )
	{
		state = ON;

		QTimer::singleShot( delayBeforeShoot, this, SLOT( shoot() ) );
	}
	
	if( weight > max_weights )
	{
		max_weights = weight;
	}
}

void HiddenWeighing::detectOff_Slot() 
{
	if( state == ON )
	{
		mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		mysqlProccessObject->updHiddenWeighing( max_weights );
	}

	state = OFF;
	max_weights = 0;
}

void HiddenWeighing::reported_Slot()
{
	if( state == ON )
	{
		mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
		mysqlProccessObject->updHiddenWeighing( true );
	}
}

void HiddenWeighing::photo_Slot()
{
	QMessageBox mess;
	bool result;
	QSqlError error;
	int row, column;

	row = ui->tableView->currentIndex().row();
	column = ui->tableView->currentIndex().column();
	
	if( row == -1 && column == -1 )
	{
	    return;
	}

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	int id = ui->tableView->model()->index( row, 0 ).data().toInt();
	result = query.exec( QString( "SELECT IDF FROM totalReports WHERE id = %1;" ).arg( id ) );

	int idf = 0;
	if( query.next() )
	{
		idf = query.value( 0 ).toInt();
	}

	qDebug() << idf;

	result = query.exec( QString( "SELECT photo_1, photo_2, photo_3, photo_4 FROM images WHERE ID=%1;" ).arg( idf ) );
	query.next();


	for( int i = 1; i <= 4; ++i )
	{
		QByteArray ba = query.record().value( QString( "photo_%1" ).arg( i ) ).toByteArray();
		if( ba.isEmpty() ) continue;

		QPixmap pixmap;
		pixmap.loadFromData( ba );
		pixmap = pixmap.scaled( QSize( 1366, 768 ), Qt::KeepAspectRatio, Qt::FastTransformation );

		int cur_snap = i - 1;
		previewSnapshotsIn[cur_snap]->setPixmap( pixmap );
		previewSnapshotsIn[cur_snap]->show();
		previewSnapshotsIn[cur_snap]->raise();
		previewSnapshotsIn[cur_snap]->setScaledContents( true );
	}
}

bool HiddenWeighing::eventFilter( QObject *object, QEvent *e )
{
	if( e->type() == QEvent::Close ) // ������������ �� �������� ���� ���� ��������� ����������
	{
		if( object->objectName() == "previewSnapshots" )
		{
			for( int i = 0; i < 4; ++i )
			{
				previewSnapshotsIn[i]->hide();			
			}
			return true;
		}		
	}
	return false;
}