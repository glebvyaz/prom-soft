#include "stdafx.h"
#include "myclass.h"
#include <QtGui/QApplication>
#include <QTextCodec>

#include "QtSingleApp\qtsingleapplication.h"
#include "Crypt.h"

//extern class mysqlProccess *mysqlProccessObject;

int main(int argc, char *argv[])
{
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("Windows-1251"));
	QTextCodec::setCodecForLocale( QTextCodec::codecForName( "UTF-8" ) );
	QTextCodec::setCodecForTr( QTextCodec::codecForName( "UTF-8" ) );
	
	QThread::currentThread()->setPriority( QThread::HighestPriority );

	// Release
	QtSingleApplication app(argc, argv);
	// Debug
	// QApplication app(argc, argv);
	QTranslator translator;

	QSettings settings( "settings.ini", QSettings::IniFormat );
	settings.setIniCodec( "Windows-1251" );
	
	QString langFromConf;
	settings.beginGroup( "Server" );
	langFromConf = settings.value( "lang", "ua" ).toString();
	settings.endGroup();

	// Translation - classic if-elseif, anywhere without it
	if( langFromConf == "ua" )
	{
		setlocale(LC_ALL, "Ukrainian");	
		translator.load("translations/ua.qm");
		app.installTranslator(&translator);
	}
	else if( langFromConf == "ru" )
	{
		setlocale(LC_ALL, "Russian");	
		translator.load("translations/ru.qm");
		app.installTranslator(&translator);
	}
	else if( langFromConf == "en" )
	{
		setlocale(LC_ALL, "English");
	}
	
	// ��������� �������� �� ��� ���������, ���� �� - �� ����� �������������� � ��������� ���������� 
    if( app.isRunning() == true )
	{
		QIcon *VIS_Icon = new QIcon();
		VIS_Icon->setPixmap( "image/promsoft200.png", QIcon::Large, QIcon::Normal, QIcon::On );

		QMessageBox mess;
		if( langFromConf == "ua" ) mess.setText( "�������� ��� ��������!" );
		else if( langFromConf == "ru" ) mess.setText( "��������� ��� ��������!" );
		else if( langFromConf == "en" )	mess.setText( "Programm is running!" );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( "AV-Control" );
		mess.setWindowIcon( *VIS_Icon );
		mess.exec();

		return 0;
	}

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	QIcon *global_Icon = new QIcon();
	global_Icon->setPixmap( "image/promsoft200.png", QIcon::Large, QIcon::Normal, QIcon::On );

	// �������� �������������� � �� MySQL 
	QStringList listDrivers = QSqlDatabase::drivers(); // ������ �������������� ��������� 

	QMessageBox mess;
	if( !listDrivers.contains( "QMYSQL" ) && !listDrivers.contains( "QMYSQL3" ) )
	{
		if( langFromConf == "ua" ) mess.setText( "���� �������� ��� ������ MySQL!" );
		else if( langFromConf == "ru" ) mess.setText( "��� �������� ��� ������ � MySQL!" );
		else if( langFromConf == "en" )	mess.setText( "MySQL driver is missing!" );
		mess.setWindowTitle( "Prom-Soft" );
		mess.setWindowIcon( *global_Icon );
		mess.exec();

		return 0;
	}
	else
	{		
		//QSettings settings( "settings.ini", QSettings::IniFormat );
		//settings.setIniCodec("Windows-1251");	

		//QSqlDatabase sqlDB = QSqlDatabase::addDatabase( "QMYSQL", "connection" );
		//sqlDB.setHostName( settings.value( "MySQL/address" ).toString() );	
		//sqlDB.setUserName( settings.value( "MySQL/login" ).toString() );
		////enc-dec pass
		//QString pass = settings.value( "MySQL/password" ).toString();
		//QString pass_enc;
		//if ( ! pass.startsWith("#") )
		//	pass_enc = Crypt::crypt( pass );
		//else
		//{
		//	pass_enc = pass.remove( 0, 1 );
		//	pass = Crypt::decrypt( pass_enc );
		//}
		//// 
		//sqlDB.setPassword( pass );
		//sqlDB.setPort( settings.value( "MySQL/port" ).toInt() );

		mysqlProccess *mysqlProccessObject = mysqlProccess::instance();

		//if( !sqlDB.open() ) // ����������� � ��
		
		if( !mysqlProccessObject->connect() )
	
		{
			if( langFromConf == "ua" ) mess.setText( "�'������� � MySQL �� �����������.\n�������� ������������." );
			else if( langFromConf == "ru" ) mess.setText( "���������� � MySQL �� �����������.\n��������� ���������." );
			else if( langFromConf == "en" )	mess.setText( "There is no connection to MySQL.\nCheck settings." );
		    mess.setWindowTitle( "Prom-Soft" );
		    mess.setWindowIcon( *global_Icon );		    
			mess.exec();

			return 0;
		}
		/*else
		{
			mysqlProccessObject->createMySQL_DataBase( );
		}*/

		//mysqlProccessObject->disconnect(); // FromDB_MySql( mysqlProccessObject->getQSqlInstance() );
	}

	MyClass w;

	return app.exec();
}

