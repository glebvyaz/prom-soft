#include "stdafx.h"
#include "dialogvpsensors.h"
#include "ui_dialogvpsensors.h"

#include <ActiveQt/qaxobject.h>
#include <ActiveQt/qaxbase.h>

#include "DB_Driver.h"
//extern mysqlProccess *mysqlProccessObject;

#define COLUMS_COUNT 17

dialogVPSensors::dialogVPSensors(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialogVPSensors)
{
    ui->setupUi(this);

	ui->dateTimeEdit_start->setDateTime( QDateTime::fromString( QDate::currentDate().toString( "dd.MM.yyyy 00:00:00" ), "dd.MM.yyyy hh:mm:ss" ) );
	ui->dateTimeEdit_finish->setDateTime( QDateTime::fromString( QDate::currentDate().toString( "dd.MM.yyyy 23:59:59" ), "dd.MM.yyyy hh:mm:ss" ) );


	QSettings settings( "settings.ini", QSettings::IniFormat );
	settings.setIniCodec( "Windows-1251" );
	numSensors = settings.value( "vp01_s/number" ).toInt();
	if( numSensors > 8 || numSensors < 4 ) numSensors = 8;

	ui->tableWidget->setColumnCount( COLUMS_COUNT );
	ui->tableWidget->setColumnWidth( 0, 120 );
	
	headers.append( "�����" );
	for( int i = 1; i < COLUMS_COUNT; i++ )
	{
		if( i <= 8 )
		{
			headers.append( QString( "�� %1, ��" ).arg( i ) );
			if( i > numSensors ) ui->tableWidget->hideColumn( i );
		}
		else
		{
			headers.append( QString( "�� %1, ���" ).arg( i-8 ) );
			if( i > numSensors+8 ) ui->tableWidget->hideColumn( i );
		}
	}

	ui->tableWidget->setHorizontalHeaderLabels( headers );	
	int tableWidth = 70*numSensors*2 + 120 + 20;
	if( tableWidth < 760 ) tableWidth = 760; 
	ui->tableWidget->setMinimumWidth( tableWidth );
	this->adjustSize();

	connect( ui->pushButton_Ok, SIGNAL( clicked() ), this, SLOT( fiterAccept_Slot() ) ); 
	connect( ui->pushButton_Cancel, SIGNAL( clicked() ), this, SLOT( fiterReject_Slot() ) ); 
	connect( ui->pushButton_Excel, SIGNAL( clicked() ), this, SLOT( excel_Slot() ) ); 
}

dialogVPSensors::~dialogVPSensors()
{
    delete ui;
}

void dialogVPSensors::fiterAccept_Slot()
{
	prev_kg.clear();
	prev_code.clear();
	for( int z = 0; z < 8; z++ )
	{
		prev_kg.append(0);
		prev_code.append(0);
	}
	
	ui->tableWidget->setUpdatesEnabled( false );

	QString params; // ..where **
	
    // �����
	QString start = ui->dateTimeEdit_start->dateTime().toString( "yyyy-MM-dd hh:mm:ss" );
	QString finish = ui->dateTimeEdit_finish->dateTime().toString( "yyyy-MM-dd hh:mm:ss" );

	params.append( QString( "date_time >= '%1' AND date_time <= '%2'" ).arg( start ).arg( finish ) ) ;

	// ��� ���������?

	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.exec( QString( "USE %1;" ).arg( dataBaseName ) );
	query.exec( QString( "SELECT * FROM sensorsdataforservice WHERE %1;" ).arg( params ) );
	

	int num_records = query.size();
	QString val;
	int i = 0; 
	int	j = 0;
	int k = 0;

	int row_count = ui->tableWidget->rowCount();
	while( row_count > 0 )
	{
		ui->tableWidget->removeRow( row_count - 1 );
		row_count--;
	}

	while( query.next() )
	{
		ui->tableWidget->setRowCount( j + 1 );

		while( i < COLUMS_COUNT )
		{
		    if( i == 0 )
			{
			    val = query.value( i+1 ).asDateTime().toString( "dd.MM.yyyy hh:mm:ss" );

				ui->tableWidget->setItem( j, i, new QTableWidgetItem( val ) );
				i++;
			}
			else
			{
			    val = query.value( i+1 ).toString();

				ui->tableWidget->setItem( j, i, new QTableWidgetItem( val ) );

				// ��������� �������
				if( j )
				{		
					if( ( k < 8 ) && ( ui->lineEdit_Deviation_kg->text().toInt() > 0 ) )
					{
						int module_int = ( prev_kg[k] > val.toInt() ) ? prev_kg[k] - val.toInt() : val.toInt() - prev_kg[k];
						if( module_int > ui->lineEdit_Deviation_kg->text().toInt() )
						{
							ui->tableWidget->item( j, i )->setBackgroundColor( Qt::red );
						}
						else
						{
							ui->tableWidget->item( j, i )->setBackgroundColor( Qt::white );
						}
					}
					else if( ( k >= 8 ) && ( ui->lineEdit_Deviation_code->text().toInt() > 0 ) )
					{
						int module_int = ( prev_code[k-8] > val.toInt() ) ? prev_code[k-8] - val.toInt() : val.toInt() - prev_code[k-8];
						if( module_int > ui->lineEdit_Deviation_code->text().toInt() )
						{
							ui->tableWidget->item( j, i )->setBackgroundColor( Qt::red );
						}
						else
						{
							ui->tableWidget->item( j, i )->setBackgroundColor( Qt::white );
						}
					}				
				}
				if( k < 8 )
				{
					prev_kg[k] = val.toInt();
				}
				else if( k < 8*2 )
				{
					prev_code[k-8] = val.toInt();
				}
				k++;
				i++;
			}
        }
        j++;
		k = 0;	
		i = 0;
    }

	ui->tableWidget->setUpdatesEnabled( true );
}

void dialogVPSensors::fiterReject_Slot()
{
	ui->dateTimeEdit_start->setDateTime( QDateTime::fromString( QDate::currentDate().toString( "dd.MM.yyyy 00:00:00" ), "dd.MM.yyyy hh:mm:ss" ) );
	ui->dateTimeEdit_finish->setDateTime( QDateTime::fromString( QDate::currentDate().toString( "dd.MM.yyyy 23:59:59" ), "dd.MM.yyyy hh:mm:ss" ) );

	fiterAccept_Slot();
}

void dialogVPSensors::excel_Slot()
{
	QString val;
	int i = 0, j = 0;
	QList<QVariant> row;
	
	if( ui->tableWidget->rowCount() == 0 )
	{
	    QMessageBox mess;
		mess.setText( tr("Data not available") );
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

		return;
	}

	// �������  �������� Excel 
	QAxObject *mExcel = new QAxObject( "Excel.Application" );
	mExcel->setProperty( "DisplayAlerts", 0 );
	QAxObject *workBooks = mExcel->querySubObject( "Workbooks" );

	if( workBooks == 0x00 )
	{
	    QMessageBox mess;
		mess.setText( tr("Microsoft Office - Excel is not installed!") );
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

		workBooks->deleteLater();
	    mExcel->deleteLater();

		return;
	}
	
	QAxObject *workBook = workBooks->querySubObject( "Add" );
	QAxObject *sheets = workBook->querySubObject( "Worksheets" );
	QAxObject *sheet1 = sheets->querySubObject( "Item( int )", 1 );

	i = 0;
	j = 0;
	
	// ������� ����� ��������
	QAxObject *rangeHeaders = sheet1->querySubObject( "Range(const QString&, const QString&)", QString("A%1").arg(1), QString("%1%2").arg( char( 0x41 + numSensors*2 ) ).arg(1) );
	row.append( ui->tableWidget->horizontalHeaderItem( 0 )->text() );
	for( int h = 1; h < ui->tableWidget->columnCount(); h++ )
	{
		if( h <= 8 )
		{
			if( !(h > numSensors) ) row.append( ui->tableWidget->horizontalHeaderItem( h )->text() );
		}
		else
		{
			if( !(h > numSensors+8) ) row.append( ui->tableWidget->horizontalHeaderItem( h )->text() );
		}
	}

	QAxObject *razmer = rangeHeaders->querySubObject( "Rows" );
    razmer->setProperty( "ColumnWidth", 16 );
	razmer->setProperty( "HorizontalAlignment", -4108 );

    QAxObject* font = rangeHeaders->querySubObject("Font");
	font->setProperty("Bold", true);

	QAxObject *borderRight = rangeHeaders->querySubObject("Borders(xlEdgeRight)" );
    borderRight->setProperty( "LineStyle", 1 );
    borderRight->setProperty( "Weight", 2 );
	
	QAxObject* interior = rangeHeaders->querySubObject("Interior");
	interior->setProperty("Color",QColor("yellow"));

	delete interior;

	rangeHeaders->setProperty("Value", QVariant( row ) );

	QSettings settings_file( "settings.ini", QSettings::IniFormat );
    settings_file.setIniCodec( "Windows-1251" );
	QString date_format = settings_file.value( "excel/date_time_format" ).toString();

	// ������� ������
	row.clear();
	while( j < ui->tableWidget->rowCount() )
	{
		QAxObject *range = sheet1->querySubObject( "Range(const QString&, const QString&)", QString("A%1").arg( j + 2 ), QString("%1%2").arg( char( 0x41 + numSensors*2 ) ).arg( j + 2 ) );

		while( i <= numSensors*2 ) 
		{
			if( i == 0 )
			{
				if( !date_format.isEmpty() )
				{
					QAxObject *cell = range->querySubObject( "Range(const QVariant&)", "A1" );
					cell->setProperty( "NumberFormat", date_format );
					delete cell;
				}
				row.append( ui->tableWidget->item( j, i )->text() );
			}
			else
			{
				if( i <= numSensors )
				{
					row.append( ui->tableWidget->item( j, i )->text() );

					if( ui->tableWidget->item( j, i )->backgroundColor() == Qt::red )
					{
						QAxObject* cell = sheet1->querySubObject( "Cells(QVariant,QVariant)", j+2, i+1 );	
						QAxObject* interior = cell->querySubObject( "Interior" );
						interior->setProperty( "Color", QColor( "red" ) );
						delete interior;
						delete cell;
					}
				}
				else
				{
					row.append( ui->tableWidget->item( j, i+8-numSensors )->text() );

					if( ui->tableWidget->item( j, i+8-numSensors )->backgroundColor() == Qt::red )
					{
						QAxObject* cell = sheet1->querySubObject( "Cells(QVariant,QVariant)", j+2, i+1 );	
						QAxObject* interior = cell->querySubObject( "Interior" );
						interior->setProperty( "Color", QColor( "red" ) );
						delete interior;
						delete cell;
					}
				}	
			}

			i++;
		}

		range->setProperty("Value", QVariant( row ) );
		row.clear();
        j++;

        i = 0;
	}

	mExcel->setProperty( "Visible", true );

	workBooks->deleteLater();
	mExcel->deleteLater();
}

void dialogVPSensors::email_Slot()
{
	QString val;
	int i = 0, j = 0;
	QList<QVariant> row;
	
	if( ui->tableWidget->rowCount() == 0 )
	{
	    QMessageBox mess;
		mess.setText( tr("Data not available") );
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

		return;
	}

	// �������  �������� Excel 
	QAxObject *mExcel = new QAxObject( "Excel.Application" );
	mExcel->setProperty( "DisplayAlerts", 0 );
	QAxObject *workBooks = mExcel->querySubObject( "Workbooks" );

	if( workBooks == 0x00 )
	{
	    QMessageBox mess;
		mess.setText( tr("Microsoft Office - Excel is not installed!") );
		mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

		workBooks->deleteLater();
	    mExcel->deleteLater();

		return;
	}
	
	QAxObject *workBook = workBooks->querySubObject( "Add" );
	QAxObject *sheets = workBook->querySubObject( "Worksheets" );
	QAxObject *sheet1 = sheets->querySubObject( "Item( int )", 1 );

	i = 0;
	j = 0;

	QSettings settings_file( "settings.ini", QSettings::IniFormat );
    settings_file.setIniCodec("Windows-1251");
	QString date_format = settings_file.value( "excel/date_time_format" ).toString();


	QAxObject *rangeHead1 = sheet1->querySubObject( "Range(const QString&, const QString&)", QString("A%1").arg(1), QString("A%1").arg(1) );
	rangeHead1->setProperty( "Value", QVariant( "����������: " + settings_file.value( "vp01_s/info_contragent" ).toString() ) );

	QAxObject *rangeHead2 = sheet1->querySubObject( "Range(const QString&, const QString&)", QString("A%1").arg(2), QString("A%1").arg(2) );
	rangeHead2->setProperty( "Value", QVariant( "��� �����: " + settings_file.value( "vp01_s/info_type" ).toString() ) );

	QAxObject *rangeHead3 = sheet1->querySubObject( "Range(const QString&, const QString&)", QString("A%1").arg(3), QString("A%1").arg(3) );
	rangeHead3->setProperty( "Value", QVariant( "��������: " + settings_file.value( "vp01_s/interval" ).toString() + "���." ) );


	// ������� ����� ��������
	QAxObject *rangeHeaders = sheet1->querySubObject( "Range(const QString&, const QString&)", QString("A%1").arg(1+3), QString("%1%2").arg( char( 0x41 + numSensors*2 ) ).arg(1+3) );
	row.append( ui->tableWidget->horizontalHeaderItem( 0 )->text() );
	for( int h = 1; h < ui->tableWidget->columnCount(); h++ )
	{
		if( h <= 8 )
		{
			if( !(h > numSensors) ) row.append( ui->tableWidget->horizontalHeaderItem( h )->text() );
		}
		else
		{
			if( !(h > numSensors+8) ) row.append( ui->tableWidget->horizontalHeaderItem( h )->text() );
		}
	}

	QAxObject *razmer = rangeHeaders->querySubObject( "Rows" );
    razmer->setProperty( "ColumnWidth", 16 );
	razmer->setProperty( "HorizontalAlignment", -4108 );

    QAxObject* font = rangeHeaders->querySubObject("Font");
	font->setProperty("Bold", true);

	QAxObject *borderRight = rangeHeaders->querySubObject("Borders(xlEdgeRight)" );
    borderRight->setProperty( "LineStyle", 1 );
    borderRight->setProperty( "Weight", 2 );
	
	QAxObject* interior = rangeHeaders->querySubObject("Interior");
	interior->setProperty("Color",QColor("yellow"));

	delete interior;

	rangeHeaders->setProperty("Value", QVariant( row ) );

	// ������� ������
	row.clear();
	while( j < ui->tableWidget->rowCount() )
	{
		QAxObject *range = sheet1->querySubObject( "Range(const QString&, const QString&)", QString("A%1").arg( j + 2+3 ), QString("%1%2").arg( char( 0x41 + numSensors*2 ) ).arg( j + 2+3 ) );

		while( i <= numSensors*2 ) 
		{
			if( i == 0 )
			{
				if( !date_format.isEmpty() )
				{
					QAxObject *cell = range->querySubObject( "Range(const QVariant&)", "A1" );
					cell->setProperty( "NumberFormat", date_format );
				}
				row.append( ui->tableWidget->item( j, i )->text() );
			}
			else
			{
				if( i <= 8 )
				{
					if( !(i > numSensors) ) row.append( ui->tableWidget->item( j, i )->text() );
				}
				else
				{
					if( !(i > numSensors+8) ) row.append( ui->tableWidget->item( j, i )->text() );
				}	

				if( ui->tableWidget->item( j, i )->backgroundColor() == Qt::red )
				{
					QAxObject* cell = sheet1->querySubObject( "Cells(QVariant,QVariant)", j+5, i+1 );	
					QAxObject* interior = cell->querySubObject( "Interior" );
					interior->setProperty( "Color", QColor( "red" ) );
					delete interior;
					delete cell;
				}
			}

			i++;
		}

		range->setProperty("Value", QVariant( row ) );
		row.clear();
        j++;

        i = 0;
	}

	mExcel->setProperty( "Visible", false);

	QString filePath;
	QString path = "hourly";

	QDir dir( path );
	if( !dir.exists() )
		dir.mkdir( dir.absolutePath() );

	QString fileName = "sensors.xls";
				
	filePath.append( QString( dir.absolutePath().replace( "/", "\\"  ) + "\\" + fileName ) );

	QFile file( filePath );
	file.open( QIODevice::ReadWrite );
	if( file.exists() )
	{
		file.remove();
	}
	file.close();
			
	QVariant var = workBook->dynamicCall( "SaveAs(const QString&, int)", filePath, -4143 );
	workBook->dynamicCall( "Close (Boolean)", false );

	QStringList sl;
	sl.append( filePath );

	emit loadMailData_Signal( true, 
		settings_file.value( "mail/host" ).toString(), 
		settings_file.value( "mail/port" ).toInt(), 
		settings_file.value( "mail/login" ).toString(), 
		settings_file.value( "mail/password" ).toString(), 
		settings_file.value( "mail/ssl" ).toString(), 
		settings_file.value( "mail/mailToSend" ).toString(),
		"��������� ��������",
		QString( "����������: " + settings_file.value( "vp01_s/info_contragent" ).toString() + 
				". ��� �����: " + settings_file.value( "vp01_s/info_type" ).toString() + 
				". ��������: " + settings_file.value( "vp01_s/interval" ).toString() + "���." ),
		false, "", sl );

	delete sheet1;
	delete sheets;
	delete workBook;
	delete workBooks;
	delete mExcel;
}