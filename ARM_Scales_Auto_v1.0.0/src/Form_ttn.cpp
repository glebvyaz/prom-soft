#include "stdafx.h"
#include "Form_ttn.h"
#include "DB_Driver.h"

//#define  TTN_DEBUG

//extern class mysqlProccess *mysqlProccessObject; // ������ ��� ������ � �� MySQL
// 
Form_ttn::Form_ttn( QObject *object )
{
	scrollArea = new QScrollArea();
	scrollArea->setFixedSize( 1117, 865 );
	scrollArea->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOn );
	scrollArea->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOn );  //  ScrollBarAsNeeded );

	ttnWidget = new QWidget();
	ttnWidget->setFixedSize( 1117, 865 );

	scrollArea->setWidget( ttnWidget );
	setupUi( ttnWidget, object );

	// ������������� vector
	QVector< QString > *temp = new QVector< QString >();

	// temp.append( "", "����", "��i", "���", "������", "�`���", "�i���", "�i��", "�i�i��", "���`���" );

	// tableConvertIntToStringUKR.append( 
		// );

        //{ "", "������", "��������", "��������", "�����", "�`���������", "�i���������", "�i��������", "�i�i��������", "���`������" },
        //{ "", "���", "��i��i", "������", "���������", "�`������", "�i������", "�i�����", "�i�i�����", "���`������" },
        //{ "", "����", "��i", "���", "������", "�`���", "�i���", "�i��", "�i�i��", "���`���" },
        //{ "", "������", "��������", "��������", "�����", "�`���������", "�i���������", "�i��������", "�i�i��������", "���`������" },
        //{ "", "���", "��i��i", "������", "���������", "�`������", "�i������", "�i�����", "�i�i�����", "���`������" },
        //{ "",  "�i�����", "��� �i������", "��� �i������", "������ �i������", "�`��� �i�����i�", "�i��� �i�����i�", "�i�� �i�����i�", "�i�i�� �i�����i�", "���`��� �i�����i�" },
	
}

// 
void Form_ttn::setupUi( QWidget *Form, QObject *object )
{
    if (Form->objectName().isEmpty())
        Form->setObjectName(QString::fromUtf8("Form"));

    Form->setEnabled(true);
    Form->resize(1117, 865);
	// Form->setFixedSize( 1117, 684 );

    labelTitle = new QLabel(Form);
    labelTitle->setObjectName(QString::fromUtf8("labelTitle"));
    labelTitle->setGeometry(QRect(420, 10, 231, 16));
    enterNumberNakladnaya = new QLineEdit(Form);
    enterNumberNakladnaya->setObjectName(QString::fromUtf8("enterNumberNakladnaya"));
    enterNumberNakladnaya->setGeometry(QRect(310, 40, 91, 20));
    labelNum = new QLabel(Form);
    labelNum->setObjectName(QString::fromUtf8("labelNum"));
    labelNum->setGeometry(QRect(280, 40, 21, 16));
    label = new QLabel(Form);
    label->setObjectName(QString::fromUtf8("label"));
    label->setGeometry(QRect(410, 40, 16, 16));
    label_2 = new QLabel(Form);
    label_2->setObjectName(QString::fromUtf8("label_2"));
    label_2->setGeometry(QRect(470, 40, 16, 16));
    enterDate = new QLineEdit(Form);
    enterDate->setObjectName(QString::fromUtf8("enterDate"));
    enterDate->setGeometry(QRect(420, 40, 41, 20));
    enterMonth = new QLineEdit(Form);
    enterMonth->setObjectName(QString::fromUtf8("enterMonth"));
    enterMonth->setGeometry(QRect(480, 40, 151, 20));
    labelNum_2 = new QLabel(Form);
    labelNum_2->setObjectName(QString::fromUtf8("labelNum_2"));
    labelNum_2->setGeometry(QRect(640, 40, 21, 16));
    enterYear = new QLineEdit(Form);
    enterYear->setObjectName(QString::fromUtf8("enterYear"));
    enterYear->setGeometry(QRect(660, 40, 31, 20));
    labelNum_3 = new QLabel(Form);
    labelNum_3->setObjectName(QString::fromUtf8("labelNum_3"));
    labelNum_3->setGeometry(QRect(700, 40, 21, 16));
    labelAuto = new QLabel(Form);
    labelAuto->setObjectName(QString::fromUtf8("labelAuto"));
    labelAuto->setGeometry(QRect(30, 80, 71, 16));
    enterAutoType = new QLineEdit(Form);
    enterAutoType->setObjectName(QString::fromUtf8("enterAutoType"));
    enterAutoType->setGeometry(QRect(120, 80, 251, 20));
    labelAutoTypes = new QLabel(Form);
    labelAutoTypes->setObjectName(QString::fromUtf8("labelAutoTypes"));
    labelAutoTypes->setGeometry(QRect(120, 100, 251, 16));
    labelAuto_2 = new QLabel(Form);
    labelAuto_2->setObjectName(QString::fromUtf8("labelAuto_2"));
    labelAuto_2->setGeometry(QRect(380, 80, 111, 16));
    labelAutoTypes_2 = new QLabel(Form);
    labelAutoTypes_2->setObjectName(QString::fromUtf8("labelAutoTypes_2"));
    labelAutoTypes_2->setGeometry(QRect(480, 100, 251, 16));
    enterPricepType = new QLineEdit(Form);
    enterPricepType->setObjectName(QString::fromUtf8("enterPricepType"));
    enterPricepType->setGeometry(QRect(500, 80, 201, 20));
    labelAuto_3 = new QLabel(Form);
    labelAuto_3->setObjectName(QString::fromUtf8("labelAuto_3"));
    labelAuto_3->setGeometry(QRect(710, 80, 101, 16));
    enterTypePerevoski = new QLineEdit(Form);
    enterTypePerevoski->setObjectName(QString::fromUtf8("enterTypePerevoski"));
    enterTypePerevoski->setGeometry(QRect(810, 80, 271, 20));
    labelAutoTransporter = new QLabel(Form);
    labelAutoTransporter->setObjectName(QString::fromUtf8("labelAutoTransporter"));
    labelAutoTransporter->setGeometry(QRect(30, 130, 161, 16));
    enterAutoTransporter = new QLineEdit(Form);
    enterAutoTransporter->setObjectName(QString::fromUtf8("enterAutoTransporter"));
    enterAutoTransporter->setGeometry(QRect(190, 130, 511, 20));
    labelAutoVoditel = new QLabel(Form);
    labelAutoVoditel->setObjectName(QString::fromUtf8("labelAutoVoditel"));
    labelAutoVoditel->setGeometry(QRect(710, 130, 41, 16));
    enterNameTransporter = new QLineEdit(Form);
    enterNameTransporter->setObjectName(QString::fromUtf8("enterNameTransporter"));
    enterNameTransporter->setGeometry(QRect(760, 130, 321, 20));
    labelAutoTransporterFIO = new QLabel(Form);
    labelAutoTransporterFIO->setObjectName(QString::fromUtf8("labelAutoTransporterFIO"));
    labelAutoTransporterFIO->setGeometry(QRect(390, 150, 131, 16));
    labelAutoTransporterLicenceNumber = new QLabel(Form);
    labelAutoTransporterLicenceNumber->setObjectName(QString::fromUtf8("labelAutoTransporterLicenceNumber"));
    labelAutoTransporterLicenceNumber->setGeometry(QRect(830, 150, 161, 16));
    labelOrderer = new QLabel(Form);
    labelOrderer->setObjectName(QString::fromUtf8("labelOrderer"));
    labelOrderer->setGeometry(QRect(30, 180, 61, 16));
    enterEnterOrder = new QLineEdit(Form);
    enterEnterOrder->setObjectName(QString::fromUtf8("enterEnterOrder"));
    enterEnterOrder->setGeometry(QRect(120, 180, 961, 20));
    labelAutoOrdererFIO = new QLabel(Form);
    labelAutoOrdererFIO->setObjectName(QString::fromUtf8("labelAutoOrdererFIO"));
    labelAutoOrdererFIO->setGeometry(QRect(480, 200, 131, 16));
    labelSender = new QLabel(Form);
    labelSender->setObjectName(QString::fromUtf8("labelSender"));
    labelSender->setGeometry(QRect(30, 230, 131, 16));
    enterSender = new QLineEdit(Form);
    enterSender->setObjectName(QString::fromUtf8("enterSender"));
    enterSender->setGeometry(QRect(160, 230, 921, 20));
    labelAutoSenderFIO = new QLabel(Form);
    labelAutoSenderFIO->setObjectName(QString::fromUtf8("labelAutoSenderFIO"));
    labelAutoSenderFIO->setGeometry(QRect(370, 250, 411, 16));
    labelAccepter = new QLabel(Form);
    labelAccepter->setObjectName(QString::fromUtf8("labelAccepter"));
    labelAccepter->setGeometry(QRect(30, 280, 121, 16));
    labelAutoAccepterFIO = new QLabel(Form);
    labelAutoAccepterFIO->setObjectName(QString::fromUtf8("labelAutoAccepterFIO"));
    labelAutoAccepterFIO->setGeometry(QRect(370, 300, 371, 16));
    enterAccepter = new QLineEdit(Form);
    enterAccepter->setObjectName(QString::fromUtf8("enterAccepter"));
    enterAccepter->setGeometry(QRect(160, 280, 921, 20));
    labelPointOfLoad = new QLabel(Form);
    labelPointOfLoad->setObjectName(QString::fromUtf8("labelPointOfLoad"));
    labelPointOfLoad->setGeometry(QRect(30, 330, 131, 16));
    enterPointOfLoading = new QLineEdit(Form);
    enterPointOfLoading->setObjectName(QString::fromUtf8("enterPointOfLoading"));
    enterPointOfLoading->setGeometry(QRect(160, 330, 361, 20));
    labelAddress1 = new QLabel(Form);
    labelAddress1->setObjectName(QString::fromUtf8("labelAddress1"));
    labelAddress1->setGeometry(QRect(280, 350, 131, 16));
    labelPointUnLoad = new QLabel(Form);
    labelPointUnLoad->setObjectName(QString::fromUtf8("labelPointUnLoad"));
    labelPointUnLoad->setGeometry(QRect(530, 330, 151, 16));
    labelAddress2 = new QLabel(Form);
    labelAddress2->setObjectName(QString::fromUtf8("labelAddress2"));
    labelAddress2->setGeometry(QRect(790, 350, 131, 16));
    enterPointOf_UnLoading = new QLineEdit(Form);
    enterPointOf_UnLoading->setObjectName(QString::fromUtf8("enterPointOf_UnLoading"));
    enterPointOf_UnLoading->setGeometry(QRect(680, 330, 401, 20));
    labelPereaddress = new QLabel(Form);
    labelPereaddress->setObjectName(QString::fromUtf8("labelPereaddress"));
    labelPereaddress->setGeometry(QRect(30, 380, 191, 16));
    enterPereaddres = new QLineEdit(Form);
    enterPereaddres->setObjectName(QString::fromUtf8("enterPereaddres"));
    enterPereaddres->setGeometry(QRect(220, 380, 861, 20));
    labelNamePereAddr = new QLabel(Form);
    labelNamePereAddr->setObjectName(QString::fromUtf8("labelNamePereAddr"));
    labelNamePereAddr->setGeometry(QRect(400, 400, 421, 16));
    labelDoverenost = new QLabel(Form);
    labelDoverenost->setObjectName(QString::fromUtf8("labelDoverenost"));
    labelDoverenost->setGeometry(QRect(30, 430, 311, 16));
    enterSeriaNumber = new QLineEdit(Form);
    enterSeriaNumber->setObjectName(QString::fromUtf8("enterSeriaNumber"));
    enterSeriaNumber->setGeometry(QRect(340, 430, 41, 20));
    labelNumber = new QLabel(Form);
    labelNumber->setObjectName(QString::fromUtf8("labelNumber"));
    labelNumber->setGeometry(QRect(390, 430, 21, 16));
    enterNumberDoverennost = new QLineEdit(Form);
    enterNumberDoverennost->setObjectName(QString::fromUtf8("enterNumberDoverennost"));
    enterNumberDoverennost->setGeometry(QRect(410, 430, 51, 20));
    labelNumber_2 = new QLabel(Form);
    labelNumber_2->setObjectName(QString::fromUtf8("labelNumber_2"));
    labelNumber_2->setGeometry(QRect(470, 430, 21, 16));
    enterDoverennostDate = new QLineEdit(Form);
    enterDoverennostDate->setObjectName(QString::fromUtf8("enterDoverennostDate"));
    enterDoverennostDate->setGeometry(QRect(500, 430, 31, 20));
    enterDoverennostMonth = new QLineEdit(Form);
    enterDoverennostMonth->setObjectName(QString::fromUtf8("enterDoverennostMonth"));
    enterDoverennostMonth->setGeometry(QRect(540, 430, 101, 20));
    enterDoverennostYear = new QLineEdit(Form);
    enterDoverennostYear->setObjectName(QString::fromUtf8("enterDoverennostYear"));
    enterDoverennostYear->setGeometry(QRect(680, 430, 31, 20));
    labelNum_4 = new QLabel(Form);
    labelNum_4->setObjectName(QString::fromUtf8("labelNum_4"));
    labelNum_4->setGeometry(QRect(650, 430, 21, 16));
    labelNum_5 = new QLabel(Form);
    labelNum_5->setObjectName(QString::fromUtf8("labelNum_5"));
    labelNum_5->setGeometry(QRect(720, 430, 81, 16));
    enterRules = new QLineEdit(Form);
    enterRules->setObjectName(QString::fromUtf8("enterRules"));
    enterRules->setGeometry(QRect(930, 470, 151, 20));
    labelVantash = new QLabel(Form);
    labelVantash->setObjectName(QString::fromUtf8("labelVantash"));
    labelVantash->setGeometry(QRect(30, 470, 301, 16));
    enterGoodsState = new QLineEdit(Form);
    enterGoodsState->setObjectName(QString::fromUtf8("enterGoodsState"));
    enterGoodsState->setGeometry(QRect(330, 470, 151, 20));
    labelNum_6 = new QLabel(Form);
    labelNum_6->setObjectName(QString::fromUtf8("labelNum_6"));
    labelNum_6->setGeometry(QRect(330, 490, 181, 16));
    labelRulsTransport = new QLabel(Form);
    labelRulsTransport->setObjectName(QString::fromUtf8("labelRulsTransport"));
    labelRulsTransport->setGeometry(QRect(490, 470, 441, 20));
    enterDoverennostVidano = new QLineEdit(Form);
    enterDoverennostVidano->setObjectName(QString::fromUtf8("enterDoverennostVidano"));
    enterDoverennostVidano->setGeometry(QRect(800, 430, 281, 20));
    labelNumberPlaces = new QLabel(Form);
    labelNumberPlaces->setObjectName(QString::fromUtf8("labelNumberPlaces"));
    labelNumberPlaces->setGeometry(QRect(30, 520, 111, 16));
    enterNetto = new QLineEdit(Form);
    enterNetto->setObjectName(QString::fromUtf8("enterNetto"));
    enterNetto->setGeometry(QRect(140, 520, 171, 20));
    labelBuWords = new QLabel(Form);
    labelBuWords->setObjectName(QString::fromUtf8("labelBuWords"));
    labelBuWords->setGeometry(QRect(140, 540, 71, 16));
    labelBrutto = new QLabel(Form);
    labelBrutto->setObjectName(QString::fromUtf8("labelBrutto"));
    labelBrutto->setGeometry(QRect(330, 520, 101, 16));
    enterBrutto = new QLineEdit(Form);
    enterBrutto->setObjectName(QString::fromUtf8("enterBrutto"));
    enterBrutto->setGeometry(QRect(430, 520, 201, 20));
    enterEcpeditor = new QLineEdit(Form);
    enterEcpeditor->setObjectName(QString::fromUtf8("enterEcpeditor"));
    enterEcpeditor->setGeometry(QRect(820, 520, 261, 20));
    labelFIO_Excpeditor = new QLabel(Form);
    labelFIO_Excpeditor->setObjectName(QString::fromUtf8("labelFIO_Excpeditor"));
    labelFIO_Excpeditor->setGeometry(QRect(790, 540, 141, 16));
    labelBuWords_2 = new QLabel(Form);
    labelBuWords_2->setObjectName(QString::fromUtf8("labelBuWords_2"));
    labelBuWords_2->setGeometry(QRect(360, 540, 81, 16));
    labelBrutto_2 = new QLabel(Form);
    labelBrutto_2->setObjectName(QString::fromUtf8("labelBrutto_2"));
    labelBrutto_2->setGeometry(QRect(638, 520, 168, 16));
    labelBuhgalterName = new QLabel(Form);
    labelBuhgalterName->setObjectName(QString::fromUtf8("labelBuhgalterName"));
    labelBuhgalterName->setGeometry(QRect(30, 570, 331, 16));
    enterBuhgalterName = new QLineEdit(Form);
    enterBuhgalterName->setObjectName(QString::fromUtf8("enterBuhgalterName"));
    enterBuhgalterName->setGeometry(QRect(360, 570, 281, 20));
    labelFIO_Buhgalter = new QLabel(Form);
    labelFIO_Buhgalter->setObjectName(QString::fromUtf8("labelFIO_Buhgalter"));
    labelFIO_Buhgalter->setGeometry(QRect(400, 590, 141, 16));
    labelBuhgalterName_2 = new QLabel(Form);
    labelBuhgalterName_2->setObjectName(QString::fromUtf8("labelBuhgalterName_2"));
    labelBuhgalterName_2->setGeometry(QRect(650, 570, 111, 16));
    enterBuhgalterName_2 = new QLineEdit(Form);
    enterBuhgalterName_2->setObjectName(QString::fromUtf8("enterBuhgalterName_2"));
    enterBuhgalterName_2->setGeometry(QRect(780, 570, 301, 20));
    labelFIO_Buhgalter_2 = new QLabel(Form);
    labelFIO_Buhgalter_2->setObjectName(QString::fromUtf8("labelFIO_Buhgalter_2"));
    labelFIO_Buhgalter_2->setGeometry(QRect(830, 590, 201, 16));
    labelTotalSold = new QLabel(Form);
    labelTotalSold->setObjectName(QString::fromUtf8("labelTotalSold"));
    labelTotalSold->setGeometry(QRect(30, 620, 221, 16));
    enterTotalSold = new QLineEdit(Form);
    enterTotalSold->setObjectName(QString::fromUtf8("enterTotalSold"));
    enterTotalSold->setGeometry(QRect(250, 620, 571, 20));
    labelPDV = new QLabel(Form);
    labelPDV->setObjectName(QString::fromUtf8("labelPDV"));
    labelPDV->setGeometry(QRect(830, 620, 81, 16));
    enterPDV = new QLineEdit(Form);
    enterPDV->setObjectName(QString::fromUtf8("enterPDV"));
    enterPDV->setGeometry(QRect(910, 620, 171, 20));
    labelSuprovodniDoc = new QLabel(Form);
    labelSuprovodniDoc->setObjectName(QString::fromUtf8("labelSuprovodniDoc"));
    labelSuprovodniDoc->setGeometry(QRect(30, 660, 211, 16));
    enterSuprovodniDoc = new QLineEdit(Form);
    enterSuprovodniDoc->setObjectName(QString::fromUtf8("enterSuprovodniDoc"));
    enterSuprovodniDoc->setGeometry(QRect(250, 660, 831, 20));
    labelTransportServices = new QLabel(Form);
    labelTransportServices->setObjectName(QString::fromUtf8("labelTransportServices"));
    labelTransportServices->setGeometry(QRect(30, 700, 411, 16));
    enterTransportServices = new QLineEdit(Form);
    enterTransportServices->setObjectName(QString::fromUtf8("enterTransportServices"));
    enterTransportServices->setGeometry(QRect(440, 700, 641, 20));
    enterOthers = new QLineEdit(Form);
    enterOthers->setObjectName(QString::fromUtf8("enterOthers"));
    enterOthers->setGeometry(QRect(30, 740, 1051, 20));
    
	pushButtonAccept = new QPushButton(Form);
    pushButtonAccept->setObjectName(QString::fromUtf8("pushButtonAccept"));
    pushButtonAccept->setGeometry( QRect( 268, 770, 171, 31 ) );
	connect( pushButtonAccept, SIGNAL( clicked() ), object, SLOT( pushButtonAccept_Slot() ) );
    
	pushButtonClose = new QPushButton(Form);
    pushButtonClose->setObjectName(QString::fromUtf8("pushButtonClose"));
    pushButtonClose->setGeometry( QRect( 480, 770, 171, 31 ) );
	connect( pushButtonClose, SIGNAL( clicked() ), this, SLOT( hideTTN() ) );

	pushButtonPrint = new QPushButton(Form);
    pushButtonPrint->setObjectName(QString::fromUtf8("pushButtonPrint"));	
    pushButtonPrint->setGeometry( QRect( 691, 770, 171, 31 ) );
	connect( pushButtonPrint, SIGNAL( clicked() ), this, SLOT( pushButtonPrint_Slot() ) );

	// 
	pushButtonTTN_2 = new QPushButton( "��� ����� 2", Form );
    pushButtonTTN_2->setObjectName(QString::fromUtf8("pushButtonTTN_2"));	
    pushButtonTTN_2->setGeometry( QRect( 10, 770, 100, 31 ) );
	connect( pushButtonTTN_2, SIGNAL( clicked() ), this, SLOT( pushButtonTTN_2_Slot() ) );
	// 
	dialogTTN_2 = new QDialog();
	dialogTTN_2->setModal( true );
	dialogTTN_2->setGeometry( 320, 320, 1117, 320 );
	dialogTTN_2->setWindowTitle( "����� ���-����� 2" );

	buttonSave = new QPushButton( "���������", dialogTTN_2 );
	buttonSave->setGeometry( 32, 186, 140, 30 );
	connect( buttonSave, SIGNAL( clicked() ), this, SLOT( saveTTN_2() ) );

	buttonClose = new QPushButton( "�������", dialogTTN_2 );
	buttonClose->setGeometry( 32 + 150, 186, 140, 30 );
	connect( buttonClose, SIGNAL( clicked() ), this, SLOT( closeTTN_2() ) );


		// 
	QLabel *labelZdav = new QLabel( "���� (����������� ����� �����������������)", dialogTTN_2 );
	labelZdav->setGeometry( 32, 240, 320, 18 );
	enterVidpovidalnaOsoba = new QLineEdit( dialogTTN_2 );
	enterVidpovidalnaOsoba->setGeometry( 32, 268, 320, 24 );

	QLabel *labelPrinyav = new QLabel( "������� ����/����������", dialogTTN_2 );
	labelPrinyav->setGeometry( 32 + 320 + 48, 240, 320, 18 );
	enterPriynyavVodiyExpeditor = new QLineEdit( dialogTTN_2 );
	enterPriynyavVodiyExpeditor->setGeometry( 32 + 290 + 30, 268, 240, 24 );

	QLabel *labelZdavExpeditor = new QLabel( "���� ����/����������", dialogTTN_2 );
	labelZdavExpeditor->setGeometry( 32 + 320 + 240 + 48, 240, 320, 18 );
	enterZdavVodiyExpeditor = new QLineEdit( dialogTTN_2 );
	enterZdavVodiyExpeditor->setGeometry( 32 + 290 + 30 + 240, 268, 240, 24 );

	QLabel *labelPriynyav = new QLabel( "������� (����������� ����� \r\n �����������������)", dialogTTN_2 );
	labelPriynyav->setGeometry( 32 + 290 + 30 + 320 + 195, 232, 320, 36 );
	enterPriynyavVidpovidalnaOsoba = new QLineEdit( dialogTTN_2 );
	enterPriynyavVidpovidalnaOsoba->setGeometry( 32 + 290 + 30 + 240 + 240, 268, 240, 24 );

	dialogTTN_2->hide();
	// 
	pushButtonTTN_3 = new QPushButton( "��� ����� 3", Form );
    pushButtonTTN_3->setObjectName(QString::fromUtf8("pushButtonTTN_3"));	
    pushButtonTTN_3->setGeometry( QRect( 140, 770, 100, 31 ) );
	connect( pushButtonTTN_3, SIGNAL( clicked() ), this, SLOT( pushButtonTTN_3_Slot() ) );
	// 
	dialogTTN_3 = new QDialog();
	dialogTTN_3->setModal( true );
	dialogTTN_3->setGeometry( 320, 320, 1117, 240 );
	dialogTTN_3->setWindowTitle( "����� ���-����� 3" );
	// 

	QLabel *labelVantagnoRozvantagniRoboti = new QLabel( "�������-��������������Ͳ �����ֲ�", dialogTTN_3 );
	labelVantagnoRozvantagniRoboti->setGeometry( 480, 32, 280, 18 );
	labelVantagnoRozvantagniRoboti->setStyleSheet( "color: #000; font-size: 19px; " );

    tableWidget2 = new QTableWidget( dialogTTN_3 );
    tableWidget2->setObjectName( QString::fromUtf8( "tableWidget2" ) );
    tableWidget2->setGeometry( QRect( 10, 24, 1107, 140 ) );

    tableWidget2->setVerticalScrollBarPolicy( Qt::ScrollBarAsNeeded );
	tableWidget2->setHorizontalScrollBarPolicy( Qt::ScrollBarAsNeeded );
	tableWidget2->setEditTriggers( QAbstractItemView::NoEditTriggers ); // ��������� ������������� ������ 

	int cntColumns = getHeadersTTN_3().count(); // ���������� ������� � ������� 
	QStringList headersText = getHeadersTTN_3(); // ������� ��� ��������  �������  �� ������ 
	tableWidget2->setColumnCount( cntColumns ); // ???

	tableWidget2->setStyleSheet( "font-weight: bold; font-size: 12px;" ); // ???
	tableWidget2->setColumnWidth( 0, 140 );
	for(int i = 1; i <= 5; i++)
		tableWidget2->setColumnWidth(i, 180);
	
	tableWidget2->setAlternatingRowColors( true );

	// ��������� ������� � ������� 
	for( int j = 0; j < cntColumns; j++ )
	{
		tableWidget2->setHorizontalHeaderLabels( headersText ); 
	}

	operation = new QLineEdit( tableWidget2 );
	operation->setGeometry( 3, 43, 140, 24 );
	operation_2 = new QLineEdit( tableWidget2 );
	operation_2->setGeometry( 3, 43 + 25, 140, 24 );

	massaBrutto2 = new QLineEdit( tableWidget2 );
	massaBrutto2->setGeometry( 3 + 140, 43, 180, 24 );
	massaBrutto2_2 = new QLineEdit( tableWidget2 );
	massaBrutto2_2->setGeometry( 3 + 140, 43 + 25, 180, 24 );

	pributtya = new QLineEdit( tableWidget2 );
	pributtya->setGeometry( 3 + 140 + 180, 43, 180, 24 );
	pributtya_2 = new QLineEdit( tableWidget2 );
	pributtya_2->setGeometry( 3 + 140 + 180, 43 + 25, 180, 24 );

	vibuttya = new QLineEdit( tableWidget2 );
	vibuttya->setGeometry( 3 + 140 + 180 + 180, 43, 180, 24 );
	vibuttya_2 = new QLineEdit( tableWidget2 );
	vibuttya_2->setGeometry( 3 + 140 + 180 + 180, 43 + 25, 180, 24 );

	prostoyu = new QLineEdit( tableWidget2 );
	prostoyu->setGeometry( 3 + 140 + 180 + 180 + 180, 43, 180, 24 );
	prostoyu_2 = new QLineEdit( tableWidget2 );
	prostoyu_2->setGeometry( 3 + 140 + 180 + 180 + 180, 43 + 25, 180, 24 );

	pidpisVidpovidalnOsobi = new QLineEdit( tableWidget2 );
	pidpisVidpovidalnOsobi->setGeometry( 3 + 140 + 180 + 180 + 180 + 180, 43, 180, 24 );
	pidpisVidpovidalnOsobi_2 = new QLineEdit( tableWidget2 );
	pidpisVidpovidalnOsobi_2->setGeometry( 3 + 140 + 180 + 180 + 180 + 180, 43 + 25, 180, 24 );

	// 
	buttonSave2 = new QPushButton( "���������", dialogTTN_3 );
	buttonSave2->setGeometry( 32, 186, 140, 30 );
	connect( buttonSave2, SIGNAL( clicked() ), this, SLOT( saveTTN_3() ) );

	buttonClose2 = new QPushButton( "�������", dialogTTN_3 );
	buttonClose2->setGeometry( 32 + 150, 186, 140, 30 );
	connect( buttonClose2, SIGNAL( clicked() ), this, SLOT( closeTTN_3() ) );

	dialogTTN_3->hide();

	// 
	// ��� ������ ����� 
	label_3 = new QLabel( dialogTTN_2 );
    label_3->setObjectName(QString::fromUtf8("label_3"));
	label_3->setGeometry(QRect(dialogTTN_2->width() / 4, 32, 186, 16));

	tableWidget = new QTableWidget( dialogTTN_2 );
    tableWidget->setObjectName( QString::fromUtf8( "tableWidget" ) );
    tableWidget->setGeometry( QRect( 10, 24, 1107, 140 ) );

    tableWidget->setVerticalScrollBarPolicy( Qt::ScrollBarAsNeeded );
	tableWidget->setHorizontalScrollBarPolicy( Qt::ScrollBarAsNeeded );
	tableWidget->setEditTriggers( QAbstractItemView::NoEditTriggers ); // ��������� ������������� ������ 

	cntColumns = getHeadersTTN().count(); // ���������� ������� � ������� 
	headersText = getHeadersTTN(); // ������� ��� ��������  �������  �� ������ 
	tableWidget->setColumnCount( cntColumns ); // ???

	tableWidget->setStyleSheet( "font-weight: bold; font-size: 12px;" ); // ???
	int arr[] = {32, 295, 110, 110, 110, 110, 90, 150, 100};
	for(int i = 0; i <= sizeof(arr)/sizeof(arr[0]); i++)
		tableWidget->setColumnWidth(i, arr[i]);	

	tableWidget->setAlternatingRowColors( true );

	// ��������� ������� � ������� 
	for( int j = 0; j < cntColumns; j++ )
	{
		tableWidget->setHorizontalHeaderLabels( headersText ); 
	}

	numPoint = new QLineEdit( tableWidget );
	numPoint->setGeometry( 3, 74, 32, 24 );

	textOfTTN_2 = new QLineEdit( tableWidget );
	textOfTTN_2->setGeometry( 3 + 32, 74, 295, 24 );

	edIsm = new QLineEdit( tableWidget );
	edIsm->setGeometry( 3 + 32 + 295, 74, 110, 24 );

	numPlaces = new QLineEdit( tableWidget );
	numPlaces->setGeometry( 3 + 32 + 295 + 110, 74, 110, 24 );
	numPlaces->setReadOnly( true );

	priceBezPDV = new QLineEdit( tableWidget );
	priceBezPDV->setGeometry( 3 + 32 + 295 + 110 + 110, 74, 110, 24 );

	zagalnaSummaS_PDV = new QLineEdit( tableWidget );
	zagalnaSummaS_PDV->setGeometry( 3 + 32 + 295 + 110 + 110 + 110, 74, 110, 24 );

	vidPakuvannya = new QLineEdit( tableWidget );
	vidPakuvannya->setGeometry( 3 + 32 + 295 + 110 + 110 + 110 + 110, 74, 90, 24 );

	docsWithLoad = new QLineEdit( tableWidget );
	docsWithLoad->setGeometry( 3 + 32 + 295 + 110 + 110 + 110 + 110 + 90, 74, 150, 24 );

	massaBrutto = new QLineEdit( tableWidget );
	massaBrutto->setGeometry( 3 + 32 + 295 + 110 + 110 + 110 + 110 + 90 + 150, 74, 100, 24 );
	massaBrutto->setReadOnly( true );

    retranslateUi(Form);

    QMetaObject::connectSlotsByName(Form);



  //  static QString[ , ] TableConvertIntToStringUKR = new string[ , ] { //  ???  Correct the grammar ???
  //      { string.Empty, "����", "��i", "���", "������", "�`���", "�i���", "�i��", "�i�i��", "���`���" },
  //      { string.Empty, "������", "��������", "��������", "�����", "�`���������", "�i���������", "�i��������", "�i�i��������", "���`������" },
  //      { string.Empty, "���", "��i��i", "������", "���������", "�`������", "�i������", "�i�����", "�i�i�����", "���`������" },
  //      { string.Empty, "����", "��i", "���", "������", "�`���", "�i���", "�i��", "�i�i��", "���`���" },
  //      { string.Empty, "������", "��������", "��������", "�����", "�`���������", "�i���������", "�i��������", "�i�i��������", "���`������" },
  //      { string.Empty, "���", "��i��i", "������", "���������", "�`������", "�i������", "�i�����", "�i�i�����", "���`������" },
  //      { string.Empty,  "�i�����", "��� �i������", "��� �i������", "������ �i������", "�`��� �i�����i�", "�i��� �i�����i�", "�i�� �i�����i�", "�i�i�� �i�����i�", "���`��� �i�����i�" },

		//};

} // setupUi

// 
void Form_ttn::retranslateUi( QWidget *Form )
{
    Form->setWindowTitle(QApplication::translate("Form", "\320\244\320\276\321\200\320\274\320\260 \320\242\320\242\320\235", 0, QApplication::UnicodeUTF8));
        labelTitle->setText(QApplication::translate("Form", "\320\242\320\236\320\222\320\220\320\240\320\235\320\236-\320\242\320\240\320\220\320\235\320\241\320\237\320\236\320\240\320\242\320\235\320\220 \320\235\320\220\320\232\320\233\320\220\320\224\320\235\320\220", 0, QApplication::UnicodeUTF8));
        enterNumberNakladnaya->setText(QString());
        labelNum->setText(QApplication::translate("Form", "\342\204\226", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Form", "\"", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Form", "\"", 0, QApplication::UnicodeUTF8));
        labelNum_2->setText(QApplication::translate("Form", "20", 0, QApplication::UnicodeUTF8));
        labelNum_3->setText(QApplication::translate("Form", "\321\200.", 0, QApplication::UnicodeUTF8));
        labelAuto->setText(QApplication::translate("Form", "\320\220\320\262\321\202\320\276\320\274\320\276\320\261\321\226\320\273\321\214", 0, QApplication::UnicodeUTF8));
        labelAutoTypes->setText(QApplication::translate("Form", "(\320\274\320\260\321\200\320\272\320\260, \320\274\320\276\320\264\320\265\320\273\321\214, \321\202\320\270\320\277, \321\200\320\265\321\224\321\201\321\202\321\200\320\260\321\206\321\226\320\271\320\275\320\270\320\271 \320\275\320\276\320\274\320\265\321\200)", 0, QApplication::UnicodeUTF8));
        labelAuto_2->setText(QApplication::translate("Form", "\320\237\321\200\320\270\321\207\321\226\320\277/\320\275\320\260\320\277\321\226\320\262\320\277\321\200\320\270\321\207\321\226\320\277", 0, QApplication::UnicodeUTF8));
        labelAutoTypes_2->setText(QApplication::translate("Form", "(\320\274\320\260\321\200\320\272\320\260, \320\274\320\276\320\264\320\265\320\273\321\214, \321\202\320\270\320\277, \321\200\320\265\321\224\321\201\321\202\321\200\320\260\321\206\321\226\320\271\320\275\320\270\320\271 \320\275\320\276\320\274\320\265\321\200)", 0, QApplication::UnicodeUTF8));
        labelAuto_3->setText(QApplication::translate("Form", "\320\222\320\270\320\264 \320\277\320\265\321\200\320\265\320\262\320\265\320\267\320\265\320\275\321\214", 0, QApplication::UnicodeUTF8));
        labelAutoTransporter->setText(QApplication::translate("Form", "\320\220\320\262\321\202\320\276\320\274\320\276\320\261\321\226\320\273\321\214\320\275\320\270\320\271 \320\277\320\265\321\200\320\265\320\262\320\265\320\267\320\275\320\270\320\272", 0, QApplication::UnicodeUTF8));
        labelAutoVoditel->setText(QApplication::translate("Form", "\320\222\320\276\320\264\321\226\320\271", 0, QApplication::UnicodeUTF8));
        labelAutoTransporterFIO->setText(QApplication::translate("Form", "(\320\275\320\260\320\271\320\274\320\265\320\275\321\203\320\262\320\260\320\275\320\275\321\217 \320\237.\320\206.\320\221)", 0, QApplication::UnicodeUTF8));
        labelAutoTransporterLicenceNumber->setText(QApplication::translate("Form", "(\320\237.\320\206.\320\221, \320\277\320\276\321\201\320\262\321\226\320\264\321\207\320\265\320\275\320\275\321\217 \320\262\320\276\320\264\321\226\321\217)", 0, QApplication::UnicodeUTF8));
        labelOrderer->setText(QApplication::translate("Form", "\320\227\320\260\320\274\320\276\320\262\320\275\320\270\320\272 ", 0, QApplication::UnicodeUTF8));
        labelAutoOrdererFIO->setText(QApplication::translate("Form", "(\320\275\320\260\320\271\320\274\320\265\320\275\321\203\320\262\320\260\320\275\320\275\321\217 \320\237.\320\206.\320\221)", 0, QApplication::UnicodeUTF8));
        labelSender->setText(QApplication::translate("Form", "\320\222\320\260\320\275\321\202\320\260\320\266\320\276\320\262\321\226\320\264\320\277\321\200\320\260\320\262\320\275\320\270\320\272", 0, QApplication::UnicodeUTF8));
        labelAutoSenderFIO->setText(QApplication::translate("Form", "(\320\277\320\276\320\262\320\275\320\265 \320\275\320\260\320\271\320\274\320\265\320\275\321\203\320\262\320\260\320\275\320\275\321\217, \320\274\321\226\321\201\321\206\320\265\320\267\320\275\320\260\321\205\320\276\320\264\320\266\320\265\320\275\320\275\321\217/\320\237.\320\206.\320\221., \320\274\321\226\321\201\321\206\320\265 \320\277\321\200\320\276\320\266\320\270\320\262\320\260\320\275\320\275\321\217)", 0, QApplication::UnicodeUTF8));
        labelAccepter->setText(QApplication::translate("Form", "\320\222\320\260\320\275\321\202\320\260\320\266\320\276\320\276\320\264\320\265\321\200\320\266\321\203\320\262\320\260\321\207", 0, QApplication::UnicodeUTF8));
        labelAutoAccepterFIO->setText(QApplication::translate("Form", "(\320\277\320\276\320\262\320\275\320\265 \320\275\320\260\320\271\320\274\320\265\320\275\321\203\320\262\320\260\320\275\320\275\321\217, \320\274\321\226\321\201\321\206\320\265\320\267\320\275\320\260\321\205\320\276\320\264\320\266\320\265\320\275\320\275\321\217/\320\237.\320\206.\320\221., \320\274\321\226\321\201\321\206\320\265 \320\277\321\200\320\276\320\266\320\270\320\262\320\260\320\275\320\275\321\217)", 0, QApplication::UnicodeUTF8));
        labelPointOfLoad->setText(QApplication::translate("Form", "\320\237\321\203\320\275\320\272\321\202 \320\275\320\260\320\262\320\260\320\275\321\202\320\260\320\266\320\265\320\275\320\275\321\217", 0, QApplication::UnicodeUTF8));
        labelAddress1->setText(QApplication::translate("Form", "(\320\274\321\226\321\201\321\206\320\265\320\267\320\275\320\260\321\205\320\276\320\264\320\266\320\265\320\275\320\275\321\217)", 0, QApplication::UnicodeUTF8));
        labelPointUnLoad->setText(QApplication::translate("Form", "\320\237\321\203\320\275\320\272\321\202 \321\200\320\276\320\267\320\262\320\260\320\275\321\202\320\260\320\266\320\265\320\275\320\275\321\217", 0, QApplication::UnicodeUTF8));
        labelAddress2->setText(QApplication::translate("Form", "(\320\274\321\226\321\201\321\206\320\265\320\267\320\275\320\260\321\205\320\276\320\264\320\266\320\265\320\275\320\275\321\217)", 0, QApplication::UnicodeUTF8));
        labelPereaddress->setText(QApplication::translate("Form", "\320\237\320\265\321\200\320\265\320\260\320\264\321\200\320\265\321\201\321\203\320\262\320\260\320\275\320\275\321\217 \320\262\320\260\320\275\321\202\320\260\320\266\321\203", 0, QApplication::UnicodeUTF8));
        labelNamePereAddr->setText(QApplication::translate("Form", "(\320\277\320\276\320\262\320\275\320\265 \320\275\320\260\320\271\320\274\320\265\320\275\321\203\320\262\320\260\320\275\320\275\321\217, \320\274\321\226\321\201\321\206\320\265\320\267\320\275\320\260\321\205\320\276\320\264\320\266\320\265\320\275\320\275\321\217/\320\237.\320\206.\320\221., \320\274\321\226\321\201\321\206\320\265 \320\277\321\200\320\276\320\266\320\270\320\262\320\260\320\275\320\275\321\217)", 0, QApplication::UnicodeUTF8));
        labelDoverenost->setText(QApplication::translate("Form", "\320\222\321\226\320\264\320\277\321\203\321\201\320\272 \320\267\320\260 \320\264\320\276\320\262\321\226\321\200\320\265\320\275\321\226\321\201\321\202\321\216 \320\262\320\260\320\275\321\202\320\260\320\266\320\276\320\276\320\264\320\265\321\200\320\266\321\203\320\262\320\260\321\207\320\260: \321\201\320\265\321\200\321\226\321\217", 0, QApplication::UnicodeUTF8));
        labelNumber->setText(QApplication::translate("Form", "\342\204\226", 0, QApplication::UnicodeUTF8));
        labelNumber_2->setText(QApplication::translate("Form", "\320\262\321\226\320\264", 0, QApplication::UnicodeUTF8));
        labelNum_4->setText(QApplication::translate("Form", "20", 0, QApplication::UnicodeUTF8));
        labelNum_5->setText(QApplication::translate("Form", "\321\200., \320\262\320\270\320\264\320\260\320\275\320\276\321\216", 0, QApplication::UnicodeUTF8));
        labelVantash->setText(QApplication::translate("Form", "\320\222\320\260\320\275\321\202\320\260\320\266 \320\275\320\260\320\264\320\260\320\275\320\270\320\271 \320\264\320\273\321\217 \320\277\320\265\321\200\320\265\320\262\320\265\320\267\320\265\320\275\320\275\321\217 \321\203 \321\201\321\202\320\260\320\275\321\226, \321\211\320\276", 0, QApplication::UnicodeUTF8));
        labelNum_6->setText(QApplication::translate("Form", "(\320\262\321\226\320\264\320\277\320\276\320\262\321\226\320\264\320\260\321\224, \320\275\320\265 \320\262\321\226\320\264\320\277\320\276\320\262\321\226\320\264\320\260\321\224)", 0, QApplication::UnicodeUTF8));
        labelRulsTransport->setText(QApplication::translate("Form", "\320\277\321\200\320\260\320\262\320\270\320\273\320\260\320\274 \320\277\320\265\321\200\320\265\320\262\320\265\320\267\320\265\320\275\321\214 \320\262\321\226\320\264\320\277\320\276\320\262\321\226\320\264\320\275\320\270\321\205 \320\262\320\260\320\275\321\202\320\260\320\266\321\226\320\262, \320\275\320\276\320\274\320\265\321\200 \320\277\320\273\320\276\320\274\320\261\320\270 (\320\267\320\260 \320\275\320\260\321\217\320\262\320\275\320\276\321\201\321\202\321\226)", 0, QApplication::UnicodeUTF8));
        labelNumberPlaces->setText(QApplication::translate("Form", "\320\232\321\226\320\273\321\214\320\272\321\226\321\201\321\202\321\214 \320\274\321\226\321\201\321\206\321\214", 0, QApplication::UnicodeUTF8));
        labelBuWords->setText(QApplication::translate("Form", "(\321\201\320\273\320\276\320\262\320\260\320\274\320\270)", 0, QApplication::UnicodeUTF8));
        labelBrutto->setText(QApplication::translate("Form", "\320\274\320\260\321\201\320\276\321\216 \320\261\321\200\321\203\321\202\321\202\320\276, \321\202", 0, QApplication::UnicodeUTF8));
        labelFIO_Excpeditor->setText(QApplication::translate("Form", "(\320\237.\320\206.\320\221., \320\277\320\276\321\201\320\260\320\264\320\260, \320\277\321\226\320\264\320\277\320\270\321\201)", 0, QApplication::UnicodeUTF8));
        labelBuWords_2->setText(QApplication::translate("Form", "(\321\201\320\273\320\276\320\262\320\260\320\274\320\270)", 0, QApplication::UnicodeUTF8));
        labelBrutto_2->setText(QApplication::translate("Form", ", \320\276\321\202\321\200\320\270\320\274\320\260\320\262 \320\262\320\276\320\264\321\226\320\271/\320\265\320\272\321\201\320\277\320\265\320\264\320\270\321\202\320\276\321\200", 0, QApplication::UnicodeUTF8));
        labelBuhgalterName->setText(QApplication::translate("Form", "\320\221\321\203\321\205\320\263\320\260\320\273\321\202\320\265\321\200 (\320\262\321\226\320\264\320\277\320\276\320\262\321\226\320\264\320\260\320\273\321\214\320\275\320\260 \320\276\321\201\320\276\320\261\320\260 \320\262\320\260\320\275\321\202\320\260\320\262\321\226\320\264\320\277\321\200\320\260\320\262\320\275\320\270\320\272\320\260)", 0, QApplication::UnicodeUTF8));
        labelFIO_Buhgalter->setText(QApplication::translate("Form", "(\320\237.\320\206.\320\221., \320\277\320\276\321\201\320\260\320\264\320\260, \320\277\321\226\320\264\320\277\320\270\321\201)", 0, QApplication::UnicodeUTF8));
        labelBuhgalterName_2->setText(QApplication::translate("Form", "\320\222\321\226\320\264\320\277\321\203\321\201\320\272 \320\264\320\276\320\267\320\262\320\260\320\273\320\270\320\262", 0, QApplication::UnicodeUTF8));
        labelFIO_Buhgalter_2->setText(QApplication::translate("Form", "(\320\237.\320\206.\320\221., \320\277\320\276\321\201\320\260\320\264\320\260, \320\277\321\226\320\264\320\277\320\270\321\201, \320\277\320\265\321\207\320\260\321\202\320\272\320\260)", 0, QApplication::UnicodeUTF8));
        labelTotalSold->setText(QApplication::translate("Form", "\320\243\321\201\321\214\320\276\320\263\320\276 \320\262\321\226\320\264\320\277\321\203\321\211\320\265\320\275\320\276 \320\275\320\260 \320\267\320\260\320\263\320\260\320\273\321\214\320\275\321\203 \321\201\321\203\320\274\321\203", 0, QApplication::UnicodeUTF8));
        labelPDV->setText(QApplication::translate("Form", ",  \321\203.\321\202.\321\207 \320\237\320\224\320\222", 0, QApplication::UnicodeUTF8));
        labelSuprovodniDoc->setText(QApplication::translate("Form", "\320\241\321\203\320\277\321\200\320\276\320\262\321\226\320\264\320\275\321\226 \320\264\320\276\320\272\321\203\320\274\320\265\320\275\321\202\320\270 \320\275\320\260 \320\262\320\260\320\275\321\202\320\260\320\266", 0, QApplication::UnicodeUTF8));
        labelTransportServices->setText(QApplication::translate("Form", "\320\242\321\200\320\260\320\275\321\201\320\277\320\276\321\200\321\202\320\275\321\226 \320\277\320\276\321\201\320\273\321\203\320\263\320\270, \321\217\320\272\321\226 \320\275\320\260\320\264\320\260\321\216\321\202\321\214\321\201\321\217 \320\260\320\262\321\202\320\276\320\274\320\276\320\261\321\226\320\273\321\214\320\275\320\270\320\274 \320\277\320\265\321\200\320\265\320\262\321\226\320\267\320\275\320\270\320\272\320\276\320\274:", 0, QApplication::UnicodeUTF8));
        pushButtonAccept->setText(QApplication::translate("Form", "\320\237\320\276\320\264\321\202\320\262\320\265\321\200\320\264\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        pushButtonClose->setText(QApplication::translate("Form", "\320\227\320\260\320\272\321\200\321\213\321\202\321\214", 0, QApplication::UnicodeUTF8));
        pushButtonPrint->setText(QApplication::translate("Form", "\320\237\320\265\321\207\320\260\321\202\321\214", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("Form", "\320\222\320\206\320\224\320\236\320\234\320\236\320\241\320\242\320\206 \320\237\320\240\320\236 \320\222\320\220\320\235\320\242\320\220\320\226", 0, QApplication::UnicodeUTF8));

} // retranslateUi

// 
void Form_ttn::showTTN()
{
	// dialogTTN->show();
	scrollArea->show();
}

// 
void Form_ttn::hideTTN()
{
	// dialogTTN->hide();
	scrollArea->hide();
}

// 
void Form_ttn::saveTTN_2()
{
	bool result;
	QSqlError error;
	
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

    query.prepare( QString( "UPDATE ttn_data_2 SET Num=:Num, NameOfGoods=:NameOfGoods, edIsm=:edIsm, Netto=:Netto, PriceWithOutPDV=:PriceWithOutPDV, ZagalnaSummaZ_PDV=:ZagalnaSummaZ_PDV, "
		"VidPakuvannya=:VidPakuvannya, Docs_Z_Vantagem=:Docs_Z_Vantagem, Brutto=:Brutto WHERE date_time='%1';; "
		).arg( dateTimeTTN ) );

	query.bindValue( ":Num", numPoint->text() );
	query.bindValue( ":NameOfGoods", textOfTTN_2->text() ); //
	query.bindValue( ":edIsm", edIsm->text() );
	query.bindValue( ":Netto", numPlaces->text() );
	query.bindValue( ":PriceWithOutPDV", priceBezPDV->text() );
	query.bindValue( ":ZagalnaSummaZ_PDV", zagalnaSummaS_PDV->text() );
	query.bindValue( ":VidPakuvannya", vidPakuvannya->text() );
	query.bindValue( ":Docs_Z_Vantagem", docsWithLoad->text() );
	query.bindValue( ":Brutto", massaBrutto->text() );

	query.exec();
	error = query.lastError();

	if( error.type() == QSqlError::NoError )
	{

	}
	else
	{
        QMessageBox mess;
	    mess.setText( "������ ������ � ��!" );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

	}

	
	query.exec();
	error = query.lastError();

	if( error.type() == QSqlError::NoError )
	{

	}
	else
	{
        QMessageBox mess;
	    mess.setText( "������ ������ � ��!" );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();
	}

	// ��� 4
	result = query.prepare( QString( "UPDATE ttn_data_4 SET vidpovidalnaOsoba=:vidpovidalnaOsoba, priynyavExpeditor=:priynyavExpeditor, zdavExpeditor=:zdavExpeditor, prinyalVidpovOsoba=:prinyalVidpovOsoba WHERE date_time='%1';"
		).arg( dateTimeTTN ) );

	query.bindValue( ":vidpovidalnaOsoba", enterVidpovidalnaOsoba->text() );
    query.bindValue( ":priynyavExpeditor", enterPriynyavVodiyExpeditor->text() );

	query.bindValue( ":zdavExpeditor",  enterZdavVodiyExpeditor->text() );
    query.bindValue( ":prinyalVidpovOsoba", enterPriynyavVidpovidalnaOsoba->text() );
		
	result = query.exec();
	error = query.lastError();

	if( error.type() == QSqlError::NoError )
	{

	}
	else
	{
        QMessageBox mess;
	    mess.setText( "������ ������ � ��!" );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

	}
	
	dialogTTN_2->hide();
}

// 
void Form_ttn::closeTTN_2()
{
	dialogTTN_2->hide();
}

void Form_ttn::closeTTN_3()
{
	dialogTTN_3->hide();
}

// ������� ������ �� �����  �  ��������  �� � �� ��� 
void Form_ttn::saveDataFromTTN_Form( QString nakladnaya, QString date_time, QString date_time_send,  QString auto_number, QString pricep_number, QString transporter, QString orderer_name, 
	 QString sender_info, QString accepter_info, int netto, int brutto, double total_price, QString goods )
{
	QMessageBox mess;
	bool result;
	QSqlError error;
	
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

	query.prepare( QString( "INSERT INTO ttn_data( nakladnaya, date_time, auto_info, pricep, transporter, orderer_name, sender_info, accepter_info, "
		"netto, brutto, total_price ) "
		"VALUES( :nakladnaya, :date_time, :auto_info, :pricep, :transporter, :orderer_name, :sender_info, :accepter_info,"
		":netto, :brutto, :total_price"
		");" ) );

	query.bindValue( ":nakladnaya", nakladnaya );
	query.bindValue( ":date_time", date_time ); //
	query.bindValue( ":auto_info", auto_number );
	query.bindValue( ":pricep", pricep_number );
	query.bindValue( ":transporter", transporter );
	query.bindValue( ":orderer_name", orderer_name );
	query.bindValue( ":sender_info", sender_info );
	query.bindValue( ":accepter_info", accepter_info );
	query.bindValue( ":netto", netto );
	query.bindValue( ":brutto", brutto );
	query.bindValue( ":total_price", total_price );
	//query.bindValue( ":nds", NDS_TTN );

	query.exec();
	error = query.lastError();

	if( error.type() == QSqlError::NoError )
	{

	}
	else
	{
        QMessageBox mess;
	    mess.setText( "������ ������ � ��!" );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

	}
	// ��������  ������  �����  ��� 
	query.prepare( QString( "INSERT INTO ttn_data_2(date_time, Num, NameOfGoods, edIsm, Netto, PriceWithOutPDV, VidPakuvannya, Docs_Z_Vantagem, Brutto) "
		"VALUES(:date_time, :Num, :NameOfGoods, :edIsm, :Netto, :PriceWithOutPDV, :VidPakuvannya, :Docs_Z_Vantagem, :Brutto);"
		) );

    query.bindValue( ":date_time", date_time );
	query.bindValue( ":Num", 1 );
	query.bindValue( ":NameOfGoods", goods ); //
	query.bindValue( ":edIsm", "��" );
	query.bindValue( ":Netto", netto );
	query.bindValue( ":PriceWithOutPDV", total_price );
	//query.bindValue( ":ZagalnaSummaZ_PDV", ( total_price * 0.25 + total_price ) );
	query.bindValue( ":VidPakuvannya", "" );
	query.bindValue( ":Docs_Z_Vantagem", "" );
	query.bindValue( ":Brutto", brutto );

	query.exec();
	error = query.lastError();

	if( error.type() == QSqlError::NoError )
	{

	}
	else
	{
        QMessageBox mess;
	    mess.setText( "������ ������ � ��!" );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();
	}

	// ��� 3
	result = query.prepare( QString( "INSERT INTO ttn_data_3 (date_time, Operation, Operation_2, Brutto, Brutto_2, Chas_pributtya, Chas_pributtya_2, Chas_vibuttya, Chas_vibuttya_2, "
		"Chas_prostoyu, Chas_prostoyu_2, Pidpis, Pidpis_2) "

		" VALUES( :date_time, :Operation, :Operation_2, :Brutto, :Brutto_2, "
		":Chas_pributtya, :Chas_pributtya_2, :Chas_vibuttya, :Chas_vibuttya_2, "
		":Chas_prostoyu, :Chas_prostoyu_2, :Pidpis, :Pidpis_2);"
		) );

	query.bindValue( ":date_time", date_time );
	query.bindValue( ":Operation", operation->text() );
    query.bindValue( ":Operation_2", operation_2->text() );
	query.bindValue( ":Brutto",  ( int )brutto );
    query.bindValue( ":Brutto_2", ( int )brutto );
	// ???????????????????????????

	QString time;

	QStringList sl = date_time.split( ' ' );
	if( sl.size() >= 2 )
	{
		time = sl.at( 1 );
	}

	query.bindValue( ":Chas_pributtya",  time );
    query.bindValue( ":Chas_pributtya_2", time );

	sl = date_time_send.split( ' ' );
	if( sl.size() >= 2 )
	{
		time = sl.at( 1 );
	}

	query.bindValue( ":Chas_vibuttya",  time );
    query.bindValue( ":Chas_vibuttya_2", time );
	query.bindValue( ":Chas_prostoyu",  prostoyu->text() );
    query.bindValue( ":Chas_prostoyu_2", prostoyu->text() );
	query.bindValue( ":Pidpis",  "" );
    query.bindValue( ":Pidpis_2", "" );

	result = query.exec();
	error = query.lastError();

	if( error.type() == QSqlError::NoError )
	{

	}
	else
	{
        QMessageBox mess;
	    mess.setText( "������ ������ � ��!" );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();
	}
	// 
	// ��� 4
	result = query.prepare( QString( "INSERT INTO ttn_data_4 (date_time, vidpovidalnaOsoba, priynyavExpeditor, zdavExpeditor, prinyalVidpovOsoba) "
		" VALUES( :date_time, :vidpovidalnaOsoba, :priynyavExpeditor, :zdavExpeditor, :prinyalVidpovOsoba);"
		) );
	query.bindValue( ":date_time", date_time );
	query.bindValue( ":vidpovidalnaOsoba", enterVidpovidalnaOsoba->text() );
    query.bindValue( ":priynyavExpeditor", enterPriynyavVodiyExpeditor->text() );
	query.bindValue( ":zdavExpeditor",  enterZdavVodiyExpeditor->text() );
    query.bindValue( ":prinyalVidpovOsoba", enterPriynyavVidpovidalnaOsoba->text() );

	result = query.exec();
	error = query.lastError();

	if( error.type() == QSqlError::NoError )
	{

	}
	else
	{
        QMessageBox mess;
	    mess.setText( "������ ������ � ��!" );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();
	}
}

// �������� ������ �� ����� � �� ��� 
void Form_ttn::updateDataFromTTN_Form( QString date_time )
{
	QMessageBox mess;
	bool result;
	QSqlError error;

	QString strDateTime = date_time;
	QDateTime dateTime = QDateTime::fromString( date_time, "dd.MM.yyyy hh:mm:ss" );
	date_time = dateTime.toString( "yyyy-MM-dd hh:mm:ss" );
	
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

	result = query.prepare( QString( "UPDATE ttn_data SET nakladnaya=:nakladnaya, date_nakl=:date_nakl, month_nakl=:month_nakl, year_nakl=:year_nakl, auto_info=:auto_info, pricep=:pricep, type_of_transporting=:type_of_transporting,"
		"transporter=:transporter, driver_name=:driver_name, orderer_name=:orderer_name, sender_info=:sender_info, accepter_info=:accepter_info, point_loading=:point_loading, "
		"point_unloading=:point_unloading, readress_of_goods_info=:readress_of_goods_info, serial_serial_dov=:serial_serial_dov, num_dov=:num_dov, date_dov=:date_dov, month_dov=:month_dov, year_dov=:year_dov, "
		"vidano=:vidano, state_of_goods=:state_of_goods, rules_of_goods=:rules_of_goods, netto=:netto, brutto=:brutto, expeditor_info=:expeditor_info, buhgalter_info=:buhgalter_info,  "
		"vidpusk_dozvoliv=:vidpusk_dozvoliv, total_price=:total_price, nds=:nds, suprovodni_docs=:suprovodni_docs, transport_service=:transport_service, others=:others "
        " WHERE date_time='%1';").arg( date_time ) );

	query.bindValue( ":nakladnaya", enterNumberNakladnaya->text() );
	query.bindValue( ":date_nakl", enterDate->text() );
	query.bindValue( ":month_nakl", enterMonth->text() );
	query.bindValue( ":year_nakl", enterYear->text() );
	query.bindValue( ":auto_info", enterAutoType->text() );
	query.bindValue( ":pricep", enterPricepType->text() );
	query.bindValue( ":type_of_transporting", enterTypePerevoski->text() );
	query.bindValue( ":transporter", enterAutoTransporter->text() );
	query.bindValue( ":driver_name", enterNameTransporter->text() );
	query.bindValue( ":orderer_name", enterEnterOrder->text() );
	query.bindValue( ":sender_info", enterSender->text() );
	query.bindValue( ":accepter_info", enterAccepter->text() );
	query.bindValue( ":point_loading", enterPointOfLoading->text() );
	query.bindValue( ":point_unloading", enterPointOf_UnLoading ->text() );
	query.bindValue( ":readress_of_goods_info", enterPereaddres->text() );
	query.bindValue( ":serial_serial_dov", enterSeriaNumber->text() );
	query.bindValue( ":num_dov", enterNumberDoverennost->text() );
	query.bindValue( ":date_dov", enterDoverennostDate->text() );
	query.bindValue( ":month_dov", enterDoverennostMonth->text() );
	query.bindValue( ":year_dov", enterDoverennostYear->text() );
	query.bindValue( ":vidano", enterDoverennostVidano->text() );
	query.bindValue( ":state_of_goods", enterGoodsState->text() );
	query.bindValue( ":rules_of_goods", enterRules->text() );
	query.bindValue( ":netto", enterNetto->text() );
	query.bindValue( ":brutto", enterBrutto->text() );
	query.bindValue( ":expeditor_info", enterEcpeditor->text() );
	query.bindValue( ":buhgalter_info", enterBuhgalterName->text() );
	query.bindValue( ":vidpusk_dozvoliv", enterBuhgalterName_2->text() );
	query.bindValue( ":total_price", enterTotalSold->text().toDouble() );
	query.bindValue( ":nds", enterPDV->text().toInt() );
	query.bindValue( ":suprovodni_docs", enterSuprovodniDoc->text() );
	query.bindValue( ":transport_service", enterTransportServices->text() );
	query.bindValue( ":others", enterOthers->text() );

	result = query.exec();
	error = query.lastError();

	if( error.type() == QSqlError::NoError )
	{

	}
	else
	{
        QMessageBox mess;
	    mess.setText( "������ ������ � ��!" );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();
	}
}

// ��������� ������  �  �����  ���  ��  �� 
void Form_ttn::loadDataToTTN_Form( QString date_time )
{
	QMessageBox mess;
	bool result;
	QSqlError error;

	QString strDateTime = date_time;
	QDateTime dateTime = QDateTime::fromString( date_time, "dd.MM.yyyy hh:mm:ss" );
	date_time = dateTime.toString( "yyyy-MM-dd hh:mm:ss" );

	dateTimeTTN = date_time; // ���������� ���� � ����� 
	
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();
	if( result == false )
    {
        // �������������� �� ������ ������ � �� 
	}
	else
	{

	}
	// ������� ������ ��� 
	result = query.exec( QString( "SELECT * FROM ttn_data WHERE date_time='%1';" ).arg( date_time ) );

	while( query.next() )
	{
		enterNumberNakladnaya->setText( query.record().value( "nakladnaya" ).toString() );
	    enterDate->setText( query.record().value( "date_nakl" ).toString() );
		enterMonth->setText( query.record().value( "month_nakl" ).toString() );
		enterYear->setText( query.record().value( "year_nakl" ).toString() );
		enterAutoType->setText( query.record().value( "auto_info" ).toString() );
		enterPricepType->setText( query.record().value( "pricep" ).toString() );
		enterTypePerevoski->setText( query.record().value( "type_of_transporting" ).toString() );
		enterAutoTransporter->setText( query.record().value( "transporter" ).toString() );
		enterNameTransporter->setText( query.record().value( "driver_name" ).toString() );
		enterEnterOrder->setText( query.record().value( "orderer_name" ).toString() );
		enterSender->setText( query.record().value( "sender_info" ).toString() );
		enterAccepter->setText( query.record().value( "accepter_info" ).toString() );
		enterPointOfLoading->setText( query.record().value( "point_loading" ).toString() );
		enterPointOf_UnLoading ->setText( query.record().value( "point_unloading" ).toString() );
		enterPereaddres->setText( query.record().value( "readress_of_goods_info" ).toString() );
		enterSeriaNumber->setText( query.record().value( "serial_serial_dov" ).toString() );
		enterNumberDoverennost->setText( query.record().value( "num_dov" ).toString() );
		enterDoverennostDate->setText( query.record().value( "date_dov" ).toString() );
		enterDoverennostMonth->setText( query.record().value( "month_dov" ).toString() );
		enterDoverennostYear->setText( query.record().value( "year_dov" ).toString() );
		enterDoverennostVidano->setText( query.record().value( "vidano" ).toString() );
		enterGoodsState->setText( query.record().value( "state_of_goods" ).toString() );
		enterRules->setText( query.record().value( "rules_of_goods" ).toString() );
		enterNetto->setText( query.record().value( "netto" ).toString() );
		enterBrutto->setText( query.record().value( "brutto" ).toString() );
		enterEcpeditor->setText( query.record().value( "expeditor_info" ).toString() );
		enterBuhgalterName->setText( query.record().value( "buhgalter_info" ).toString() );
		enterBuhgalterName_2->setText( query.record().value( "vidpusk_dozvoliv" ).toString() );
		enterTotalSold->setText( query.record().value( "total_price" ).toString() );

		enterPDV->setText( query.record().value( "nds" ).toString() );
		enterSuprovodniDoc->setText( query.record().value( "suprovodni_docs" ).toString() );
		enterTransportServices->setText( query.record().value( "transport_service" ).toString() );
		enterOthers->setText( query.record().value( "others" ).toString() );
    }

	if( error.type() == QSqlError::NoError )
	{

	}
	else
	{
        QMessageBox mess;
	    mess.setText( "������ ������ �� ��!" );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();
	}

    // ������� ������ ��� 2
    result = query.exec( QString( "SELECT * FROM ttn_data_2 WHERE date_time='%1';" ).arg( date_time ) );

	while( query.next() )
	{
		numPoint->setText( query.record().value( "Num" ).toString() );
		textOfTTN_2->setText( query.record().value( "NameOfGoods" ).toString() ); //
		edIsm->setText( query.record().value( "edIsm" ).toString() );
		numPlaces->setText( query.record().value( "Netto" ).toString() );
		priceBezPDV->setText( query.record().value( "PriceWithOutPDV" ).toString() );
		zagalnaSummaS_PDV->setText( query.record().value( "ZagalnaSummaZ_PDV" ).toString() );
		vidPakuvannya->setText( query.record().value( "VidPakuvannya" ).toString() );
		docsWithLoad->setText( query.record().value( "Docs_Z_Vantagem" ).toString() );
		massaBrutto->setText( query.record().value( "Brutto" ).toString() );
    }

	if( error.type() == QSqlError::NoError )
	{

	}
	else
	{
        QMessageBox mess;
	    mess.setText( "������ ������ �� ��!" );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();
	}
    // ������� ������ ��� 3
    result = query.exec( QString( "SELECT * FROM ttn_data_3 WHERE date_time='%1';" ).arg( date_time ) );

	while( query.next() )
	{
		operation->setText( query.record().value( "Operation" ).toString() );
		operation_2->setText( query.record().value( "Operation_2" ).toString() );
		massaBrutto2->setText( query.record().value( "Brutto" ).toString() );
		massaBrutto2_2->setText( query.record().value( "Brutto_2" ).toString() );
		pributtya->setText( query.record().value( "Chas_pributtya" ).toString() );
		pributtya_2->setText( query.record().value( "Chas_pributtya_2" ).toString() );
		vibuttya->setText( query.record().value( "Chas_vibuttya" ).toString() );
		vibuttya_2->setText( query.record().value( "Chas_vibuttya_2" ).toString() );
		prostoyu->setText( query.record().value( "Chas_prostoyu" ).toString() );
		prostoyu_2->setText( query.record().value( "Chas_prostoyu_2" ).toString() );
		pidpisVidpovidalnOsobi->setText( query.record().value( "Pidpis" ).toString() );
		pidpisVidpovidalnOsobi_2->setText( query.record().value( "Pidpis_2" ).toString() );
    }

	if( error.type() == QSqlError::NoError )
	{

	}
	else
	{
        QMessageBox mess;
	    mess.setText( "������ ������ �� ��!" );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();
	}

	// ��� 4
	result = query.exec( QString( "SELECT * FROM ttn_data_4 WHERE date_time='%1';" ).arg( date_time ) );

	while( query.next() )
	{
		enterVidpovidalnaOsoba->setText( query.record().value( "vidpovidalnaOsoba" ).toString() );
		enterPriynyavVodiyExpeditor->setText( query.record().value( "priynyavExpeditor" ).toString() );
		enterZdavVodiyExpeditor->setText( query.record().value( "zdavExpeditor" ).toString() );
		enterPriynyavVidpovidalnaOsoba->setText( query.record().value( "prinyalVidpovOsoba" ).toString() );
	}

	error = query.lastError();

	if( error.type() == QSqlError::NoError )
	{
	}
	else
	{
        QMessageBox mess;
	    mess.setText( "������ ������ � ��!" );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();
	}
}

// 
void Form_ttn::pushButtonTTN_2_Slot()
{
	dialogTTN_2->show();
}

// 
void Form_ttn::pushButtonTTN_3_Slot()
{
	dialogTTN_3->show();
}

// 
QList<QString> Form_ttn::getHeadersTTN()
{
    QList<QString> result = QList<QString>();
	QString buf[] = {"�\r\n �/�", "������������ ������� (����� ����������), �\r\n"
		"��� ����������� ����������� �������: ����\r\n"
		"����������� �������, �� ����� ��������\r\n ������",
		"������� �����", "ʳ������ ����", "ֳ�� ��� ��� ��\r\n �������, ���",
		"�������� ���� �\r\n ���, ���", "���    \r\n ���������",
		"��������� � ��������", "����  \r\n ������, �"};
	for(int i = 0; i < sizeof(buf)/sizeof(buf[0]); i++)
		result.append(buf[i]);

	return result;
}

// 
QList<QString> Form_ttn::getHeadersTTN_3()
{
    QList<QString> result = QList<QString>();
	QString buf[] = {"��������", "����� ������, �", "��� �������� (���., ��.)", 
		"��� ������� (���., ��.)", "��� ������� (���., ��.)", "ϳ���� ����������� \r\n �����"};
	for(int i = 0; i < sizeof(buf)/sizeof(buf[0]); i++)
		result.append(buf[i]);

	return result;
}

// ������ ��� 
void Form_ttn::pushButtonPrint_Slot()
{
    QMessageBox mess;
	bool result;
	QSqlError error;

	QList< QString > data;
	QList< QString > data2;
	QList< QString > data3;
	QList< QString > data4;

	QString strDateTime = dateTimeTTN;
		
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

	// ������� ������ ��� 
	result = query.exec( QString( "SELECT * FROM ttn_data WHERE date_time='%1';" ).arg( dateTimeTTN ) );

	while( query.next() )
	{
		data.append( query.record().value( "nakladnaya" ).toString() );
	    data.append( query.record().value( "date_nakl" ).toString() );
		data.append( query.record().value( "month_nakl" ).toString() );
		data.append( query.record().value( "year_nakl" ).toString() );
		data.append( query.record().value( "auto_info" ).toString() );
		data.append( query.record().value( "pricep" ).toString() );
		data.append( query.record().value( "type_of_transporting" ).toString() );
		data.append( query.record().value( "transporter" ).toString() );
		data.append( query.record().value( "driver_name" ).toString() );
		data.append( query.record().value( "orderer_name" ).toString() );
		data.append( query.record().value( "sender_info" ).toString() );
		data.append( query.record().value( "accepter_info" ).toString() );
		data.append( query.record().value( "point_loading" ).toString() );
		data.append( query.record().value( "point_unloading" ).toString() );
		data.append( query.record().value( "readress_of_goods_info" ).toString() );
		data.append( query.record().value( "serial_serial_dov" ).toString() );
		data.append( query.record().value( "num_dov" ).toString() );
		data.append( query.record().value( "date_dov" ).toString() );
		data.append( query.record().value( "month_dov" ).toString() );
		data.append( query.record().value( "year_dov" ).toString() );
		data.append( query.record().value( "vidano" ).toString() );
		data.append( query.record().value( "state_of_goods" ).toString() );
		data.append( query.record().value( "rules_of_goods" ).toString() );
		data.append( query.record().value( "netto" ).toString() );
		data.append( query.record().value( "brutto" ).toString() );
		data.append( query.record().value( "expeditor_info" ).toString() );
		data.append( query.record().value( "buhgalter_info" ).toString() );
		data.append( query.record().value( "vidpusk_dozvoliv" ).toString() );
		data.append( query.record().value( "total_price" ).toString() );
		data.append( query.record().value( "nds" ).toString() );
		data.append( query.record().value( "suprovodni_docs" ).toString() );
		data.append( query.record().value( "transport_service" ).toString() );
		data.append( query.record().value( "others" ).toString() );
    }

	if( error.type() == QSqlError::NoError )
	{

	}
	else
	{
        QMessageBox mess;
	    mess.setText( "������ ������ �� ��!" );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();
		return;
	}

    // ������� ������ ��� 2
    result = query.exec( QString( "SELECT * FROM ttn_data_2 WHERE date_time='%1';" ).arg( dateTimeTTN ) );

	while( query.next() )
	{
		data2.append( query.record().value( "Num" ).toString() );
		data2.append( query.record().value( "NameOfGoods" ).toString() ); //
		data2.append( query.record().value( "edIsm" ).toString() );
		data2.append( query.record().value( "Netto" ).toString() );
		//data2.append( query.record().value( "PriceWithOutPDV" ).toString() );
		data2.append(QString::number( query.record().value( "PriceWithOutPDV" ).toDouble() / 1000, 'g', 6 ) );
		data2.append( query.record().value( "ZagalnaSummaZ_PDV" ).toString() );
		data2.append( query.record().value( "VidPakuvannya" ).toString() );
		data2.append( query.record().value( "Docs_Z_Vantagem" ).toString() );
		data2.append( query.record().value( "Brutto" ).toString() );
		qDebug()<<data2;
    }

	if( error.type() == QSqlError::NoError )
	{

	}
	else
	{
        QMessageBox mess;
	    mess.setText( "������ ������ �� ��!" );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

		return;
	}

	// ������� ������ ��� 3
    result = query.exec( QString( "SELECT * FROM ttn_data_3 WHERE date_time='%1';" ).arg( dateTimeTTN ) );

	while( query.next() )
	{
		data3.append( query.record().value( "Operation" ).toString() );
		data3.append( query.record().value( "Operation_2" ).toString() ); //

		data3.append( query.record().value( "Brutto" ).toString() );
		data3.append( query.record().value( "Brutto_2" ).toString() );

		data3.append( query.record().value( "Chas_pributtya" ).toString() );
		data3.append( query.record().value( "Chas_pributtya_2" ).toString() );

		data3.append( query.record().value( "Chas_vibuttya" ).toString() );
		data3.append( query.record().value( "Chas_vibuttya_2" ).toString() );

		data3.append( query.record().value( "Chas_prostoyu" ).toString() );
		data3.append( query.record().value( "Chas_prostoyu_2" ).toString() );

		data3.append( query.record().value( "Pidpis" ).toString() );
		data3.append( query.record().value( "Pidpis_2" ).toString() );
    }

	if( error.type() == QSqlError::NoError )
	{

	}
	else
	{
        QMessageBox mess;
	    mess.setText( "������ ������ �� ��!" );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

		return;
	}
	// ��� 4
	// ������� ������ ���  4
    result = query.exec( QString( "SELECT * FROM ttn_data_4 WHERE date_time='%1';" ).arg( dateTimeTTN ) );

	while( query.next() )
	{
		data4.append( query.record().value( "vidpovidalnaOsoba" ).toString() );
		data4.append( query.record().value( "priynyavExpeditor" ).toString() ); //

		data4.append( query.record().value( "zdavExpeditor" ).toString() );
		data4.append( query.record().value( "prinyalVidpovOsoba" ).toString() );
    }

	if( error.type() == QSqlError::NoError )
	{

	}
	else
	{
        QMessageBox mess;
	    mess.setText( "������ ������ �� ��!" );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();

		return;
	}

	QString html;

// ��������� ��� ��� ���������� 
	QRect screenResolution = qApp->desktop()->screenGeometry();
	QPrinter printer( QPrinter::HighResolution );
	printer.setPaperSize( QSize( 210, 297 ), QPrinter::Millimeter );
	printer.setOrientation( QPrinter::Landscape );
	printer.setFullPage( false );
	printer.setPageMargins( 16, 6, 8, 4, QPrinter::Millimeter );
	// ��� ������ � PDF 
/*	QDir dir( "" ); 
	QString path = dir.absolutePath().append( QString( "/%1" ).arg( QDate::currentDate().toString( "yyyy.MM.dd/" ) ) );// ����� ��������� ����� �������� ���
	if( !dir.exists( path ) )
	{
		dir.mkdir( path );
	}

	QString nameOfFile = QString( "%1_Rep_%2.pdf" ).arg( data.at(0) ).arg( QDateTime::currentDateTime().toString( "hh_mm_ss" ) );
	nameOfFile = path + nameOfFile;
	printer.setOutputFileName( nameOfFile );
	printer.setOutputFormat( QPrinter::PdfFormat );
*/
	QWebView *webView = new QWebView; // ������ ��� �������� ������ 
	webView->setStyleSheet( "background-color: #fff; width: 100%; height: 100%; " );

    QPrintPreviewDialog preview( &printer );
	preview.setWindowFlags( Qt::Window );

	QObject::connect( &preview, SIGNAL( paintRequested( QPrinter* ) ), webView, SLOT( print( QPrinter* ) ) );

	preview.setModal( true );
	preview.setOrientation( Qt::Horizontal );
	preview.setMinimumSize( screenResolution.width(), screenResolution.height() ); // ���������� �� ���� ������ ������ 

	//QList<QString> rowHeader;
	int row;

	html = QString( "<h3 align=center style='margin-bottom: -10px; margin-top: -5px; '>�������-����������� ��������</h3>" );

	try
	{
	 
		QString row1, subRow;
		QString text;
		html.append( "<span style=' font-size: 14px; font-weight: normal; ' >" );

		html.append( QString( "<p style='text-align: center;' ><b> � &nbsp; %1 &nbsp; </b>\"<span style='text-decoration: underline;'>  &nbsp;&nbsp;&nbsp; %2 &nbsp; </span>\" &nbsp; "
			"<span style='text-decoration: underline;'> &nbsp; %3 &nbsp;&nbsp;&nbsp; </span> &nbsp; "
			" 20 &nbsp; <span style='text-decoration: underline;'> %4�. </span> </p>" ).arg( data.at( 0 ) ).arg( data.at( 1 ) ).arg( data.at( 2 ) ).arg( data.at( 3 ) ) );

		html.append( "" );

		// ������ 1
		row1.append( "<p style='margin-top: -8px;'><nobr>&nbsp;" );
		text = "�������i�� ";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom: 1px; width: 25%;' > &nbsp;&nbsp;&nbsp; %1 " ).arg( data.at( 4 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		text = "  ����i�/���i�����i� ";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom: 1px; width: 20%;' > &nbsp;&nbsp;&nbsp; %1 " ).arg( data.at( 5 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		text = " ��� ���������� ";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom: 1px; width: 32%;' > &nbsp;&nbsp;&nbsp; %1 " ).arg( "����" ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		row1.append( "</nobr></p>" );
		html.append( row1 );
		row1.clear();

		subRow.append( "<p style='font-size: 11px;'>" );
		text = "(�����, ������, ���, �������i���� �����)";
		subRow.append( QString( "<span style='display:inline-block; padding-left: 100px; margin-top: -14px; padding-top: -14px; ' > %1 " ).arg( text ) );
		subRow.append( "</span>" );
		text = "(�����, ������, ���, �������i���� �����)";
		subRow.append( QString( "<span style='display:inline-block; padding-left: 286px; margin-top: -14px; padding-top: -14px; ' > %1 " ).arg( text ) );
		subRow.append( "</span>" );
		subRow.append( "</p>" );
		html.append( subRow );
		subRow.clear();

		// ������ 2
		row1.append( "<p style='margin-top: -6px;' ><nobr>&nbsp;" );
		text = "�������i����� �����i����   ";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 43%;' > &nbsp;&nbsp;&nbsp; %1 " ).arg( data.at( 7 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		text = " ���i�   ";
		row1.append( text );
		row1.append( QString( "&nbsp;<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 40.5%;' > &nbsp;&nbsp;&nbsp; %1 " ).arg( data.at( 8 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		row1.append( "</nobr></p>" );
		html.append( row1 );
		row1.clear();

		subRow.append( "<p style='font-size: 11px;'>" );
		text = "(������������/�.I.�.)";
		subRow.append( QString( "<span style='display:inline-block; padding-left: 433px; margin-top: -14px; padding-top: -14px; ' > %1 " ).arg( text ) );
		subRow.append( "</span>" );
		text = "(�.I.�., ����� ����i������ ���i�)";
		subRow.append( QString( "<span style='display:inline-block; padding-left: 988px; margin-top: -14px; padding-top: -14px; ' > %1 " ).arg( text ) );
		subRow.append( "</span>" );
		subRow.append( "</p>" );
		html.append( subRow );
		subRow.clear();

		// ������ 3
		row1.append( "<p style='margin-top: -6px;' ><nobr>&nbsp;" );
		text = "��������  ";
		row1.append( text );
		row1.append( QString( "&nbsp;<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 93.4%;' >&nbsp;&nbsp;&nbsp; %1 " ).arg( data.at( 9 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		row1.append( "</nobr></p>" );
		html.append( row1 );
		row1.clear();

		subRow.append( "<p style='font-size: 11px;'>" );
		text = "(������������/�.I.�.)";
		subRow.append( QString( "<span style='display:inline-block; padding-left: 633px; margin-top: -14px; padding-top: -14px; ' > %1 " ).arg( text ) );
		subRow.append( "</span>" );
		subRow.append( "</p>" );
		html.append( subRow );
		subRow.clear();

		// ������ 4
		row1.append( "<p style='margin-top: -6px;' ><nobr>&nbsp;" );
		text = "��������i��������";
		row1.append( text );
		row1.append( QString( "&nbsp;<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 89.5%;' >&nbsp;&nbsp;&nbsp;  %1 " ).arg( data.at( 10 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		row1.append( "</nobr></p>" );
		html.append( row1 );
		row1.clear();

		subRow.append( "<p style='font-size: 11px;'>" );
		text = "(����� ������������, �i��������������/�.I.�., �i��� ����������)";
		subRow.append( QString( "<span style='display:inline-block; padding-left: 533px; margin-top: -14px; padding-top: -14px; ' > %1 " ).arg( text ) );
		subRow.append( "</span>" );
		subRow.append( "</p>" );
		html.append( subRow );
		subRow.clear();

		// ������ 5
		row1.append( "<p style='margin-top: -6px;' ><nobr>&nbsp;" );
		text = "����������������";
		row1.append( text );
		row1.append( QString( "&nbsp;<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 89.5%' >&nbsp;&nbsp;&nbsp;  %1 " ).arg( data.at( 11 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		row1.append( "</nobr></p>" );
		html.append( row1 );
		row1.clear();

		subRow.append( "<p style='font-size: 11px;'>" );
		text = "(����� ������������, �i��������������/�.I.�., �i��� ����������)";
		subRow.append( QString( "<span style='display:inline-block; padding-left: 533px; margin-top: -14px; padding-top: -14px; ' > %1 " ).arg( text ) );
		subRow.append( "</span>" );
		subRow.append( "</p>" );
		html.append( subRow );
		subRow.clear();

		// ������ 6
		row1.append( "<p style='margin-top: -6px;' ><nobr>&nbsp;" );
		text = "����� ������������   ";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 39.3%;' > &nbsp;&nbsp;&nbsp; %1 " ).arg( data.at( 12 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		text = " ����� �������������   ";
		row1.append( text );
		row1.append( QString( "&nbsp;<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 39.8%;' > &nbsp;&nbsp;&nbsp; %1 " ).arg( data.at( 13 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		row1.append( "</nobr></p>" );
		html.append( row1 );
		row1.clear();

		subRow.append( "<p style='font-size: 11px;'>" );
		text = "(�i��������������)";
		subRow.append( QString( "<span style='display:inline-block; padding-left: 365px; margin-top: -14px; padding-top: -14px; ' >  %1 " ).arg( text ) );
		subRow.append( "</span>" );
		text = "(�i��������������)";
		subRow.append( QString( "<span style='display:inline-block; padding-left: 1048px; margin-top: -14px; padding-top: -14px; ' > %1 " ).arg( text ) );
		subRow.append( "</span>" );
		subRow.append( "</p>" );
		html.append( subRow );
		subRow.clear();

		// ������ 7
		row1.append( "<p style='margin-top: -6px;' ><nobr>&nbsp;" );
		text = "��������������� �������  ";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 86.5%;' > &nbsp;&nbsp;&nbsp; %1 " ).arg( data.at( 14 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		row1.append( "</nobr></p>" );
		html.append( row1 );
		row1.clear();

		subRow.append( "<p style='font-size: 11px;'>" );
		text = "(������������, �i��������������/�.I.�., �i������������� ������ �����������������; �.I.�., ������ �� �i���� �i����i������ �����)";
		subRow.append( QString( "<span style='display:inline-block; padding-left: 433px; margin-top: -14px; padding-top: -14px; ' > %1 " ).arg( text ) );
		subRow.append( "</span>" );
		subRow.append( "</p>" );
		html.append( subRow );
		subRow.clear();

		// ������ 8
		row1.append( "<p style='margin-top: -10px;' ><nobr>&nbsp;" );
		text = "�i����� �� ���i���i��� �����������������: ���i� ";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 2%;' > &nbsp;&nbsp;&nbsp; %1 " ).arg( data.at( 15 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		text = " � ";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 6%;' > &nbsp;&nbsp;&nbsp; %1 " ).arg( data.at( 16 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		text = " �i� ";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 3%;' >&nbsp;&nbsp;&nbsp;  %1 " ).arg( data.at( 17 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		row1.append( "&nbsp;&nbsp;" );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 7%;' >&nbsp;&nbsp;&nbsp;  %1 " ).arg( data.at( 18 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		text = " 20";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 2%;' >&nbsp;&nbsp;&nbsp;  %1 " ).arg( data.at( 19 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		text = " �., ������� ";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 45.8%;' >&nbsp;&nbsp;&nbsp;  %1 " ).arg( data.at( 20 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		row1.append( "</nobr></p>" );
		html.append( row1 );
		row1.clear();

		// ������ 9
		row1.append( "<p style='margin-top: 5px;' ><nobr>&nbsp;" );
		text = "������ ������� ��� ����������� � ����i, �� ";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 14%;' >&nbsp;&nbsp;&nbsp;  %1 " ).arg( data.at( 21 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		text = " �������� ���������� �i����i���� ������i�, ����� ������ (�� ��������i) ";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 33%;' >&nbsp;&nbsp;&nbsp;  %1 " ).arg( data.at( 22 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		row1.append( "</nobr></p>" );
		html.append( row1 );
		row1.clear();

		subRow.append( "<p style='font-size: 11px;'>" );
		text = "(�i����i��, �� �i����i��)";
		subRow.append( QString( "<span style='display:inline-block; padding-left: 280px; margin-top: -14px; padding-top: -14px; ' > %1 " ).arg( text ) );
		subRow.append( "</span>" );
		subRow.append( "</p>" );
		html.append( subRow );
		subRow.clear();

		// ������ 10  // ����� ����� ��������  �������� !!!
		row1.append( "<p style='margin-top: -10px;' ><nobr>&nbsp;" );
		text = "�i���i��� �i��� ";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 25%;' >&nbsp;&nbsp;&nbsp;  %1 " ).arg( data.at( 23 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		text = "  ����� ������, �� ";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 25%;' > &nbsp;&nbsp;&nbsp; %1 " ).arg( data.at( 24 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		text = ", ������� ���i�/���������� ";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 21.4%;' >&nbsp;&nbsp;&nbsp;  %1 " ).arg( data.at( 25 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		row1.append( "</nobr></p>" );
		html.append( row1 );
		row1.clear();

		subRow.append( "<p style='font-size: 11px;'>" );
		text = "(�������)";
		subRow.append( QString( "<span style='display:inline-block; padding-left: 193px; margin-top: -14px; padding-top: -14px; ' > %1 " ).arg( text ) );
		subRow.append( "</span>" );
		text = "(�������)";
		subRow.append( QString( "<span style='display:inline-block; padding-left: 348px; margin-top: -14px; padding-top: -14px; ' > %1 " ).arg( text ) );
		subRow.append( "</span>" );
		text = "(�.I.�., ������, �i����)";
		subRow.append( QString( "<span style='display:inline-block; padding-left: 1100px; margin-top: -14px; padding-top: -14px; ' > %1 " ).arg( text ) );
		subRow.append( "</span>" );
		subRow.append( "</p>" );
		html.append( subRow );
		subRow.clear();

		// ������ 11
		row1.append( "<p style='margin-top: -6px;' ><nobr>&nbsp;" );
		text = "��������� (�i����i������ ����� ��������i���������) ";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 38.5%;' >&nbsp;&nbsp;&nbsp;  %1 " ).arg( data.at( 26 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		text = "  �i����� �������� ";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 28.2%;' >&nbsp;&nbsp;&nbsp;  %1 " ).arg( data.at( 27 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		row1.append( "</nobr></p>" );
		html.append( row1 );
		row1.clear();

		subRow.append( "<p style='font-size: 11px;'>" );
		text = "(�.I.�. ������, �i����)";
		subRow.append( QString( "<span style='display:inline-block; padding-left: 605px; margin-top: -14px; padding-top: -14px; ' > %1 " ).arg( text ) );
		subRow.append( "</span>" );
		text = "(�.I.�. ������, �i����)";
		subRow.append( QString( "<span style='display:inline-block; padding-left: 1133px; margin-top: -14px; padding-top: -14px; ' > %1 " ).arg( text ) );
		subRow.append( "</span>" );
		subRow.append( "</p>" );
		html.append( subRow );
		subRow.clear();

		// ������ 12
		row1.append( "<p style='margin-top: -6px;' ><nobr>&nbsp;" );
		text = "������ �i������� �� �������� ���� ";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 60%;' >&nbsp;&nbsp;&nbsp;  %1 " ).arg( data.at( 28 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		text = " � �.�. ��� ";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 17%;' >&nbsp;&nbsp;&nbsp;  %1 " ).arg( ( "" ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		row1.append( "</nobr></p>" );
		html.append( row1 );
		row1.clear();

		// ������ 13
		row1.append( "<p style='margin-top: -6px;' ><nobr>&nbsp;" );
		text = "������i��i ��������� �� ������  ";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 83.7%;' >&nbsp;&nbsp;&nbsp;  %1 " ).arg( data.at( 30 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		row1.append( "</nobr></p>" );
		html.append( row1 );
		row1.clear();

		// ������ 14
		row1.append( "<p style='margin-top: -6px;' ><nobr>&nbsp;" );
		text = "����������i �������, ��i ��������� �������i����� �����i������ ";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; font-size: 16px; padding-bottom:2px; width: 69.6%;' >&nbsp;&nbsp;&nbsp;  %1 " ).arg( data.at( 31 ) ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		row1.append( "</nobr></p>" );
		html.append( row1 );
		row1.clear();

		// for( int i = 0; i < 8; i++ )
			// html.append( "<br />" );

		// 
		html.append( "<H4 align=center style='margin-bottom:-5px; margin-top: -5px;'>�I������I ��� ������</H4>" );

	// ��������, �������! ����-����������-����-����-����-��-����-��-����-��-��������
		data2.append( "" );
		data2.append( "" );
	///
			
		double price_za_ed = data2.at( 4 ).toDouble();
		//double price_bulk_plus_nds = data2.at( 3 ).toDouble() * price_za_ed + ( data2.at( 3 ).toDouble() * price_za_ed )  * NDS_TTN / 100;
		double price_bulk_plus_nds = data2.at( 5 ).toDouble();
		double price_prizep_plus_nds = data2.at( 9 ).toDouble() * price_za_ed + ( data2.at( 9 ).toDouble() * price_za_ed ) * NDS_TTN / 100;

		QString total_pos_price;
		total_pos_price = total_pos_price.sprintf( "%8.2f", price_bulk_plus_nds );

		html.append( QString( " <table border='1' cellspacing='0' cellpadding='2' width=98% height=98% style='text-align: center; font-size: 13px; font-weight: normal; "
			"border: 3px solid #000; margin: 5px;'>" ).append( 
			"<tr><td>� \r\n �\\�</td><td>������������ ������� (����� ����������), �\r\n ���i ����������� ����������� ������i�: ����\r\n ����������� �������, �� ����� �i�������\r\n ������</td>"
			"<td>������� ���i��</td><td>�i���i��� �i���</td><td>�i�� ��� ��� ��\r\n �������, ���</td><td>�������� ���� �\r\n ���, ���</td><td>���  "
			"\r\n ���������</td><td>��������� � ��������</td><td>����  \r\n ������</td></tr>" ).append( 
			"    <tr><td>%1</td><td>%2</td><td>%3</td><td>%4</td><td>%5</td><td>%6</td><td>%7</td><td>%8</td><td>%9</td></tr>"
			"<tr><td> &nbsp; </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td></tr>"
			"<tr><td> &nbsp; </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td></tr>"
			"<tr><td> &nbsp; </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td><td>  </td>"
			"</tr>").arg( ( data2.at( 0 ).toInt() > 1 ? 1 : 1 ) ).arg( data2.at( 1 ) ).arg( data2.at( 2 ) ).
			arg( data2.at( 3 ) ).arg( data2.at( 4 ) ).arg( total_pos_price ).arg( data2.at( 6 ) ).arg( data2.at( 7 ) ).arg( data2.at( 8 ) ) ); 

		if( data2.at( 9 ).toDouble() != 0 )
		{
			total_pos_price = total_pos_price.sprintf( "%8.2f", price_prizep_plus_nds );
			QString massa_brutto_prizep;
			massa_brutto_prizep = massa_brutto_prizep.sprintf( "%8.3f", data2.at( 10 ).toDouble() );

			html.append( QString(
			"    <tr><td>%1</td><td>%2</td><td>%3</td><td>%4</td><td>%5</td><td>%6</td><td>%7</td><td>%8</td><td>%9</td></tr>").arg( ( data2.at( 0 ).toInt() > 1 ? 2 : 1 ) ).
			arg( data2.at( 1 ) ).arg( data2.at( 2 ) ).arg( data2.at( 9 ) ).arg( data2.at( 4 ) ). // data2.at( 9 ) - ��� ����� ������� 
			arg( total_pos_price ).arg( data2.at( 6 ) ).arg( data2.at( 7 ) ).arg( massa_brutto_prizep ) );
		}

		QString netto_total;
		netto_total = netto_total.sprintf( "%8.3f", ( data2.at( 3 ).toDouble() + data2.at( 9 ).toDouble() ) );

		QString total_price;
		total_price = total_price.sprintf( "%8.2f", ( price_bulk_plus_nds ) + price_prizep_plus_nds  );

		QString brutto_total;
		brutto_total = brutto_total.sprintf( "%8.3f", ( data2.at( 8 ).toDouble() + data2.at( 10 ).toDouble() ) );

		html.append(
			QString( "<tr><td>������: </td><td></td><td></td><td> %1 </td><td></td><td>%2</td><td></td><td></td><td>%3</td></tr>").
			arg( netto_total ).
			arg( total_price ).
			arg( brutto_total ) );

		html.append( "</table>" );
		// 
		row1.append( "<p style='font-size: 14px; margin-left: 5px; margin-top: 0px;'><nobr>" );
		text = "���� (�i����i������ ����� ��������i���������)";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; margin-left: 85px;' >" ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		text = "������� ���i�/����������";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; margin-left: 225px;' >" ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		text = "���� ���i�/����������";
		row1.append( text );
		row1.append( QString( "<span style='display:inline-block; margin-left: 115px;' > " ) );
		row1.append( "&nbsp;" );
		row1.append( "</span>" );
		text = "������� (�i����i������ ����� �����������������)";
		row1.append( text );
		//row1.append( QString( "<span style='display:inline-block; margin-left: 143px;' > " ) );
		//row1.append( "&nbsp;" );
		//row1.append( "</span>" );
		row1.append( "</nobr></p>" );
		html.append( row1 );
		row1.clear();
		// 
		row1.append( "<p style='font-size: 14px; margin-left: 0px;'><nobr>&nbsp;" );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; padding-bottom:2px; width: 25%; margin-left: 0px;' > %1 " ).arg( data4.at( 0 ) ) );
		row1.append( "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" );
		row1.append( "</span>" );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; padding-bottom:2px; width: 25%; margin-left: 30px;' > %1 " ).arg( data4.at( 1 ) ) );
		row1.append( "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" );
		row1.append( "</span>" );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; padding-bottom:2px; width: 17%; margin-left: 25px;' > %1 " ).arg( data4.at( 2 ) ) );
		row1.append( "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" );
		row1.append( "</span>" );
		row1.append( QString( "<span style='display:inline-block; border-bottom:1px solid black; padding-bottom:2px; width: 23%; margin-left: 26px' > %1 " ).arg( data4.at( 3 ) ) );
		row1.append( "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" );
		row1.append( "</span>" );
		row1.append( "</nobr></p>" );
		html.append( row1 );
		row1.clear();

		subRow.append( "<p style='font-size: 11px;'>" );
		text = "(�.I.�., ������, �i����, �������)";
		subRow.append( QString( "<span style='display:inline-block; padding-left: 48px; margin-top: -14px; padding-top: -14px; ' > %1 " ).arg( text ) );
		subRow.append( "</span>" );
		text = "(�.I.�., ������, �i����)";
		subRow.append( QString( "<span style='display:inline-block; padding-left: 240px; margin-top: -14px; padding-top: -14px; ' > %1 " ).arg( text ) );
		subRow.append( "</span>" );
		text = "(�.I.�., ������, �i����)";
		subRow.append( QString( "<span style='display:inline-block; padding-left: 228px; margin-top: -14px; padding-top: -14px; ' > %1 " ).arg( text ) );
		subRow.append( "</span>" );
		text = "(�.I.�., ������, �i����, �������)";
		subRow.append( QString( "<span style='display:inline-block; padding-left: 186px; margin-top: -14px; padding-top: -14px; ' > %1 " ).arg( text ) );
		subRow.append( "</span>" );
		subRow.append( "</p>" );
		html.append( subRow );
		subRow.clear();
		// 
		html.append( "<H4 align=center style='margin-bottom:-5px; margin-top: -2px;'>�������-���������������I ������I�</H4>" );

		html.append( QString( " <table border='1' cellspacing='0' cellpadding='2' "
			"width=98%  height=98% style='text-align: center; font-size: 13px; font-weight: normal; border: 1px solid #000; margin-top: 5px; margin-bottom: -2px; margin-left: 5px; margin-right: 5px'>" ).append( 

			"<tr>"
			"<th rowspan=\"2\">������i�</th>"
			"<th rowspan=\"2\">���� ������, ��</th>"
			"<th colspan=\"3\">��� (���.��.)</th>"
			"<th rowspan=\"2\">�i���� �i����i������ \r\n �����<br></th>"
			"</tr>"
			"<tr>"
			"<td>��������</td>"
			"<td>�������</td>"
			"<td>�������</td>"
			"</tr>"
			"<tr>" ).append(
			QString( "<td>%1<br></td>"
			"<td>%2</td>"
			"<td>%3</td>"
			"<td>%4</td>"
			"<td>%5</td>"
			"<td>%6</td>"
			"</tr>" ).arg( data3.at( 0 ) ).arg( data3.at( 2 ) ).arg( data3.at( 4 ) ).arg( data3.at( 6 ) ).arg( data3.at( 8 ) ).arg( data3.at( 10 ) ) ).append( 
			   
				QString( "<tr><td>%1<br></td>"
			"<td>%2</td>"
			"<td>%3</td>"
			"<td>%4</td>"
			"<td>%5</td>"
			"<td>%6</td>"
			"</tr>" ).arg( data3.at( 1 ) ).arg( data3.at( 3 ) ).arg( data3.at( 5 ) ).arg( data3.at( 7 ) ).arg( data3.at( 9 ) ).arg( data3.at( 11 ) ) ).append( 
		" </table>" ) );

  	}
 	catch( ... )
	{


	}

	
	webView->setHtml( html );
	preview.exec();
	//��� ������ � PDF
    //webView->print( &printer );
	//QDesktopServices::openUrl( QUrl::fromLocalFile( nameOfFile ) );
	webView->deleteLater();


#ifdef  TTN_DEBUG
	QFile file("d:\\1\\ttn_out.htm");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;
	QTextStream out(&file);
    out << html;
	file.close();
#endif

	

}

// 
// 
void Form_ttn::saveTTN_3()
{
	bool result;
	QSqlError error;
	
	mysqlProccess *mysqlProccessObject = mysqlProccess::instance();
	QSqlQuery query( *mysqlProccessObject->getQSqlInstance() );

	query.prepare( QString( "USE %1;" ).arg( dataBaseName ) );
	result = query.exec();

    // 
	query.prepare( QString( "UPDATE ttn_data_3 SET Operation=:Operation, Operation_2=:Operation_2, Brutto=:Brutto, Brutto_2=:Brutto_2, "
		"Chas_pributtya=:Chas_pributtya, Chas_pributtya_2=:Chas_pributtya_2, Chas_vibuttya=:Chas_vibuttya, Chas_vibuttya_2=:Chas_vibuttya_2, "
		"Chas_prostoyu=:Chas_prostoyu, Chas_prostoyu_2=:Chas_prostoyu_2, Pidpis=:Pidpis, Pidpis_2=:Pidpis_2; "
		) );

	query.bindValue( ":Operation", operation->text() );
    query.bindValue( ":Operation_2", operation_2->text() );

	query.bindValue( ":Brutto",  massaBrutto2->text().toInt() );
    query.bindValue( ":Brutto_2", massaBrutto2_2->text().toInt() );

	query.bindValue( ":Chas_pributtya",  pributtya->text() );
    query.bindValue( ":Chas_pributtya_2", pributtya_2->text() );

	query.bindValue( ":Chas_vibuttya",  vibuttya->text() );
    query.bindValue( ":Chas_vibuttya_2", vibuttya_2->text() );

	query.bindValue( ":Chas_prostoyu",  prostoyu->text() );
    query.bindValue( ":Chas_prostoyu_2", prostoyu_2->text() );

	query.bindValue( ":Pidpis",  pidpisVidpovidalnOsobi->text() );
    query.bindValue( ":Pidpis_2", pidpisVidpovidalnOsobi_2->text() );

	result = query.exec();
	error = query.lastError();

	if( error.type() == QSqlError::NoError )
	{
	}
	else
	{
        QMessageBox mess;
	    mess.setText( "������ ������ � ��!" );
	    mess.setStandardButtons( QMessageBox::Cancel );
		mess.setButtonText( QMessageBox::Cancel, "Ok" );
		mess.setIcon( QMessageBox::Warning );
		mess.setWindowTitle( tr( "Prom-Soft" ) );
		mess.exec();
	}
	
	dialogTTN_3->hide();
}

// 
Form_ttn::~Form_ttn()
{
}