#include "StdAfx.h"
#include "Crypt.h"

QStringList Crypt::mac_list = QStringList();

Crypt::Crypt(QObject *parent)
	: QObject(parent)
{
	method = "simple";
}

Crypt::~Crypt()
{

}

QString Crypt::crypt(QString str)
{
	QString key = QString::number( 82623304 /*getMacHash()*/ );
	if ( key.isEmpty() ||  key == "0" || str.isEmpty() ) return "";
	
	QByteArray ba;
	for(int i = 0, j = key.count() - 1; i < key.count() / 2; i++, j-- )
		ba.append( (key.at(i).ascii() - QChar('0').ascii()) ^ (key.at(j).ascii() - QChar('0').ascii()) );

	int ikey1 = ba.at(1)*10 + ba.at(0); 
	int ikey2 =  ba.at(3)*10 + ba.at(2);
	
	QByteArray str_ = str.toAscii();
	for(int i = 0; i < str_.size(); i++)
	{
		str_[i] = (int) str_[i] ^ ikey1; 
		str_[i] = (int) str_[i] ^ ikey2; 

	}
	str_ = str_.toBase64();
	
	int count_eq_symbol = 0;
	while(str_.endsWith("="))
	{
		str_.chop( 1 );
		count_eq_symbol++;
	}
	for( int i = 0; i < count_eq_symbol; i++ )
		str_.append('@');

	return str_;
}

QString Crypt::decrypt(QString str)
{
	QString key = QString::number( 82623304 /*getMacHash()*/ );
	if ( key.isEmpty() ||  key == "0" || str.isEmpty() ) return "";
	
	QByteArray ba;
	for(int i = 0, j = key.count() - 1; i < key.count() / 2; i++, j-- )
		ba.append( (key.at(i).ascii() - QChar('0').ascii()) ^ (key.at(j).ascii() - QChar('0').ascii()) );

	int ikey1 = ba.at(1)*10 + ba.at(0); 
	int ikey2 =  ba.at(3)*10 + ba.at(2);
	
	QByteArray str_ = str.toAscii();

	int count_eq_symbol = 0;
	while(str_.endsWith("@"))
	{
		str_.chop( 1 );
		count_eq_symbol++;
	}
	for( int i = 0; i < count_eq_symbol; i++ )
		str_.append('=');
	
	str_ = QByteArray::fromBase64( str_ );
	
	for(int i = 0; i < str_.size(); i++)
	{
		str_[i] = (int) str_[i] ^ ikey2; 
		str_[i] = (int) str_[i] ^ ikey1; 

	}
	return str_;
}

QStringList Crypt::getMACs() 
{
    QString exec = "cmd";
	QStringList params;
	QProcess macProcess;
   // QProcess::startDetached("cmd /c getmac /NH");
	params << "/c" << "getmac" << "/NH";
	macProcess.start(exec, params);
    macProcess.waitForFinished(); // sets current thread to sleep and waits for pingProcess end
	QString output(macProcess.readAllStandardOutput());
	output.remove("\r\n", Qt::CaseInsensitive);
	//connect(&pingProcess, SIGNAL(readyReadStandardOutput()), this, SLOT(readData()));
   
	QStringList mac_list = output.split(' ',  QString::SkipEmptyParts);
	int i = 0; 
	while( i < mac_list.size() )
		 if( mac_list.at(i).size() != 17 ) 
			 mac_list.removeAt(i);
		 else
			i++;

	 for (int i = 0; i < mac_list.size(); ++i)
         mac_list[i].remove(QChar('-'), Qt::CaseInsensitive);
  
  
    return mac_list;
}
unsigned long Crypt::getMacHash()
{
	unsigned long hash = 0; 
	unsigned long hash1 = 0; 
	unsigned long hash2 = 0; 
	QString str1,str2;
	bool ok = false;
	if ( mac_list.isEmpty() )
		mac_list = getMACs();

	for (int i = 0; i < mac_list.size(); ++i)
	{
		hash1 ^=  mac_list[i].left(6).toLong(&ok, 16) * ( i+1 );
		hash2 ^=  mac_list[i].right(6).toLong(&ok, 16) * ( i+1 );
	}
	if ( ok ) return hash1^hash2;
	else return 0;
}