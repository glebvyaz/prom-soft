#include "stdafx.h"
#include "settings.h"
#include "serialPortClass.h"
#include <QFile>
#include <QDataStream>
#include "options.h"

extern options* optionsObj;

extern class requestIP_Cam *reqIP_Cam; // ��������� ����� - ��������� �������� MyClass
currentSettingsIP_Cam *ip_cam_settings_for_compare;
QSettings settingsComPort;
#define MAX_NUM_SCALES 1 // ���������� �������������� �����
#define MAX_NUM_IP_CAMS 4 // ���������� �������������� IP-�����

// ����������� ������ �������� 
SettingsForm::SettingsForm()  
{
	traff_light = 0;
	currentScales = 0; // ������  ����� ������ ����� 
    vectorTypesVP_main = QVector<QComboBox* >(); // ������ comboBox - �� ��� ������ ����  ��
	vectorComPortsVP = QVector<QComboBox* >(); 	// ������ QComboBox-�� COM-������ 
	
	vectorAddrVP = QVector< QSpinBox* >();
	vectorMuxKoef = QVector< QLineEdit* >();

	vectorBaudsVP = QVector<QComboBox* >(); // ������ QComboBox-�� ��������� COM-������ 
	vectorEOFrameVP = QVector<QComboBox* >(); // ������ QComboBox-�� ��������� ������� ������
	vectorWorkWithRM = QVector<QCheckBox* >(); // ������ QCheckBox-�� ���� ������ ����� �����  ��
	vectorGrouBoxWorkWithBK = QVector<QGroupBox* >(); // ������ QGroupBox-�� ��� ������ ����� �� 

	// currentStateOfServer = true;

	// ������� Hash-������� �������� � ������������ � ������� (�����)  ����� � ������ �������������� ����� 
	QList<QString> endsOfFrames = this->getEndsOfFrames();
	QList<QByteArray> endsOfFramesData = this->getEndsOfFramesData();
	int len = endsOfFrames.length();

	// ������� ������� ���  ��������  ��������� ������� �������� �������� �� UART
	for( int i = 0; i < len; i++ )
	{
		hashTypesEndsOfFrames.insert( endsOfFrames.at( i ), endsOfFramesData.at( i ) );
	}
}

// ������ ���� �������� 
void SettingsForm::setupSettingsForm( QList<QString> serPorts, QList<QString> bauds, QObject *obj )
{
    serialPortClass *sp = new serialPortClass(); // �������� ����������� �������� ������ 

    QRect screenResolution = qApp->desktop()->screenGeometry();
	const QRect settWindowSizePos = QRect( 
		( screenResolution.width() / 2 ) - 550/2,
		( screenResolution.height() / 2 ) - 400/2,
		550, 400 );

	const QSize settWindowSize = QSize( 550, 400 );
	const QSize comboBoxSize = QSize( 100, 24 ); // ������ comboBox - � 
	const QSize lineEditSize = QSize( 64, 24 ); // ������ lineEdit - �
	const QSize pushButtonSize = QSize( 165, 32 ); // ������  pushButton - �

	// 
	dialogSettings = new QDialog(); // ������ ���������� ���� �������� 
	dialogSettings->setWindowTitle( tr("Settings") );
	dialogSettings->setStyleSheet( "QTabBar::tab{height: 34px;}" );

	dialogSettings->setGeometry( settWindowSizePos );
	dialogSettings->setModal( true );
	dialogSettings->setFixedSize( settWindowSize );
	
	//QIcon iconsLogo;
	//iconsLogo.setPixmap( "image/advancedsettingspng", QIcon::Large, QIcon::Normal, QIcon::On );
	//dialogSettings->setWindowIcon( iconsLogo );

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// ���� �������� COM-�����
	settComPortWindow = new QWidget( dialogSettings );
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	// ������� �������� ���
    settingsTabs = new QTabWidget( dialogSettings );	
	settingsTabs->addTab( settComPortWindow, tr("COM-port") );
	settingsTabs->setObjectName( "settComPortWindow" );
	settingsTabs->setFixedSize( settWindowSize );

	//������� �������� ������ ����� ��������
	settAnaliticsTab = new QWidget( dialogSettings );
	settAnaliticsTab->setFixedSize( settWindowSize );
	settingsTabs->addTab( settAnaliticsTab, tr("Analytics of\nsensor's codes") );
	settAnaliticsTab->setObjectName( "settAnalitWindow" );
	
	//
	QLabel *label_sInterval = new QLabel( tr("Recording interval, sec") , settAnaliticsTab ); //�������� ������
	label_sInterval->setGeometry( 10, 10, 170, 20 );
	enter_sInterval = new QLineEdit( settAnaliticsTab );
	enter_sInterval->setGeometry( 180, 10, 150, 24 );

	//
	QLabel *label_sNumber = new QLabel( tr("Number of sensors") , settAnaliticsTab ); //���������� ��������
	label_sNumber->setGeometry( 10, 50, 170, 20 );
	enter_sNumber = new QLineEdit( settAnaliticsTab );
	enter_sNumber->setGeometry( 180, 50, 150, 24 );

	//
	QLabel *label_sInfoContragent = new QLabel( tr("Contracting party") , settAnaliticsTab ); //����������
	label_sInfoContragent->setGeometry( 10, 90, 170, 20 );
	enter_sInfoContragent = new QLineEdit( settAnaliticsTab );
	enter_sInfoContragent->setGeometry( 180, 90, 300, 24 );

	//
	QLabel *label_sInfoWType = new QLabel( tr("Type of scales") , settAnaliticsTab ); //��� �����
	label_sInfoWType->setGeometry( 10, 130, 170, 20 );
	enter_sInfoWType = new QLineEdit( settAnaliticsTab );
	enter_sInfoWType->setGeometry( 180, 130, 300, 24 );

	buttonForSaveSettingsAnalit = new QPushButton( tr("Save"), settAnaliticsTab );
	buttonForSaveSettingsAnalit->setGeometry( 10, 210, pushButtonSize.width(), pushButtonSize.height() );
	connect( buttonForSaveSettingsAnalit, SIGNAL( clicked() ), this, SLOT( saveSens_Slot() ) );

	QSettings settingsVP01_s( "settings.ini", QSettings::IniFormat );
	settingsVP01_s.setIniCodec( "Windows-1251" );
	enter_sInterval->setText( settingsVP01_s.value( "vp01_s/interval" ).toString() );
	enter_sNumber->setText( settingsVP01_s.value( "vp01_s/number" ).toString() );
	enter_sInfoContragent->setText( settingsVP01_s.value( "vp01_s/info_contragent" ).toString() );
	enter_sInfoWType->setText( settingsVP01_s.value( "vp01_s/info_type" ).toString() );

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	tabNumScalesSettings = new QTabWidget( settComPortWindow ); // ������� �������� COM-������ 
	tabNumScalesSettings->setFixedSize( settWindowSize );

	numberOfEntityScales = new QVector<QString>();
	numberOfEntityScales->clear();

	// ������ ini - ����  ���  ���������� �������� ������ � ������
	QSettings settingsComPort( "settings.ini", QSettings::IniFormat );
	settingsComPort.setIniCodec( "Windows-1251" );

	int i;
	char n[ 16 ];
	QString strData;

	settingsComPort.beginGroup( "Server" );
	strData = settingsComPort.value( "AutoStart", "" ).toString();
	if( strData == "1" ) // ���� ��������� ������� ������� 
		currentStateOfServer = true;
	else
		currentStateOfServer = false;
	settingsComPort.endGroup();

	for( i = 0; i < MAX_NUM_SCALES; i++ ) // ���������� ���������� ������� ���� �� ����, ������� ������������ 
	{
		memset( n, 0x00, sizeof( n ) );

	    scalesN = new QWidget( settingsTabs );

		sprintf( n, "# %2d", i + 1 );
		tabNumScalesSettings->addTab( scalesN, n );
		
		sprintf( n, "scales%d", i + 1 );
		settingsComPort.beginGroup( QString( n ) ); // ����� ������ ���������� �������� 

		scalesN->setObjectName( QString( n ) ); // ������� ��� ��� ������� ����� N
		numberOfEntityScales->append( scalesN->objectName() ); // �������� ��� ������� � Vector
		
		// 
		serialPort = new QSerialPort();
		sprintf( n, "serialPort%2d", i ); // ������� N �������� ������ COM-����� 
		serialPort->setObjectName( QString( n ) );

		// ��������� ������ �������� COM-����� 
		serPortObjectsList.reserve( MAX_NUM_SCALES );
		serPortObjectsList.append( serialPort );  // new QSerialPort() );
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// ���� ��
		typesVPLabel = new QLabel( tr("Weighing proc. model"), scalesN );
		typesVPLabel->setGeometry( 10, 0, 395, comboBoxSize.height() );
		typesVPLabel->setStyleSheet( "font-size: 18; font-weight: bold" ); // ������� � ��������� ���� 

		QList<QString> typeVpList = this->loadTypesOfVP();
		typesVPList = new QComboBox( scalesN );
		typesVPList->setGeometry( 10, 25, comboBoxSize.width() + 64, comboBoxSize.height() );
		typesVPList->addItems( typeVpList ); // ���������� ���� ��
		vectorTypesVP_main.append( typesVPList ); // �������� ������  comboBox  ������  ����  ������� � ������ 

		strData = settingsComPort.value( "type_scales", "" ).toString(); // �������� �� ini - ����� �������� ���� �����
		
		if (! typeVpList.contains( strData ) )
		{
			strData = typeVpList.at(0);
			settingsComPort.setValue("type_scales", strData );
			settingsComPort.sync();
		}
		
		typesVPList->setCurrentIndex( typeVpList.indexOf( strData ) );

		// bind typesVPList and endOfFrame
		connect( typesVPList, SIGNAL( activated(int) ), this, SLOT( setEndOfFrame_Slot(int) ) );

		

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// Controls ��������� � ������� COM-����� 
		comPortsNamesLabel = new QLabel( "COM", scalesN );
		comPortsNamesLabel->setGeometry( 10, 50, comboBoxSize.width(), comboBoxSize.height() );
		comPortsNamesLabel->setStyleSheet( "font-size: 18; font-weight: bold" ); // ???  ������� � ��������� ����

		comPortsList = new QComboBox( scalesN );
		comPortsList->setFixedSize( comboBoxSize ); // ����� ������ comboBox
		comPortsList->setGeometry( 10, 75, comboBoxSize.width() + 32 + 32, comboBoxSize.height() );

		sprintf( n, "comPortsList%2d", i ); // ������� N �������� ������ COM-����� 
		comPortsList->setObjectName( QString( n ) );
		comPortsList->addItems( serPorts );
		vectorComPortsVP.append( comPortsList ); // �������� comboBox - �  COM-������� � ������ 

		strData = settingsComPort.value( "com", "" ).toString(); // �������� �� ini - ����� COM-����� 
		comPortsList->setCurrentIndex( ( serPorts.indexOf( strData ) != -1 ? serPorts.indexOf( strData ) : 0  ) );
        // 
		QLabel *labelAddrVP = new QLabel( tr("Address WP"), scalesN );
		labelAddrVP->setGeometry( 12 + comboBoxSize.width() + 2, 50, comboBoxSize.width(), comboBoxSize.height() );

		spinBoxAddrVP = new QSpinBox( scalesN );
		spinBoxAddrVP->setGeometry( 14 + comboBoxSize.width() + 2, 75, comboBoxSize.width() / 2 + 6, comboBoxSize.height() );
		spinBoxAddrVP->setMinimum( 1 );
		spinBoxAddrVP->setMaximum( 99 );
		strData = settingsComPort.value( "addr", "" ).toString(); // �������� �� ini - ����� COM-�����
		spinBoxAddrVP->setValue( strData.toInt() );
		vectorAddrVP.append( spinBoxAddrVP );

		// 
		labelMuxKoeff = new QLabel( tr("Koef."), scalesN );
		labelMuxKoeff->setGeometry( 20 + comboBoxSize.width() + 2, 100, comboBoxSize.width(), comboBoxSize.height() );

	    enterMuxKoeff = new QLineEdit( scalesN );
		enterMuxKoeff->setGeometry( 14 + comboBoxSize.width() + 2, 125, comboBoxSize.width() / 2 + 6, comboBoxSize.height() );
		strData = settingsComPort.value( "koef", "" ).toString();
		enterMuxKoeff->setText( strData );
		vectorMuxKoef.append( enterMuxKoeff );

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		//
		baudRatesNamesList = new QLabel( tr("Baud rate"), scalesN );
		baudRatesNamesList->setGeometry( 10, 100, comboBoxSize.width(), comboBoxSize.height() );
		baudRatesNamesList->setStyleSheet( "font-size: 18; font-weight: bold" ); // ������� � ��������� ���� 

		baudRatesList = new QComboBox( scalesN );
		baudRatesList->setFixedSize( comboBoxSize ); // ����� ������ comboBox
		baudRatesList->setGeometry( 10, 125, comboBoxSize.width() + 32, comboBoxSize.height() );
		baudRatesList->addItems( bauds );
		baudRatesList->setCurrentIndex( DEFAULT_UART_BAUD );
		vectorBaudsVP.append( baudRatesList ); // ������ QComboBox-�� ��������� COM-������ 

		strData = settingsComPort.value( "baud", "" ).toString(); // �������� �� ini - ����� �������� �������� COM-����� 
		baudRatesList->setCurrentIndex( bauds.indexOf( strData ) );
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// 
		endOfFrameLabel = new QLabel( tr("End of frame"), scalesN ); // ����� �������
		endOfFrameLabel->setGeometry( 10, 150, comboBoxSize.width(), comboBoxSize.height() );
		endOfFrameLabel->setStyleSheet( "font-size: 18; font-weight: bold" ); // ???  ������� � ��������� ����

		endOfFrame = new QComboBox( scalesN );
		endOfFrame->setFixedSize( comboBoxSize ); // ����� ������ comboBox
		endOfFrame->setGeometry( 10, 175, comboBoxSize.width() + 32, comboBoxSize.height() );
		endOfFrame->addItems( this->getEndsOfFrames() );
		vectorEOFrameVP.append( endOfFrame ); // ������ QComboBox-�� ��������� ������� ������ 
		strData = settingsComPort.value( "endoframe", "" ).toString(); // �������� �� ini - ����� �������� ��������� �������� ������ 
		endOfFrame->setCurrentIndex( this->getEndsOfFrames().indexOf( strData ) );
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// ���./����. ������ ����� ��
		chkBoxRM_Work = new QCheckBox( scalesN ); // ��������� ������ � ������ ����� ���������� 
		chkBoxRM_Work->setText( tr("Radio modem") ); 
		chkBoxRM_Work->setGeometry( 10, 210, 180, 18 );
        vectorWorkWithRM.append( chkBoxRM_Work ); // ������ QCheckBox-�� ���� ������ ����� �����  �� 
		chkBoxRM_Work->hide();

		strData = settingsComPort.value( "rm", "" ).toString(); // �������� �� ini - ����� ��������� ������������� ��
		( strData == "1" ? chkBoxRM_Work->setChecked( true ) : chkBoxRM_Work->setChecked( false ) );
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		checkBoxUSB_En = new QCheckBox( scalesN ); // ��������� ������ ���������� �����
		checkBoxUSB_En->setText( tr("Periodic reopen") ); 
		checkBoxUSB_En->setGeometry( 10, 270, 140, 18 );
        vectorcheckBoxUSB_En.append( checkBoxUSB_En ); // ������ QCheckBox-�� ����  ����������  ������ �����

		strData = settingsComPort.value( "usb", "" ).toString();
		( strData == "1" ? checkBoxUSB_En->setChecked( true ) : checkBoxUSB_En->setChecked( false ) );
		//
		enableScalesEntity = new QCheckBox( scalesN ); // ��������� ������ ���������� �����
		enableScalesEntity->setText( tr("Switch ON") ); 
		enableScalesEntity->setGeometry( 195, 210 + 24, 180, 18 );
        vectorEnableScalesEntity.append( enableScalesEntity ); // ������ QCheckBox-�� ����  ����������  ������ ����� 
		enableScalesEntity->hide();

		strData = settingsComPort.value( "scales_enable", "" ).toString();
		( strData == "1" ? enableScalesEntity->setChecked( true ) : enableScalesEntity->setChecked( false ) );
		
		// ���
		group_Sornost = new QGroupBox( scalesN );
		group_Sornost->setGeometry( 210, 5, 310, 95 );
		group_Sornost->setTitle( tr("Soreness") ); 
		group_Sornost->setCheckable( true );
		group_Sornost->setChecked( settingsComPort.value( "sor_on" ).toBool() );

		if( !optionsObj->isCheked[optionsObj->modules.indexOf("sornost")] )
		{
			group_Sornost->setChecked( false );
			group_Sornost->setEnabled( false );
		}

		QLabel *labelKoefSor = new QLabel( tr("Koef."), group_Sornost );
		labelKoefSor->setGeometry( 10, 20, 40, 18 );
		spinKoefSor = new QDoubleSpinBox( group_Sornost );
		spinKoefSor->setGeometry( 60, 20, 100, 18 );
		spinKoefSor->setRange( 0, 1 );
		spinKoefSor->setDecimals( 4 );
		spinKoefSor->setSingleStep( 0.0001 );
		spinKoefSor->setValue( settingsComPort.value( "sor_koef" ).toDouble() );

		checkSorDiscret = new QCheckBox( group_Sornost );
		checkSorDiscret->setGeometry( 180, 20, 120, 18 );
		checkSorDiscret->setText( tr("Discret 20kg") );
		checkSorDiscret->setChecked( settingsComPort.value( "sor_discret" ).toBool() );

		checkSorWeightShow = new QCheckBox( group_Sornost );
		checkSorWeightShow->setGeometry( 10, 50, 210, 18 );
		checkSorWeightShow->setText( tr("Show weight") );
		checkSorWeightShow->setChecked( settingsComPort.value( "sor_show" ).toBool() );

		checkSorUseOnlyBrutto = new QCheckBox( group_Sornost );
		checkSorUseOnlyBrutto->setGeometry( 10, 70, 210, 18 );
		checkSorUseOnlyBrutto->setText( tr("Use for brutto only") );
		checkSorUseOnlyBrutto->setChecked( settingsComPort.value( "sor_brutto" ).toBool() );

		// ������.
		group_TCPWeight = new QGroupBox( scalesN );
		group_TCPWeight->setGeometry( 6, 210, 180, 50 );
		group_TCPWeight->setTitle( tr("Weight by TCP/IP converter") ); 
		group_TCPWeight->setCheckable( true );
		group_TCPWeight->setChecked( settingsComPort.value( "tcp_on" ).toBool() );

		if(group_TCPWeight->isChecked() )
			emit StartTimerWeight(); 
		
		QLabel *labelTCP_IP = new QLabel( tr("IP"), group_TCPWeight );
		labelTCP_IP->setGeometry( 8, 20, 30, 18 );
		lineEditTCP_IP = new QLineEdit( group_TCPWeight );
		lineEditTCP_IP->setGeometry( 30, 20, 120, 18 );
		lineEditTCP_IP->setText( settingsComPort.value( "tcp_ip" ).toString() );

		// ����������������
		group_PositionS2 = new QGroupBox( scalesN );
		group_PositionS2->setGeometry( 210, 105, 310, 50 );
		group_PositionS2->setTitle( settingsComPort.value( "position_use_s1" ).toBool() ? tr("Position on Socket1") : tr("Position on Socket2") ); 
		group_PositionS2->setCheckable( true );
		group_PositionS2->setChecked( settingsComPort.value( "position_on" ).toBool() );
		
		QLabel *label_PositionS2 = new QLabel( tr("IP"), group_PositionS2 );
		label_PositionS2->setGeometry( 10, 20, 30, 18 );
		lineEdit_PositionS2 = new QLineEdit( group_PositionS2 );
		lineEdit_PositionS2->setGeometry( 50, 20, 110, 18 );
		lineEdit_PositionS2->setText( settingsComPort.value( "position_ip" ).toString() );

		check_PositionS2 = new QCheckBox( tr("Inversion"), group_PositionS2 );
		check_PositionS2->setGeometry( 165, 8, 90, 24 );
		check_PositionS2->setChecked( settingsComPort.value( "position_inversion" ).toBool() );

		check_PositionUseS1 = new QCheckBox( tr("Use S1"), group_PositionS2 );
		check_PositionUseS1->setGeometry( 236, 8, 90, 24 );
		check_PositionUseS1->setChecked( settingsComPort.value( "position_use_s1" ).toBool() );

		check_PositionUse4ch = new QCheckBox( tr("Use 4ch"), group_PositionS2 );
		check_PositionUse4ch->setGeometry( 236, 28, 90, 24 );
		check_PositionUse4ch->setChecked( settingsComPort.value( "position_4ch" ).toBool() );
		
		check_PositionIgnore = new QCheckBox( tr("Ignore"), group_PositionS2 );
		check_PositionIgnore->setGeometry( 165, 28, 90, 24 );
		check_PositionIgnore->setChecked( settingsComPort.value( "position_ignore" ).toBool() );



		
		if( !optionsObj->isCheked[optionsObj->modules.indexOf("datchik")] )
		{
			group_PositionS2->setChecked( false );
			group_PositionS2->setEnabled( false );
		}

		// ���������
		group_SvetoforS2 = new QGroupBox( scalesN );
		group_SvetoforS2->setGeometry( 210, 160, 310, 50 );
		group_SvetoforS2->setTitle( tr("Trafflight on Socket2") ); 
		group_SvetoforS2->setCheckable( true );
		group_SvetoforS2->setChecked( settingsComPort.value( "svetofor_s2_on" ).toBool() );
		
		QLabel *label_SvetoforS2 = new QLabel( tr("IP"), group_SvetoforS2 );
		label_SvetoforS2->setGeometry( 10, 20, 30, 18 );
		lineEdit_SvetoforS2 = new QLineEdit( group_SvetoforS2 );
		lineEdit_SvetoforS2->setGeometry( 50, 20, 110, 18 );
		lineEdit_SvetoforS2->setText( settingsComPort.value( "svetofor_s2_ip" ).toString() );

				
		if( !optionsObj->isCheked[optionsObj->modules.indexOf("svetofor")] )
		{
			group_SvetoforS2->setChecked( false );
			group_SvetoforS2->setEnabled( false );
		}

		// ���������
		group_BarrierS2 = new QGroupBox( scalesN );
		group_BarrierS2->setGeometry( 210, 215, 310, 50 );
		group_BarrierS2->setTitle( tr("Barrier on Socket2") ); 
		group_BarrierS2->setCheckable( true );
		group_BarrierS2->setChecked( settingsComPort.value( "barrier_s2_on" ).toBool() );
		
		QLabel *label_BarrierS2 = new QLabel( tr("IP"), group_BarrierS2 );
		label_BarrierS2->setGeometry( 10, 20, 30, 18 );
		lineEdit_BarrierS2 = new QLineEdit( group_BarrierS2 );
		lineEdit_BarrierS2->setGeometry( 50, 20, 140, 18 );
		lineEdit_BarrierS2->setText( settingsComPort.value( "barrier_s2_ip" ).toString() );
				
		//if( !optionsObj->isCheked[optionsObj->modules.indexOf("shlagbaum")] )
		//{
			group_BarrierS2->setChecked( false );
			group_BarrierS2->setEnabled( false );
		//}
			

        // ������ ���������� ��� ���������� ��-01
		groupBK_Params = new QGroupBox( scalesN ); //  ��������������� ��������� ��� ��
		groupBK_Params->setGeometry( 180, 5, 182, 195 );
		groupBK_Params->setTitle( tr("Use BC-01") ); 
		groupBK_Params->setStyleSheet( "font-size: 16; font-weight: none; border-width: 2px; " ); 
		groupBK_Params->setCheckable( true ); // ���������� ������� ����������� 
		vectorGrouBoxWorkWithBK.append( groupBK_Params );
	groupBK_Params->hide();

		strData = settingsComPort.value( "bk", "" ).toString(); // �������� �� ini - ����� �������� ��������� �������� ������ 
		( strData == "1" ? groupBK_Params->setChecked( true ) : groupBK_Params->setChecked( false ) );
        // QObject::connect( groupBK_Params, SIGNAL( toggled( bool ) ), obj, SLOT() ); 
		// ??? ��� ������ � ��-01 - �������� ������ � ������� ����������  turnOnOffTrafficLights( bool ) ) ); // ���������/���������� ���������� 
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// 
		typeVP_ForBKLabel = new QLabel( tr("WP Model via BC-01"), groupBK_Params ); 
		typeVP_ForBKLabel->setGeometry( 10, 20, 160, 18 );
		typeVP_ForBKLabel->setStyleSheet( "font-size: 18; font-weight: bold" ); 

		typeVP_ForBKComboBox = new QComboBox( groupBK_Params );
		typeVP_ForBKComboBox->setGeometry( 10, 40, comboBoxSize.width() + 57, comboBoxSize.height() );
		typeVP_ForBKComboBox->addItems( this->loadTypesOfVP_BK() ); // ���������� ���� ��  ???
		vectorTypesVP_BK.append( typeVP_ForBKComboBox ); // ������ ����� ��������  �� 

        QList<QString> typeBK_VP_List = this->loadTypesOfVP_BK();

		strData = settingsComPort.value( "bk_type_scales", "" ).toString(); // �������� �� ini - ����� �������� ���� �� ��� ������ � �� 
		typeVP_ForBKComboBox->setCurrentIndex( typeBK_VP_List.indexOf( strData ) ); // ??? (��������� ������ �������� ��� ������ � ��-01)
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// 
        filterInputsBKLabel = new QLabel( tr("Input delay, ms"), groupBK_Params ); 
		filterInputsBKLabel->setGeometry( 10, 70, 160, 18 );
		filterInputsBKLabel->setStyleSheet( "font-size: 18; font-weight: bold" ); 

		filterInputsBK_LineEdit = new QLineEdit( groupBK_Params ) ; // ���� �������� �������� ���������������� 
		filterInputsBK_LineEdit->setGeometry( 10, 90, comboBoxSize.width() + 57, lineEditSize.height() );
		vectorInputDelay_BK.append( filterInputsBK_LineEdit ); // ������ �������  textBox - ��,  ���  �������� ������� ��-01 

		strData = settingsComPort.value( "input_delay", "500" ).toString(); 
		// �������� �� ini - ����� �������� �������� ������������ �������� ���������������� 
		filterInputsBK_LineEdit->setText( strData );

		// ���������� ��������
		trafficLightCheckBox = new QCheckBox( groupBK_Params ); // ��������� ������ ���������� �����
		trafficLightCheckBox->setText( tr("Show traffic light") );
		trafficLightCheckBox->setGeometry( 10, 120, 180, 18 );
		strData = settingsComPort.value( "traff", "" ).toString();
		( strData == "1" ? trafficLightCheckBox->setChecked( true ) : trafficLightCheckBox->setChecked( false ) );

		sensorsCheckBox = new QCheckBox( groupBK_Params ); // ��������� ������ ���������� �����
		sensorsCheckBox->setText( tr("Enable position sensors") );
		sensorsCheckBox->setGeometry( 10, 138, 180, 36 );
		strData = settingsComPort.value( "sensors", "1" ).toString();
		( strData == "1" ? sensorsCheckBox->setChecked( true ) : sensorsCheckBox->setChecked( false ) );

		sensorsInversionCheckBox = new QCheckBox( groupBK_Params ); // ��������� ������ ���������� �����
		sensorsInversionCheckBox->setText( tr("Sensor inversion") );
		sensorsInversionCheckBox->setGeometry( 10, 164, 180, 36 );
		strData = settingsComPort.value( "sensors_inversion", "0" ).toString();
		( strData == "1" ? sensorsInversionCheckBox->setChecked( true ) : sensorsInversionCheckBox->setChecked( false ) );

        // ������ ���������� ���������� 
		QIcon okButton;
		buttonForSaveSettings = new QPushButton( tr("Save"), scalesN ); 
		buttonForSaveSettings->setStyleSheet( "color: #000; font-weight: bold; " );
		buttonForSaveSettings->setGeometry( 187, 270, pushButtonSize.width(), pushButtonSize.height() );
		buttonForSaveSettings->setObjectName( "buttonForSaveSettings" );
		//okButton.setPixmap( "image/ok_32_32_bigpng", QIcon::Large, QIcon::Normal, QIcon::On );
	    //buttonForSaveSettings->setIcon( okButton );
		// 
        // ������ ����� �� ���� ��������� ���������
		QIcon cancelButton;
		buttonExitSettings = new QPushButton( tr("Cancel"), scalesN );
		buttonExitSettings->setStyleSheet( "color: #000; font-weight: bold; " );
		buttonExitSettings->setObjectName( "buttonExitSettings" );
		buttonExitSettings->setGeometry( 365, 270, pushButtonSize.width(), pushButtonSize.height() );
		//cancelButton.setPixmap( "image/cancel_32_32_bigpng", QIcon::Large, QIcon::Normal, QIcon::On );
	   // buttonExitSettings->setIcon( cancelButton );

		// 
		// ��������� UDP-���������� � ������ 
		QGroupBox *groupUDP_Settings = new QGroupBox( scalesN );
		groupUDP_Settings->setGeometry( 180 + 187, 5, 175, 195 );
		groupUDP_Settings->setTitle( tr("UDP params") ); 
		groupUDP_Settings->setStyleSheet( "font-size: 16; font-weight: none; border-width: 2px; " );
		groupUDP_Settings->setChecked( false ); // ??? ��� ������ - �������� �� ini ����� 
        groupUDP_Settings->setCheckable( true ); // ���������� ������� ����������� 
		groupUDP_Settings->hide();

		strData = settingsComPort.value( "udp_en", "" ).toString();
		( strData == "1" ? groupUDP_Settings->setChecked( true ) : groupUDP_Settings->setChecked( false ) );

		if( strData == "1" )
		{
			vectorComPortsVP.at( i )->setEnabled( false );
			vectorBaudsVP.at( i )->setEnabled( false );
			vectorEOFrameVP.at( i )->setEnabled( true );
		}
		else
		{
			vectorComPortsVP.at( i )->setEnabled( true );
			vectorBaudsVP.at( i )->setEnabled( true );
			vectorEOFrameVP.at( i )->setEnabled( true );
		}

		bool flag = QObject::connect( groupUDP_Settings, SIGNAL( toggled( bool ) ), obj, SLOT( groupUDP_Settings_Slot( bool ) ) );
		vectorGroupUDP_Settings.append( groupUDP_Settings );
		// 
		QLabel *labelUDP_IP_address = new QLabel( tr("IP-address"), groupUDP_Settings );
		labelUDP_IP_address->setGeometry( 10, 20, 180, 18 );

		enterIP_UDP_Settings = new QLineEdit( groupUDP_Settings );
		enterIP_UDP_Settings->setGeometry( 10, 41, 156, 24 );
		enterIP_UDP_Settings->setText( settingsComPort.value( "ip" ).toString() );
		vectorEnterIP_UDP_Settings.append( enterIP_UDP_Settings );
		// 
		QLabel *labelUDP_PortSrc = new QLabel( tr("Src. port"), groupUDP_Settings );
		labelUDP_PortSrc->setGeometry( 10, 68, 180, 18 );
	
		enterSrcPort_UDP_Settings = new QLineEdit( groupUDP_Settings );
		enterSrcPort_UDP_Settings->setGeometry( 10, 89, 120, 24 );
		enterSrcPort_UDP_Settings->setText( settingsComPort.value( "src_port" ).toString() );
		vectorEnterSrcPort_UDP_Settings.append( enterSrcPort_UDP_Settings );
		// 
		QLabel *labelUDP_PortDst = new QLabel( tr("Dest. port"), groupUDP_Settings );
		labelUDP_PortDst->setGeometry( 10, 116, 180, 18 );
	
		enterDstPort_UDP_Settings = new QLineEdit( groupUDP_Settings );
		enterDstPort_UDP_Settings->setGeometry( 10, 134, 120, 24 );
		enterDstPort_UDP_Settings->setText( settingsComPort.value( "dst_port" ).toString() );
		vectorEnterDstPort_UDP_Settings.append( enterDstPort_UDP_Settings );
		
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	    // ������������ ����������� ������� ��� �������� COM-������ 
	    connect( typesVPList, SIGNAL( activated( int ) ), obj, SLOT( selectedNewTypeVP( int ) ) );
		connect( comPortsList, SIGNAL( activated( int ) ), obj, SLOT( selectedNewComPort( int ) ) );
		connect( baudRatesList, SIGNAL( activated( int ) ), obj, SLOT( selectedNewComPortSpeed( int ) ) );
		connect( groupBK_Params, SIGNAL( toggled( bool ) ), obj, SLOT( chkBoxBK_Work_stateChanged( bool ) ) );
		connect( chkBoxRM_Work, SIGNAL( stateChanged( int ) ), obj, SLOT( chkBoxRM_Work_stateChanged( int ) ) );
		connect( buttonExitSettings, SIGNAL( clicked() ), obj, SLOT( closeSettingsWin() ) );
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// ���������� ������ COM-����� � ComboBox
		comPortsList->installEventFilter( this );
		// ���������� ������ "������� ���� ����������"
		buttonForSaveSettings->installEventFilter( this );
        // 
		settingsComPort.endGroup(); //  ���������  ������  ����������  �  �����  *.ini
	}

	// ������� Hash-������� �������� � ������������ � ������� (�����)  ����� � ������ �������������� ����� 
	QList<QString> listVP_Requests = this->loadVP_REquests();
	int len = listVP_Requests.length();
	for( int i = 0; i < len; i++ )
	{
		hashTypesVP_RequestNum.insert( ( const QString )listVP_Requests.at( i ), i );
	}

    // ������ ������ ������ ������� 
    buttonStartServer = new QPushButton( settComPortWindow ); 
    buttonStartServer->setGeometry( 10, 308, pushButtonSize.width(), pushButtonSize.height() );
    buttonStartServer->setStyleSheet( "font-size: 18; font-weight: bold; color: Green" );
    buttonStartServer->setObjectName( "buttonStartServer" );
    buttonStartServer->installEventFilter( this );
	buttonStartServer->hide();

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// ��������� IP-����� 
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// ���� �������� IP-����� 
	
	IpCamsSettings = new QWidget( dialogSettings );
	IpCamsSettings->setFixedSize( settWindowSize );	
	// ������
	//IpCamsSettings->hide();
	settingsTabs->addTab( IpCamsSettings, tr("IP cams") );
	tabNumIP_Cam_Settings = new QTabWidget( IpCamsSettings ); // ������� IP-������ � ������� �� �������
    tabNumIP_Cam_Settings->setFixedSize( settWindowSize );
	
	// ������� ������� ��������  IP-����� 
	numberOfEntityIP_Cams = new QVector<QString>(); // ��� ������� ������� IP-������
	numberOfEntityIP_Cams->clear();

	for( int i = 0; i < this->getNumIP_Cams(); i++ )
	{
        memset( n, 0x00, sizeof( n ) );

		IP_CamTab_N = new QWidget( settingsTabs ); // ������� ��� IP-�����
		IP_CamTab_N->setGeometry(
			( screenResolution.width() / 2 ) - ( screenResolution.width() / 5 ),
			( screenResolution.height() / 5 ) - ( screenResolution.height() / 30 ),
		    screenResolution.width() / 3,
			screenResolution.height() / 3 ); // ���������� ������ ������� IP-������ 

		sprintf( n, "# %2d", i + 1 ); // ������������ ������� IP_������ 
		tabNumIP_Cam_Settings->addTab( IP_CamTab_N, n ); // �������� ������� ���������� IP-������ �� ������ �������� IP-������ 
		tabNumIP_Cam_Settings->setFixedSize( settWindowSize );

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		sprintf( n, "Cam%d", i + 1 );
		IP_CamTab_N->setObjectName( QString( n ) ); // ������� ��� ������� ������� IP ������ 
		numberOfEntityIP_Cams->append( IP_CamTab_N->objectName() ); // �������� ����� �������� ������� IP-����� 

		settingsComPort.beginGroup( QString( n ) ); // ����� ������ ���������� �������� IP-������ 
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	    // �������� ��� �������� IP-����� 
		groubBoxIP_Cams = new QGroupBox( IP_CamTab_N ); // ��������� �������� IP-����� 
		groubBoxIP_Cams->setGeometry( 10, 15, 356, 155 );
		groubBoxIP_Cams->setStyleSheet( "font-weight: bolder; " );
		groubBoxIP_Cams->setTitle( tr("IP cam settings") ); 
		// 
		labelIP_Camera = new QLabel( tr("IP-address"), groubBoxIP_Cams ); 
		labelIP_Camera->setGeometry( 10, 15, 180, comboBoxSize.height() );
		labelIP_Camera->setStyleSheet( "font-size: 18; font-weight: bold; " ); 

		enterIP_Address = new QLineEdit( groubBoxIP_Cams ); // ���� IP-������ 
		enterIP_Address->setGeometry( 10, 40, lineEditSize.width() + 64, lineEditSize.height() );
		vectorEnterIP_Address.append( enterIP_Address ); // ������ ���� ����� IP-������� ��� IP-������
		strData = settingsComPort.value( "ip_address" ).toString();
		enterIP_Address->setText( strData ); // ���������� IP-����� ������ 
		// 
		labelPort_Camera = new QLabel( tr("Port"), groubBoxIP_Cams ); 
		labelPort_Camera->setGeometry( 165, 15, 32, comboBoxSize.height() );
		labelPort_Camera->setStyleSheet( "font-size: 18; font-weight: bold; " ); 

		enterPort_Address= new QLineEdit( groubBoxIP_Cams ); // ���� ����� ��� IP-������ 
		enterPort_Address->setGeometry( lineEditSize.width() + 64 + 20, 40, lineEditSize.width() + 24, lineEditSize.height() );
		vectorEnterPort_Address.append( enterPort_Address ); // ������ ���� ����� IP-������� ��� IP-������
		strData = settingsComPort.value( "port" ).toString();
		enterPort_Address->setText( strData ); // ���������� ���� ������ 
		// 
		chkboxEnableIP_Cam = new QCheckBox( groubBoxIP_Cams ); // ������� ���������� IP-������ 
		chkboxEnableIP_Cam->setGeometry( 10, 75, 230, comboBoxSize.height() );
		chkboxEnableIP_Cam->setText( tr("Enable") ); 
		
		chkboxEnableIP_Cam->setStyleSheet( "font-size: 18; font-weight: bold; " ); //������� � ��������� ����
		vectorChkboxEnableIP_Cam.append( chkboxEnableIP_Cam ); // ������ ��������� ��� ���������� ������ IP-������ 
		strData = settingsComPort.value( "cam_en", 0 ).toString();
		( ( strData == "1" ) ? chkboxEnableIP_Cam->setChecked( true ) : chkboxEnableIP_Cam->setChecked( false ) ); // ������� ���������� ������ IP-������ 
		// 
        checkBoxEnablePhotoFix = new QCheckBox( groubBoxIP_Cams ); // ������� ���������� ������������ 
		checkBoxEnablePhotoFix->setGeometry( 10, 100, 230, comboBoxSize.height() );
		checkBoxEnablePhotoFix->setText( tr("Photo enable") ); 
		checkBoxEnablePhotoFix->setStyleSheet( "font-size: 18; font-weight: bold; " );
		vectorCheckBoxEnablePhotoFix.append( checkBoxEnablePhotoFix ); // ������ ��������� ���������� ������������ 
		strData = settingsComPort.value( "cam_photofix_en", 0 ).toString();
		( ( strData == "1" ) ? checkBoxEnablePhotoFix->setChecked( true ) : checkBoxEnablePhotoFix->setChecked( false ) ); // ������� ���������� ������ ������������ IP-������ 
		// 
		checkBoxEnablePhotoFixPreview = new QCheckBox( groubBoxIP_Cams ); // ������� ���������� snapshot - �� ��� ������������ 
		checkBoxEnablePhotoFixPreview->setGeometry( 10, 125, 320, comboBoxSize.height() );
		checkBoxEnablePhotoFixPreview->setText( tr("Photo preview enable") );
		checkBoxEnablePhotoFixPreview->setStyleSheet( "font-size: 18; font-weight: bold; " ); 
		vectorCheckBoxEnablePhotoFixPreview.append( checkBoxEnablePhotoFixPreview ); // vector checkboxes ��� ���������� snapshot - �� ��� ������������ 
		strData = settingsComPort.value( "cam_photofix_preview_en", 0 ).toString();
		( ( strData == "1" ) ? checkBoxEnablePhotoFixPreview->setChecked( true ) : checkBoxEnablePhotoFixPreview->setChecked( false ) ); // ������� ���������� ����������� ������� ��� ������������ IP-������

		
        labelLogin = new QLabel( tr("Login"), IP_CamTab_N );
		labelLogin->setGeometry( lineEditSize.width() + 195 + 41 , 30, 50, comboBoxSize.height() );
		labelLogin->setStyleSheet( "font-size: 18; font-weight: bold; " ); 

		enterLogin = new QLineEdit( groubBoxIP_Cams ); // ���� ����� ��� ����������� � IP-������� 
	    enterLogin->setGeometry( lineEditSize.width() + 195, 40, lineEditSize.width() + 24, lineEditSize.height() );
		vectorEnterLogin.append( enterLogin ); // ������ ���� ����� ����� ��� ����������� IP-������ 
		strData = settingsComPort.value( "login" ).toString();
		enterLogin->setText( strData ); // ���������� ��� ��� ����������� 
		//
		labelPassword = new QLabel( tr("Password"), IP_CamTab_N );
		labelPassword->setGeometry( lineEditSize.width() + 195 + 32 , 80, 64, comboBoxSize.height() );
		labelPassword->setStyleSheet( "font-size: 18; font-weight: bold; " ); 

		enterPass = new QLineEdit( groubBoxIP_Cams ); // ���� ������ ��� ����������� � IP-������� 
	    enterPass->setGeometry( lineEditSize.width() + 195, 91, lineEditSize.width() + 27, lineEditSize.height() );
		enterPass->setEchoMode( QLineEdit::Password );
		vectorEnterPass.append( enterPass ); // ������ ���� ����� ������ ��� ����������� IP-������ 
		
		strData = settingsComPort.value( "password" ).toString();
		//enc-dec pass
		QString pass = strData;
		QString pass_enc;
		if ( ! pass.startsWith("#") )
			pass_enc = Crypt::crypt( pass );
		else
		{
			pass_enc = pass.remove( 0, 1 );
			pass = Crypt::decrypt( pass_enc );
		}
		strData = pass_enc.prepend("#");
		// 
		enterPass->setText( strData ); // ���������� ������ ��� ����������� 
		settingsComPort.endGroup(); //  ���������  ������  ����������  �  �����  *.ini
	}

    // ��������� ��� ��������� � Vector �������� ����������� �������� IP-����� 
    vector_ip_cams_settings = QVector<currentSettingsIP_Cam*>();

	check_HorizontalCams = new QCheckBox( tr("Show cams in a horizontal row (vertical if not checked)"), tabNumIP_Cam_Settings );
	check_HorizontalCams->setGeometry( 10, 210, 300, 20 );
	check_HorizontalCams->setChecked( settingsComPort.value( "main_cameras/isHorizontal", true ).toBool() );

    buttonSaveSettingsIP_Cams = new QPushButton( tr("Save"), tabNumIP_Cam_Settings ); // ������ ���������� �������� IP-������ 
	buttonSaveSettingsIP_Cams->setGeometry( 10, 240, pushButtonSize.width(), pushButtonSize.height() );
	buttonSaveSettingsIP_Cams->setStyleSheet( "color: #000; font-weight: bold; " );
	buttonSaveSettingsIP_Cams->setObjectName( "buttonSaveSettingsIP_Cams" );
	buttonSaveSettingsIP_Cams->installEventFilter( this );

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    chkBoxServer_AutoStart = new QCheckBox( settComPortWindow ); //  ���������������  �����  ��������  �������
    chkBoxServer_AutoStart->setText( tr("Server autostart") ); 
    chkBoxServer_AutoStart->setGeometry( 10, 210 + 48+15, 140, 18 );
	chkBoxServer_AutoStart->hide();
	// ��������� ������� 
	settingsComPort.beginGroup( "Server" );
    strData = settingsComPort.value( "AutoStart", "" ).toString(); // �������� �� ini - ����� ���� ��������������� ������ 
    ( strData == "1" ? chkBoxServer_AutoStart->setChecked( true ) : chkBoxServer_AutoStart->setChecked( false ) );
	
	displayServerState( buttonStartServer, ( strData == "1" ? true: false ) ); // ���������� ��������� ������� (��������� ��� ����������)
	settingsComPort.endGroup();

	// 
	// ��������� ����� 
	eMailSettings = new QWidget( dialogSettings );
	eMailSettings->setFixedSize( settWindowSize );
	settingsTabs->addTab( eMailSettings, tr("Email settings") );
	
	labelHostMail = new QLabel( tr("Host"), eMailSettings );
	labelHostMail->setGeometry( 10, 20, 120, 18 );

	enterHostName = new QLineEdit( eMailSettings );
	enterHostName->setGeometry( 10, 41, 140, 24 );
	enterHostName->setText( settingsComPort.value( "mail/host" ).toString() );

	labelPortMail = new QLabel( tr("SMTP"), eMailSettings );
	labelPortMail->setGeometry( 10, 70, 120, 18 );

	enterPort = new QLineEdit( eMailSettings );
	enterPort->setGeometry( 10, 90, 140, 24 );
	enterPort->setText( settingsComPort.value( "mail/port" ).toString() );

    labelLoginEmail = new QLabel( tr("Login"), eMailSettings );
	labelLoginEmail->setGeometry( 10, 118, 120, 18 );

	enterLoginEmail = new QLineEdit( eMailSettings );
	enterLoginEmail->setGeometry( 10, 138, 284, 24 );
	enterLoginEmail->setText( settingsComPort.value( "mail/login" ).toString() );

    labelPasswordEmail = new QLabel( tr("Password"), eMailSettings );
	labelPasswordEmail->setGeometry( 10, 172, 120, 18 );

	enterPassword = new QLineEdit( eMailSettings );
	enterPassword->setGeometry( 10, 190, 140, 24 );
	//enc-dec pass
	QString pass = settingsComPort.value( "mail/password" ).toString();
	QString pass_enc;
	if ( ! pass.startsWith("#") )
		pass_enc = Crypt::crypt( pass );
	else
	{
		pass_enc = pass.remove( 0, 1 );
		pass = Crypt::decrypt( pass_enc );
	}
	// 
	enterPassword->setText( pass_enc.prepend("#") );
	enterPassword->setEchoMode( QLineEdit::Password );

	labelSelectInterval = new QLabel( tr("Send interval, min"), eMailSettings );
	labelSelectInterval->setGeometry( 56 + 97, 172, 200, 18 );

	selectIntervalSend = new QComboBox( eMailSettings );
	selectIntervalSend->setGeometry( 10 + 143, 190, 143, 24 );

	// ��������� ������� ������� 
	QStringList strList;
	QString buf[9] = {"5", "10", "30", "60", "120", "240", "360", "720", "1440"};
	for(int i = 0; i < 9; i++)
		strList.append( buf[i] );

	selectIntervalSend->addItems( strList );

	selectIntervalSend->setCurrentText( settingsComPort.value( "mail/interval_send_report" ).toString() );

	checkBoxSendEachRec = new QCheckBox( tr("Send each record"), eMailSettings );
	checkBoxSendEachRec->setGeometry( 10 + 143 * 2 + 10, 190, 155, 24 );
	( settingsComPort.value( "mail/enable_send_eachrec" ).toString() == "1" ? checkBoxSendEachRec->setChecked( true ) : checkBoxSendEachRec->setChecked( false ) );

    labelMailTo = new QLabel( tr("Addressee"), eMailSettings );
	labelMailTo->setGeometry( 10, 224, 120, 18 );

	enterMailTo = new QLineEdit( eMailSettings );
	enterMailTo->setGeometry( 10, 242, 284, 24 );
	enterMailTo->setText( settingsComPort.value( "mail/mailToSend" ).toString() );

	checkBox_ssl = new QCheckBox( tr("Use SSL"), eMailSettings );
	checkBox_ssl->setGeometry( 10, 270, 140, 24 );
	( settingsComPort.value( "mail/ssl" ).toString() == "1" ? checkBox_ssl->setChecked( true ) : checkBox_ssl->setChecked( false ) );

	checkBoxEnableMailSend = new QCheckBox( tr("Mail ON"), eMailSettings );
	checkBoxEnableMailSend->setGeometry( 10 + 153, 270, 140, 24 );
	( settingsComPort.value( "mail/enable_send_report" ).toString() == "1" ? checkBoxEnableMailSend->setChecked( true ) : checkBoxEnableMailSend->setChecked( false ) );

	checkBoxPhotoSaveAll = new QCheckBox( tr("Save all photos"), eMailSettings );
	checkBoxPhotoSaveAll->setGeometry( 10 + 153, 270 + 24, 140, 24 );
	//connect( checkBoxPhotoSaveAll, SIGNAL( stateChanged( int ) ), this, SLOT( checkBoxPhotoSaveAll_Slot( int ) ) );
	//checkBoxPhotoSaveAll->hide();

	settingsComPort.value( "mail/enable_send_photos" ).toString() == "1" ? checkBoxPhotoSaveAll->setChecked( true ) : checkBoxPhotoSaveAll->setChecked( false );
	

	


	buttonSaveEmailSettings = new QPushButton( tr("Save"), eMailSettings );
	buttonSaveEmailSettings->setGeometry( 10, 290 , 140, 32 );
	QObject::connect( buttonSaveEmailSettings, SIGNAL( clicked() ), this, SLOT( buttonSaveEmailSettings_Slot() ) );


	// ��������� ������� ����/������� ��� Excel 
	QGroupBox *groupExcel = new QGroupBox( eMailSettings );
	groupExcel->setGeometry( 180 + 133, 5, 205, 48 );
	groupExcel->setTitle( tr("Excel datetime") );
	groupExcel->setStyleSheet( "font-size: 18; font-weight: none; border-width: 2px; " );

	enterExcelDateTimeFormat = new QLineEdit( groupExcel );
	enterExcelDateTimeFormat->setGeometry( 10, 20, 185, 24 );

	enterExcelDateTimeFormat->setText( settingsComPort.value( "excel/date_time_format", "" ).toString() );

	//�������������� ������
	QGroupBox *groupAdress = new QGroupBox( eMailSettings );
	groupAdress->setGeometry( 180 + 133, 224, 225, 125 );
	groupAdress->setTitle( tr("Additional addresses") );
	groupAdress->setStyleSheet( "font-size: 18; font-weight: none; border-width: 2px; " );

	checkBox_Adr_1 = new QCheckBox( tr(""), groupAdress );
	checkBox_Adr_1->setGeometry( 5, 25, 15, 15 );
	( settingsComPort.value( "mail/check_Adr_1" ).toString() == "1" ? checkBox_Adr_1->setChecked( true ) : checkBox_Adr_1->setChecked( false ) );
	enterAdress_1 = new QLineEdit( groupAdress );
	enterAdress_1->setGeometry( 25, 20, 190, 24 );
	enterAdress_1->setText( settingsComPort.value( "mail/enterAdress_1" ).toString() );

	checkBox_Adr_2 = new QCheckBox( tr(""), groupAdress );
	checkBox_Adr_2->setGeometry( 5, 55, 15, 15 );
	( settingsComPort.value( "mail/check_Adr_2" ).toString() == "1" ? checkBox_Adr_2->setChecked( true ) : checkBox_Adr_2->setChecked( false ) );
	enterAdress_2 = new QLineEdit( groupAdress );
	enterAdress_2->setGeometry( 25, 55, 190, 24 );
	enterAdress_2->setText( settingsComPort.value( "mail/enterAdress_2" ).toString() );

	checkBox_Adr_3 = new QCheckBox( tr(""), groupAdress );
	checkBox_Adr_3->setGeometry( 5, 90, 15, 15 );
	( settingsComPort.value( "mail/check_Adr_3" ).toString() == "1" ? checkBox_Adr_3->setChecked( true ) : checkBox_Adr_3->setChecked( false ) );
	enterAdress_3 = new QLineEdit( groupAdress );
	enterAdress_3->setGeometry( 25, 90, 190, 24 );
	enterAdress_3->setText( settingsComPort.value( "mail/enterAdress_3" ).toString() );



	// Optional devices
	devicesSettings = new QWidget( dialogSettings );
	devicesSettings->setFixedSize( settWindowSize );
	settingsTabs->addTab( devicesSettings, tr("Optional devices") );
	
	// socket 1
	labelVk1 = new QLabel( tr("Socket2") + " #1", devicesSettings );
	labelVk1->setGeometry( 10, 24, 100, 20 );

	enterVk1 = new QLineEdit( devicesSettings );
	enterVk1->setGeometry( 112, 20, 150, 24 );
	enterVk1->setText( settingsComPort.value( "Devices/socket1_ip" ).toString() );

	chkboxEnableVk1 = new QCheckBox( devicesSettings ); 
	chkboxEnableVk1->setGeometry( 265, 20, 200, comboBoxSize.height() );
	chkboxEnableVk1->setText( tr("Enable") ); 
	strData = settingsComPort.value( "Devices/socket1_en" ).toString();
	( strData == "1" ) ? chkboxEnableVk1->setChecked( true ) : chkboxEnableVk1->setChecked( false );
	
	// socket 2
	labelVk2 = new QLabel( tr("Socket2") + " #2", devicesSettings );
	labelVk2->setGeometry( 10, 54, 100, 20 );

	enterVk2 = new QLineEdit( devicesSettings );
	enterVk2->setGeometry( 112, 50, 150, 24 );
	enterVk2->setText( settingsComPort.value( "Devices/socket2_ip" ).toString() );

	chkboxEnableVk2 = new QCheckBox( devicesSettings ); 
	chkboxEnableVk2->setGeometry( 265, 50, 200, comboBoxSize.height() );
	chkboxEnableVk2->setText( tr("Enable") ); 
	strData = settingsComPort.value( "Devices/socket2_en" ).toString();
	( strData == "1" ) ? chkboxEnableVk2->setChecked( true ) : chkboxEnableVk2->setChecked( false );

	// socket 3
	labelVk3 = new QLabel( QString( tr("Socket2") + " #3" ), devicesSettings );
	labelVk3->setGeometry( 10, 84, 100, 20 );

	enterVk3 = new QLineEdit( devicesSettings );
	enterVk3->setGeometry( 112, 80, 150, 24 );
	enterVk3->setText( settingsComPort.value( "Devices/socket3_ip" ).toString() );

	chkboxEnableVk3 = new QCheckBox( devicesSettings ); 
	chkboxEnableVk3->setGeometry( 265, 80, 200, comboBoxSize.height() );
	chkboxEnableVk3->setText( tr("Enable") ); 
	strData = settingsComPort.value( "Devices/socket3_en" ).toString();
	( strData == "1" ) ? chkboxEnableVk3->setChecked( true ) : chkboxEnableVk3->setChecked( false );

	//Rfid 1
	group_Rfid = new QGroupBox( devicesSettings );
	group_Rfid->setGeometry( 368, 20, 200, 85 );
	group_Rfid->setTitle( tr("Rfid on Socket2") ); 
	group_Rfid->setCheckable( true );
	group_Rfid->setChecked( settingsComPort.value( "rfid/enable" ).toBool() );

	labelRfid1 = new QLabel(tr("Rfid") + " #1", group_Rfid);
	labelRfid1->setGeometry(5, 10, 100, 20);

	enterRfid1 = new QLineEdit(group_Rfid);
	enterRfid1->setGeometry(5, 30, 150, 24);
	enterRfid1->setText(settingsComPort.value("rfid/IP2").toString());

	chkboxEnableRfid1 = new QCheckBox(group_Rfid);
	chkboxEnableRfid1->setGeometry(5, 55, 200, comboBoxSize.height());
	chkboxEnableRfid1->setText(tr("For registration"));
	strData = settingsComPort.value("rfid/registration").toString();
	(strData == "1") ? chkboxEnableRfid1->setChecked(true) : chkboxEnableRfid1->setChecked(false);
	//
	//Lamps
	QLabel *label_Lamps = new QLabel( tr("Lamps"), group_Rfid );
	label_Lamps->setGeometry( 5, 10, 30, 18 );
	lineEdit_Lamps = new QLineEdit( group_Rfid );
	lineEdit_Lamps->setGeometry( 5, 20, 110, 18 );
	lineEdit_Lamps->setText( settingsComPort.value( "rfid/lamps_ip" ).toString() );
	label_Lamps->hide();
	lineEdit_Lamps->hide();
	
	if( !optionsObj->isCheked[optionsObj->modules.indexOf("rfid")] )
	{
		group_Rfid->setChecked( false );
		group_Rfid->setEnabled( false );
	}
	
	//---



	//
	QLabel *labelVk1name1 = new QLabel( QString( tr("Socket2") + " �1 - 0" ), devicesSettings );
	labelVk1name1->setGeometry( 10, 124, 120, 20 );
	enterVk1Name1 = new QLineEdit( devicesSettings );
	enterVk1Name1->setGeometry( 112+20, 120, 120, 24 );
	enterVk1Name1->setText( settingsComPort.value( "Devices/socket1_name1" ).toString() );

	QLabel *labelVk1name2 = new QLabel( QString( tr("Socket2") + " �1 - 1" ), devicesSettings );
	labelVk1name2->setGeometry( 280, 124, 120, 20 );
	enterVk1Name2 = new QLineEdit( devicesSettings );
	enterVk1Name2->setGeometry( 390+20, 120, 120, 24 );
	enterVk1Name2->setText( settingsComPort.value( "Devices/socket1_name2" ).toString() );

	QLabel *labelVk2name1 = new QLabel( QString( tr("Socket2") + " �2 - 0" ), devicesSettings );
	labelVk2name1->setGeometry( 10, 154, 120, 20 );
	enterVk2Name1 = new QLineEdit( devicesSettings );
	enterVk2Name1->setGeometry( 112+20, 150, 120, 24 );
	enterVk2Name1->setText( settingsComPort.value( "Devices/socket2_name1" ).toString() );

	QLabel *labelVk2name2 = new QLabel( QString( tr("Socket2") + " �2 - 1" ), devicesSettings );
	labelVk2name2->setGeometry( 280, 154, 120, 20 );
	enterVk2Name2 = new QLineEdit( devicesSettings );
	enterVk2Name2->setGeometry( 390+20, 150, 120, 24 );
	enterVk2Name2->setText( settingsComPort.value( "Devices/socket2_name2" ).toString() );

	QLabel *labelVk3name1 = new QLabel( QString( tr("Socket2") + " �3 - 0" ), devicesSettings );
	labelVk3name1->setGeometry( 10, 184, 120, 20 );
	enterVk3Name1 = new QLineEdit( devicesSettings );
	enterVk3Name1->setGeometry( 112+20, 180, 120, 24 );
	enterVk3Name1->setText( settingsComPort.value( "Devices/socket3_name1" ).toString() );

	QLabel *labelVk3name2 = new QLabel( QString( tr("Socket2") + " �3 - 1" ), devicesSettings );
	labelVk3name2->setGeometry( 280, 184, 120, 20 );
	enterVk3Name2 = new QLineEdit( devicesSettings );
	enterVk3Name2->setGeometry( 390+20, 180, 120, 24 );
	enterVk3Name2->setText( settingsComPort.value( "Devices/socket3_name2" ).toString() );
	
	buttonSaveDevicesSettings = new QPushButton( tr("Save"), devicesSettings );
	buttonSaveDevicesSettings->setGeometry( 10, 320, 140, 32 );
	connect( buttonSaveDevicesSettings, SIGNAL( clicked() ), this, SLOT( buttonSaveDevicesSettings_Slot() ) );

	groupTraffLight1 = new QGroupBox( devicesSettings );
	groupTraffLight1->setGeometry( 10, 210, 210, 100 );
	groupTraffLight1->setTitle( tr("TCM 2") ); 
	groupTraffLight1->setCheckable( true );
	strData = settingsComPort.value( "Devices/traff1", "" ).toString();
	groupTraffLight1->setChecked( strData == "1" ? true : false );
	
	radioTablo_COM1 = new QRadioButton( "COM", groupTraffLight1 );
	radioTablo_COM1->setGeometry( 10, 27, 50, 18 );
	radioTablo_COM1->setChecked( settingsComPort.value( "Devices/traff1_isCOM", true ).toBool() );
	
	selectComPortTablo1 = new QComboBox( groupTraffLight1 );
	selectComPortTablo1->setGeometry( 60, 26, 80, 24 );
	strData = settingsComPort.value( "Devices/traff1_port", "" ).toString();
	selectComPortTablo1->setCurrentIndex( ( serPorts.indexOf( strData ) != -1 ? serPorts.indexOf( strData ) : 0  ) );
	selectComPortTablo1->setObjectName( "selectComPortTablo1" );
	selectComPortTablo1->installEventFilter( this );

	radioTablo_IP1 = new QRadioButton( "IP", groupTraffLight1 );
	radioTablo_IP1->setGeometry( 10, 62, 40, 18 );
	radioTablo_IP1->setChecked( settingsComPort.value( "Devices/traff1_isIP", false ).toBool() );

	enterTablo_IP1 = new QLineEdit( groupTraffLight1 );
	enterTablo_IP1->setGeometry( 45, 60, 150, 24 );
	enterTablo_IP1->setText( settingsComPort.value( "Devices/traff1_IP" ).toString() );

	groupTraffLight2 = new QGroupBox( devicesSettings );
	groupTraffLight2->setGeometry( 230, 210, 210, 100 );
	groupTraffLight2->setTitle( tr("TCM 2") ); 
	groupTraffLight2->setCheckable( true );
	strData = settingsComPort.value( "Devices/traff2", "" ).toString();
	groupTraffLight2->setChecked( strData == "1" ? true : false );
	//groupTraffLight->hide();
	
	radioTablo_COM2 = new QRadioButton( "COM", groupTraffLight2 );
	radioTablo_COM2->setGeometry( 10, 27, 50, 18 );
	radioTablo_COM2->setChecked( settingsComPort.value( "Devices/traff2_isCOM", true ).toBool() );
	
	selectComPortTablo2 = new QComboBox( groupTraffLight2 );
	selectComPortTablo2->setGeometry( 60, 26, 80, 24 );
	strData = settingsComPort.value( "Devices/traff2_port", "" ).toString();
	selectComPortTablo2->setCurrentIndex( ( serPorts.indexOf( strData ) != -1 ? serPorts.indexOf( strData ) : 0  ) );
	selectComPortTablo2->setObjectName( "selectComPortTablo1" );
	selectComPortTablo2->installEventFilter( this );

	radioTablo_IP2 = new QRadioButton( "IP", groupTraffLight2 );
	radioTablo_IP2->setGeometry( 10, 62, 40, 18 );
	radioTablo_IP2->setChecked( settingsComPort.value( "Devices/traff2_isIP", false ).toBool() );

	enterTablo_IP2 = new QLineEdit( groupTraffLight2 );
	enterTablo_IP2->setGeometry( 45, 60, 150, 24 );
	enterTablo_IP2->setText( settingsComPort.value( "Devices/traff2_IP" ).toString() );



	QLabel *labelMinWeightTablo = new QLabel( tr( "Minimum weight, kg" ), groupTraffLight1 );
	labelMinWeightTablo->setGeometry( 210, 14, 130, 18 );

	enterMinWeightTablo = new QLineEdit( groupTraffLight1 );
	enterMinWeightTablo->setGeometry( 350, 10, 120, 24 );
	enterMinWeightTablo->setText( settingsComPort.value( "Devices/traff_min" ).toString() );

	QLabel *labelTimerTablo = new QLabel( tr( "Time switch, sec" ), groupTraffLight1 );
	labelTimerTablo->setGeometry( 210, 44, 130, 18 );

	enterTimerTablo = new QLineEdit( groupTraffLight1 );
	enterTimerTablo->setGeometry( 350, 40, 120, 24 );
	enterTimerTablo->setText( settingsComPort.value( "Devices/traff_time" ).toString() );

	labelMinWeightTablo->hide();
	enterMinWeightTablo->hide();
	labelTimerTablo->hide();
	enterTimerTablo->hide();

	if( !optionsObj->isCheked[optionsObj->modules.indexOf("tablo")] )
	{
		groupTraffLight1->setChecked( false );
		groupTraffLight1->setEnabled( false );
		groupTraffLight2->setChecked( false );
		groupTraffLight2->setEnabled( false );
	}



	
	QWidget *mysqlSettings = new QWidget( dialogSettings );
	mysqlSettings->setFixedSize( settWindowSize );
	settingsTabs->addTab( mysqlSettings, tr( "MySQL" ) );

	QLabel *label_address = new QLabel( "Address", mysqlSettings );
	label_address->setGeometry( 10, 20, 80, 18 );
	enterMySQL_address = new QLineEdit( mysqlSettings );
	enterMySQL_address->setGeometry( 100, 20, 150, 24 );
	enterMySQL_address->setText( settingsComPort.value( "MySQL/address" ).toString() );

	QLabel *label_port = new QLabel( "Port", mysqlSettings );
	label_port->setGeometry( 10, 50, 80, 18 );
	enterMySQL_port = new QLineEdit( mysqlSettings );
	enterMySQL_port->setGeometry( 100, 50, 150, 24 );
	enterMySQL_port->setText( settingsComPort.value( "MySQL/port" ).toString() );

	QLabel *label_login = new QLabel( "Login", mysqlSettings );
	label_login->setGeometry( 10, 80, 80, 18 );
	enterMySQL_login = new QLineEdit( mysqlSettings );
	enterMySQL_login->setGeometry( 100, 80, 150, 24 );
	enterMySQL_login->setText( settingsComPort.value( "MySQL/login" ).toString() );

	QLabel *label_password = new QLabel( "Password", mysqlSettings );
	label_password->setGeometry( 10, 110, 80, 18 );
	enterMySQL_password = new QLineEdit( mysqlSettings );
	enterMySQL_password->setGeometry( 100, 110, 150, 24 );
	//enc-dec pass
	QString pass_mysql = settingsComPort.value( "MySQL/password" ).toString();
	QString pass_mysql_enc;
	if ( ! pass_mysql.startsWith("#") )
		pass_mysql_enc = Crypt::crypt( pass_mysql );
	else
	{
		pass_mysql_enc = pass_mysql.remove( 0, 1 );
		pass_mysql = Crypt::decrypt( pass_mysql_enc );
	}
	enterMySQL_password->setText( settingsComPort.value( pass_mysql_enc.prepend("#") ).toString() );
	enterMySQL_password->setEchoMode( QLineEdit::Password );
	//
	QPushButton	*buttonSaveMySQL = new QPushButton( tr("Save"), mysqlSettings );
	buttonSaveMySQL->setGeometry( 10, 320, 140, 32 );
	connect( buttonSaveMySQL, SIGNAL( clicked() ), this, SLOT( buttonSaveMySQL_Slot() ) );

	//
	QWidget *imageSettings = new QWidget( dialogSettings );
	imageSettings->setFixedSize( settWindowSize );

	// ������
	//imageSettings->hide();
	settingsTabs->addTab( imageSettings, tr("Photo\n stamp") );

	checkBoxCoord1 = new QCheckBox( tr("Top-left"), imageSettings );
	checkBoxCoord1->setGeometry( 10, 10, 240, 20 );
	strData = settingsComPort.value( "image/left_top" ).toString();
	( strData == "1" ) ? checkBoxCoord1->setChecked( true ) : checkBoxCoord1->setChecked( false );
	connect( checkBoxCoord1, SIGNAL( stateChanged( int ) ), this, SLOT( checkBoxCoord1_Slot( int ) ) );

	checkBoxCoord2 = new QCheckBox( tr("Bottom-left"), imageSettings );
	checkBoxCoord2->setGeometry( 10, 34, 240, 20 );
	strData = settingsComPort.value( "image/left_bottom" ).toString();
	( strData == "1" ) ? checkBoxCoord2->setChecked( true ) : checkBoxCoord2->setChecked( false );
	connect( checkBoxCoord2, SIGNAL( stateChanged( int ) ), this, SLOT( checkBoxCoord2_Slot( int ) ) );

	checkBoxCoord3 = new QCheckBox( tr("Top-right"), imageSettings );
	checkBoxCoord3->setGeometry( 10, 58, 240, 20 );
	strData = settingsComPort.value( "image/right_top" ).toString();
	( strData == "1" ) ? checkBoxCoord3->setChecked( true ) : checkBoxCoord3->setChecked( false );
	connect( checkBoxCoord3, SIGNAL( stateChanged( int ) ), this, SLOT( checkBoxCoord3_Slot( int ) ) );

	checkBoxCoord4 = new QCheckBox( tr("Bottom-right"), imageSettings );
	checkBoxCoord4->setGeometry( 10, 82, 240, 20 );
	strData = settingsComPort.value( "image/right_bottom" ).toString();
	( strData == "1" ) ? checkBoxCoord4->setChecked( true ) : checkBoxCoord4->setChecked( false );
	connect( checkBoxCoord4, SIGNAL( stateChanged( int ) ), this, SLOT( checkBoxCoord4_Slot( int ) ) );

	serialPortTablo1 = new QSerialPort();

	// ��������� COM_���� 
	QList<QString> serPortsList;
	//
	update_SerialPortsList( sp, selectComPortTablo1, selectComPortTablo1->currentText(), &serPortsList );
	if( selectComPortTablo1->currentText() != "" )
	{
		strData = settingsComPort.value( "Devices/traff1_port" ).toString();
	 	serialPortTablo1->close();

		int baud = settingsComPort.value( "Devices/traff1_baud", 560 ).toInt();

		if( groupTraffLight1->isChecked() && radioTablo_COM1->isChecked() )
			sp->OpenSerialPort( serialPortTablo1, strData, baud ); //2400 omgg
		
		selectComPortTablo1->setCurrentText( strData ); 
	}

	serialPortTablo2 = new QSerialPort();

	update_SerialPortsList( sp, selectComPortTablo2, selectComPortTablo2->currentText(), &serPortsList );
	if( selectComPortTablo2->currentText() != "" )
	{
		strData = settingsComPort.value( "Devices/traff2_port" ).toString();
	 	serialPortTablo2->close();

		int baud = settingsComPort.value( "Devices/traff2_baud", 560 ).toInt();

		if( groupTraffLight2->isChecked() && radioTablo_COM2->isChecked() )
			sp->OpenSerialPort( serialPortTablo2, strData, baud ); //2400 omgg
		selectComPortTablo2->setCurrentText( strData ); 
	}
}
// ��������� ���������� ��������  ���  IP-����� ������������� 
void SettingsForm::Load_IP_CAMs_Settings()
{
	QSettings settingsIP_Cams( "settings.ini", QSettings::IniFormat );
	settingsIP_Cams.setIniCodec( "Windows-1251" );

	char n[ 16 ];
	vector_ip_cams_settings.clear();

	for( int i = 0; i < MAX_NUM_IP_CAMS; i++ )
	{
        ip_cam_settings_for_compare = new currentSettingsIP_Cam(); // ������ ��������� �������� ������ ���������� ��� IP-�����

		memset( n, 0x00, sizeof( n ) );
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		sprintf( n, "Cam%d", i + 1 );
		settingsIP_Cams.beginGroup( QString( n ) ); // ����� ������ ���������� �������� IP-������ 
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		// ��������� ��� ��������� � Vector �������� ����������� �������� IP-����� 
		ip_cam_settings_for_compare->cam_en = ( settingsIP_Cams.value( "cam_en" ) == 1 ) ? true : false;
		ip_cam_settings_for_compare->cam_photofix_en = ( settingsIP_Cams.value( "cam_photofix_en" ) == 1 ) ? true : false;
		ip_cam_settings_for_compare->cam_photofix_preview_en = ( settingsIP_Cams.value( "cam_photofix_preview_en" ) == 1 ) ? true : false;
		ip_cam_settings_for_compare->ip_address = settingsIP_Cams.value( "ip_address" ).toString();
		ip_cam_settings_for_compare->port = settingsIP_Cams.value( "port" ).toString();
		ip_cam_settings_for_compare->login = settingsIP_Cams.value( "login" ).toString();
		//enc-dec pass
		QString pass = settingsIP_Cams.value("password" ).toString();
		QString pass_enc;
		if ( ! pass.startsWith("#") )
			pass_enc = Crypt::crypt( pass );
		else
		{
			pass_enc = pass.remove( 0, 1 );
			pass = Crypt::decrypt( pass_enc );
		}
		//
		ip_cam_settings_for_compare->password = pass;

		vector_ip_cams_settings.append( ip_cam_settings_for_compare );

		settingsIP_Cams.endGroup();
	}
}
// ��������� ������������  ��������� �������� ������������� 
bool SettingsForm::CompareSettings_IP_CAMs( QVector< QString > *log_messages_vec )
{
	bool result = false;
	char n[ 16 ];

    QSettings settingsIP_Cams( "settings.ini", QSettings::IniFormat );
	settingsIP_Cams.setIniCodec( "Windows-1251" );

	for( int i = 0; i < MAX_NUM_IP_CAMS; i++ )
	{
		memset( n, 0x00, sizeof( n ) );
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		sprintf( n, "Cam%d", i + 1 );
		settingsIP_Cams.beginGroup( QString( n ) ); // ����� ������ ���������� �������� IP-������ 
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		//enc-dec pass
		QString pass = settingsIP_Cams.value("password" ).toString();
		QString pass_enc;
		if ( ! pass.startsWith("#") )
			pass_enc = Crypt::crypt( pass );
		else
		{
			pass_enc = pass.remove( 0, 1 );
			pass = Crypt::decrypt( pass_enc );
		}
		//
		// ��������� ��� ��������� � Vector �������� ����������� �������� IP-����� 
		if( vector_ip_cams_settings.at( i )->cam_en != ( settingsIP_Cams.value( "cam_en" ) == 1 ) ? true : false 
			|| vector_ip_cams_settings.at( i )->cam_photofix_en != ( settingsIP_Cams.value( "cam_photofix_en" ) == 1 ) ? true : false
			|| vector_ip_cams_settings.at( i )->cam_photofix_preview_en != ( settingsIP_Cams.value( "cam_photofix_preview_en" ) == 1 ) ? true : false
			|| vector_ip_cams_settings.at( i )->ip_address != settingsIP_Cams.value( "ip_address" ).toString()
			|| vector_ip_cams_settings.at( i )->port != settingsIP_Cams.value( "port" ).toString()
			|| vector_ip_cams_settings.at( i )->login != settingsIP_Cams.value( "login" ).toString()
			|| vector_ip_cams_settings.at( i )->password != pass )
		{
			result = true;

			if( vector_ip_cams_settings.at( i )->cam_en != ( settingsIP_Cams.value( "cam_en" ) == 1 ) ? true : false )
			{
				//log_messages_vec->append( QString( "��������� ���������� IP-������ � %1:" ).arg( i + 1 ).append( QString( "\"���������� ������ IP-������\" ���� %1 ���� %2" ).arg( vector_ip_cams_settings.at( i )->cam_en ).arg( settingsIP_Cams.value( "cam_en" ).toString() ).append( "\r\n" ) ) );
			}
			if( vector_ip_cams_settings.at( i )->cam_photofix_en != ( settingsIP_Cams.value( "cam_photofix_en" ) == 1 ) ? true : false )
			{
				//log_messages_vec->append( QString( "��������� ���������� IP-������ � %1:" ).arg( i + 1 ).append( QString( "\"���� ���������� ������������ ��� %1 ���� %2" ).arg( vector_ip_cams_settings.at( i )->cam_photofix_en ).arg( settingsIP_Cams.value( "cam_photofix_en" ).toString() ).append( "\r\n" ) ) );
			}
			if( vector_ip_cams_settings.at( i )->cam_photofix_preview_en != ( settingsIP_Cams.value( "cam_photofix_preview_en" ) == 1 ) ? true : false )
			{
				//log_messages_vec->append( QString( "��������� ���������� IP-������ � %1:" ).arg( i + 1 ).append( QString( "\"���� ���������� ����������� ����������� ��� ������������ ��� %1 ���� %2" ).arg( vector_ip_cams_settings.at( i )->cam_photofix_en ).arg( settingsIP_Cams.value( "cam_photofix_en" ).toString() ).append( "\r\n" ) ) );
			}
			if( vector_ip_cams_settings.at( i )->ip_address != ( settingsIP_Cams.value( "ip_address" ).toString() ) )
			{
				//log_messages_vec->append( QString( "��������� ���������� IP-������ � %1:" ).arg( i + 1 ).append( QString( "\"IP-�����\" ��� %1 ���� %2" ).arg( vector_ip_cams_settings.at( i )->ip_address ).arg( settingsIP_Cams.value( "ip_address" ).toString() ).append( "\r\n" ) ) );
			}
			if( vector_ip_cams_settings.at( i )->port != ( settingsIP_Cams.value( "port" ).toString() ) )
			{
				//log_messages_vec->append( QString( "��������� ���������� IP-������ � %1:" ).arg( i + 1 ).append(  QString( "\"����\" ��� %1 ���� %2" ).arg( vector_ip_cams_settings.at( i )->port ).arg( settingsIP_Cams.value( "port" ).toString() ).append( "\r\n" ) ) );
			}
			if( vector_ip_cams_settings.at( i )->login != ( settingsIP_Cams.value( "login" ).toString() ) )
			{
				//log_messages_vec->append( QString( "��������� ���������� IP-������ � %1:" ).arg( i + 1 ).append( QString( "\"�����\" ��� %1 ���� %2" ).arg( vector_ip_cams_settings.at( i )->login ).arg( settingsIP_Cams.value( "login" ).toString() ).append( "\r\n" ) ) );
			}
			if( vector_ip_cams_settings.at( i )->password != ( pass ) )
			{
				//log_messages_vec->append( QString( "��������� ���������� IP-������ � %1:" ).arg( i + 1 ).append( QString( "��������� ������" ).append( "\r\n" ) ) );
			}
		}

		settingsIP_Cams.endGroup();
	}

	return result;
}

// ������� ���������� ������������� ����� 
int SettingsForm::getNumVP()
{
	return MAX_NUM_SCALES;
}

// ������� ���������� IP-����� � ������� 
int SettingsForm::getNumIP_Cams()
{
	return MAX_NUM_IP_CAMS;
}

// ���������� ����������  ����  �������� 
void SettingsForm::showSettingsDialog()
{
	dialogSettings->show();
}
// ������� ����������  ����  �������� 
void SettingsForm::hideSettingsDialog()
{
	// dialogSettings->hide();
}
// ��������� ���� �������� ��
QList<QString> SettingsForm::loadTypesOfVP()
{
	QList<QString> *typesVP = new QList<QString>(); // ������ ����� ���������������
	typesVP->clear();
	QString buf[] = {
		"��-05�\\10\\11(�1)",
		"Esit ART", 
		"����(CAS ��� I3)",
		"Keli A12(���.P5 �4)",
		"����(�������� �1)",
		"���� ����������(��-01)",
		"��-01 (��������)",
		"WE2108",
		"XK3118T1",
		"ESIT PWI",
		"�������",
		"RiceLake",
		"Axis SE01",
		"ESIT ART(Mod1)",
		"T7E",
		"SMART ABS (F9 Mode)",
		"SMART ABS (��� �������)",
		"��-05",
		"ESIT PWI(� ������)",
		"WE2107(��� �������)",
		"WE2110(��� �������)",
		"Keli D12 (f1)",
		"��������� (L)",
		"���� ��-1",
		"Zemic A9 (��� �������, tf0)",
		"Keli D39 (���2)",
		"XK3118T1 (� �������� )"
	};
	
	for(int i = 0; i < (sizeof(buf)/sizeof(buf[0])); i++)
		typesVP->append(buf[i]);
	
	return *typesVP;
}
// ��������� ���� �������� �� ��� ������ � �� 
QList<QString> SettingsForm::loadTypesOfVP_BK()
{
	QList<QString> *typesVP = new QList<QString>(); // ������ ����� ���������������
	typesVP->clear();
	QString buf[] = {"��-01/ESIT ART", "��-05�(���.tf1)", "WE2108", "DB", "XK3118T1", "A12", "ESIT PWI", "�������", "RiceLake"};
	for(int i = 0; i < (sizeof(buf)/sizeof(buf[0])); i++)
		typesVP->append(buf[i]);
	
	return *typesVP;
}
// ��������� ���� ��������  ��  ��-01  �  ��-xx
QList<int> SettingsForm::load_BK_VP_REquests()
{
	QList<int> *typesVP = new QList<int>(); // ������ ����� ���������������
	typesVP->clear();	
	for(int i = 0; i < 8; i++)
		typesVP->append(i);
	return *typesVP;
}

// ��������� �������� ���� (��)
QList<QString> SettingsForm::loadDiscretesVP()
{
	QList<QString> *result = new QList<QString>(); // ������ ����� ���������������
	result->clear();
	int buf[] = {1, 2, 5, 10, 20, 50, 100, 200};
	for(int i = 0; i < (sizeof(buf)/sizeof(buf[0])); i++)
		result->append(QString::number(buf[i]));
	
	return *result;
}

// ��������� ���� �������� ���� ��� �� 
QList<QString> SettingsForm::loadVP_REquests()
{
	QList<QString> *result = new QList<QString>();

	result->clear();
	
	result->append( "\2AB03\3" ); // ��-05
	result->append( QByteArray().append( 0xff ).append( vectorAddrVP.at( 0 )->text() ).append( '\0' ) ); // ��-01 ����� ����� � 1
	QString buf[] = {
		"",
		"R",
		"\01",
		"G",
		"\2AB03\3", //"\2AC0\3",
		"MSV?", 
		"",
		"\xff""\x01",
		"W",
		"P",
		"SI",
		"",
		"",
		"P",
		"",
		"GVES",
		"\xff""\x01",
		"",
		""
	};

	QByteArray d12;
	d12.append( 0x01 );
	d12.append( 0x03 );
	d12.append( (char)0x00 );
	d12.append( 0x03 );
	d12.append( (char)0x00 );
	d12.append( 0x04 );
	d12.append( 0xB4 );
	d12.append( 0x09 );
	//01 03 00 03 00 04 b4 09
	
	for(int i = 0; i < (sizeof(buf)/sizeof(buf[0])); i++)
	{
		result->append(buf[i]);
	}
	//21: D12
	result->append( QString( d12 ) );
	
	//���������� � 22
	//22: // ���������
	QString addrVP = QString::number(vectorAddrVP.at( currentScales)->value());
	if ( addrVP.size() == 1 ) 
		addrVP.prepend("0");
	QByteArray ba = "G" + addrVP + "W";
	// G01W0d0a
	result->append( ba );
	
	//23: ����
	
	result->append( "VES\x0d" );
	//24: Zemic A9
	result->append( "" );
	//25:  Keli D39 (���2)
	result->append( "" );
	//26: T1 � ��������
	result->append( "" );

	return *result;
}
// ������� � ComboBox ��������� �������  ������ �� COM-����� 
QList<QString> SettingsForm::getEndsOfFrames()
{
	QList<QString> *result = new QList<QString>();
		
	result->clear();
	QString buf[] = {"NONE", "CR", "LF", "CRLF"};
	for(int i = 0; i < (sizeof(buf)/sizeof(buf[0])); i++)
		result->append(buf[i]);
   
	return *result;
}

// ������� � ComboBox ��������� �������  ������ �� COM-����� 
QList<QByteArray> SettingsForm::getEndsOfFramesData()
{
	QList<QByteArray> result;

	result = QList<QByteArray>();
	result.clear();
	QByteArray buf[] = {"", "\r", "\n", "\r\n"};
	for(int i = 0; i < (sizeof(buf)/sizeof(buf[0])); i++)
		result.append(buf[i]);
    
	return result;
}
// ��������� �������� � ���������� ������ ����� 
QList<QString> SettingsForm::load_list_Scales()
{
    QList<QString> result;

	result.clear();
	for( int i = 0; i < MAX_NUM_SCALES; i++ )
	{
		result.append( tr("Scale") + QString( "%1" ).arg( i + 1 ) );
	}

	return result;
}
// ���������� ������� ������ SettingsForm
bool SettingsForm::eventFilter( QObject *object, QEvent *e )
{
	if( e->type() == Qt::RightButton )
	{
		if( object->objectName() == "selectselectComPortTablo1" )
		{
			QComboBox *comboBox = qobject_cast< QComboBox* >( object );
		    if( comboBox != 0x00 )
		    {
			    // �������� ������ COM-������ 
			    comboBox->clear(); // �������� ������ ������ 
			    serialPortClass *sp = new serialPortClass();

                comboBox->addItem( "" ); // �������� ������ ������ � ������ ������
			    comboBox->addItems( sp->QuerrySerialPorts() ); // ����������� COM-������ � ������� 

				delete sp;
				return false;
		    }
	    }		
	}

		// ���������� ������� �� ComboBox � COM-������� 
	    if( e->type() == QEvent::MouseButtonPress && Qt::LeftButton )	
		{
			QPushButton *button = qobject_cast< QPushButton* >( object );
			if( button != 0x00 )
			{
				if( button->objectName() == "buttonForSaveSettings"  )
			    {
					int i;
					char n[ 16 ];
					QString dataIni;

					memset( n, 0x00, sizeof( n ) );
					// ??? ��������� ��������� � *.ini ���� 
                    // ��������� � ini - ����  ���  ���������� �������� ������ � ������
                    QSettings settingsComPort( "settings.ini", QSettings::IniFormat );
					settingsComPort.setIniCodec("Windows-1251");

					// ���������� ������ ���� ���������� � ini - ���� 
					for( i = 0; i < this->getNumVP(); i++ )
					{
                        sprintf( n, "scales%d", i + 1 ); // �������� ����� ����� 
		                settingsComPort.beginGroup( QString( n ) ); // ����� ������ ���������� ��������

						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						dataIni = this->vectorTypesVP_main.at( i )->currentText(); // ��� ����� � ��������� comboBox - �
					    // ���������� ��� ������� � ini - ���� (����� �����)
						settingsComPort.setValue( "type_scales", dataIni );

						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                        dataIni = this->vectorComPortsVP.at( i )->currentText(); // COM-���� � ��������� comboBox - �
					    // ���������� COM-���� � ini - ���� (����� �����)
						settingsComPort.setValue( "com", dataIni );

						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						dataIni = this->vectorAddrVP.at( i )->text(); // ����� �� � ��������� comboBox - �
					    // ���������� ����� � ini - ���� (����� �����)
						settingsComPort.setValue( "addr", dataIni );

						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						dataIni = this->vectorMuxKoef.at( i )->text(); // ����� �� � ��������� comboBox - �
					    // ���������� ��������� � ini - ���� (����� �����)
						settingsComPort.setValue( "koef", dataIni );

						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						dataIni = this->vectorBaudsVP.at( i )->currentText(); // �������� COM-������ � comboBox - � 
					    // ���������� �������� � ini - ���� (����� �����)
						settingsComPort.setValue( "baud", dataIni );

						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						dataIni = this->vectorEOFrameVP.at( i )->currentText(); // ��������� ������� ������ (� comboBox - �)
					    // ���������� ��������� ������� ������ � ini - ���� (����� �����)
						settingsComPort.setValue( "endoframe", dataIni );

						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						// ???
						dataIni = ( this->vectorGrouBoxWorkWithBK.at( i )->isChecked() ? "1" : "0" ); // ���� ������ ����� �����  �� 
					    // ���������� ��������� ������ �� � ini - ���� (����� �����)
						settingsComPort.setValue( "bk", dataIni );

						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                        dataIni = ( this->vectorWorkWithRM.at( i )->isChecked() ? "1" : "0" ); // ���� ������ ����� �����  RM
					    // ���������� ��������� ������ �� � ini - ���� (����� �����)
						settingsComPort.setValue( "rm", dataIni );

						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						// ???
                        dataIni = this->vectorTypesVP_BK.at( i )->currentText(); // ��� �����  ���  ������  �  ��, � ��������� comboBox - � 
					    // ���������� ��� ������� � ini - ���� (����� �����)
						settingsComPort.setValue( "bk_type_scales", dataIni );
						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						dataIni = this->vectorInputDelay_BK.at( i )->text(); // �������� ������������ �������� ��������� 
					    // ���������� ��� ������� � ini - ���� (����� �����)
						settingsComPort.setValue( "input_delay", dataIni );

						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						// dataIni = this->vectorMinVes_BK.at( i )->text(); // ����������� ���  ������  ���������� 
						// ���������� ��� ������� � ini - ���� (����� �����)
						// settingsComPort.setValue( "ves_min", dataIni );

						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						dataIni = ( this->vectorEnableScalesEntity.at( i )->isChecked() ? "1" : "0" ); // ���� ����������/������� ������ ���������� ����� 
					    // ���������� ��������� ���������� ������ ����� � ini - ���� (����� �����)
						settingsComPort.setValue( "scales_enable", dataIni );
						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						dataIni = ( this->vectorcheckBoxUSB_En.at( i )->isChecked() ? "1" : "0" ); // ���� ����������/������� ������ ����� ����� USB
					    // ���������� ��������� ���������� ������ ����� � ini - ���� (����� �����)
						settingsComPort.setValue( "usb", dataIni );
						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

						dataIni = ( trafficLightCheckBox->isChecked() ? "1" : "0" ); // ���� ���������
						settingsComPort.setValue( "traff", dataIni );

						dataIni = ( sensorsCheckBox->isChecked() ? "1" : "0" ); 
						settingsComPort.setValue( "sensors", dataIni );
						dataIni = ( sensorsInversionCheckBox->isChecked() ? "1" : "0" ); 
						settingsComPort.setValue( "sensors_inversion", dataIni );
						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

						// ��������� UDP
						dataIni = this->vectorEnterIP_UDP_Settings.at( i )->text();
						settingsComPort.setValue( "ip", dataIni );
						// 
	                    dataIni = this->vectorEnterSrcPort_UDP_Settings.at( i )->text();
						settingsComPort.setValue( "src_port", dataIni );
						// 
						dataIni = this->vectorEnterDstPort_UDP_Settings.at( i )->text();
						settingsComPort.setValue( "dst_port", dataIni );
						// 
						dataIni = ( this->vectorGroupUDP_Settings.at( i )->isChecked() == true ? "1" : "0" );
                        settingsComPort.setValue( "udp_en", dataIni );


						settingsComPort.setValue( "sor_on", group_Sornost->isChecked() );
						settingsComPort.setValue( "sor_koef", spinKoefSor->value() );
						settingsComPort.setValue( "sor_show", checkSorWeightShow->isChecked() );
						settingsComPort.setValue( "sor_brutto", checkSorUseOnlyBrutto->isChecked() );
						settingsComPort.setValue( "sor_discret", checkSorDiscret->isChecked() );								

						settingsComPort.setValue( "tcp_ip", lineEditTCP_IP->text() );
						settingsComPort.setValue( "tcp_on", group_TCPWeight->isChecked() );

						settingsComPort.setValue( "position_on", group_PositionS2->isChecked() );
						settingsComPort.setValue( "position_ip", lineEdit_PositionS2->text() );
						settingsComPort.setValue( "position_inversion", check_PositionS2->isChecked() );
						settingsComPort.setValue( "position_use_s1", check_PositionUseS1->isChecked() );
						settingsComPort.setValue( "position_ignore", check_PositionIgnore->isChecked() );
						settingsComPort.setValue( "position_4ch", check_PositionUse4ch->isChecked() );

						settingsComPort.setValue( "svetofor_s2_on", group_SvetoforS2->isChecked() );
						settingsComPort.setValue( "svetofor_s2_ip", lineEdit_SvetoforS2->text() );

						settingsComPort.setValue( "barrier_s2_on", group_BarrierS2->isChecked() );
						settingsComPort.setValue( "barrier_s2_ip", lineEdit_BarrierS2->text() );
						
						settingsComPort.endGroup();
					}					

					// ���������� ��������� ���������� ������ ����� � ini - ���� (��������� �������)
					settingsComPort.beginGroup( "Server" );
					dataIni = ( chkBoxServer_AutoStart->isChecked() == true ? "1" : "0" ); // ���� ���������� ������� 
					settingsComPort.setValue( "AutoStart", dataIni );
					settingsComPort.endGroup();
					if(group_TCPWeight->isChecked() )
						emit StartTimerWeight();
				}
                if( button->objectName() == "buttonSaveSettingsIP_Cams"  )
			    {
					int i;
					char n[ 16 ];
					QString dataIni;

					memset( n, 0x00, sizeof( n ) );
                    QSettings settingsComPort( "settings.ini", QSettings::IniFormat );
					settingsComPort.setIniCodec("Windows-1251");

					// ���������� ������ ���� ���������� � ini - ���� 
					for( i = 0; i < this->getNumIP_Cams(); i++ )
					{
                        sprintf( n, "Cam%d", i + 1 ); // �������� ����� ����� 
		                settingsComPort.beginGroup( QString( n ) ); // ����� ������ ���������� ��������

						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						dataIni = this->vectorEnterIP_Address.at( i )->text(); // IP-����� ������ 
						settingsComPort.setValue( "ip_address", dataIni );

						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						dataIni = this->vectorEnterPort_Address.at( i )->text(); // ���� IP-������ 
						settingsComPort.setValue( "port", dataIni );

						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						dataIni = ( this->vectorChkboxEnableIP_Cam.at( i )->isChecked() ? "1" : "0" ); // ���� ���������� ������ IP-������ 
						settingsComPort.setValue( "cam_en", dataIni );

						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						dataIni = ( this->vectorCheckBoxEnablePhotoFix.at( i )->isChecked() ? "1" : "0" ); // ���� ���������� ������������ IP-������ 
						settingsComPort.setValue( "cam_photofix_en", dataIni );

						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						dataIni = ( this->vectorCheckBoxEnablePhotoFixPreview.at( i )->isChecked() ? "1" : "0" ); // ���� ���������� snapshot - �� ��� ������������ 
						settingsComPort.setValue( "cam_photofix_preview_en", dataIni );

						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						//dataIni = ( this->vectorCheckBoxEnableRecognitionAutoNumber.at( i )->isChecked() ? "1" : "0" ); // ���� ���������� ������������� ���������� �� IP-������ 
						//settingsComPort.setValue( "cam_recogn_en", dataIni );
						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						dataIni = this->vectorEnterLogin.at( i )->text(); // ��� ��� ����������� IP-������ 
						settingsComPort.setValue( "login", dataIni );
						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						dataIni = this->vectorEnterPass.at( i )->text(); // ������ ��� ����������� IP-������
						//enc-dec pass
						QString pass = dataIni;
						QString pass_enc;
						if ( ! pass.startsWith("#") )
							pass_enc = Crypt::crypt( pass );
						else
						{
							pass_enc = pass.remove( 0, 1 );
							pass = Crypt::decrypt( pass_enc );
						}
						// 
						settingsComPort.setValue( "password",  pass_enc.prepend("#") );
						// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						settingsComPort.endGroup(); // ������� ������� ������ 
					}
					settingsComPort.setValue( "main_cameras/isHorizontal", check_HorizontalCams->isChecked() );
				}
				// ������ �����/���� ������� 
				if( button->objectName() == "buttonStartServer" )
				{
					static bool  startFlag = false;
                    QIcon icon;

					if( startFlag == false )
					{
						startFlag = true;
						displayServerState( button, startFlag ); // ���������� ��������� ������� (��������� ��� ����������)
					}
					else
					{
						startFlag = false;
						displayServerState( button, startFlag ); // ���������� ��������� ������� (��������� ��� ����������)
					}

					setServerFlagActivity( startFlag ); // ������� ��������� ������� (�����/����)
				}
				return false;
			}
			QComboBox *comboBox = qobject_cast< QComboBox* >( object );
			if( comboBox != 0x00 )
			{
				// �������� ������ COM-������ 
				comboBox->clear(); // �������� ������ ������ 
				comboBox->addItem( "" );
				serialPortClass *sp = new serialPortClass();
			    comboBox->addItems( sp->QuerrySerialPorts() ); // ����������� COM-������ � ������� 				

				return false;
			}
		}

    return false;
}
// ���������� ��������� ������� (��������� ��� ����������)
void SettingsForm::displayServerState( QPushButton *button, bool state )
{
	QIcon icon;

	if( state == true )
	{
		button->setText( "STOP" ); 
	    button->setStyleSheet( "color: Red;" );
	    //icon.setPixmap( "image/Stop_Normal_Red_Icon_48png", QIcon::Large, QIcon::Normal, QIcon::On );
	    //button->setIcon( icon );
	}
	else
	{
		button->setText( "START" ); 
		button->setStyleSheet( "color: Green;" );
		//icon.setPixmap( "image/Play_1_Normal_Icon_48png", QIcon::Large, QIcon::Normal, QIcon::On );
		//button->setIcon( icon );
	}
}
// ���������� ���� ��������� ������� 
void SettingsForm::setServerFlagActivity( bool state )
{
    currentStateOfServer = state;
}
// ������� ��������� ������� 
bool SettingsForm::getServerFlagActivity()
{
    return currentStateOfServer;
}

// ��������� ������� ��������� COM-������  ��  Ini-����� 
void SettingsForm::getCurrentSettingsCOM_PortIniFile( currentSettingsCOM_Port *currSettingsComPortPtr, int num_scales )
{
	QString dataIni;
	char n[ 16 ];

	currSettingsComPortPtr->reset(); // �������� ����  ������� 

    // ??? ��������� ��������� �� *.ini ����� 

	// ��������� � ini - ����  ���  ���������� �������� ������ � ������
    QSettings settingsComPort( "settings.ini", QSettings::IniFormat );
	settingsComPort.setIniCodec("Windows-1251");

    // ���������� ������ ���� ���������� �� ini - ����� 
    sprintf( n, "scales%d", num_scales + 1 ); // �������� ����� ����� 
    settingsComPort.beginGroup( QString( n ) ); // ����� ������ ���������� ��������

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// ��� ����� 
	QList<QString> typesVP = loadTypesOfVP();
	QString currTypeVP = settingsComPort.value( "type_scales" ).toString();
	if (! typesVP.contains( currTypeVP ) )
	{
		currTypeVP = typesVP.at(0);
		settingsComPort.setValue("type_scales", currTypeVP );
		settingsComPort.sync();
		currSettingsComPortPtr->type_scales.append( currTypeVP );
	}
	else
		currSettingsComPortPtr->type_scales.append( ( ( QString* )&settingsComPort.value( "type_scales" ) ) );
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// COM-���� � ini - ���� (����� �����)
	currSettingsComPortPtr->com.append( ( ( QString* )&settingsComPort.value( "com" ) ) );
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// �������� UART
	currSettingsComPortPtr->baud.append( ( ( QString* )&settingsComPort.value( "baud" ) ) );
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// ����� ������� ������ 
	currSettingsComPortPtr->endoframe.append( ( ( QString* )&settingsComPort.value( "endoframe" ) ) );
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ??? 
	// ���� ������ ����� �����  �� 
	currSettingsComPortPtr->bk = ( settingsComPort.value( "bk" ) == 1 ? true : false );
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ���� ������ ����� �����  RM
    currSettingsComPortPtr->rm = ( settingsComPort.value( "rm" ) == 1 ? true : false );
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ��� �����  ���  ������  �  ��, � ��������� comboBox - � 
	currSettingsComPortPtr->bk_type_scales.append( ( ( QString* )&settingsComPort.value( "bk_type_scales" ) ) );
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // �������� ������������ �������� ��������� 
	currSettingsComPortPtr->input_delay = ( int )( ( QString* )&settingsComPort.value( "input_delay" ) );
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// ����������� ���  ������  ���������� 
	currSettingsComPortPtr->ves_min = ( int )( ( QString* )&settingsComPort.value( "ves_min" ) );
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ���� ����������/������� ������ ���������� ����� 
	currSettingsComPortPtr->scales_enable = ( settingsComPort.value( "scales_enable" ) == 1 ? true : false );
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    settingsComPort.endGroup(); // ������� ������� ������ 
}

// ��������� ������� ��������� IP-����� ��  Ini-����� 
void SettingsForm::getCurrentSettings_IP_CamsIniFile( currentSettingsIP_Cam *currentSettingsIP_CamPtr, int num_cam )
{
	QString dataIni;
	char n[ 16 ];

	if( currentSettingsIP_CamPtr == 0x00 )
		return;

	currentSettingsIP_CamPtr->reset(); // �������� ����  ������� 

    // ��������� ��������� �� *.ini ����� 
	// ��������� � ini - ����  ���  ���������� �������� ������ � ������
    QSettings settingsComPort( "settings.ini", QSettings::IniFormat );
	settingsComPort.setIniCodec("Windows-1251");

        // ���������� ������ ���� ���������� �� ini - ����� 
        sprintf( n, "Cam%d", num_cam + 1 ); // �������� ����� ����� 
        settingsComPort.beginGroup( QString( n ) ); // ����� ������ ���������� ��������

        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// IP-����� 
		currentSettingsIP_CamPtr->ip_address.append( ( ( QString* )&settingsComPort.value( "ip_address" ) ) );
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	    // ���� � ini - ���� (����� �����)
		currentSettingsIP_CamPtr->port.append( ( ( QString* )&settingsComPort.value( "port" ) ) );
        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // ���� ���������� ������ IP-������ 
		currentSettingsIP_CamPtr->cam_en = ( settingsComPort.value( "cam_en" ) == 1 ? true : false );
        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // ���� ���������� ������ ������������ 
		currentSettingsIP_CamPtr->cam_photofix_en = ( settingsComPort.value( "cam_photofix_en" ) == 1 ? true : false );
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // ���� ���������� snapshot - �� ��� ������������ 
		currentSettingsIP_CamPtr->cam_photofix_preview_en = ( settingsComPort.value( "cam_photofix_preview_en" ) == 1 ? true : false );

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        // ���� ���������� ������ ������������� ������ ���������� 
		// currentSettingsIP_CamPtr->cam_recogn_en = ( settingsComPort.value( "cam_recogn_en" ) == 1 ? true : false );
        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// ��� ��� ����������� IP-������ 
		currentSettingsIP_CamPtr->login.append( ( ( QString* )&settingsComPort.value( "login" ) ) );
        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// ������ ��� ����������� IP-������ 
		//enc-dec pass
		QString pass = settingsComPort.value( "password" ).toString();
		QString pass_enc;
		if ( ! pass.startsWith("#") )
			pass_enc = Crypt::crypt( pass );
		else
		{
			pass_enc = pass.remove( 0, 1 );
			pass = Crypt::decrypt( pass_enc );
		}
		// 
		currentSettingsIP_CamPtr->password = pass;
        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        settingsComPort.endGroup(); // ������� ������� ������ 
}

// Devices save
void SettingsForm::buttonSaveDevicesSettings_Slot()
{
    QSettings settings( "settings.ini", QSettings::IniFormat );
	settings.setIniCodec("Windows-1251");
	QTextCodec *textCodec = QTextCodec::codecForName("Windows-1251");

	settings.setValue( "Devices/socket1_en", chkboxEnableVk1->isChecked() ? "1" : "0" );
	settings.setValue( "Devices/socket2_en", chkboxEnableVk2->isChecked() ? "1" : "0" );
	settings.setValue( "Devices/socket3_en", chkboxEnableVk3->isChecked() ? "1" : "0" );

	settings.setValue( "Devices/socket1_ip", enterVk1->text() );
	settings.setValue( "Devices/socket2_ip", enterVk2->text() );
	settings.setValue( "Devices/socket3_ip", enterVk3->text() );

	settings.setValue( "Devices/socket1_name1", QString(textCodec->fromUnicode( enterVk1Name1->text() )));
	settings.setValue( "Devices/socket1_name2", QString(textCodec->fromUnicode(enterVk1Name2->text() )));
	settings.setValue( "Devices/socket2_name1", QString(textCodec->fromUnicode(enterVk2Name1->text() )));
	settings.setValue( "Devices/socket2_name2", QString(textCodec->fromUnicode(enterVk2Name2->text() )));
	settings.setValue( "Devices/socket3_name1", QString(textCodec->fromUnicode(enterVk3Name1->text() )));
	settings.setValue( "Devices/socket3_name2", QString(textCodec->fromUnicode(enterVk3Name2->text() )));

	//Rfid
	settings.setValue("rfid/IP2", enterRfid1->text());
	settings.setValue("rfid/lamps_ip", lineEdit_Lamps->text());
	settings.setValue("rfid/enable", group_Rfid->isChecked() ? "1" : "0");
	settings.setValue("rfid/registration", chkboxEnableRfid1->isChecked() ? "1" : "0");


	settings.setValue( "Devices/traff1", groupTraffLight1->isChecked() ? "1" : "0" );
	settings.setValue( "Devices/traff1_port", selectComPortTablo1->currentText() );
	settings.setValue( "Devices/traff1_isCOM", radioTablo_COM1->isChecked() );
	settings.setValue( "Devices/traff1_isIP", radioTablo_IP1->isChecked() );
	settings.setValue( "Devices/traff1_IP", enterTablo_IP1->text() );

	settings.setValue( "Devices/traff2", groupTraffLight2->isChecked() ? "1" : "0" );
	settings.setValue( "Devices/traff2_port", selectComPortTablo2->currentText() );
	settings.setValue( "Devices/traff2_isCOM", radioTablo_COM2->isChecked() );
	settings.setValue( "Devices/traff2_isIP", radioTablo_IP2->isChecked() );
	settings.setValue( "Devices/traff2_IP", enterTablo_IP2->text() );

	settings.setValue( "Devices/traff_min", enterMinWeightTablo->text() );
	settings.setValue( "Devices/traff_time", enterTimerTablo->text() );	
		
    //  
	settings.sync();
}

void SettingsForm::buttonSaveMySQL_Slot()
{
    QSettings settings( "settings.ini", QSettings::IniFormat );
	settings.setIniCodec("Windows-1251");

	settings.setValue( "MySQL/address",		enterMySQL_address->text() );
	settings.setValue( "MySQL/port",		enterMySQL_port->text() );
	settings.setValue( "MySQL/login",		enterMySQL_login->text() );
	//enc-dec pass
	QString pass = enterMySQL_password->text();
	QString pass_enc;
	if ( ! pass.startsWith("#") )
		pass_enc = Crypt::crypt( pass );
	else
	{
		pass_enc = pass.remove( 0, 1 );
		pass = Crypt::decrypt( pass_enc );
	}
	// 
	settings.setValue( "MySQL/password", pass_enc.prepend("#") );
	
	settings.sync();
}

void SettingsForm::buttonSaveEmailSettings_Slot()
{
	// ��������� � ini - ����  ���  ���������� �������� ������ � ������
    QSettings settings( "settings.ini", QSettings::IniFormat );
	settings.setIniCodec("Windows-1251");

    // ��������� ��������� ����� 
	settings.setValue( "mail/host", enterHostName->text() );
	settings.setValue( "mail/port", enterPort->text() );
	settings.setValue( "mail/ssl", ( checkBox_ssl->isChecked() == true ? "1" : "0" ) );
	settings.setValue( "mail/login", enterLoginEmail->text() );
	//enc-dec pass
	QString pass = enterPassword->text();
	QString pass_enc;
	if ( ! pass.startsWith("#") )
		pass_enc = Crypt::crypt( pass );
	else
	{
		pass_enc = pass.remove( 0, 1 );
		pass = Crypt::decrypt( pass_enc );
	}
	// 
	settings.setValue( "mail/password", pass_enc.prepend("#") );
	settings.setValue( "mail/mailToSend", enterMailTo->text() );
	settings.setValue( "mail/check_Adr_1", ( checkBox_Adr_1->isChecked() ? "1" : "0" ) );
	settings.setValue( "mail/enterAdress_1", enterAdress_1->text() );
	settings.setValue( "mail/check_Adr_2", ( checkBox_Adr_2->isChecked() ? "1" : "0" ) );
	settings.setValue( "mail/enterAdress_2", enterAdress_2->text() );
	settings.setValue( "mail/check_Adr_3", ( checkBox_Adr_3->isChecked() ? "1" : "0" ) );
	settings.setValue( "mail/enterAdress_3", enterAdress_3->text() );
	settings.setValue( "mail/enable_send_report", ( checkBoxEnableMailSend->isChecked() ? "1" : "0" ) );
	settings.setValue( "mail/enable_send_photos", ( checkBoxPhotoSaveAll->isChecked() ? "1" : "0" ) );
	settings.setValue( "mail/enable_send_eachrec", ( checkBoxSendEachRec->isChecked() ? "1" : "0" ) );
	settings.setValue( "mail/interval_send_report", selectIntervalSend->currentText() );
	settings.setValue( "excel/date_time_format", enterExcelDateTimeFormat->text() ); // ��.��.���� ��:��
	settings.sync();
}

void SettingsForm::saveSens_Slot()
{
	QSettings settings( "settings.ini", QSettings::IniFormat );
	settings.setIniCodec("Windows-1251");

	settings.setValue( "vp01_s/interval", enter_sInterval->text() );
	settings.setValue( "vp01_s/number", enter_sNumber->text() );
	settings.setValue( "vp01_s/info_contragent", enter_sInfoContragent->text() );
	settings.setValue( "vp01_s/info_type", enter_sInfoWType->text() );

	settings.sync();
}

// 
bool SettingsForm::getCurrentScales( int num_scales )
{
	return ( this->vectorTypesVP_main.at( num_scales )->currentIndex() == 0 || this->vectorTypesVP_main.at( num_scales )->currentIndex() == 5 );
}

// 
SettingsForm::~SettingsForm()
{
}

// set end of frame for choosen processor
void SettingsForm::setEndOfFrame_Slot( int i )
{
	const int NONE = 0;
	const int CR = 1;
	const int LF = 2;
	const int CRLF = 3;

	switch( i )
	{
	case 0: // ��-05
		endOfFrame->setCurrentIndex( NONE );
		break;
	case 1: // ��-01
		endOfFrame->setCurrentIndex( CR );
		break;
	case 2: // ����(CAS ��� I3)
		endOfFrame->setCurrentIndex( NONE );
		break;
	case 3: // Keli A12(���.P5 �4)
		endOfFrame->setCurrentIndex( NONE );
		break;
	case 4: // ����(�������� �1)
		endOfFrame->setCurrentIndex( NONE );
		break;
	case 5: // ���� ����������(��-01)
		endOfFrame->setCurrentIndex( CR );
		break;
	case 6: // ��-01 (��������)
		endOfFrame->setCurrentIndex( NONE );
		break;
	case 7: // WE2108
		endOfFrame->setCurrentIndex( LF );
		break;
	case 8: // XK3118T1
		endOfFrame->setCurrentIndex( NONE );
		break;
	case 9: // ESIT PWI
		endOfFrame->setCurrentIndex( NONE );
		break;
	case 10: // �������
		endOfFrame->setCurrentIndex( CR );
		break;
	case 11: // RiceLake
		endOfFrame->setCurrentIndex( CRLF );
		break;
	case 12: // axis se01
		endOfFrame->setCurrentIndex( CRLF );
		break;
	case 13: // esit �������
		endOfFrame->setCurrentIndex( NONE );
		break;
	case 14: // t7e
		endOfFrame->setCurrentIndex( NONE );
		break;
	case 15: // SMART_ABS_F9
		endOfFrame->setCurrentIndex( CR );
		break;
	case 16: // SMART_ABS_��� �������
		endOfFrame->setCurrentIndex( CR );
		break;
	case 17: // BK05
		endOfFrame->setCurrentIndex( CR );
		break;
	case 18: // ESIT PWI � ������
		endOfFrame->setCurrentIndex( NONE );	
		break;
	case 19: // WE2107(��� �������)
		endOfFrame->setCurrentIndex( NONE );	
		break;
	case 20: // WE2110(��� �������)
		endOfFrame->setCurrentIndex( NONE );	
		break;
	case 21: // D12
		endOfFrame->setCurrentIndex( NONE );	
		break;
	case 22: // ���������
		endOfFrame->setCurrentIndex( CRLF );	
		break;
	case 23: // ���� ��-1
		endOfFrame->setCurrentIndex( CR );	
		break;
	case 24: // Zemic A9
		endOfFrame->setCurrentIndex( NONE );	
		break;
	case 25: // Keli D39 (���2)
		endOfFrame->setCurrentIndex( NONE );	
		break;
	case 26: // XK3118T1 � �������� ������
		endOfFrame->setCurrentIndex( NONE );
		break;
	}
}

// �������� ������ ������ 
void SettingsForm::update_SerialPortsList( QObject *serPortClassObject, QObject *control, QString currentElement,  QList< QString > *serPortsList )
{
	serialPortClass *sp = qobject_cast< serialPortClass* >( serPortClassObject );

	*serPortsList = sp->QuerrySerialPorts();
	QComboBox *cb = qobject_cast< QComboBox* >( control );
	
	cb->addItem( " " ); // �������� ������ ������ � ������ ������ 
	
	if( serPortsList->length() != 0 )
	{
        cb->addItems( *serPortsList );
        cb->setCurrentIndex( ( serPortsList->indexOf( currentElement ) != -1 ? serPortsList->indexOf( currentElement ) : 0  ) );
	}
	else
		cb->clear();
}

// 
void SettingsForm::checkBoxCoord1_Slot( int state )
{
	QSettings settings( "settings.ini", QSettings::IniFormat );
	settings.setIniCodec( "Windows-1251" );

	if( state )
	{
		checkBoxCoord2->setChecked( false );
		checkBoxCoord3->setChecked( false );
		checkBoxCoord4->setChecked( false );

		settings.setValue( "image/left_top", "1" );
		settings.setValue( "image/left_bottom", "0" );
		settings.setValue( "image/right_top", "0" );
		settings.setValue( "image/right_bottom", "0" );
	}
}

// 
void SettingsForm::checkBoxCoord2_Slot( int state )
{
	QSettings settings( "settings.ini", QSettings::IniFormat );
	settings.setIniCodec( "Windows-1251" );

	if( state )
	{
		checkBoxCoord1->setChecked( false );
		checkBoxCoord3->setChecked( false );
		checkBoxCoord4->setChecked( false );

		settings.setValue( "image/left_top", "0" );
		settings.setValue( "image/left_bottom", "1" );
		settings.setValue( "image/right_top", "0" );
		settings.setValue( "image/right_bottom", "0" );
	}
}

// 
void SettingsForm::checkBoxCoord3_Slot( int state )
{
	QSettings settings( "settings.ini", QSettings::IniFormat );
	settings.setIniCodec( "Windows-1251" );

	if( state )
	{
		checkBoxCoord1->setChecked( false );
		checkBoxCoord2->setChecked( false );
		checkBoxCoord4->setChecked( false );

		settings.setValue( "image/left_top", "0" );
		settings.setValue( "image/left_bottom", "0" );
		settings.setValue( "image/right_top", "1" );
		settings.setValue( "image/right_bottom", "0" );
	}
}

// 
void SettingsForm::checkBoxCoord4_Slot( int state )
{
	QSettings settings( "settings.ini", QSettings::IniFormat );
	settings.setIniCodec( "Windows-1251" );

	if( state )
	{
		checkBoxCoord1->setChecked( false );
		checkBoxCoord2->setChecked( false );
		checkBoxCoord3->setChecked( false );

		settings.setValue( "image/left_top", "0" );
		settings.setValue( "image/left_bottom", "0" );
		settings.setValue( "image/right_top", "0" );
		settings.setValue( "image/right_bottom", "1" );
	}
}

// 
int SettingsForm::getNumPosStampPhoto()
{	
	if( checkBoxCoord1->isChecked() )	return 0;
	else if( checkBoxCoord2->isChecked() )	return 1;
	else if( checkBoxCoord3->isChecked() )	return 2;
	
	return 3;
}
