#include "stdafx.h"
#include "taraDictionaryModel.h"

taraDictionaryModel::taraDictionaryModel( int rows, int columns, QList<QString > headers_names, QObject *parent ) : QAbstractTableModel( parent )
{
    m_rowsCount = rows;
	m_columnCount = columns;
	this->headers_names = headers_names;
}

// 
void taraDictionaryModel::addParams( int rows, int columns, QList<QString > headers_names )
{
	m_rowsCount = rows;
	m_columnCount = columns;
	this->headers_names = headers_names;
}

// Row count
int taraDictionaryModel::rowCount( const QModelIndex &parent ) const
{
	Q_UNUSED(parent);
	int rows = m_data.count();
	return rows;
}

int taraDictionaryModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_columnCount;
}

// 
QVariant taraDictionaryModel::data(const QModelIndex &index, int role) const
{
	if (role == Qt::DisplayRole)
	{
        return m_data[index.row()].at(index.column());
	}
	
	return QVariant();
}

// 
QVariant taraDictionaryModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
    QString header;

    if (role != Qt::DisplayRole)
        return QVariant();
		
    if (orientation == Qt::Horizontal)
	{
		header = headers_names.at( section );
		return header;
    }
	else if (orientation == Qt::Vertical)
	{
		header = QString( "%1" ).arg( section + 1 );
		return header;
	}

	return QVariant();
}

// 
bool taraDictionaryModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole) {
		// m_data[index.row()]->replace(index.column(), value.toString() );
        // emit dataChanged(index, index);
        return true;
    }
	else if (index.isValid() && role == Qt::TextColorRole )
	{
		return true;
	}

    return false;
}

Qt::ItemFlags taraDictionaryModel::flags(const QModelIndex &index) const
{
    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

// �������� ������ ������ 
void taraDictionaryModel::update( QVector< QString > *data )
{
	m_data.append( *data );
}

// �������� ������ 
void taraDictionaryModel::clear()
{
	m_data.clear();
}
