#include "stdafx.h"
#include "Models/reportsTableModel.h"


ReportsTableModel::ReportsTableModel( int rows, int columns, QList<QString > headers_names, QObject *parent ) : QAbstractTableModel( parent )
{
    m_rowsCount = rows;
	m_columnCount = columns;
	this->headers_names = headers_names;
}

// 
void ReportsTableModel::addParams( int rows, int columns, QList<QString > headers_names )
{
	m_rowsCount = rows;
	m_columnCount = columns;
	this->headers_names = headers_names;
}

// �������� � ������ ��������  ��������� ����� � ������ 
void ReportsTableModel::setColoredLinesSign( int colored_row )
{
	this->rows_colored.append( colored_row );
}

// �������� ������ ���������  ��������� ����� � ������ 
void ReportsTableModel::clearColoredLinesSign()
{
	this->rows_colored.clear();
}

// Row count
int ReportsTableModel::rowCount( const QModelIndex &parent ) const
{
	Q_UNUSED(parent);

	int rows = m_data.count();

	return rows;
}

int ReportsTableModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_columnCount;
}

// 
QVariant ReportsTableModel::data(const QModelIndex &index, int role) const
{
	if (role == Qt::DisplayRole)
	{
        return m_data[index.row()].at(index.column());
	}
	/*else if( role == Qt::ForegroundRole )
	{
		if( index.column() == 9 || index.column() == 10 || index.column() == 11 )
		{
			QBrush whiteColor( Qt::blue );
			return whiteColor;
		}
	}
	*/

	else if( role == Qt::BackgroundColorRole )
	{
		if( rows_colored.size() != 0 )
		{
			// "������������� �����������"  -  ���������� ���� 
			if( rows_colored.at( index.row() ) == Colors::GRAY )
			{
				QBrush color( QColor( 56, 56, 56 ) );
				return color;
			}
			else // ������� ����������� 
			if( rows_colored.at( index.row() ) == Colors::WHITE )
			{
				QBrush color( QColor( 0xff, 0xff, 0xff ) );
				return color;
			}
		}
	}
	else if( role == Qt::ForegroundRole )
	{
		if( rows_colored.size() != 0 )
		{
			// "������������� �����������"  -  ���� ������ - ����� 
			if( rows_colored.at( index.row() ) == Colors::GRAY )
			{
				QBrush color( QColor( 0xff, 0xff, 0xff ) );
				return color;
			}
			else // ������� ����������� 
				if( rows_colored.at( index.row() ) == Colors::WHITE )
			{
				QBrush color( QColor( 0x00, 0x00, 0x00 ) );
				return color;
			}
		}
	}

	/*
	else if( role == Qt::FontRole )
	{
		if( index.column() == 9 || index.column() == 10 || index.column() == 11 )
		{
			QFont font( "Arial", 12, QFont::Bold );

		    return font;
		}
	}
	else if( role == Qt::TextAlignmentRole )
	{
		if( index.column() == 9 || index.column() == 10 || index.column() == 11 )
		{
		    return Qt::AlignCenter;
		}
	}*/

	return QVariant();
}

// 
QVariant ReportsTableModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
    QString header;

    if (role != Qt::DisplayRole)
        return QVariant();
		
    if (orientation == Qt::Horizontal)
	{
		header = headers_names.at( section );
		return header;
    }
	else if (orientation == Qt::Vertical)
	{
		header = QString( "%1" ).arg( section + 1 );
		return header;
	}

	return QVariant();
}

// 
bool ReportsTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole) {
		// m_data[index.row()]->replace(index.column(), value.toString() );
        // emit dataChanged(index, index);
        return true;
    }
	else if (index.isValid() && role == Qt::TextColorRole )
	{
		return true;
	}

    return false;
}

Qt::ItemFlags ReportsTableModel::flags(const QModelIndex &index) const
{
    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

// �������� ������ ������ 
void ReportsTableModel::update( QVector< QString > *data )
{
	m_data.append( *data );
}

// �������� ������ 
void ReportsTableModel::clear()
{
	m_data.clear();
}
