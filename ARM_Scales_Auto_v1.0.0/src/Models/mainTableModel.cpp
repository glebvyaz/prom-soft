#include "stdafx.h"
#include "Models/MainTableModel.h"
#include <QtCore>

MainTableModel::MainTableModel( QObject *parent ) : QAbstractTableModel( parent )
{
}

MainTableModel::MainTableModel( int rows, int columns, QList<QString > headers_names, QObject *parent ) : QAbstractTableModel( parent )
{
    m_rowsCount = rows;
	m_columnCount = columns;
	this->headers_names.clear();
	this->headers_names.append( headers_names );
}

// 
void MainTableModel::addParams( int row_count, int column_count, QList<QString > headers_names )
{
    m_rowsCount = row_count;
	m_columnCount = column_count;
	this->headers_names = headers_names;
}

// Row count
int MainTableModel::rowCount( const QModelIndex &parent ) const
{
	Q_UNUSED(parent);

	int rows = m_data.count();

	return rows;
}

int MainTableModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_columnCount;
}

// 
QVariant MainTableModel::data(const QModelIndex &index, int role) const
{
	if (role == Qt::DisplayRole)
	{
        return m_data[index.row()].at(index.column());
	}
	else if( role == Qt::ForegroundRole )
	{
		/*if( index.column() == 9 || index.column() == 10 || index.column() == 11 )
		{
			QBrush whiteColor( Qt::blue );
			return whiteColor;
		}*/
	}
	else if( role == Qt::BackgroundColorRole )
	{
		/*if( index.column() == 9 || index.column() == 10 || index.column() == 11 )
		{
			QBrush greenBackground( QColor( 234, 234, 235 ) );
		    return greenBackground;
		}*/
	}
	else if( role == Qt::FontRole )
	{
		/*if( index.column() == 9 || index.column() == 10 || index.column() == 11 )
		{
			QFont font( "Arial", 12, QFont::Bold );

		    return font;
		}*/
	}
	else if( role == Qt::TextAlignmentRole )
	{
		/*if( index.column() == 9 || index.column() == 10 || index.column() == 11 )
		{
		    return Qt::AlignCenter;
		}*/
	}

	return QVariant();
}

// 
QVariant MainTableModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
    QString header;

    if (role != Qt::DisplayRole)
        return QVariant();
		
    if (orientation == Qt::Horizontal)
	{
		header = headers_names.at( section );
		return header;
    }
	else if (orientation == Qt::Vertical)
	{
		header = QString( "%1" ).arg( section + 1 );
		return header;
	}

	return QVariant();
}

// 
bool MainTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole) {
		// m_data[index.row()]->replace(index.column(), value.toString() );
        // emit dataChanged(index, index);
        return true;
    }
	else if (index.isValid() && role == Qt::TextColorRole )
	{
		return true;
	}


    return false;
}

Qt::ItemFlags MainTableModel::flags(const QModelIndex &index) const
{
    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

// �������� ������ ������ 
void MainTableModel::update( QVector< QString > *data )
{
	m_data.append( *data );
}

// �������� ������ 
void MainTableModel::clear()
{
	m_data.clear();
}









