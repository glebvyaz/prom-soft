#include "stdafx.h"
#include "Models/RegistrationModel.h"


RegistrationModel::RegistrationModel( int rows, int columns, QList<QString > headers_names, QObject *parent ) : QAbstractTableModel( parent )
{
	//textCodec->codecForName("Windows-1251");
	textCodec = QTextCodec::codecForName("Windows-1251");
	m_rowsCount = rows;
	m_columnCount = columns;
	this->headers_names = headers_names;
}

// 
void RegistrationModel::addParams( int rows, int columns, QList<QString > headers_names )
{
	m_rowsCount = rows;
	m_columnCount = columns;
	this->headers_names = headers_names;
}

// �������� � ������ ��������  ��������� ����� � ������ 
void RegistrationModel::setColoredLinesSign( int colored_row )
{
	this->rows_colored.append( colored_row );
}

// �������� ������ ���������  ��������� ����� � ������ 
void RegistrationModel::clearColoredLinesSign()
{
	this->rows_colored.clear();
}

// Row count
int RegistrationModel::rowCount( const QModelIndex &parent ) const
{
	Q_UNUSED(parent);

	int rows = m_data.count();

	return rows;
}

int RegistrationModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_columnCount;
}

// 
QVariant RegistrationModel::data(const QModelIndex &index, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if(index.column() == 9)
			return m_data[index.row()].at(index.column()).toInt() ? tr("Yes") : tr("No");
			//textCodec->toUnicode(m_data[index.row()].at(index.column()).toInt() ? "��" : "���");
		else
		{
			qDebug()<<m_data[index.row()].at(index.column())<< "  ";
			return m_data[index.row()].at(index.column());
		}

	}
	/*else if( role == Qt::ForegroundRole )
	{
		if( index.column() == 9 || index.column() == 10 || index.column() == 11 )
		{
			QBrush whiteColor( Qt::blue );
			return whiteColor;
		}
	}
	*/

	//else if( role == Qt::BackgroundColorRole )
	//{
	//	/*
	//	    ���� �  ������� ����������� ����� ������, �������  ������  ����  �������  � ���� "������ ��������",
	//		�� ���������� ��� ���������� ������ 
	//	*/

	//	if( rows_colored.size() != 0 )
	//	{
	//		if( rows_colored.at( index.row() ) == Colors::WHITE )
	//		{
	//			QBrush greenBackground( QColor( 255, 255, 255 ) );

	//			return greenBackground;
	//		}
	//		else // ����� - "��������� �������"  -  ���������� ���� 
	//		if( rows_colored.at( index.row() ) == Colors::BROWN )
	//		{
	//			QBrush greenBackground( QColor( 0x99, 0x66, 0x33 ) );

	//			return greenBackground;
	//		}
	//		else
	//		if( rows_colored.at( index.row() ) == Colors::BLUE )
	//		{
	//			QBrush greenBackground( QColor( 0x00, 0x99, 0xFF ) );

	//			return greenBackground;
	//		}
	//		else
	//		if( rows_colored.at( index.row() ) == Colors::GREEN )
	//		{
	//			QBrush greenBackground( QColor( 0x99, 0xFF, 0x99 ) );

	//			return greenBackground;
	//		}
	//	}
	//}
	//else if( role == Qt::ForegroundRole )
	//{
	//	/*
	//		���� �  ������� ����������� ����� ������, �������  ������  ����  �������  � ���� "������ ��������",
	//		�� ���������� ��� ����� ������ 
	//	*/
	//	if( rows_colored.size() != 0 )
	//	{
	//		// ����� - "��������� �������"  -  ���������� ���� 
 //           if( rows_colored.at( index.row() ) == Colors::BROWN )
	//		{
	//			QBrush greenBackground( QColor( 0xFF, 0xFF, 0xFF ) );

	//			return greenBackground;
	//		}
	//		else
	//		if( rows_colored.at( index.row() ) == Colors::BLUE )
	//		{
	//			QBrush greenBackground( QColor( 0xFF, 0xFF, 0xFF ) );

	//			return greenBackground;
	//		}
	//		/*else
	//		if( rows_colored.at( index.row() ) == Colors::GREEN )
	//		{
 //               QBrush greenBackground( QColor( 0xFF, 0xFF, 0xFF ) );

	//			return greenBackground;
	//		}*/
	//	}
	//}

	/*
	else if( role == Qt::FontRole )
	{
		if( index.column() == 9 || index.column() == 10 || index.column() == 11 )
		{
			QFont font( "Arial", 12, QFont::Bold );

		    return font;
		}
	}
	else if( role == Qt::TextAlignmentRole )
	{
		if( index.column() == 9 || index.column() == 10 || index.column() == 11 )
		{
		    return Qt::AlignCenter;
		}
	}*/

	return QVariant();
}

// 
QVariant RegistrationModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
    QString header;

    if (role != Qt::DisplayRole)
        return QVariant();
		
    if (orientation == Qt::Horizontal)
	{
		header = headers_names.at( section );
		return header;
    }
	else if (orientation == Qt::Vertical)
	{
		header = QString( "%1" ).arg( section + 1 );
		return header;
	}

	return QVariant();
}

// 
bool RegistrationModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole) {
		// m_data[index.row()]->replace(index.column(), value.toString() );
        // emit dataChanged(index, index);
        return true;
    }
	else if (index.isValid() && role == Qt::TextColorRole )
	{
		return true;
	}

    return false;
}

Qt::ItemFlags RegistrationModel::flags(const QModelIndex &index) const
{
    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

// �������� ������ ������ 
void RegistrationModel::update( QVector< QString > *data )
{
	m_data.append( *data );
}

// �������� ������ 
void RegistrationModel::clear()
{
	m_data.clear();
}
