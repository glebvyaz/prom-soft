#include "stdafx.h"
#include "serialPortClass.h"
#include "settings.h"

serialPortClass::serialPortClass()
{
}

void serialPortClass::initComPortProperties()
{
	portInfo = new QSerialPortInfo();
	// serialPort = new QSerialPort();
}

// ���������� ������� ������ MyClass
bool serialPortClass::eventFilter( QObject *object, QEvent *e )
{
	if( object == this ) // comPortsList ) // this->self->comPortsList )
	{
		// ���������� ������� �� ComboBox
		if( e->type() == QEvent::MouseButtonPress )
		{
			// QuerrySerialPorts( self ); // �������� COM-������ � ������� 
		}
	}

	return false;
}

// ���������� ����� � �������
// ���������� ������ ��������� COM-������ 
QList<QString> serialPortClass::QuerrySerialPorts()
{
	int i;
	QList<QString> *list = new QList<QString>();

	// listOfPorts.setSharable( true );
	listOfPorts = portInfo->availablePorts();

    list->clear();
	for( i = 0; i < listOfPorts.count(); i++ )
	{
		list->append( listOfPorts.at(i).portName() );
	}

	currPort = ( i != 0 ? list->at( 0 ) : "" ); // ???  ������� ���� �� ��������� 

    return *list;
}
// ������� � ComboBox ����������� �������� ������ �� COM-����� 
QList<QString> serialPortClass::QuerrySerialPortSpeeds()
{
	QList<QString> *list = new QList<QString>();

	Bauds = portInfo->standardBaudRates(); // ������� ����������� �������� UART

	QString baud;

	list->clear();
	for( int i = 0; i < Bauds.count(); i++ )
	{
		baud = QString::number( Bauds[ i ] ); // �������������� �� int � QString
		list->append( baud );
	}

	return *list;
}

// �������/������� COM-����� 
bool serialPortClass::OpenSerialPort( QString port, int baud )
{
	bool result = false;

	try
	{
		if( serialPort->isOpen() )
			serialPort->close();

	    serialPort->setPortName( port );
	    serialPort->Baud9600;
	    serialPort->OneStop;
	    serialPort->NoParity;

		result = serialPort->open( QIODevice::ReadWrite ); // ���� ��������� �������� COM-����� 

		if( result == 0 ) // ���� ���� �� �������� �������, �� ���������� ���������� 
			throw("Com port is closed");
	}
	catch( const char* message )
	{
        QMessageBox qm;
        qm.setText(message);
		qm.setWindowTitle("Error");
        qm.exec();
	}

	return result;
}

// �������/������� COM-����� 
bool serialPortClass::OpenSerialPort( QSerialPort *serialPort, QString port, int baud )
{
	bool result = false;

	try
	{
		if( serialPort->isOpen() )
		    serialPort->close();

		serialPort->setBaudRate( baud, QSerialPort::AllDirections );
		serialPort->setPortName( port );

		
		result = serialPort->open( QIODevice::ReadWrite ); // ���������� ���� ���������/�� ��������� �������� COM-�����

		serialPort->setStopBits( QSerialPort::OneStop );
		serialPort->setParity( QSerialPort::NoParity );
		serialPort->setDataBits( QSerialPort::Data8 );
		serialPort->setFlowControl( QSerialPort::NoFlowControl );

        serialPort->setBaudRate( baud, QSerialPort::AllDirections );
		serialPort->setPortName( port );

		QString error = QString( "������ COM-port-� %1" ).arg( port );

		if( result == false ) // ���� ���� �� �������� �������, �� ���������� ���������� 
		{
			throw( "Com port is closed" );
		}

	}
	catch( const char* message )
	{
		serialPort->close();

        QMessageBox qm;
        qm.setText( message );
		qm.setWindowTitle("Error");
        qm.exec();
	}

	return result;
}
// ������� COM-����� 
void serialPortClass::CloseSerialPort( QSerialPort *serialPort )
{
    serialPort->close();
}

// ������� ������ � ���� 
qint64 serialPortClass::writeData2Port( QSerialPort *serPort, char *buff, int len )
{
	qint64 result = 0;

	result = serPort->write( buff );
	return result;
}

// ������� ������ � ���� 
qint64 serialPortClass::writeData2Port( QSerialPort *serPort, QByteArray data, int len )
{
	qint64 result = 0;

	result = serPort->write( data, len );
	// serPort->waitForBytesWritten( 1000 );
	return result;
}
// ������� ���� � ���� 
qint64 serialPortClass::writeData2Port( QSerialPort *serPort, unsigned char data )
{
	qint64 result = 0;

	result = serPort->write( QByteArray().append( data ) );
	return result;
}
// ���������� �������� ������ �� COM-����� 
void serialPortClass::dataReceived()
{
	int A = 10000;


}
// ������ ������ �� ����� 
void rdDataFromPort( QSerialPort *serialPort )
{
	
}

// ���������� �������� ������ �� COM-����� 
void serialPortClass::serialPort0_dataReceived()
{
	int A = 10000;
}
// ���������� �������� ������ �� COM-����� 
void serialPortClass::serialPort1_dataReceived()
{
	int A = 10000;
}
// ���������� �������� ������ �� COM-����� 
void serialPortClass::serialPort2_dataReceived()
{
	int A = 10000;
}
// ���������� �������� ������ �� COM-����� 
void serialPortClass::serialPort3_dataReceived()
{
	int A = 10000;
}
// ���������� �������� ������ �� COM-����� 
void serialPortClass::serialPort4_dataReceived()
{
	int A = 10000;
}
// ���������� �������� ������ �� COM-����� 
void serialPortClass::serialPort5_dataReceived()
{
	int A = 10000;
}
// ���������� �������� ������ �� COM-����� 
void serialPortClass::serialPort6_dataReceived()
{
	int A = 10000;
}
// ���������� �������� ������ �� COM-����� 
void serialPortClass::serialPort7_dataReceived()
{
	int A = 10000;
}

// ���������� ������ serialPortClass
serialPortClass::~serialPortClass()
{
}