#ifndef TELSEND_GLOBAL_H
#define TELSEND_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(TELSEND_LIBRARY)
#  define TELSENDSHARED_EXPORT Q_DECL_EXPORT
#else
#  define TELSENDSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // TELSEND_GLOBAL_H
