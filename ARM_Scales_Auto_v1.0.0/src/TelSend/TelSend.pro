#-------------------------------------------------
#
# Project created by QtCreator 2020-05-15T06:10:51
#
#-------------------------------------------------

QT       += widgets

TARGET = TelSend
TEMPLATE = lib

DEFINES += TELSEND_LIBRARY

SOURCES += telsend.cpp

HEADERS += telsend.h\
        telsend_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
