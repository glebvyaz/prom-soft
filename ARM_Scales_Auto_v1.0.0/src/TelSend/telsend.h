#ifndef TELSEND_H
#define TELSEND_H

#include "telsend_global.h"

#include <QDialog>

class QUILIBSHARED_EXPORT QuiLib : public QDialog
{

public:
    explicit QuiLib(QWidget* parent = nullptr);
};


class TELSENDSHARED_EXPORT TelSend
{

public:
    TelSend();
};

#endif // TELSEND_H
