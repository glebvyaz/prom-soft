#include "stdafx.h"
#include "Mail.h"

QWaitCondition waitMailSending;
QMutex mailMutex;

Mail::Mail()
{


}

Mail::~Mail()
{

}

//
bool Mail::SendEmail( QString mailTo, QString subject, QString text, bool isHtml, QString htmlBody )
{
	QByteArray ba;
	bool result = false;

	QSettings settings( "configure.ini", QSettings::IniFormat );
	settings.setIniCodec("Windows-1251");

	QProcess *proccess = new QProcess( this ); 
	QStringList arguments;

	arguments << settings.value( "mail/host" ).toString()
	<< settings.value( "mail/port" ).toString()
	<< settings.value( "mail/login" ).toString()
	<< settings.value( "mail/password" ).toString()
	<< settings.value( "mail/ssl" ).toString()
	<< ( mailTo == ""  ? settings.value( "mail/mailToSend" ).toString() : mailTo )
	<< subject
	<< text
	<< ( isHtml == true ? "1" : "0" )
	<< htmlBody;

	proccess->start( "SmtpClientv1.exe",  arguments );
	if( proccess->waitForFinished() )
	{
	    ba = proccess->readAll();
	}

	delete proccess;

	if( ba.size() != 0 )
		result = true;

	return result;
}

// ���������� ��������� � ���������(��) ������� 
bool Mail::SendEmail( QString mailTo, QString subject, QString text, bool isHtml, QString htmlBody, QStringList listOfFiles )
{
	QByteArray ba;
	bool result = false;

	QSettings settings( "configure.ini", QSettings::IniFormat );
	settings.setIniCodec("Windows-1251");

	QProcess *proccess = new QProcess( this ); 
	QStringList arguments;

	arguments << settings.value( "mail/host" ).toString()
	<< settings.value( "mail/port" ).toString()
	<< settings.value( "mail/login" ).toString()
	<< settings.value( "mail/password" ).toString()
	<< settings.value( "mail/ssl" ).toString()
	<< ( mailTo == ""  ? settings.value( "mail/mailToSend" ).toString() : mailTo )
	<< subject
	<< text
	<< ( isHtml == true ? "1" : "0" )
	<< htmlBody
	<< ( listOfFiles.count() >= 1 ? listOfFiles.at( 0 ) : "" )
	<< ( listOfFiles.count() >= 2 ? listOfFiles.at( 1 ) : "" )
	<< ( listOfFiles.count() >= 3 ? listOfFiles.at( 2 ) : "" )
	<< ( listOfFiles.count() >= 4 ? listOfFiles.at( 3 ) : "" );

	proccess->start( "SmtpClientv1.exe",  arguments );
	if( proccess->waitForFinished() )
	{
	    ba = proccess->readAll();
	}

	delete proccess;

	if( ba.size() != 0 )
		result = true;

	return result;
}

// 
void MailThread::loadMailData_Slot( bool manual_send, QString host, int port, QString login, QString password, QString ssl, QString mailTo, 
	QString subject, QString text, bool isHtml, QString htmlBody, QStringList listOfFiles )
{
    this->host = host;
	this->port = port;
	this->login = login;
	this->password = password;
	this->ssl = ssl;
	this->mailTo = mailTo;
	this->subject = subject;
	this->text = text;
	this->isHtml = isHtml;
	this->htmlBody = htmlBody;
	this->listOfFiles = listOfFiles;
	this->manual_send = manual_send;

	waitMailSending.wakeOne();
}

// 
void MailThread::run()
{
	while( active )
	{
		bool result;
		QStringList arguments;
		QByteArray ba;

		mailMutex.lock();
		waitMailSending.wait( &mailMutex );


		arguments << host
			<< QString( "%1" ).arg( port )
		<< login
		<< password
		<< ssl
		<< mailTo
		<< subject
		<< text
		<< ( isHtml == true ? "1" : "0" )
		<< htmlBody
		<< ( listOfFiles.count() >= 1 ? listOfFiles.at( 0 ) : "" )
		<< ( listOfFiles.count() >= 2 ? listOfFiles.at( 1 ) : "" )
		<< ( listOfFiles.count() >= 3 ? listOfFiles.at( 2 ) : "" )
		<< ( listOfFiles.count() >= 4 ? listOfFiles.at( 3 ) : "" );

		QProcess *proccess = new QProcess( this ); 
		proccess->start( "SmtpClientv1.exe",  arguments );
		if( proccess->waitForFinished() )
		{
			ba = proccess->readAll();
		}

		delete proccess;

		if( ba.size() > 0 )
		{
			if( QString( ba.at( 0 ) ) == "1" )
			    result = true;
			else
				result = false;
		}
		else
			result = false;

		emit mailSendStatus_Signal( result, manual_send ); // ���������� ������ ���������

		mailMutex.unlock();
	}
}

// 
MailThread::MailThread()
{
	active = true;
}

// 
MailThread::~MailThread()
{
}