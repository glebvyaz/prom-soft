#ifndef LOGEVENTS_H
#define LOGEVENTS_H

#include "stdafx.h"

// �����  �����  ������� ������� 
class logEventsFormClass : public QObject
{
	Q_OBJECT

public:
    QDialog *logEventsFormWindow;
    QLabel *logoVIS;
	
	QGroupBox *sortGroupBox;
	
	QLabel *dateBeginLabel;
	QDateEdit *dateBeginEdit;
	QLabel *dateEndLabel;
	QDateEdit *dateEndEdit;

	QLabel *timeBeginLabel;
	QTimeEdit *timeBeginEdit;
	QLabel *timeEndLabel;
	QTimeEdit *timeEndEdit;
	// 
	QPushButton *acceptFilterButton; // ������ ���������� �������(��)
	QPushButton *resetFilterButton; // ������ ������ �������(��)
	QPushButton *closeButton;
	// 
	QTextEdit *logWindow; // ���� ����������� ����� 
	
// ������ 
public:
	void showWindow();
	void closeWindow();


public:
	logEventsFormClass::logEventsFormClass( QWidget *w, QObject *object );
	logEventsFormClass::~logEventsFormClass();
};

#endif