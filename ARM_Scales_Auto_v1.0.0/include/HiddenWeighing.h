#ifndef HIDDENWEIGHING_H
#define HIDDENWEIGHING_H

#include <QDialog>
#include "DB_Driver.h"

namespace Ui {
class HiddenWeighing;
}

class HiddenWeighing : public QDialog
{
    Q_OBJECT

public:
    explicit HiddenWeighing(QWidget *parent = 0);
    ~HiddenWeighing();

	QStringList tableHeaders;	
	int hidden_id;
	bool hiddenWeightEnable;

public slots:
	void detectOn_Slot( int weight );
	void detectOff_Slot();
	void reported_Slot();

signals:
	void makeHiddenPhotos();

private:
    Ui::HiddenWeighing *ui;

	QTextCodec *textCodec;

	enum State { ON, OFF };
	State state;
	int max_weights;
	int delayBeforeShoot;
		
	QVector<QLabel *> previewSnapshotsIn;

	bool eventFilter( QObject *object, QEvent *e );

private slots:
	void acceptFilter_Slot();
	void resetFilter_Slot();

	void photo_Slot();

	void shoot();
};

#endif // HIDDENWEIGHING_H
