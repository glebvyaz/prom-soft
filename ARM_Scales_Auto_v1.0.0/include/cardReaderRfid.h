#ifndef CARDREADERRFID_H
#define CARDREADERRFID_H

#include <QObject>
#include <QString>
#include <QtNetwork>

class cardReaderRfid : public QTcpSocket
{
	Q_OBJECT

private:
	//QSerialPort *serialPort;
	//QSerialPortInfo	*portInfo;

	// RFID 1
	QTcpSocket *tcpSocketCardReader_1;
	int nextBlockSize_1;
	// RFID 2
	QTcpSocket *tcpSocketCardReader_2;
	int nextBlockSize_2;
	// RFID 3
	QTcpSocket *tcpSocketCardReader_3;
	int nextBlockSize_3;
	
public:
	bool mode_active;
	cardReaderRfid(QObject *parent);
	~cardReaderRfid();
	void reopenRfid();

public slots:
	void data_rfid_received_Slot();
	// RFID 1
	void readCard_1_Slot();
	void slotError_1( QAbstractSocket::SocketError err );
	// RFID 2
	void readCard_2_Slot();
	void slotError_2( QAbstractSocket::SocketError err );
	// RFID 3
	void readCard_3_Slot();
	void slotError_3( QAbstractSocket::SocketError err );

signals:
	void sendCodeRfid_Signal( QString code );
	void sendCodeRfid_Make_Record_Signal( QString code );
	
};

#endif // CARDREADERRFID_H
