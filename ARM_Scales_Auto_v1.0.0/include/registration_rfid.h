#ifndef REGISTRATION_RFID_H
#define REGISTRATION_RFID_H

#include <QDialog>
#include <QTextCodec>
#include <QtableView>

namespace Ui {
class registration_rfid;
}

class registration_rfid: public QDialog
{
    Q_OBJECT

public:
	enum typeSpravochnik
	{
		OTPRAVITELI = 0,
		POLUCHATELI,
		PEREVOSCHIKI,
		PLATELSCHIKI,
		TYPE_GRUZ
	};
	enum typeOperation
	{
		INSERT = 0,
		UPDATE,
	};


public:
    explicit registration_rfid(QWidget *parent = 0);
    ~registration_rfid();
	void setupParamsAndSlots();
	QString getDateTimeOfRecord( QTableView *table, int row, int column );
	QString convertAutoNumToUpperCase( QString num_auto );
	void getRegistrationContent();
	QString getCurrentNumAuto();

public slots:
	void showForm();
	void registerCard(QString code);
	void closeForm();
    void addRecord_Slot();
	void modifyRecord_Slot();
	void removeRecord_Slot();
	void tabChanged_Slot( int );
	int getCurrentTab();
	void cellClicked_tableSpravochniki_Slot( const QModelIndex & );

	void getIndexes( int *row, int *column );
	void clearIndexes();
	void fillAcceptForm();
	void tabSprav_Gruzootpraviteli();
	void tabSprav_Gruzopoluchateli();
	void tabSprav_Perevoschiki();
	void tabSprav_Platelschiki();
	void tabSprav_TypesOfGoods();
	void acceptCodeRfid_Slot( QString code );
	void AutoTaraCheck(bool checked);
	void autoNumEnter_Slot(const QString &);

signals:
	void setSpravochnikiContent_Signal( QList< QString >, int );
	//void setTaraAutoCheck(bool checked);

private:
    Ui::registration_rfid *ui;
	QString NumAuto;
	QDialog *dlgRegister;
	QList< QString > getHeadersRegistration()
	{
		QTextCodec *textCodec = QTextCodec::codecForName("Windows-1251");

		QList< QString > list;
		list.append( textCodec->toUnicode( "����/�����" ) );
		list.append( textCodec->toUnicode( "ID �����" ) );
		list.append( textCodec->toUnicode( "���������" ) );
		list.append( textCodec->toUnicode( "��������" ) );
		list.append( textCodec->toUnicode( "����� ����������" ) );
		list.append( textCodec->toUnicode( "����� �������" ) );
		list.append( textCodec->toUnicode( "�.�.�. ��������" ) );
		list.append( textCodec->toUnicode( "���������� ����� �����" ) );
		list.append( textCodec->toUnicode( "����������" ) );
		list.append( textCodec->toUnicode( "���� �� �����������" ) );

		return list;
	}

	int m_current_tab;
	int row;
	int column;
	QTextCodec *textCodec;

	void addModifyRecords( bool save, QString code );

};

#endif // REGISTRATION_RFID_H