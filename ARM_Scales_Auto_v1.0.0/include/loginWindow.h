#ifndef LOGINWINDOW_H
#define LOGINWINDOW_H

#include"stdafx.h"
QT_USE_NAMESPACE

#define NAME_OF_ADMIN_USER "ADMIN"
#define PASSWORD_OF_SUPERVISOR_USER "Pr0mS0ftsu"

// ����� ���� ��� ����� � ��� 
class loginWindowClass : public QThread
{
    Q_OBJECT

public:
    enum
	{   USER = 0,
		ADMIN
	};

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// �������� ���� ��� ����������� 
public:
	QWidget *loginWin;
	QLabel *nameOfUser;
	QLabel *nameOfUserValidation;
	QComboBox *enterName;
	QLabel *password;
	QLabel *passwordOfUserValidation;
	QLineEdit *enterPassword;
	QPushButton *confirmButton;
	QPushButton *cancelButton;
	QPushButton *exitCrossNewUser;

	QString currentNameUser;
	QString currentPasswordUser;
    int role; // ���� ��� �����������  ( 0 - user, 1 - ����� )
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ������ 
public:
	void createWindow();
	void show();
	void hide();
	bool confirmLogin( QString nameUser, QString password );
	// bool eventFilter( QObject *object, QEvent *e ); // ������ ������� 
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ����������/�������� ������������ 
    QWidget *createAccountWin;
	QLabel *labelPassValidation;
	QLabel *user;
	QComboBox *name;
	QLabel *pass;
	QLineEdit *newPass;
	QLabel *reenterNewPass;
	QLabel *reenterNewPassValidation;
	QLineEdit *reenterNewPassword;
	QCheckBox *updateLogin;
	QLabel *labelUser;

	QPushButton *newUserConfirmButton;
	QPushButton *newUserCancelButton;
    QPushButton *deleteUserButton;

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ������ 
public slots:
	void showCreateAccountWin();
	void hideCreateAccountWin();
	bool confirmNewUserData();
    void deleteRecord();
	void hideLoginWindow();

	// �������� ������� � ��� 
    // void saveEventToLog( QString logEventData );

public:
	void addUser();
	bool removeUser( QString nameUser );
    bool createUserRecord( QString nameUser, QString password, int role ); // ������� ������� ������ 
    bool updateUserRecord( QString nameUser, QString password, int role ); // �������� ������� ������ 

	bool loadUsers();

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


public:
	loginWindowClass::loginWindowClass();
	loginWindowClass::~loginWindowClass();
};

#endif