#ifndef FORMTTN_H
#define FORMTTN_H

#include "stdafx.h"
#include <QWebView>

#define NDS_TTN 20

class Form_ttn : public QObject
{
	Q_OBJECT

public:
	QScrollArea *scrollArea;
	QDialog *dialogTTN;
	QWidget *ttnWidget;

public:
    QLabel *labelTitle;
    QLineEdit *enterNumberNakladnaya;
    QLabel *labelNum;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *enterDate;
    QLineEdit *enterMonth;
    QLabel *labelNum_2;
    QLineEdit *enterYear;
    QLabel *labelNum_3;
    QLabel *labelAuto;
    QLineEdit *enterAutoType;
    QLabel *labelAutoTypes;
    QLabel *labelAuto_2;
    QLabel *labelAutoTypes_2;
    QLineEdit *enterPricepType;
    QLabel *labelAuto_3;
    QLineEdit *enterTypePerevoski;
    QLabel *labelAutoTransporter;
    QLineEdit *enterAutoTransporter;
    QLabel *labelAutoVoditel;
    QLineEdit *enterNameTransporter;
    QLabel *labelAutoTransporterFIO;
    QLabel *labelAutoTransporterLicenceNumber;
    QLabel *labelOrderer;
    QLineEdit *enterEnterOrder;
    QLabel *labelAutoOrdererFIO;
    QLabel *labelSender;
    QLineEdit *enterSender;
    QLabel *labelAutoSenderFIO;
    QLabel *labelAccepter;
    QLabel *labelAutoAccepterFIO;
    QLineEdit *enterAccepter;
    QLabel *labelPointOfLoad;
    QLineEdit *enterPointOfLoading;
    QLabel *labelAddress1;
    QLabel *labelPointUnLoad;
    QLabel *labelAddress2;
    QLineEdit *enterPointOf_UnLoading;
    QLabel *labelPereaddress;
    QLineEdit *enterPereaddres;
    QLabel *labelNamePereAddr;
    QLabel *labelDoverenost;
    QLineEdit *enterSeriaNumber;
    QLabel *labelNumber;
    QLineEdit *enterNumberDoverennost;
    QLabel *labelNumber_2;
    QLineEdit *enterDoverennostDate;
    QLineEdit *enterDoverennostMonth;
    QLineEdit *enterDoverennostYear;
    QLabel *labelNum_4;
    QLabel *labelNum_5;
    QLineEdit *enterRules;
    QLabel *labelVantash;
    QLineEdit *enterGoodsState;
    QLabel *labelNum_6;
    QLabel *labelRulsTransport;
    QLineEdit *enterDoverennostVidano;
    QLabel *labelNumberPlaces;
    QLineEdit *enterNetto;
    QLabel *labelBuWords;
    QLabel *labelBrutto;
    QLineEdit *enterBrutto;
    QLineEdit *enterEcpeditor;
    QLabel *labelFIO_Excpeditor;
    QLabel *labelBuWords_2;
    QLabel *labelBrutto_2;
    QLabel *labelBuhgalterName;
    QLineEdit *enterBuhgalterName;
    QLabel *labelFIO_Buhgalter;
    QLabel *labelBuhgalterName_2;
    QLineEdit *enterBuhgalterName_2;
    QLabel *labelFIO_Buhgalter_2;
    QLabel *labelTotalSold;
    QLineEdit *enterTotalSold;
    QLabel *labelPDV;
    QLineEdit *enterPDV;
    QLabel *labelSuprovodniDoc;
    QLineEdit *enterSuprovodniDoc;
    QLabel *labelTransportServices;
    QLineEdit *enterTransportServices;
    QLineEdit *enterOthers;
    QPushButton *pushButtonAccept;
    QPushButton *pushButtonClose;
    QPushButton *pushButtonPrint;
    QLabel *label_3;
    QTableWidget *tableWidget;

	QPushButton *pushButtonTTN_2;
	QDialog *dialogTTN_2;
	QPushButton *buttonSave;
	QPushButton *buttonClose;

	// 
	QLineEdit *numPoint;
	QLineEdit *textOfTTN_2;
	QLineEdit *edIsm;
	QLineEdit *numPlaces;
	QLineEdit *priceBezPDV;
	QLineEdit *zagalnaSummaS_PDV;
	QLineEdit *vidPakuvannya;
	QLineEdit *docsWithLoad;
	QLineEdit *massaBrutto;

	QString dateTimeTTN;
	// 
	QDialog *dialogTTN_3;
	QPushButton *pushButtonTTN_3;
	QPushButton *buttonSave2;
	QPushButton *buttonClose2;

	QTableWidget *tableWidget2;

	QLineEdit *operation;
	QLineEdit *massaBrutto2;
	QLineEdit *pributtya;
	QLineEdit *vibuttya;
	QLineEdit *prostoyu;
	QLineEdit *pidpisVidpovidalnOsobi;

    QLineEdit *operation_2;
	QLineEdit *massaBrutto2_2;
	QLineEdit *pributtya_2;
	QLineEdit *vibuttya_2;
	QLineEdit *prostoyu_2;
	QLineEdit *pidpisVidpovidalnOsobi_2;

	QLineEdit *enterVidpovidalnaOsoba;
	QLineEdit *enterPriynyavVodiyExpeditor;
	QLineEdit *enterZdavVodiyExpeditor;
	QLineEdit *enterPriynyavVidpovidalnaOsoba;

	QVector< QVector< QString > > tableConvertIntToStringUKR;

	/*{
		{ "", "����", "��i", "���", "������", "�`���", "�i���", "�i��", "�i�i��", "���`���" },
        { "", "������", "��������", "��������", "�����", "�`���������", "�i���������", "�i��������", "�i�i��������", "���`������" },
        { "", "���", "��i��i", "������", "���������", "�`������", "�i������", "�i�����", "�i�i�����", "���`������" },
        { "", "����", "��i", "���", "������", "�`���", "�i���", "�i��", "�i�i��", "���`���" },
        { "", "������", "��������", "��������", "�����", "�`���������", "�i���������", "�i��������", "�i�i��������", "���`������" },
        { "", "���", "��i��i", "������", "���������", "�`������", "�i������", "�i�����", "�i�i�����", "���`������" },
        { "",  "�i�����", "��� �i������", "��� �i������", "������ �i������", "�`��� �i�����i�", "�i��� �i�����i�", "�i�� �i�����i�", "�i�i�� �i�����i�", "���`��� �i�����i�" },
	};*/

public:
	void setupUi( QWidget *Form, QObject *object );
	void retranslateUi( QWidget *Form );
	void showTTN();
    void saveDataFromTTN_Form( QString nakladnaya, QString date_time, QString date_time_send, QString auto_number, QString pricep_number, QString transporter, QString orderer_name, 
	QString sender_info, QString accepter_info, int netto, int brutto, double total_price, QString goods ); // ������� ������ �� �����  �  ��������  �� � �� ��� 

    void loadDataToTTN_Form( QString date ); // ��������� ������  �  �����  ���  ��  �� 
    void updateDataFromTTN_Form( QString date_time ); // �������� ������ �� ����� � �� ��� 

    QList<QString> getHeadersTTN();
	QList<QString> getHeadersTTN_3();

	QString generateLetterPrice( double price );

public slots:
	void hideTTN();
    void pushButtonTTN_2_Slot();
	void saveTTN_2();
	void closeTTN_2();
	void pushButtonPrint_Slot();

	void pushButtonTTN_3_Slot();
	void saveTTN_3();
	void closeTTN_3();


public:
    Form_ttn( QObject *object );
	~Form_ttn();
};

#endif