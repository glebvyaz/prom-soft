#include <QAbstractTableModel>

class RegistrationModel : public QAbstractTableModel
{
    Q_OBJECT

public:
	enum Colors
	{
		GRAY = 0,
		WHITE,
		BROWN,
		GREEN,
		BLUE

	};

public:
	RegistrationModel( int rows, int columns, QList<QString > headers_names, QObject *parent = 0 );
	RegistrationModel( QObject *parent = 0 );

	void addParams( int rows, int columns, QList<QString > headers_names );
	int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
	Qt::ItemFlags flags(const QModelIndex &index) const;

    // �������� ������ 
    void clear();
	// �������� ������ ������ 
    void update( QVector< QString > *data );

	// �������� � ������ ��������  ��������� ����� � ������ 
    void setColoredLinesSign( int colored_row );
	// �������� ������ ���������  ��������� ����� � ������ 
    void clearColoredLinesSign();

private:
	QTextCodec *textCodec;
	QList< QVector< QString > > m_data;
	int m_rowsCount;
	int m_columnCount;
	QList<QString > headers_names;
	QVector< int > rows_colored;
};
