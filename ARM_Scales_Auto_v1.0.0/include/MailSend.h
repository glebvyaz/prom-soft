#ifndef MAILSEND_H
#define MAILSEND_H

#include "SmtpMime"

class MailSend : public QObject
{
	Q_OBJECT

public:

	QString m_subject;
	QString m_FilePath;
	bool m_autosend;


	explicit MailSend(QString subject, QString filePath = "", bool autosend = false, QObject *parent = 0) : m_subject(subject), m_FilePath(filePath), m_autosend(autosend) {};
	~MailSend() {};

	EmailAddress* stringToEmail(const QString &str);
	bool SendMail( const QString filePath, bool showStatus1 = true  );
	
};
#endif //MAILSEND_H