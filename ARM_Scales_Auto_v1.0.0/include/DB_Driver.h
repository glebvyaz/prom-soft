#ifndef DBDRIVER_H
#define DBDRIVER_H

#include"stdafx.h"
#include <QtSql>
#include "taraDictionaryModel.h"
#include "Models/reportsTableModel.h"
#include "Models/registrationModel.h"
#include "Models/mainTableModel.h"

#define dataBaseName QString( "Promsoft_auto" )
//#define dataBaseName QString( "Promsoft_auto_RDB" )

#define NUM_ATTEMPTS_MYSQL_CONN 2

// ���� ������ � ������� �� �� 
//  ������ � �� MySQL (5.1)
class mysqlProccess : public QObject
{
	 Q_OBJECT

    taraDictionaryModel *taraDictionaryModel_obj;

public:
// ������� � �� 
enum typesTables
{
    MAIN_TABLE = 0,
	REPORT_TABLE,
	TOTAL
};

// Members

//=========
private:
	static mysqlProccess* m_Instance;
	QSqlDatabase *m_sqlDatabase;
	QSqlDatabase *mySqlDBRemout; // ������ ��� ����������� ��������� � �� 

	QString m_conn_name;

	bool removeRecordsPhotos(int id);

public:
	MainTableModel *mainTableModel_Obj;
	RegistrationModel *registration_Obj;
	ReportsTableModel *reportsTableModel_Obj;
	QPushButton *buttonPhotoShow; // ������ ��������� ����������� ���������� 
	QPushButton *buttonPhotoSave; // ������ ������ ����������� ���������� �� ���� �� HDD
public:
	
	static mysqlProccess* instance();

	// �������� �������������� � �� MySQL 
    bool isConnectable();
	void createMySQL_DataBase();
	bool connect( QSqlDatabase *sqlDatabase, QString db_name );
    bool connect();
    bool isConnected();
	QSqlDatabase *getQSqlInstance();

	//��������� ��
	QSqlDatabase *connectDBR();


	void disconnect( QSqlDatabase *sqlDatabase, QString db_name );
	void disconnect();
	void drop();
	//=========

	//QSqlDatabase *connect();

	//QSqlDatabase *mySqlDatabase; // ������ ��� �����������  � �� 
	QSqlTableModel *autoReceptionSqlTableModel; // ����������� ������ ��� ������ ������ ������ 
	QTableView *tableView;

	QSqlTableModel* getHiddenWeighingTableModel( QStringList headers, QStringList filters );
	int addHiddenWeighing( int weight );
	void updHiddenWeighing( bool registered );
	void updHiddenWeighing( int );


// Methods 
public:
    // ������� �� 
    //bool createMySQL_DataBase( QSqlDatabase *sqlDatabase );
    // ������������ � ��
    // ���������� true � ������ ������ 
    bool connectToDB_MySql( QSqlDatabase *sqlDatabase, QString db_name ); // ����������� � �� 
	void disconnectFromDB_MySql( QSqlDatabase *sqlDatabase, QString db_name ); // ���������� �� �� 
	void CopyOnServer(); //���������� �� ������(��������� ��)
    void saveEventToLog( QString logEventData ); // �������� ������� � ��� 
    QString loadEventFromLog( QDateTime dateTimeStart, QDateTime dateTimeEnd ); // ��������� ������� �� ��� 
    bool getDataForHourlyReports( QString last_dateTime, QString now_dateTime, typesTables table, QVector< QList< QString > > *data, bool bSaveEachRec ); // ������� ������ �� �� ��� ������������ ������ �� ������ ������� 
	int getLastNumNakladnaya();
	QList< QString > getNakladnayaRecord( QString date_time, int num_table );
	void getPhotosbyIdAndSave(QList<int> id_list, QStringList fileNames, bool autosend = false);
	void getLastRecForSend(int &idf, QString &fileName);

	// ����������� 
	bool getTaraAuto( QList< QString > headers_names, QTableView *tableWidget );
	double getTaraAuto( QString numAuto );

	bool insertTaraAuto( QString number_auto, QString mark_auto, float weight_tara );
	bool updateTaraAuto( QString dateTime, QString number_auto, QString mark_auto, float weight_tara );
	bool deleteTaraAuto( QString dateTime );

	QVector<int> getRealBruttoAndTara( QString accept_time );

	//�����----------

	//�����������
	bool saveToRegisterTable( QString code, QString supplier, QString material, QString num_auto, QString num_prizep, QString fio_driver, QString num_card_poryadk, bool auto_tara );
	bool updateRegisterTable( QString date_time, QString code, QString supplier, QString material, QString num_auto, QString num_prizep, QString fio_driver, QString num_card_poryadk, bool auto_tara);
	//-------------
	int getIdfForRecord( QString dateTime );
	int getIdfInTableForPhoto( QString date_time );


	//������ � ��.
	// �������� �������� ��  ������� ������ ���� 
	bool getMainTableData(QList< QString > headers_names, QTableView *tableWidget, QProgressBar *progrBar, QList< QString > filters); // �������� �������� ��  �������� ������� ������
																																	  // �������� �������� ��  ������� �������
	bool getReportsTableData(QList< QString > headers_names, QTableView *tableWidget, QProgressBar *progrBar, QList< QString > filters);

	bool saveMainTableData(QString dateTime, struct Record & record, int idf);
	bool saveReportsTableData(struct Record & record, int idf);


	bool hasRecordMainTable(QList< QString > & data, QString code);
	int saveRecordsPhotos(QString dateTime);
	bool updateRecordsPhotos(int id, int num_cam, QByteArray photo, int num_measure);
	QVector< QByteArray > getSavedPhoto(int id);
	// ������� ������ �� �� ��� ������������ ������ �� ������ ������� 
	bool getDataForHourlyCardsReports(QString last_dateTime, QString now_dateTime, QVector< QList< QString > > *data);

	// �������� ������������������ ������ �� ������������ 
    // ����� ������������������ ����� � ����������� 
    QList< QString > getRegistrationData( QString code );
	bool getRegistrationData( QList< QString > headers_names, QTableView *tableWidget, QList< QString > filters );
	QList< QString > getRegisteredDataToFilterControls( int type );
	bool removeRecordFromSpravochniki( QString dateTime );
	bool hasRfidCode( QString code );
	bool hasNumAuto( QString code );
	bool hasAutoNumberOrCode( QString code, QString num_auto );
	
	//-----------------

	//trial
	bool read_trial();//��������� 0��� ��� ������ ������
	bool check_trial();//���������� 3 ���� � �������� �� ������ ������
	bool add_trial();//�������� 3 � � ���������� ����
	bool timer_trial();//�������� 1 � � ���������� 15 ���
	QString set_time_leable();

public:
	mysqlProccess();
	~mysqlProccess();
private slots:
	void showPhotoReports();
	void savePhotoReports();
signals:
	void showPhotoReports_Signal();
	void savePhotoReports_Signal();
public slots:
	void writeSensors_Slot( QVector<int>, QVector<int> );
	void stable_Slot();
};

#endif