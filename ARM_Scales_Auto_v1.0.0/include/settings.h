#ifndef SETTINGS_H
#define SETTINGS_H

#include "stdafx.h"
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QtNetwork>
#include "Crypt.h"

QT_USE_NAMESPACE

class currentSettingsCOM_Port; // ��������� ����� ������� �������� COM-����� 
class currentSettingsIP_Cam; // ��������� ����� �������� IP-������ 

class SettingsForm : public QDialog
{
	Q_OBJECT

protected:
	bool  eventFilter( QObject *object, QEvent *event );
public:
	QDialog *dialogSettings;

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// �������� ��� �������� ����� ���-� � COM-������ 
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
QWidget *scalesN; // ������� ����� ��� ������� N

// ComboBox ��� �������� ����� �� � COM-����� 
public:
	int traff_light;	// 1 �������, 2 �������
	int current_light;

	QWidget *settAnaliticsTab;
	QLineEdit *enter_sInterval; // �������� ������, ���
	QLineEdit *enter_sNumber;	// ���-�� ������������ ��������
	QLineEdit *enter_sInfoContragent;	// ���������� ��� ����� ���������
	QLineEdit *enter_sInfoWType;
	QPushButton* buttonForSaveSettingsAnalit;

	QLabel *typesVPLabel;
	QComboBox *typesVPList;
	QLabel *comPortsNamesLabel;
	QComboBox *comPortsList;
	QLabel *baudRatesNamesList;
	QComboBox *baudRatesList;
	QLabel *endOfFrameLabel; // ����� ������� 
	QComboBox *endOfFrame;
	QCheckBox *chkBoxBK_Work; // ��������� ������ � ������ ����� ��
	QCheckBox *chkBoxRM_Work; // ��������� ������ � ������ ����� ���������� 
	QCheckBox *chkBoxServer_AutoStart; //  ���������������  �����  ��������  ������� 

	QTabWidget *tabNumScalesSettings; // ������� ����� � ������� �� ������� 

	QVector<QString> *numberOfEntityScales; // ����� ������� ����� 

	QGroupBox *group_Sornost;
	QDoubleSpinBox *spinKoefSor;
	QCheckBox *checkSorWeightShow;
	QCheckBox *checkSorDiscret;
	QCheckBox *checkSorUseOnlyBrutto;

	QGroupBox *group_TCPWeight;
	QLineEdit *lineEditTCP_IP;

	QGroupBox* group_PositionS2;
	QLineEdit* lineEdit_PositionS2;
	QCheckBox* check_PositionS2;
	QCheckBox* check_PositionUseS1;
	QCheckBox* check_PositionUse4ch;
	QCheckBox* check_PositionIgnore;
	


	QGroupBox* group_SvetoforS2;
	QLineEdit* lineEdit_SvetoforS2;

	QGroupBox* group_BarrierS2;
	QLineEdit* lineEdit_BarrierS2;

	QCheckBox* check_HorizontalCams;
	// 
	QGroupBox *groupBK_Params; //  ��������������� ��������� ��� ��
    QLabel *typeVP_ForBKLabel; // 
	QComboBox *typeVP_ForBKComboBox; // 

    QLabel *filterInputsBKLabel; // 
	QLineEdit *filterInputsBK_LineEdit; // ���� �������� �������� ���������������� 

	QLabel *minVesBKLabel; // 
	QLineEdit *minVesBK_LineEdit; // ���� ������������ ���� ������ ���������� 

    QLabel *diskret_ForBKLabel; // 
	QComboBox *diskret_ForBKComboBox; // ������� ����������� ���� � ��

	QLabel *sendCommand_ForBKLabel; // 
	QLineEdit *sendCommand_ForBKComboBox; // ���� �����  �������� ��� �������� ��������� 
	QPushButton *sendCommandButton; // ������ ������� �������� � ��
	// QCommandLinkButton *sendCommandButton;
	// 
	QLabel *rcvCommand_ForBKLabel;
	QTextEdit *rcvCommand_ForBKComboBox;

	
    // 
	QPushButton *buttonForSaveSettings; // ������ ����������  ���������
	QPushButton *buttonExitSettings; // ������ ����� �� ���� ��������� ��������� 

	QPushButton *buttonSaveDevicesSettings;

	QPushButton *buttonStartServer; // ������ ������ ������ ������� 
	QPushButton *buttonStopServer; // ������ ��������� ������ ������� 

	QCheckBox *enableScalesEntity; // ��������� ������ ���������� ����� 

	QCheckBox *trafficLightCheckBox; // �������� �������� ���������
	QCheckBox *sensorsCheckBox;
	QCheckBox *sensorsInversionCheckBox;
	
	QSpinBox *spinBoxAddrVP;
	
	QLabel *labelMuxKoeff;
	QLineEdit *enterMuxKoeff;

	// e-mail
    QWidget *eMailSettings;
	QLabel *labelHostMail;
	QLineEdit *enterHostName;
	QLabel *labelPortMail;
	QLineEdit *enterPort;
    QLabel *labelLoginEmail;
	QLineEdit *enterLoginEmail;
    QLabel *labelPasswordEmail;
	QLineEdit *enterPassword;
    QLabel *labelMailTo;
	QLineEdit *enterMailTo;
	QCheckBox *checkBox_ssl;
	QCheckBox *checkBoxEnableMailSend;
	QCheckBox *checkBox_Adr_1;
	QLineEdit *enterAdress_1;
	QCheckBox *checkBox_Adr_2;
	QLineEdit *enterAdress_2;
	QCheckBox *checkBox_Adr_3;
	QLineEdit *enterAdress_3;
	QCheckBox *checkBoxPhotoSaveAll;
	QCheckBox *checkBoxSendEachRec;

	QCheckBox *checkBoxUSB_En;

	QLineEdit *enterIP_UDP_Settings;
	QLineEdit *enterSrcPort_UDP_Settings;
	QLineEdit *enterDstPort_UDP_Settings;

	QLineEdit *enterExcelDateTimeFormat;

	QLabel *labelSelectInterval;
	QComboBox *selectIntervalSend;

	QPushButton *buttonSaveEmailSettings;

	// devices
	QWidget *devicesSettings;

	QLabel *labelVk1;	
	QLineEdit *enterVk1;
	QCheckBox *chkboxEnableVk1;
	QLineEdit *enterVk1Name1;
	QLineEdit *enterVk1Name2;

	QLabel *labelVk2;
	QLineEdit *enterVk2;
	QCheckBox *chkboxEnableVk2;
	QLineEdit *enterVk2Name1;
	QLineEdit *enterVk2Name2;

	QLabel *labelVk3;
	QLineEdit *enterVk3;
	QCheckBox *chkboxEnableVk3;
	QLineEdit *enterVk3Name1;
	QLineEdit *enterVk3Name2;

	//Rfid
	QGroupBox *group_Rfid;
	QLabel *labelRfid1;
	QLineEdit *enterRfid1;
	QCheckBox *chkboxEnableRfid1;
	QLineEdit *lineEdit_Lamps;

	QGroupBox *groupTraffLight1;
	QComboBox *selectComPortTablo1;
	QSerialPort *serialPortTablo1;	
	QRadioButton* radioTablo_COM1;
	QRadioButton* radioTablo_IP1;
	QLineEdit* enterTablo_IP1;

	QGroupBox *groupTraffLight2;
	QComboBox *selectComPortTablo2;
	QSerialPort *serialPortTablo2;	
	QRadioButton* radioTablo_COM2;
	QRadioButton* radioTablo_IP2;
	QLineEdit* enterTablo_IP2;


	QLineEdit *enterTimerTablo;
	QLineEdit *enterMinWeightTablo;

	QLineEdit *enterMySQL_address;
	QLineEdit *enterMySQL_port;
	QLineEdit *enterMySQL_login;
	QLineEdit *enterMySQL_password;

public:
// ������ ����� ��������  ��
	QVector<QComboBox* > vectorTypesVP_main;
// ������ QComboBox-�� COM-������ 
	QVector<QComboBox* > vectorComPortsVP;
// ������ QSpinBox-�� ������� �� 
	QVector<QSpinBox* > vectorAddrVP;
// ������ QSpinBox-�� ������� ��
	QVector< QLineEdit* > vectorMuxKoef;

// ������ QComboBox-�� ��������� COM-������ 
	QVector<QComboBox* > vectorBaudsVP;
// ������ QComboBox-�� ��������� ������� ������ 
	QVector<QComboBox* > vectorEOFrameVP;
// ������ QCheckBox-�� ���� ������ ����� �����  �� 
	QVector<QCheckBox* > vectorWorkWithBK;
// ������ QCheckBox-�� ���� ������ ����� �����  �� 
	QVector<QCheckBox* > vectorWorkWithRM;

// ������ QGroupBox-�� ��� ������ ����� �� 
	QVector<QGroupBox* > vectorGrouBoxWorkWithBK;
// ������ QCheckBox-�� ���� ����������  ������ ����� 
	QVector<QCheckBox* > vectorEnableScalesEntity;

	QVector<QCheckBox* > vectorcheckBoxUSB_En;

	QVector<QGroupBox* >   vectorGroupUDP_Settings;
	QVector<QLineEdit* >   vectorEnterIP_UDP_Settings;
	QVector<QLineEdit* >   vectorEnterSrcPort_UDP_Settings;
	QVector<QLineEdit* >   vectorEnterDstPort_UDP_Settings;


	void update_SerialPortsList( QObject *serPortClassObject, QObject *control, QString currentElement,  QList< QString > *serPortsList );
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ��������� ��-01 
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ������ ����� ��������  ��
	QVector<QComboBox* > vectorTypesVP_BK;
// ������ �������  textBox - ��,  ���  �������� ������� ��-01
	QVector<QLineEdit* > vectorInputDelay_BK;
// ������ �������  textBox - ��,  ���  ������������ ���� ������ �� ����  ���  ��-01
	// QVector<QLineEdit* > vectorMinVes_BK;
	QMultiHash<QString, int> hashTypesVP_RequestNum; // Hash-������� ������� ����� ����� � ���� ��������, ����������������� ������ � ������ �����
    // ������� � ComboBox ��������� �������  ������ �� COM-����� 
	QList<QString> getEndsOfFrames();
	// ������� � ComboBox ��������� �������  ������ �� COM-����� 
    QList<QByteArray> getEndsOfFramesData();
	QHash<QString, QByteArray> hashTypesEndsOfFrames; // Hash-������� ������� ����� ����� � ���� ��������, ����������������� ������ � ������ ����� 

	bool getCurrentScales( int num_scales );

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ��������� ��� �� �������� IP-����� 
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
public:
	QWidget *IP_CamTab_N; // ������� ��� IP-����� 
	QTabWidget *tabNumIP_Cam_Settings; // ������� IP-������ � ������� �� ������� 

	QGroupBox *groubBoxIP_Cams; // ��������� �������� IP-����� 

	QLabel *labelIP_Camera; // ������� ����� IP-������ 
	QLineEdit *enterIP_Address; // ���� IP-������ 

	QLabel *labelPort_Camera; // ������� ����� ����� ��� IP-������ 
	QLineEdit *enterPort_Address; // ���� ����� ��� IP-������ 

	QCheckBox *chkboxEnableIP_Cam; // ������� ���������� ������ IP-������ 
	QCheckBox *checkBoxEnablePhotoFix; // ������� ���������� ������������ 
	QCheckBox *checkBoxEnablePhotoFixPreview; // ������� ���������� snapshot - �� ��� ������������ 
    QCheckBox *checkBoxEnableRecognitionAutoNumber; // ������� ���������� ������������� ������ 

	QPushButton *buttonStartIP_CamQuery; // ������ ����� IP-������ 

    QLabel *labelPreviewImageFromCam; // ������������ ����������� �� IP-������ 
	QFrame *framePreviewImageFromCam; // ����� ��� ������������� ����������� �� IP-������ 
	QLabel *imagePreviewImageFromCam; // ����������� ���������������� ��������� �� IP-������ 

	QPushButton *buttonRecognizeAutoNumber; // ������ ��������� ������������� ������ ���������� 
	QLineEdit *recognizedAutoNumber; // ���� ��� ������������� ������ ���������� 
    QLabel *labelRecognizedNumber; // �������  "������������ �����"

	QPushButton *buttonSaveSettingsIP_Cams; // ������ ���������� �������� IP-������ 

	QLabel *labelLogin; // ������� "���"
	QLineEdit *enterLogin; // ���� ����� ��� ����������� � IP-������� 
    QLabel *labelPassword; // ������� "������"
	QLineEdit *enterPass; // ���� ������ ��� ����������� � IP-������� 

	// ������� ��� ���������� �������� ���������� IP-����� 
	QVector<QLineEdit *> vectorEnterIP_Address; // ������ ���� ����� IP-������� ��� IP-������ 
	QVector<QLineEdit *> vectorEnterPort_Address; // ������ ���� ����� ����� ��� IP-������ 

	QVector<QCheckBox *> vectorChkboxEnableIP_Cam; // ������ ��������� ��� ���������� ������ IP-������ 
	QVector<QCheckBox *> vectorCheckBoxEnablePhotoFix; // ������ ��������� ���������� ������������ 
	QVector<QCheckBox *> vectorCheckBoxEnablePhotoFixPreview; // vector checkboxes ��� ���������� snapshot - �� ��� ������������ 
	QVector<QCheckBox *> vectorCheckBoxEnableRecognitionAutoNumber; // ������ ��������� ���������� ������������� ������ 

	QVector<QPushButton *> vectorButtonStartIP_CamQuery; // ������ ������ ��� ������ ������ IP-����� 

	QVector<QFrame *> vectorFramePreviewImageFromCam; // ������ ����� ��� ������������� ����������� �� IP-������ 
	QVector< QLabel *> vectorImagePreviewImageFromCam; // ������ ����������� ��� ���������������� ��������� �� IP-������ 

	QVector<QPushButton *> vectorButtonRecognizeAutoNumber; // ������ ������ ��������� ������������� ������ ���������� 
	QVector<QLineEdit *> vectorRecognizedAutoNumber; // ������ ����� ��� ������������� ������ ���������� 

    QVector<QLineEdit *> vectorEnterLogin; // ���� ����� ��� ����������� � IP-������� 
	QVector<QLineEdit *> vectorEnterPass; // ���� ������ ��� ����������� � IP-�������

	QVector<QString> *numberOfEntityIP_Cams; // ��� ������� ������� IP-������ 

	QCheckBox *checkBoxCoord1;
	QCheckBox *checkBoxCoord2;
	QCheckBox *checkBoxCoord3;
	QCheckBox *checkBoxCoord4;

// ��������� ���� �������� ��
public:
	QList<QString> loadTypesOfVP();
	QList<QString> loadTypesOfVP_BK(); // ��������� ���� �������� �� ��� ������ � �� 
    QList<QString> loadDiscretesVP(); // ��������� �������� ����������� ���� 
	QList<QString> loadVP_REquests(); // ��������� ���� �������� ���� ��� �� �� ��� 
    QList<int> load_BK_VP_REquests(); // ��������� ���� ��������  ��  ��-01  �  ��-xx
	QList<QString> load_list_Scales(); // ��������� �������� � ���������� ������ ����� 
    int getNumVP(); // ������� ���������� ������������� ����� 
	// ������� ���������� IP-����� � ������� 
	int getNumIP_Cams();
    void displayServerState( QPushButton *button, bool state ); // ���������� ��������� ������� (��������� ��� ����������)
	// 
	QTabWidget *settingsTabs; // ������� ��� �������� ���������� ���-�
	QWidget *settComPortWindow; // ���� �������� COM-����� 
	QWidget *tcpIpConnServerWindow; // ���� �������� TCP/IP ���������� 
	QWidget *IpCamsSettings; // ���� �������� IP-����� 

	int getNumPosStampPhoto();
	
signals:
	void StartTimerWeight();

public slots:
	void buttonSaveEmailSettings_Slot();
	void buttonSaveDevicesSettings_Slot();
	void saveSens_Slot();
	void buttonSaveMySQL_Slot();

	void checkBoxCoord1_Slot( int );
	void checkBoxCoord2_Slot( int );
	void checkBoxCoord3_Slot( int );
	void checkBoxCoord4_Slot( int );

	void setEndOfFrame_Slot(int);

public:
	int currentScales;
public:
	SettingsForm();
	~SettingsForm();

public:
	void setupSettingsForm( QList<QString> serPorts, QList<QString> bauds, QObject *obj );
    void showSettingsDialog(); // ���������� ����������  ����  ��������
    void hideSettingsDialog(); // ������� ����������  ����  �������� 

public:
	void setServerFlagActivity( bool state ); // ���������� ��������� ������� 
	bool currentStateOfServer;
	bool getServerFlagActivity(); // ������� ��������� ������� 
    bool initialised_bk01; // ���� ������������� ��-01

public:
	void getCurrentSettingsCOM_PortIniFile( currentSettingsCOM_Port *currSettingsComPortPtr, int num_scales ); // ��������� ������� ��������� COM-������  ��  Ini-����� 

public:
	QSerialPort *serialPort;
	QList<QSerialPort* > serPortObjectsList; // ��������� ������ ��������  COM - ������ 

// 
// ��������� ������� ��������� IP-����� ��  Ini-����� 
public:
    void getCurrentSettings_IP_CamsIniFile( currentSettingsIP_Cam *currentSettingsIP_CamPtr, int num_cam );
	QVector< currentSettingsIP_Cam* > vector_ip_cams_settings;
	void Load_IP_CAMs_Settings(); // ��������� ������ �������� ��� IP-����� 
	bool CompareSettings_IP_CAMs( QVector< QString > *log_messages_vec ); // ��������� ������������  ��������� �������� ������������� 

};

// ����� �������� COM-����� 
class currentSettingsCOM_Port
{
    public: QString type_scales;
    public: QString com;
	public: QString baud;
	public: QString endoframe;
	public: bool bk;
    public: bool rm;
	public: QString bk_type_scales;
	public: int input_delay;
	public: int ves_min;
	public: bool scales_enable;

public:
	void reset() // ��������  ���� ������� 
	{
		type_scales.clear();
		baud.clear();
		com.clear();
		baud.clear();
		endoframe.clear();
	    bk = false;
        rm = false;
		bk_type_scales.clear();
	    input_delay = 0;
	    ves_min = 0;
	    scales_enable = false;
	}
};

// ����� �������� IP-������ 
class currentSettingsIP_Cam
{
    public: QString ip_address;
    public: QString port;
	public: bool cam_en;
    public: bool cam_photofix_en;
    public: bool cam_photofix_preview_en;
    public: bool cam_recogn_en;
    public: QString login;
    public: QString password;

public:
	void reset() // ��������  ���� ������� 
	{
		ip_address.clear();
		port.clear();
        cam_en = false;
        cam_photofix_en = false;
		cam_photofix_preview_en = false;
        cam_recogn_en = false;
		login.clear();
		password.clear();
	}
};

#endif