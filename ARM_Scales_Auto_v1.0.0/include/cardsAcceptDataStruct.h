#include <QString>

struct Record
{
	QString dateTimeAccept;
	QString dateTimeSend;

	QString code;
	double weight_1;
	double weight_2;
	double netto;
	QString supplier;
	QString material;
	QString num_auto;
	QString num_prizep;
	QString fio_driver;
	QString num_card_poryadk;
	int blocked;
};

//extern struct Record record;

struct Photofix_data
{
	int id;
	int num_measure;
};

//extern struct Photofix_data photofix_struct;

struct File_data
{
	QString num_auto;
	QString sender;
	QString accepter;
	QString transporter;
	QString goods;
	double brutto;
	double tara;
	double netto;
	QString dateTimeAccept;
	QString user_accept;
	QString dateTimeSend;
	QString user_send;
	QString nakladnaya;
	QString num_prizep;
	bool enter_tara;
};

//extern struct File_data file_struct;

enum StatesWeight
{
	IDLE = 0,
	AUTO_ON_PLATFORM,
	RFID_ACTIVATED,
	FIN_WEIGHT
};

