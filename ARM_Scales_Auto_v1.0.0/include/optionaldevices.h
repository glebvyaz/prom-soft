#ifndef OPTIONALDEVICES_H
#define OPTIONALDEVICES_H

#include "stdafx.h"
#include <QDialog>

namespace Ui {
class optionalDevices;
}

class optionalDevices : public QDialog
{
    Q_OBJECT

public:
    explicit optionalDevices(QWidget *parent = 0);
    ~optionalDevices();

	void setSocket2( int num, int relay, bool setTo );
	void setLink( int num, bool setTo );

	bool getRelayState( int s, int r );

private:
    Ui::optionalDevices *ui;

public slots:
	void radioClick_Slot();

signals:
	void radioClicked_Signal( int, int, int ); // num, relay, set
};

#endif // OPTIONALDEVICES_H
