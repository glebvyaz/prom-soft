#ifndef SERIALPORT_H
#define SERIALPORT_H

#include "stdafx.h"
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

#define DEFAULT_UART_BAUD 6 // ����� �������� = 9600 � ������ ����������� ���������

class serialPortClass : public QObject
{
	Q_OBJECT

protected:
	bool  eventFilter( QObject *object, QEvent *event ); // ������ ������� ������ (���� ������ � �.�.)

public:
	void initComPortProperties(); //  ���������� �������� COM-�����

//-----------------------------------
// SerialPort Settings
//-----------------------------------
// ���������� � ������ � ������� 
public:
	QSerialPortInfo	*portInfo;
// �������� COM-������ 
public:
	QList<int> Bauds;
// ������ COM-���� 
public:
	QSerialPort *serialPort;
// ������� COM-���� 
public:
	bool OpenSerialPort( QString port, int baud );
    // �������/������� COM-����� 
    bool OpenSerialPort( QSerialPort *serialPort, QString port, int baud );
    // ������� COM-����� 
    void CloseSerialPort( QSerialPort *serialPort );

	// ������� ������ � ���� 
    qint64 writeData2Port( QSerialPort *serPort, char *buff, int len );
	qint64 writeData2Port( QSerialPort *serPort, QByteArray data, int len );
	// ������� ���� � ���� 
    qint64 writeData2Port( QSerialPort *serPort, unsigned char data );

// ��������� �� ������ COM-������ 
public:
	QList<QSerialPortInfo> listOfPorts;
// ���������� ����� � �������
// ������� �� � ComboBox
public:
	QList<QString> serialPortClass::QuerrySerialPorts();
// ������� � ComboBox ����������� �������� ������ �� COM-����� 
public:
	QList<QString> serialPortClass::QuerrySerialPortSpeeds();
//// ������� � ComboBox ��������� �������  ������ �� COM-����� 
//public:
//	QList<QString> getEndsOfFrames();
//	// ������� � ComboBox ��������� �������  ������ �� COM-����� 
//    QList<QByteArray> getEndsOfFramesData();
//
//	QHash<QString, QByteArray> hashTypesEndsOfFrames; // Hash-������� ������� ����� ����� � ���� ��������, ����������������� ������ � ������ �����
//	// QVector<QByteArray> endsUartRequest = new QVector<QByteArray>();


// ������� ���� � ������� 
private:
	QString currPort;
// ������� �������� ����� � ������� 
private:
	int currBaud;
// ����� ������� ������ � ����� ����� 
public:
	QList<QString> endOfFrameList;
// ��������  ������  �� COM-����� 
public:
	QList<unsigned char> *receivedDataCOM_1;
	QList<unsigned char> *receivedDataCOM_2;
	QList<unsigned char> *receivedDataCOM_3;
	QList<unsigned char> *receivedDataCOM_4;
	QList<unsigned char> *receivedDataCOM_5;
    QList<unsigned char> *receivedDataCOM_6;
    QList<unsigned char> *receivedDataCOM_7;
	QList<unsigned char> *receivedDataCOM_8;

// ���������� �������� ������ �� COM-�����
public slots:
    void dataReceived();

// ���������� �������� ������ �� COM-����� �1
public slots:
	void serialPort0_dataReceived();
// ���������� �������� ������ �� COM-����� �2
public slots:
	void serialPort1_dataReceived();
// ���������� �������� ������ �� COM-����� �3
public slots:
	void serialPort2_dataReceived();
// ���������� �������� ������ �� COM-����� �4
public slots:
	void serialPort3_dataReceived();
// ���������� �������� ������ �� COM-����� �5
public slots:
	void serialPort4_dataReceived();
// ���������� �������� ������ �� COM-����� �6
public slots:
	void serialPort5_dataReceived();
// ���������� �������� ������ �� COM-����� �7
public slots:
	void serialPort6_dataReceived();
// ���������� �������� ������ �� COM-����� �8
public slots:
	void serialPort7_dataReceived();


public:
	serialPortClass();
	~serialPortClass();
};

#endif