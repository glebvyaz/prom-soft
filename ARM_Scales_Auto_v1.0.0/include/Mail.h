#ifndef MAIL_H
#define MAIL_H

#include "stdafx.h"

class Mail : public QObject
{
	Q_OBJECT

public:
	bool SendEmail( QString mailTo, QString subject, QString text, bool isHtml, QString htmlBody );
    bool SendEmail( QString mailTo, QString subject, QString text, bool isHtml, QString htmlBody, QStringList listOfFiles );

    Mail();
    ~Mail();
};

// 
class MailThread : public QThread
{
	Q_OBJECT

private:
	QString host;
	int port;
	QString login;
	QString password;
	QString ssl;
	QString mailTo;
	QString subject;
	QString text;
	bool isHtml;
	QString htmlBody;
	QStringList listOfFiles;

	bool manual_send;

public:
    bool active;
	void run();

// Signals
signals:
    void mailSendStatus_Signal( bool status, bool manual_send );

public slots:
	void loadMailData_Slot( bool manual_send, QString, int, QString, QString, QString, QString, QString, QString, bool, QString, QStringList );

public:
    MailThread::MailThread();
	MailThread::~MailThread();
};

#endif