#ifndef CRYPT_H
#define CRYPT_H

#include <QObject>
#include <QString>
#include <QStringList>

class Crypt : public QObject
{
private:

	static QStringList mac_list;
	QString method;
	static QStringList getMACs(); 

public:

	Crypt(QObject *parent);
	~Crypt();
	static QString crypt(QString str);
	static QString decrypt(QString str);
	static QStringList getMAClist() {if(mac_list.isEmpty()) mac_list = getMACs();  return mac_list;};
	static unsigned long getMacHash();


};

#endif // CRYPT_H
