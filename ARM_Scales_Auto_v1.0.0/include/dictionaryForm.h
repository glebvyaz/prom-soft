#ifndef DICTIONARY_H
#define DICTIONARY_H

#include"stdafx.h"

#define MAX_NUM_DICTIONARIES 7 // ���������� ������������ 

// ����� ��� �������� ����� ����������� ������� 
class dictionaryFormClass : public QObject
{
	Q_OBJECT

public:

enum _dictionaryFormClass // ���� ������������ 
{
	SENDERS = 0, // "�����������"
	RECEIVERS, // "����������"
	PAYERS, // "�����������"
    TRANSPORTERS, // "�����������"
	GOODS, // "�����"
	AUTO,// "������ �����������"
	PRIZEP//������ ��������
};
enum _typesOfDictionaries typesOfDictionaries;

    QWidget *dictionaryWindow;
	QLabel *nameOfWindow;
	QPushButton *exitCross;
	
	QTabWidget *tabbedWindow;
	QWidget *tabbedWindowSenders; // "�����������"
	QWidget *tabbedWindowReceivers; // "����������"
	QWidget *tabbedWindowPayers; // "�����������"
	QWidget *tabbedWindowTransporters; // "�����������"
	QWidget *tabbedWindowGoods; // "�����"
	QWidget *tabbedWindowAuto; // "����"
	QWidget *tabbedWindowPrizep; // "������"

    QIcon *tabIcon;

	QLabel *nameOfEntity;

	QLabel *validationDictionaries; // ��������� ��� ����� ������ � ���������� 

	QComboBox *namesSenders; // "�����������"
	QComboBox *namesReceivers; // "����������"
	QComboBox *namesPayers; // "�����������"
	QComboBox *namesTransporters; // "�����������"
	QComboBox *namesGoods; // "�����"
	QComboBox *namesAuto; // "����"
	QComboBox *namesPrizep; // "������"

	QPushButton *attToDB_Button;
	QPushButton *cancelButton;
	QPushButton *deleteRecord;

	QLabel *dataBaseIcon;

public:
	int current_tab; // ������� ������� ����������� 

// ������ 
public slots:
	void showDictionaryWindow(); // ���������� ���� ������������ 
	// void closeDictionaryWindow(); // c����� ���� ������������ 
public:
	void addToDictionary( int num_dictionary, int num_pos ); // �������� � ��������� ������ 
	void removeFromDictionary( int num_dictionary, int num_pos ); // ������� �� ���������� 
    void resetValidation(); // ����� ��������� 
	
// 
public:
	dictionaryFormClass::dictionaryFormClass( QObject *object );
	dictionaryFormClass::~dictionaryFormClass();
};

#endif