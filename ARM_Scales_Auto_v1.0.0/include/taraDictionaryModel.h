#include "stdafx.h"
#include <QAbstractTableModel>

class taraDictionaryModel : public QAbstractTableModel
{
    Q_OBJECT

public:
	taraDictionaryModel( int rows, int columns, QList<QString > headers_names, QObject *parent = 0 );
	taraDictionaryModel( QObject *parent = 0 );

	void addParams( int rows, int columns, QList<QString > headers_names );
	int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
	Qt::ItemFlags flags(const QModelIndex &index) const;

    // �������� ������ 
    void clear();
	// �������� ������ ������ 
    void update( QVector< QString > *data );

private:
	QList< QVector< QString > > m_data;
	int m_rowsCount;
	int m_columnCount;
	QList<QString > headers_names;

};

