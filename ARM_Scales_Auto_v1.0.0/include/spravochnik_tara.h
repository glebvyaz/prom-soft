#ifndef SPRACHNIK_TARA_H
#define SPRACHNIK_TARA_H

#include "stdafx.h"
#include <QDialog>

namespace Ui {
class spravochnik_tara;
}

class spravochnik_tara : public QDialog
{
    Q_OBJECT

public:
    explicit spravochnik_tara(QWidget *parent = 0);
    ~spravochnik_tara();
	void showWindow();

private:
	Ui::spravochnik_tara *ui;

	QList< QString > getHeadersTaraDictionary()
	{
		QList< QString > list;
		list.append( tr("Datetime") );
		list.append( tr("Vehicle number") );
		list.append( tr("Brand") );
		list.append( tr("Tara weight") );

		return list;
	}
	int row;
	int column;
	double m_weight_tara;
	QLineEdit *enterWeightTaraAuto;

private slots:
	void addRecord_Slot();
	void modifyRecord_Slot();
	void removeRecord_Slot();
	void tableView_Clicked( const QModelIndex & );
	void update_tara_weight_Slot();

public slots:
    void getTaraWeight_Slot( double weight );

};

#endif // SPRACHNIK_TARA_H
