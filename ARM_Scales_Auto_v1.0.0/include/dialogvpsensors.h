#ifndef DIALOGVPSENSORS_H
#define DIALOGVPSENSORS_H

#include "stdafx.h"
#include <QDialog>

namespace Ui {
class dialogVPSensors;
}

class dialogVPSensors : public QDialog
{
    Q_OBJECT

public:
    explicit dialogVPSensors(QWidget *parent = 0);
    ~dialogVPSensors();

	QStringList headers;
	int numSensors;

	QVector<int> prev_kg;
	QVector<int> prev_code;

private:
    Ui::dialogVPSensors *ui;
	
public slots:
	void fiterAccept_Slot();
	void fiterReject_Slot();
	void excel_Slot();
	void email_Slot();

signals:
	void loadMailData_Signal( bool, QString, int, QString, QString, QString, QString, QString, QString, bool, QString, QStringList );
};

#endif // DIALOGVPSENSORS_H
