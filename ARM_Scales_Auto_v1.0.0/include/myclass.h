#ifndef MYCLASS_H
#define MYCLASS_H

#include "ui_myclass.h"

#include <QtGui>
#include <QtGui/QTableWidget>
#include <QtGui/QMainWindow>
#include <stdio.h>
#include <QtSql>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QtNetwork>
#include <QtOpenGL>
#include <QWebView>

#include "serialPortClass.h"
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

#include "settings.h"
#include "AcceptionForms.h"
#include "logEventsForm.h"
#include "Form_ttn.h"
#include "DB_Driver.h"
#include "loginWindow.h"
#include "reportFormClass.h"
#include "optionaldevices.h"
#include "dialogvpsensors.h"
#include "HiddenWeighing.h"
#include "MailSend.h"
#include "cardsAcceptDataStruct.h"
#include "cardReaderRfid.h"

QT_USE_NAMESPACE

const float NDS = 0.20; // ��� = 20%
// ��������� ��� ������ � ��-01
#define NUM_BK_01_PARAMS 2 // ���������� ���������� ��-01
#define MIN_WEIGHT_PLATFORM 500 // 500 ��

#define DATETIME_AUTORECEPTION_COLUMN_SHOWN 10
#define DATETIME_AUTORECEPTION_COLUMN 13
#define DATETIME_AUTORECEPTION_COLUMN_CARDS 1
#define ENTERTARA_AUTORECEPTION_COLUMN 20



class MyClass : public QMainWindow
{
	Q_OBJECT

public:
	MyClass(QWidget *parent = 0, Qt::WFlags flags = 0);
	~MyClass();

	QByteArray tabloData;

private:
	Ui::MyClassClass ui;
	void CreateControlsMainWindow( QWidget *w );

public:

	QThread threadSavePhotos; 

	QDialog *sensorsPasswordDialog;
	QLineEdit *enterPasswordDC;
	QPushButton *okButtonDC;
	QPushButton *cancelButtonDC;

	QProgressBar *progrBarMainTable;
	QProgressBar *progrBarReportTable;
	QTableView *tableViewMain;
	QTableView *tableViewReports;

	QVector<int> aw; // vpsensors
	QVector<int> ac;
	int secondsAW;

	int tabloColor;
	int tabloTraff;
	int stateHit; // 1 ��������, 2 ������ ���� ������� �����, 3 ������ 5��� ��� ������� ��� �����, 4 �����������

	QByteArray VP01_svetofor_request;
	QTimer *VP01_svetofor_timer;
	
	HiddenWeighing* hiddenWeighing;

	QLabel *labelMaxSize;

	enum StatesIP_cams
    {
		NO_SNAPSHOT = 0,
		SNAPSHOT = 1
    };
    StatesIP_cams states_ip_cams;

    // ������ � ������� �� IP-������ �1
	QLabel *labelIP_CAM1; // ������� "������ 1"
	QLabel *imageLabel1;
	const QRect *imageLabel_minGeometry1; // ����������� ������ ����������� �� IP-������ 1
	const QRect *imageLabel_maxGeometry1; // ����������� ������ � ��������� ����������� �� IP-������ 1
	const QSize *imageLabel_minSize1; // ����������� ������ ����������� �� IP-������ 1
	const QSize *imageLabel_maxSize1; // ������������ ������ ����������� �� IP-������ 1
    QLabel *labelPrev;
	// 
    QLabel *labelIP_CAM2; // ������� "������ 2"
	QLabel *imageLabel2;
	const QRect *imageLabel_minGeometry2; // ����������� ������ ����������� �� IP-������ 2
	const QRect *imageLabel_maxGeometry2; // ����������� ������ � ��������� ����������� �� IP-������ 2
	const QSize *imageLabel_minSize2; // ����������� ������ ����������� �� IP-������ 2
	const QSize *imageLabel_maxSize2; // ������������ ������ ����������� �� IP-������ 2
    // 
    QLabel *labelIP_CAM3; // ������� "������ 3"
	QLabel *imageLabel3;
	const QRect *imageLabel_minGeometry3; // ����������� ������ ����������� �� IP-������ 3
	const QRect *imageLabel_maxGeometry3; // ����������� ������ � ��������� ����������� �� IP-������ 3
	const QSize *imageLabel_minSize3; // ����������� ������ ����������� �� IP-������ 3
	const QSize *imageLabel_maxSize3; // ������������ ������ ����������� �� IP-������ 3
    // 
    QLabel *labelIP_CAM4; // ������� "������ 4"
	QLabel *imageLabel4;
	const QRect *imageLabel_minGeometry4; // ����������� ������ ����������� �� IP-������ 4
	const QRect *imageLabel_maxGeometry4; // ����������� ������ � ��������� ����������� �� IP-������ 4
	const QSize *imageLabel_minSize4; // ����������� ������ ����������� �� IP-������ 4
	const QSize *imageLabel_maxSize4; // ������������ ������ ����������� �� IP-������ 4

	// ��������� ��������� ����������� �� IP-����� 
	QLabel *snapshot_1; // ������� �1 ���������� ��� ����������� 
	QTimer *timeSnapShotShow_1; // ����� ����������� ���������� �� ������ �1 
	QLabel *previewSnapshot_1; // �������� �������� �1 ���������� ��� ����������� � �������
	// 
	QLabel *snapshot_2; // ������� �2 ���������� ��� ����������� 
	QTimer *timeSnapShotShow_2; // ����� ����������� ���������� �� ������ �2
	QLabel *previewSnapshot_2; // �������� �������� �2 ���������� ��� ����������� � �������
	// 
	QLabel *snapshot_3; // ������� �3 ���������� ��� ����������� 
	QTimer *timeSnapShotShow_3; // ����� ����������� ���������� �� ������ �3
	QLabel *previewSnapshot_3; // �������� �������� �3 ���������� ��� ����������� � ������� 
	// 
	QLabel *snapshot_4; // ������� �4 ���������� ��� ����������� 
	QTimer *timeSnapShotShow_4; // ����� ����������� ���������� �� ������ �4
	QLabel *previewSnapshot_4; // �������� �������� �4 ���������� ��� ����������� � �������
	// 
    QLabel *snapshot_5; // ������� �1 ���������� ��� ����������� 
	QTimer *timeSnapShotShow_5; // ����� ����������� ���������� �� ������ �1 
	QLabel *previewSnapshot_5; // �������� �������� �1 ���������� ��� ����������� � �������
	// 
	QLabel *snapshot_6; // ������� �2 ���������� ��� ����������� 
	QTimer *timeSnapShotShow_6; // ����� ����������� ���������� �� ������ �2
	QLabel *previewSnapshot_6; // �������� �������� �2 ���������� ��� ����������� � �������
	// 
	QLabel *snapshot_7; // ������� �3 ���������� ��� ����������� 
	QTimer *timeSnapShotShow_7; // ����� ����������� ���������� �� ������ �3
	QLabel *previewSnapshot_7; // �������� �������� �3 ���������� ��� ����������� � ������� 
	// 
	QLabel *snapshot_8; // ������� �4 ���������� ��� ����������� 
	QTimer *timeSnapShotShow_8; // ����� ����������� ���������� �� ������ �4
	QLabel *previewSnapshot_8; // �������� �������� �4 ���������� ��� ����������� � �������

	// 
	QPushButton *buttonPhotoShow; // ������ ��������� ����������� ���������� 
	QPushButton *buttonPhotoSave; // ������ ������ ����������� ���������� �� ���� �� HDD

	QLabel *widget_Cam1;
	QLabel *widget_Cam2;
	QLabel *widget_Cam3;
	QLabel *widget_Cam4;

	QLabel *labNoSignal_Cam1;
	QLabel *labNoSignal_Cam2;
	QLabel *labNoSignal_Cam3;
	QLabel *labNoSignal_Cam4;

	QTimer *reconnectIP_Cams;	// ������ �������������� ����� � IP-������� �1
	QTimer *reconnectIP_Cams_2;
	QTimer *reconnectIP_Cams_3;
	QTimer *reconnectIP_Cams_4;

	QNetworkAccessManager *networkAccessManager_CamView01;
	QNetworkAccessManager *networkAccessManager_CamView02;
	QNetworkAccessManager *networkAccessManager_CamView03;
	QNetworkAccessManager *networkAccessManager_CamView04;

	QTimer *timerRequestIP_Cams;


	QUdpSocket *udp_socket;

	QAction *restore_action;
	QAction *addUser_action;
	QAction *log_action;
	QAction *sensors_action;
	QAction *CardsMode_action;
	QAction *hidden_action;

	serialPortClass *serialPortClassObject;
	

	QGroupBox *groupDirAutoMovement;
	QRadioButton *radioButtAutoComeIn; // ������/�������� ���� 
	QRadioButton *radioButtAutoComeOut;
	QPushButton *buttonAutoComeIn; // ������ "������� ����������"
	QPushButton *buttonAutoComeOut; // ������ "�������� ����������"
	QPushButton *reportViewButton; // ������ "�������� �������"
	QPushButton *deleteRecord; // ������ "������� ������ � �����������"

	QTabWidget *tabScalesIndicator; // �������� ���������� ���� 
	QLabel *labelKg; // ����� ����������� ��.���. ���� 

	//QLCDNumber *lcdScalesIndicator; // ��������� ���� 
	QLabel *lcdScalesIndicator;
	QLabel *lcdScalesConnection;
	//QVector<QLCDNumber*> vectorLCD_Indicator; // ������ ����������� ��� ����������� ����
	QVector<QLabel*> vectorLCD_Indicator;
	QLabel *labelEdIsmVes; // ����������� ������� ��������� ���� 
	QCheckBox *checkBoxTrafficLights; // ���./����. ������ ���������� 
	QCheckBox *checkBoxShowIP_Cams; // ���./����. ������ IP ����� 
	QTableWidget *tableAutoReception; // ������� ������/�������� ����������� 
	QScrollArea *scrollBarArea;
	QGroupBox *groupBoxFindAuto; // �����  ������ ���������� 	
	
	QPushButton *printAutoTable; // ������ ���������� ������ � ������� ������/�������� ����������� 
	// 
	QLabel *acceptAutoLabel; // �������  "������� ����������"
	QLabel *shipingAutoLabel; // �������  "��������� ����������"

	QIcon *global_Icon;
	QMenu *menuFile; // ����  ���� 
	QMenu *menuSettings; // ���� �������� 
	QMenu *menuHelp; // ���� ������ 
	QMenu *menuDevices;
	QMenu *menuDB;
	QMenu *menuLang;
	QWebView *webView; // web-����� ��� ������������ ������� 
	QDial *demoDialWeight; // ��������� ���� ��� ����-������ 
    QDialog *dialogHelp; // ���������� ���� ��� ����� ������� � ��������� 
	QPushButton *buttonZeroing;	
	QTimer *timeOutSnapshotIP_Cams;
	optionalDevices *socketDevices;
	QDialog *dialogDB;
	QDialog *dialogDBdump;
	QCheckBox *dbPhoto;
	QListWidget *listWidget;
	
	//����������������
	QHBoxLayout *hLayoutPlatform;
    QGroupBox *groupBoxPlatform;
    QLabel *labelPosRight, *labelPos2Right;
    QLabel *labelPlatform;
    QLabel *labelPosLeft, *labelPos2Left;
    QLabel *labelAuto;
	
public:
    bool eventFilter( QObject *object, QEvent *e ); // ������ ������� 
    void readDictionariesFromDB(); // ��������� ������ ������������ �� �� 
    void readDictionariesFromDB_ALL(); // ���������  ��� ������� �� �� 
	void closeEvent ( QCloseEvent * e ); // ������ �������� ���� 
	
    void saveEventToLog( QString logData ); // �������� ������� � ��� 

	// 
    void makePhotos();
	QByteArray makeStampPhoto( QByteArray src, QString date_time, QString num_auto, int num_pos, QString weight = "", int num_pos2 = 0 );

	QTimer *timerRepeatedRFid;

	// �������� ����� ��� ������ � ��������� ����� 
    void addMailThread();
	QThread *threadMail;
	QTimer *timerSendMail;
	QTimer *timerSendMailEachRec;

	bool isDiscret;
	double roundTo( int value, int div );

	QVector<QNetworkAccessManager *> networkManagersPhotofix;
	QVector<QNetworkAccessManager *> networkManagersPhotofixHidden;
	void updateHiddenImages( QByteArray *ba, int camNum );

	QString pathFile1C;
	void clean_1C();

public slots:	
	void write_1C();

	void finishedHTTP_Hidden_Cams( QNetworkReply *reply );

	void tablo_timer_Slot();

	void finishedHTTP_Request_CamView_01( QNetworkReply* );
	void finishedHTTP_Request_CamView_02( QNetworkReply* );
	void finishedHTTP_Request_CamView_03( QNetworkReply* );
	void finishedHTTP_Request_CamView_04( QNetworkReply* );
	void timerRequestIP_Cams_Slot();

	void VP01_svetofor_timer_Slot();

	void timerRequestCOM_Port_elapsed();
	void timer_sysTick_elapsed(); 
	void timerCursorOff_Slot();
	void cursorChange_Slot( bool a );
	void StartTimerWeight();

	void timeSnapShotShow_1_slot();
	void timeSnapShotShow_2_slot();
	void timeSnapShotShow_3_slot();
	void timeSnapShotShow_4_slot();

	void makeTTN_Button_Slot();
	void pushButtonAccept_Slot();
	void UDP_DataAvalable_Slot();

	void showTaraDictionary_Slot();

	void showTrafficLightsPanel( int state ); // ���� ��� ������� ��������� checkBox
    void buttonAutoComeIn_Clicked(); // ���� �� ������ "������� ����������"
	void buttonAutoComeOut_Clicked(); // ���� �� ������ "��������� ����������"
	void buttonRegistration_Clicked(); // ���� �� ������ "�����������"
	void zeroing(); // ��������� ���������� ����� 
    void closeAcceptForm(); // ������� ����� ������/��������
	void confirmAcceptForm(); // ����������� ����� ������/�������� 
	void showIP_CamsPanel( int state );  // �����������/�������� ������ ����������
	void openSettingsWindow( bool state ); // ������� ���� �������� 
    void closeSettingsWin(); // ������� �������� ���� �������� 
	void makeBruttoWeightSlot(); // �������� ��� 
    void makeTaraWeightSlot(); // �������� ���  �����  � ���� ������/�������� ���������� 
    void makeNettoWeight(); // ��������� �����

	void makeBruttoWeightPricepSlot(); // �������� ��� ������ ������� 
    void makeTaraWeightPricepSlot(); // �������� ���  �����  � ���� ������/�������� ���������� ��� ������� 
    void makeNettoWeightPricep(); // ��������� ����� ������� 

    void makeShippingAuto( QString date_time ); // �������� ���������� � ���������� ��������/��������� 
    void shippingAuto( int, int ); // �������� ������ ��� �������� ���������� 
	void headerShippingAuto_Clicked( int ); // ���� �� ��������� � ������� �������� ����� ���������� ������� 
	void headerTableReports_Clicked( int ); // ���� �� ��������� � ������� �������� ����� ���������� ������� 

	void show_records(); // ���������� ������ �� ��������/������������ ����������� � ������� 
	void reportsAcceptFiltering(); // ��������� ������ � ������� 
	void reportsResetFiltering(); // �������� ������� ������������ ������� 
	void showReportsWindow(); // ���������� ���� ������� 
    void hideReportsWindow(); // c����� ���� ������� 
	void showSettingsWindow(); // ���������� ���� �������� 
	void showDictionaryWindow(); // ������� ���� ������������ 
	void closeDictionaryWindow();
	void addToDB_Slot(); // �������� ������ � ��������� 
	void deleteRecordDB_Slot(); // ������� ������ �� ���������� 
    void currDictionaryChanged( int num_tab ); // ����� ����� ��� ����� ������� ����������� 

	void selectSender_Slot(); // ����� ����������� � �������� "�����������"
	void selectRecepient_Slot();
	void selectPayer_Slot();
	void selectCarrier_Slot();
	void selectGoods_Slot();
	void selectAuto_Slot();
	void selectPrizep_Slot();

	void prizepWeightEnable( int ); // ��������� ����������� ������� �������� 

	void makeNakladnaya_SLOT(); // ������� �������� - ��������� 
    void makeNakladnayaDoubleClick_SLOT( int row, int column ); // �������� ������ ������� ������ ��� ������������ ��������� 

    void convertToExcell(); // ������������� ������ �������� � ������� Excell
	void convertToExcell_sor();
	void printRecords_Slot(); // ����������� ������� � ������� �� ������� �����������

	void setNewValue( int ); // ����� ��������� ���� ��� ���� - ������ 
	void closeApp_SLOT(); // ���� �������� ���� ���������� 

    void printAutoReception_Slot(); // ����������� ������� ������/�������� ������ 

	void launchHelp(); // ����� ����������� ��������� (Help)
	void aboutSoft();  //���� � ���������

	void showLogEventsWindow_Slot(); // ������� ���� ����� 
	void closeLogEventsWindow_Slot(); // ������� ���� ����� 

	void logEventsAcceptFiltering(); // ��������� ������� ����� ������� �������
	void logEventsResetFiltering(); // �������� ������� ����� ������� 

	void timerStartFilteringReports_Slots(); // ��������� ������ � ������� 

	void mailSendStatus_Slot( bool status, bool manual_send );
	void timerSendMail_Slot();
	void timeOutSnapshotIP_Cams_Slot();

	void makeHiddenPhotos_Slot();

	
	void CardsMode_Slot(bool);
	void reopenRfid_Slot();
	void getRfidCode_Slot(QString rfid_code);
	QList< QString > getHeadersMainTable();
	
	void startThreadSendEmail_Slot();

signals:
	void loadMailData_Signal( bool, QString, int, QString, QString, QString, QString, QString, QString, bool, QString, QStringList );

	void cursorChange_Signal( bool a );

	void onBoard_Signal( int );
	void offBoard_Signal();
	void reported_Signal();

	void finishedSavePhotos();

// �������������  Timer - � �������� � COM-���� 
//public:
//	void initTimerRequestCOM_Port();
// ��������� ������� ������� ������ ����������� 
public:
    QList<QString> getColumnsHeadersTableAutoAcception();
	QList<QString> getColumnsHeadersTableAcceptCards();
    // Hash-�������  ���  ��������� ��� ��������  �  ��
    QHash<QString, int> *hashTypesOfVP2;
	QMap<int, unsigned char> type_command_scales;
	QVector< QMap<int, unsigned char> > vector_type_command;

// ������� ��� ���������� ��������
enum _commands_scales
{
	BK_TRAFF_LIGHTS = 0,
	COUNT_COMMANDS_SCALES

}commands_scales;

//-----------------------------------
// SerialPort Settings
//-----------------------------------
public slots:
	void  selectedNewComPort( int );  // , QObject *object );
	void selectedNewComPortSpeed( int );
// ��������� checkBox - � ������ � �� ���������� 
    void chkBoxBK_Work_stateChanged( bool );
    // ��������� checkBox - � ������ � RM ����������
	void chkBoxRM_Work_stateChanged( int );
    // Slot ��� ������  ����������� ������� ����� � ������� ���� (������� � ������� �����)
    void mainWinVP_tabChanged( int );

	// ����� ���� ����� 
	void selectedNewTypeVP( int );
// ���������� �������� ������ �� COM-����� �1
	void serialPort0_dataReceived();
	void serialPort1_dataReceived(); // ���������� �������� ������ �� COM-����� �2
	void serialPort2_dataReceived();
	void serialPort3_dataReceived();
	void serialPort4_dataReceived();
	void serialPort5_dataReceived();
	void serialPort6_dataReceived();
	void serialPort7_dataReceived();

// ��� �� �� (�� 8-�� ����� �����)
public:
	int weightScales_sor;
	int weightScales_last_no_sor;
	QString weightScales_01;
	qint32 weightScales_02;
	qint32 weightScales_03;
	qint32 weightScales_04;
	qint32 weightScales_05;
	qint32 weightScales_06;
	qint32 weightScales_07;
	qint32 weightScales_08;

	// �������������  Timer - � �������� � COM-���� 
    void initTimerRequestCOM_Port();
	// ������ ������� ������ �� 
    bool startServerProcess( bool command );
    // 
	QTimer *timerRequestCOM_Port; // ��������� �� ������ ������� �������� � COM-���� 
	QTimer *sysTickTimer; // ��������� �� ������ ���������� ������� 
    // 
    // ������ ������ ���������� ����� �� COM-������ 
	bool flagsNoUarts_connection[ 8 ]; // ???
	QVector<bool*> vectorFlagsNoUARTs_Connection;
	int timeoutsNoUartConnection[ 8 ]; // ???

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ���������� ��� ������ � IP-������� ��� ������� jpeg, HTTP_������� 
// �������� ������ � HTTP-���������  ���  IP-����� 
public:
	QNetworkAccessManager *networkAccessManager_01;
	QNetworkAccessManager *networkAccessManager_02;
	QNetworkAccessManager *networkAccessManager_03;
	QNetworkAccessManager *networkAccessManager_04;
	QNetworkReply *reply_cam1;
	QNetworkReply *reply_cam2;
	QNetworkReply *reply_cam3;
	QNetworkReply *reply_cam4;

// ���������� ��� ������ � IP-������� ��� ������� mjpeg, h264
public:
	QTcpSocket *RTSP_tcpSocket; // ����� ��� ���������� � IP-������� � ��� ���������� �������� (PLAY, STOP � �.�.)
	QTcpSocket *RTSP_tcpSocket_2; // ����� ��� ���������� � IP-������� � ��� ���������� �������� (PLAY, STOP � �.�.)
	QTcpSocket *RTSP_tcpSocket_3; // ����� ��� ���������� � IP-������� � ��� ���������� �������� (PLAY, STOP � �.�.)
	QTcpSocket *RTSP_tcpSocket_4; // ����� ��� ���������� � IP-������� � ��� ���������� �������� (PLAY, STOP � �.�.)

// ������ ��� ������ � IP_������� 
public:
	void sendRequestIP_Cam( QNetworkAccessManager *mngr,  QString url_string, int port, QString login, QString password, int num_cam );
	void sendRequestIP_Cam_2( QNetworkAccessManager *mngr,  QString url_string, int port, QString login, QString password, int num_cam ); // �2
	void sendRequestIP_Cam_3( QNetworkAccessManager *mngr,  QString url_string, int port, QString login, QString password, int num_cam ); // �3
	void sendRequestIP_Cam_4( QNetworkAccessManager *mngr,  QString url_string, int port, QString login, QString password, int num_cam ); // �4

	// ���� ��� ��������� ������ ����� ����� �� UDP-������ ������ �� IP-������ 
public slots:
	void finishedHTTP_Request_Cam_01( QNetworkReply* ); // ����� �� HTTP-������ ���  IP-������ �1
	void finishedHTTP_Request_Cam_02( QNetworkReply* ); // ����� �� HTTP-������ ���  IP-������ �2
	void finishedHTTP_Request_Cam_03( QNetworkReply* ); // ����� �� HTTP-������ ���  IP-������ �3
	void finishedHTTP_Request_Cam_04( QNetworkReply* ); // ����� �� HTTP-������ ���  IP-������ �4

	void errorSocket();
    void hostFoundSlot();
    void proxyAuthenticationRequiredSlot( const QNetworkProxy & proxy, QAuthenticator * authenticator );\
    void stateChangedSlot( QAbstractSocket::SocketState socketState );
	void aboutToCloseSlot();
	void bytesWrittenSlot( qint64 bytes );
	void readChannelFinishedSlot();
	void destroyedTCP_Socket();
	void connectedToServerRTP(); // ���� ��������  �������  �  �������� (����� �������� ����� ������)
	void connectedToServerRTCP(); // ���� ��������  �������  �  �������� (����� �������� �������� ����� ������)
	void readPendingDatagramDataRTP(); // ���� ������ �� IP-������ 
	void readPendingDatagramControlRTCP(); // ���� ������ �������� �������� �� IP-������ 

// ������������ JPEG �������� 
public:
	QByteArray CreateJFIFHeader( QByteArray *header, int type, int width, int height, /* QByteArray *lqt */ unsigned char *lqt, /* QByteArray *cqt  */ unsigned char *cqt, int dri );
	void MakeTables( int q, /* QByteArray *lqt */ unsigned char *lqt, /* QByteArray *cqt  */ unsigned char *cqt ); // ��������� ������� ��� JPEG-����� (Luma � Croma)
	void MakeQuantHeader( QByteArray *header, /* QByteArray *qt */ unsigned char *qt , int numTable ); // ��������� ������� � ��������� JPEG-����� (Luma ��� Croma)
	void MakeHuffmanHeader( QByteArray *header, unsigned char *codelens, int lenCodes, unsigned char *symbols, int lenSymbols, int numTable, int tableClass);
	void MakeDRIHeader( QByteArray *header, int dri ); // ��������� JPEG  ???

	// Service Reminder
	QTimer *timerReminderService; // ������ ����������� �� ��������� ������������ 
	QTimer *timerRemainTimeService; // ������ ��������� ������� �� "��������������" ������ ����������� � ��������� ������������ 

	int timerServiceValue; // �������� ��������� ������� ����������� ����������� � ��������� ������������ 

	QTimer *timerTrial;
	
public slots:
	void timer_Trial_Enable();
	void showDataBaseContent_Slot();
	void deleteAutoReception_Record();
    void unselectAutoReception_Record_Item(); // �������� ������� ������ ��� ���������� �������� ������ �� ������� ������/�������� ����������� 
	void selectAutoReception_Record_ForDelete( int, int );
	void show_InAutoAcception(); // ���������� ������ �� �������� ����������� 
	void show_CardsAcception(); // ���������� ������ �� �������� ����������� � ������ RFID
	void changeUser(); // ����� ���� ����� ������������ 
	void addUser(); // ����� ���� ����������� ������������ 
	void showMainWindow(); // ���������� ������� ���� ��������� 

	void selectedAcceptingAutoMode();
	void selectedShippingAutoMode();

	void show_AcceptionForm(); // ���������� ����� ������/�������� ����������� 
	void hide_AcceptionForm(); // ������ ����� ������/�������� ����������� 

	void showPhotoReports(); // ���������� ���������� �� ������� 
	void prepare_showPhotoReports( QModelIndex index );
	void selectTableReports_Record( int, int );  // �������� ������  ��� ���������� ��������� ����������� �� ������� ������� 
	void savePhotoReports_Rfid( );  // ��������� ���������� �� ������� ��� Rfid
	void savePhotoReportsTo_HDD( ); // ��������� ���������� �� ������� �� HDD
	void savePhotoForBot( QString date_time, QString numAuto, float Brutto, float Netto, float Tara); // ��������� ���������� ��� ����
	bool isInetConnect(); //�������� ������� ��������� ��� �������� ������ �� ����
	void sendLastRecToEmail( ); // ��������� ��� ��������� �� ����� ����� �����������
	void SendMailMode_Slot(int state); //������������� ������� �����
	

	// ��������� (��������������� ����� ������) ���� �� ��  �  ���� 
    void saveImageFromDB_ToFile( QByteArray ba, QString fileName );
	void changeDirectory(); // ���� ���������� ��� ����� ���������� 

	void groupUDP_Settings_Slot( bool state );

	void split_contacts(QString path);

	void lang_ua_Slot();
	void lang_ru_Slot();
	void lang_en_Slot();

	void socketsDialog_Slot();
	void socketError_Slot(QAbstractSocket::SocketError);
	void readSocket2_1_Slot();
	void readSocket2_2_Slot();
	void readSocket2_3_Slot();
	void timerSocket2_1_Slot();
	void timerSocket2_2_Slot();
	void timerSocket2_3_Slot();
	void setSocket2State_Slot( int socket_num, int relay, int setTo );
	void setSocket2State_Slot( int socket_num, int relay, int setTo, int time );

	void dbBackup_Slot();
	void dbPackingFinished_Slot(int, QProcess::ExitStatus);
	void dbRestoreDialog_Slot();
	void dbBackupDialog_Slot();
	void dbRestore_Slot();
	void dbUnpackingFinished_Slot(int, QProcess::ExitStatus);
	void restoreGo_Slot();
	void dumpGo_Slot();
	void dbOpenFolder_Slot();

	void tabloStateTimer_Slot();
	void sayAboutZero_Slot(bool);

	void askSensorPassword();
	void okButtonDC_Slot();
	void cancelButtonDC_Slot();

	void timer_PositionS2_Slot();
	void readSocket_PositionS2_Slot();
	void socketError_PositionS2_Slot( QAbstractSocket::SocketError );

	
	void set_Lamp( int rele, int setTo );
	void set_Lamp1_on();
	void set_Lamp1_off();
	void set_Lamp2_on();
	void set_Lamp2_off();
	void timer_Lamps_Slot();
	void readSocket_Lamps_Slot();
	void socketError_Lamps_Slot( QAbstractSocket::SocketError );

	void set_Svetofor( int rele, int setTo );
	void timer_SvetoforS2_Slot();
	void readSocket_SvetoforS2_Slot();
	void socketError_SvetoforS2_Slot( QAbstractSocket::SocketError );

	void set_Barrier( int rele, int setTo );
	void timer_BarrierS2_Slot();
	void readSocket_BarrierS2_Slot();
	void socketError_BarrierS2_Slot( QAbstractSocket::SocketError );

	void socketError_tcpSocket_Tablo1_Slot( QAbstractSocket::SocketError );
	void socketError_tcpSocket_Tablo2_Slot( QAbstractSocket::SocketError );

	void socketErrorWeight_Slot( QAbstractSocket::SocketError );

	void timerWeight_Slot();
	void readWeight_Slot();
	void timerConnection_Slot();

	void parserVP( QByteArray ba );

public:
	void getSocket2State( int socket_num );	
	void drawPositionAuto(bool leftOk, bool rightOK);
	void drawPosition2Auto(bool left2Ok, bool right2OK);
	bool zeroBlock;

	QWidget *waitW;
	QProcess *dumpProcess;
	QProcess *restoreProcess;
	QString lastNameOfDump;

	bool isVPConnected;
	bool isON_flag;

// �����
private:
	QTcpSocket *tcpSocket2_1;
	QTcpSocket *tcpSocket2_2;
	QTcpSocket *tcpSocket2_3;
		
	QTimer *timerSocket2_1;
	QTimer *timerSocket2_2;
	QTimer *timerSocket2_3;

	quint16 nextBlockSize1;
	quint16 nextBlockSize2;
	quint16 nextBlockSize3;

	QNetworkSession *networkSession;

	QString strError1;
	QString strError2;
	QString strError3;

	QTimer* timer_PositionS2;
	QTcpSocket* tcpSocket_PositionS2;

	QTimer* timer_Lamps;
	QTcpSocket* tcpSocket_Lamps;

	QTimer* timer_SvetoforS2;
	QTcpSocket* tcpSocket_SvetoforS2;

	QTimer* timer_BarrierS2;
	QTcpSocket* tcpSocket_BarrierS2;

	bool isPositionOK, isPositionLeftOK, isPositionRightOK, isPositionLeft2OK, isPositionRight2OK;
	bool show2ndPos;

	bool getPosState();
	bool verifyAutoPosition( int weight, bool silent = false );

	QLabel* label_Svetovor1;
	QLabel* label_Svetovor2;

	QLabel* label_number_fone;
	QLabel* label_picture_vesi;

	QTcpSocket* tcpSocket_Tablo1;
	QTcpSocket* tcpSocket_Tablo2;

	QTimer* timerWeight;
	QTcpSocket* tcpWeight;

private:
	int rowAutoRecept;
	int columnAutoRecept;
	bool flag_UDP_listening_state;
	unsigned char _code_pos;


signals:
	 void setParams( int zoomP, int zoomPhotoAutoN, int threshouldB /* , int, int, double, int, double, int, int, int */ );
    
     void readyBytes( QByteArray* );
	 void readyBytes_2( QByteArray* );
	 void readyBytes_3( QByteArray* );
	 void readyBytes_4( QByteArray* );

	 void readyImage_MyClass( QImage );
	 void readyImage_MyClass_2( QImage );
	 void readyImage_MyClass_3( QImage );
     void readyImage_MyClass_4( QImage );

	 void sayAboutZero_Signal(bool);

	 void setTaraWeight_Signal( double weight );

	 void writeSensors_Signal( QVector<int>, QVector<int> );
};

#endif // MYCLASS_H
