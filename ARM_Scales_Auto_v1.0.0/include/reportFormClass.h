#ifndef REPORTFORM_H
#define REPORTFORM_H

#include "stdafx.h"
#include <QtGui>
#include <QtSql>
#include <QtWebKit>
#include "MailSend.h"

#define DATETIME_REPORTS_COLUMN1 13
#define DATETIME_REPORTS_COLUMN2 15
#define ENTERTARA_REPORTS_COLUMN 24


// �����  ����� ������� 
class reportFormClass : public QObject
{
    Q_OBJECT

public:
	QThread threadSavePhotos;
    QDialog *reportForm;
	QGroupBox *sortGroupBox;
	// 
/*	QCheckBox *checkBoxNumAuto;
	QLineEdit *enterNumAuto;*/
	// 
	QCheckBox *checkBoxDateBegin;
	QLabel *beginDate;
	QCalendarWidget *beginCalendar;
	QCheckBox *checkBoxDateEnd;
	QLabel *endDate;
	QCalendarWidget *endCalendar;
    // 
	QCheckBox *checkBoxTimeBegin;
	QLabel *beginTimeLabel;
	QTimeEdit *beginTime;
	QCheckBox *checkBoxTimeEnd;
	QLabel *endTimeLabel;
	QTimeEdit *endTime;


	QCheckBox *checkBoxNumAuto;  //��������� ComboBox ��� ���� ���������� ������ QLineEdit 
	QComboBox *enterNumAuto;

	// 
	QCheckBox *checkBoxCarrier;
	QComboBox *selectCarrier;
    // 
	QCheckBox *checkBoxTypeOfGoods;
	QComboBox *selectTypeOfGoods;
	// 
	QCheckBox *checkBoxSender;
	QComboBox *selectSender;
	// 
	QCheckBox *checkBoxAccepter;
	QComboBox *selectAccepter;
	// 
	QCheckBox *checkBoxPayer;
	QComboBox *selectPayer;
	// 
	QPushButton *sortButton;
	QTableWidget *tableReports;
	// 
	QPushButton *acceptFilterButton; // ������ ���������� �������(��)
	QPushButton *resetFilterButton; // ������ ������ �������(��)
	// 
	QRadioButton *radioButtAutoComeInReports;
	QRadioButton *radioButtAutoComeOutReports;
	// 
	QPushButton *closeButton;
	QPushButton *makeTTN_Button;
	QPushButton *makeNakladnayaButton; // ������� �������� ������/�������� 
	QPushButton *exportExccellButton; // ������� ������ � Exccell
	QPushButton *exportExccellButton_sor;
	QPushButton *printRecords; // ����������� ���������� ������� ����������� 
	
	QPushButton *buttonPhotoSaveAll;
	QCheckBox *checkBoxPhotoSaveAll;
	// 
	QLabel *reportsShowValidation; // ����� � �������� � ���������� ������ ������� � ����������� 
	// 
	QLabel *labelNumScales; // �������  "����� ������ �����"
	QComboBox *comboBoxNumScales; // ����� ������ ����� 
	// 
	QPushButton *buttonPhotoShow; // ������ ��� ��������� ����������� ����������� �� ������� 

	// ��������� �������� ������ � ������� 
	QLabel *loadIndicatorReports; // ��������� �������, ���������� � ������� �������, ��� ������� ������ �� ������� 
	QProgressBar *loadProgressBarReports;
    QTimer *timerStartFilteringReports;
	QGraphicsOpacityEffect *opacityEffect;

	QDialog *dialonNumNakladn;
	QComboBox *selectNumNakl;
	QPushButton *buttonPrint;

	QPushButton *buttonSendReportEmail;

	int row;
	int column;

// Methods
public: // ��������� ������� ������� ������� 
    QList<QString> getColumnsHeadersTableAutoReports();
    void enableDisableControls(); // ��������� ���������  ���������� 
    void disableControls(); // ��������� ���������  ���������� 
	void showPrintDialog();

// Slots
public slots:
	void checkBoxNumAutoSlot( int );
	void checkBoxDateBeginSlot( int );
    void checkBoxDateEndSlot( int );
	void checkBoxTimeBeginSlot( int );
	void checkBoxTimeEndSlot( int );
	void checkBoxSenderSlot( int );
	void checkBoxAccepterSlot( int );
	void checkBoxCarrierSlot( int );
	void checkBoxTypeOfGoodsSlot( int );
	void checkBoxPayerSlot( int );
	void buttonPrint_Slot();
	void buttonSendReportEmail_Slot();
	void startThreadSendEmail_Slot();

signals:
	void loadMailData_Signal( bool, QString, int, QString, QString, QString, QString, QString, QString, bool, QString, QStringList );
	void finishedSavePhotos();

// �����������/���������� 
public:
	reportFormClass( QWidget *w, QObject *object );
	~reportFormClass();

};

#endif