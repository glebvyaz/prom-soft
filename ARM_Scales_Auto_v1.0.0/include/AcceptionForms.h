#ifndef ACCEPTIONFORMS_H
#define ACCEPTIONFORMS_H

#include "stdafx.h"
QT_USE_NAMESPACE

// ���� ���� ������/�������� ����������� 
class acceptionForm : public QObject
{
	Q_OBJECT

public:
	QWidget *acceptionFormWindow; // ���� ������/�������� ���������� 

	QLabel *headerName; // ��� ���� 
	QFrame *operationName; // ����� � �������� �������� 

	QRadioButton  *shippingAuto; // ����� �������� ���������� 
	QRadioButton  *acceptingAuto; // ����� ������ ���������� 

	QFrame *frameDescription; // ����� � ���������� �������� 

	QLabel *labelOperationDescription; // ��������� ���� �������� 
	QLabel *labelDocumentName; // ������������ ��������� 
	QFrame *frameDocumentName; // ����� ������������ ��������� 

	QLabel *labelDocumentNumber; // ����� ��������� 
	QLineEdit *documentNumber;

	QLabel *labelAutoNumber; // ����� ���������� 
	QComboBox *AutoNumber;
	QPushButton *selectAutoNumber;
	// 
	QCheckBox *checkBoxPrizepWeightEnable; // ����� ����������� ������� ��������
	// 
	QLabel *labelAutoPrizepNumber; // ����� ������� 
	QComboBox *AutoPrizepNumber;
	QPushButton *selectPrizepNumber;  //����� ������ �������

	QLabel *labelSender; // ����������� 
	QComboBox *sendersList; // ������ ������������ 
	QPushButton *selectSender; // ����� ����������� 

	QLabel *labelRecepient; // ���������� 
	QComboBox *recepientsList; // ������ ����������� 
	QPushButton *selectRecepient; // ����� ���������� 

	QLabel *labelPayer; // ���������� 
	QComboBox *payersList; // ������ ������������ 
	QPushButton *selectPayer; // ����� ����������� 

	QLabel *labelNameCarrier; // ���������� 
	QComboBox *namesCarrierList; // ������ ������������ 
	QPushButton *selectCarrier; // ����� ����������� 

	QLabel *labelGoods; // ���� 
	QComboBox *namesGoods; // ������ ������ 
	QPushButton *selectGoods; // ����� ������������ ����� 

	QLabel *labelPricePerUnit; // ���� ����� �� ����� 
	QLineEdit *pricePerUnit;

	QLabel *labelNumidity;
	// QLineEdit *enterHumidity;

	QPushButton *buttonGetLastData;

	QFrame *horisontalLine1; // �������������� ����� 
	QFrame *horisontalLine2; // �������������� ����� 
	QFrame *horisontalLine3; // �������������� �����

	QPushButton *enterTaraFromDB;

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// 
	QLabel *labelFactWeight; // ����������� ��� 
	// 
	QLabel *labelBrutto; // ��� ������ 
	QLineEdit *bruttoWeight;
	QPushButton *makeBruttoWeight; // �������� ��� ������ 
	// 
	QLabel *labelTara; // ��� ���� 
	QLineEdit *taraWeight;
	QPushButton *makeTaraWeigh; // �������� ��� ���� 
	QPushButton *getTaraWeighFromBase; // ����� ��� ���� �� �� 
    QCheckBox *enterTaraManually;
	QLineEdit *enterTaraWeightManualy; // ������ ��� ���� ������� 

	QCheckBox *checkSor;
	QDoubleSpinBox *spinSor;

	QCheckBox *checkTaraFromBD;
	// 
	QLabel *labelNetto; // ��� ����� 
	QLineEdit *nettoWeight; 
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// 
	QLabel *labelFactWeightPricep; // ����������� ��� ������� 
	// 
	QLabel *labelBruttoPricep; // ��� ������ 
	QLineEdit *bruttoWeightPricep;
	QPushButton *makeBruttoWeightPricep; // �������� ��� ������ 
	// 
	QLabel *labelTaraPricep; // ��� ���� 
	QLineEdit *taraWeightPricep;
	QPushButton *makeTaraWeighPricep; // �������� ��� ���� 
	QPushButton *getTaraWeighFromBasePricep; // ����� ��� ���� �� �� 
	QCheckBox *enterTaraManuallyPricep;
	QLineEdit *enterTaraWeightManualyPricep; // ������ ��� ���� ������� 

	// 
	QLabel *labelNettoPricep; // ��� ����� 
	QLineEdit *nettoWeightPricep;
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//
	QLabel *labelOperatorReception; // ������� "������" (��������)
	QLabel *labelOperatorUserRecept; // ������� "������������" (�����������)
	QLineEdit *operatorUserRecept; // ����������� �������� 
	QLabel *labelReceptionDateTime; // ������� "����/�����"
	QLineEdit *receptionDateTime; // ����/����� ������ ���������� 
	// 
    QLabel *labelOperatorShipping; // ������� "��������" (��������)
	QLabel *labelOperatorUserShipping; // ������� "������������" (������������)
	QLineEdit *operatorUserShipping; // ������������ �������� 
	QLabel *labelShippingDateTime; // ������� "����/�����"
	QLineEdit *shippingDateTime; // ����/����� ��������� ���������� 
    // 
	QPushButton *confirmForm; // ����������� ����� 
	QPushButton *cancelForm; // �������� ����� 

// ������ 
public:
	void setupUI_AcceptionForm( QObject *object ); // ������� ��������� ���� ������/ �������� ���������� 
	// ������� ������ � ������� ������ ����������� 
    bool insertDataIntoAcceptTable();
	void clearControls(); // ��������  ���� ��� ����� ������ 
    bool eventFilter( QObject *object, QEvent *e ); // ������ �������
	void keyPressEvent( QKeyEvent * event );
    void setHeaderName( int num_scales ); // ���������� ������� (����� �����) ��� ����� ������/�������� ����������� 
    bool validationAcceptForm(); // ��������� �������� ������ � ����� ������/�������� ����������� 
    void enableDisablePrizepWeight( bool state ); // ���������/��������� ��������� ��� ����������� ������� �������� 
    void resetValidation(); // ����� ��������� ����� ������/������� 

	// ������ ������ 
public:
	void enableAcceptingFormControls(); // ��������� ��������� � ������ ������ ��������� 
    void disableAcceptingFormControls(); // ��������� ��������� � ������ ������ ��������� 
    void enableShippingFormControls(); // ��������� ��������� � ������ �������� ��������� 
    void disableShippingFormControls(); // ��������� ��������� � ������ �������� ��������� 

public slots:
	void textChanged_Slot( const QString& );
	void textChangedPricep_Slot( const QString& );

	void buttonGetLastData_Slot();
	void selectedTareFromDataBase_Slot();

// �����������/���������� 
public:
	acceptionForm();
	~acceptionForm();

};

// ������ ������ ����� ������/�������� ����������� 
class acceptFormModel
{
public:
	int type_operation; // ��� �������� - �������/�������� ����� 
    // 
	QString Date_Time; // �����/���� 
	QString Nakladnaya; // ����� ��������� 
	QString Name_User_Accept; // ������������, ����������� ���� 
	QString Number_Auto; // ����� ���������� 
	double Brutto; // ������ 
	double Tara; // ���� 
	double Netto; // ����� 
    // 
	QString Num_Prizep; // ����� ������� ���������� 
	double Brutto_Prizep; // ������ 
	double Tara_Prizep; // ���� 
	double Netto_Prizep; // �����
	QString Goods; // ��� ����� 
	float Price; // ���� ����� �� ������� ���� (�����)
	// float humidity; // ��������� 
	// 
	QString Sender; // ����������� ����� 
	QString Accepter; // ���������� ����� 
	QString Payer; // ���������� 
	QString Transporter; // ���������� 
    // 
	QString Name_User_Send; // ������������, ������������ ���� 
	QString Date_Time_Send; // ����/����� �������� ����� 

	int id_photo; // id ������ �  ������������  ���  ���������� �� � �� 

	int tara_type;

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
public:
	void clear(); // �������� ��������� ����� 
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

public:
	acceptFormModel();
	~acceptFormModel();
};
// ������ ������ ��� �������� ����������� 
class imageSaveModel
{
public:
	int id_photo; // id ������ �  ������������  ���  ���������� �� � �� 
	QString Date_Time; // �����/���� ������ ���� 
	QString Date_Time_Send; // �����/���� �������� ���� 
	QByteArray image1;
	QByteArray image2;
	QByteArray image3;
	QByteArray image4;
	short num_measure;
	QString num_auto;

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
public:
	void clear(); // �������� ��������� �����

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
public:
	imageSaveModel();
	~imageSaveModel();
};

#endif