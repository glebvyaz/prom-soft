#ifndef REPORTS_FORM_H
#define REPORTS_FORM_H

#include <QDialog>
#include <QTextCodec>
#include <QProgressBar>
#include <QTableView>
#include <QMessageBox>
#include <QPrinter>
#include <QPrintDialog>
#include <QPrintPreviewDialog>
#include <QtWebKitWidgets>
#include <QWebView>
#include <QWebFrame>
#include <QTextEdit>
#include <QFont>
#include <QDesktopServices>
#include <QImage>
#include <qbytearray.h>
#include <QBuffer>


namespace Ui {
class reports_form;
}

class reports_form : public QDialog
{
    Q_OBJECT

public:
    explicit reports_form(QWidget *parent = 0);
    ~reports_form();
	void setupParamsAndSlots();
	int getReportTableColumnsCount();
	QTableView* getTableView();

	void printRecords( QTableView & table, QList< QString > & headers, int type_table );
	int getIdForShowPhoto( QTableView & table, int row, int column );
	bool eventFilter( QObject *object, QEvent *e );

	QString getDateTimeRecord();
	QTableView *getTableReports();
	QProgressBar *getProgressBarReports();
	void updateReportsTable();
	// returns num photos
    int showPhotos( QVector< QByteArray > ba, QVector< QLabel* > labels );
	// ����������� ������� ������ � ������� �� ������� ����������� 
    void printRecordWithPhotos( const QModelIndex & index, QTableView & table, QList< QString > headers, int num_table );

public slots:
    void showForm();
	void updateReportsTable_Slot();
	void btnAcceptFilter_Slot();
	void radioBtnSend_Slot();
	void btnResetFilter_Slot();
	void setSpravochnikiContent_Slot( QList< QString > data, int tab );
	void btnPrintNakladnaya_Slot();
	void tableViewReportsDoubleClicked_Slot( const QModelIndex & index );
	void tableViewReportsClicked_Slot( const QModelIndex & index );
	void btnPrintTable_Slot();
	// ������������� ������ �������� � ������� Excel
    void convertToExcell();
	void makeTTN_Button_Slot();

	void tableViewReportsHeader_Slot( int logicalIndex );

private:
    Ui::reports_form *ui;
	QTextCodec *textCodec;
	QProgressBar *progrBar;
	int row;
	int column;
	QWebView *webView;

	QLabel *labelViewPict_1;
	QLabel *labelViewPict_2;
	QLabel *labelViewPict_3;
	QLabel *labelViewPict_4;
	QSortFilterProxyModel *sort_filter;

public:
	QList< QString > getHeadersReportsTable()
	{
		QList< QString > list;
		
		list.append( textCodec->toUnicode( "����/�����\n ��������" ) );
		list.append( textCodec->toUnicode( "����/�����\n ������" ) );		
		list.append( textCodec->toUnicode( "����� 1,\n ��" ) );
		list.append( textCodec->toUnicode( "����� 2,\n ��" ) );
		list.append( textCodec->toUnicode( "�����,\n ��" ) );
		list.append( textCodec->toUnicode( "����" ) );
		list.append( textCodec->toUnicode( "��� RFID" ) );

		/*list.append( textCodec->toUnicode( "����/�����\n ��������" ) );
		list.append( textCodec->toUnicode( "����/�����\n ������" ) );		
		list.append( textCodec->toUnicode( "����� 1,\n ��" ) );
		list.append( textCodec->toUnicode( "����� 2,\n ��" ) );
		list.append( textCodec->toUnicode( "�����,\n ��" ) );
		list.append( textCodec->toUnicode( "���������" ) );
		list.append( textCodec->toUnicode( "��������" ) );
		list.append( textCodec->toUnicode( "����� ����" ) );
		list.append( textCodec->toUnicode( "�.�.�. ��������" ) );
		list.append( textCodec->toUnicode( "� ��������" ) );
		list.append( textCodec->toUnicode( "������" ) );
        list.append( textCodec->toUnicode( "����" ) );
		list.append( textCodec->toUnicode( "��� RFID" ) );*/

		// ����������� ������� �� DATA-file
		list.append( textCodec->toUnicode( "����� ����" ) );
		list.append( textCodec->toUnicode( "�����������" ) );
		list.append( textCodec->toUnicode( "����������" ) );
		list.append( textCodec->toUnicode( "����������" ) );
		list.append( textCodec->toUnicode( "����" ) );
		list.append( textCodec->toUnicode( "������" ) );
		list.append( textCodec->toUnicode( "��������" ) );
		list.append( textCodec->toUnicode( "� ���������" ) );
		list.append( textCodec->toUnicode( "� �������" ) );
		list.append( textCodec->toUnicode( "���� ���� �������" ) );
        

	    return list;
	};
};

#endif // REPORTS_FORM_H