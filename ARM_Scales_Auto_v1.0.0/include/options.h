#ifndef options_H
#define options_H

#include "stdafx.h"
#include <QDialog>

namespace Ui {
class options;
}

class options : public QDialog
{
    Q_OBJECT

public:
    explicit options(QWidget *parent = 0);
    ~options();
	
	QString getCamerasKey();
	QString getWasteKey();
	QString getTrafficKey();
	QString getBarrierKey();
	QString getPositioningKey();
	QString getScoreboardsKey();
	QString getRfidKey();
	QString getBotKey();
	QString getProgramKey();

	QStringList modules;
	QVector<bool> isCheked;

	bool check( QString key, QString module );

	void set_Trial_Time( QString );

private:
    Ui::options *ui;
	QTextCodec *textCodec;

	unsigned short getCpuHash();

	QVector<QPushButton*> pushButtons;
	QVector<QLineEdit*> lineEdits;

public slots:
	void push();

//signals:
	
};

#endif // options_H
