#ifndef ADSTRUCT_H
#define ADSTRUCT_H

#include <QString>

struct Record
{
	QString dateTimeAccept;
	QString dateTimeSend;

	QString code;
	double weight_1;
	double weight_2;
	double netto;
    QString num_auto;
    QString fio_driver;
	QString material;
    QString quarry;
    QString unload;
	QString num_card_poryadk;
	int blocked;
};

extern struct Record record;

struct Photofix_data
{
	int id;
	int num_measure;
};

extern struct Photofix_data photofix_struct;

#endif
