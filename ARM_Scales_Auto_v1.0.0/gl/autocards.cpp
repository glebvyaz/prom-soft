#include "include/autocards.h"
#include "include/photoFix.h"
#include "include/tablethread.h"

tablethread *tableThreadObj;

#define PROGRAMM_VERSION QString( "v2.5.1.15 GlinaDonbass" )

#define RESTART_MYSQL_MSECS     (2 * 60 * 1000) // --������!!! (60)


#define NUM_PHOTO_CELL_MAIN 10

#define MAX_SOCKETS1 11

#define MAX_SOCKETS2 2   //    ����2

class settings_form *settingsObj;
class loginWindowClass *loginWindowObject;
class mysql *mysqlObj;
class events_form *eventObj;
class Form_ttn *formTTN_Object;
class spravochniki_form *spravochnikiObj;
class reports_form *reportsFormObj;
//class TabloTCM *tabloTCM_Obj;
class dialogPreview *dialogPreview_Obj;

struct Record record;
struct Photofix_data photofix_struct;

enum StatesWeight
{
	IDLE = 0, 
	AUTO_ON_PLATFORM,
	RFID_ACTIVATED, 
	FIN_WEIGHT
};

enum StatesWeight statesWeight;


AutoCards::AutoCards(QWidget *parent)
    : QMainWindow(parent), row( -1 ), column( -1 ), MIN_POROG( 1000 ), MIN_WEIGHT( 500 )
{
    qDebug() << "autocards constructor";

    QSettings settingsTime( "debug_log.ini", QSettings::IniFormat );
    settingsTime.setIniCodec( "Windows-1251" );
    settingsTime.setValue( "Debug/time_net_start", QDateTime::currentDateTime().toString("hh:mm:ss.zzz") );

    //QProcess::startDetached("cmd /c net start mysql");
    restartMySQL_Slot(QString("Program start-up..."));

    // ���� 2 ������ ������� ������
    // --������
   QTimer::singleShot(RESTART_MYSQL_MSECS, this, &AutoCards::construct);
    //QTimer::singleShot(10, this, &AutoCards::construct);

    colorRFID = "red";
    colorSocket1 = "red";
//    ����2
    colorRFID2 = "red";
    colorSocket2 = "red";
//    --
    isStopped = false;
    weight = 0;
    temperature = 0;
    tabloColor = "0";
    tabloZnak = "0";

    ups.inputVoltage = 0;
    ups.outputVoltage = 0;
}

void AutoCards::construct()
{
    this->show();
    ui.setupUi(this);
    tipsWindow = new tips;


    //label_test = new QLabel;
    //label_test->setStyleSheet("font: 36pt MS Shell Dlg 2;");
    //label_test->setGeometry( 20,20, 500,300);
    //label_test->hide();

    setUpGraphics();
    setupTimer();
    setupSignalSlots();

    mysqlObj->saveEventToLog(textCodec->toUnicode("������ ���������"));

    moxaInitialization();
    rfidInitialization();
//    ����2
    rfid2Initialization();
//    --
    socketsInitialization();

    this->setWindowTitle( QString( textCodec->toUnicode( "����������� ����������� ��� ����� ������ �� ������������� ����� %1" ) ).arg( PROGRAMM_VERSION ) );
    ui.centralWidget->hide();

    mysqlObj->getMainTableData( getHeadersMainTable(), ui.tableViewMain, progrBarMainTable, QList< QString >() );

    //���� �����
    loginWindowObject->confirmButton->clicked();

//    if(mysqlObj->isServerConnected()) colorSQL = "green";
//    else colorSQL = "red";

    buttonAuto_Slot();

    QVector<int> ct = mysqlObj->getCancelTara();
    if(ct.count() < 2) return;
    CANCEL_BUTTON = ct[0];
    TARA_BUTTON = ct[1];

//    ����2
    TARA_BUTTON2 = 41;
    CANCEL_BUTTON2 = 42;

//    --
    connect( ui.pushButton_test, SIGNAL( clicked() ), this, SLOT( test_Slot() ) );
    connect( ui.pushButton_cancel, SIGNAL( clicked() ), this, SLOT( cancel_Slot() ) );

    tipsWindow->InitialPosition_Slot();


    QSettings settings( configurePath, QSettings::IniFormat );
    settings.setIniCodec("Windows-1251");

    timerFinish = new QTimer();
    timerFinish->setInterval( mysqlObj->WAIT_MESSAGE_MSECS );
    timerFinish->setSingleShot(true);
    connect( timerFinish, SIGNAL(timeout()), this, SLOT(timerFinish_Slot()) );

    /*QTimer *timerNonSleep = new QTimer();
    timerNonSleep->setInterval( 5*60*1000 );
    connect( timerNonSleep, SIGNAL(timeout()), this, SLOT(timerNonSleep_Slot()) );
    timerNonSleep->start();
    */

    //ui.pushButton_test->hide();

}

void AutoCards::delay( int millisecondsToWait )
{
    QTime dieTime = QTime::currentTime().addMSecs( millisecondsToWait );
    while( QTime::currentTime() < dieTime )
    {
        QCoreApplication::processEvents( QEventLoop::AllEvents, 100 );
    }
}

// 
void AutoCards::setUpGraphics()
{
	statusLabel = new QLabel;

    QSettings settingsTime( "debug_log.ini", QSettings::IniFormat );
    settingsTime.setIniCodec( "Windows-1251" );
    settingsTime.setValue( "Debug/time_db_instance", QDateTime::currentDateTime().toString("hh:mm:ss.zzz") );

	mysqlObj = mysql::instance();

	textCodec = QTextCodec::codecForName( "Windows-1251" );
    settingsObj = new settings_form;
    eventObj = new events_form;
    loginWindowObject = new loginWindowClass;
    spravochnikiObj = new spravochniki_form;
    reportsFormObj = new reports_form;
    dialogPreview_Obj = new dialogPreview;

	progrBarMainTable = new QProgressBar( ui.tableViewMain );
	progrBarMainTable->resize( 180, 34 );

	loginWindowObject = new loginWindowClass;

	// ��������������� 
	ui.menuBar->addAction( textCodec->toUnicode( "���������������" ), this, SLOT( showLoginWindow() ) );
	ui.menuBar->addAction( textCodec->toUnicode( "������" ), this, SLOT( showHelp() ) );


    tableThreadObj = new tablethread();

    connect( this, SIGNAL( startTableThread_Signal( Record, int ) ),
        tableThreadObj, SLOT( start_Slot( Record, int ) ) );


    tableThreadObj->start();
    tableThreadObj->setPriority( QThread::LowPriority );

	// 
    this->showMaximized();
	showLoginWindow();
}

// 
void AutoCards::setupSignalSlots()
{
	bool flag = connect( ui.buttonSettings, SIGNAL( clicked() ), settingsObj, SLOT( showForm() ) );
	flag = connect( ui.buttonAddUser, SIGNAL( clicked() ), loginWindowObject, SLOT( showCreateAccountWin() ) );
	flag = connect( ui.buttonChangeUser, SIGNAL( clicked() ), this, SLOT( showLoginWindow() ) );
	flag = connect( ui.buttonLogs, SIGNAL( clicked() ), eventObj, SLOT( showForm() ) );
    flag = connect( ui.buttonBlockRec, SIGNAL( clicked() ), this, SLOT( buttonBlockRec_Slot() ) );
    flag = connect( ui.buttonSerachAuto, SIGNAL( clicked() ), this, SLOT( buttonSerachAuto_Slot() ) );
    flag = connect( ui.buttonAuto, SIGNAL( clicked() ), this, SLOT( buttonAuto_Slot() ) );
    flag = connect( ui.buttonRegisterAuto, SIGNAL( clicked() ), spravochnikiObj, SLOT( showForm() ) );
    flag = connect( ui.buttonReports, SIGNAL( clicked() ), reportsFormObj, SLOT( showForm() ) );

	flag = connect( loginWindowObject->confirmButton, SIGNAL( clicked() ), this, SLOT( showMainWindow() ) );

	flag = connect( ui.tableViewMain, SIGNAL( clicked( const QModelIndex & ) ), this, SLOT( tableViewMain_Clicked( const QModelIndex & ) ) );
    flag = connect( ui.tableViewMain, SIGNAL( doubleClicked( const QModelIndex & ) ), this, SLOT( tableViewMain_doubleClicked( const QModelIndex & ) ) );

    flag = connect( mysqlObj, SIGNAL(  restartMySQL_signal( QString& ) ), this, SLOT( restartMySQL_Slot( QString& )) );

}

// 
void AutoCards::setupTimer()
{
    // ���������  ������
    sysTimer = new QTimer( this );
    sysTimer->setInterval( 1000 );
    sysTimer->setSingleShot( false );
	connect( sysTimer, SIGNAL( timeout() ), this, SLOT( sysTimer_Slot() ) );
    sysTimer->start();

    chkDbTimer = new QTimer( this );
    chkDbTimer->setInterval( 3 * 60 * 1000 );
    chkDbTimer->setSingleShot( false );
    connect( chkDbTimer, SIGNAL( timeout() ), mysqlObj, SLOT( stable_Slot() ) );
    chkDbTimer->start();

    restartMySQLTimer = new QTimer( this );
    restartMySQLTimer->setInterval( RESTART_MYSQL_MSECS );
    restartMySQLTimer->setSingleShot( true );
    bool flag = connect( restartMySQLTimer, SIGNAL( timeout() ), this, SLOT( finishedMySQLrestart_Slot() ) );

}

void AutoCards::buttonAuto_Slot()
{
    tipsWindow->setWindowFlags( Qt::CustomizeWindowHint );
    tipsWindow->showFullScreen();
    tipsWindow->setCursor(Qt::BlankCursor);
}

// 
void AutoCards::sysTimer_Slot()
{
    tipsWindow->setWeightLabel1Sec(weight);
	switch( statesWeight )
	{
	    case IDLE:
		{
            if( weight > MIN_POROG )
			{
				statesWeight = AUTO_ON_PLATFORM;
                //labelTrack->show();
			}
            else if( weight < ( MIN_POROG - 200 ) )
			{
				statesWeight = FIN_WEIGHT;
			}

            tabloZnak = "1";
            tabloColor = "1";
		} break;
		case AUTO_ON_PLATFORM:
		{
            if( weight < ( MIN_POROG - 200 ) )
			{
				statesWeight = FIN_WEIGHT;
			}

            tabloZnak = "0";
            tabloColor = "0";
		} break;
		case RFID_ACTIVATED:
		{
            if( weight < ( MIN_POROG - 200 ) )
			{
				statesWeight = FIN_WEIGHT;
			}

            tabloZnak = "1";
            tabloColor = "0";
		} break;
		case FIN_WEIGHT:
		{
            if( weight > MIN_POROG )
			{
				statesWeight = AUTO_ON_PLATFORM;
                //labelTrack->show();
			}
			else
            if( weight < ( MIN_POROG - 200 ) )
			{
				statesWeight = IDLE;
                //labelTrack->hide();
			}

            tabloZnak = "1";
            tabloColor = "1";
		} break;
	}

    //qDebug() << "Weight: " << weight << " | UPS: " << ups.inputVoltage << "->" << ups.outputVoltage;

	ui.labelUserData->setText( QString( "<html><body style='font-size: 18px;font-weight: bold;'>%1   |   %2</html></body>" )
        .arg( QDateTime::currentDateTime().toString( "dd.MM.yyyy hh:mm" ) )
		.arg( loginWindowObject->getCurrentUser() ) );


    tipsWindow->setFramesColors(colorRFID, colorSocket1);
    tipsWindow->setFramesColors2(colorRFID2, colorSocket2); // ����2
}

// 
void AutoCards::showLoginWindow()
{
	ui.centralWidget->hide();
	ui.buttonSettings->setEnabled( false );
	loginWindowObject->show();
	ui.centralWidget->hide();
}

// ���������� ������� ���� ��������� 
void AutoCards::showMainWindow()
{
	bool result;

    result = loginWindowObject->confirmLogin( loginWindowObject->enterName->currentText(), mysqlObj->getAdminPassword() );

	if( result == true )
	{
	    loginWindowObject->hide();
		ui.centralWidget->show();
		ui.menuBar->setEnabled( true );

	    // ���������/��������� ��������� � ����������� �� ����, ����� ������������ ����������� 
	    if( loginWindowObject->role != loginWindowObject->ADMIN )
	    {
			ui.buttonSettings->setEnabled( false );
			ui.buttonAddUser->setEnabled( false );
			ui.buttonBlockRec->setEnabled( false );

			// menuAddRemoveUser->setEnabled( false );
		}
		else
		{
			ui.buttonSettings->setEnabled( true );
			ui.buttonAddUser->setEnabled( true );
			ui.buttonBlockRec->setEnabled( true );

			// menuAddRemoveUser->setEnabled( true );
		}

		// �������� ������� � ��� 
		mysqlObj->saveEventToLog( QString( QTextCodec::codecForName( "Windows-1251" )->toUnicode( "���� � ��������� ��� ������: %1" ) ).
			arg( loginWindowObject->getCurrentUser() ) );

		loginWindowObject->loginWin->hide();
        ui.centralWidget->show();

        QSettings settings( configurePath, QSettings::IniFormat );
		settings.setIniCodec("Windows-1251");

		for(int i = 0; i < ui.tableViewMain->model()->columnCount() - 1; i++ )
		{
			ui.tableViewMain->setColumnWidth( i, settings.value( QString( "columnSizes/c%1" ).arg( i ), 140 ).toInt() );
		}
		ui.tableViewMain->setSortingEnabled( false );
		ui.tableViewMain->horizontalHeader()->setStretchLastSection( true );
	}
}

void AutoCards::closeEvent( QCloseEvent *event )
{
//	QMessageBox mess;
//	mess.setText( textCodec->toUnicode( "�� ������������� ������ ����� �� ���������?" ) );
//	mess.setStandardButtons( QMessageBox::Ok | QMessageBox::Cancel );
//	mess.setButtonText( QMessageBox::Ok, textCodec->toUnicode( "��" ) );
//	mess.setButtonText( QMessageBox::Cancel, textCodec->toUnicode( "������" ) );
//	mess.setIcon( QMessageBox::Question );
//	mess.setWindowTitle( textCodec->toUnicode( " ����� " ) );
//	mess.exec();

//	if( mess.result() != QMessageBox::Ok )
//	{
//		event->ignore();
//	}
//	else
//	{
        QSettings settings( configurePath, QSettings::IniFormat );
		settings.setIniCodec("Windows-1251");

		for(int i = 0; i < ui.tableViewMain->model()->columnCount() - 1; i++ )
		{
			settings.setValue( QString( "columnSizes/c%1" ).arg( i ), ui.tableViewMain->columnWidth( i ) );
		}
		settings.sync();

		mysqlObj->saveEventToLog( QString( textCodec->toUnicode( "����� �� ��." ) ) );
		event->accept();
//      ����2
//       �������� �����
        QTimer::singleShot( 10, tipsWindow, SLOT( Card2LampOff() ));
        QTimer::singleShot( 15, tipsWindow, SLOT( Tara2LampOff() ));
        QTimer::singleShot( 20, tipsWindow, SLOT( Error2LampOff() ));
//   --
        delete tipsWindow;
//	}
}

// 
void AutoCards::getRfidCode_Slot( QString rfid_code )
{
    mysql *mysqlObj = mysql::instance();

    if( tipsWindow->currentCommand != 0)
    {
        mysqlObj->saveEventToLog( QString( textCodec->toUnicode( "������������ ����� �� ���������� �����������: %1!" ).arg(rfid_code) ) );
        // ��������������� ������������

//      ����2
//        �������� ������ ������ � ����� ����
        QTimer::singleShot( 100, tipsWindow, SLOT( Tara2LampOff() ));
        QTimer::singleShot( 120, tipsWindow, SLOT( Error2LampOn() ));

        QTimer::singleShot( 620, tipsWindow, SLOT( Error2LampOff() ));
        QTimer::singleShot( 1620, tipsWindow, SLOT( Error2LampOn() ));
        QTimer::singleShot( 2220, tipsWindow, SLOT( Error2LampOff() ));

        QTimer::singleShot( 2240, tipsWindow, SLOT( Tara2LampOn() ));
        QTimer::singleShot( 2740, tipsWindow, SLOT( Tara2LampOff() ));
        QTimer::singleShot( 3240, tipsWindow, SLOT( Tara2LampOn() ));
//        --
        return;
    }

    if( weight < MIN_WEIGHT )
    {
        mysqlObj->saveEventToLog( QString( textCodec->toUnicode( "��� ������������ �����, ��� �� ��������� ���� MIN = !" )
            .append( "%1" ).append( textCodec->toUnicode( " ��" ) ).arg( MIN_WEIGHT ) ) );

//      ����2
//        �������� ������� ���� � ������
        QTimer::singleShot( 100, tipsWindow, SLOT( Tara2LampOn() ));
        QTimer::singleShot( 120, tipsWindow, SLOT( Error2LampOn() ));

        QTimer::singleShot( 600, tipsWindow, SLOT( Tara2LampOff() ));
        QTimer::singleShot( 620, tipsWindow, SLOT( Error2LampOff() ));

        QTimer::singleShot( 1600, tipsWindow, SLOT( Tara2LampOn() ));
        QTimer::singleShot( 1620, tipsWindow, SLOT( Error2LampOn() ));

        QTimer::singleShot( 2200, tipsWindow, SLOT( Tara2LampOff() ));
        QTimer::singleShot( 2220, tipsWindow, SLOT( Error2LampOff() ));
//        --
        return;
    }

    QList< QString > data = mysqlObj->getRegistrationData( rfid_code );
    qDebug() << data;
    if( data.size() <= 0 ) // ������ ��� ������ RFID ����� ���
    {
        statesWeight = AUTO_ON_PLATFORM;
        mysqlObj->saveEventToLog( textCodec->toUnicode( "����� %1 ��� � �����������!" )
            .arg( rfid_code ) );

        tipsWindow->noCardReg();
        return;
    }

    if(tipsWindow->isTaraMode)
    {
        int idfoto = mysqlObj->saveRecordsPhotos( QDateTime::currentDateTime().toString( "yyyy-MM-dd hh:mm:ss" ) );
        photofix_struct.id = idfoto;
        photofix_struct.num_measure = 0;

        PhotoFix *photoFix = PhotoFix::getInstance();
        photoFix->makePhotos();

        mysqlObj->saveEventToLog( textCodec->toUnicode( "����������� ��� ����� %1, ������� ���� %2." )
            .arg( record.code )
            .arg( record.num_auto ) );

        tipsWindow->setTaraPhrase(mysqlObj->updateTaraByCard(weight, rfid_code, idfoto), weight, mysqlObj->getRegistrationData(rfid_code));
        tipsWindow->isTaraMode = false;

        QList< QString > data2;
        if( mysqlObj->hasRecordMainTable( data2, rfid_code ) ) // ��������� ������ �����
        {
            mysqlObj->saveEventToLog( QString( textCodec->toUnicode( "��������� ����� %1 � ������� ������������� �����������." ).arg(rfid_code) ) );

            record.dateTimeAccept = data2.at( 0 );
            record.dateTimeSend = QDateTime::currentDateTime().toString( "yyyy-MM-dd hh:mm:ss" );
            record.code = data2.at( 1 );
            record.weight_1 = data2.at( 2 ).toDouble();
            record.weight_2 = weight;
            record.netto = abs( record.weight_2 - record.weight_1 );

            record.num_auto = data2.at( 3 );
            record.fio_driver = data2.at( 4 );
            record.material = data2.at( 5 );
            record.quarry = data2.at( 6 );
            record.unload = data2.at( 7 );

            QString dateT = QDateTime::fromString( record.dateTimeAccept, "dd.MM.yyyy hh:mm:ss" ).toString( "yyyy-MM-dd hh:mm:ss" );
            record.dateTimeAccept = dateT;

            int idf = mysqlObj->getIdfForRecord( dateT );

            emit startTableThread_Signal( record, idf );
        }

        statesWeight = RFID_ACTIVATED;
        return;
    }

    QList< QString > data2;
    if( mysqlObj->hasRecordMainTable( data2, rfid_code ) ) // ��� ��� ��� ��� �� ���������, �� �������
    {
        mysqlObj->saveEventToLog( QString( textCodec->toUnicode( "����� %1 �� �������, �.�. ��� ������������ � ������� ������������� ����������� � ������� �����������!" ).arg(rfid_code) ) );

        tipsWindow->setNoTara( 1 );
        return;
    }

	if( data.size() > 0 ) // ���� ������ ���� 
	{
        record.code = rfid_code;
        record.weight_1 = weight;
        record.weight_2 = 0;
        record.netto = 0;
        record.num_auto = data.at( 0 );
        record.fio_driver = data.at( 1 );
        record.num_card_poryadk = data.at( 2 );

        QStringList toTips;
        toTips.append( QString::number( weight ) );
        toTips.append( record.num_auto );
        toTips.append( record.fio_driver );
        tipsWindow->setNextPhrase( toTips );


        // ����/����� ���������� ������
        QString dateTime = QDateTime::currentDateTime().toString( "yyyy-MM-dd hh:mm:ss" );

        record.dateTimeAccept = dateTime;
        record.dateTimeSend = dateTime;
        int id = mysqlObj->saveRecordsPhotos( dateTime );

        photofix_struct.id = id;
        photofix_struct.num_measure = 0; // ������ �����������

        PhotoFix *photoFix = PhotoFix::getInstance();
        photoFix->makePhotos();

        mysqlObj->saveEventToLog( textCodec->toUnicode( "����������� ��� ����� %1, ������� ���� %2." )
            .arg( record.code )
            .arg( record.num_auto ) );

		statesWeight = RFID_ACTIVATED;
	}
}

// 
void AutoCards::tableViewMain_Clicked( const QModelIndex & index )
{
	this->row = index.row();
	this->column = index.column();

	ui.tableViewMain->selectRow( this->row );
}

// 
void AutoCards::tableViewMain_doubleClicked( const QModelIndex & index )
{
	int row = index.row();
	int column = index.column();

	QTableView & table = *ui.tableViewMain;
	if( column == NUM_PHOTO_CELL_MAIN )
	{
		int idf = reportsFormObj->getIdForShowPhoto( table, row, 0 );

		mysql *mysqlObj = mysql::instance();
		QVector< QByteArray > ba = mysqlObj->getSavedPhoto( idf );

		QVector< QLabel* > labels = dialogPreview_Obj->getPreviewControls();

		if( reportsFormObj->showPhotos( ba, labels ) > 0 )
		{
			dialogPreview_Obj->showMaximized();
			dialogPreview_Obj->raise();
		}

		return;
	}
}

// 
AutoCards::~AutoCards()
{
    //delete ui;
    //delete tipsWindow;
}

// 
void AutoCards::buttonBlockRec_Slot()
{
	if( QMessageBox::Cancel == QMessageBox::question( NULL, textCodec->toUnicode( "���������� ������" ), textCodec->toUnicode( "�� ������������� ������ ��������� ������ � ������ �����������?" ), QMessageBox::Ok, QMessageBox::Cancel ) )
	{
		return;
	}

	if( this->row != -1 && this->column != -1 )
	{
		record.dateTimeAccept = QDateTime::fromString( ui.tableViewMain->model()->index( this->row, 0 ).data().toString(), "dd.MM.yyyy hh:mm:ss" ).toString( "yyyy-MM-dd hh:mm:ss" );
		record.dateTimeSend = QDateTime::currentDateTime().toString( "yyyy-MM-dd hh:mm:ss" );
		record.code = ui.tableViewMain->model()->index( this->row, 11 ).data().toString();
		record.weight_1 = ui.tableViewMain->model()->index( this->row, 1 ).data().toDouble();
		record.weight_2 = 0;
		record.netto = 0;

        record.num_auto = ui.tableViewMain->model()->index( this->row, 4 ).data().toString();
        record.fio_driver = ui.tableViewMain->model()->index( this->row, 5 ).data().toString();

        record.material = ui.tableViewMain->model()->index( this->row, 6 ).data().toString();
        record.quarry = ui.tableViewMain->model()->index( this->row, 7 ).data().toString();
        record.unload = ui.tableViewMain->model()->index( this->row, 8 ).data().toString();

        //record.num_card_poryadk = ui.tableViewMain->model()->index( this->row, 8 ).data().toString();
		record.blocked = 1; // ������������� ����������� 

		int idf = mysqlObj->getIdfForRecord( record.dateTimeAccept );

		if( mysqlObj->saveReportsTableData( record, idf ) )
		{
			photofix_struct.id = idf;
			photofix_struct.num_measure = 2; // ������ �����������
			reportsFormObj->updateReportsTable_Slot();
			mysqlObj->getMainTableData( getHeadersMainTable(), ui.tableViewMain, progrBarMainTable, QList< QString >() );
		}
	}
	else
	{
		QMessageBox::warning( NULL, textCodec->toUnicode( "���������� ������" ), textCodec->toUnicode( "�������� ������ ��� ����������!" ), QMessageBox::Ok );
	}

	this->row = -1;
	this->column = -1;
}

// 
void AutoCards::buttonSerachAuto_Slot()
{
	if( ui.enterSearchAuto->text().isEmpty() )
	{
		QMessageBox::warning( NULL, textCodec->toUnicode( "����� ����������" ), textCodec->toUnicode( "������� ����� ����������!" ), QMessageBox::Cancel );
	}

	mysql *mysqlObj = mysql::instance();
	QStringList dateTimes = mysqlObj->getIdxForNumAutoFromMainTable( ui.enterSearchAuto->text().trimmed() );

	ui.tableViewMain->clearSelection();
	ui.tableViewMain->setSelectionMode( QAbstractItemView::MultiSelection );
	ui.tableViewMain->setSelectionBehavior( QAbstractItemView::SelectRows );
	
	for(int n = 0; n < dateTimes.count(); n++ )
	{
		for(int i = 0; i < ui.tableViewMain->model()->rowCount(); i++ )
		{
			if( ui.tableViewMain->model()->index( i, 0 ).data().toString() == dateTimes[n] )
			{
				ui.tableViewMain->selectRow( i );
			}
		}
	}

	ui.tableViewMain->setFocus();
}

// 
void AutoCards::showHelp()
{
	QDesktopServices::openUrl( QUrl::fromLocalFile( "help\\help.pdf" ) );
}

void AutoCards::moxaInitialization()
{
    QSettings settings( configurePath, QSettings::IniFormat );
    settings.setIniCodec( "Windows-1251" );
	QString moxaIP = settings.value( "Moxa/ip" ).toString();
    QString moxaPort1 = settings.value( "Moxa/port1" ).toString();
    QString moxaPort2 = settings.value( "Moxa/port2" ).toString();
    //QString moxaPort3 = settings.value( "Moxa/port3" ).toString();
    QString moxaPort4 = settings.value( "Moxa/port3" ).toString(); // ��� ������ �������!!11��������

    nextBlockSizeMoxa1 = 0;
    nextBlockSizeMoxa2 = 0;
    nextBlockSizeMoxa4 = 0;

    socketMoxaPort1 = new QTcpSocket( this );
    socketMoxaPort2 = new QTcpSocket( this );
    //QTcpSocket* socketMoxaPort3 = new QTcpSocket( this );
    socketMoxaPort4 = new QTcpSocket( this );

    socketMoxaPort1->disconnectFromHost();
    socketMoxaPort2->disconnectFromHost();
    //socketMoxaPort3->disconnectFromHost();
    socketMoxaPort4->disconnectFromHost();

    int connected = 0;
    socketMoxaPort1->connectToHost( moxaIP, moxaPort1.toInt() );
    if(socketMoxaPort1->waitForConnected(100)) connected++;

    socketMoxaPort2->connectToHost( moxaIP, moxaPort2.toInt() );
    if(socketMoxaPort2->waitForConnected(100)) connected++;

    //socketMoxaPort3->connectToHost( moxaIP, moxaPort3.toInt() );

    socketMoxaPort4->connectToHost( moxaIP, moxaPort4.toInt() );
    if(socketMoxaPort4->waitForConnected(100)) connected++;

    qDebug() << "MOXA ports connected: " << connected << moxaIP << moxaPort1 << moxaPort2 << moxaPort4;
    mysqlObj->saveEventToLog( textCodec->toUnicode( "���������� � ������� ��������������� MOXA: ") + (connected == 3 ? textCodec->toUnicode("������") : textCodec->toUnicode("��������")));


    //--A�����! �������!
    if( connected != 3 )
    //if( connected != 1 )

	{
//        QMessageBox mess;
//        mess.setText( textCodec->toUnicode( "��� ����������� � ��������������� MOXA!" ) );
//        mess.setStandardButtons( QMessageBox::Cancel );
//        mess.setButtonText( QMessageBox::Cancel, "��" );
//        mess.setIcon( QMessageBox::Warning );
//        mess.setWindowTitle( textCodec->toUnicode( "������" ) );
//        mess.exec();
		isMoxaON_flag = false;
        //return;
	}	
	else
	{
		isMoxaON_flag = true;
	}

    // init UPS
    socketMoxaPort4->write( "QPI\r" );
    socketMoxaPort4->write( "I\r" );
    socketMoxaPort4->write( "M\r" );

    connect( socketMoxaPort1, SIGNAL( readyRead() ), this, SLOT( readMoxaPort1_Slot() ) );
    connect( socketMoxaPort2, SIGNAL( readyRead() ), this, SLOT( readMoxaPort2_Slot() ) );
    connect( socketMoxaPort4, SIGNAL( readyRead() ), this, SLOT( readMoxaPort4_Slot() ) );

    QTimer *timerMoxaWrite = new QTimer( this );
    timerMoxaWrite->setInterval( 500 );
    connect( timerMoxaWrite, SIGNAL( timeout() ), this, SLOT( writeMoxa_Slot() ) );
    timerMoxaWrite->start();

	isMoxaConnected = false;	
	QTimer *timerMoxaConnection = new QTimer( this );
	timerMoxaConnection->setInterval( 5000 );
	connect( timerMoxaConnection, SIGNAL( timeout() ), this, SLOT( timerMoxaConnection_Slot() ) );
	timerMoxaConnection->start();
}

void AutoCards::timerMoxaConnection_Slot()
{
    if( isMoxaConnected == false )
    {
        isMoxaON_flag = false;
        weight = -1;
    }

    if( isMoxaConnected == true )
        isMoxaConnected = false;
}

void AutoCards::writeMoxa_Slot()
{
    //�������!  --
    socketMoxaPort1->write( "\2AB03\3" );

    QString tabloWeight = QString::number(weight);
    while(tabloWeight.count() < 6) tabloWeight.push_front(' ');
    socketMoxaPort2->write( QString( tabloZnak + " " + tabloWeight + " " + tabloColor + "\r" ).toLatin1() );

    socketMoxaPort4->write( "QS\r" );
}

void AutoCards::readMoxaPort1_Slot()
{
    // AB+00001071e
    isMoxaConnected = true;

    QTcpSocket* pClientSocket = (QTcpSocket*)sender();

    QByteArray message;
    forever
    {
        if(nextBlockSizeMoxa1 == 0)
        {
            if(pClientSocket->bytesAvailable() < 1) break;

            message = pClientSocket->readAll();
        }
        if(nextBlockSizeMoxa1 > pClientSocket->bytesAvailable()) break;

        QString temp = QString( message.mid( 3,8 ) );
        weight = temp.toInt();
        QString weightString = QString::number( weight );
        ui.labelWeightIndicator->setText( weightString );

        //ui.labelStateConn->setStyleSheet( "background-color: Red;font-size: 18px;color: #ffffff;" );
        //ui.labelWeightIndicator->setText( "---" );
        ui.labelStateConn->setStyleSheet( "background-color: Green; font-size: 18px; color: #ffffff;" );
    }
}

void AutoCards::readMoxaPort2_Slot()
{
    QTcpSocket* pClientSocket = (QTcpSocket*)sender();

    QByteArray message;
    forever
    {
        if(nextBlockSizeMoxa2 == 0)
        {
            if(pClientSocket->bytesAvailable() < 1) break;

            message = pClientSocket->readAll();
        }
        if(nextBlockSizeMoxa2 > pClientSocket->bytesAvailable()) break;

        qDebug() << "From board(that should not happen, something is wrong): " << message;
    }
}

void AutoCards::readMoxaPort4_Slot()
{
    // 23 6e 01 20 65 20 00 01 20 65 20 00 20 60 23 20 12 c0 00 20 eb 20 3c 20 0b 20 22 0d
    //isMoxaConnected = true;

    QTcpSocket* pClientSocket = (QTcpSocket*)sender();

    QByteArray message;
    forever
    {
        if(nextBlockSizeMoxa4 == 0)
        {
            if(pClientSocket->bytesAvailable() < 27) break;

            message = pClientSocket->readAll();
        }
        if(nextBlockSizeMoxa4 > pClientSocket->bytesAvailable()) break;

        QList<QByteArray> sl = message.split(' ');
        if( sl.count() >= 4 )
        {
            sl[0].remove(0, 1);
            for(int i = 0; i < sl.count(); i++) sl[i] = "0x" + sl[i].toHex();

            ups.inputVoltage = sl[0].toInt(0, 16) * sl[1].toInt(0, 16) / 51 / 256;
            ups.outputVoltage = sl[2].toInt(0, 16) * sl[3].toInt(0, 16) / 51 / 256;
        }
    }
}

void AutoCards::rfidInitialization()
{
    QSettings settings( configurePath, QSettings::IniFormat );
    settings.setIniCodec( "Windows-1251" );
    rfidIP = settings.value( "rfid/IP" ).toString();
    rfidPort = settings.value( "rfid/PORT" ).toString();

    nextBlockSizeRFID = 0;

    socketRFID = new QTcpSocket( this );
    socketRFID->disconnectFromHost();

    int connected = 0;
    socketRFID->connectToHost( rfidIP, rfidPort.toInt() );
    if(socketRFID->waitForConnected(200)) connected++;
    if(connected) colorRFID = "green";

    qDebug() << "RFID connected: " << connected << " ip: " << rfidIP << " port: " << rfidPort.toInt();
    mysqlObj->saveEventToLog( textCodec->toUnicode( "���������� � RFID: " ) + (connected ?  textCodec->toUnicode("�������") : textCodec->toUnicode("�������")) +
                               (!connected ? " ip: " + rfidIP + " port: " + rfidPort : "" ));

    connect( socketRFID, SIGNAL( readyRead() ), this, SLOT( readFRID_Slot() ) );

    QTimer *timer = new QTimer( this );
    timer->setInterval( 60000 );
    connect( timer, SIGNAL( timeout() ), this, SLOT( reconnectRFID_Slot() ) );
    timer->start();
}

void AutoCards::rfid2Initialization()
{
    QSettings settings( configurePath, QSettings::IniFormat );
    settings.setIniCodec( "Windows-1251" );
    rfid2IP = settings.value( "rfid_tara/IP" ).toString();
    rfid2Port = settings.value( "rfid_tara/PORT" ).toString();

    nextBlockSizeRFID2 = 0;

    socketRFID2 = new QTcpSocket( this );
    socketRFID2->disconnectFromHost();

    int connected = 0;
    socketRFID2->connectToHost( rfid2IP, rfid2Port.toInt() );
    if(socketRFID2->waitForConnected(200)) connected++;
    if(connected) colorRFID2 = "green";

    qDebug() << "RFID2 connected: " << connected << " ip: " << rfid2IP << " port: " << rfid2Port.toInt();
    mysqlObj->saveEventToLog( textCodec->toUnicode( "���������� � RFID-2 (���� ����): " ) + (connected ?  textCodec->toUnicode("�������") : textCodec->toUnicode("�������")) +
                               (!connected ? " ip: " + rfid2IP + " port: " + rfid2Port : "" ));


    connect( socketRFID2, SIGNAL( readyRead() ), this, SLOT( readFRID2_Slot() ) );

    QTimer *timer = new QTimer( this );
    timer->setInterval( 60000 );
    connect( timer, SIGNAL( timeout() ), this, SLOT( reconnectRFID2_Slot() ) );
    timer->start();
}

void AutoCards::reconnectRFID_Slot()
{
    socketRFID->disconnectFromHost();

    socketRFID->connectToHost( rfidIP, rfidPort.toInt() );
    if(socketRFID->waitForConnected(500))
    {
        if( colorRFID == "red" )
        {
            colorRFID = "green";
            mysqlObj->saveEventToLog(textCodec->toUnicode("������� ����������������� RFID ������, ip: ") + rfidIP + ", port: " + rfidPort );
        }
    }
    else
    {
        if( colorRFID == "green" )
        {
            colorRFID = "red";
            mysqlObj->saveEventToLog(textCodec->toUnicode("������� ����������������� RFID �� �������, ip: ") + rfidIP + ", port: " + rfidPort );
        }
    }
    qDebug() << "recconected_RFID" << colorRFID;
}

void AutoCards::reconnectRFID2_Slot()
{
    socketRFID2->disconnectFromHost();

    socketRFID2->connectToHost( rfid2IP, rfid2Port.toInt() );
    if(socketRFID2->waitForConnected(500))
    {
        if( colorRFID2 == "red" )
        {
            colorRFID2 = "green";
            mysqlObj->saveEventToLog(textCodec->toUnicode("������� ����������������� RFID-2 (���� ����) ������, ip: ") + rfid2IP + ", port: " + rfid2Port );
        }
    }
    else
    {
        if( colorRFID2 == "green" )
        {
            colorRFID2 = "red";
            mysqlObj->saveEventToLog(textCodec->toUnicode("������� ����������������� RFID-2 (���� ����) �� �������, ip: ") + rfid2IP + ", port: " + rfid2Port );
        }
    }
    qDebug() << "recconected_RFID2" << colorRFID2;
}

void AutoCards::readFRID_Slot()
{
    forever
    {
        QTcpSocket* pClientSocket = (QTcpSocket*)sender();
        QByteArray message;

        if(nextBlockSizeRFID == 0)
        {
            if(pClientSocket->bytesAvailable() < 6) break;

            message = pClientSocket->readAll();
        }
        if(nextBlockSizeRFID > pClientSocket->bytesAvailable()) break;

        char buff[ 100 ];
        memset( buff, 0, sizeof( buff ) );
        sprintf( buff, "%02x %02x %02x %02x %02x %02x" ,
            ( unsigned char )message.at( 0 ),
            ( unsigned char )message.at( 1 ),
            ( unsigned char )message.at( 2 ),
            ( unsigned char )message.at( 3 ),
            ( unsigned char )message.at( 4 ),
            ( unsigned char )message.at( 5 )
        );

        QString code(buff);
        qDebug() << "-> Code: " << code << QDateTime::currentDateTime().toString("hh:mm:ss.zzz");
        mysqlObj->saveEventToLog(textCodec->toUnicode("���������� ���� �����: ") + code);
        getRfidCode_Slot(code);
    }
}

void AutoCards::readFRID2_Slot()
{
    forever
    {
        QTcpSocket* pClientSocket = (QTcpSocket*)sender();
        QByteArray message;

        if(nextBlockSizeRFID2 == 0)
        {
            if(pClientSocket->bytesAvailable() < 6) break;

            message = pClientSocket->readAll();
        }
        if(nextBlockSizeRFID2 > pClientSocket->bytesAvailable()) break;

        char buff[ 100 ];
        memset( buff, 0, sizeof( buff ) );
        sprintf( buff, "%02x %02x %02x %02x %02x %02x" ,
            ( unsigned char )message.at( 0 ),
            ( unsigned char )message.at( 1 ),
            ( unsigned char )message.at( 2 ),
            ( unsigned char )message.at( 3 ),
            ( unsigned char )message.at( 4 ),
            ( unsigned char )message.at( 5 )
        );

        QString code(buff);
        qDebug() << "-> Code2: " << code << QDateTime::currentDateTime().toString("hh:mm:ss.zzz");
        mysqlObj->saveEventToLog(textCodec->toUnicode("���������� ���� ����� (���� ����): ") + code);
        getRfidCode_Slot(code);
    }
}



void AutoCards::socketsInitialization()
{
    QSettings settings( configurePath, QSettings::IniFormat );
    settings.setIniCodec( "Windows-1251" );

    int connected = 0;
    int connected2 = 0;


    QVector<QString> socket1_IPs;
    QVector<QString> socket1_ports;
    for(int i = 0; i < MAX_SOCKETS1 ; ++i)
    {
        if ( i < 10 )
        {
            socket1_IPs.append( settings.value( QString( "socket1/IP%1" ).arg(i+1) ).toString() );
            socket1_ports.append( settings.value( QString( "socket1/port%1" ).arg(i+1) ).toString() );
        }
        if ( i == 10 )  // ����2
        {
            socket1_IPs.append( settings.value( QString( "socket1_tara/IP1" ) ).toString() );
            socket1_ports.append( settings.value( QString( "socket1_tara/port1") ).toString() );
        }
        QTcpSocket *socket = new QTcpSocket( this );
        socket->disconnectFromHost();
        socket->connectToHost( socket1_IPs[i], socket1_ports[i].toInt() );

        qDebug() << "try connect s1: " << i << " ip: " << socket1_IPs[i] << " port: " << socket1_ports[i].toInt();
        mysqlObj->saveEventToLog(textCodec->toUnicode("������������� Socket-1 �") + QString::number( i + 1 ) + ", ip: " + socket1_IPs[i] + " port: " + socket1_ports[i]);


        if(socket->waitForConnected(100))
        {
            mysqlObj->saveEventToLog(textCodec->toUnicode("�����������"));
            connected++;
        }
        else
        {
            mysqlObj->saveEventToLog(textCodec->toUnicode("�� �����������!"));
        }
        connect( socket, SIGNAL( readyRead() ), this, SLOT( readSocket1_Slot() ) );
        socket->setObjectName( QString("%1").arg(i+1) );
        socket1s.append(socket);
        for(int j = 0; j < 4; ++j)
        {
            prevNum.append(0);
        }
    }

    QString socket3_IP = settings.value( "socket3/IP" ).toString();
    QString socket3_port = settings.value( "socket3/port" ).toString();
    nextBlockSizeSocket3 = 0;
    socket3 = new QTcpSocket( this );
    socket3->disconnectFromHost();
    socket3->connectToHost( socket3_IP, socket3_port.toInt() );
    mysqlObj->saveEventToLog(textCodec->toUnicode("������������� Socket-3") + ", ip: " + socket3_IP + " port: " + socket3_port);

    if(socket3->waitForConnected(100))
    {
        mysqlObj->saveEventToLog(textCodec->toUnicode("�����������"));
        //connected++;
    }
    else
    {
        mysqlObj->saveEventToLog(textCodec->toUnicode("�� �����������!"));
    }

//    ����2
    QVector<QString> socket2_IPs;
    QVector<QString> socket2_ports;
    nextBlockSizeSocket2 = 0;
    for(int i = 0; i < MAX_SOCKETS2; ++i)
    {
        socket2_IPs.append( settings.value( QString("socket2_tara/IP%1").arg(i+1) ).toString() );
        socket2_ports.append( settings.value( QString("socket2_tara/port%1").arg(i+1) ).toString() );

        QTcpSocket *socket = new QTcpSocket( this );
        socket->disconnectFromHost();
        socket->connectToHost( socket2_IPs[i], socket2_ports[i].toInt() );

        qDebug() << "try connect s2: " << i << " ip: " << socket2_IPs[i] << " port: " << socket2_ports[i].toInt();
        mysqlObj->saveEventToLog(textCodec->toUnicode("������������� Socket-2 (���� ����) �") +  QString::number(i + 1) + ", ip: " + socket2_IPs[i] + " port: " + socket2_ports[i]);


        if(socket->waitForConnected(100))
        {
            mysqlObj->saveEventToLog(textCodec->toUnicode("�����������"));
            connected2++;
        }
        else
        {
            mysqlObj->saveEventToLog(textCodec->toUnicode("�� �����������!"));
        }
        connect( tipsWindow, SIGNAL( card2Lamp_Signal(bool) ), this, SLOT( card2Lamp_Slot(bool)  ) );
        connect( tipsWindow, SIGNAL( tara2Lamp_Signal(bool) ), this, SLOT( tara2Lamp_Slot(bool)  ) );
        connect( tipsWindow, SIGNAL( error2Lamp_Signal(bool) ), this, SLOT( error2Lamp_Slot(bool)  ) );

        socket->setObjectName( QString("%1").arg(i+1) );
        socket2s.append(socket);
    }
    if(connected2)
    {
        colorSocket2 = "yellow";
        if(connected2 == (MAX_SOCKETS2)) colorSocket2 = "green";
    }

//    --

    if(connected)
    {
        colorSocket1 = "yellow";
        if(connected == (MAX_SOCKETS1) ) colorSocket1 = "green";
    }

    qDebug() << "Sockets connected: " << connected;

    connect( socket3, SIGNAL( readyRead() ), this, SLOT( readSocket3_Slot() ) );

    QTimer *timerSocketsWrite = new QTimer( this );
    timerSocketsWrite->setInterval( 15/*150*/ ); //--������
    connect( timerSocketsWrite, SIGNAL( timeout() ), this, SLOT( writeSockets_Slot() ) );
    timerSocketsWrite->start();

    QTimer *timerSocketsTemp = new QTimer( this );
    timerSocketsTemp->setInterval( 5000 );
    connect( timerSocketsTemp, SIGNAL( timeout() ), this, SLOT( socketsTemp_Slot() ) );
    timerSocketsTemp->start();
}

void AutoCards::writeSockets_Slot()
{
    static int current = 0;

    socket1s[current]->write( "2" );

    if(++current >= MAX_SOCKETS1) current = 0;
}

void AutoCards::socketsTemp_Slot()
{
    QByteArray data;
    data.append( (char)66 );
    socket3->write( data );
}

void AutoCards::readSocket1_Slot()
{
    // 31 00 00
    // 0x31 - ������� ����� ��������
    // 00..03 - ����� �����
    // 00 �������, 01 ���������

    // 32 01 01 01 01
    // 0x32 - ����� �� ������ ���������

    QTcpSocket* pClientSocket = (QTcpSocket*)sender();
    QString name = pClientSocket->objectName();
    int currentSocket = name.toInt();

    QByteArray message;
    forever
    {
        if(pClientSocket->bytesAvailable() < 5) break;
        message = pClientSocket->readAll();

        char data[5] = {0};
        data[0] = (char)message.at(0); // 32
        data[1] = (char)message.at(1);
        data[2] = (char)message.at(2);
        data[3] = (char)message.at(3);
        data[4] = (char)message.at(4);

        if( data[0] != 0x32 )
        {
            qDebug() << "unknown package from socket-1";
            return;
        }

        for(int i = 1; i <= 4; ++i)
        {
            int currentNum = data[i] * i;
            int currentButton = i + 4*(currentSocket-1);
            //--������
            //qDebug() << prevNum[currentButton-1] << currentNum;

            if(currentNum) // �� ����, �.�. ��������� ��������������
            {
                if(prevNum[currentButton-1] != currentNum) // �� ����� �����������, ����� �� ����������� ��� �������
                {
                    socketButtonPressed( currentButton ); // ������� ���� 1..4 �� ������� ����� 1..11
                }
            }
            prevNum[currentButton-1] = currentNum;
        }
    }
}

void AutoCards::readSocket2_Slot()
{
    // 21 00 00
    // 0x21 - ������� ����� ��������
    // 00..01 - ����� �����
    // 00 �������, 01 ���������

    // 23 00 01 00 01
    // 0x23 - ����� �� ������ ���������
    // 2 ����� - ��������� ������ (00 �������, 01 ���������)
    // 2 ����� - ��������� ���� (00 ����, 01 �������)

    QTcpSocket* pClientSocket = (QTcpSocket*)sender();
    QString name = pClientSocket->objectName();
    int currentSocket = name.toInt();

    QByteArray message;
    forever
    {
        if(pClientSocket->bytesAvailable() < 5) break;
        message = pClientSocket->readAll();

        char data[5] = {0};
        data[0] = (char)message.at(0); // 23
        data[1] = (char)message.at(1);
        data[2] = (char)message.at(2);
        data[3] = (char)message.at(3);
        data[4] = (char)message.at(4);

        if( data[0] != 0x23 )
        {
            qDebug() << "unknown package from socket-1";
            return;
        }

        for(int i = 1; i <= 4; ++i)
        {
            int currentNum = data[i] * i;
            int currentButton = i + 4*(currentSocket-1);

            //qDebug() << prevNum[currentButton-1] << currentNum;

            if(currentNum) // �� ����, �.�. ��������� ��������������
            {
                if(prevNum[currentButton-1] != currentNum) // �� ����� �����������, ����� �� ����������� ��� �������
                {
                    socketButtonPressed( currentButton ); // ������� ���� 1..4 �� ������� ����� 1..11
                }
            }
            prevNum[currentButton-1] = currentNum;
        }
    }
}


void AutoCards::readSocket3_Slot()
{
    QTcpSocket* pClientSocket = (QTcpSocket*)sender();

    QByteArray message;
    forever
    {
        if(nextBlockSizeSocket3 == 0)
        {
            if(pClientSocket->bytesAvailable() < 1) break;

            message = pClientSocket->readAll();
        }
        if(nextBlockSizeSocket3 > pClientSocket->bytesAvailable()) break;

        qDebug() << "Socket-3: " << message;

        if( (unsigned char)message.at( 0 ) == 0x41 )
        {
            tipsWindow->setAlarm(true);
            isStopped = true;
        }
        else if( (unsigned char)message.at( 0 ) == 0x42 ) // �����������
        {
            char temp = ( char )message.at( 1 );
            if(temp < 0)
            {
                temp -= 128;
                temp *= -1;
            }
            temperature = temp;
            mysqlObj->setTemperature( temperature );
        }
    }
}

void AutoCards::alarmLamp_Slot(bool set)
{
    char x;
    set ? x = 1 : x = 0;
    QByteArray data;
    data.append((char)0x43);
    data.append((char)0x00);
    data.append(x);
    data.append((char)0x00);
    socket3->write( data );
}

void AutoCards::batteryLamp_Slot(bool set)
{
    char x;
    set ? x = 1 : x = 0;
    QByteArray data;
    data.append((char)0x43);
    data.append((char)0x01);
    data.append(x);
    data.append((char)0x00);
    socket3->write( data );
}

void AutoCards::card2Lamp_Slot(bool set)
{
    //socket2 "1" (0) channel 0
    char x;
    set ? x = 1 : x = 0;
    QByteArray data;
    data.append((char)0x22);
    data.append((char)0x00);
    data.append(x);
    data.append((char)0x00);
    socket2s.at( 0 )->write( data );
}

void AutoCards::tara2Lamp_Slot(bool set)
{
    //socket2 "2" (1) channel 0
    char x;
    set ? x = 1 : x = 0;
    QByteArray data;
    data.append((char)0x22);
    data.append((char)0x00);
    data.append(x);
    data.append((char)0x00);
    socket2s.at( 1 )->write( data );
}
void AutoCards::error2Lamp_Slot(bool set)
{
    //socket2 "2" (1) channel 1
    char x;
    set ? x = 1 : x = 0;
    QByteArray data;
    data.append((char)0x22);
    data.append((char)0x01);
    data.append(x);
    data.append((char)0x00);
    socket2s.at( 1 )->write( data );
}
void AutoCards::socketButtonPressed(int num)
{
    //mysqlObj->saveEventToLog( QString( textCodec->toUnicode( "���������� �������, ���: %1, ������: %2." ) ).
    //    arg( num ).arg( tipsWindow->currentCommand ) );

    //QString code = "code: " + QString::number(num);
    //qDebug() << code;
    //label_test->setText(code);
    //label_test->show();

    QString str;

    if(num == CANCEL_BUTTON)
    {
        str = "cancel";
        beepRfid_Slot();
    }
    else if(num == TARA_BUTTON)
    {
        str = "tara";
        beepRfid_Slot();
    }
    else if(num == TARA_BUTTON2)
    {
        str = "tara2";
        beepRfid2_Slot();
    }
    else if(num == CANCEL_BUTTON2)
    {
        str = "cancel2";
        beepRfid2_Slot();
    }
    else
    {
        // �.�. ������� ��������� ���������� ������ �� �����1-����������� �� ����������,
        // ��� ����������� ����� �������� ��� ������ ����������� ����� ������
        // ������ ������� ������ �������� ���� ������������ � ��
        if(tipsWindow->currentCommand == 0)
        {
            return;
        }
        if(tipsWindow->currentCommand == 1)
        {
            if(num > 20) return;
        }
        else if(tipsWindow->currentCommand == 2)
        {
            if(num < 21 || num > 29) return;
        }
        else if(tipsWindow->currentCommand == 3)
        {
            if(num < 30 || num > 38) return;
        }
        else if( tipsWindow->currentCommand > 3)
        {
            return;
        }

        str = mysqlObj->getButtonInfo(num, tipsWindow->currentCommand);
        beepRfid_Slot();
    }

    mysqlObj->saveEventToLog( QString( textCodec->toUnicode( "�������� ������: %1, ���: %2." ) ).arg( str ).arg(num) );

    if(str == "error")
    {
        tipsWindow->noButton();
        return;
    }
    else if(isStopped)
    {
        return;
    }
    else if(str == "alarm" && !isStopped)
    {
        return;
    }
    else if(str == "cancel" || str == "cancel2" )
    {
        if(tipsWindow->isTaraMode)
        {
            tipsWindow->InitialPosition_Slot();
            return;
        }

        timerFinish->stop();
        tipsWindow->setPrevPhrase();
        return;
    }
    else if(tipsWindow->isTaraMode)
    {
        return;
    }
    else if( (str == "tara" || str == "tara2") && !tipsWindow->isTaraMode)
    {
        tipsWindow->setTaraPhrase();        
        return;
    }

    QStringList strList;
    strList.append(str);
    tipsWindow->setNextPhrase(strList);

    if(tipsWindow->currentCommand == 2)
    {
        record.material = str;
    }
    else if(tipsWindow->currentCommand == 3)
    {
        record.quarry = str;
    }
    else if(tipsWindow->currentCommand == 4)
    {
        record.unload = str;

        if( !mysqlObj->getLastTaraByCard( record.code ) > 0 )
        {
            tipsWindow->setNoTara();
        }

        timerFinish->start();
    }    
}

void AutoCards::timerFinish_Slot()
{
    tipsWindow->InitialPosition_Slot();
    emit startTableThread_Signal( record, photofix_struct.id );
}

void AutoCards::timerNonSleep_Slot()
{
     mysqlObj->getSettings();
     qDebug() << "noSleep";
}

void AutoCards::beepRfid_Slot()
{
    QByteArray data;
    data.append((char)0x1E);
    data.append((char)0x08);
    data.append((char)0x00);
    data.append((char)0xFF);
    data.append((char)0x00);
    data.append((char)0xFF);
    data.append((char)0x00);
    data.append((char)0xFF);
    data.append((char)0x00);
    data.append((char)0xFF);
    data.append((char)0x01);
    data.append((char)0x08);
    data.append((char)0x00);
    data.append((char)0x00);

    socketRFID->write( data );
}

void AutoCards::beepRfid2_Slot()
{
    QByteArray data;
    data.append((char)0x1E);
    data.append((char)0x08);
    data.append((char)0x00);
    data.append((char)0xFF);
    data.append((char)0x00);
    data.append((char)0xFF);
    data.append((char)0x00);
    data.append((char)0xFF);
    data.append((char)0x00);
    data.append((char)0xFF);
    data.append((char)0x01);
    data.append((char)0x08);
    data.append((char)0x00);
    data.append((char)0x00);

    socketRFID2->write( data );
}

void AutoCards::test_Slot()
{
  /*  static int click = 1;
    if(click == 1)
    {
        weight = 600;
        getRfidCode_Slot("1f 03 00 a6 69 3d");
        click++;
    }
    else if(click == 2)
    {
        socketButtonPressed(7);
        click++;
    }
    else if(click == 3)
    {
        socketButtonPressed(11);
        click++;
    }
    else if(click == 4)
    {
        socketButtonPressed(10);
        click++;
    }
    else if(click == 5)
    {
        socketButtonPressed(31);
        click++;
    }
    else if(click == 6)
    {
        socketButtonPressed(46);
        click = 1;
    }
    if(click == 1)
    {
        socketButtonPressed(62);
        click = 2;
    }
    else if(click == 2)
    {
        weight = 600;
        socketButtonPressed(20);
        click = 1;
    }

*/
}

void AutoCards::cancel_Slot()
{
    socketButtonPressed(61);
}

void AutoCards::restartMySQL_Slot( QString &err )
{
    QProcess::startDetached("cmd /c net start mysql");

    if ( err.compare( "Program start-up..." ) != 0 )
    {
        restartMySQLTimer->start();
        lastMySQLError = err;
		mysqlObj->blockSignals(true);
        tipsWindow->progress_restartMySQL( RESTART_MYSQL_MSECS );
        qDebug()<< "restartMySQL_Slot Error!!"<<endl;
        return;
    }
     qDebug()<< "restartMySQL_Slot"<<endl;

}

void AutoCards::finishedMySQLrestart_Slot()
{
    bool result = false;
    int attempts = 0;
    qDebug()<< "finishedMySQLrestart_Slot"<<endl;
    mysqlObj->blockSignals( false );
    mysqlObj->delay(400);
    if ( mysqlObj->reconnect()!= true )
    {
        qDebug()<<" Cannot reconnect to MySQL!" << endl;
    }
    do
    {
        mysqlObj->delay(200);
        result = mysqlObj->saveEventToLog(textCodec->toUnicode("������ MySQL ������������, ��������� ������: ") + lastMySQLError);
    }
    while(result == false && ++attempts < NUM_ATTEMPTS_MYSQL_CONN);
     qDebug()<<"finishedMySQLrestart_Slot, result: " <<result<<". Attempts: "<< attempts;
     //mysqlObj->stable_Slot();

}
bool AutoCards::try_reconnect()
{
   return mysqlObj->try_reconnect();
}
