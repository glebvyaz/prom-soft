#include "include/tips.h"
#include "ui_tips.h"

tips::tips(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::tips)
{
    ui->setupUi(this);
    textCodec = QTextCodec::codecForName( "Windows-1251" );

    current_exit_sec = 0;
    currentCommand = 0;
    isTaraMode = false;

    commandPhrases.append(textCodec->toUnicode( "��������� ���� �����\n" ));
    commandPhrases.append(textCodec->toUnicode( "�������� ��������" ));
    commandPhrases.append(textCodec->toUnicode( "�������� ����� �������" ));
    commandPhrases.append(textCodec->toUnicode( "�������� ����� ��������" ));
    commandPhrases.append(textCodec->toUnicode( "����������� ����" ));

    // ++
    // ���������� �� ������ �����
    // ��� � ���� ������

    pixmaps.append( QPixmap( picturePath.arg(0) ) );
    pixmaps.append( QPixmap( picturePath.arg(1) ) );
    pixmaps.append( QPixmap( picturePath.arg(2) ) );
    pixmaps.append( QPixmap( picturePath.arg(3) ) );
    pixmaps.append( QPixmap( picturePath.arg(4) ) );

    pixmap_tara.load( picturePath.arg("tara") );
    pixmap_tara_yes.load( picturePath.arg("tara_yes") );
    pixmap_tara_no.load( picturePath.arg("tara_no") );
    pixmap_alarm.load( picturePath.arg("alarm") );
    pixmap_no_tara.load( picturePath.arg("no_tara") );
    pixmap_no_card.load( picturePath.arg("no_card") );
    pixmap_no_button.load( picturePath.arg("no_button") );
    ui->progressBar->hide();

    mysqlObj = mysql::instance();    
}

tips::~tips()
{
    delete ui;
}

void tips::setFramesColors(QString colorRFID, QString colorSocket1)
{
    ui->frame_rfid->setStyleSheet(QString("background-color: %1; border: none;").arg(colorRFID));
    ui->frame_socket1->setStyleSheet(QString("background-color: %1; border: none;").arg(colorSocket1));
}
//����2
void tips::setFramesColors2(QString colorRFID2, QString colorSocket2)
{
    ui->frame_rfid2->setStyleSheet(QString("background-color: %1; border: none;").arg(colorRFID2));
    ui->frame_socket2->setStyleSheet(QString("background-color: %1; border: none;").arg(colorSocket2));
}
//--

void tips::setNextPhrase( QStringList strings )
{
    current_exit_sec = mysqlObj->WAIT_BEFORE_EXIT_SECS;
    ui->label_timerExit->setText( QString::number(current_exit_sec) );

    currentCommand++;
    if(currentCommand == commandPhrases.count()-1)
    {
        current_exit_sec = mysqlObj->WAIT_MESSAGE_MSECS/1000;
    }

    ui->label_tip->setPixmap( pixmaps[currentCommand] );

    switch (currentCommand) {
    case 1:
        ui->enter_time->setText( QDateTime::currentDateTime().toString("hh:mm dd.MM.yyyy") );
        ui->enter_weight->setText( strings[0] );
        ui->enter_numAuto->setText( strings[1] );
        ui->enter_driver->setText( strings[2] );
        break;
    case 2:
        ui->enter_material->setText( strings[0] );
        break;
    case 3:
        ui->enter_quarry->setText( strings[0] );
        break;
    case 4:
        ui->enter_unload->setText( strings[0] );
        break;
    default:
        break;
    }
}

void tips::setPrevPhrase()
{
    current_exit_sec = mysqlObj->WAIT_BEFORE_EXIT_SECS;
    ui->label_timerExit->setText( QString::number(current_exit_sec) );

    switch (currentCommand) {
    case 1:
        ui->enter_time->clear();
        ui->enter_weight->clear();
        ui->enter_driver->clear();
        ui->enter_numAuto->clear();
        break;
    case 2:
        ui->enter_material->clear();
        break;
    case 3:
        ui->enter_quarry->clear();
        break;
    case 4:
        ui->enter_unload->clear();
        break;
    default:
        break;
    }

    currentCommand--;
    if(currentCommand < 0)
    {
        InitialPosition_Slot();
        return;
    }
    ui->label_tip->setPixmap( pixmaps[currentCommand] );
}

void tips::setSecondWeightingPhrase( QStringList strings )
{
//    ui->label_tip->setText(QString(textCodec->toUnicode("�����������\n������ �����������\n����� = %1��").arg(strings[0])));

//    ui->enter_time->setText( QDateTime::currentDateTime().toString("hh:mm dd.MM.yyyy") );
//    ui->enter_weight->setText( strings[1] );
//    ui->enter_numAuto->setText( strings[2] );
//    ui->enter_driver->setText( strings[3] );

//    ui->enter_material->setText( strings[4] );
//    ui->enter_quarry->setText( strings[5] );
//    ui->enter_unload->setText( strings[6] );

//    QTimer::singleShot( WAIT_MESSAGE_MSECS, this, SLOT( InitialPosition_Slot() ) );
}

void tips::InitialPosition_Slot()
{

    current_exit_sec = 0;
    currentCommand = 0;
    isTaraMode = false;
    ui->progressBar->hide();

    //QPixmap myPixmap( picturePath.arg(currentCommand) );
    ui->label_tip->setPixmap( pixmaps[currentCommand] );
    ui->enter_time->clear();
    ui->enter_weight->clear();
    ui->enter_driver->clear();
    ui->enter_numAuto->clear();
    ui->enter_material->clear();
    ui->enter_quarry->clear();
    ui->enter_unload->clear();
//   ����2
    //emit card2Lamp_Signal( false );
    //emit tara2Lamp_Signal( true );
    //emit error2Lamp_Signal( false );
    QTimer::singleShot( 80, this, SLOT( Card2LampOff() ));
    QTimer::singleShot( 100, this, SLOT( Tara2LampOn() ));
    QTimer::singleShot( 120, this, SLOT( Error2LampOff() ));

//    --
}

void tips::setTaraPhrase()
{    
    current_exit_sec = mysqlObj->WAIT_BEFORE_EXIT_SECS;
    ui->label_timerExit->setText( QString::number(current_exit_sec) );
    isTaraMode = true;

    ui->label_tip->setPixmap( pixmap_tara );
    ui->enter_time->clear();
    ui->enter_weight->clear();
    ui->enter_driver->clear();
    ui->enter_numAuto->clear();
    ui->enter_material->clear();
    ui->enter_quarry->clear();
    ui->enter_unload->clear();

//  ����2

    //emit card2Lamp_Signal( true );
    //emit tara2Lamp_Signal( false );
    QTimer::singleShot( 100, this, SLOT( Card2LampOn() ));
    QTimer::singleShot( 120, this, SLOT( Tara2LampOff() ));


//  --

    qDebug() << "tara image";
}

void tips::setTaraPhrase(bool status, int weight, QStringList strings)
{
    if(status)
    {        
        ui->label_tip->setPixmap( pixmap_tara_yes );

        ui->enter_weight->setText( QString::number(weight) );
        ui->enter_numAuto->setText( strings[0] );
        ui->enter_driver->setText( strings[1] );

        ui->enter_material->clear();
        ui->enter_quarry->clear();
        ui->enter_unload->clear();

//        ����2
        //emit tara2Lamp_Signal( false );
        //emit card2Lamp_Signal( false );
        QTimer::singleShot( 100, this, SLOT( Tara2LampOff() ));
        QTimer::singleShot( 120, this, SLOT( Card2LampOff() ));

//        --

        qDebug() << "tara YES image";
    }
    else
    {        
        ui->label_tip->setPixmap( pixmap_tara_no );
        ui->enter_time->clear();
        ui->enter_weight->clear();
        ui->enter_driver->clear();
        ui->enter_numAuto->clear();
        ui->enter_material->clear();
        ui->enter_quarry->clear();
        ui->enter_unload->clear();
//        ����2
       //emit tara2Lamp_Signal( true );
       //emit error2Lamp_Signal( true );
        QTimer::singleShot( 100, this, SLOT( Tara2LampOn() ));
        QTimer::singleShot( 120, this, SLOT( Error2LampOn() ));

//        --


        qDebug() << "tara NO image";
    }

    QTimer::singleShot( mysqlObj->WAIT_MESSAGE_MSECS, this, SLOT( InitialPosition_Slot() ) );
    current_exit_sec = mysqlObj->WAIT_MESSAGE_MSECS/1000;
}

void tips::setAlarm(bool state)
{    
    ui->label_tip->setPixmap( pixmap_alarm );
    //        ����2
    emit error2Lamp_Signal( true );
    //        --
}

void tips::setNoTara()
{
    ui->label_tip->setPixmap( pixmap_no_tara );
    //        ����2
    //emit tara2Lamp_Signal( true );
    //emit error2Lamp_Signal( true );
    QTimer::singleShot( 100, this, SLOT( Tara2LampOn() ));
    QTimer::singleShot( 120, this, SLOT( Error2LampOn() ));

    //        --

}

void tips::setNoTara( int a )
{
    ui->label_tip->setPixmap( pixmap_no_tara );
    //        ����2
    //emit tara2Lamp_Signal( true );
    //emit error2Lamp_Signal( true );
    QTimer::singleShot( 100, this, SLOT( Tara2LampOn() ));
    QTimer::singleShot( 120, this, SLOT( Error2LampOn() ));

    //        --


    QTimer::singleShot( mysqlObj->WAIT_MESSAGE_MSECS, this, SLOT( InitialPosition_Slot() ) );
    current_exit_sec = mysqlObj->WAIT_MESSAGE_MSECS/1000;
}

void tips::setWeightLabel1Sec(int weight)
{
    if(weight == -1)    ui->label_currentWeight->setText( textCodec->toUnicode("��� �����") );
    else                ui->label_currentWeight->setText( QString::number(weight) );

    if(current_exit_sec != 0)
    {
        current_exit_sec--;
        ui->label_timerExit->setText( QString::number(current_exit_sec) );
        ui->label_timerExit->show();

        if(current_exit_sec == 0)
        {
            InitialPosition_Slot();
        }
    }
    else
    {
        ui->label_timerExit->setText( "--" );
        ui->label_timerExit->hide();
    }
}

void tips::noCardReg()
{
    ui->label_tip->setPixmap( pixmap_no_card );
    ui->enter_time->clear();
    ui->enter_weight->clear();
    ui->enter_driver->clear();
    ui->enter_numAuto->clear();
    ui->enter_material->clear();
    ui->enter_quarry->clear();
    ui->enter_unload->clear();
    //        ����2
    //emit card2Lamp_Signal( true );
    //emit error2Lamp_Signal( true );
    QTimer::singleShot( 80, this, SLOT( Tara2LampOff() ));
    QTimer::singleShot( 100, this, SLOT( Card2LampOn() ));
    QTimer::singleShot( 120, this, SLOT( Error2LampOn() ));

    //        --


    QTimer::singleShot( mysqlObj->WAIT_MESSAGE_MSECS, this, SLOT( InitialPosition_Slot() ) );
    current_exit_sec = mysqlObj->WAIT_MESSAGE_MSECS/1000;
}

void tips::noButton()
{
    ui->label_tip->setPixmap( pixmap_no_button );
    current_exit_sec = mysqlObj->WAIT_BEFORE_EXIT_SECS;
    ui->label_timerExit->setText( QString::number(current_exit_sec) );
}

void tips::Card2LampOn()
{
    emit card2Lamp_Signal(true);
}
void tips::Tara2LampOn()
{
    emit tara2Lamp_Signal(true);
}
void tips::Error2LampOn()
{
    emit error2Lamp_Signal(true);
}
void tips::Card2LampOff()
{
    emit card2Lamp_Signal(false);
}
void tips::Tara2LampOff()
{
    emit tara2Lamp_Signal(false);
}
void tips::Error2LampOff()
{
    emit error2Lamp_Signal(false);
}

void tips::progress_restartMySQL(int delay)
{
   int secs = 0;
   QString prev_txt_label = ui->label_10->text();
   QString prev_style_label = ui->label_10->styleSheet();
   ui->label_10->setStyleSheet("font: 12pt \"MS Shell Dlg 2\"; color: rgb(255, 64, 64); font-weight:600;");

    ui->progressBar->setMinimum( 0 );
    ui->progressBar->setMaximum( delay / 1000 );
    ui->progressBar->setValue( 0 );
    ui->progressBar->setVisible(true);

    QDateTime dtime = QDateTime::currentDateTime().addSecs(1);
    ui->label_10->setText(textCodec->toUnicode("���������� ��"));
    forever
    {
        if ( dtime <= QDateTime::currentDateTime() )
        {
            secs++;
            dtime = dtime.addSecs(1);
            ui->progressBar->setValue( secs );
            if ( secs >= delay / 1000 - 1 ) break;
        }
        else
        {
            QCoreApplication::processEvents( QEventLoop::AllEvents, delay / 1000 / 10 );
        }
    }
    ui->label_10->setStyleSheet( prev_style_label );
    ui->label_10->setText( prev_txt_label );
    ui->progressBar->setVisible(false);

}
