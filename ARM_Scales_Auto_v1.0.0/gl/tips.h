#ifndef TIPS_H
#define TIPS_H

#include <QWidget>
#include <QTextCodec>
#include <QDateTime>
#include <QTimer>
#include <QDebug>
#include "include/db_driver.h"

namespace Ui {
class tips;
}

class tips : public QWidget
{
    Q_OBJECT

public:
    explicit tips(QWidget *parent = 0);
    ~tips();

    int current_exit_sec;
    mysql *mysqlObj;

    QTextCodec *textCodec;
    QStringList commandPhrases;
    int currentCommand;
    bool isTaraMode;

    QVector<QPixmap> pixmaps;
    QPixmap pixmap_tara;
    QPixmap pixmap_tara_yes;
    QPixmap pixmap_tara_no;
    QPixmap pixmap_alarm;
    QPixmap pixmap_no_tara;
    QPixmap pixmap_no_card;
    QPixmap pixmap_no_button;

    void setFramesColors(QString colorRFID, QString colorSocket1);
    void setFramesColors2(QString colorRFID2, QString colorSocket2);
    void setNextPhrase( QStringList strings );
    void setPrevPhrase();
    void setSecondWeightingPhrase( QStringList strings );
    void setTaraPhrase();
    void setTaraPhrase(bool status, int weight, QStringList strings);
    void setAlarm(bool state);
    void setNoTara();
    void setNoTara( int a );
    void setWeightLabel1Sec(int weight);
    void noCardReg();
    void noButton();
    void progress_restartMySQL(int delay);



private:
    Ui::tips *ui;

public slots:
    void InitialPosition_Slot();
//    ����2
    void Card2LampOn();
    void Tara2LampOn();
    void Error2LampOn();
    void Card2LampOff();
    void Tara2LampOff();
    void Error2LampOff();
//    --

signals:
    void tara2Lamp_Signal(bool);
    void card2Lamp_Signal(bool);
    void error2Lamp_Signal(bool);
};

#endif // TIPS_H
