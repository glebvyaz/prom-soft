#ifndef CARDS_H
#define CARDS_H

#include <QtWidgets/QMainWindow>
#include "ui_autocards.h"
#include <QProgressBar>
#include <QLabel>
#include <QTextCodec>
#include <QString>
#include <QThread>
#include <QCloseEvent>
#include <QVector>

#include "include/db_driver.h"
#include "include/loginWindow.h"
#include "include/events_form.h"
#include "include/settings_form.h"
#include "include/spravochniki_form.h"
#include "include/reports_form.h"
#include "include/AcceptDataStruct.h"
#include "include/dialogPreview.h"
#include "include/tips.h"


class AutoCards : public QMainWindow
{
	Q_OBJECT

public:
	AutoCards(QWidget *parent = 0);
	~AutoCards();

    void delay( int millisecondsToWait );
    bool try_reconnect();
	void setUpGraphics();
	void setupTimer();
    void setupSignalSlots();
	void closeEvent( QCloseEvent *event );

	void moxaInitialization();
    QTcpSocket* socketMoxaPort1;
    QTcpSocket* socketMoxaPort2;
    QTcpSocket* socketMoxaPort4;
    int nextBlockSizeMoxa1;
    int nextBlockSizeMoxa2;
    int nextBlockSizeMoxa4;

    tips *tipsWindow;
    int weight;
    QString tabloZnak;
    QString tabloColor;
    const int MIN_POROG;
    const int MIN_WEIGHT;

    struct UPS {
        float inputVoltage;
        float outputVoltage;
    };
    UPS ups;

    void rfidInitialization();
    QTcpSocket* socketRFID;
    int nextBlockSizeRFID;

//    ����2
    void rfid2Initialization();
    QTcpSocket* socketRFID2;
    int nextBlockSizeRFID2;
//    --

    void socketsInitialization();
    QVector<QTcpSocket*> socket1s;
    QVector<int> prevNum;
    QTcpSocket* socket3;
    int nextBlockSizeSocket3;
//    ����2
    QVector<QTcpSocket*> socket2s;
    int nextBlockSizeSocket2;
//    --
    void socketButtonPressed(int num);

    bool isStopped;
    QString colorRFID;
    QString colorSocket1;
//    ����2
    QString colorRFID2;
    QString colorSocket2;
//    --
    int temperature;

    QTimer *timerFinish;

    int TARA_BUTTON;
    int CANCEL_BUTTON;
//    ����2
    int TARA_BUTTON2;
    int CANCEL_BUTTON2;

private:
	bool isMoxaConnected;	// ��� 5 ��� �������� �������
    bool isMoxaON_flag;		// ���� ��� ����������� � ����������

	Ui::cards_forpaladysanya ui;
	QTextCodec *textCodec;
	QLabel *statusLabel;
	QProgressBar *progrBarMainTable;
    QTimer *sysTimer;
    QTimer *chkDbTimer;
    QTimer *restartMySQLTimer;
    QString lastMySQLError;

	int row;
    int column;

    QString rfidIP;
    QString rfidPort;
 //    ����2
    QString rfid2IP;
    QString rfid2Port;
//   --
    //QLabel *label_test;

	QList< QString > getHeadersMainTable()
	{
		QList< QString > list;

		list.append( textCodec->toUnicode( "����/�����\n ��������" ) );
        list.append( textCodec->toUnicode( "�����,\n ��" ) );
		list.append( textCodec->toUnicode( "����� ����" ) );
		list.append( textCodec->toUnicode( "�.�.�. ��������" ) );
        list.append( textCodec->toUnicode( "��������" ) );
        list.append( textCodec->toUnicode( "������" ) );
        list.append( textCodec->toUnicode( "��������" ) );

		list.append( textCodec->toUnicode( "� ��������" ) );
		list.append( textCodec->toUnicode( "������" ) );
        list.append( textCodec->toUnicode( "����" ) );
		list.append( textCodec->toUnicode( "��� RFID" ) );

		// list.append( textCodec->toUnicode( "��������� �� ����" ) );

	    return list;
    }

private slots:
	void sysTimer_Slot();
	void showMainWindow();
    void showLoginWindow();
    void getRfidCode_Slot( QString rfid_code );
    void tableViewMain_doubleClicked( const QModelIndex & index );
	void buttonBlockRec_Slot();
	void tableViewMain_Clicked( const QModelIndex & index );
	void buttonSerachAuto_Slot();
	void showHelp();

    void buttonAuto_Slot();

    void timerMoxaConnection_Slot();
    void writeMoxa_Slot();
    void socketsTemp_Slot();
    void readMoxaPort1_Slot();
    void readMoxaPort2_Slot();
    void readMoxaPort4_Slot();

    void readFRID_Slot();
    void reconnectRFID_Slot();

//    ����2
    void readFRID2_Slot();
    void reconnectRFID2_Slot();
    void beepRfid2_Slot();

    void card2Lamp_Slot(bool set);
    void tara2Lamp_Slot(bool set);
    void error2Lamp_Slot(bool set);

//    --

    void writeSockets_Slot();
    void readSocket1_Slot();
    void readSocket2_Slot();
    void readSocket3_Slot();    
    void alarmLamp_Slot(bool set);
    void batteryLamp_Slot(bool set);

    void timerFinish_Slot();
    void timerNonSleep_Slot();

    void test_Slot();
    void cancel_Slot();

    void beepRfid_Slot();

    void construct();
    void restartMySQL_Slot(QString & );
    void finishedMySQLrestart_Slot();

signals:
    void startTableThread_Signal( Record, int );
};

#endif // CARDS_H
