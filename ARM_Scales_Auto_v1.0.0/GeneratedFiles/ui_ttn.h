/********************************************************************************
** Form generated from reading UI file 'ttn.ui'
**
** Created: Wed 3. Feb 17:45:06 2021
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TTN_H
#define UI_TTN_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QTableWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Form
{
public:
    QLabel *labelTitle;
    QLineEdit *enterNumberNakladnaya;
    QLabel *labelNum;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *enterDate;
    QLineEdit *enterMonth;
    QLabel *labelNum_2;
    QLineEdit *enterYear;
    QLabel *labelNum_3;
    QLabel *labelAuto;
    QLineEdit *enterAutoType;
    QLabel *labelAutoTypes;
    QLabel *labelAuto_2;
    QLabel *labelAutoTypes_2;
    QLineEdit *enterPricepType;
    QLabel *labelAuto_3;
    QLineEdit *enterTypePerevoski;
    QLabel *labelAutoTransporter;
    QLineEdit *enterAutoTransporter;
    QLabel *labelAutoVoditel;
    QLineEdit *enterNameTransporter;
    QLabel *labelAutoTransporterFIO;
    QLabel *labelAutoTransporterLicenceNumber;
    QLabel *labelOrderer;
    QLineEdit *enterEnterOrder;
    QLabel *labelAutoOrdererFIO;
    QLabel *labelSender;
    QLineEdit *enterSender;
    QLabel *labelAutoSenderFIO;
    QLabel *labelAccepter;
    QLabel *labelAutoAccepterFIO;
    QLineEdit *enterAccepter;
    QLabel *labelPointOfLoad;
    QLineEdit *enterPointOfLoading;
    QLabel *labelAddress1;
    QLabel *labelPointUnLoad;
    QLabel *labelAddress2;
    QLineEdit *enterPointOf_UnLoading;
    QLabel *labelPereaddress;
    QLineEdit *enterPereaddres;
    QLabel *labelNamePereAddr;
    QLabel *labelDoverenost;
    QLineEdit *enterSeriaNumber;
    QLabel *labelNumber;
    QLineEdit *enterNumberDoverennost;
    QLabel *labelNumber_2;
    QLineEdit *enterDoverennostDate;
    QLineEdit *enterDoverennostMonth;
    QLineEdit *enterDoverennostYear;
    QLabel *labelNum_4;
    QLabel *labelNum_5;
    QLineEdit *enterRules;
    QLabel *labelVantash;
    QLineEdit *enterGoodsState;
    QLabel *labelNum_6;
    QLabel *labelRulsTransport;
    QLineEdit *enterDoverennostVidano;
    QLabel *labelNumberPlaces;
    QLineEdit *enterNetto;
    QLabel *labelBuWords;
    QLabel *labelBrutto;
    QLineEdit *enterBrutto;
    QLineEdit *enterEcpeditor;
    QLabel *labelFIO_Excpeditor;
    QLabel *labelBuWords_2;
    QLabel *labelBrutto_2;
    QLabel *labelBuhgalterName;
    QLineEdit *enterBuhgalterName;
    QLabel *labelFIO_Buhgalter;
    QLabel *labelBuhgalterName_2;
    QLineEdit *enterBuhgalterName_2;
    QLabel *labelFIO_Buhgalter_2;
    QLabel *labelTotalSold;
    QLineEdit *enterTotalSold;
    QLabel *labelPDV;
    QLineEdit *enterPDV;
    QLabel *labelSuprovodniDoc;
    QLineEdit *enterSuprovodniDoc;
    QLabel *labelTransportServices;
    QLineEdit *enterTransportServices;
    QLineEdit *enterOthers;
    QPushButton *pushButtonAccept;
    QPushButton *pushButtonClose;
    QPushButton *pushButtonPrint;
    QLabel *label_3;
    QTableWidget *tableWidget;

    void setupUi(QWidget *Form)
    {
        if (Form->objectName().isEmpty())
            Form->setObjectName(QString::fromUtf8("Form"));
        Form->setEnabled(true);
        Form->resize(1095, 781);
        labelTitle = new QLabel(Form);
        labelTitle->setObjectName(QString::fromUtf8("labelTitle"));
        labelTitle->setGeometry(QRect(420, 10, 231, 16));
        enterNumberNakladnaya = new QLineEdit(Form);
        enterNumberNakladnaya->setObjectName(QString::fromUtf8("enterNumberNakladnaya"));
        enterNumberNakladnaya->setGeometry(QRect(310, 30, 91, 20));
        labelNum = new QLabel(Form);
        labelNum->setObjectName(QString::fromUtf8("labelNum"));
        labelNum->setGeometry(QRect(280, 30, 21, 16));
        label = new QLabel(Form);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(410, 30, 16, 16));
        label_2 = new QLabel(Form);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(470, 30, 16, 16));
        enterDate = new QLineEdit(Form);
        enterDate->setObjectName(QString::fromUtf8("enterDate"));
        enterDate->setGeometry(QRect(420, 30, 41, 20));
        enterMonth = new QLineEdit(Form);
        enterMonth->setObjectName(QString::fromUtf8("enterMonth"));
        enterMonth->setGeometry(QRect(480, 30, 151, 20));
        labelNum_2 = new QLabel(Form);
        labelNum_2->setObjectName(QString::fromUtf8("labelNum_2"));
        labelNum_2->setGeometry(QRect(640, 30, 16, 16));
        enterYear = new QLineEdit(Form);
        enterYear->setObjectName(QString::fromUtf8("enterYear"));
        enterYear->setGeometry(QRect(660, 30, 31, 20));
        labelNum_3 = new QLabel(Form);
        labelNum_3->setObjectName(QString::fromUtf8("labelNum_3"));
        labelNum_3->setGeometry(QRect(700, 30, 21, 16));
        labelAuto = new QLabel(Form);
        labelAuto->setObjectName(QString::fromUtf8("labelAuto"));
        labelAuto->setGeometry(QRect(30, 60, 71, 16));
        enterAutoType = new QLineEdit(Form);
        enterAutoType->setObjectName(QString::fromUtf8("enterAutoType"));
        enterAutoType->setGeometry(QRect(120, 60, 251, 20));
        labelAutoTypes = new QLabel(Form);
        labelAutoTypes->setObjectName(QString::fromUtf8("labelAutoTypes"));
        labelAutoTypes->setGeometry(QRect(120, 80, 251, 16));
        labelAuto_2 = new QLabel(Form);
        labelAuto_2->setObjectName(QString::fromUtf8("labelAuto_2"));
        labelAuto_2->setGeometry(QRect(380, 60, 111, 16));
        labelAutoTypes_2 = new QLabel(Form);
        labelAutoTypes_2->setObjectName(QString::fromUtf8("labelAutoTypes_2"));
        labelAutoTypes_2->setGeometry(QRect(480, 80, 251, 16));
        enterPricepType = new QLineEdit(Form);
        enterPricepType->setObjectName(QString::fromUtf8("enterPricepType"));
        enterPricepType->setGeometry(QRect(500, 60, 201, 20));
        labelAuto_3 = new QLabel(Form);
        labelAuto_3->setObjectName(QString::fromUtf8("labelAuto_3"));
        labelAuto_3->setGeometry(QRect(710, 60, 101, 16));
        enterTypePerevoski = new QLineEdit(Form);
        enterTypePerevoski->setObjectName(QString::fromUtf8("enterTypePerevoski"));
        enterTypePerevoski->setGeometry(QRect(810, 60, 271, 20));
        labelAutoTransporter = new QLabel(Form);
        labelAutoTransporter->setObjectName(QString::fromUtf8("labelAutoTransporter"));
        labelAutoTransporter->setGeometry(QRect(30, 100, 161, 16));
        enterAutoTransporter = new QLineEdit(Form);
        enterAutoTransporter->setObjectName(QString::fromUtf8("enterAutoTransporter"));
        enterAutoTransporter->setGeometry(QRect(190, 100, 511, 20));
        labelAutoVoditel = new QLabel(Form);
        labelAutoVoditel->setObjectName(QString::fromUtf8("labelAutoVoditel"));
        labelAutoVoditel->setGeometry(QRect(710, 100, 41, 16));
        enterNameTransporter = new QLineEdit(Form);
        enterNameTransporter->setObjectName(QString::fromUtf8("enterNameTransporter"));
        enterNameTransporter->setGeometry(QRect(760, 100, 321, 20));
        labelAutoTransporterFIO = new QLabel(Form);
        labelAutoTransporterFIO->setObjectName(QString::fromUtf8("labelAutoTransporterFIO"));
        labelAutoTransporterFIO->setGeometry(QRect(390, 120, 131, 16));
        labelAutoTransporterLicenceNumber = new QLabel(Form);
        labelAutoTransporterLicenceNumber->setObjectName(QString::fromUtf8("labelAutoTransporterLicenceNumber"));
        labelAutoTransporterLicenceNumber->setGeometry(QRect(830, 120, 201, 16));
        labelOrderer = new QLabel(Form);
        labelOrderer->setObjectName(QString::fromUtf8("labelOrderer"));
        labelOrderer->setGeometry(QRect(30, 140, 61, 16));
        enterEnterOrder = new QLineEdit(Form);
        enterEnterOrder->setObjectName(QString::fromUtf8("enterEnterOrder"));
        enterEnterOrder->setGeometry(QRect(120, 140, 961, 20));
        labelAutoOrdererFIO = new QLabel(Form);
        labelAutoOrdererFIO->setObjectName(QString::fromUtf8("labelAutoOrdererFIO"));
        labelAutoOrdererFIO->setGeometry(QRect(480, 160, 131, 16));
        labelSender = new QLabel(Form);
        labelSender->setObjectName(QString::fromUtf8("labelSender"));
        labelSender->setGeometry(QRect(30, 180, 131, 16));
        enterSender = new QLineEdit(Form);
        enterSender->setObjectName(QString::fromUtf8("enterSender"));
        enterSender->setGeometry(QRect(160, 180, 921, 20));
        labelAutoSenderFIO = new QLabel(Form);
        labelAutoSenderFIO->setObjectName(QString::fromUtf8("labelAutoSenderFIO"));
        labelAutoSenderFIO->setGeometry(QRect(370, 200, 411, 16));
        labelAccepter = new QLabel(Form);
        labelAccepter->setObjectName(QString::fromUtf8("labelAccepter"));
        labelAccepter->setGeometry(QRect(30, 220, 121, 16));
        labelAutoAccepterFIO = new QLabel(Form);
        labelAutoAccepterFIO->setObjectName(QString::fromUtf8("labelAutoAccepterFIO"));
        labelAutoAccepterFIO->setGeometry(QRect(370, 240, 371, 16));
        enterAccepter = new QLineEdit(Form);
        enterAccepter->setObjectName(QString::fromUtf8("enterAccepter"));
        enterAccepter->setGeometry(QRect(160, 220, 921, 20));
        labelPointOfLoad = new QLabel(Form);
        labelPointOfLoad->setObjectName(QString::fromUtf8("labelPointOfLoad"));
        labelPointOfLoad->setGeometry(QRect(30, 260, 131, 16));
        enterPointOfLoading = new QLineEdit(Form);
        enterPointOfLoading->setObjectName(QString::fromUtf8("enterPointOfLoading"));
        enterPointOfLoading->setGeometry(QRect(160, 260, 361, 20));
        labelAddress1 = new QLabel(Form);
        labelAddress1->setObjectName(QString::fromUtf8("labelAddress1"));
        labelAddress1->setGeometry(QRect(280, 280, 131, 16));
        labelPointUnLoad = new QLabel(Form);
        labelPointUnLoad->setObjectName(QString::fromUtf8("labelPointUnLoad"));
        labelPointUnLoad->setGeometry(QRect(530, 260, 151, 16));
        labelAddress2 = new QLabel(Form);
        labelAddress2->setObjectName(QString::fromUtf8("labelAddress2"));
        labelAddress2->setGeometry(QRect(790, 280, 131, 16));
        enterPointOf_UnLoading = new QLineEdit(Form);
        enterPointOf_UnLoading->setObjectName(QString::fromUtf8("enterPointOf_UnLoading"));
        enterPointOf_UnLoading->setGeometry(QRect(680, 260, 401, 20));
        labelPereaddress = new QLabel(Form);
        labelPereaddress->setObjectName(QString::fromUtf8("labelPereaddress"));
        labelPereaddress->setGeometry(QRect(30, 300, 191, 16));
        enterPereaddres = new QLineEdit(Form);
        enterPereaddres->setObjectName(QString::fromUtf8("enterPereaddres"));
        enterPereaddres->setGeometry(QRect(220, 300, 861, 20));
        labelNamePereAddr = new QLabel(Form);
        labelNamePereAddr->setObjectName(QString::fromUtf8("labelNamePereAddr"));
        labelNamePereAddr->setGeometry(QRect(400, 320, 421, 16));
        labelDoverenost = new QLabel(Form);
        labelDoverenost->setObjectName(QString::fromUtf8("labelDoverenost"));
        labelDoverenost->setGeometry(QRect(30, 350, 311, 16));
        enterSeriaNumber = new QLineEdit(Form);
        enterSeriaNumber->setObjectName(QString::fromUtf8("enterSeriaNumber"));
        enterSeriaNumber->setGeometry(QRect(340, 350, 41, 20));
        labelNumber = new QLabel(Form);
        labelNumber->setObjectName(QString::fromUtf8("labelNumber"));
        labelNumber->setGeometry(QRect(390, 350, 21, 16));
        enterNumberDoverennost = new QLineEdit(Form);
        enterNumberDoverennost->setObjectName(QString::fromUtf8("enterNumberDoverennost"));
        enterNumberDoverennost->setGeometry(QRect(410, 350, 51, 20));
        labelNumber_2 = new QLabel(Form);
        labelNumber_2->setObjectName(QString::fromUtf8("labelNumber_2"));
        labelNumber_2->setGeometry(QRect(470, 350, 21, 16));
        enterDoverennostDate = new QLineEdit(Form);
        enterDoverennostDate->setObjectName(QString::fromUtf8("enterDoverennostDate"));
        enterDoverennostDate->setGeometry(QRect(500, 350, 31, 20));
        enterDoverennostMonth = new QLineEdit(Form);
        enterDoverennostMonth->setObjectName(QString::fromUtf8("enterDoverennostMonth"));
        enterDoverennostMonth->setGeometry(QRect(540, 350, 101, 20));
        enterDoverennostYear = new QLineEdit(Form);
        enterDoverennostYear->setObjectName(QString::fromUtf8("enterDoverennostYear"));
        enterDoverennostYear->setGeometry(QRect(680, 350, 31, 20));
        labelNum_4 = new QLabel(Form);
        labelNum_4->setObjectName(QString::fromUtf8("labelNum_4"));
        labelNum_4->setGeometry(QRect(650, 350, 21, 16));
        labelNum_5 = new QLabel(Form);
        labelNum_5->setObjectName(QString::fromUtf8("labelNum_5"));
        labelNum_5->setGeometry(QRect(720, 350, 81, 16));
        enterRules = new QLineEdit(Form);
        enterRules->setObjectName(QString::fromUtf8("enterRules"));
        enterRules->setGeometry(QRect(930, 380, 151, 20));
        labelVantash = new QLabel(Form);
        labelVantash->setObjectName(QString::fromUtf8("labelVantash"));
        labelVantash->setGeometry(QRect(30, 380, 301, 16));
        enterGoodsState = new QLineEdit(Form);
        enterGoodsState->setObjectName(QString::fromUtf8("enterGoodsState"));
        enterGoodsState->setGeometry(QRect(330, 380, 151, 20));
        labelNum_6 = new QLabel(Form);
        labelNum_6->setObjectName(QString::fromUtf8("labelNum_6"));
        labelNum_6->setGeometry(QRect(330, 400, 181, 16));
        labelRulsTransport = new QLabel(Form);
        labelRulsTransport->setObjectName(QString::fromUtf8("labelRulsTransport"));
        labelRulsTransport->setGeometry(QRect(490, 380, 441, 20));
        enterDoverennostVidano = new QLineEdit(Form);
        enterDoverennostVidano->setObjectName(QString::fromUtf8("enterDoverennostVidano"));
        enterDoverennostVidano->setGeometry(QRect(800, 350, 281, 20));
        labelNumberPlaces = new QLabel(Form);
        labelNumberPlaces->setObjectName(QString::fromUtf8("labelNumberPlaces"));
        labelNumberPlaces->setGeometry(QRect(30, 430, 111, 16));
        enterNetto = new QLineEdit(Form);
        enterNetto->setObjectName(QString::fromUtf8("enterNetto"));
        enterNetto->setGeometry(QRect(140, 430, 171, 20));
        labelBuWords = new QLabel(Form);
        labelBuWords->setObjectName(QString::fromUtf8("labelBuWords"));
        labelBuWords->setGeometry(QRect(140, 450, 71, 16));
        labelBrutto = new QLabel(Form);
        labelBrutto->setObjectName(QString::fromUtf8("labelBrutto"));
        labelBrutto->setGeometry(QRect(330, 430, 101, 16));
        enterBrutto = new QLineEdit(Form);
        enterBrutto->setObjectName(QString::fromUtf8("enterBrutto"));
        enterBrutto->setGeometry(QRect(430, 430, 201, 20));
        enterEcpeditor = new QLineEdit(Form);
        enterEcpeditor->setObjectName(QString::fromUtf8("enterEcpeditor"));
        enterEcpeditor->setGeometry(QRect(820, 430, 261, 20));
        labelFIO_Excpeditor = new QLabel(Form);
        labelFIO_Excpeditor->setObjectName(QString::fromUtf8("labelFIO_Excpeditor"));
        labelFIO_Excpeditor->setGeometry(QRect(790, 450, 141, 16));
        labelBuWords_2 = new QLabel(Form);
        labelBuWords_2->setObjectName(QString::fromUtf8("labelBuWords_2"));
        labelBuWords_2->setGeometry(QRect(360, 450, 81, 16));
        labelBrutto_2 = new QLabel(Form);
        labelBrutto_2->setObjectName(QString::fromUtf8("labelBrutto_2"));
        labelBrutto_2->setGeometry(QRect(650, 430, 161, 16));
        labelBuhgalterName = new QLabel(Form);
        labelBuhgalterName->setObjectName(QString::fromUtf8("labelBuhgalterName"));
        labelBuhgalterName->setGeometry(QRect(30, 480, 331, 16));
        enterBuhgalterName = new QLineEdit(Form);
        enterBuhgalterName->setObjectName(QString::fromUtf8("enterBuhgalterName"));
        enterBuhgalterName->setGeometry(QRect(360, 480, 281, 20));
        labelFIO_Buhgalter = new QLabel(Form);
        labelFIO_Buhgalter->setObjectName(QString::fromUtf8("labelFIO_Buhgalter"));
        labelFIO_Buhgalter->setGeometry(QRect(400, 500, 141, 16));
        labelBuhgalterName_2 = new QLabel(Form);
        labelBuhgalterName_2->setObjectName(QString::fromUtf8("labelBuhgalterName_2"));
        labelBuhgalterName_2->setGeometry(QRect(650, 480, 111, 16));
        enterBuhgalterName_2 = new QLineEdit(Form);
        enterBuhgalterName_2->setObjectName(QString::fromUtf8("enterBuhgalterName_2"));
        enterBuhgalterName_2->setGeometry(QRect(780, 480, 301, 20));
        labelFIO_Buhgalter_2 = new QLabel(Form);
        labelFIO_Buhgalter_2->setObjectName(QString::fromUtf8("labelFIO_Buhgalter_2"));
        labelFIO_Buhgalter_2->setGeometry(QRect(830, 500, 201, 16));
        labelTotalSold = new QLabel(Form);
        labelTotalSold->setObjectName(QString::fromUtf8("labelTotalSold"));
        labelTotalSold->setGeometry(QRect(30, 520, 221, 16));
        enterTotalSold = new QLineEdit(Form);
        enterTotalSold->setObjectName(QString::fromUtf8("enterTotalSold"));
        enterTotalSold->setGeometry(QRect(240, 520, 581, 20));
        labelPDV = new QLabel(Form);
        labelPDV->setObjectName(QString::fromUtf8("labelPDV"));
        labelPDV->setGeometry(QRect(830, 520, 81, 16));
        enterPDV = new QLineEdit(Form);
        enterPDV->setObjectName(QString::fromUtf8("enterPDV"));
        enterPDV->setGeometry(QRect(910, 520, 171, 20));
        labelSuprovodniDoc = new QLabel(Form);
        labelSuprovodniDoc->setObjectName(QString::fromUtf8("labelSuprovodniDoc"));
        labelSuprovodniDoc->setGeometry(QRect(30, 550, 211, 16));
        enterSuprovodniDoc = new QLineEdit(Form);
        enterSuprovodniDoc->setObjectName(QString::fromUtf8("enterSuprovodniDoc"));
        enterSuprovodniDoc->setGeometry(QRect(240, 550, 841, 20));
        labelTransportServices = new QLabel(Form);
        labelTransportServices->setObjectName(QString::fromUtf8("labelTransportServices"));
        labelTransportServices->setGeometry(QRect(30, 580, 411, 16));
        enterTransportServices = new QLineEdit(Form);
        enterTransportServices->setObjectName(QString::fromUtf8("enterTransportServices"));
        enterTransportServices->setGeometry(QRect(440, 580, 641, 20));
        enterOthers = new QLineEdit(Form);
        enterOthers->setObjectName(QString::fromUtf8("enterOthers"));
        enterOthers->setGeometry(QRect(30, 610, 1051, 20));
        pushButtonAccept = new QPushButton(Form);
        pushButtonAccept->setObjectName(QString::fromUtf8("pushButtonAccept"));
        pushButtonAccept->setGeometry(QRect(180, 740, 171, 31));
        pushButtonClose = new QPushButton(Form);
        pushButtonClose->setObjectName(QString::fromUtf8("pushButtonClose"));
        pushButtonClose->setGeometry(QRect(470, 740, 171, 31));
        pushButtonPrint = new QPushButton(Form);
        pushButtonPrint->setObjectName(QString::fromUtf8("pushButtonPrint"));
        pushButtonPrint->setGeometry(QRect(750, 740, 171, 31));
        label_3 = new QLabel(Form);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(510, 640, 131, 16));
        tableWidget = new QTableWidget(Form);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setGeometry(QRect(30, 660, 1051, 61));

        retranslateUi(Form);

        QMetaObject::connectSlotsByName(Form);
    } // setupUi

    void retranslateUi(QWidget *Form)
    {
        Form->setWindowTitle(QApplication::translate("Form", "\320\244\320\276\321\200\320\274\320\260 \320\242\320\242\320\235", 0, QApplication::UnicodeUTF8));
        labelTitle->setText(QApplication::translate("Form", "\320\242\320\236\320\222\320\220\320\240\320\235\320\236-\320\242\320\240\320\220\320\235\320\241\320\237\320\236\320\240\320\242\320\235\320\220 \320\235\320\220\320\232\320\233\320\220\320\224\320\235\320\220", 0, QApplication::UnicodeUTF8));
        enterNumberNakladnaya->setText(QString());
        labelNum->setText(QApplication::translate("Form", "\342\204\226", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Form", "\"", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Form", "\"", 0, QApplication::UnicodeUTF8));
        labelNum_2->setText(QApplication::translate("Form", "20", 0, QApplication::UnicodeUTF8));
        labelNum_3->setText(QApplication::translate("Form", "\321\200.", 0, QApplication::UnicodeUTF8));
        labelAuto->setText(QApplication::translate("Form", "\320\220\320\262\321\202\320\276\320\274\320\276\320\261\321\226\320\273\321\214", 0, QApplication::UnicodeUTF8));
        labelAutoTypes->setText(QApplication::translate("Form", "(\320\274\320\260\321\200\320\272\320\260, \320\274\320\276\320\264\320\265\320\273\321\214, \321\202\320\270\320\277, \321\200\320\265\321\224\321\201\321\202\321\200\320\260\321\206\321\226\320\271\320\275\320\270\320\271 \320\275\320\276\320\274\320\265\321\200)", 0, QApplication::UnicodeUTF8));
        labelAuto_2->setText(QApplication::translate("Form", "\320\237\321\200\320\270\321\207\321\226\320\277/\320\275\320\260\320\277\321\226\320\262\320\277\321\200\320\270\321\207\321\226\320\277", 0, QApplication::UnicodeUTF8));
        labelAutoTypes_2->setText(QApplication::translate("Form", "(\320\274\320\260\321\200\320\272\320\260, \320\274\320\276\320\264\320\265\320\273\321\214, \321\202\320\270\320\277, \321\200\320\265\321\224\321\201\321\202\321\200\320\260\321\206\321\226\320\271\320\275\320\270\320\271 \320\275\320\276\320\274\320\265\321\200)", 0, QApplication::UnicodeUTF8));
        labelAuto_3->setText(QApplication::translate("Form", "\320\222\320\270\320\264 \320\277\320\265\321\200\320\265\320\262\320\265\320\267\320\265\320\275\321\214", 0, QApplication::UnicodeUTF8));
        labelAutoTransporter->setText(QApplication::translate("Form", "\320\220\320\262\321\202\320\276\320\274\320\276\320\261\321\226\320\273\321\214\320\275\320\270\320\271 \320\277\320\265\321\200\320\265\320\262i\320\267\320\275\320\270\320\272", 0, QApplication::UnicodeUTF8));
        labelAutoVoditel->setText(QApplication::translate("Form", "\320\222\320\276\320\264\321\226\320\271", 0, QApplication::UnicodeUTF8));
        labelAutoTransporterFIO->setText(QApplication::translate("Form", "(\320\275\320\260\320\271\320\274\320\265\320\275\321\203\320\262\320\260\320\275\320\275\321\217/\320\237.\320\206.\320\221.)", 0, QApplication::UnicodeUTF8));
        labelAutoTransporterLicenceNumber->setText(QApplication::translate("Form", "(\320\237.\320\206.\320\221., \320\275\320\276\320\274\320\265\321\200 \320\277\320\276\321\201\320\262\321\226\320\264\321\207\320\265\320\275\320\275\321\217 \320\262\320\276\320\264\321\226\321\217)", 0, QApplication::UnicodeUTF8));
        labelOrderer->setText(QApplication::translate("Form", "\320\227\320\260\320\274\320\276\320\262\320\275\320\270\320\272 ", 0, QApplication::UnicodeUTF8));
        labelAutoOrdererFIO->setText(QApplication::translate("Form", "(\320\275\320\260\320\271\320\274\320\265\320\275\321\203\320\262\320\260\320\275\320\275\321\217/\320\237.\320\206.\320\221.)", 0, QApplication::UnicodeUTF8));
        labelSender->setText(QApplication::translate("Form", "\320\222\320\260\320\275\321\202\320\260\320\266\320\276\320\262\321\226\320\264\320\277\321\200\320\260\320\262\320\275\320\270\320\272", 0, QApplication::UnicodeUTF8));
        labelAutoSenderFIO->setText(QApplication::translate("Form", "(\320\277\320\276\320\262\320\275\320\265 \320\275\320\260\320\271\320\274\320\265\320\275\321\203\320\262\320\260\320\275\320\275\321\217, \320\274\321\226\321\201\321\206\320\265\320\267\320\275\320\260\321\205\320\276\320\264\320\266\320\265\320\275\320\275\321\217/\320\237.\320\206.\320\221., \320\274\321\226\321\201\321\206\320\265 \320\277\321\200\320\276\320\266\320\270\320\262\320\260\320\275\320\275\321\217)", 0, QApplication::UnicodeUTF8));
        labelAccepter->setText(QApplication::translate("Form", "\320\222\320\260\320\275\321\202\320\260\320\266\320\276\320\276\320\264\320\265\321\200\320\266\321\203\320\262\320\260\321\207", 0, QApplication::UnicodeUTF8));
        labelAutoAccepterFIO->setText(QApplication::translate("Form", "(\320\277\320\276\320\262\320\275\320\265 \320\275\320\260\320\271\320\274\320\265\320\275\321\203\320\262\320\260\320\275\320\275\321\217, \320\274\321\226\321\201\321\206\320\265\320\267\320\275\320\260\321\205\320\276\320\264\320\266\320\265\320\275\320\275\321\217/\320\237.\320\206.\320\221., \320\274\321\226\321\201\321\206\320\265 \320\277\321\200\320\276\320\266\320\270\320\262\320\260\320\275\320\275\321\217)", 0, QApplication::UnicodeUTF8));
        labelPointOfLoad->setText(QApplication::translate("Form", "\320\237\321\203\320\275\320\272\321\202 \320\275\320\260\320\262\320\260\320\275\321\202\320\260\320\266\320\265\320\275\320\275\321\217", 0, QApplication::UnicodeUTF8));
        labelAddress1->setText(QApplication::translate("Form", "(\320\274\321\226\321\201\321\206\320\265\320\267\320\275\320\260\321\205\320\276\320\264\320\266\320\265\320\275\320\275\321\217)", 0, QApplication::UnicodeUTF8));
        labelPointUnLoad->setText(QApplication::translate("Form", "\320\237\321\203\320\275\320\272\321\202 \321\200\320\276\320\267\320\262\320\260\320\275\321\202\320\260\320\266\320\265\320\275\320\275\321\217", 0, QApplication::UnicodeUTF8));
        labelAddress2->setText(QApplication::translate("Form", "(\320\274\321\226\321\201\321\206\320\265\320\267\320\275\320\260\321\205\320\276\320\264\320\266\320\265\320\275\320\275\321\217)", 0, QApplication::UnicodeUTF8));
        labelPereaddress->setText(QApplication::translate("Form", "\320\237\320\265\321\200\320\265\320\260\320\264\321\200\320\265\321\201\321\203\320\262\320\260\320\275\320\275\321\217 \320\262\320\260\320\275\321\202\320\260\320\266\321\203", 0, QApplication::UnicodeUTF8));
        labelNamePereAddr->setText(QApplication::translate("Form", "(\320\275\320\260\320\271\320\274\320\265\320\275\321\203\320\262\320\260\320\275\320\275\321\217, \320\274\321\226\321\201\321\206\320\265\320\267\320\275\320\260\321\205\320\276\320\264\320\266\320\265\320\275\320\275\321\217/\320\237.\320\206.\320\221., \320\274\321\226\321\201\321\206\320\265 \320\277\321\200\320\276\320\266\320\270\320\262\320\260\320\275\320\275\321\217 \320\275\320\276\320\262\320\276\320\263\320\276 \320\262\320\260\320\275\321\202\320\260\320\266\320\276\320\276\320\264\320\265\321\200\320\266\321\203\320\262\320\260\321\207\320\260; \320\237.I.\320\221., \320\277\320\276\321\201\320\260\320\264\320\260 \321\202\320\260 \320\277i\320\264\320\277\320\270\321\201)", 0, QApplication::UnicodeUTF8));
        labelDoverenost->setText(QApplication::translate("Form", "\320\262\321\226\320\264\320\277\321\203\321\201\320\272 \320\267\320\260 \320\264\320\276\320\262\321\226\321\200\320\265\320\275\321\226\321\201\321\202\321\216 \320\262\320\260\320\275\321\202\320\260\320\266\320\276\320\276\320\264\320\265\321\200\320\266\321\203\320\262\320\260\321\207\320\260: \321\201\320\265\321\200\321\226\321\217", 0, QApplication::UnicodeUTF8));
        labelNumber->setText(QApplication::translate("Form", "\342\204\226", 0, QApplication::UnicodeUTF8));
        labelNumber_2->setText(QApplication::translate("Form", "\320\262\321\226\320\264", 0, QApplication::UnicodeUTF8));
        labelNum_4->setText(QApplication::translate("Form", "20", 0, QApplication::UnicodeUTF8));
        labelNum_5->setText(QApplication::translate("Form", "\321\200., \320\262\320\270\320\264\320\260\320\275\320\276\321\216", 0, QApplication::UnicodeUTF8));
        labelVantash->setText(QApplication::translate("Form", "\320\222\320\260\320\275\321\202\320\260\320\266 \320\275\320\260\320\264\320\260\320\275\320\270\320\271 \320\264\320\273\321\217 \320\277\320\265\321\200\320\265\320\262\320\265\320\267\320\265\320\275\320\275\321\217 \321\203 \321\201\321\202\320\260\320\275\321\226, \321\211\320\276", 0, QApplication::UnicodeUTF8));
        labelNum_6->setText(QApplication::translate("Form", "(\320\262\321\226\320\264\320\277\320\276\320\262\321\226\320\264\320\260\321\224/\320\275\320\265 \320\262\321\226\320\264\320\277\320\276\320\262\321\226\320\264\320\260\321\224)", 0, QApplication::UnicodeUTF8));
        labelRulsTransport->setText(QApplication::translate("Form", "\320\277\321\200\320\260\320\262\320\270\320\273\320\260\320\274 \320\277\320\265\321\200\320\265\320\262\320\265\320\267\320\265\320\275\321\214 \320\262\321\226\320\264\320\277\320\276\320\262\321\226\320\264\320\275\320\270\321\205 \320\262\320\260\320\275\321\202\320\260\320\266\321\226\320\262, \320\275\320\276\320\274\320\265\321\200 \320\277\320\273\320\276\320\274\320\261\320\270 (\320\267\320\260 \320\275\320\260\321\217\320\262\320\275\320\276\321\201\321\202\321\226)", 0, QApplication::UnicodeUTF8));
        labelNumberPlaces->setText(QApplication::translate("Form", "\320\272\321\226\320\273\321\214\320\272\321\226\321\201\321\202\321\214 \320\274\321\226\321\201\321\206\321\214", 0, QApplication::UnicodeUTF8));
        labelBuWords->setText(QApplication::translate("Form", "(\321\201\320\273\320\276\320\262\320\260\320\274\320\270)", 0, QApplication::UnicodeUTF8));
        labelBrutto->setText(QApplication::translate("Form", "\320\274\320\260\321\201\320\276\321\216 \320\261\321\200\321\203\321\202\321\202\320\276, \321\202", 0, QApplication::UnicodeUTF8));
        labelFIO_Excpeditor->setText(QApplication::translate("Form", "(\320\237.\320\206.\320\221., \320\277\320\276\321\201\320\260\320\264\320\260, \320\277\321\226\320\264\320\277\320\270\321\201)", 0, QApplication::UnicodeUTF8));
        labelBuWords_2->setText(QApplication::translate("Form", "(\321\201\320\273\320\276\320\262\320\260\320\274\320\270)", 0, QApplication::UnicodeUTF8));
        labelBrutto_2->setText(QApplication::translate("Form", ", \320\276\321\202\321\200\320\270\320\274\320\260\320\262 \320\262\320\276\320\264\321\226\320\271/\320\265\320\272\321\201\320\277\320\265\320\264\320\270\321\202\320\276\321\200", 0, QApplication::UnicodeUTF8));
        labelBuhgalterName->setText(QApplication::translate("Form", "\320\221\321\203\321\205\320\263\320\260\320\273\321\202\320\265\321\200 (\320\262\321\226\320\264\320\277\320\276\320\262\321\226\320\264\320\260\320\273\321\214\320\275\320\260 \320\276\321\201\320\276\320\261\320\260 \320\262\320\260\320\275\321\202\320\260\320\266\320\276\320\262\321\226\320\264\320\277\321\200\320\260\320\262\320\275\320\270\320\272\320\260)", 0, QApplication::UnicodeUTF8));
        labelFIO_Buhgalter->setText(QApplication::translate("Form", "(\320\237.\320\206.\320\221., \320\277\320\276\321\201\320\260\320\264\320\260, \320\277\321\226\320\264\320\277\320\270\321\201)", 0, QApplication::UnicodeUTF8));
        labelBuhgalterName_2->setText(QApplication::translate("Form", "\320\222\321\226\320\264\320\277\321\203\321\201\320\272 \320\264\320\276\320\267\320\262\320\276\320\273\320\270\320\262", 0, QApplication::UnicodeUTF8));
        labelFIO_Buhgalter_2->setText(QApplication::translate("Form", "(\320\237.\320\206.\320\221., \320\277\320\276\321\201\320\260\320\264\320\260, \320\277\321\226\320\264\320\277\320\270\321\201, \320\277\320\265\321\207\320\260\321\202\320\272\320\260)", 0, QApplication::UnicodeUTF8));
        labelTotalSold->setText(QApplication::translate("Form", "\320\243\321\201\321\214\320\276\320\263\320\276 \320\262\321\226\320\264\320\277\321\203\321\211\320\265\320\275\320\276 \320\275\320\260 \320\267\320\260\320\263\320\260\320\273\321\214\320\275\321\203 \321\201\321\203\320\274\321\203", 0, QApplication::UnicodeUTF8));
        labelPDV->setText(QApplication::translate("Form", ",  \321\203 \321\202.\321\207 \320\237\320\224\320\222", 0, QApplication::UnicodeUTF8));
        labelSuprovodniDoc->setText(QApplication::translate("Form", "\320\241\321\203\320\277\321\200\320\276\320\262\321\226\320\264\320\275\321\226 \320\264\320\276\320\272\321\203\320\274\320\265\320\275\321\202\320\270 \320\275\320\260 \320\262\320\260\320\275\321\202\320\260\320\266", 0, QApplication::UnicodeUTF8));
        labelTransportServices->setText(QApplication::translate("Form", "\320\242\321\200\320\260\320\275\321\201\320\277\320\276\321\200\321\202\320\275\321\226 \320\277\320\276\321\201\320\273\321\203\320\263\320\270, \321\217\320\272\321\226 \320\275\320\260\320\264\320\260\321\216\321\202\321\214\321\201\321\217 \320\260\320\262\321\202\320\276\320\274\320\276\320\261\321\226\320\273\321\214\320\275\320\270\320\274 \320\277\320\265\321\200\320\265\320\262\321\226\320\267\320\275\320\270\320\272\320\276\320\274", 0, QApplication::UnicodeUTF8));
        pushButtonAccept->setText(QApplication::translate("Form", "\320\237\320\276\320\264\321\202\320\262\320\265\321\200\320\264\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        pushButtonClose->setText(QApplication::translate("Form", "\320\227\320\260\320\272\321\200\321\213\321\202\321\214", 0, QApplication::UnicodeUTF8));
        pushButtonPrint->setText(QApplication::translate("Form", "\320\237\320\265\321\207\320\260\321\202\321\214", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("Form", "\320\222\320\206\320\224\320\236\320\234\320\236\320\241\320\242\320\206 \320\237\320\240\320\236 \320\222\320\220\320\235\320\242\320\220\320\226", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Form: public Ui_Form {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TTN_H
