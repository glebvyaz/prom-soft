/********************************************************************************
** Form generated from reading UI file 'spravochnik_tara.ui'
**
** Created: Wed 3. Feb 17:45:06 2021
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SPRAVOCHNIK_TARA_H
#define UI_SPRAVOCHNIK_TARA_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QTableView>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_spravochnik_tara
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QTableView *tableView;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *buttonAdd;
    QPushButton *buttonModify;
    QPushButton *buttonRemove;
    QPushButton *buttonClose;

    void setupUi(QDialog *spravochnik_tara)
    {
        if (spravochnik_tara->objectName().isEmpty())
            spravochnik_tara->setObjectName(QString::fromUtf8("spravochnik_tara"));
        spravochnik_tara->resize(820, 624);
        spravochnik_tara->setMinimumSize(QSize(820, 624));
        spravochnik_tara->setMaximumSize(QSize(820, 624));
        spravochnik_tara->setModal(true);
        verticalLayout = new QVBoxLayout(spravochnik_tara);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        tableView = new QTableView(spravochnik_tara);
        tableView->setObjectName(QString::fromUtf8("tableView"));

        horizontalLayout->addWidget(tableView);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        buttonAdd = new QPushButton(spravochnik_tara);
        buttonAdd->setObjectName(QString::fromUtf8("buttonAdd"));
        buttonAdd->setMinimumSize(QSize(0, 43));
        buttonAdd->setStyleSheet(QString::fromUtf8("font-size: 18px;;"));

        horizontalLayout_2->addWidget(buttonAdd);

        buttonModify = new QPushButton(spravochnik_tara);
        buttonModify->setObjectName(QString::fromUtf8("buttonModify"));
        buttonModify->setMinimumSize(QSize(0, 43));
        buttonModify->setStyleSheet(QString::fromUtf8("font-size: 18px;;"));

        horizontalLayout_2->addWidget(buttonModify);

        buttonRemove = new QPushButton(spravochnik_tara);
        buttonRemove->setObjectName(QString::fromUtf8("buttonRemove"));
        buttonRemove->setMinimumSize(QSize(0, 43));
        buttonRemove->setStyleSheet(QString::fromUtf8("font-size: 18px;;"));

        horizontalLayout_2->addWidget(buttonRemove);

        buttonClose = new QPushButton(spravochnik_tara);
        buttonClose->setObjectName(QString::fromUtf8("buttonClose"));
        buttonClose->setMinimumSize(QSize(0, 43));
        buttonClose->setStyleSheet(QString::fromUtf8("font-size: 18px;;"));

        horizontalLayout_2->addWidget(buttonClose);


        verticalLayout->addLayout(horizontalLayout_2);


        retranslateUi(spravochnik_tara);

        QMetaObject::connectSlotsByName(spravochnik_tara);
    } // setupUi

    void retranslateUi(QDialog *spravochnik_tara)
    {
        spravochnik_tara->setWindowTitle(QApplication::translate("spravochnik_tara", "Tara directory", 0, QApplication::UnicodeUTF8));
        buttonAdd->setText(QApplication::translate("spravochnik_tara", "Add", 0, QApplication::UnicodeUTF8));
        buttonModify->setText(QApplication::translate("spravochnik_tara", "Modify", 0, QApplication::UnicodeUTF8));
        buttonRemove->setText(QApplication::translate("spravochnik_tara", "Delete", 0, QApplication::UnicodeUTF8));
        buttonClose->setText(QApplication::translate("spravochnik_tara", "Close", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class spravochnik_tara: public Ui_spravochnik_tara {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SPRAVOCHNIK_TARA_H
