/********************************************************************************
** Form generated from reading UI file 'dialogvpsensors.ui'
**
** Created: Wed 3. Feb 17:45:06 2021
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGVPSENSORS_H
#define UI_DIALOGVPSENSORS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDateTimeEdit>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTableWidget>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_dialogVPSensors
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QGroupBox *groupBox_2;
    QLabel *label;
    QDateTimeEdit *dateTimeEdit_finish;
    QLabel *label_2;
    QDateTimeEdit *dateTimeEdit_start;
    QGroupBox *groupBox_3;
    QLineEdit *lineEdit_Deviation_kg;
    QLabel *label_3;
    QLineEdit *lineEdit_Deviation_code;
    QLabel *label_4;
    QPushButton *pushButton_Excel;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_Ok;
    QPushButton *pushButton_Cancel;
    QSpacerItem *verticalSpacer;
    QTableWidget *tableWidget;

    void setupUi(QDialog *dialogVPSensors)
    {
        if (dialogVPSensors->objectName().isEmpty())
            dialogVPSensors->setObjectName(QString::fromUtf8("dialogVPSensors"));
        dialogVPSensors->resize(780, 584);
        dialogVPSensors->setMinimumSize(QSize(780, 0));
        verticalLayout = new QVBoxLayout(dialogVPSensors);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox = new QGroupBox(dialogVPSensors);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setMinimumSize(QSize(0, 160));
        groupBox->setMaximumSize(QSize(16777215, 160));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        groupBox_2 = new QGroupBox(groupBox);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMinimumSize(QSize(350, 60));
        groupBox_2->setMaximumSize(QSize(300, 50));
        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 30, 16, 21));
        dateTimeEdit_finish = new QDateTimeEdit(groupBox_2);
        dateTimeEdit_finish->setObjectName(QString::fromUtf8("dateTimeEdit_finish"));
        dateTimeEdit_finish->setGeometry(QRect(200, 30, 140, 22));
        dateTimeEdit_finish->setMinimumSize(QSize(140, 0));
        dateTimeEdit_finish->setMaximumSize(QSize(100000, 16777215));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(180, 30, 16, 21));
        dateTimeEdit_start = new QDateTimeEdit(groupBox_2);
        dateTimeEdit_start->setObjectName(QString::fromUtf8("dateTimeEdit_start"));
        dateTimeEdit_start->setGeometry(QRect(30, 30, 141, 21));
        dateTimeEdit_start->setMinimumSize(QSize(140, 0));
        dateTimeEdit_start->setMaximumSize(QSize(100000, 16777215));

        gridLayout->addWidget(groupBox_2, 1, 0, 1, 1);

        groupBox_3 = new QGroupBox(groupBox);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setMinimumSize(QSize(0, 60));
        lineEdit_Deviation_kg = new QLineEdit(groupBox_3);
        lineEdit_Deviation_kg->setObjectName(QString::fromUtf8("lineEdit_Deviation_kg"));
        lineEdit_Deviation_kg->setGeometry(QRect(30, 30, 91, 21));
        label_3 = new QLabel(groupBox_3);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(130, 30, 31, 16));
        lineEdit_Deviation_code = new QLineEdit(groupBox_3);
        lineEdit_Deviation_code->setObjectName(QString::fromUtf8("lineEdit_Deviation_code"));
        lineEdit_Deviation_code->setGeometry(QRect(200, 30, 91, 21));
        label_4 = new QLabel(groupBox_3);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(300, 30, 31, 16));

        gridLayout->addWidget(groupBox_3, 2, 0, 1, 1);

        pushButton_Excel = new QPushButton(groupBox);
        pushButton_Excel->setObjectName(QString::fromUtf8("pushButton_Excel"));
        pushButton_Excel->setMinimumSize(QSize(150, 40));
        pushButton_Excel->setMaximumSize(QSize(150, 40));

        gridLayout->addWidget(pushButton_Excel, 1, 4, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 1, 3, 1, 1);

        pushButton_Ok = new QPushButton(groupBox);
        pushButton_Ok->setObjectName(QString::fromUtf8("pushButton_Ok"));
        pushButton_Ok->setMinimumSize(QSize(100, 40));
        pushButton_Ok->setMaximumSize(QSize(100, 40));

        gridLayout->addWidget(pushButton_Ok, 1, 1, 1, 1);

        pushButton_Cancel = new QPushButton(groupBox);
        pushButton_Cancel->setObjectName(QString::fromUtf8("pushButton_Cancel"));
        pushButton_Cancel->setMinimumSize(QSize(100, 40));
        pushButton_Cancel->setMaximumSize(QSize(100, 40));

        gridLayout->addWidget(pushButton_Cancel, 2, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 5, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);

        gridLayout->addItem(verticalSpacer, 0, 0, 1, 1);


        verticalLayout->addWidget(groupBox);

        tableWidget = new QTableWidget(dialogVPSensors);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setMinimumSize(QSize(400, 400));
        tableWidget->horizontalHeader()->setDefaultSectionSize(70);
        tableWidget->horizontalHeader()->setStretchLastSection(true);
        tableWidget->verticalHeader()->setVisible(false);

        verticalLayout->addWidget(tableWidget);


        retranslateUi(dialogVPSensors);

        QMetaObject::connectSlotsByName(dialogVPSensors);
    } // setupUi

    void retranslateUi(QDialog *dialogVPSensors)
    {
        dialogVPSensors->setWindowTitle(QApplication::translate("dialogVPSensors", "\320\220\320\275\320\260\320\273\320\270\321\202\320\270\320\272\320\260 \320\264\320\260\321\202\321\207\320\270\320\272\320\276\320\262", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("dialogVPSensors", "\320\244\320\270\320\273\321\214\321\202\321\200", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("dialogVPSensors", "\320\237\320\276 \320\262\321\200\320\265\320\274\320\265\320\275\320\270", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("dialogVPSensors", "\321\201", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("dialogVPSensors", "\320\277\320\276", 0, QApplication::UnicodeUTF8));
        groupBox_3->setTitle(QApplication::translate("dialogVPSensors", "\320\236\321\202\320\272\320\273\320\276\320\275\320\265\320\275\320\270\320\265", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("dialogVPSensors", "\320\272\320\263", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("dialogVPSensors", "\320\272\320\276\320\264", 0, QApplication::UnicodeUTF8));
        pushButton_Excel->setText(QApplication::translate("dialogVPSensors", "\320\255\320\272\321\201\320\277\320\276\321\200\321\202\320\270\321\200\320\276\320\262\320\260\321\202\321\214\n"
"\321\202\320\260\320\261\320\273\320\270\321\206\321\203 \320\262 Excel", 0, QApplication::UnicodeUTF8));
        pushButton_Ok->setText(QApplication::translate("dialogVPSensors", "\320\237\321\200\320\270\320\275\321\217\321\202\321\214", 0, QApplication::UnicodeUTF8));
        pushButton_Cancel->setText(QApplication::translate("dialogVPSensors", "\320\236\321\202\320\274\320\265\320\275\320\260", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class dialogVPSensors: public Ui_dialogVPSensors {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGVPSENSORS_H
