/********************************************************************************
** Form generated from reading UI file 'hiddenweighing.ui'
**
** Created: Wed 3. Feb 17:45:06 2021
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HIDDENWEIGHING_H
#define UI_HIDDENWEIGHING_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDateTimeEdit>
#include <QtGui/QDialog>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTableView>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_HiddenWeighing
{
public:
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_5;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QDateTimeEdit *dateTimeEdit_from;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QDateTimeEdit *dateTimeEdit_to;
    QVBoxLayout *verticalLayout_6;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButton_accept;
    QPushButton *pushButton_reset;
    QSpacerItem *horizontalSpacer;
    QVBoxLayout *verticalLayout_7;
    QVBoxLayout *verticalLayout_4;
    QPushButton *pushButton_photo;
    QTableView *tableView;

    void setupUi(QDialog *HiddenWeighing)
    {
        if (HiddenWeighing->objectName().isEmpty())
            HiddenWeighing->setObjectName(QString::fromUtf8("HiddenWeighing"));
        HiddenWeighing->resize(600, 300);
        HiddenWeighing->setMinimumSize(QSize(600, 300));
        verticalLayout_3 = new QVBoxLayout(HiddenWeighing);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        groupBox = new QGroupBox(HiddenWeighing);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout_5 = new QVBoxLayout(groupBox);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalSpacer = new QSpacerItem(10, 15, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_5->addItem(verticalSpacer);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMinimumSize(QSize(30, 0));

        horizontalLayout->addWidget(label);

        dateTimeEdit_from = new QDateTimeEdit(groupBox);
        dateTimeEdit_from->setObjectName(QString::fromUtf8("dateTimeEdit_from"));
        dateTimeEdit_from->setMinimumSize(QSize(140, 24));

        horizontalLayout->addWidget(dateTimeEdit_from);


        verticalLayout_2->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(30, 0));

        horizontalLayout_2->addWidget(label_2);

        dateTimeEdit_to = new QDateTimeEdit(groupBox);
        dateTimeEdit_to->setObjectName(QString::fromUtf8("dateTimeEdit_to"));
        dateTimeEdit_to->setMinimumSize(QSize(140, 24));

        horizontalLayout_2->addWidget(dateTimeEdit_to);


        verticalLayout_2->addLayout(horizontalLayout_2);


        horizontalLayout_3->addLayout(verticalLayout_2);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));

        horizontalLayout_3->addLayout(verticalLayout_6);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        pushButton_accept = new QPushButton(groupBox);
        pushButton_accept->setObjectName(QString::fromUtf8("pushButton_accept"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(pushButton_accept->sizePolicy().hasHeightForWidth());
        pushButton_accept->setSizePolicy(sizePolicy);
        pushButton_accept->setMinimumSize(QSize(160, 32));

        verticalLayout->addWidget(pushButton_accept);

        pushButton_reset = new QPushButton(groupBox);
        pushButton_reset->setObjectName(QString::fromUtf8("pushButton_reset"));
        sizePolicy.setHeightForWidth(pushButton_reset->sizePolicy().hasHeightForWidth());
        pushButton_reset->setSizePolicy(sizePolicy);
        pushButton_reset->setMinimumSize(QSize(160, 32));

        verticalLayout->addWidget(pushButton_reset);


        horizontalLayout_3->addLayout(verticalLayout);

        horizontalSpacer = new QSpacerItem(154, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));

        horizontalLayout_3->addLayout(verticalLayout_7);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        pushButton_photo = new QPushButton(groupBox);
        pushButton_photo->setObjectName(QString::fromUtf8("pushButton_photo"));
        pushButton_photo->setMinimumSize(QSize(160, 32));

        verticalLayout_4->addWidget(pushButton_photo);


        horizontalLayout_3->addLayout(verticalLayout_4);


        verticalLayout_5->addLayout(horizontalLayout_3);


        verticalLayout_3->addWidget(groupBox);

        tableView = new QTableView(HiddenWeighing);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->horizontalHeader()->setDefaultSectionSize(120);
        tableView->horizontalHeader()->setStretchLastSection(true);

        verticalLayout_3->addWidget(tableView);


        retranslateUi(HiddenWeighing);

        QMetaObject::connectSlotsByName(HiddenWeighing);
    } // setupUi

    void retranslateUi(QDialog *HiddenWeighing)
    {
        HiddenWeighing->setWindowTitle(QApplication::translate("HiddenWeighing", "Hidden weighing", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("HiddenWeighing", "Filter", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("HiddenWeighing", "From", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("HiddenWeighing", "To", 0, QApplication::UnicodeUTF8));
        pushButton_accept->setText(QApplication::translate("HiddenWeighing", "Accept", 0, QApplication::UnicodeUTF8));
        pushButton_reset->setText(QApplication::translate("HiddenWeighing", "Reset", 0, QApplication::UnicodeUTF8));
        pushButton_photo->setText(QApplication::translate("HiddenWeighing", "Show photos", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class HiddenWeighing: public Ui_HiddenWeighing {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HIDDENWEIGHING_H
