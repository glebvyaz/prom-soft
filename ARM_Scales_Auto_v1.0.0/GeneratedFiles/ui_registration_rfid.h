/********************************************************************************
** Form generated from reading UI file 'registration_rfid.ui'
**
** Created: Wed 3. Feb 17:45:06 2021
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REGISTRATION_RFID_H
#define UI_REGISTRATION_RFID_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTabWidget>
#include <QtGui/QTableView>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_registration_rfid
{
public:
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_6;
    QTabWidget *tabsSpravochniki;
    QWidget *tabTypeOfGoods;
    QVBoxLayout *verticalLayout_4;
    QLabel *label;
    QLineEdit *enterCode;
    QHBoxLayout *horizontalLayout_9;
    QTableView *tableViewRegistration;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *btnAdd;
    QPushButton *btnModify;
    QPushButton *btnRemove;
    QPushButton *btnClose;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QDialog *registration_rfid)
    {
        if (registration_rfid->objectName().isEmpty())
            registration_rfid->setObjectName(QString::fromUtf8("registration_rfid"));
        registration_rfid->resize(1228, 598);
        registration_rfid->setModal(true);
        verticalLayout_2 = new QVBoxLayout(registration_rfid);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        tabsSpravochniki = new QTabWidget(registration_rfid);
        tabsSpravochniki->setObjectName(QString::fromUtf8("tabsSpravochniki"));
        tabsSpravochniki->setStyleSheet(QString::fromUtf8("font-size: 14px;\n"
"font-weight: bold; \n"
"color: #000000;"));
        tabsSpravochniki->setProperty("tabBarAutoHide", QVariant(true));
        tabTypeOfGoods = new QWidget();
        tabTypeOfGoods->setObjectName(QString::fromUtf8("tabTypeOfGoods"));
        verticalLayout_4 = new QVBoxLayout(tabTypeOfGoods);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label = new QLabel(tabTypeOfGoods);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout_4->addWidget(label);

        enterCode = new QLineEdit(tabTypeOfGoods);
        enterCode->setObjectName(QString::fromUtf8("enterCode"));
        enterCode->setReadOnly(true);

        verticalLayout_4->addWidget(enterCode);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        tableViewRegistration = new QTableView(tabTypeOfGoods);
        tableViewRegistration->setObjectName(QString::fromUtf8("tableViewRegistration"));

        horizontalLayout_9->addWidget(tableViewRegistration);


        verticalLayout_4->addLayout(horizontalLayout_9);

        tabsSpravochniki->addTab(tabTypeOfGoods, QString());

        horizontalLayout_6->addWidget(tabsSpravochniki);


        verticalLayout_2->addLayout(horizontalLayout_6);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        btnAdd = new QPushButton(registration_rfid);
        btnAdd->setObjectName(QString::fromUtf8("btnAdd"));
        btnAdd->setMinimumSize(QSize(148, 43));
        btnAdd->setMaximumSize(QSize(165, 48));

        horizontalLayout_2->addWidget(btnAdd);

        btnModify = new QPushButton(registration_rfid);
        btnModify->setObjectName(QString::fromUtf8("btnModify"));
        btnModify->setMinimumSize(QSize(148, 43));
        btnModify->setMaximumSize(QSize(165, 48));

        horizontalLayout_2->addWidget(btnModify);

        btnRemove = new QPushButton(registration_rfid);
        btnRemove->setObjectName(QString::fromUtf8("btnRemove"));
        btnRemove->setMinimumSize(QSize(148, 43));
        btnRemove->setMaximumSize(QSize(165, 48));

        horizontalLayout_2->addWidget(btnRemove);

        btnClose = new QPushButton(registration_rfid);
        btnClose->setObjectName(QString::fromUtf8("btnClose"));
        btnClose->setMinimumSize(QSize(148, 43));
        btnClose->setMaximumSize(QSize(165, 48));

        horizontalLayout_2->addWidget(btnClose);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        verticalLayout_2->addLayout(horizontalLayout_2);


        retranslateUi(registration_rfid);

        tabsSpravochniki->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(registration_rfid);
    } // setupUi

    void retranslateUi(QDialog *registration_rfid)
    {
        registration_rfid->setWindowTitle(QApplication::translate("registration_rfid", "\320\234\320\276\320\264\321\203\320\273\321\214 RFID", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("registration_rfid", "\320\232\320\276\320\264 \320\272\320\260\321\200\321\202\321\213", 0, QApplication::UnicodeUTF8));
        tabsSpravochniki->setTabText(tabsSpravochniki->indexOf(tabTypeOfGoods), QApplication::translate("registration_rfid", "\320\240\320\265\320\263\320\270\321\201\321\202\321\200\320\260\321\206\320\270\320\276\320\275\320\275\321\213\320\265 \320\264\320\260\320\275\320\275\321\213\320\265", 0, QApplication::UnicodeUTF8));
        btnAdd->setText(QApplication::translate("registration_rfid", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        btnModify->setText(QApplication::translate("registration_rfid", "\320\230\320\267\320\274\320\265\320\275\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        btnRemove->setText(QApplication::translate("registration_rfid", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
        btnClose->setText(QApplication::translate("registration_rfid", "\320\227\320\260\320\272\321\200\321\213\321\202\321\214", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class registration_rfid: public Ui_registration_rfid {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REGISTRATION_RFID_H
