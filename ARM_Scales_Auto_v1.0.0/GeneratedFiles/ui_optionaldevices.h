/********************************************************************************
** Form generated from reading UI file 'optionaldevices.ui'
**
** Created: Wed 3. Feb 17:45:06 2021
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPTIONALDEVICES_H
#define UI_OPTIONALDEVICES_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QRadioButton>

QT_BEGIN_NAMESPACE

class Ui_optionalDevices
{
public:
    QGroupBox *groupBox_Socket1;
    QGroupBox *groupBox_s1_rele0;
    QRadioButton *radioButton_s1_rele0_ON;
    QRadioButton *radioButton_s1_rele0_OFF;
    QGroupBox *groupBox_s1_rele1;
    QRadioButton *radioButton_s1_rele1_OFF;
    QRadioButton *radioButton_s1_rele1_ON;
    QLabel *label_link_s1;
    QLabel *label;
    QGroupBox *groupBox_Socket2;
    QGroupBox *groupBox_s2_rele0;
    QRadioButton *radioButton_s2_rele0_ON;
    QRadioButton *radioButton_s2_rele0_OFF;
    QGroupBox *groupBox_s2_rele1;
    QRadioButton *radioButton_s2_rele1_OFF;
    QRadioButton *radioButton_s2_rele1_ON;
    QLabel *label_link_s2;
    QLabel *label_2;
    QGroupBox *groupBox_Socket3;
    QGroupBox *groupBox_s3_rele0;
    QRadioButton *radioButton_s3_rele0_ON;
    QRadioButton *radioButton_s3_rele0_OFF;
    QGroupBox *groupBox_s3_rele1;
    QRadioButton *radioButton_s3_rele1_OFF;
    QRadioButton *radioButton_s3_rele1_ON;
    QLabel *label_link_s3;
    QLabel *label_3;

    void setupUi(QDialog *optionalDevices)
    {
        if (optionalDevices->objectName().isEmpty())
            optionalDevices->setObjectName(QString::fromUtf8("optionalDevices"));
        optionalDevices->resize(390, 370);
        optionalDevices->setMinimumSize(QSize(390, 370));
        optionalDevices->setMaximumSize(QSize(390, 370));
        groupBox_Socket1 = new QGroupBox(optionalDevices);
        groupBox_Socket1->setObjectName(QString::fromUtf8("groupBox_Socket1"));
        groupBox_Socket1->setGeometry(QRect(10, 10, 371, 111));
        groupBox_s1_rele0 = new QGroupBox(groupBox_Socket1);
        groupBox_s1_rele0->setObjectName(QString::fromUtf8("groupBox_s1_rele0"));
        groupBox_s1_rele0->setGeometry(QRect(10, 40, 171, 61));
        radioButton_s1_rele0_ON = new QRadioButton(groupBox_s1_rele0);
        radioButton_s1_rele0_ON->setObjectName(QString::fromUtf8("radioButton_s1_rele0_ON"));
        radioButton_s1_rele0_ON->setGeometry(QRect(10, 20, 131, 21));
        radioButton_s1_rele0_OFF = new QRadioButton(groupBox_s1_rele0);
        radioButton_s1_rele0_OFF->setObjectName(QString::fromUtf8("radioButton_s1_rele0_OFF"));
        radioButton_s1_rele0_OFF->setGeometry(QRect(10, 40, 131, 21));
        groupBox_s1_rele1 = new QGroupBox(groupBox_Socket1);
        groupBox_s1_rele1->setObjectName(QString::fromUtf8("groupBox_s1_rele1"));
        groupBox_s1_rele1->setGeometry(QRect(190, 40, 171, 61));
        radioButton_s1_rele1_OFF = new QRadioButton(groupBox_s1_rele1);
        radioButton_s1_rele1_OFF->setObjectName(QString::fromUtf8("radioButton_s1_rele1_OFF"));
        radioButton_s1_rele1_OFF->setGeometry(QRect(10, 40, 131, 21));
        radioButton_s1_rele1_ON = new QRadioButton(groupBox_s1_rele1);
        radioButton_s1_rele1_ON->setObjectName(QString::fromUtf8("radioButton_s1_rele1_ON"));
        radioButton_s1_rele1_ON->setGeometry(QRect(10, 20, 131, 21));
        label_link_s1 = new QLabel(groupBox_Socket1);
        label_link_s1->setObjectName(QString::fromUtf8("label_link_s1"));
        label_link_s1->setGeometry(QRect(260, 10, 101, 21));
        label = new QLabel(groupBox_Socket1);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(200, 10, 61, 21));
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        groupBox_Socket2 = new QGroupBox(optionalDevices);
        groupBox_Socket2->setObjectName(QString::fromUtf8("groupBox_Socket2"));
        groupBox_Socket2->setGeometry(QRect(10, 130, 371, 111));
        groupBox_s2_rele0 = new QGroupBox(groupBox_Socket2);
        groupBox_s2_rele0->setObjectName(QString::fromUtf8("groupBox_s2_rele0"));
        groupBox_s2_rele0->setGeometry(QRect(10, 40, 171, 61));
        radioButton_s2_rele0_ON = new QRadioButton(groupBox_s2_rele0);
        radioButton_s2_rele0_ON->setObjectName(QString::fromUtf8("radioButton_s2_rele0_ON"));
        radioButton_s2_rele0_ON->setGeometry(QRect(10, 20, 131, 21));
        radioButton_s2_rele0_OFF = new QRadioButton(groupBox_s2_rele0);
        radioButton_s2_rele0_OFF->setObjectName(QString::fromUtf8("radioButton_s2_rele0_OFF"));
        radioButton_s2_rele0_OFF->setGeometry(QRect(10, 40, 131, 21));
        groupBox_s2_rele1 = new QGroupBox(groupBox_Socket2);
        groupBox_s2_rele1->setObjectName(QString::fromUtf8("groupBox_s2_rele1"));
        groupBox_s2_rele1->setGeometry(QRect(190, 40, 171, 61));
        radioButton_s2_rele1_OFF = new QRadioButton(groupBox_s2_rele1);
        radioButton_s2_rele1_OFF->setObjectName(QString::fromUtf8("radioButton_s2_rele1_OFF"));
        radioButton_s2_rele1_OFF->setGeometry(QRect(10, 40, 121, 21));
        radioButton_s2_rele1_ON = new QRadioButton(groupBox_s2_rele1);
        radioButton_s2_rele1_ON->setObjectName(QString::fromUtf8("radioButton_s2_rele1_ON"));
        radioButton_s2_rele1_ON->setGeometry(QRect(10, 20, 121, 21));
        label_link_s2 = new QLabel(groupBox_Socket2);
        label_link_s2->setObjectName(QString::fromUtf8("label_link_s2"));
        label_link_s2->setGeometry(QRect(260, 10, 101, 21));
        label_2 = new QLabel(groupBox_Socket2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(200, 10, 61, 21));
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        groupBox_Socket3 = new QGroupBox(optionalDevices);
        groupBox_Socket3->setObjectName(QString::fromUtf8("groupBox_Socket3"));
        groupBox_Socket3->setGeometry(QRect(10, 250, 371, 111));
        groupBox_s3_rele0 = new QGroupBox(groupBox_Socket3);
        groupBox_s3_rele0->setObjectName(QString::fromUtf8("groupBox_s3_rele0"));
        groupBox_s3_rele0->setGeometry(QRect(10, 40, 171, 61));
        radioButton_s3_rele0_ON = new QRadioButton(groupBox_s3_rele0);
        radioButton_s3_rele0_ON->setObjectName(QString::fromUtf8("radioButton_s3_rele0_ON"));
        radioButton_s3_rele0_ON->setGeometry(QRect(10, 20, 131, 21));
        radioButton_s3_rele0_OFF = new QRadioButton(groupBox_s3_rele0);
        radioButton_s3_rele0_OFF->setObjectName(QString::fromUtf8("radioButton_s3_rele0_OFF"));
        radioButton_s3_rele0_OFF->setGeometry(QRect(10, 40, 131, 21));
        groupBox_s3_rele1 = new QGroupBox(groupBox_Socket3);
        groupBox_s3_rele1->setObjectName(QString::fromUtf8("groupBox_s3_rele1"));
        groupBox_s3_rele1->setGeometry(QRect(190, 40, 171, 61));
        radioButton_s3_rele1_OFF = new QRadioButton(groupBox_s3_rele1);
        radioButton_s3_rele1_OFF->setObjectName(QString::fromUtf8("radioButton_s3_rele1_OFF"));
        radioButton_s3_rele1_OFF->setGeometry(QRect(10, 40, 121, 21));
        radioButton_s3_rele1_ON = new QRadioButton(groupBox_s3_rele1);
        radioButton_s3_rele1_ON->setObjectName(QString::fromUtf8("radioButton_s3_rele1_ON"));
        radioButton_s3_rele1_ON->setGeometry(QRect(10, 20, 121, 21));
        label_link_s3 = new QLabel(groupBox_Socket3);
        label_link_s3->setObjectName(QString::fromUtf8("label_link_s3"));
        label_link_s3->setGeometry(QRect(260, 10, 101, 21));
        label_3 = new QLabel(groupBox_Socket3);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(200, 10, 61, 21));
        label_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        retranslateUi(optionalDevices);

        QMetaObject::connectSlotsByName(optionalDevices);
    } // setupUi

    void retranslateUi(QDialog *optionalDevices)
    {
        optionalDevices->setWindowTitle(QApplication::translate("optionalDevices", "Socket-2", 0, QApplication::UnicodeUTF8));
        groupBox_Socket1->setTitle(QApplication::translate("optionalDevices", "Socket2 #1", 0, QApplication::UnicodeUTF8));
        groupBox_s1_rele0->setTitle(QApplication::translate("optionalDevices", "Relay 0", 0, QApplication::UnicodeUTF8));
        radioButton_s1_rele0_ON->setText(QApplication::translate("optionalDevices", "ON", 0, QApplication::UnicodeUTF8));
        radioButton_s1_rele0_OFF->setText(QApplication::translate("optionalDevices", "OFF", 0, QApplication::UnicodeUTF8));
        groupBox_s1_rele1->setTitle(QApplication::translate("optionalDevices", "Relay 1", 0, QApplication::UnicodeUTF8));
        radioButton_s1_rele1_OFF->setText(QApplication::translate("optionalDevices", "OFF", 0, QApplication::UnicodeUTF8));
        radioButton_s1_rele1_ON->setText(QApplication::translate("optionalDevices", "ON", 0, QApplication::UnicodeUTF8));
        label_link_s1->setText(QString());
        label->setText(QApplication::translate("optionalDevices", "Link:  ", 0, QApplication::UnicodeUTF8));
        groupBox_Socket2->setTitle(QApplication::translate("optionalDevices", "Socket2 #2", 0, QApplication::UnicodeUTF8));
        groupBox_s2_rele0->setTitle(QApplication::translate("optionalDevices", "Relay 0", 0, QApplication::UnicodeUTF8));
        radioButton_s2_rele0_ON->setText(QApplication::translate("optionalDevices", "ON", 0, QApplication::UnicodeUTF8));
        radioButton_s2_rele0_OFF->setText(QApplication::translate("optionalDevices", "OFF", 0, QApplication::UnicodeUTF8));
        groupBox_s2_rele1->setTitle(QApplication::translate("optionalDevices", "Relay 1", 0, QApplication::UnicodeUTF8));
        radioButton_s2_rele1_OFF->setText(QApplication::translate("optionalDevices", "OFF", 0, QApplication::UnicodeUTF8));
        radioButton_s2_rele1_ON->setText(QApplication::translate("optionalDevices", "ON", 0, QApplication::UnicodeUTF8));
        label_link_s2->setText(QString());
        label_2->setText(QApplication::translate("optionalDevices", "Link:  ", 0, QApplication::UnicodeUTF8));
        groupBox_Socket3->setTitle(QApplication::translate("optionalDevices", "Socket2 #3", 0, QApplication::UnicodeUTF8));
        groupBox_s3_rele0->setTitle(QApplication::translate("optionalDevices", "Relay 0", 0, QApplication::UnicodeUTF8));
        radioButton_s3_rele0_ON->setText(QApplication::translate("optionalDevices", "ON", 0, QApplication::UnicodeUTF8));
        radioButton_s3_rele0_OFF->setText(QApplication::translate("optionalDevices", "OFF", 0, QApplication::UnicodeUTF8));
        groupBox_s3_rele1->setTitle(QApplication::translate("optionalDevices", "Relay 1", 0, QApplication::UnicodeUTF8));
        radioButton_s3_rele1_OFF->setText(QApplication::translate("optionalDevices", "OFF", 0, QApplication::UnicodeUTF8));
        radioButton_s3_rele1_ON->setText(QApplication::translate("optionalDevices", "ON", 0, QApplication::UnicodeUTF8));
        label_link_s3->setText(QString());
        label_3->setText(QApplication::translate("optionalDevices", "Link:  ", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class optionalDevices: public Ui_optionalDevices {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPTIONALDEVICES_H
