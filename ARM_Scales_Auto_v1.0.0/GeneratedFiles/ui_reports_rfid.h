/********************************************************************************
** Form generated from reading UI file 'reports_rfid.ui'
**
** Created: Wed 3. Feb 17:45:06 2021
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REPORTS_RFID_H
#define UI_REPORTS_RFID_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCalendarWidget>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTableView>
#include <QtGui/QTimeEdit>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_reports_rfid
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBoxFilters;
    QGridLayout *gridLayout_3;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer_5;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_6;
    QHBoxLayout *horizontalLayout_9;
    QCalendarWidget *calendarFinDate;
    QSpacerItem *horizontalSpacer;
    QVBoxLayout *verticalLayout_10;
    QPushButton *btnPrintNakladnaya;
    QPushButton *btnPrintTable;
    QPushButton *btnExcelConv;
    QPushButton *btnMakeTTN;
    QSpacerItem *verticalSpacer;
    QSpacerItem *horizontalSpacer_2;
    QTimeEdit *timeEditFin;
    QLabel *label_5;
    QTimeEdit *timeEditStart;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *btnAcceptFilter;
    QPushButton *btnResetFilter;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label;
    QSpacerItem *horizontalSpacer_4;
    QHBoxLayout *horizontalLayout_8;
    QCalendarWidget *calendarStartDate;
    QGridLayout *gridLayout;
    QComboBox *selectNumCard;
    QLabel *label_7;
    QLabel *label_num_auto;
    QComboBox *selectNumScales;
    QComboBox *selectSender;
    QLabel *label_11;
    QComboBox *selectTypeOfGoods;
    QComboBox *selectFinRecords;
    QLabel *label_3;
    QComboBox *selectFIO;
    QLineEdit *enterNumAuto;
    QLabel *label_10;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_4;
    QTableView *tableViewReports;

    void setupUi(QDialog *reports_rfid)
    {
        if (reports_rfid->objectName().isEmpty())
            reports_rfid->setObjectName(QString::fromUtf8("reports_rfid"));
        reports_rfid->setWindowModality(Qt::ApplicationModal);
        reports_rfid->resize(1376, 777);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(reports_rfid->sizePolicy().hasHeightForWidth());
        reports_rfid->setSizePolicy(sizePolicy);
        reports_rfid->setMinimumSize(QSize(0, 0));
        reports_rfid->setSizeIncrement(QSize(0, 0));
        reports_rfid->setStyleSheet(QString::fromUtf8("text-align: center;\n"
"font-size: 12px;\n"
"font-weight: bold;"));
        reports_rfid->setSizeGripEnabled(true);
        reports_rfid->setModal(false);
        verticalLayout = new QVBoxLayout(reports_rfid);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBoxFilters = new QGroupBox(reports_rfid);
        groupBoxFilters->setObjectName(QString::fromUtf8("groupBoxFilters"));
        groupBoxFilters->setMinimumSize(QSize(0, 340));
        groupBoxFilters->setMaximumSize(QSize(16777215, 340));
        gridLayout_3 = new QGridLayout(groupBoxFilters);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_5);

        label_2 = new QLabel(groupBoxFilters);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMaximumSize(QSize(165, 16777215));

        horizontalLayout_7->addWidget(label_2);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_6);


        verticalLayout_7->addLayout(horizontalLayout_7);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        calendarFinDate = new QCalendarWidget(groupBoxFilters);
        calendarFinDate->setObjectName(QString::fromUtf8("calendarFinDate"));
        calendarFinDate->setMinimumSize(QSize(340, 100));
        calendarFinDate->setMaximumSize(QSize(480, 320));

        horizontalLayout_9->addWidget(calendarFinDate);


        verticalLayout_7->addLayout(horizontalLayout_9);


        gridLayout_3->addLayout(verticalLayout_7, 13, 3, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer, 13, 2, 1, 1);

        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        btnPrintNakladnaya = new QPushButton(groupBoxFilters);
        btnPrintNakladnaya->setObjectName(QString::fromUtf8("btnPrintNakladnaya"));
        btnPrintNakladnaya->setMinimumSize(QSize(148, 48));

        verticalLayout_10->addWidget(btnPrintNakladnaya);

        btnPrintTable = new QPushButton(groupBoxFilters);
        btnPrintTable->setObjectName(QString::fromUtf8("btnPrintTable"));
        btnPrintTable->setMinimumSize(QSize(165, 48));

        verticalLayout_10->addWidget(btnPrintTable);

        btnExcelConv = new QPushButton(groupBoxFilters);
        btnExcelConv->setObjectName(QString::fromUtf8("btnExcelConv"));
        btnExcelConv->setMinimumSize(QSize(148, 48));

        verticalLayout_10->addWidget(btnExcelConv);

        btnMakeTTN = new QPushButton(groupBoxFilters);
        btnMakeTTN->setObjectName(QString::fromUtf8("btnMakeTTN"));
        btnMakeTTN->setMinimumSize(QSize(148, 48));

        verticalLayout_10->addWidget(btnMakeTTN);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_10->addItem(verticalSpacer);


        gridLayout_3->addLayout(verticalLayout_10, 13, 0, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_2, 13, 4, 1, 1);

        timeEditFin = new QTimeEdit(groupBoxFilters);
        timeEditFin->setObjectName(QString::fromUtf8("timeEditFin"));
        timeEditFin->setMinimumSize(QSize(65, 24));
        timeEditFin->setMaximumSize(QSize(65, 16777215));

        gridLayout_3->addWidget(timeEditFin, 2, 3, 1, 1);

        label_5 = new QLabel(groupBoxFilters);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout_3->addWidget(label_5, 0, 3, 1, 1);

        timeEditStart = new QTimeEdit(groupBoxFilters);
        timeEditStart->setObjectName(QString::fromUtf8("timeEditStart"));
        timeEditStart->setMinimumSize(QSize(65, 24));
        timeEditStart->setMaximumSize(QSize(65, 16777215));
        timeEditStart->setSizeIncrement(QSize(0, 0));

        gridLayout_3->addWidget(timeEditStart, 2, 1, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        btnAcceptFilter = new QPushButton(groupBoxFilters);
        btnAcceptFilter->setObjectName(QString::fromUtf8("btnAcceptFilter"));
        btnAcceptFilter->setMinimumSize(QSize(140, 43));
        btnAcceptFilter->setMaximumSize(QSize(179, 43));
        btnAcceptFilter->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout_4->addWidget(btnAcceptFilter);

        btnResetFilter = new QPushButton(groupBoxFilters);
        btnResetFilter->setObjectName(QString::fromUtf8("btnResetFilter"));
        btnResetFilter->setMinimumSize(QSize(148, 43));
        btnResetFilter->setMaximumSize(QSize(180, 43));

        horizontalLayout_4->addWidget(btnResetFilter);


        gridLayout_3->addLayout(horizontalLayout_4, 0, 5, 1, 1);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_3);

        label = new QLabel(groupBoxFilters);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMaximumSize(QSize(180, 16777215));
        label->setLayoutDirection(Qt::LeftToRight);
        label->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout_6->addWidget(label);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_4);


        verticalLayout_4->addLayout(horizontalLayout_6);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        calendarStartDate = new QCalendarWidget(groupBoxFilters);
        calendarStartDate->setObjectName(QString::fromUtf8("calendarStartDate"));
        calendarStartDate->setMinimumSize(QSize(340, 100));
        calendarStartDate->setMaximumSize(QSize(480, 320));
        calendarStartDate->setContextMenuPolicy(Qt::DefaultContextMenu);
        calendarStartDate->setLayoutDirection(Qt::LeftToRight);

        horizontalLayout_8->addWidget(calendarStartDate);


        verticalLayout_4->addLayout(horizontalLayout_8);


        gridLayout_3->addLayout(verticalLayout_4, 13, 1, 1, 1);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        selectNumCard = new QComboBox(groupBoxFilters);
        selectNumCard->setObjectName(QString::fromUtf8("selectNumCard"));
        selectNumCard->setMinimumSize(QSize(0, 24));
        selectNumCard->setEditable(true);

        gridLayout->addWidget(selectNumCard, 5, 1, 1, 1);

        label_7 = new QLabel(groupBoxFilters);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout->addWidget(label_7, 1, 0, 1, 1);

        label_num_auto = new QLabel(groupBoxFilters);
        label_num_auto->setObjectName(QString::fromUtf8("label_num_auto"));
        label_num_auto->setMaximumSize(QSize(165, 16777215));

        gridLayout->addWidget(label_num_auto, 0, 0, 1, 1);

        selectNumScales = new QComboBox(groupBoxFilters);
        selectNumScales->setObjectName(QString::fromUtf8("selectNumScales"));
        selectNumScales->setMinimumSize(QSize(0, 24));
        selectNumScales->setMaximumSize(QSize(16777215, 24));
        selectNumScales->setSizeIncrement(QSize(0, 0));
        selectNumScales->setEditable(false);

        gridLayout->addWidget(selectNumScales, 0, 1, 1, 1);

        selectSender = new QComboBox(groupBoxFilters);
        selectSender->setObjectName(QString::fromUtf8("selectSender"));
        selectSender->setMinimumSize(QSize(180, 24));
        selectSender->setEditable(true);

        gridLayout->addWidget(selectSender, 1, 1, 1, 1);

        label_11 = new QLabel(groupBoxFilters);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout->addWidget(label_11, 2, 0, 1, 1);

        selectTypeOfGoods = new QComboBox(groupBoxFilters);
        selectTypeOfGoods->setObjectName(QString::fromUtf8("selectTypeOfGoods"));
        selectTypeOfGoods->setMinimumSize(QSize(0, 24));
        selectTypeOfGoods->setEditable(true);

        gridLayout->addWidget(selectTypeOfGoods, 2, 1, 1, 1);

        selectFinRecords = new QComboBox(groupBoxFilters);
        selectFinRecords->setObjectName(QString::fromUtf8("selectFinRecords"));
        selectFinRecords->setMinimumSize(QSize(0, 24));
        selectFinRecords->setEditable(true);

        gridLayout->addWidget(selectFinRecords, 6, 1, 1, 1);

        label_3 = new QLabel(groupBoxFilters);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMaximumSize(QSize(165, 16777215));

        gridLayout->addWidget(label_3, 3, 0, 1, 1);

        selectFIO = new QComboBox(groupBoxFilters);
        selectFIO->setObjectName(QString::fromUtf8("selectFIO"));
        selectFIO->setMinimumSize(QSize(0, 24));
        selectFIO->setEditable(true);

        gridLayout->addWidget(selectFIO, 4, 1, 1, 1);

        enterNumAuto = new QLineEdit(groupBoxFilters);
        enterNumAuto->setObjectName(QString::fromUtf8("enterNumAuto"));
        enterNumAuto->setMinimumSize(QSize(100, 24));
        enterNumAuto->setMaximumSize(QSize(16777215, 16777215));
        enterNumAuto->setEchoMode(QLineEdit::Normal);
        enterNumAuto->setProperty("clearButtonEnabled", QVariant(true));

        gridLayout->addWidget(enterNumAuto, 3, 1, 1, 1);

        label_10 = new QLabel(groupBoxFilters);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout->addWidget(label_10, 4, 0, 1, 1);

        label_8 = new QLabel(groupBoxFilters);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout->addWidget(label_8, 5, 0, 1, 1);

        label_9 = new QLabel(groupBoxFilters);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout->addWidget(label_9, 6, 0, 1, 1);


        gridLayout_3->addLayout(gridLayout, 13, 5, 1, 1);

        label_4 = new QLabel(groupBoxFilters);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(0, 38));
        label_4->setMaximumSize(QSize(180, 16777215));

        gridLayout_3->addWidget(label_4, 0, 1, 1, 1);


        verticalLayout->addWidget(groupBoxFilters);

        tableViewReports = new QTableView(reports_rfid);
        tableViewReports->setObjectName(QString::fromUtf8("tableViewReports"));

        verticalLayout->addWidget(tableViewReports);


        retranslateUi(reports_rfid);

        QMetaObject::connectSlotsByName(reports_rfid);
    } // setupUi

    void retranslateUi(QDialog *reports_rfid)
    {
        reports_rfid->setWindowTitle(QApplication::translate("reports_rfid", "\320\226\321\203\321\200\320\275\320\260\320\273 \320\262\320\267\320\262\320\265\321\210\320\270\320\262\320\260\320\275\320\270\320\271", 0, QApplication::UnicodeUTF8));
        groupBoxFilters->setTitle(QApplication::translate("reports_rfid", "\320\244\320\270\320\273\321\214\321\202\321\200\321\213 ", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("reports_rfid", "\320\224\320\260\321\202\320\260 \320\267\320\260\320\265\320\267\320\264\320\260 \320\260\320\262\321\202\320\276\320\274\320\276\320\261\320\270\320\273\321\217 \n"
"       (\320\272\320\276\320\275\320\265\321\207\320\275\320\260\321\217 \320\264\320\260\321\202\320\260)", 0, QApplication::UnicodeUTF8));
        btnPrintNakladnaya->setText(QApplication::translate("reports_rfid", "     \320\237\320\265\321\207\320\260\321\202\321\214\n"
"  \320\275\320\260\320\272\320\273\320\260\320\264\320\275\320\276\320\271", 0, QApplication::UnicodeUTF8));
        btnPrintTable->setText(QApplication::translate("reports_rfid", "\320\237\320\265\321\207\320\260\321\202\321\214 \320\276\321\202\321\207\321\221\321\202\320\260", 0, QApplication::UnicodeUTF8));
        btnExcelConv->setText(QApplication::translate("reports_rfid", "\320\255\320\272\321\201\320\277\320\276\321\200\321\202 \320\262 Excel", 0, QApplication::UnicodeUTF8));
        btnMakeTTN->setText(QApplication::translate("reports_rfid", "\320\241\320\276\320\267\320\264\320\260\321\202\321\214 \320\242\320\242\320\235", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("reports_rfid", "\320\222\321\200\320\265\320\274\321\217 \320\267\320\260\320\265\320\267\320\264\320\260 \320\260\320\262\321\202\320\276\320\274\320\276\320\261\320\270\320\273\321\217 \n"
"       (\320\272\320\276\320\275\320\265\321\207\320\275\320\276\320\265 \320\262\321\200\320\265\320\274\321\217)", 0, QApplication::UnicodeUTF8));
        btnAcceptFilter->setText(QApplication::translate("reports_rfid", "\320\237\321\200\320\270\320\274\320\265\320\275\320\270\321\202\321\214 \321\204\320\270\320\273\321\214\321\202\321\200", 0, QApplication::UnicodeUTF8));
        btnResetFilter->setText(QApplication::translate("reports_rfid", "\320\241\320\261\321\200\320\276\321\201\320\270\321\202\321\214 \321\204\320\270\320\273\321\214\321\202\321\200", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("reports_rfid", "\320\224\320\260\321\202\320\260 \320\267\320\260\320\265\320\267\320\264\320\260 \320\260\320\262\321\202\320\276\320\274\320\276\320\261\320\270\320\273\321\217 \n"
"       (\320\275\320\260\321\207\320\260\320\273\321\214\320\275\320\260\321\217 \320\264\320\260\321\202\320\260)", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("reports_rfid", "\320\237\320\276\321\201\321\202\320\260\320\262\321\211\320\270\320\272", 0, QApplication::UnicodeUTF8));
        label_num_auto->setText(QApplication::translate("reports_rfid", "\320\235\320\276\320\274\320\265\321\200 \320\262\320\265\321\201\320\276\320\262", 0, QApplication::UnicodeUTF8));
        label_11->setText(QApplication::translate("reports_rfid", "\320\234\320\260\321\202\320\265\321\200\320\270\320\260\320\273", 0, QApplication::UnicodeUTF8));
        selectFinRecords->clear();
        selectFinRecords->insertItems(0, QStringList()
         << QString()
         << QApplication::translate("reports_rfid", "\320\227\320\260\320\262\320\265\321\200\321\210\321\221\320\275\320\275\321\213\320\265", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("reports_rfid", "\320\235\320\265\320\267\320\260\320\262\320\265\321\200\321\210\321\221\320\275\320\275\321\213\320\265", 0, QApplication::UnicodeUTF8)
        );
        label_3->setText(QApplication::translate("reports_rfid", "\320\235\320\276\320\274\320\265\321\200 \320\260\320\262\321\202\320\276\320\274\320\276\320\261\320\270\320\273\321\217", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("reports_rfid", "\320\244.\320\230.\320\236. \320\262\320\276\320\264\320\270\321\202\320\265\320\273\321\217", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("reports_rfid", "\320\235\320\276\320\274\320\265\321\200 \320\272\320\260\321\200\321\202\321\213", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("reports_rfid", "\320\227\320\260\320\262\320\265\321\200\321\210\321\221\320\275\320\275\320\276\321\201\321\202\321\214 \320\262\320\267\320\262\320\265\321\210\320\270\320\262\320\260\320\275\320\270\321\217", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("reports_rfid", "\320\222\321\200\320\265\320\274\321\217 \320\267\320\260\320\265\320\267\320\264\320\260 \320\260\320\262\321\202\320\276\320\274\320\276\320\261\320\270\320\273\321\217 \n"
"      (\320\275\320\260\321\207\320\260\320\273\321\214\320\275\320\276\320\265 \320\262\321\200\320\265\320\274\321\217)", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class reports_rfid: public Ui_reports_rfid {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REPORTS_RFID_H
