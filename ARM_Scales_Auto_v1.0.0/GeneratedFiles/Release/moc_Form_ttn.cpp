/****************************************************************************
** Meta object code from reading C++ file 'Form_ttn.h'
**
** Created: Wed 3. Feb 16:13:09 2021
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "StdAfx.h"
#include "../../include/Form_ttn.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Form_ttn.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Form_ttn[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x0a,
      20,    9,    9,    9, 0x0a,
      43,    9,    9,    9, 0x0a,
      55,    9,    9,    9, 0x0a,
      68,    9,    9,    9, 0x0a,
      91,    9,    9,    9, 0x0a,
     114,    9,    9,    9, 0x0a,
     126,    9,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Form_ttn[] = {
    "Form_ttn\0\0hideTTN()\0pushButtonTTN_2_Slot()\0"
    "saveTTN_2()\0closeTTN_2()\0"
    "pushButtonPrint_Slot()\0pushButtonTTN_3_Slot()\0"
    "saveTTN_3()\0closeTTN_3()\0"
};

void Form_ttn::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Form_ttn *_t = static_cast<Form_ttn *>(_o);
        switch (_id) {
        case 0: _t->hideTTN(); break;
        case 1: _t->pushButtonTTN_2_Slot(); break;
        case 2: _t->saveTTN_2(); break;
        case 3: _t->closeTTN_2(); break;
        case 4: _t->pushButtonPrint_Slot(); break;
        case 5: _t->pushButtonTTN_3_Slot(); break;
        case 6: _t->saveTTN_3(); break;
        case 7: _t->closeTTN_3(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData Form_ttn::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Form_ttn::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Form_ttn,
      qt_meta_data_Form_ttn, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Form_ttn::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Form_ttn::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Form_ttn::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Form_ttn))
        return static_cast<void*>(const_cast< Form_ttn*>(this));
    return QObject::qt_metacast(_clname);
}

int Form_ttn::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
