/****************************************************************************
** Meta object code from reading C++ file 'optionaldevices.h'
**
** Created: Wed 3. Feb 16:13:12 2021
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "StdAfx.h"
#include "../../include/optionaldevices.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'optionaldevices.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_optionalDevices[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      20,   17,   16,   16, 0x05,

 // slots: signature, parameters, type, tag, flags
      53,   16,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_optionalDevices[] = {
    "optionalDevices\0\0,,\0"
    "radioClicked_Signal(int,int,int)\0"
    "radioClick_Slot()\0"
};

void optionalDevices::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        optionalDevices *_t = static_cast<optionalDevices *>(_o);
        switch (_id) {
        case 0: _t->radioClicked_Signal((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 1: _t->radioClick_Slot(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData optionalDevices::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject optionalDevices::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_optionalDevices,
      qt_meta_data_optionalDevices, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &optionalDevices::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *optionalDevices::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *optionalDevices::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_optionalDevices))
        return static_cast<void*>(const_cast< optionalDevices*>(this));
    return QDialog::qt_metacast(_clname);
}

int optionalDevices::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void optionalDevices::radioClicked_Signal(int _t1, int _t2, int _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
