/****************************************************************************
** Meta object code from reading C++ file 'registration_rfid.h'
**
** Created: Wed 3. Feb 16:13:16 2021
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "StdAfx.h"
#include "../../include/registration_rfid.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'registration_rfid.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_registration_rfid[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      21,   19,   18,   18, 0x05,

 // slots: signature, parameters, type, tag, flags
      71,   18,   18,   18, 0x0a,
      87,   82,   18,   18, 0x0a,
     109,   18,   18,   18, 0x0a,
     121,   18,   18,   18, 0x0a,
     138,   18,   18,   18, 0x0a,
     158,   18,   18,   18, 0x0a,
     178,   18,   18,   18, 0x0a,
     203,   18,  199,   18, 0x0a,
     219,   18,   18,   18, 0x0a,
     278,  267,   18,   18, 0x0a,
     300,   18,   18,   18, 0x0a,
     315,   18,   18,   18, 0x0a,
     332,   18,   18,   18, 0x0a,
     360,   18,   18,   18, 0x0a,
     388,   18,   18,   18, 0x0a,
     412,   18,   18,   18, 0x0a,
     436,   18,   18,   18, 0x0a,
     460,   82,   18,   18, 0x0a,
     497,  489,   18,   18, 0x0a,
     517,   18,   18,   18, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_registration_rfid[] = {
    "registration_rfid\0\0,\0"
    "setSpravochnikiContent_Signal(QList<QString>,int)\0"
    "showForm()\0code\0registerCard(QString)\0"
    "closeForm()\0addRecord_Slot()\0"
    "modifyRecord_Slot()\0removeRecord_Slot()\0"
    "tabChanged_Slot(int)\0int\0getCurrentTab()\0"
    "cellClicked_tableSpravochniki_Slot(QModelIndex)\0"
    "row,column\0getIndexes(int*,int*)\0"
    "clearIndexes()\0fillAcceptForm()\0"
    "tabSprav_Gruzootpraviteli()\0"
    "tabSprav_Gruzopoluchateli()\0"
    "tabSprav_Perevoschiki()\0tabSprav_Platelschiki()\0"
    "tabSprav_TypesOfGoods()\0"
    "acceptCodeRfid_Slot(QString)\0checked\0"
    "AutoTaraCheck(bool)\0autoNumEnter_Slot(QString)\0"
};

void registration_rfid::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        registration_rfid *_t = static_cast<registration_rfid *>(_o);
        switch (_id) {
        case 0: _t->setSpravochnikiContent_Signal((*reinterpret_cast< QList<QString>(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: _t->showForm(); break;
        case 2: _t->registerCard((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->closeForm(); break;
        case 4: _t->addRecord_Slot(); break;
        case 5: _t->modifyRecord_Slot(); break;
        case 6: _t->removeRecord_Slot(); break;
        case 7: _t->tabChanged_Slot((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: { int _r = _t->getCurrentTab();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 9: _t->cellClicked_tableSpravochniki_Slot((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 10: _t->getIndexes((*reinterpret_cast< int*(*)>(_a[1])),(*reinterpret_cast< int*(*)>(_a[2]))); break;
        case 11: _t->clearIndexes(); break;
        case 12: _t->fillAcceptForm(); break;
        case 13: _t->tabSprav_Gruzootpraviteli(); break;
        case 14: _t->tabSprav_Gruzopoluchateli(); break;
        case 15: _t->tabSprav_Perevoschiki(); break;
        case 16: _t->tabSprav_Platelschiki(); break;
        case 17: _t->tabSprav_TypesOfGoods(); break;
        case 18: _t->acceptCodeRfid_Slot((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 19: _t->AutoTaraCheck((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 20: _t->autoNumEnter_Slot((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData registration_rfid::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject registration_rfid::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_registration_rfid,
      qt_meta_data_registration_rfid, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &registration_rfid::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *registration_rfid::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *registration_rfid::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_registration_rfid))
        return static_cast<void*>(const_cast< registration_rfid*>(this));
    return QDialog::qt_metacast(_clname);
}

int registration_rfid::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    }
    return _id;
}

// SIGNAL 0
void registration_rfid::setSpravochnikiContent_Signal(QList<QString> _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
