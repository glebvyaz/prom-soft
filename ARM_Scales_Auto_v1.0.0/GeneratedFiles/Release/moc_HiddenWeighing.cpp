/****************************************************************************
** Meta object code from reading C++ file 'HiddenWeighing.h'
**
** Created: Wed 3. Feb 16:13:15 2021
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "StdAfx.h"
#include "../../include/HiddenWeighing.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'HiddenWeighing.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_HiddenWeighing[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x05,

 // slots: signature, parameters, type, tag, flags
      42,   35,   15,   15, 0x0a,
      61,   15,   15,   15, 0x0a,
      78,   15,   15,   15, 0x0a,
      94,   15,   15,   15, 0x08,
     114,   15,   15,   15, 0x08,
     133,   15,   15,   15, 0x08,
     146,   15,   15,   15, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_HiddenWeighing[] = {
    "HiddenWeighing\0\0makeHiddenPhotos()\0"
    "weight\0detectOn_Slot(int)\0detectOff_Slot()\0"
    "reported_Slot()\0acceptFilter_Slot()\0"
    "resetFilter_Slot()\0photo_Slot()\0shoot()\0"
};

void HiddenWeighing::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        HiddenWeighing *_t = static_cast<HiddenWeighing *>(_o);
        switch (_id) {
        case 0: _t->makeHiddenPhotos(); break;
        case 1: _t->detectOn_Slot((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->detectOff_Slot(); break;
        case 3: _t->reported_Slot(); break;
        case 4: _t->acceptFilter_Slot(); break;
        case 5: _t->resetFilter_Slot(); break;
        case 6: _t->photo_Slot(); break;
        case 7: _t->shoot(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData HiddenWeighing::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject HiddenWeighing::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_HiddenWeighing,
      qt_meta_data_HiddenWeighing, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &HiddenWeighing::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *HiddenWeighing::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *HiddenWeighing::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_HiddenWeighing))
        return static_cast<void*>(const_cast< HiddenWeighing*>(this));
    return QDialog::qt_metacast(_clname);
}

int HiddenWeighing::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void HiddenWeighing::makeHiddenPhotos()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
