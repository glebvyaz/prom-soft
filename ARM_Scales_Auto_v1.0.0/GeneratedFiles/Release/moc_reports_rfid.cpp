/****************************************************************************
** Meta object code from reading C++ file 'reports_rfid.h'
**
** Created: Wed 3. Feb 16:12:59 2021
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "StdAfx.h"
#include "../../include/reports_rfid.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'reports_rfid.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_reports_rfid[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x0a,
      25,   13,   13,   13, 0x0a,
      51,   13,   13,   13, 0x0a,
      74,   13,   13,   13, 0x0a,
      94,   13,   13,   13, 0x0a,
     125,  116,   13,   13, 0x0a,
     173,   13,   13,   13, 0x0a,
     205,  199,   13,   13, 0x0a,
     253,  199,   13,   13, 0x0a,
     295,   13,   13,   13, 0x0a,
     316,   13,   13,   13, 0x0a,
     334,   13,   13,   13, 0x0a,
     369,  356,   13,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_reports_rfid[] = {
    "reports_rfid\0\0showForm()\0"
    "updateReportsTable_Slot()\0"
    "btnAcceptFilter_Slot()\0radioBtnSend_Slot()\0"
    "btnResetFilter_Slot()\0data,tab\0"
    "setSpravochnikiContent_Slot(QList<QString>,int)\0"
    "btnPrintNakladnaya_Slot()\0index\0"
    "tableViewReportsDoubleClicked_Slot(QModelIndex)\0"
    "tableViewReportsClicked_Slot(QModelIndex)\0"
    "btnPrintTable_Slot()\0convertToExcell()\0"
    "makeTTN_Button_Slot()\0logicalIndex\0"
    "tableViewReportsHeader_Slot(int)\0"
};

void reports_rfid::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        reports_rfid *_t = static_cast<reports_rfid *>(_o);
        switch (_id) {
        case 0: _t->showForm(); break;
        case 1: _t->updateReportsTable_Slot(); break;
        case 2: _t->btnAcceptFilter_Slot(); break;
        case 3: _t->radioBtnSend_Slot(); break;
        case 4: _t->btnResetFilter_Slot(); break;
        case 5: _t->setSpravochnikiContent_Slot((*reinterpret_cast< QList<QString>(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 6: _t->btnPrintNakladnaya_Slot(); break;
        case 7: _t->tableViewReportsDoubleClicked_Slot((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 8: _t->tableViewReportsClicked_Slot((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 9: _t->btnPrintTable_Slot(); break;
        case 10: _t->convertToExcell(); break;
        case 11: _t->makeTTN_Button_Slot(); break;
        case 12: _t->tableViewReportsHeader_Slot((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData reports_rfid::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject reports_rfid::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_reports_rfid,
      qt_meta_data_reports_rfid, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &reports_rfid::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *reports_rfid::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *reports_rfid::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_reports_rfid))
        return static_cast<void*>(const_cast< reports_rfid*>(this));
    return QDialog::qt_metacast(_clname);
}

int reports_rfid::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
