/****************************************************************************
** Meta object code from reading C++ file 'loginWindow.h'
**
** Created: Wed 3. Feb 16:13:05 2021
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "StdAfx.h"
#include "../../include/loginWindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'loginWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_loginWindowClass[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      18,   17,   17,   17, 0x0a,
      41,   17,   17,   17, 0x0a,
      69,   17,   64,   17, 0x0a,
      90,   17,   17,   17, 0x0a,
     105,   17,   17,   17, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_loginWindowClass[] = {
    "loginWindowClass\0\0showCreateAccountWin()\0"
    "hideCreateAccountWin()\0bool\0"
    "confirmNewUserData()\0deleteRecord()\0"
    "hideLoginWindow()\0"
};

void loginWindowClass::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        loginWindowClass *_t = static_cast<loginWindowClass *>(_o);
        switch (_id) {
        case 0: _t->showCreateAccountWin(); break;
        case 1: _t->hideCreateAccountWin(); break;
        case 2: { bool _r = _t->confirmNewUserData();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 3: _t->deleteRecord(); break;
        case 4: _t->hideLoginWindow(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData loginWindowClass::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject loginWindowClass::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_loginWindowClass,
      qt_meta_data_loginWindowClass, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &loginWindowClass::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *loginWindowClass::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *loginWindowClass::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_loginWindowClass))
        return static_cast<void*>(const_cast< loginWindowClass*>(this));
    return QThread::qt_metacast(_clname);
}

int loginWindowClass::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
