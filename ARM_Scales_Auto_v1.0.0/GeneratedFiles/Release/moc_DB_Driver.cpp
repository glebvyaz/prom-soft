/****************************************************************************
** Meta object code from reading C++ file 'DB_Driver.h'
**
** Created: Wed 3. Feb 16:13:04 2021
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "StdAfx.h"
#include "../../include/DB_Driver.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DB_Driver.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_mysqlProccess[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x05,
      41,   14,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
      67,   14,   14,   14, 0x08,
      86,   14,   14,   14, 0x08,
     107,  105,   14,   14, 0x0a,
     152,   14,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_mysqlProccess[] = {
    "mysqlProccess\0\0showPhotoReports_Signal()\0"
    "savePhotoReports_Signal()\0showPhotoReports()\0"
    "savePhotoReports()\0,\0"
    "writeSensors_Slot(QVector<int>,QVector<int>)\0"
    "stable_Slot()\0"
};

void mysqlProccess::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        mysqlProccess *_t = static_cast<mysqlProccess *>(_o);
        switch (_id) {
        case 0: _t->showPhotoReports_Signal(); break;
        case 1: _t->savePhotoReports_Signal(); break;
        case 2: _t->showPhotoReports(); break;
        case 3: _t->savePhotoReports(); break;
        case 4: _t->writeSensors_Slot((*reinterpret_cast< QVector<int>(*)>(_a[1])),(*reinterpret_cast< QVector<int>(*)>(_a[2]))); break;
        case 5: _t->stable_Slot(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData mysqlProccess::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject mysqlProccess::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_mysqlProccess,
      qt_meta_data_mysqlProccess, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &mysqlProccess::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *mysqlProccess::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *mysqlProccess::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_mysqlProccess))
        return static_cast<void*>(const_cast< mysqlProccess*>(this));
    return QObject::qt_metacast(_clname);
}

int mysqlProccess::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void mysqlProccess::showPhotoReports_Signal()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void mysqlProccess::savePhotoReports_Signal()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
QT_END_MOC_NAMESPACE
