/********************************************************************************
** Form generated from reading UI file 'options.ui'
**
** Created: Wed 3. Feb 17:45:06 2021
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPTIONS_H
#define UI_OPTIONS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_options
{
public:
    QGroupBox *groupBox;
    QPushButton *pushButton_camera;
    QLineEdit *lineEdit_camera;
    QGroupBox *groupBox_2;
    QLabel *label_yourKey;
    QGroupBox *groupBox_3;
    QPushButton *pushButton_sornost;
    QLineEdit *lineEdit_sornost;
    QGroupBox *groupBox_4;
    QPushButton *pushButton_svetofor;
    QLineEdit *lineEdit_svetofor;
    QGroupBox *groupBox_5;
    QPushButton *pushButton_tablo;
    QLineEdit *lineEdit_tablo;
    QGroupBox *groupBox_6;
    QPushButton *pushButton_shlagbaum;
    QLineEdit *lineEdit_shlagbaum;
    QGroupBox *groupBox_7;
    QPushButton *pushButton_datchik;
    QLineEdit *lineEdit_datchik;
    QGroupBox *groupBox_PO;
    QPushButton *pushButton_PO;
    QLineEdit *lineEdit_PO;
    QLabel *label;
    QLabel *label_start;
    QGroupBox *groupBox_8;
    QPushButton *pushButton_rfid;
    QLineEdit *lineEdit_rfid;
    QGroupBox *groupBox_9;
    QLineEdit *lineEdit_bot;
    QPushButton *pushButton_bot;

    void setupUi(QDialog *options)
    {
        if (options->objectName().isEmpty())
            options->setObjectName(QString::fromUtf8("options"));
        options->resize(390, 670);
        options->setMinimumSize(QSize(390, 670));
        options->setMaximumSize(QSize(390, 670));
        groupBox = new QGroupBox(options);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 60, 371, 60));
        pushButton_camera = new QPushButton(groupBox);
        pushButton_camera->setObjectName(QString::fromUtf8("pushButton_camera"));
        pushButton_camera->setGeometry(QRect(240, 20, 121, 30));
        lineEdit_camera = new QLineEdit(groupBox);
        lineEdit_camera->setObjectName(QString::fromUtf8("lineEdit_camera"));
        lineEdit_camera->setGeometry(QRect(10, 20, 221, 30));
        QFont font;
        font.setPointSize(14);
        lineEdit_camera->setFont(font);
        groupBox_2 = new QGroupBox(options);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(130, 5, 141, 50));
        groupBox_2->setAlignment(Qt::AlignCenter);
        label_yourKey = new QLabel(groupBox_2);
        label_yourKey->setObjectName(QString::fromUtf8("label_yourKey"));
        label_yourKey->setGeometry(QRect(10, 15, 121, 30));
        label_yourKey->setFont(font);
        label_yourKey->setAlignment(Qt::AlignCenter);
        groupBox_3 = new QGroupBox(options);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(10, 125, 371, 60));
        pushButton_sornost = new QPushButton(groupBox_3);
        pushButton_sornost->setObjectName(QString::fromUtf8("pushButton_sornost"));
        pushButton_sornost->setGeometry(QRect(240, 20, 121, 30));
        lineEdit_sornost = new QLineEdit(groupBox_3);
        lineEdit_sornost->setObjectName(QString::fromUtf8("lineEdit_sornost"));
        lineEdit_sornost->setGeometry(QRect(10, 20, 221, 30));
        lineEdit_sornost->setFont(font);
        groupBox_4 = new QGroupBox(options);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setGeometry(QRect(10, 190, 371, 60));
        pushButton_svetofor = new QPushButton(groupBox_4);
        pushButton_svetofor->setObjectName(QString::fromUtf8("pushButton_svetofor"));
        pushButton_svetofor->setGeometry(QRect(240, 20, 121, 31));
        lineEdit_svetofor = new QLineEdit(groupBox_4);
        lineEdit_svetofor->setObjectName(QString::fromUtf8("lineEdit_svetofor"));
        lineEdit_svetofor->setGeometry(QRect(10, 20, 221, 31));
        lineEdit_svetofor->setFont(font);
        groupBox_5 = new QGroupBox(options);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        groupBox_5->setGeometry(QRect(10, 385, 371, 60));
        pushButton_tablo = new QPushButton(groupBox_5);
        pushButton_tablo->setObjectName(QString::fromUtf8("pushButton_tablo"));
        pushButton_tablo->setGeometry(QRect(240, 20, 121, 31));
        lineEdit_tablo = new QLineEdit(groupBox_5);
        lineEdit_tablo->setObjectName(QString::fromUtf8("lineEdit_tablo"));
        lineEdit_tablo->setGeometry(QRect(10, 20, 221, 31));
        lineEdit_tablo->setFont(font);
        groupBox_6 = new QGroupBox(options);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        groupBox_6->setGeometry(QRect(10, 255, 371, 60));
        pushButton_shlagbaum = new QPushButton(groupBox_6);
        pushButton_shlagbaum->setObjectName(QString::fromUtf8("pushButton_shlagbaum"));
        pushButton_shlagbaum->setGeometry(QRect(240, 20, 121, 31));
        lineEdit_shlagbaum = new QLineEdit(groupBox_6);
        lineEdit_shlagbaum->setObjectName(QString::fromUtf8("lineEdit_shlagbaum"));
        lineEdit_shlagbaum->setGeometry(QRect(10, 20, 221, 31));
        lineEdit_shlagbaum->setFont(font);
        groupBox_7 = new QGroupBox(options);
        groupBox_7->setObjectName(QString::fromUtf8("groupBox_7"));
        groupBox_7->setGeometry(QRect(10, 320, 371, 60));
        pushButton_datchik = new QPushButton(groupBox_7);
        pushButton_datchik->setObjectName(QString::fromUtf8("pushButton_datchik"));
        pushButton_datchik->setGeometry(QRect(240, 20, 121, 31));
        lineEdit_datchik = new QLineEdit(groupBox_7);
        lineEdit_datchik->setObjectName(QString::fromUtf8("lineEdit_datchik"));
        lineEdit_datchik->setGeometry(QRect(10, 20, 221, 31));
        lineEdit_datchik->setFont(font);
        groupBox_PO = new QGroupBox(options);
        groupBox_PO->setObjectName(QString::fromUtf8("groupBox_PO"));
        groupBox_PO->setGeometry(QRect(10, 610, 371, 60));
        pushButton_PO = new QPushButton(groupBox_PO);
        pushButton_PO->setObjectName(QString::fromUtf8("pushButton_PO"));
        pushButton_PO->setGeometry(QRect(240, 20, 121, 31));
        lineEdit_PO = new QLineEdit(groupBox_PO);
        lineEdit_PO->setObjectName(QString::fromUtf8("lineEdit_PO"));
        lineEdit_PO->setGeometry(QRect(10, 20, 221, 31));
        lineEdit_PO->setFont(font);
        label = new QLabel(options);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 580, 121, 20));
        label->setLayoutDirection(Qt::LeftToRight);
        label_start = new QLabel(options);
        label_start->setObjectName(QString::fromUtf8("label_start"));
        label_start->setGeometry(QRect(150, 580, 231, 20));
        QFont font1;
        font1.setPointSize(10);
        label_start->setFont(font1);
        groupBox_8 = new QGroupBox(options);
        groupBox_8->setObjectName(QString::fromUtf8("groupBox_8"));
        groupBox_8->setGeometry(QRect(10, 450, 371, 60));
        pushButton_rfid = new QPushButton(groupBox_8);
        pushButton_rfid->setObjectName(QString::fromUtf8("pushButton_rfid"));
        pushButton_rfid->setGeometry(QRect(240, 20, 121, 31));
        lineEdit_rfid = new QLineEdit(groupBox_8);
        lineEdit_rfid->setObjectName(QString::fromUtf8("lineEdit_rfid"));
        lineEdit_rfid->setGeometry(QRect(10, 20, 221, 31));
        lineEdit_rfid->setFont(font);
        groupBox_9 = new QGroupBox(options);
        groupBox_9->setObjectName(QString::fromUtf8("groupBox_9"));
        groupBox_9->setGeometry(QRect(10, 515, 371, 60));
        lineEdit_bot = new QLineEdit(groupBox_9);
        lineEdit_bot->setObjectName(QString::fromUtf8("lineEdit_bot"));
        lineEdit_bot->setGeometry(QRect(10, 20, 221, 31));
        lineEdit_bot->setFont(font);
        pushButton_bot = new QPushButton(groupBox_9);
        pushButton_bot->setObjectName(QString::fromUtf8("pushButton_bot"));
        pushButton_bot->setGeometry(QRect(240, 20, 121, 31));

        retranslateUi(options);

        QMetaObject::connectSlotsByName(options);
    } // setupUi

    void retranslateUi(QDialog *options)
    {
        options->setWindowTitle(QApplication::translate("options", "Software modules activation", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("options", "Photographic fixation module", 0, QApplication::UnicodeUTF8));
        pushButton_camera->setText(QApplication::translate("options", "Activate", 0, QApplication::UnicodeUTF8));
        lineEdit_camera->setPlaceholderText(QApplication::translate("options", "enter key", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("options", "Your number", 0, QApplication::UnicodeUTF8));
        label_yourKey->setText(QApplication::translate("options", "____________", 0, QApplication::UnicodeUTF8));
        groupBox_3->setTitle(QApplication::translate("options", "Waste definition module", 0, QApplication::UnicodeUTF8));
        pushButton_sornost->setText(QApplication::translate("options", "Activate", 0, QApplication::UnicodeUTF8));
        lineEdit_sornost->setPlaceholderText(QApplication::translate("options", "enter key", 0, QApplication::UnicodeUTF8));
        groupBox_4->setTitle(QApplication::translate("options", "Control module - traffic lights", 0, QApplication::UnicodeUTF8));
        pushButton_svetofor->setText(QApplication::translate("options", "Activate", 0, QApplication::UnicodeUTF8));
        lineEdit_svetofor->setPlaceholderText(QApplication::translate("options", "enter key", 0, QApplication::UnicodeUTF8));
        groupBox_5->setTitle(QApplication::translate("options", "Duplicative scoreboards module", 0, QApplication::UnicodeUTF8));
        pushButton_tablo->setText(QApplication::translate("options", "Activate", 0, QApplication::UnicodeUTF8));
        lineEdit_tablo->setPlaceholderText(QApplication::translate("options", "enter key", 0, QApplication::UnicodeUTF8));
        groupBox_6->setTitle(QApplication::translate("options", "Control module - barriers", 0, QApplication::UnicodeUTF8));
        pushButton_shlagbaum->setText(QApplication::translate("options", "Activate", 0, QApplication::UnicodeUTF8));
        lineEdit_shlagbaum->setPlaceholderText(QApplication::translate("options", "enter key", 0, QApplication::UnicodeUTF8));
        groupBox_7->setTitle(QApplication::translate("options", "Control module - positioning sensors", 0, QApplication::UnicodeUTF8));
        pushButton_datchik->setText(QApplication::translate("options", "Activate", 0, QApplication::UnicodeUTF8));
        lineEdit_datchik->setPlaceholderText(QApplication::translate("options", "enter key", 0, QApplication::UnicodeUTF8));
        groupBox_PO->setTitle(QApplication::translate("options", "Program activation", 0, QApplication::UnicodeUTF8));
        pushButton_PO->setText(QApplication::translate("options", "Activate", 0, QApplication::UnicodeUTF8));
        lineEdit_PO->setPlaceholderText(QApplication::translate("options", "enter key", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("options", "Program fist run :", 0, QApplication::UnicodeUTF8));
        label_start->setText(QApplication::translate("options", "TextLabel", 0, QApplication::UnicodeUTF8));
        groupBox_8->setTitle(QApplication::translate("options", "Rfid module", 0, QApplication::UnicodeUTF8));
        pushButton_rfid->setText(QApplication::translate("options", "Activate", 0, QApplication::UnicodeUTF8));
        lineEdit_rfid->setPlaceholderText(QApplication::translate("options", "enter key", 0, QApplication::UnicodeUTF8));
        groupBox_9->setTitle(QApplication::translate("options", "Telegramm-Bot", 0, QApplication::UnicodeUTF8));
        lineEdit_bot->setPlaceholderText(QApplication::translate("options", "enter key", 0, QApplication::UnicodeUTF8));
        pushButton_bot->setText(QApplication::translate("options", "Activate", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class options: public Ui_options {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPTIONS_H
