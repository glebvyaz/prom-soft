/****************************************************************************
** Meta object code from reading C++ file 'settings.h'
**
** Created: Wed 3. Feb 17:45:02 2021
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "StdAfx.h"
#include "../../include/settings.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'settings.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_SettingsForm[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
      33,   13,   13,   13, 0x0a,
      64,   13,   13,   13, 0x0a,
      97,   13,   13,   13, 0x0a,
     113,   13,   13,   13, 0x0a,
     136,   13,   13,   13, 0x0a,
     161,   13,   13,   13, 0x0a,
     186,   13,   13,   13, 0x0a,
     211,   13,   13,   13, 0x0a,
     236,   13,   13,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_SettingsForm[] = {
    "SettingsForm\0\0StartTimerWeight()\0"
    "buttonSaveEmailSettings_Slot()\0"
    "buttonSaveDevicesSettings_Slot()\0"
    "saveSens_Slot()\0buttonSaveMySQL_Slot()\0"
    "checkBoxCoord1_Slot(int)\0"
    "checkBoxCoord2_Slot(int)\0"
    "checkBoxCoord3_Slot(int)\0"
    "checkBoxCoord4_Slot(int)\0"
    "setEndOfFrame_Slot(int)\0"
};

void SettingsForm::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SettingsForm *_t = static_cast<SettingsForm *>(_o);
        switch (_id) {
        case 0: _t->StartTimerWeight(); break;
        case 1: _t->buttonSaveEmailSettings_Slot(); break;
        case 2: _t->buttonSaveDevicesSettings_Slot(); break;
        case 3: _t->saveSens_Slot(); break;
        case 4: _t->buttonSaveMySQL_Slot(); break;
        case 5: _t->checkBoxCoord1_Slot((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->checkBoxCoord2_Slot((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->checkBoxCoord3_Slot((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->checkBoxCoord4_Slot((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->setEndOfFrame_Slot((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData SettingsForm::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SettingsForm::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_SettingsForm,
      qt_meta_data_SettingsForm, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SettingsForm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SettingsForm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SettingsForm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SettingsForm))
        return static_cast<void*>(const_cast< SettingsForm*>(this));
    return QDialog::qt_metacast(_clname);
}

int SettingsForm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void SettingsForm::StartTimerWeight()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
