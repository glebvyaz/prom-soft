/****************************************************************************
** Meta object code from reading C++ file 'serialPortClass.h'
**
** Created: Wed 3. Feb 17:44:54 2021
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "StdAfx.h"
#include "../../include/serialPortClass.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'serialPortClass.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_serialPortClass[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x0a,
      32,   16,   16,   16, 0x0a,
      59,   16,   16,   16, 0x0a,
      86,   16,   16,   16, 0x0a,
     113,   16,   16,   16, 0x0a,
     140,   16,   16,   16, 0x0a,
     167,   16,   16,   16, 0x0a,
     194,   16,   16,   16, 0x0a,
     221,   16,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_serialPortClass[] = {
    "serialPortClass\0\0dataReceived()\0"
    "serialPort0_dataReceived()\0"
    "serialPort1_dataReceived()\0"
    "serialPort2_dataReceived()\0"
    "serialPort3_dataReceived()\0"
    "serialPort4_dataReceived()\0"
    "serialPort5_dataReceived()\0"
    "serialPort6_dataReceived()\0"
    "serialPort7_dataReceived()\0"
};

void serialPortClass::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        serialPortClass *_t = static_cast<serialPortClass *>(_o);
        switch (_id) {
        case 0: _t->dataReceived(); break;
        case 1: _t->serialPort0_dataReceived(); break;
        case 2: _t->serialPort1_dataReceived(); break;
        case 3: _t->serialPort2_dataReceived(); break;
        case 4: _t->serialPort3_dataReceived(); break;
        case 5: _t->serialPort4_dataReceived(); break;
        case 6: _t->serialPort5_dataReceived(); break;
        case 7: _t->serialPort6_dataReceived(); break;
        case 8: _t->serialPort7_dataReceived(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData serialPortClass::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject serialPortClass::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_serialPortClass,
      qt_meta_data_serialPortClass, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &serialPortClass::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *serialPortClass::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *serialPortClass::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_serialPortClass))
        return static_cast<void*>(const_cast< serialPortClass*>(this));
    return QObject::qt_metacast(_clname);
}

int serialPortClass::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
