/****************************************************************************
** Meta object code from reading C++ file 'dialogvpsensors.h'
**
** Created: Wed 3. Feb 17:45:00 2021
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "StdAfx.h"
#include "../../include/dialogvpsensors.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dialogvpsensors.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_dialogVPSensors[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      29,   17,   16,   16, 0x05,

 // slots: signature, parameters, type, tag, flags
     140,   16,   16,   16, 0x0a,
     159,   16,   16,   16, 0x0a,
     178,   16,   16,   16, 0x0a,
     191,   16,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_dialogVPSensors[] = {
    "dialogVPSensors\0\0,,,,,,,,,,,\0"
    "loadMailData_Signal(bool,QString,int,QString,QString,QString,QString,Q"
    "String,QString,bool,QString,QStringList)\0"
    "fiterAccept_Slot()\0fiterReject_Slot()\0"
    "excel_Slot()\0email_Slot()\0"
};

void dialogVPSensors::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        dialogVPSensors *_t = static_cast<dialogVPSensors *>(_o);
        switch (_id) {
        case 0: _t->loadMailData_Signal((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4])),(*reinterpret_cast< QString(*)>(_a[5])),(*reinterpret_cast< QString(*)>(_a[6])),(*reinterpret_cast< QString(*)>(_a[7])),(*reinterpret_cast< QString(*)>(_a[8])),(*reinterpret_cast< QString(*)>(_a[9])),(*reinterpret_cast< bool(*)>(_a[10])),(*reinterpret_cast< QString(*)>(_a[11])),(*reinterpret_cast< QStringList(*)>(_a[12]))); break;
        case 1: _t->fiterAccept_Slot(); break;
        case 2: _t->fiterReject_Slot(); break;
        case 3: _t->excel_Slot(); break;
        case 4: _t->email_Slot(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData dialogVPSensors::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject dialogVPSensors::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_dialogVPSensors,
      qt_meta_data_dialogVPSensors, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &dialogVPSensors::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *dialogVPSensors::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *dialogVPSensors::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_dialogVPSensors))
        return static_cast<void*>(const_cast< dialogVPSensors*>(this));
    return QDialog::qt_metacast(_clname);
}

int dialogVPSensors::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void dialogVPSensors::loadMailData_Signal(bool _t1, QString _t2, int _t3, QString _t4, QString _t5, QString _t6, QString _t7, QString _t8, QString _t9, bool _t10, QString _t11, QStringList _t12)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)), const_cast<void*>(reinterpret_cast<const void*>(&_t6)), const_cast<void*>(reinterpret_cast<const void*>(&_t7)), const_cast<void*>(reinterpret_cast<const void*>(&_t8)), const_cast<void*>(reinterpret_cast<const void*>(&_t9)), const_cast<void*>(reinterpret_cast<const void*>(&_t10)), const_cast<void*>(reinterpret_cast<const void*>(&_t11)), const_cast<void*>(reinterpret_cast<const void*>(&_t12)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
