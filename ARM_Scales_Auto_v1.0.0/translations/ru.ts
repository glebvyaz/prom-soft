<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>Form</name>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="17"/>
        <location filename="../src/Form_ttn.cpp" line="517"/>
        <source>Форма ТТН</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="29"/>
        <location filename="../src/Form_ttn.cpp" line="518"/>
        <source>ТОВАРНО-ТРАНСПОРТНА НАКЛАДНА</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="55"/>
        <location filename="../forms/ttn.ui" line="556"/>
        <location filename="../src/Form_ttn.cpp" line="520"/>
        <location filename="../src/Form_ttn.cpp" line="547"/>
        <source>№</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/ttn.ui" line="68"/>
        <location filename="../forms/ttn.ui" line="81"/>
        <location filename="../src/Form_ttn.cpp" line="521"/>
        <location filename="../src/Form_ttn.cpp" line="522"/>
        <source>&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/ttn.ui" line="114"/>
        <location filename="../forms/ttn.ui" line="622"/>
        <location filename="../src/Form_ttn.cpp" line="523"/>
        <location filename="../src/Form_ttn.cpp" line="549"/>
        <source>20</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="137"/>
        <location filename="../src/Form_ttn.cpp" line="524"/>
        <source>р.</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="150"/>
        <location filename="../src/Form_ttn.cpp" line="525"/>
        <source>Автомобіль</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="173"/>
        <location filename="../forms/ttn.ui" line="199"/>
        <location filename="../src/Form_ttn.cpp" line="526"/>
        <location filename="../src/Form_ttn.cpp" line="528"/>
        <source>(марка, модель, тип, реєстраційний номер)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="186"/>
        <location filename="../src/Form_ttn.cpp" line="527"/>
        <source>Причіп/напівпричіп</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="222"/>
        <location filename="../src/Form_ttn.cpp" line="529"/>
        <source>Вид перевезень</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="245"/>
        <source>Автомобільний перевiзник</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="268"/>
        <location filename="../src/Form_ttn.cpp" line="531"/>
        <source>Водій</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="291"/>
        <location filename="../forms/ttn.ui" line="340"/>
        <source>(найменування/П.І.Б.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="304"/>
        <source>(П.І.Б., номер посвідчення водія)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="317"/>
        <location filename="../src/Form_ttn.cpp" line="534"/>
        <source>Замовник </source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="353"/>
        <location filename="../src/Form_ttn.cpp" line="536"/>
        <source>Вантажовідправник</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="376"/>
        <location filename="../forms/ttn.ui" line="402"/>
        <location filename="../src/Form_ttn.cpp" line="537"/>
        <location filename="../src/Form_ttn.cpp" line="539"/>
        <location filename="../src/Form_ttn.cpp" line="545"/>
        <source>(повне найменування, місцезнаходження/П.І.Б., місце проживання)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="389"/>
        <location filename="../src/Form_ttn.cpp" line="538"/>
        <source>Вантажоодержувач</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="425"/>
        <location filename="../src/Form_ttn.cpp" line="540"/>
        <source>Пункт навантаження</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="448"/>
        <location filename="../forms/ttn.ui" line="474"/>
        <location filename="../src/Form_ttn.cpp" line="541"/>
        <location filename="../src/Form_ttn.cpp" line="543"/>
        <source>(місцезнаходження)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="461"/>
        <location filename="../src/Form_ttn.cpp" line="542"/>
        <source>Пункт розвантаження</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="497"/>
        <location filename="../src/Form_ttn.cpp" line="544"/>
        <source>Переадресування вантажу</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="520"/>
        <source>(найменування, місцезнаходження/П.І.Б., місце проживання нового вантажоодержувача; П.I.Б., посада та пiдпис)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="533"/>
        <source>відпуск за довіреністю вантажоодержувача: серія</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="579"/>
        <location filename="../src/Form_ttn.cpp" line="548"/>
        <source>від</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="635"/>
        <location filename="../src/Form_ttn.cpp" line="550"/>
        <source>р., виданою</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="658"/>
        <location filename="../src/Form_ttn.cpp" line="551"/>
        <source>Вантаж наданий для перевезення у стані, що</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="681"/>
        <source>(відповідає/не відповідає)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="694"/>
        <location filename="../src/Form_ttn.cpp" line="553"/>
        <source>правилам перевезень відповідних вантажів, номер пломби (за наявності)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="717"/>
        <source>кількість місць</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="740"/>
        <location filename="../forms/ttn.ui" line="799"/>
        <location filename="../src/Form_ttn.cpp" line="555"/>
        <location filename="../src/Form_ttn.cpp" line="558"/>
        <source>(словами)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="753"/>
        <location filename="../src/Form_ttn.cpp" line="556"/>
        <source>масою брутто, т</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="786"/>
        <location filename="../forms/ttn.ui" line="848"/>
        <location filename="../src/Form_ttn.cpp" line="557"/>
        <location filename="../src/Form_ttn.cpp" line="561"/>
        <source>(П.І.Б., посада, підпис)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="812"/>
        <location filename="../src/Form_ttn.cpp" line="559"/>
        <source>, отримав водій/експедитор</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="825"/>
        <source>Бухгалтер (відповідальна особа вантажовідправника)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="861"/>
        <source>Відпуск дозволив</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="884"/>
        <location filename="../src/Form_ttn.cpp" line="563"/>
        <source>(П.І.Б., посада, підпис, печатка)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="897"/>
        <location filename="../src/Form_ttn.cpp" line="564"/>
        <source>Усього відпущено на загальну суму</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="920"/>
        <source>,  у т.ч ПДВ</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="943"/>
        <location filename="../src/Form_ttn.cpp" line="566"/>
        <source>Супровідні документи на вантаж</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="966"/>
        <source>Транспортні послуги, які надаються автомобільним перевізником</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="999"/>
        <location filename="../src/Form_ttn.cpp" line="568"/>
        <source>Подтвердить</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="1012"/>
        <location filename="../src/Form_ttn.cpp" line="569"/>
        <source>Закрыть</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="1025"/>
        <location filename="../src/Form_ttn.cpp" line="570"/>
        <source>Печать</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="1038"/>
        <location filename="../src/Form_ttn.cpp" line="571"/>
        <source>ВІДОМОСТІ ПРО ВАНТАЖ</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/Form_ttn.cpp" line="530"/>
        <source>Автомобільний перевезник</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/Form_ttn.cpp" line="532"/>
        <location filename="../src/Form_ttn.cpp" line="535"/>
        <source>(найменування П.І.Б)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/Form_ttn.cpp" line="533"/>
        <source>(П.І.Б, посвідчення водія)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/Form_ttn.cpp" line="546"/>
        <source>Відпуск за довіреністю вантажоодержувача: серія</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/Form_ttn.cpp" line="552"/>
        <source>(відповідає, не відповідає)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/Form_ttn.cpp" line="554"/>
        <source>Кількість місць</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/Form_ttn.cpp" line="560"/>
        <source>Бухгалтер (відповідальна особа вантавідправника)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/Form_ttn.cpp" line="562"/>
        <source>Відпуск дозвалив</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/Form_ttn.cpp" line="565"/>
        <source>,  у.т.ч ПДВ</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/Form_ttn.cpp" line="567"/>
        <source>Транспортні послуги, які надаються автомобільним перевізником:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Form_ttn</name>
    <message>
        <source>Windows styles missing</source>
        <translation type="obsolete">Стили окон не загружены</translation>
    </message>
    <message>
        <source>Weighbridge</source>
        <translation type="obsolete">Weighbridge</translation>
    </message>
    <message>
        <source>Preview</source>
        <translation type="obsolete">Предварительный просмотр</translation>
    </message>
    <message>
        <location filename="../src/Form_ttn.cpp" line="629"/>
        <location filename="../src/Form_ttn.cpp" line="649"/>
        <location filename="../src/Form_ttn.cpp" line="677"/>
        <location filename="../src/Form_ttn.cpp" line="743"/>
        <location filename="../src/Form_ttn.cpp" line="777"/>
        <location filename="../src/Form_ttn.cpp" line="841"/>
        <location filename="../src/Form_ttn.cpp" line="872"/>
        <location filename="../src/Form_ttn.cpp" line="956"/>
        <location filename="../src/Form_ttn.cpp" line="1049"/>
        <location filename="../src/Form_ttn.cpp" line="1080"/>
        <location filename="../src/Form_ttn.cpp" line="1119"/>
        <location filename="../src/Form_ttn.cpp" line="1147"/>
        <location filename="../src/Form_ttn.cpp" line="1275"/>
        <location filename="../src/Form_ttn.cpp" line="1312"/>
        <location filename="../src/Form_ttn.cpp" line="1353"/>
        <location filename="../src/Form_ttn.cpp" line="1384"/>
        <location filename="../src/Form_ttn.cpp" line="2003"/>
        <source>Prom-Soft</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HiddenWeighing</name>
    <message>
        <location filename="../forms/hiddenweighing.ui" line="20"/>
        <source>Hidden weighing</source>
        <translation>Скрытые взвешивания</translation>
    </message>
    <message>
        <location filename="../forms/hiddenweighing.ui" line="26"/>
        <source>Filter</source>
        <translation>Фильтр</translation>
    </message>
    <message>
        <location filename="../forms/hiddenweighing.ui" line="60"/>
        <source>From</source>
        <translation>От</translation>
    </message>
    <message>
        <location filename="../forms/hiddenweighing.ui" line="87"/>
        <source>To</source>
        <translation>До</translation>
    </message>
    <message>
        <location filename="../forms/hiddenweighing.ui" line="125"/>
        <source>Accept</source>
        <translation>Принять</translation>
    </message>
    <message>
        <location filename="../forms/hiddenweighing.ui" line="144"/>
        <source>Reset</source>
        <translation>Сброс</translation>
    </message>
    <message>
        <location filename="../forms/hiddenweighing.ui" line="177"/>
        <source>Show photos</source>
        <translation>Фото</translation>
    </message>
    <message>
        <location filename="../src/HiddenWeighing.cpp" line="28"/>
        <source>Time</source>
        <translation>Время</translation>
    </message>
    <message>
        <location filename="../src/HiddenWeighing.cpp" line="29"/>
        <source>Weight, kg</source>
        <translation>Вес, кг</translation>
    </message>
    <message>
        <location filename="../src/HiddenWeighing.cpp" line="30"/>
        <source>Max weight, kg</source>
        <translation>Максимальный вес, кг</translation>
    </message>
    <message>
        <source>Select the entry first!</source>
        <translation type="obsolete">Сначала выберите запись!</translation>
    </message>
</context>
<context>
    <name>MailSend</name>
    <message>
        <location filename="../src/MailSend.cpp" line="139"/>
        <location filename="../src/MailSend.cpp" line="216"/>
        <source>Sending mail status</source>
        <translation>Отправка почты</translation>
    </message>
    <message>
        <location filename="../src/MailSend.cpp" line="151"/>
        <source>SMTP client: Connection Failed</source>
        <translation>SMTP клиент: Нет соединения с почтовым сервером</translation>
    </message>
    <message>
        <location filename="../src/MailSend.cpp" line="164"/>
        <source>SMTP client: Authentification Failed</source>
        <translation>SMTP клиент: Ошибка аутентификации</translation>
    </message>
    <message>
        <location filename="../src/MailSend.cpp" line="179"/>
        <source>SMTP client: Mail sending failed, Server response:</source>
        <translation>SMTP клиент: Ошибка отправки почты, ответ сервера:</translation>
    </message>
    <message>
        <location filename="../src/MailSend.cpp" line="179"/>
        <location filename="../src/MailSend.cpp" line="190"/>
        <location filename="../src/MailSend.cpp" line="200"/>
        <source> Code: </source>
        <translation>Код: </translation>
    </message>
    <message>
        <location filename="../src/MailSend.cpp" line="190"/>
        <location filename="../src/MailSend.cpp" line="200"/>
        <source>SMTP client: Mail sending successful, Server response:</source>
        <translation>SMTP клиент: Почта отправлена успешно, ответ сервера:</translation>
    </message>
    <message>
        <location filename="../src/MailSend.cpp" line="215"/>
        <source>SMTP client: Mail sending failed, No file to send!</source>
        <translation>SMTP клиент: Ошибка отправки почты,нет файла для отправки!</translation>
    </message>
</context>
<context>
    <name>MyClass</name>
    <message>
        <source>Weight</source>
        <translation type="obsolete">Весовая</translation>
    </message>
    <message>
        <source>Report -</source>
        <translation type="obsolete">Отчет -</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="931"/>
        <source>Login in: </source>
        <translation>Вход пользователя:</translation>
    </message>
    <message>
        <source>The program has not been activated
or the temporary key has expired!
Please contact VIS-Service.</source>
        <translation type="obsolete">Программа не активирована
либо истёк срок действия ключа!
Пожалуйста, обратитесь в ВИС-Сервис.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="939"/>
        <source>Incorrect password</source>
        <translation>Неправильный пароль</translation>
    </message>
    <message>
        <source>Windows styles missing</source>
        <translation type="obsolete">Стили окон не загружены</translation>
    </message>
    <message>
        <source>The code is wrong!
Please contact VIS-Service.</source>
        <translation type="obsolete">Код неправильный!
Обратитесь в ВИС-Сервис.</translation>
    </message>
    <message>
        <source>Enter the activation code for programm.</source>
        <translation type="obsolete">Введите код активации ПО.</translation>
    </message>
    <message>
        <source>Enter the service code.</source>
        <translation type="obsolete">Введите сервисный код.</translation>
    </message>
    <message>
        <source>Click to visit our website.</source>
        <translation type="obsolete">Кликните для перехода на наш сайт.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1101"/>
        <source>Service</source>
        <translation>Служебные</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1102"/>
        <source>Dictionary</source>
        <translation>Справочники</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1103"/>
        <source>Change user</source>
        <translation>Смена пользователя</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1106"/>
        <source>Add user</source>
        <translation>Добавить пользователя</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1111"/>
        <source>Event log</source>
        <translation>Журнал событий</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1137"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1177"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <source>User manual - F1</source>
        <translation type="obsolete">Руководство пользователя F1</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1181"/>
        <location filename="../src/myclass.cpp" line="9786"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <source>Activation</source>
        <translation type="obsolete">Активация ПО</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1158"/>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
    <message>
        <source>Search vehicles by number</source>
        <translation type="obsolete">Поиск авто по номеру</translation>
    </message>
    <message>
        <source>Vehicles ¹</source>
        <translation type="obsolete">Авто №</translation>
    </message>
    <message>
        <source>Enter the vehicles number</source>
        <translation type="obsolete">Введите номер авто</translation>
    </message>
    <message>
        <source>Num fragment</source>
        <translation type="obsolete">Фрагм. номера</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="obsolete">Поиск</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="obsolete">Сброс</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1188"/>
        <source>Print</source>
        <translation>Печать</translation>
    </message>
    <message>
        <source>Cargo accept</source>
        <translation type="obsolete">Приемка груза</translation>
    </message>
    <message>
        <source>Cargo dispatch</source>
        <translation type="obsolete">Отправка груза</translation>
    </message>
    <message>
        <source>Press to accept the vehicle.</source>
        <translation type="obsolete">Нажмите для приёмки авто.</translation>
    </message>
    <message>
        <source>Press to send the vehicle</source>
        <translation type="obsolete">Нажмите для отправки авто</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1241"/>
        <source>Registry</source>
        <translation>Журнал</translation>
    </message>
    <message>
        <source>To view the history of weighing.</source>
        <translation type="obsolete">Посмотреть журнал взвешиваний.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1205"/>
        <source>Delete
record</source>
        <translation>Удалить
запись</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1207"/>
        <source>Deletes the selected entry in the table.</source>
        <translation>Удалить взвешивание из таблицы.</translation>
    </message>
    <message>
        <source>To enter the settings menu.</source>
        <translation type="obsolete">Вход в настройки.</translation>
    </message>
    <message>
        <source>To edit dictionaries.</source>
        <translation type="obsolete">Редактировать справочники.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1249"/>
        <source>Zero &gt;0&lt;</source>
        <translation>Обнуление &gt;0&lt;</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1279"/>
        <source>WEIGHT</source>
        <translation>МАССА</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1286"/>
        <source>
kg</source>
        <translation>
кг</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1343"/>
        <source>CAM 1</source>
        <translation>Камера 1</translation>
    </message>
    <message>
        <source>View weighing photo ¹1 cam ¹1</source>
        <translation type="obsolete">Просмотр фото 1 камеры 1</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1364"/>
        <source>CAM 2</source>
        <translation>Камера 2</translation>
    </message>
    <message>
        <source>View weighing photo ¹2 cam ¹1</source>
        <translation type="obsolete">Просмотр фото 2 камеры 1</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1386"/>
        <source>CAM 3</source>
        <translation>Камера 3</translation>
    </message>
    <message>
        <source>View weighing photo ¹1 cam ¹2</source>
        <translation type="obsolete">Просмотр фото 1 камеры 2</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1407"/>
        <source>CAM 4</source>
        <translation>Камера 4</translation>
    </message>
    <message>
        <source>View weighing photo ¹2 cam ¹2</source>
        <translation type="obsolete">Просмотр фото 2 камеры 2</translation>
    </message>
    <message>
        <source>View weighing photo ¹1 cam ¹3</source>
        <translation type="obsolete">Просмотр фото 1 камеры 3</translation>
    </message>
    <message>
        <source>View weighing photo ¹1 cam ¹4</source>
        <translation type="obsolete">Просмотр фото 1 камеры 4</translation>
    </message>
    <message>
        <source>View weighing photo ¹2 cam ¹3</source>
        <translation type="obsolete">Просмотр фото 2 камеры 3</translation>
    </message>
    <message>
        <source>View weighing photo ¹2 cam ¹4</source>
        <translation type="obsolete">Просмотр фото 2 камеры 4</translation>
    </message>
    <message>
        <source>Click on the traffic light to change its state.</source>
        <translation type="obsolete">Кликните по светофору для изменения его состояния.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1767"/>
        <source>Select the entry to generate the report!</source>
        <translation>Сначала выберите строку для формирования отчета!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1791"/>
        <source>Select record to start!</source>
        <translation>Сначала выберите запись!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1819"/>
        <location filename="../src/myclass.cpp" line="1975"/>
        <source>Data not available</source>
        <translation>Данные отсутствуют</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1837"/>
        <location filename="../src/myclass.cpp" line="1993"/>
        <source>Microsoft Office - Excel is not installed!</source>
        <translation>Microsoft Office - Excel не установлен!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="2209"/>
        <location filename="../src/myclass.cpp" line="2279"/>
        <location filename="../src/myclass.cpp" line="3231"/>
        <location filename="../src/myclass.cpp" line="4701"/>
        <location filename="../src/myclass.cpp" line="4824"/>
        <source>From scales</source>
        <translation>С весопроцессора</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="2211"/>
        <location filename="../src/myclass.cpp" line="2281"/>
        <location filename="../src/myclass.cpp" line="3233"/>
        <location filename="../src/myclass.cpp" line="4703"/>
        <location filename="../src/myclass.cpp" line="4826"/>
        <source>Manually</source>
        <translation>Вручную</translation>
    </message>
    <message>
        <source>Number auto</source>
        <translation type="obsolete">Номер авто</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6438"/>
        <source>Select the entry to delete!</source>
        <translation>Выберите запись для удаления!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6448"/>
        <source>Do you want to remove the entry?</source>
        <translation>Действительно хотите удалить запись?</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6450"/>
        <location filename="../src/myclass.cpp" line="9718"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6451"/>
        <location filename="../src/myclass.cpp" line="9719"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6505"/>
        <source>Removed the record. Trailer num:</source>
        <translation>Запись удалена. Номер прицепа:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6511"/>
        <source>Removed the record. Vehicle num:</source>
        <translation>Запись удалена. Номер авто:</translation>
    </message>
    <message>
        <source>This software - an automatic workstation
weigher (ARM) is intended to control and account
weighing for auto weighing using operator-weigher. 


This software is the property of
the company VIS-Service.</source>
        <translation type="obsolete">Данное программное обеспечение - Автоматическое Рабочее Место
весовщика (АРМ) предназначено для осуществления контроля и учёта
взвешиваний на автовесовых при помощи оператора-весовщика.

Даное программное обеспечение является собственностью
компании ВИС-Сервис.</translation>
    </message>
    <message>
        <source>Cargo send</source>
        <translation type="obsolete">Отправка</translation>
    </message>
    <message>
        <source>   Delete
   record</source>
        <translation type="obsolete">   Удалить
   запись</translation>
    </message>
    <message>
        <source>Vehicles ?</source>
        <translation type="obsolete">Авто №</translation>
    </message>
    <message>
        <source>View weighing photo ?1 cam ?1</source>
        <translation type="obsolete">Просмотр фото 1 камеры 1</translation>
    </message>
    <message>
        <source>View weighing photo ?2 cam ?1</source>
        <translation type="obsolete">Просмотр фото 2 камеры 1</translation>
    </message>
    <message>
        <source>View weighing photo ?1 cam ?2</source>
        <translation type="obsolete">Просмотр фото 1 камеры 2</translation>
    </message>
    <message>
        <source>View weighing photo ?2 cam ?2</source>
        <translation type="obsolete">Просмотр фото 2 камеры 2</translation>
    </message>
    <message>
        <source>View weighing photo ?1 cam ?3</source>
        <translation type="obsolete">Просмотр фото 1 камеры 3</translation>
    </message>
    <message>
        <source>View weighing photo ?1 cam ?4</source>
        <translation type="obsolete">Просмотр фото 1 камеры 4</translation>
    </message>
    <message>
        <source>View weighing photo ?2 cam ?3</source>
        <translation type="obsolete">Просмотр фото 2 камеры 3</translation>
    </message>
    <message>
        <source>View weighing photo ?2 cam ?4</source>
        <translation type="obsolete">Просмотр фото 2 камеры 4</translation>
    </message>
    <message>
        <source>This software - an automatic workstation 
weigher (ARM) is intended to control and account 
weighing for auto weighing using operator-weigher. 


This software is the property of 
the company VIS-Service.</source>
        <translation type="obsolete">Данное программное обеспечение - Автоматическое Рабочее Место
весовщика (АРМ) предназначено для осуществления контроля и учёта
взвешиваний на автовесовых при помощи оператора-весовщика.

Даное программное обеспечение является собственностью
компании ВИС-Сервис.</translation>
    </message>
    <message>
        <source>Vehicles #</source>
        <translation type="obsolete">Авто №</translation>
    </message>
    <message>
        <source>Optional devices</source>
        <translation type="obsolete">Доп. оборудование</translation>
    </message>
    <message>
        <source>This software - an automatic workstation 
weigher (AWW) is intended to control and account 
weighing for auto weighing using operator-weigher. 


This software is the property of 
the company VIS-Service.</source>
        <translation type="obsolete">Данное программное обеспечение - Автоматическое Рабочее Место
весовщика (АРМ) предназначено для осуществления контроля и учёта
взвешиваний на автовесовых при помощи оператора-весовщика.

Даное программное обеспечение является собственностью
компании ВИС-Сервис.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="9814"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="2514"/>
        <location filename="../src/myclass.cpp" line="3317"/>
        <location filename="../src/myclass.cpp" line="3337"/>
        <location filename="../src/myclass.cpp" line="3471"/>
        <location filename="../src/myclass.cpp" line="3790"/>
        <location filename="../src/myclass.cpp" line="6819"/>
        <location filename="../src/myclass.cpp" line="6839"/>
        <source>Select the entry first!</source>
        <translation>Сначала выберите запись!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="2579"/>
        <location filename="../src/myclass.cpp" line="2595"/>
        <location filename="../src/myclass.cpp" line="4963"/>
        <location filename="../src/myclass.cpp" line="4974"/>
        <location filename="../src/myclass.cpp" line="5033"/>
        <location filename="../src/myclass.cpp" line="5044"/>
        <location filename="../src/myclass.cpp" line="5108"/>
        <location filename="../src/myclass.cpp" line="5119"/>
        <location filename="../src/myclass.cpp" line="5177"/>
        <location filename="../src/myclass.cpp" line="5188"/>
        <source>Communication with scales missing!</source>
        <translation>Связь с весами отсутствует!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="2813"/>
        <source>Trailer accepted:</source>
        <translation>Прицеп принят:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="2818"/>
        <source>Vehicle accepted:</source>
        <translation>Авто принято:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="3113"/>
        <source>Trailer send:</source>
        <translation>Прицеп отправлен:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="3118"/>
        <source>Vehicle send:</source>
        <translation>Авто отправлено:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="3750"/>
        <location filename="../src/myclass.cpp" line="4012"/>
        <location filename="../src/myclass.cpp" line="4323"/>
        <source>Error. There are no photos to save!</source>
        <translation>Ошибка. Нет фото для сохранения!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="4656"/>
        <source>Data according to the filters was not found</source>
        <translation>Данные по заданным фильтрам отсутствуют</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="3245"/>
        <location filename="../src/myclass.cpp" line="4715"/>
        <location filename="../src/myclass.cpp" line="4839"/>
        <source>Photos</source>
        <translation>Фото</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1360"/>
        <source>View weighing photo #1 cam #1</source>
        <translation>Просмотр фото 1 камеры 1</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1382"/>
        <source>View weighing photo #2 cam #1</source>
        <translation>Просмотр фото 2 камеры 1</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1403"/>
        <source>View weighing photo #1 cam #2</source>
        <translation>Просмотр фото 1 камеры 2</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1424"/>
        <source>View weighing photo #2 cam #2</source>
        <translation>Просмотр фото 2 камеры 2</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1432"/>
        <source>View weighing photo #1 cam #3</source>
        <translation>Просмотр фото 1 камеры 3</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1447"/>
        <source>View weighing photo #1 cam #4</source>
        <translation>Просмотр фото 1 камеры 4</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1439"/>
        <source>View weighing photo #2 cam #3</source>
        <translation>Просмотр фото 2 камеры 3</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="94"/>
        <source>The trial version has expired!
Please activate the software
Technical support - 093 009 9990 
Prom-Soft</source>
        <translation>Срок действия пробной версии ПО окончен!
Пожалуйста активируйте вашу копию.
Телефон технической поддержки 093 009 999
Prom-Soft</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="217"/>
        <source>Please wait until the process ends.</source>
        <translation>Пожалуйста, дождитесь окончания процесса.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="227"/>
        <source>Database backup</source>
        <translation>Резервная копия</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="233"/>
        <source>Push the button to start the process. It can take a long time.</source>
        <translation>Для старта процесса резервирования нажмите кнопку. Это может занять длительное время.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="234"/>
        <source>Save photos</source>
        <translation>Сохранять фото</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="237"/>
        <source>Dump</source>
        <translation>Создать копию</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="249"/>
        <source>Database restore</source>
        <translation>Восстановление базы данных</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="255"/>
        <source>Select the file to restore. Be careful! Current data will be deleted!</source>
        <translation>Выберите файл, который требуется восстановить. Будьте осторожны! Текущие данные будут удалены!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="266"/>
        <location filename="../src/myclass.cpp" line="1149"/>
        <source>Restore</source>
        <translation>Восстановить</translation>
    </message>
    <message>
        <source>Service engineer password</source>
        <translation type="obsolete">Пароль сервисного инженера</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1115"/>
        <source>Tara dictionary</source>
        <translation>Справочник тары</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1125"/>
        <source>Analytics of sensor&apos;s codes</source>
        <translation>Аналитика датчиков</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1147"/>
        <source>Database</source>
        <translation>База данных</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1151"/>
        <source>Backup</source>
        <translation>Резервное копирование</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1156"/>
        <source>Open the dump folder</source>
        <translation>Открыть папку с копиями</translation>
    </message>
    <message>
        <source>Brutto</source>
        <translation type="obsolete">Брутто</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1201"/>
        <source>Tara</source>
        <translation>Тара</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1224"/>
        <location filename="../src/myclass.cpp" line="12508"/>
        <source>Check in</source>
        <translation>Въезд</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1232"/>
        <source>Check out</source>
        <translation>Выезд</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1275"/>
        <location filename="../src/myclass.cpp" line="9672"/>
        <location filename="../src/myclass.cpp" line="9683"/>
        <source>No connection</source>
        <translation>Нет подключения</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="93"/>
        <location filename="../src/myclass.cpp" line="12384"/>
        <source>Software License</source>
        <translation>Лицензия на ПО</translation>
    </message>
    <message>
        <source>The trial version has expired!
Please activate the software
Technical support - 097 898 78 25
OOO &apos;Etalon-Systems&apos;</source>
        <translation type="obsolete">Срок действия пробной версии ПО окончен!
Пожалуйста активируйте вашу копию.
Телефон технической поддержки  097 898 7825
ООО &quot;Эталон-Систем&quot;</translation>
    </message>
    <message>
        <source>Dispenser</source>
        <translation type="obsolete">Дозатор</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1454"/>
        <source>View weighing photo #2 cam #4</source>
        <translation>Просмотр фото 2 камеры 4</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1582"/>
        <source>Customer Service. Maintenance, repair, calibration of scales. 067-754-96-76</source>
        <translation>Сервисная служба. Обслуживание, ремонт, поверка весов. 067-754-96-76</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1771"/>
        <location filename="../src/myclass.cpp" line="1795"/>
        <location filename="../src/myclass.cpp" line="1823"/>
        <location filename="../src/myclass.cpp" line="1841"/>
        <location filename="../src/myclass.cpp" line="1979"/>
        <location filename="../src/myclass.cpp" line="1997"/>
        <location filename="../src/myclass.cpp" line="2518"/>
        <location filename="../src/myclass.cpp" line="2583"/>
        <location filename="../src/myclass.cpp" line="2599"/>
        <location filename="../src/myclass.cpp" line="3794"/>
        <location filename="../src/myclass.cpp" line="4016"/>
        <location filename="../src/myclass.cpp" line="4327"/>
        <location filename="../src/myclass.cpp" line="4967"/>
        <location filename="../src/myclass.cpp" line="4978"/>
        <location filename="../src/myclass.cpp" line="5037"/>
        <location filename="../src/myclass.cpp" line="5048"/>
        <location filename="../src/myclass.cpp" line="5112"/>
        <location filename="../src/myclass.cpp" line="5123"/>
        <location filename="../src/myclass.cpp" line="5181"/>
        <location filename="../src/myclass.cpp" line="5192"/>
        <location filename="../src/myclass.cpp" line="6823"/>
        <location filename="../src/myclass.cpp" line="6843"/>
        <location filename="../src/myclass.cpp" line="7622"/>
        <location filename="../src/myclass.cpp" line="9721"/>
        <location filename="../src/myclass.cpp" line="10709"/>
        <location filename="../src/myclass.cpp" line="10977"/>
        <location filename="../src/myclass.cpp" line="10993"/>
        <location filename="../src/myclass.cpp" line="11009"/>
        <location filename="../src/myclass.cpp" line="11345"/>
        <location filename="../src/myclass.cpp" line="11420"/>
        <location filename="../src/myclass.cpp" line="11447"/>
        <location filename="../src/myclass.cpp" line="11457"/>
        <location filename="../src/myclass.cpp" line="11487"/>
        <location filename="../src/myclass.cpp" line="11554"/>
        <location filename="../src/myclass.cpp" line="11564"/>
        <location filename="../src/myclass.cpp" line="11581"/>
        <location filename="../src/myclass.cpp" line="11796"/>
        <source>Prom-Soft</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="2213"/>
        <location filename="../src/myclass.cpp" line="2283"/>
        <location filename="../src/myclass.cpp" line="3235"/>
        <location filename="../src/myclass.cpp" line="4705"/>
        <location filename="../src/myclass.cpp" line="4828"/>
        <source>Saved tara</source>
        <translation>Сохраненная</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="3321"/>
        <location filename="../src/myclass.cpp" line="3341"/>
        <location filename="../src/myclass.cpp" line="3475"/>
        <location filename="../src/myclass.cpp" line="3754"/>
        <source>AV-Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="4069"/>
        <source>Error</source>
        <translation type="unfinished">Ошибка</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="4070"/>
        <source>Not found TelSend.dll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="4887"/>
        <source>Entered the settings</source>
        <translation>Вход в настройки</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="4916"/>
        <source>Entered the registry:</source>
        <translation>Вход в журнал:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="4933"/>
        <source>Closed the registry:</source>
        <translation>Закрыт журнал:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="4943"/>
        <source>Closed the settings:</source>
        <translation>Закрыты настройки:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="4988"/>
        <location filename="../src/myclass.cpp" line="5058"/>
        <location filename="../src/myclass.cpp" line="5133"/>
        <location filename="../src/myclass.cpp" line="5202"/>
        <source>by Admin</source>
        <translation>под Администратором</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="4989"/>
        <location filename="../src/myclass.cpp" line="5059"/>
        <location filename="../src/myclass.cpp" line="5134"/>
        <location filename="../src/myclass.cpp" line="5203"/>
        <source>Do you want to weigh the vehicle?</source>
        <translation>Всё равно взвесить авто?</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5003"/>
        <source>Weighing Brutto ignoring auto position!</source>
        <translation>Взвешивание Брутто с игнорированием позиции авто!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5073"/>
        <source>Weighing Tara ignoring auto position!</source>
        <translation>Взвешивание Тары с игнорированием позиции авто!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5148"/>
        <source>Weighing Brutto trailer ignoring auto position!</source>
        <translation>Взвешивание Брутто прицепа с игнорированием позиции авто!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5217"/>
        <source>Weighing Tara trailer ignoring auto position!</source>
        <translation>Взвешивание Тары прицепа с игнорированием позиции авто!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5251"/>
        <source>Zero error</source>
        <translation>Ошибка обнуления</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5251"/>
        <source>COM-port closed</source>
        <translation>COM-порт закрыт</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5257"/>
        <source>Set to zero: </source>
        <translation>Обнуление:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5337"/>
        <source>Entered dictionaries: </source>
        <translation>Вход в справочники: </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5356"/>
        <source>Dictionaries closed: </source>
        <translation>Справочники закрыты: </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5383"/>
        <location filename="../src/myclass.cpp" line="5488"/>
        <location filename="../src/myclass.cpp" line="5593"/>
        <location filename="../src/myclass.cpp" line="5698"/>
        <location filename="../src/myclass.cpp" line="5804"/>
        <location filename="../src/myclass.cpp" line="5911"/>
        <location filename="../src/myclass.cpp" line="6013"/>
        <source>Enter the name</source>
        <translation>Ввод наименования</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5402"/>
        <location filename="../src/myclass.cpp" line="5462"/>
        <location filename="../src/myclass.cpp" line="5507"/>
        <location filename="../src/myclass.cpp" line="5567"/>
        <location filename="../src/myclass.cpp" line="5612"/>
        <location filename="../src/myclass.cpp" line="5672"/>
        <location filename="../src/myclass.cpp" line="5717"/>
        <location filename="../src/myclass.cpp" line="5778"/>
        <location filename="../src/myclass.cpp" line="5823"/>
        <location filename="../src/myclass.cpp" line="5883"/>
        <location filename="../src/myclass.cpp" line="5930"/>
        <location filename="../src/myclass.cpp" line="5989"/>
        <location filename="../src/myclass.cpp" line="6032"/>
        <location filename="../src/myclass.cpp" line="6091"/>
        <location filename="../src/myclass.cpp" line="6254"/>
        <location filename="../src/myclass.cpp" line="6281"/>
        <location filename="../src/myclass.cpp" line="6308"/>
        <location filename="../src/myclass.cpp" line="6335"/>
        <location filename="../src/myclass.cpp" line="6362"/>
        <location filename="../src/myclass.cpp" line="6390"/>
        <location filename="../src/myclass.cpp" line="6416"/>
        <source>Database error!</source>
        <translation>Ошибка БД!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5424"/>
        <location filename="../src/myclass.cpp" line="5529"/>
        <location filename="../src/myclass.cpp" line="5634"/>
        <location filename="../src/myclass.cpp" line="5739"/>
        <location filename="../src/myclass.cpp" line="5845"/>
        <location filename="../src/myclass.cpp" line="5952"/>
        <location filename="../src/myclass.cpp" line="6054"/>
        <source>The name is already exist!</source>
        <translation>Такое наименование уже есть!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5439"/>
        <location filename="../src/myclass.cpp" line="5469"/>
        <location filename="../src/myclass.cpp" line="5544"/>
        <location filename="../src/myclass.cpp" line="5574"/>
        <location filename="../src/myclass.cpp" line="5649"/>
        <location filename="../src/myclass.cpp" line="5679"/>
        <location filename="../src/myclass.cpp" line="5755"/>
        <location filename="../src/myclass.cpp" line="5785"/>
        <location filename="../src/myclass.cpp" line="5860"/>
        <location filename="../src/myclass.cpp" line="5890"/>
        <location filename="../src/myclass.cpp" line="5967"/>
        <location filename="../src/myclass.cpp" line="5996"/>
        <location filename="../src/myclass.cpp" line="6069"/>
        <location filename="../src/myclass.cpp" line="6098"/>
        <source>The record is successfully saved.</source>
        <translation>Запись успешно сохранена.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5446"/>
        <location filename="../src/myclass.cpp" line="5476"/>
        <location filename="../src/myclass.cpp" line="5551"/>
        <location filename="../src/myclass.cpp" line="5581"/>
        <location filename="../src/myclass.cpp" line="5656"/>
        <location filename="../src/myclass.cpp" line="5686"/>
        <location filename="../src/myclass.cpp" line="5762"/>
        <location filename="../src/myclass.cpp" line="5792"/>
        <location filename="../src/myclass.cpp" line="5867"/>
        <location filename="../src/myclass.cpp" line="5897"/>
        <location filename="../src/myclass.cpp" line="5974"/>
        <location filename="../src/myclass.cpp" line="6002"/>
        <location filename="../src/myclass.cpp" line="6076"/>
        <location filename="../src/myclass.cpp" line="6105"/>
        <source>Added to dictionary: </source>
        <translation>Добавлено в справочник: </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6242"/>
        <location filename="../src/myclass.cpp" line="6269"/>
        <location filename="../src/myclass.cpp" line="6296"/>
        <location filename="../src/myclass.cpp" line="6323"/>
        <location filename="../src/myclass.cpp" line="6350"/>
        <location filename="../src/myclass.cpp" line="6378"/>
        <location filename="../src/myclass.cpp" line="6404"/>
        <source>The entry was successfully deleted from the database!</source>
        <translation>Запись успешно удалена из БД!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6249"/>
        <location filename="../src/myclass.cpp" line="6276"/>
        <location filename="../src/myclass.cpp" line="6303"/>
        <location filename="../src/myclass.cpp" line="6330"/>
        <location filename="../src/myclass.cpp" line="6357"/>
        <location filename="../src/myclass.cpp" line="6385"/>
        <location filename="../src/myclass.cpp" line="6411"/>
        <source>Deleted from dictionaries: </source>
        <translation>Удалено из справочника: </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6442"/>
        <source>Delete Record</source>
        <translation>Удалить запись</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6452"/>
        <source>Delete record</source>
        <translation>Удалить запись</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6887"/>
        <location filename="../src/myclass.cpp" line="7664"/>
        <source>Preview</source>
        <translation>Предварительный просмотр</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6893"/>
        <source>Weight data from  </source>
        <translation>Данные взвешиваний от </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6899"/>
        <source>Operating mode:</source>
        <translation>Режим работы:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6900"/>
        <source>Accepting</source>
        <translation>Приём</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6900"/>
        <source>Sending</source>
        <translation>Отправка</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6936"/>
        <source>Total gross: </source>
        <translation>Сумма брутто: </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6939"/>
        <source>Total tare: </source>
        <translation>Сумма тара: </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6942"/>
        <source>Total net: </source>
        <translation>Сумма нетто: </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6946"/>
        <location filename="../src/myclass.cpp" line="7704"/>
        <source>Responsible: </source>
        <translation>Ответственный: </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6948"/>
        <source>Date/time </source>
        <translation>Дата/время </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7146"/>
        <location filename="../src/myclass.cpp" line="7180"/>
        <location filename="../src/myclass.cpp" line="12908"/>
        <source>Vehicle
number</source>
        <translation>Номер
авто</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7147"/>
        <source>Sender</source>
        <translation>Отправитель</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7148"/>
        <source>Recipient</source>
        <translation>Получатель</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7149"/>
        <source>Payer</source>
        <translation>Плательщик</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7150"/>
        <source>Carrier</source>
        <translation>Перевозчик</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7151"/>
        <source>Cargo</source>
        <translation>Груз</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7152"/>
        <source>Cargo value</source>
        <translation>Цена груза</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7173"/>
        <source>Scales</source>
        <translation>Весы</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7178"/>
        <location filename="../src/myclass.cpp" line="12906"/>
        <source>Supplier</source>
        <translation>Поставщик</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7179"/>
        <location filename="../src/myclass.cpp" line="12907"/>
        <source>Material</source>
        <translation>Материал</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7184"/>
        <location filename="../src/myclass.cpp" line="12912"/>
        <source>Blocked</source>
        <translation>Блокирована</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="9810"/>
        <source>This software is an automatic workstation 
weighing system wich is intended to control and account 
auto weighing with assistance of operator-weigher. 


Technical support - 093 009 9990 
Prom-Soft</source>
        <translation>Данное программное обеспечение - Автоматическое Рабочее Место
весовщика  предназначено для осуществления контроля и учёта
взвешиваний на автовесовых при помощи оператора-весовщика.

Техническая поддержка 
093 009 9990    
Prom-Soft</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12385"/>
        <source>The trial version has expired!
Please activate the software
Technical support - 093 009 9990 
 &apos;Prom-Soft&apos;</source>
        <translation>Срок действия пробной версии ПО окончен!
Пожалуйста активируйте вашу копию.
Телефон технической поддержки 093 009 999
Prom-Soft</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12464"/>
        <source>Registration</source>
        <translation>Регистрация</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12605"/>
        <location filename="../src/myclass.cpp" line="12713"/>
        <source>Repeated reading of RFID card %1 !</source>
        <translation>Повторное срабатывание карты %1 !</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12653"/>
        <source>Second weighing for card # </source>
        <translation>Второе взвешивание для карты № </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12654"/>
        <location filename="../src/myclass.cpp" line="12757"/>
        <location filename="../src/myclass.cpp" line="12819"/>
        <source>, auto number </source>
        <translation>, авто с номером </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12655"/>
        <location filename="../src/myclass.cpp" line="12758"/>
        <location filename="../src/myclass.cpp" line="12820"/>
        <source>, material </source>
        <translation>, материалом </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12656"/>
        <location filename="../src/myclass.cpp" line="12759"/>
        <location filename="../src/myclass.cpp" line="12821"/>
        <source> and supplier </source>
        <translation>и поставщиком </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12656"/>
        <location filename="../src/myclass.cpp" line="12759"/>
        <location filename="../src/myclass.cpp" line="12821"/>
        <source> is done.</source>
        <translation>сделано.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12756"/>
        <source>First weighing for card # </source>
        <translation>Первое взвешивание для карты № </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12817"/>
        <source>First and Second weighing for card # </source>
        <translation>Первоеи второе взвешивание для карты № </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12818"/>
        <source>, auto tare </source>
        <translation>, тара авто</translation>
    </message>
    <message>
        <source>Second weighing for card ? %1, auto number %2, material %3 and supplier %4 is done.</source>
        <translation type="obsolete">Второе взвешивние для карты № %1, номером авто %2, материалом %3 и поставщиком %4 сделано.</translation>
    </message>
    <message>
        <source>First weighing for card ? %1, auto number %2, material %3 and supplier %4 is done.</source>
        <translation type="obsolete">Первое взвешивние для карты № %1, номером авто %2, материалом %3 и поставщиком %4 сделано.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12861"/>
        <source>Weighing is not possible! Card with code %1 is not registered!</source>
        <translation>Взвешивание невозможно! Карта с кодом %1 не зарегистрирована!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1194"/>
        <location filename="../src/myclass.cpp" line="7154"/>
        <source>Gross</source>
        <translation>Брутто</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="540"/>
        <source>Enter password</source>
        <translation>Введите пароль</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1130"/>
        <source>Hidden weighing</source>
        <translation>Скрытые взвешивания</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1118"/>
        <source>Cards mode</source>
        <translation>Режим RFID-карт</translation>
    </message>
    <message>
        <source>Dispenser Mode</source>
        <translation type="obsolete">Режим Дозатор</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1179"/>
        <source>Enable modules</source>
        <translation>Активация модулей</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="2018"/>
        <source>(REAL)</source>
        <translation type="unfinished">(факт.)</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7155"/>
        <source>Tare</source>
        <translation>Тара</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7156"/>
        <location filename="../src/myclass.cpp" line="7177"/>
        <location filename="../src/myclass.cpp" line="12905"/>
        <source>Net</source>
        <translation>Нетто</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7157"/>
        <location filename="../src/myclass.cpp" line="7172"/>
        <location filename="../src/myclass.cpp" line="12901"/>
        <source>Date
time</source>
        <translation>Дата
время</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7158"/>
        <source>Waybill</source>
        <translation>Накл.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7159"/>
        <source>User</source>
        <translation>Принял</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7160"/>
        <location filename="../src/myclass.cpp" line="7181"/>
        <location filename="../src/myclass.cpp" line="12909"/>
        <source>Trailer
number</source>
        <translation>Номер
прицепа</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7161"/>
        <source>Gross
(trailer)</source>
        <translation>Брутто
(прицеп)</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7162"/>
        <source>Tare
(trailer)</source>
        <translation>Тара
(прицеп)</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7163"/>
        <source>Net
(trailer)</source>
        <translation>Нетто
(прицеп)</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7164"/>
        <source>Tara
enter</source>
        <translation>Ввод
тары</translation>
    </message>
    <message>
        <source>Entries by specified criteria was not found!</source>
        <translation type="obsolete">Записи по заданному критерию не найдены!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7462"/>
        <location filename="../src/myclass.cpp" line="7478"/>
        <location filename="../src/myclass.cpp" line="7494"/>
        <location filename="../src/myclass.cpp" line="7510"/>
        <source>Cam is turn off</source>
        <translation>Камера выключена</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7618"/>
        <source>There is no data to print!</source>
        <translation>Нет данных для печати!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7671"/>
        <source>vehicles accepted</source>
        <translation>приёмке авто</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7673"/>
        <source>vehicles sended</source>
        <translation>отправке авто</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7676"/>
        <source>Weight data - </source>
        <translation>Данные взвешиваний - </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7706"/>
        <source>Date time </source>
        <translation>Дата время </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="9621"/>
        <source>Start server</source>
        <translation>Старт сервера</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="9647"/>
        <source>Stop server</source>
        <translation>Стоп сервера</translation>
    </message>
    <message>
        <source>User: </source>
        <translation type="obsolete">Пользователь: </translation>
    </message>
    <message>
        <source>Scales link status </source>
        <translation type="obsolete">Связь с весами </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="9668"/>
        <location filename="../src/myclass.cpp" line="9679"/>
        <source>Connected</source>
        <translation>Подключено</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="obsolete">Отключено</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="9716"/>
        <source>Do you want to exit the program?</source>
        <translation>Выйти из программы?</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="9757"/>
        <source>Exit program</source>
        <translation>Выход из ПО</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="10722"/>
        <source>Attempt to send e-mail successfully </source>
        <translation>Успешная попытка отсылки на почту </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="10726"/>
        <source>Sended to </source>
        <translation>Отправлено на </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="10732"/>
        <source>Attempt to send e-mail unsuccessful </source>
        <translation>Неуспешная попытка отправки на почту </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="10736"/>
        <source>The letter is not sent to destination. Check your mail settings and internet connection.</source>
        <translation>Письмо не отправлено. Проверьте настройки почты и интернет соединение.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11791"/>
        <source>The weight on the weighing platform is less than the minimum </source>
        <translation>Вес на платформе меньше минимального </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11775"/>
        <location filename="../src/myclass.cpp" line="12571"/>
        <source>The vehicle is not correctly standing on the weighing platform!</source>
        <translation>Авто некорректно расположено на весовой платформе!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11732"/>
        <source>CABINET IS </source>
        <translation>ШКАФ </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11732"/>
        <source>CLOSED</source>
        <translation>ЗАКРЫТ</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11732"/>
        <source>OPENED</source>
        <translation>ОТКРЫТ</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11774"/>
        <source>Positioning</source>
        <translation>Позиционирование</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11336"/>
        <source>Setting of zero successful.
Press &apos;Ok&apos; to continue.</source>
        <translation>ВП обнулен успешно.
Нажмите &apos;Ок&apos; для продолжения.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1098"/>
        <source> - Material accounting system</source>
        <translation> - Система учета материалов</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7175"/>
        <location filename="../src/myclass.cpp" line="12903"/>
        <source>Weight 1</source>
        <translation>Взвешивание 1</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7176"/>
        <location filename="../src/myclass.cpp" line="12904"/>
        <source>Weight 2</source>
        <translation>Взвешивание 2</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7185"/>
        <location filename="../src/myclass.cpp" line="12913"/>
        <source>Photo</source>
        <translation>Фото</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7174"/>
        <location filename="../src/myclass.cpp" line="12914"/>
        <source>RFID Code</source>
        <translation>Код карты</translation>
    </message>
    <message>
        <source>The trial version has expired!
Please activate the software</source>
        <translation type="obsolete">Срок действия пробной версии ПО окончен!
Пожалуйста активируйте вашу копию</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1098"/>
        <source>Prom-Soft </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7182"/>
        <location filename="../src/myclass.cpp" line="12910"/>
        <source>Driver</source>
        <translation>Водитель</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7183"/>
        <location filename="../src/myclass.cpp" line="12911"/>
        <source>Order number</source>
        <translation>Порядк. номер</translation>
    </message>
    <message>
        <source>This software - an automatic workstation 
weigher (AWW) is intended to control and account 
weighing for auto weighing using operator-weigher. 


This software is the property of 
the company ETALON-System.</source>
        <translation type="obsolete">Данное программное обеспечение разработано и принадлежит ООО «Эталон-Систем».
Любое копирование и распространение без согласования, строго запрещено.
ООО «Эталон-Систем» г.Днепр, 
телефон 097 898 78 25.   адрес сайта www.e-s.in.ua
Весовое оборудование и программное обеспечение.
Системы АСУ  технологических процессов и  программы учета.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11340"/>
        <source>Setting of zero unsuccessful.
Press &apos;Ok&apos; to continue.</source>
        <translation>Не удалось обнулить ВП.
Нажмите &apos;Ок&apos; для продолжения.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11416"/>
        <source>Folder to save the dump file can not be found. A new folder will be created.</source>
        <translation>Папка для резервного копирования базы данных не найдена. Будет создана новая папка.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11443"/>
        <source>The dump was successfully created. Filename: </source>
        <translation>Дамп был успешно создан с именем:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11453"/>
        <source>Creating dump failed.</source>
        <translation>Создание дампа не удалось.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11483"/>
        <source>Dump file can not be found.</source>
        <translation>Файлы не обнаружены.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11550"/>
        <source>The database is restored. Please restart the program.</source>
        <translation>База данных восстановлена. Пожалуйста, перезапустите программу.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11560"/>
        <source>Unexpected error.</source>
        <translation>Непредвиденная ошибка.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11577"/>
        <source>Dump folder can not be found. Make your first dump for creation it.</source>
        <translation>Папка с сохранёнными файлами не обнаружена. Совершите ваше первое сохранение для её создания.</translation>
    </message>
    <message>
        <source>Weighbridge</source>
        <translation type="obsolete">Weighbridge</translation>
    </message>
</context>
<context>
    <name>MyClassClass</name>
    <message>
        <location filename="../forms/myclass.ui" line="26"/>
        <source>MyClass</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>RegistrationModel</name>
    <message>
        <location filename="../src/Models/registrationModel.cpp" line="56"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="../src/Models/registrationModel.cpp" line="56"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
</context>
<context>
    <name>SettingsForm</name>
    <message>
        <location filename="../src/settings.cpp" line="64"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>COM-port settings</source>
        <translation type="obsolete">Настройки
COM порта</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="89"/>
        <source>Analytics of
sensor&apos;s codes</source>
        <translation>Аналитика
кодов АЦП</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="93"/>
        <source>Recording interval, sec</source>
        <translation>Интервал записи, сек</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="99"/>
        <source>Number of sensors</source>
        <translation>Количество датчиков</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="105"/>
        <source>Contracting party</source>
        <translation>Контрагент</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="111"/>
        <source>Type of scales</source>
        <translation>Тип весов</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="175"/>
        <source>Weighing proc. model</source>
        <translation>Модель весового процессора</translation>
    </message>
    <message>
        <source>Adress WP</source>
        <translation type="obsolete">Адрес ВП</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="231"/>
        <location filename="../src/settings.cpp" line="309"/>
        <source>Koef.</source>
        <translation>Коэф.</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="242"/>
        <source>Baud rate</source>
        <translation>Скорость</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="257"/>
        <source>End of frame</source>
        <translation>Конец посылки</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="271"/>
        <source>Radio modem</source>
        <translation>Радиомодем</translation>
    </message>
    <message>
        <source>Use USB</source>
        <translation type="obsolete">Использовать USB</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="288"/>
        <source>Switch ON</source>
        <translation>Включить в работу</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="471"/>
        <source>Show traffic light</source>
        <translation>Отобразить светофор</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="430"/>
        <source>Use BC-01</source>
        <translation>Использ. БК-01</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="82"/>
        <source>COM-port</source>
        <translation>Подключения</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="442"/>
        <source>WP Model via BC-01</source>
        <translation>Модель ВП для БК-01</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="457"/>
        <source>Input delay, ms</source>
        <translation>Задержка входов</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="116"/>
        <location filename="../src/settings.cpp" line="490"/>
        <location filename="../src/settings.cpp" line="726"/>
        <location filename="../src/settings.cpp" line="837"/>
        <location filename="../src/settings.cpp" line="1005"/>
        <location filename="../src/settings.cpp" line="1133"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Закрыть</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="510"/>
        <source>UDP params</source>
        <translation>Параметры UDP</translation>
    </message>
    <message>
        <source>IP-adress</source>
        <translation type="obsolete">IP адрес</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="219"/>
        <source>Address WP</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="280"/>
        <source>Periodic reopen</source>
        <translation>Периодический рестарт</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="352"/>
        <source>Position on Socket1</source>
        <translation>Позиционирование по Socket1</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="477"/>
        <source>Enable position sensors</source>
        <translation>Задействовать
датчики положения</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="483"/>
        <source>Sensor inversion</source>
        <translation>Инверсия</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="499"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="535"/>
        <location filename="../src/settings.cpp" line="637"/>
        <source>IP-address</source>
        <translation>IP адрес</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="543"/>
        <source>Src. port</source>
        <translation>Порт получатель</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="551"/>
        <source>Dest. port</source>
        <translation>Порт отправитель</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="601"/>
        <source>IP cams</source>
        <translation>IP-камеры</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="635"/>
        <source>IP cam settings</source>
        <translation>Настройки IP камер</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="647"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="299"/>
        <source>Soreness</source>
        <translation>Сорность</translation>
    </message>
    <message>
        <source>Massa sornosti</source>
        <translation type="obsolete">Масса сорности</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="320"/>
        <source>Discret 20kg</source>
        <translation>Дискрет 20кг</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="325"/>
        <source>Show weight</source>
        <translation>Всегда отображать вес</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="330"/>
        <source>Use for brutto only</source>
        <translation>Учитывать только при брутировании</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="336"/>
        <source>Weight by TCP/IP converter</source>
        <translation>Вес по TCP преобразователю</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="343"/>
        <location filename="../src/settings.cpp" line="356"/>
        <location filename="../src/settings.cpp" line="394"/>
        <location filename="../src/settings.cpp" line="414"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="352"/>
        <source>Position on Socket2</source>
        <translation>Позиционирование по Socket2</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="362"/>
        <source>Inversion</source>
        <translation>Инверсия</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="366"/>
        <source>Use S1</source>
        <translation>Исп. S1</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="370"/>
        <source>Use 4ch</source>
        <translation>Исп.4к</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="374"/>
        <source>Ignore</source>
        <translation>Игнор.</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="390"/>
        <source>Trafflight on Socket2</source>
        <translation>Светофор по Socket2</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="410"/>
        <source>Barrier on Socket2</source>
        <translation>Шлагбаум по Socket2</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="659"/>
        <location filename="../src/settings.cpp" line="897"/>
        <location filename="../src/settings.cpp" line="911"/>
        <location filename="../src/settings.cpp" line="925"/>
        <source>Enable</source>
        <translation>Задействовать</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="668"/>
        <source>Photo enable</source>
        <translation>Фотофиксация</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="676"/>
        <source>Photo preview enable</source>
        <translation>Отображение фото</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="683"/>
        <location filename="../src/settings.cpp" line="765"/>
        <source>Login</source>
        <translation>Логин</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="693"/>
        <location filename="../src/settings.cpp" line="772"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="722"/>
        <source>Show cams in a horizontal row (vertical if not checked)</source>
        <translation>Отобразить камеры в ряд по горизонтали (иначе по вертикали)</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="734"/>
        <source>Server autostart</source>
        <translation>Автостарт сервера</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="749"/>
        <source>Email settings</source>
        <translation>eMail</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="751"/>
        <source>Host</source>
        <translation>Почтовый сервер</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="758"/>
        <source>SMTP</source>
        <translation>Порт сервера</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="791"/>
        <source>Send interval, min</source>
        <translation>Интервал отсылки, мин</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="807"/>
        <source>Send each record</source>
        <translation>Отсылать каждую запись</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="811"/>
        <source>Addressee</source>
        <translation>Адресат</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="818"/>
        <source>Use SSL</source>
        <translation>Использовать SSL</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="822"/>
        <source>Mail ON</source>
        <translation>Включить рассылку</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="826"/>
        <source>Save all photos</source>
        <translation>Оправлять фото</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="845"/>
        <source>Excel datetime</source>
        <translation>Формат даты Excel</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="856"/>
        <source>Additional addresses</source>
        <translation>Дополнительные адреса</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="932"/>
        <source>Rfid on Socket2</source>
        <translation>Cчитыватель Rfid карт</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="936"/>
        <source>Rfid</source>
        <translation>Rfid</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="945"/>
        <source>For registration</source>
        <translation>Для регистрации</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="950"/>
        <source>Lamps</source>
        <translation>Лампы</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="1096"/>
        <source>MySQL</source>
        <translation>MySQL</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="1143"/>
        <source>Photo
 stamp</source>
        <translation>Информация
на фото</translation>
    </message>
    <message>
        <source>Internet connection</source>
        <translation type="obsolete">Интернет соединение</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="885"/>
        <source>Optional devices</source>
        <translation>Доп.
оборудование</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="888"/>
        <location filename="../src/settings.cpp" line="902"/>
        <location filename="../src/settings.cpp" line="916"/>
        <location filename="../src/settings.cpp" line="969"/>
        <location filename="../src/settings.cpp" line="975"/>
        <location filename="../src/settings.cpp" line="981"/>
        <location filename="../src/settings.cpp" line="987"/>
        <location filename="../src/settings.cpp" line="993"/>
        <location filename="../src/settings.cpp" line="999"/>
        <source>Socket2</source>
        <translation>IP-коммутатор</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="1011"/>
        <location filename="../src/settings.cpp" line="1037"/>
        <source>TCM 2</source>
        <translation>TCM 2</translation>
    </message>
    <message>
        <source>Photo
time stamp</source>
        <translation type="obsolete">Фото
штамп времени</translation>
    </message>
    <message>
        <source>Photo/ntime stamp</source>
        <translation type="obsolete">Фото
штамп времени</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="1145"/>
        <source>Top-left</source>
        <translation>Верх-лево</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="1151"/>
        <source>Bottom-left</source>
        <translation>Низ-лево</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="1157"/>
        <source>Top-right</source>
        <translation>Верх-право</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="1163"/>
        <source>Bottom-right</source>
        <translation>Низ-право</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="1064"/>
        <source>Minimum weight, kg</source>
        <translation>Минимальный вес, кг</translation>
    </message>
    <message>
        <source>Minimum weight</source>
        <translation type="obsolete">Минимальный вес</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="1071"/>
        <source>Time switch, sec</source>
        <translation>Ожидание, сек</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="1520"/>
        <source>Scale</source>
        <translation>Весы</translation>
    </message>
    <message>
        <source>Checking</source>
        <translation type="obsolete">Проверка</translation>
    </message>
    <message>
        <source>Internet connection is available.</source>
        <translation type="obsolete">Интернет соединение доступно.</translation>
    </message>
    <message>
        <source>Internet connection is not available.</source>
        <translation type="obsolete">Интернет соединение отсутствует.</translation>
    </message>
</context>
<context>
    <name>acceptionForm</name>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="30"/>
        <source>Accept/Send</source>
        <translation>Приёмка/отправка</translation>
    </message>
    <message>
        <source>Current operation</source>
        <translation type="obsolete">Текущая операция</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="46"/>
        <source>Empty vehicle</source>
        <translation>Пустой автомобиль</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="56"/>
        <source>Loaded vehicle</source>
        <translation>Груженый автомобиль</translation>
    </message>
    <message>
        <source>Waybill</source>
        <translation type="obsolete">Накл.</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="68"/>
        <source>Waybill </source>
        <translation>Накладная </translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="72"/>
        <source>Waybill number</source>
        <translation>Номер накл.</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="81"/>
        <source>Vehicle number</source>
        <translation>Номер авто</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="95"/>
        <source>Trailer separately</source>
        <translation>Взвесить прицеп отдельно</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="99"/>
        <source>Trailer number</source>
        <translation>Номер прицепа</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="113"/>
        <source>Sender</source>
        <translation>Отправитель</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="90"/>
        <location filename="../src/AcceptionForms.cpp" line="108"/>
        <location filename="../src/AcceptionForms.cpp" line="122"/>
        <location filename="../src/AcceptionForms.cpp" line="136"/>
        <location filename="../src/AcceptionForms.cpp" line="150"/>
        <location filename="../src/AcceptionForms.cpp" line="164"/>
        <location filename="../src/AcceptionForms.cpp" line="178"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="127"/>
        <source>Recipient</source>
        <translation>Получатель</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="141"/>
        <source>Payer</source>
        <translation>Плательщик</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="155"/>
        <source>Carrier</source>
        <translation>Перевозчик</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="169"/>
        <source>Cargo</source>
        <translation>Груз</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="183"/>
        <source>Cargo price</source>
        <translation>Цена т груза</translation>
    </message>
    <message>
        <source>Last data</source>
        <translation type="obsolete">Последнее заполнение</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="39"/>
        <source>Scales</source>
        <translation>Весы</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="195"/>
        <source>Latest data</source>
        <translation>Последние данные</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="205"/>
        <source>Actual weight</source>
        <translation>Фактический вес</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="209"/>
        <location filename="../src/AcceptionForms.cpp" line="289"/>
        <source>Gross</source>
        <translation>Брутто</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="218"/>
        <location filename="../src/AcceptionForms.cpp" line="232"/>
        <location filename="../src/AcceptionForms.cpp" line="298"/>
        <location filename="../src/AcceptionForms.cpp" line="312"/>
        <source>Weigh</source>
        <translation>Взвесить</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="223"/>
        <location filename="../src/AcceptionForms.cpp" line="303"/>
        <source>Tare</source>
        <translation>Тара</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="237"/>
        <location filename="../src/AcceptionForms.cpp" line="317"/>
        <source>Net</source>
        <translation>Нетто</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="246"/>
        <source>Soreness</source>
        <translation>Сорность</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="257"/>
        <location filename="../src/AcceptionForms.cpp" line="326"/>
        <source>Manual tare</source>
        <translation>Тара вручную</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="265"/>
        <source>Tare from database</source>
        <translation>Тара из справочника</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="285"/>
        <source>Actual trailer weight</source>
        <translation>Фактический вес прицепа</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="349"/>
        <source>Accepted:</source>
        <translation>Принял:</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="353"/>
        <location filename="../src/AcceptionForms.cpp" line="375"/>
        <source>User:</source>
        <translation>Пользователь:</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="362"/>
        <location filename="../src/AcceptionForms.cpp" line="384"/>
        <source>Date/time</source>
        <translation>Дата/время</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="371"/>
        <source>Sended:</source>
        <translation>Отправил:</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="394"/>
        <source>Confirm</source>
        <translation>Принять</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="401"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="1045"/>
        <source>Scales - </source>
        <translation>Весы - </translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="1063"/>
        <source>Prom-Soft</source>
        <translation></translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Закрыть</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="668"/>
        <location filename="../src/AcceptionForms.cpp" line="965"/>
        <source>Enter waybill number</source>
        <translation>Введите номер накладной</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="682"/>
        <location filename="../src/AcceptionForms.cpp" line="969"/>
        <source>Enter vehicle number</source>
        <translation>Введите номер автомобиля</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="694"/>
        <location filename="../src/AcceptionForms.cpp" line="967"/>
        <source>Enter trailer number</source>
        <translation>Введите номер прицепа</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="922"/>
        <location filename="../src/AcceptionForms.cpp" line="929"/>
        <location filename="../src/AcceptionForms.cpp" line="935"/>
        <location filename="../src/AcceptionForms.cpp" line="941"/>
        <location filename="../src/AcceptionForms.cpp" line="947"/>
        <location filename="../src/AcceptionForms.cpp" line="953"/>
        <source>The number of characters entered is greater than MAX possible!</source>
        <translation>Количество введенных символов больше максимально возможного!</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="971"/>
        <source>Enter sender name</source>
        <translation>Введите отправителя</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="973"/>
        <source>Enter recipient name</source>
        <translation>Введите получателя</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="975"/>
        <source>Enter payer name</source>
        <translation>Введите плательщика</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="977"/>
        <source>Enter carrier name</source>
        <translation>Введите перевозчика</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="979"/>
        <source>Enter cargo name</source>
        <translation>Введите наименование груза</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="996"/>
        <source>Set tare manually</source>
        <translation>Установить тару вручную</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="998"/>
        <location filename="../src/AcceptionForms.cpp" line="1002"/>
        <source>Enter tare</source>
        <translation>Введите тару</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="1000"/>
        <source>Set trailer tare manually</source>
        <translation>Установить тару прицепа вручную</translation>
    </message>
    <message>
        <source>Current operation - </source>
        <translation type="obsolete">Текущая операция - </translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="1059"/>
        <source>Data for today is not available.</source>
        <translation>Данных на сегодня не имеется.</translation>
    </message>
    <message>
        <source>Weighbridge</source>
        <translation type="obsolete">Weighbridge</translation>
    </message>
</context>
<context>
    <name>activationOfProgramForm</name>
    <message>
        <source>Activation of program</source>
        <translation type="obsolete">Активация программного обеспечения</translation>
    </message>
    <message>
        <source>Software activation</source>
        <translation type="obsolete">Активация ПО</translation>
    </message>
    <message>
        <source>The activation parameters</source>
        <translation type="obsolete">Параметры активации</translation>
    </message>
    <message>
        <source>Enter activation code</source>
        <translation type="obsolete">Введите код активации</translation>
    </message>
    <message>
        <source>Confirm code</source>
        <translation type="obsolete">Принять код</translation>
    </message>
    <message>
        <source>Expiration date</source>
        <translation type="obsolete">Срок действия</translation>
    </message>
    <message>
        <source>Service activation</source>
        <translation type="obsolete">Активация сервисного обслуживания</translation>
    </message>
    <message>
        <source>Enter service code</source>
        <translation type="obsolete">Введите сервисный код</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="obsolete">Выход</translation>
    </message>
    <message>
        <source>Maintenance</source>
        <translation type="obsolete">Техническое обслуживание</translation>
    </message>
    <message>
        <source>Complete maintenance of scales.
Inquiries by phone at 
Dnepropetrovsk:

</source>
        <translation type="obsolete">Пройдите сервисное обслуживание.
Справки по телефонам
в Днепропетровске:
</translation>
    </message>
    <message>
        <source>(around the clock)</source>
        <translation type="obsolete">(круглосуточно)</translation>
    </message>
    <message>
        <source>(office hours)</source>
        <translation type="obsolete">(в рабочие часы)</translation>
    </message>
</context>
<context>
    <name>dialogVPSensors</name>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="20"/>
        <source>Аналитика датчиков</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="38"/>
        <source>Фильтр</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="56"/>
        <source>По времени</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="68"/>
        <source>с</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="103"/>
        <source>по</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="139"/>
        <source>Отклонение</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="161"/>
        <source>кг</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="184"/>
        <source>код</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="204"/>
        <source>Экспортировать
таблицу в Excel</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="237"/>
        <source>Принять</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="256"/>
        <source>Отмена</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dialogvpsensors.cpp" line="188"/>
        <location filename="../src/dialogvpsensors.cpp" line="336"/>
        <source>Data not available</source>
        <translation>Данные отсутствуют</translation>
    </message>
    <message>
        <location filename="../src/dialogvpsensors.cpp" line="192"/>
        <location filename="../src/dialogvpsensors.cpp" line="210"/>
        <location filename="../src/dialogvpsensors.cpp" line="340"/>
        <location filename="../src/dialogvpsensors.cpp" line="358"/>
        <source>Prom-Soft</source>
        <translation></translation>
    </message>
    <message>
        <source>Weighbridge</source>
        <translation type="obsolete">Weighbridge</translation>
    </message>
    <message>
        <location filename="../src/dialogvpsensors.cpp" line="206"/>
        <location filename="../src/dialogvpsensors.cpp" line="354"/>
        <source>Microsoft Office - Excel is not installed!</source>
        <translation>Microsoft Office - Excel не установлен!</translation>
    </message>
</context>
<context>
    <name>dictionaryFormClass</name>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="23"/>
        <location filename="../src/dictionaryForm.cpp" line="29"/>
        <source>Dictionaries</source>
        <translation>Справочники</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="68"/>
        <source>Senders</source>
        <translation>Отправители</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="69"/>
        <source>Recipients</source>
        <translation>Получатели</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="70"/>
        <source>Payers</source>
        <translation>Плательщики</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="71"/>
        <source>Carriers</source>
        <translation>Перевозчики</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="72"/>
        <source>Cargos</source>
        <translation>Грузы</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="73"/>
        <source>Auto</source>
        <translation>Авто</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="74"/>
        <source>Prizep</source>
        <translatorcomment>Прицеп</translatorcomment>
        <translation>Прицеп</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="76"/>
        <source>Name:</source>
        <translation>Наименования:</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Наименование</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="112"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="119"/>
        <source>Delete record</source>
        <translation>Удалить запись</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="126"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <source>Windows styles missing</source>
        <translation type="obsolete">Стили окон не загружены</translation>
    </message>
</context>
<context>
    <name>logEventsFormClass</name>
    <message>
        <location filename="../src/logEventsForm.cpp" line="26"/>
        <source>Logging</source>
        <translation>Журнал событий</translation>
    </message>
    <message>
        <location filename="../src/logEventsForm.cpp" line="34"/>
        <source>Datetime</source>
        <translation>Время событий</translation>
    </message>
    <message>
        <source>Confirm
 filter</source>
        <translation type="obsolete">Применить
фильтр</translation>
    </message>
    <message>
        <location filename="../src/logEventsForm.cpp" line="36"/>
        <source>Confirm
filter</source>
        <translation>Применить
фильтр</translation>
    </message>
    <message>
        <location filename="../src/logEventsForm.cpp" line="43"/>
        <source>Reset
filter</source>
        <translation>Сброс
фильтра</translation>
    </message>
    <message>
        <location filename="../src/logEventsForm.cpp" line="50"/>
        <source>Date (start)</source>
        <translation>Дата (начало)</translation>
    </message>
    <message>
        <location filename="../src/logEventsForm.cpp" line="56"/>
        <source>Date (end)</source>
        <translation>Дата (конец)</translation>
    </message>
    <message>
        <location filename="../src/logEventsForm.cpp" line="62"/>
        <source>Time(start)</source>
        <translation>Время (начало)</translation>
    </message>
    <message>
        <location filename="../src/logEventsForm.cpp" line="68"/>
        <source>Time(end)</source>
        <translation>Время (конец)</translation>
    </message>
    <message>
        <location filename="../src/logEventsForm.cpp" line="75"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>loginWindowClass</name>
    <message>
        <location filename="../src/loginWindow.cpp" line="22"/>
        <source>Login</source>
        <translation>Пользователь</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="27"/>
        <location filename="../src/loginWindow.cpp" line="76"/>
        <source>Username</source>
        <translation>Пользователь</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="31"/>
        <source>Incorrect name</source>
        <translation>Неправильное имя</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="42"/>
        <location filename="../src/loginWindow.cpp" line="96"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="46"/>
        <source>Incorrect password</source>
        <translation>Неправильный пароль</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="55"/>
        <source>Enter</source>
        <translation>Вход</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="60"/>
        <location filename="../src/loginWindow.cpp" line="120"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="72"/>
        <source>Users</source>
        <translation>Пользователи</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="104"/>
        <source>Repeat</source>
        <translation>Повтор</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="116"/>
        <source>Confirm</source>
        <translation>Принять</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="124"/>
        <source>Delete user</source>
        <translation>Удалить пользователя</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="136"/>
        <source>Change user data</source>
        <translation>Изменить данные пользователя</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="278"/>
        <location filename="../src/loginWindow.cpp" line="419"/>
        <location filename="../src/loginWindow.cpp" line="431"/>
        <location filename="../src/loginWindow.cpp" line="443"/>
        <location filename="../src/loginWindow.cpp" line="451"/>
        <source>Prom-Soft</source>
        <translation></translation>
    </message>
    <message>
        <source>Windows styles missing</source>
        <translation type="obsolete">Стили окон не загружены</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="235"/>
        <source>without spaces!</source>
        <translation>без пробелов!</translation>
    </message>
    <message>
        <source>the password should be without spaces</source>
        <translation type="obsolete">пароль не должен содержать пробелы</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="239"/>
        <source>re-enter password incorrect</source>
        <translation>пароли не совпадают</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="263"/>
        <source>User password changed: </source>
        <translation>Изменен пароль пользователя: </translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="274"/>
        <source>Cannot create a second administrator account!</source>
        <translation>Невозможно создать второго администратора!</translation>
    </message>
    <message>
        <source>Weighbridge</source>
        <translation type="obsolete">Weighbridge</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="294"/>
        <source>Add user: </source>
        <translation>Добавлен пользователь: </translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="362"/>
        <source>Such a name is already exist</source>
        <translation>Такое имя уже существует</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="415"/>
        <source>Do you want to delete record?</source>
        <translation>Удалить запись?</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="417"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="418"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="427"/>
        <source>You cannot delete Admin user!</source>
        <translation>Невозможно удалить администратора!</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="439"/>
        <source>Delete error.</source>
        <translation>Ошибка при удалении.</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="448"/>
        <source>Deleted</source>
        <translation>Удалено</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="457"/>
        <source>User deleted: </source>
        <translation>Пользователь удален: </translation>
    </message>
</context>
<context>
    <name>mysqlProccess</name>
    <message>
        <source>Weighbridge</source>
        <translation type="obsolete">Weighbridge</translation>
    </message>
    <message>
        <location filename="../src/DB_Driver.cpp" line="157"/>
        <source>Error connect with Remote Data Base</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DB_Driver.cpp" line="161"/>
        <source>AV-CONTROL PLUS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DB_Driver.cpp" line="590"/>
        <source>Photos</source>
        <translation>Фото</translation>
    </message>
</context>
<context>
    <name>names::Form_ttn</name>
    <message>
        <source>Windows styles missing</source>
        <translation type="obsolete">Стили окон не загружены</translation>
    </message>
    <message>
        <source>Weighbridge</source>
        <translation type="obsolete">АРМ - ВИС-Сервис</translation>
    </message>
</context>
<context>
    <name>names::MyClass</name>
    <message>
        <source>Weight</source>
        <translation type="obsolete">Вес</translation>
    </message>
    <message>
        <source>Report -</source>
        <translation type="obsolete">Отчет -</translation>
    </message>
    <message>
        <source>Login in: </source>
        <translation type="obsolete">Вход пользователя:</translation>
    </message>
    <message>
        <source>The program has not been activated
or the temporary key has expired!
Please contact VIS-Service.</source>
        <translation type="obsolete">Программа не активирована
либо истёк срок действия ключа!
Пожалуйста, обратитесь в ВИС-Сервис.</translation>
    </message>
    <message>
        <source>Weighbridge</source>
        <translation type="obsolete">АРМ - ВИС-Сервис</translation>
    </message>
    <message>
        <source>Incorrect password</source>
        <translation type="obsolete">Неправильный пароль</translation>
    </message>
    <message>
        <source>Windows styles missing</source>
        <translation type="obsolete">Стили окон не загружены</translation>
    </message>
    <message>
        <source>The code is wrong!
Please contact VIS-Service.</source>
        <translation type="obsolete">Код неправильный!
Обратитесь в ВИС-Сервис.</translation>
    </message>
    <message>
        <source>Enter the activation code for programm.</source>
        <translation type="obsolete">Введите код активации ПО.</translation>
    </message>
    <message>
        <source>Enter the service code.</source>
        <translation type="obsolete">Введите сервисный код.</translation>
    </message>
    <message>
        <source>Click to visit our website.</source>
        <translation type="obsolete">Кликните для перехода на наш сайт.</translation>
    </message>
    <message>
        <source>Service</source>
        <translation type="obsolete">Служебные</translation>
    </message>
    <message>
        <source>Dictionary</source>
        <translation type="obsolete">Справочники</translation>
    </message>
    <message>
        <source>Change user</source>
        <translation type="obsolete">Смена пользователя</translation>
    </message>
    <message>
        <source>Add user</source>
        <translation type="obsolete">Добавить пользователя</translation>
    </message>
    <message>
        <source>Event log</source>
        <translation type="obsolete">Журнал событий</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="obsolete">Настройки</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Помощь</translation>
    </message>
    <message>
        <source>User manual - F1</source>
        <translation type="obsolete">Руководство пользователя F1</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="obsolete">О программе</translation>
    </message>
    <message>
        <source>Activation</source>
        <translation type="obsolete">Активация ПО</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="obsolete">Язык</translation>
    </message>
    <message>
        <source>Search vehicles by number</source>
        <translation type="obsolete">Поиск авто по номеру</translation>
    </message>
    <message>
        <source>Vehicles ¹</source>
        <translation type="obsolete">Авто №</translation>
    </message>
    <message>
        <source>Enter the vehicles number</source>
        <translation type="obsolete">Введите номер авто</translation>
    </message>
    <message>
        <source>Num fragment</source>
        <translation type="obsolete">Фрагм. номера</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="obsolete">Поиск</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="obsolete">Сброс</translation>
    </message>
    <message>
        <source>Print</source>
        <translation type="obsolete">Печать</translation>
    </message>
    <message>
        <source>Cargo accept</source>
        <translation type="obsolete">Приемка груза</translation>
    </message>
    <message>
        <source>Cargo send</source>
        <translation type="obsolete">Отправка</translation>
    </message>
    <message>
        <source>Press to accept the vehicle.</source>
        <translation type="obsolete">Нажмите для приёмки авто.</translation>
    </message>
    <message>
        <source>Press to send the vehicle</source>
        <translation type="obsolete">Нажмите для отправки авто</translation>
    </message>
    <message>
        <source>Registry</source>
        <translation type="obsolete">Журнал</translation>
    </message>
    <message>
        <source>To view the history of weighing.</source>
        <translation type="obsolete">Посмотреть журнал взвешиваний.</translation>
    </message>
    <message>
        <source>Delete
record</source>
        <translation type="obsolete">Удалить\nзапись</translation>
    </message>
    <message>
        <source>Deletes the selected entry in the table.</source>
        <translation type="obsolete">Удалить взвешивание из таблицы.</translation>
    </message>
    <message>
        <source>To enter the settings menu.</source>
        <translation type="obsolete">Вход в настройки.</translation>
    </message>
    <message>
        <source>To edit dictionaries.</source>
        <translation type="obsolete">Редактировать справочники.</translation>
    </message>
    <message>
        <source>Zero &gt;0&lt;</source>
        <translation type="obsolete">Обнуление &gt;0&lt;</translation>
    </message>
    <message>
        <source>WEIGHT</source>
        <translation type="obsolete">МАССА</translation>
    </message>
    <message>
        <source>
kg</source>
        <translation type="obsolete">
кг</translation>
    </message>
    <message>
        <source>CAM 1</source>
        <translation type="obsolete">Камера 1</translation>
    </message>
    <message>
        <source>View weighing photo ¹1 cam ¹1</source>
        <translation type="obsolete">Просмотр фото 1 камеры 1</translation>
    </message>
    <message>
        <source>CAM 2</source>
        <translation type="obsolete">Камера 2</translation>
    </message>
    <message>
        <source>View weighing photo ¹2 cam ¹1</source>
        <translation type="obsolete">Просмотр фото 2 камеры 1</translation>
    </message>
    <message>
        <source>CAM 3</source>
        <translation type="obsolete">Камера 3</translation>
    </message>
    <message>
        <source>View weighing photo ¹1 cam ¹2</source>
        <translation type="obsolete">Просмотр фото 1 камеры 2</translation>
    </message>
    <message>
        <source>CAM 4</source>
        <translation type="obsolete">Камера 4</translation>
    </message>
    <message>
        <source>View weighing photo ¹2 cam ¹2</source>
        <translation type="obsolete">Просмотр фото 2 камеры 2</translation>
    </message>
    <message>
        <source>View weighing photo ¹1 cam ¹3</source>
        <translation type="obsolete">Просмотр фото 1 камеры 3</translation>
    </message>
    <message>
        <source>View weighing photo ¹1 cam ¹4</source>
        <translation type="obsolete">Просмотр фото 1 камеры 4</translation>
    </message>
    <message>
        <source>View weighing photo ¹2 cam ¹3</source>
        <translation type="obsolete">Просмотр фото 2 камеры 3</translation>
    </message>
    <message>
        <source>View weighing photo ¹2 cam ¹4</source>
        <translation type="obsolete">Просмотр фото 2 камеры 4</translation>
    </message>
    <message>
        <source>Click on the traffic light to change its state.</source>
        <translation type="obsolete">Кликните по светофору для изменения его состояния.</translation>
    </message>
    <message>
        <source>Select the entry to generate the report!</source>
        <translation type="obsolete">Сначала выберите строку для формирования отчета!</translation>
    </message>
    <message>
        <source>Select record to start!</source>
        <translation type="obsolete">Сначала выберите запись!</translation>
    </message>
    <message>
        <source>Data not available</source>
        <translation type="obsolete">Данные отсутствуют</translation>
    </message>
    <message>
        <source>Microsoft Office - Excel is not installed!</source>
        <translation type="obsolete">Microsoft Office - Excel не установлен!</translation>
    </message>
    <message>
        <source>From scales</source>
        <translation type="obsolete">С весопроцессора</translation>
    </message>
    <message>
        <source>Manually</source>
        <translation type="obsolete">Вручную</translation>
    </message>
    <message>
        <source>Select the entry to delete!</source>
        <translation type="obsolete">Выберите запись для удаления!</translation>
    </message>
    <message>
        <source>Do you want to remove the entry?</source>
        <translation type="obsolete">Действительно хотите удалить запись?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Да</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
    <message>
        <source>Removed the record. Trailer num:</source>
        <translation type="obsolete">Запись удалена. Номер прицепа:</translation>
    </message>
    <message>
        <source>Removed the record. Vehicle num:</source>
        <translation type="obsolete">Запись удалена. Номер авто:</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Закрыть</translation>
    </message>
    <message>
        <source>Select the entry first!</source>
        <translation type="obsolete">Сначала выберите запись!</translation>
    </message>
    <message>
        <source>Communication with scales missing!</source>
        <translation type="obsolete">Связь с весами отсутствует!</translation>
    </message>
    <message>
        <source>Trailer accepted:</source>
        <translation type="obsolete">Прицеп принят:</translation>
    </message>
    <message>
        <source>Vehicle accepted:</source>
        <translation type="obsolete">Авто принято:</translation>
    </message>
    <message>
        <source>Trailer send:</source>
        <translation type="obsolete">Прицеп отправлен:</translation>
    </message>
    <message>
        <source>Vehicle send:</source>
        <translation type="obsolete">Авто отправлено:</translation>
    </message>
    <message>
        <source>Error. There are no photos to save!</source>
        <translation type="obsolete">Ошибка. Нет фото для сохранения!</translation>
    </message>
    <message>
        <source>Data according to the filters was not found</source>
        <translation type="obsolete">Данные по заданным фильтрам отсутствуют</translation>
    </message>
    <message>
        <source>Photos</source>
        <translation type="obsolete">Фото</translation>
    </message>
    <message>
        <source>Entered the settings</source>
        <translation type="obsolete">Вход в настройки</translation>
    </message>
    <message>
        <source>Entered the registry:</source>
        <translation type="obsolete">Вход в журнал:</translation>
    </message>
    <message>
        <source>Closed the registry:</source>
        <translation type="obsolete">Закрыт журнал:</translation>
    </message>
    <message>
        <source>Closed the settings:</source>
        <translation type="obsolete">Закрыты настройки:</translation>
    </message>
    <message>
        <source>by Admin</source>
        <translation type="obsolete">под Администратором</translation>
    </message>
    <message>
        <source>Do you want to weigh the vehicle?</source>
        <translation type="obsolete">Всё равно взвесить авто?</translation>
    </message>
    <message>
        <source>Zero error</source>
        <translation type="obsolete">Ошибка обнуления</translation>
    </message>
    <message>
        <source>COM-port closed</source>
        <translation type="obsolete">COM-порт закрыт</translation>
    </message>
    <message>
        <source>Set to zero: </source>
        <translation type="obsolete">Обнуление:</translation>
    </message>
    <message>
        <source>Entered dictionaries: </source>
        <translation type="obsolete">Вход в справочники: </translation>
    </message>
    <message>
        <source>Dictionaries closed: </source>
        <translation type="obsolete">Справочники закрыты: </translation>
    </message>
    <message>
        <source>Enter the name</source>
        <translation type="obsolete">Ввод наименования</translation>
    </message>
    <message>
        <source>Database error!</source>
        <translation type="obsolete">Ошибка БД!</translation>
    </message>
    <message>
        <source>The name is already exist!</source>
        <translation type="obsolete">Такое наименование уже есть!</translation>
    </message>
    <message>
        <source>The record is successfully saved.</source>
        <translation type="obsolete">Запись успешно сохранена.</translation>
    </message>
    <message>
        <source>Added to dictionary: </source>
        <translation type="obsolete">Добавлено в справочник: </translation>
    </message>
    <message>
        <source>The entry was successfully deleted from the database!</source>
        <translation type="obsolete">Запись успешно удалена из БД!</translation>
    </message>
    <message>
        <source>Deleted from dictionaries: </source>
        <translation type="obsolete">Удалено из справочника: </translation>
    </message>
    <message>
        <source>Weight data from  </source>
        <translation type="obsolete">Данные взвешиваний от </translation>
    </message>
    <message>
        <source>Total gross: </source>
        <translation type="obsolete">Сумма брутто: </translation>
    </message>
    <message>
        <source>Total tare: </source>
        <translation type="obsolete">Сумма тара: </translation>
    </message>
    <message>
        <source>Total net: </source>
        <translation type="obsolete">Сумма нетто: </translation>
    </message>
    <message>
        <source>Responsible: </source>
        <translation type="obsolete">Ответственный: </translation>
    </message>
    <message>
        <source>Date/time </source>
        <translation type="obsolete">Дата/время </translation>
    </message>
    <message>
        <source>Sender</source>
        <translation type="obsolete">Отправитель</translation>
    </message>
    <message>
        <source>Recipient</source>
        <translation type="obsolete">Получатель</translation>
    </message>
    <message>
        <source>Payer</source>
        <translation type="obsolete">Плательщик</translation>
    </message>
    <message>
        <source>Carrier</source>
        <translation type="obsolete">Перевозчик</translation>
    </message>
    <message>
        <source>Cargo</source>
        <translation type="obsolete">Груз</translation>
    </message>
    <message>
        <source>Cargo value</source>
        <translation type="obsolete">Цена груза</translation>
    </message>
    <message>
        <source>Gross</source>
        <translation type="obsolete">Брутто</translation>
    </message>
    <message>
        <source>Tare</source>
        <translation type="obsolete">Тара</translation>
    </message>
    <message>
        <source>Net</source>
        <translation type="obsolete">Нетто</translation>
    </message>
    <message>
        <source>Date
time</source>
        <translation type="obsolete">Дата
время</translation>
    </message>
    <message>
        <source>Waybill</source>
        <translation type="obsolete">Накл.</translation>
    </message>
    <message>
        <source>User</source>
        <translation type="obsolete">Принял</translation>
    </message>
    <message>
        <source>Entries by specified criteria was not found!</source>
        <translation type="obsolete">Записи по заданному критерию не найдены!</translation>
    </message>
    <message>
        <source>Cam is turn off</source>
        <translation type="obsolete">Камера выключена</translation>
    </message>
    <message>
        <source>There is no data to print!</source>
        <translation type="obsolete">Нет данных для печати!</translation>
    </message>
    <message>
        <source>vehicles accepted</source>
        <translation type="obsolete">приёмке авто</translation>
    </message>
    <message>
        <source>vehicles sended</source>
        <translation type="obsolete">отправке авто</translation>
    </message>
    <message>
        <source>Weight data - </source>
        <translation type="obsolete">Данные взвешиваний - </translation>
    </message>
    <message>
        <source>Date time </source>
        <translation type="obsolete">Дата время </translation>
    </message>
    <message>
        <source>Start server</source>
        <translation type="obsolete">Старт сервера</translation>
    </message>
    <message>
        <source>Stop server</source>
        <translation type="obsolete">Стоп сервера</translation>
    </message>
    <message>
        <source>User: </source>
        <translation type="obsolete">Пользователь: </translation>
    </message>
    <message>
        <source>Scales link status </source>
        <translation type="obsolete">Связь с весами </translation>
    </message>
    <message>
        <source>Connected</source>
        <translation type="obsolete">Подключено</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="obsolete">Отключено</translation>
    </message>
    <message>
        <source>Do you want to exit the program?</source>
        <translation type="obsolete">Выйти из программы?</translation>
    </message>
    <message>
        <source>Exit program</source>
        <translation type="obsolete">Выход из ПО</translation>
    </message>
    <message>
        <source>Attempt to send e-mail successfully </source>
        <translation type="obsolete">Успешная попытка отсылки на почту </translation>
    </message>
    <message>
        <source>Sended to </source>
        <translation type="obsolete">Отправлено на </translation>
    </message>
    <message>
        <source>Attempt to send e-mail unsuccessful </source>
        <translation type="obsolete">Неуспешная попытка отправки на почту </translation>
    </message>
    <message>
        <source>The letter is not sent to destination. Check your mail settings and internet connection.</source>
        <translation type="obsolete">Письмо не отправлено. Проверьте настройки почты и интернет соединение.</translation>
    </message>
    <message>
        <source>The weight on the weighing platform is less than the minimum </source>
        <translation type="obsolete">Вес на платформе меньше минимального </translation>
    </message>
    <message>
        <source>The vehicle is not correctly standing on the weighing platform!</source>
        <translation type="obsolete">Авто некорректно расположено на весовой платформе!</translation>
    </message>
    <message>
        <source>Positioning</source>
        <translation type="obsolete">Позиционирование</translation>
    </message>
</context>
<context>
    <name>names::SettingsForm</name>
    <message>
        <source>Settings</source>
        <translation type="obsolete">Настройки</translation>
    </message>
    <message>
        <source>COM-port settings</source>
        <translation type="obsolete">Настройки COM порта</translation>
    </message>
    <message>
        <source>Weighing proc. model</source>
        <translation type="obsolete">Модель весопроцессора</translation>
    </message>
    <message>
        <source>Adress WP</source>
        <translation type="obsolete">Адрес ВП</translation>
    </message>
    <message>
        <source>Koef.</source>
        <translation type="obsolete">Коеф.</translation>
    </message>
    <message>
        <source>Baud rate</source>
        <translation type="obsolete">Скорость</translation>
    </message>
    <message>
        <source>End of frame</source>
        <translation type="obsolete">Конец посылки</translation>
    </message>
    <message>
        <source>Radio modem</source>
        <translation type="obsolete">Радиомодем</translation>
    </message>
    <message>
        <source>Use USB</source>
        <translation type="obsolete">Использовать USB</translation>
    </message>
    <message>
        <source>Switch ON</source>
        <translation type="obsolete">Включить в работу</translation>
    </message>
    <message>
        <source>Show traffic light</source>
        <translation type="obsolete">Отобразить светофор</translation>
    </message>
    <message>
        <source>Use BC-01</source>
        <translation type="obsolete">Использ. БК-01</translation>
    </message>
    <message>
        <source>WP Model via BC-01</source>
        <translation type="obsolete">Модель ВП для БК-01</translation>
    </message>
    <message>
        <source>Input delay, ms</source>
        <translation type="obsolete">Задержка входов</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Сохранить</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Закрыть</translation>
    </message>
    <message>
        <source>UDP params</source>
        <translation type="obsolete">Параметры UDP</translation>
    </message>
    <message>
        <source>IP-adress</source>
        <translation type="obsolete">IP адрес</translation>
    </message>
    <message>
        <source>Src. port</source>
        <translation type="obsolete">Порт получатель</translation>
    </message>
    <message>
        <source>Dest. port</source>
        <translation type="obsolete">Порт отправитель</translation>
    </message>
    <message>
        <source>IP cams</source>
        <translation type="obsolete">Видеокамеры</translation>
    </message>
    <message>
        <source>IP cam settings</source>
        <translation type="obsolete">Настройки IP камер</translation>
    </message>
    <message>
        <source>Port</source>
        <translation type="obsolete">Порт</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation type="obsolete">Задействовать</translation>
    </message>
    <message>
        <source>Photo enable</source>
        <translation type="obsolete">Фотофиксация</translation>
    </message>
    <message>
        <source>Photo preview enable</source>
        <translation type="obsolete">Отображение фото</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">Пароль</translation>
    </message>
    <message>
        <source>Server autostart</source>
        <translation type="obsolete">Автостарт сервера</translation>
    </message>
    <message>
        <source>Email settings</source>
        <translation type="obsolete">Настройки почты</translation>
    </message>
    <message>
        <source>Send interval, min</source>
        <translation type="obsolete">Интревал отсылки, мин</translation>
    </message>
    <message>
        <source>Addressee</source>
        <translation type="obsolete">Адресат</translation>
    </message>
    <message>
        <source>Use SSL</source>
        <translation type="obsolete">Использовать SSL</translation>
    </message>
    <message>
        <source>Internet connection</source>
        <translation type="obsolete">Интернет соединение</translation>
    </message>
    <message>
        <source>Scale</source>
        <translation type="obsolete">Весы</translation>
    </message>
    <message>
        <source>Checking</source>
        <translation type="obsolete">Проверка</translation>
    </message>
    <message>
        <source>Internet connection is available.</source>
        <translation type="obsolete">Интернет соединение доступно.</translation>
    </message>
    <message>
        <source>Internet connection is not available.</source>
        <translation type="obsolete">Интернет соединение отсутствует.</translation>
    </message>
</context>
<context>
    <name>names::reportFormClass</name>
    <message>
        <source>Registry</source>
        <translation type="obsolete">Журнал</translation>
    </message>
    <message>
        <source>Filter params</source>
        <translation type="obsolete">Параметры фильтра</translation>
    </message>
    <message>
        <source>Vehicle num</source>
        <translation type="obsolete">Номер авто</translation>
    </message>
    <message>
        <source>Confirm
 filter</source>
        <translation type="obsolete">Применить\nфильтр</translation>
    </message>
    <message>
        <source>Reset
filter</source>
        <translation type="obsolete">Сброс\nфильтра</translation>
    </message>
    <message>
        <source>Accepting</source>
        <translation type="obsolete">Приём</translation>
    </message>
    <message>
        <source>Sending</source>
        <translation type="obsolete">Отправка</translation>
    </message>
    <message>
        <source>Date (start)</source>
        <translation type="obsolete">Дата (начало)</translation>
    </message>
    <message>
        <source>Date (end)</source>
        <translation type="obsolete">Дата (конец)</translation>
    </message>
    <message>
        <source>Time (start)</source>
        <translation type="obsolete">Время (начало)</translation>
    </message>
    <message>
        <source>Time (end)</source>
        <translation type="obsolete">Время (конец)</translation>
    </message>
    <message>
        <source>Sender</source>
        <translation type="obsolete">Отправитель</translation>
    </message>
    <message>
        <source>Recipient</source>
        <translation type="obsolete">Получатель</translation>
    </message>
    <message>
        <source>Carrier</source>
        <translation type="obsolete">Перевозчик</translation>
    </message>
    <message>
        <source>Scales num</source>
        <translation type="obsolete">Номер весов</translation>
    </message>
    <message>
        <source>Cargo</source>
        <translation type="obsolete">Груз</translation>
    </message>
    <message>
        <source>Payer</source>
        <translation type="obsolete">Плательщик</translation>
    </message>
    <message>
        <source>Create &quot;ÒÒÍ&quot;</source>
        <translation type="obsolete">Создать ТТН</translation>
    </message>
    <message>
        <source>Create
 waybill</source>
        <translation type="obsolete">Создать\nнакладную</translation>
    </message>
    <message>
        <source>Export to
Excel</source>
        <translation type="obsolete">Экспорт\nв Excel</translation>
    </message>
    <message>
        <source>Print
 table</source>
        <translation type="obsolete">Печать\nтаблицы</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Закрыть</translation>
    </message>
    <message>
        <source>Send
e-mail</source>
        <translation type="obsolete">Отправить\ne-mail</translation>
    </message>
    <message>
        <source>The number of prints on the A4</source>
        <translation type="obsolete">Количество накланых на листе А4</translation>
    </message>
    <message>
        <source>Set number</source>
        <translation type="obsolete">Выбор</translation>
    </message>
    <message>
        <source>Print</source>
        <translation type="obsolete">Печать</translation>
    </message>
    <message>
        <source>Cargo value</source>
        <translation type="obsolete">Цена груза</translation>
    </message>
    <message>
        <source>Gross</source>
        <translation type="obsolete">Брутто</translation>
    </message>
    <message>
        <source>Tare</source>
        <translation type="obsolete">Тара</translation>
    </message>
    <message>
        <source>Net</source>
        <translation type="obsolete">Нетто</translation>
    </message>
    <message>
        <source>Datetime
accept</source>
        <translation type="obsolete">Время\nприема</translation>
    </message>
    <message>
        <source>Accept
user</source>
        <translation type="obsolete">Принял</translation>
    </message>
    <message>
        <source>Sender
user</source>
        <translation type="obsolete">Отправил</translation>
    </message>
    <message>
        <source>Waybill</source>
        <translation type="obsolete">Накл.</translation>
    </message>
    <message>
        <source>Photo</source>
        <translation type="obsolete">Фото</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Сохранить</translation>
    </message>
    <message>
        <source>Vehicle number</source>
        <translation type="obsolete">Номер авто</translation>
    </message>
    <message>
        <source>Shipment details</source>
        <translation type="obsolete">Детали отправки</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Наименование</translation>
    </message>
    <message>
        <source>unit</source>
        <translation type="obsolete">ед.изм.</translation>
    </message>
    <message>
        <source>Count</source>
        <translation type="obsolete">Кол-во</translation>
    </message>
    <message>
        <source>Price, uah</source>
        <translation type="obsolete">Цена, грн</translation>
    </message>
    <message>
        <source>Sum, uah</source>
        <translation type="obsolete">Сумма, грн</translation>
    </message>
    <message>
        <source>Sum +VAT, uah</source>
        <translation type="obsolete">Сумма с НДС, грн</translation>
    </message>
    <message>
        <source>tons</source>
        <translation type="obsolete">тонны</translation>
    </message>
    <message>
        <source>VAT</source>
        <translation type="obsolete">НДС</translation>
    </message>
    <message>
        <source>Accept user</source>
        <translation type="obsolete">Принял:</translation>
    </message>
    <message>
        <source>Sender user</source>
        <translation type="obsolete">Отправил</translation>
    </message>
    <message>
        <source>Report</source>
        <translation type="obsolete">Отчет</translation>
    </message>
    <message>
        <source>No data to e-mail</source>
        <translation type="obsolete">Нет данных для отправки по почте</translation>
    </message>
    <message>
        <source>Weight mail</source>
        <translation type="obsolete">С весовой</translation>
    </message>
    <message>
        <source>Report - </source>
        <translation type="obsolete">Отчет за </translation>
    </message>
</context>
<context>
    <name>optionalDevices</name>
    <message>
        <location filename="../forms/optionaldevices.ui" line="26"/>
        <source>Socket-2</source>
        <translation>Управление реле</translation>
    </message>
    <message>
        <location filename="../forms/optionaldevices.ui" line="38"/>
        <source>Socket2 #1</source>
        <translation>Socket2 #1</translation>
    </message>
    <message>
        <location filename="../forms/optionaldevices.ui" line="50"/>
        <location filename="../forms/optionaldevices.ui" line="170"/>
        <location filename="../forms/optionaldevices.ui" line="290"/>
        <source>Relay 0</source>
        <translation>Реле 0</translation>
    </message>
    <message>
        <location filename="../forms/optionaldevices.ui" line="62"/>
        <location filename="../forms/optionaldevices.ui" line="114"/>
        <location filename="../forms/optionaldevices.ui" line="182"/>
        <location filename="../forms/optionaldevices.ui" line="234"/>
        <location filename="../forms/optionaldevices.ui" line="302"/>
        <location filename="../forms/optionaldevices.ui" line="354"/>
        <source>ON</source>
        <translation>Вкл.</translation>
    </message>
    <message>
        <location filename="../forms/optionaldevices.ui" line="75"/>
        <location filename="../forms/optionaldevices.ui" line="101"/>
        <location filename="../forms/optionaldevices.ui" line="195"/>
        <location filename="../forms/optionaldevices.ui" line="221"/>
        <location filename="../forms/optionaldevices.ui" line="315"/>
        <location filename="../forms/optionaldevices.ui" line="341"/>
        <source>OFF</source>
        <translation>Выкл.</translation>
    </message>
    <message>
        <location filename="../forms/optionaldevices.ui" line="89"/>
        <location filename="../forms/optionaldevices.ui" line="209"/>
        <location filename="../forms/optionaldevices.ui" line="329"/>
        <source>Relay 1</source>
        <translation>Реле 1</translation>
    </message>
    <message>
        <location filename="../forms/optionaldevices.ui" line="141"/>
        <location filename="../forms/optionaldevices.ui" line="261"/>
        <location filename="../forms/optionaldevices.ui" line="381"/>
        <source>Link:  </source>
        <translation>Связь: </translation>
    </message>
    <message>
        <location filename="../forms/optionaldevices.ui" line="158"/>
        <source>Socket2 #2</source>
        <translation>Socket2 #2</translation>
    </message>
    <message>
        <location filename="../forms/optionaldevices.ui" line="278"/>
        <source>Socket2 #3</source>
        <translation>Socket2 #3</translation>
    </message>
    <message>
        <location filename="../src/optionaldevices.cpp" line="99"/>
        <location filename="../src/optionaldevices.cpp" line="111"/>
        <location filename="../src/optionaldevices.cpp" line="123"/>
        <source>Connected</source>
        <translation>Подключено</translation>
    </message>
    <message>
        <location filename="../src/optionaldevices.cpp" line="104"/>
        <location filename="../src/optionaldevices.cpp" line="116"/>
        <location filename="../src/optionaldevices.cpp" line="128"/>
        <source>Not connected</source>
        <translation>Отключено</translation>
    </message>
</context>
<context>
    <name>options</name>
    <message>
        <location filename="../forms/options.ui" line="38"/>
        <source>Photographic fixation module</source>
        <translation>Модуль фотофиксации - IP камеры</translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="50"/>
        <location filename="../forms/options.ui" line="131"/>
        <location filename="../forms/options.ui" line="175"/>
        <location filename="../forms/options.ui" line="219"/>
        <location filename="../forms/options.ui" line="263"/>
        <location filename="../forms/options.ui" line="307"/>
        <location filename="../forms/options.ui" line="351"/>
        <location filename="../forms/options.ui" line="429"/>
        <location filename="../forms/options.ui" line="491"/>
        <source>Activate</source>
        <translation>Активировать</translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="68"/>
        <location filename="../forms/options.ui" line="149"/>
        <location filename="../forms/options.ui" line="193"/>
        <location filename="../forms/options.ui" line="237"/>
        <location filename="../forms/options.ui" line="281"/>
        <location filename="../forms/options.ui" line="325"/>
        <location filename="../forms/options.ui" line="369"/>
        <location filename="../forms/options.ui" line="447"/>
        <location filename="../forms/options.ui" line="478"/>
        <source>enter key</source>
        <translation>введите ключ</translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="82"/>
        <source>Your number</source>
        <translation>Ваш номер</translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="102"/>
        <source>____________</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="119"/>
        <source>Waste definition module</source>
        <translation>Модуль задания сорности</translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="163"/>
        <source>Control module - traffic lights</source>
        <translation>Модуль контроля - светофоры</translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="207"/>
        <source>Duplicative scoreboards module</source>
        <translation>Модуль табло дублирующие</translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="251"/>
        <source>Control module - barriers</source>
        <translation>Модуль контроля - шлагбаумы</translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="295"/>
        <source>Control module - positioning sensors</source>
        <translation>Модуль контроля - датчики позиционирования</translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="339"/>
        <source>Program activation</source>
        <translation>Активация программы</translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="386"/>
        <source>Program fist run :</source>
        <translation>Программа запущенна :</translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="417"/>
        <source>Rfid module</source>
        <translation>Модуль  Rfid карт</translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="461"/>
        <source>Telegramm-Bot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="26"/>
        <source>Software modules activation</source>
        <translation>Подключение модулей ПО</translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="404"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>registration_rfid</name>
    <message utf8="true">
        <location filename="../forms/registration_rfid.ui" line="14"/>
        <source>Модуль RFID</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/registration_rfid.ui" line="37"/>
        <source>Регистрационные данные</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/registration_rfid.ui" line="43"/>
        <source>Код карты</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/registration_rfid.ui" line="97"/>
        <source>Добавить</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/registration_rfid.ui" line="116"/>
        <source>Изменить</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/registration_rfid.ui" line="135"/>
        <source>Удалить</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/registration_rfid.ui" line="154"/>
        <source>Закрыть</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/registration_rfid.cpp" line="556"/>
        <source>Data for today is not available.</source>
        <translation type="unfinished">Данных на сегодня не имеется.</translation>
    </message>
    <message>
        <location filename="../src/registration_rfid.cpp" line="560"/>
        <source>Prom-Soft</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>reportFormClass</name>
    <message>
        <location filename="../src/reportFormClass.cpp" line="46"/>
        <source>Registry</source>
        <translation>Журнал</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="51"/>
        <source>Filter params</source>
        <translation>Параметры фильтра</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="53"/>
        <source>Vehicle num</source>
        <translation>Номер авто</translation>
    </message>
    <message>
        <source>Confirm
 filter</source>
        <translation type="obsolete">Применить
фильтр</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="69"/>
        <source>Reset
filter</source>
        <translation>Сброс
фильтра</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="76"/>
        <source>Accepting</source>
        <translation>Приём</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="80"/>
        <source>Sending</source>
        <translation>Отправка</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="83"/>
        <source>Date (start)</source>
        <translation>Дата (начало)</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="112"/>
        <source>Date (end)</source>
        <translation>Дата (конец)</translation>
    </message>
    <message>
        <source>Time (start)</source>
        <translation type="obsolete">Время (начало)</translation>
    </message>
    <message>
        <source>Time (end)</source>
        <translation type="obsolete">Время (конец)</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="133"/>
        <source>Time(start)</source>
        <translation>Время(старт)</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="140"/>
        <source>Time(end)</source>
        <translation>Время(конец)</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="147"/>
        <location filename="../src/reportFormClass.cpp" line="611"/>
        <location filename="../src/reportFormClass.cpp" line="715"/>
        <source>Sender</source>
        <translation>Отправитель</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="155"/>
        <location filename="../src/reportFormClass.cpp" line="612"/>
        <location filename="../src/reportFormClass.cpp" line="724"/>
        <source>Recipient</source>
        <translation>Получатель</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="163"/>
        <location filename="../src/reportFormClass.cpp" line="614"/>
        <location filename="../src/reportFormClass.cpp" line="721"/>
        <source>Carrier</source>
        <translation>Перевозчик</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="171"/>
        <source>Scales num</source>
        <translation>Номер весов</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="178"/>
        <location filename="../src/reportFormClass.cpp" line="615"/>
        <source>Cargo</source>
        <translation>Груз</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="186"/>
        <location filename="../src/reportFormClass.cpp" line="613"/>
        <location filename="../src/reportFormClass.cpp" line="718"/>
        <source>Payer</source>
        <translation>Плательщик</translation>
    </message>
    <message>
        <source>Create &quot;ÒÒÍ&quot;</source>
        <translation type="obsolete">Создать ТТН</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="246"/>
        <source>Create TTN</source>
        <translation>Создать ТТН</translation>
    </message>
    <message>
        <source>Create
 waybill</source>
        <translation type="obsolete">Создать
накладную</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="265"/>
        <source>Export to
Excel</source>
        <translation>Экспорт
в Excel</translation>
    </message>
    <message>
        <source>Print
 table</source>
        <translation type="obsolete">Печать
таблицы</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="62"/>
        <source>Confirm
filter</source>
        <translation>Применить
фильтр</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="255"/>
        <source>Create
waybill</source>
        <translation>Создать
накладную</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="276"/>
        <source>Export to
Excel (+sor)</source>
        <translation>Экспорт в
Excel (+сорность)</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="284"/>
        <source>Print
table</source>
        <translation>Печать
таблицы</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="294"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="304"/>
        <source>Save all
photos</source>
        <translation>Отправлять фото</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="314"/>
        <source>Send
e-mail</source>
        <translation>Отправить
e-mail</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="320"/>
        <source>Save all photos</source>
        <translation>Отправлять фото</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="368"/>
        <source>The number of prints on the A4</source>
        <translation>Количество накланых на листе А4</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="373"/>
        <source>Set number</source>
        <translation>Выбор</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="384"/>
        <source>Print</source>
        <translation>Печать</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="610"/>
        <source>Vehicle
number</source>
        <translation>Номер
авто</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="616"/>
        <source>Cargo value</source>
        <translation>Цена груза</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="617"/>
        <source>Gross</source>
        <translation>Брутто</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="618"/>
        <source>Tare</source>
        <translation>Тара</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="619"/>
        <source>Net</source>
        <translation>Нетто</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="620"/>
        <source>Datetime
accept</source>
        <translation>Дата/время
приёмки</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="621"/>
        <source>Accept
user</source>
        <translation>Принял</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="622"/>
        <source>Datetime
send</source>
        <translation>Дата/время
отправки</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="623"/>
        <source>Sender
user</source>
        <translation>Отправил</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="624"/>
        <location filename="../src/reportFormClass.cpp" line="712"/>
        <source>Waybill</source>
        <translation>Накл.</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="625"/>
        <source>Trailer
number</source>
        <translation>Номер
прицепа</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="626"/>
        <source>Gross
(trailer)</source>
        <translation>Нетто
(прицеп)</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="627"/>
        <source>Tare
(trailer)</source>
        <translation>Тара
(прицеп)</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="628"/>
        <source>Net
(trailer)</source>
        <translation>Нетто
(прицеп)</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="629"/>
        <source>Photo</source>
        <translation>Фото</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="630"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="631"/>
        <source>Tara
enter</source>
        <translation>Ввод
тары</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="663"/>
        <source>Preview</source>
        <translation>Предварительный просмотр</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="727"/>
        <source>Vehicle number</source>
        <translation>Номер авто</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="730"/>
        <source>Shipment details</source>
        <translation>Детали отправки</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="733"/>
        <source>Name</source>
        <translation>Наименование</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="735"/>
        <source>Unit</source>
        <translation>Ед.изм.</translation>
    </message>
    <message>
        <source>unit</source>
        <translation type="obsolete">ед.изм.</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="737"/>
        <source>Count</source>
        <translation>Кол-во</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="739"/>
        <source>Price, uah</source>
        <translation>Цена, грн</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="741"/>
        <source>Sum, uah</source>
        <translation>Сумма, грн</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="743"/>
        <source>Sum +VAT, uah</source>
        <translation>Сумма с НДС, грн</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="746"/>
        <source>tons</source>
        <translation>тонны</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="752"/>
        <source>VAT</source>
        <translation>НДС</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="754"/>
        <source>Accept user</source>
        <translation>Принял:</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="756"/>
        <source>Sender user</source>
        <translation>Отправил:</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="776"/>
        <source>Report</source>
        <translation>Отчет</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="777"/>
        <source>No data to e-mail</source>
        <translation>Нет данных для отправки по почте</translation>
    </message>
    <message>
        <source>Weight mail</source>
        <translation type="obsolete">С весовой</translation>
    </message>
    <message>
        <source>Report - </source>
        <translation type="obsolete">Отчет за </translation>
    </message>
</context>
<context>
    <name>reports_rfid</name>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="35"/>
        <source>Журнал взвешиваний</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="64"/>
        <source>Фильтры </source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="93"/>
        <source>Дата заезда автомобиля 
       (конечная дата)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="162"/>
        <source>     Печать
  накладной</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="176"/>
        <source>Печать отчёта</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="189"/>
        <source>Экспорт в Excel</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="202"/>
        <source>Создать ТТН</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="256"/>
        <source>Время заезда автомобиля 
       (конечное время)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="303"/>
        <source>Применить фильтр</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="322"/>
        <source>Сбросить фильтр</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="360"/>
        <source>Дата заезда автомобиля 
       (начальная дата)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="426"/>
        <source>Поставщик</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="439"/>
        <source>Номер весов</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="484"/>
        <source>Материал</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="519"/>
        <source>Завершённые</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="524"/>
        <source>Незавершённые</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="538"/>
        <source>Номер автомобиля</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="580"/>
        <source>Ф.И.О. водителя</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="587"/>
        <source>Номер карты</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="594"/>
        <source>Завершённость взвешивания</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="615"/>
        <source>Время заезда автомобиля 
      (начальное время)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="106"/>
        <source>Datetime
accept</source>
        <translation>Дата/время
прибытия</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="107"/>
        <source>Datetime
send</source>
        <translation>Дата/время
убытия</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="109"/>
        <source>Weight 1</source>
        <translation>Взвешивание 1</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="110"/>
        <source>Weight 2</source>
        <translation>Взвешивание 2</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="111"/>
        <source>Net</source>
        <translation>Нетто</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="112"/>
        <source>Photo</source>
        <translation>Фото</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="113"/>
        <source>Save Photo</source>
        <translation>Сохранить
фото</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="114"/>
        <source>RFID Code</source>
        <translation>Код карты</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="115"/>
        <source>Supplier</source>
        <translation>Поставщик</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="116"/>
        <source>Material</source>
        <translation>Материал</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="117"/>
        <source>Vehicle
number</source>
        <translation>Номер
авто</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="118"/>
        <source>Trailer
number</source>
        <translation>Номер
прицепа</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="119"/>
        <source>Driver</source>
        <translation>Водитель</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="120"/>
        <source>Order number</source>
        <translation>Порядк. номер</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="121"/>
        <source>Type of container</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Blocked</source>
        <translation type="obsolete">Блокирована</translation>
    </message>
</context>
<context>
    <name>spravochnik_tara</name>
    <message>
        <location filename="../forms/spravochnik_tara.ui" line="26"/>
        <source>Tara directory</source>
        <translation>Справочник тары</translation>
    </message>
    <message>
        <location filename="../forms/spravochnik_tara.ui" line="53"/>
        <location filename="../src/spravochnik_tara.cpp" line="34"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="../forms/spravochnik_tara.ui" line="69"/>
        <location filename="../src/spravochnik_tara.cpp" line="117"/>
        <source>Modify</source>
        <translation>Редактировать</translation>
    </message>
    <message>
        <location filename="../forms/spravochnik_tara.ui" line="85"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../forms/spravochnik_tara.ui" line="101"/>
        <location filename="../src/spravochnik_tara.cpp" line="79"/>
        <location filename="../src/spravochnik_tara.cpp" line="172"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../include/spravochnik_tara.h" line="26"/>
        <source>Datetime</source>
        <translation>Время</translation>
    </message>
    <message>
        <location filename="../include/spravochnik_tara.h" line="27"/>
        <location filename="../src/spravochnik_tara.cpp" line="36"/>
        <location filename="../src/spravochnik_tara.cpp" line="123"/>
        <source>Vehicle number</source>
        <translation>Номер авто</translation>
    </message>
    <message>
        <location filename="../include/spravochnik_tara.h" line="28"/>
        <location filename="../src/spravochnik_tara.cpp" line="45"/>
        <location filename="../src/spravochnik_tara.cpp" line="135"/>
        <source>Brand</source>
        <translation>Марка</translation>
    </message>
    <message>
        <location filename="../include/spravochnik_tara.h" line="29"/>
        <location filename="../src/spravochnik_tara.cpp" line="56"/>
        <location filename="../src/spravochnik_tara.cpp" line="148"/>
        <source>Tara weight</source>
        <translation>Вес тара</translation>
    </message>
    <message>
        <location filename="../src/spravochnik_tara.cpp" line="71"/>
        <location filename="../src/spravochnik_tara.cpp" line="163"/>
        <source>Refresh</source>
        <translation>Обновить</translation>
    </message>
    <message>
        <location filename="../src/spravochnik_tara.cpp" line="75"/>
        <location filename="../src/spravochnik_tara.cpp" line="168"/>
        <source>Confirm</source>
        <translation>Принять</translation>
    </message>
    <message>
        <location filename="../src/spravochnik_tara.cpp" line="90"/>
        <location filename="../src/spravochnik_tara.cpp" line="100"/>
        <location filename="../src/spravochnik_tara.cpp" line="182"/>
        <location filename="../src/spravochnik_tara.cpp" line="192"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../src/spravochnik_tara.cpp" line="90"/>
        <location filename="../src/spravochnik_tara.cpp" line="182"/>
        <source>Not all fields are filled</source>
        <translation>Не все поля заполнены</translation>
    </message>
    <message>
        <location filename="../src/spravochnik_tara.cpp" line="96"/>
        <location filename="../src/spravochnik_tara.cpp" line="188"/>
        <source>Data entry</source>
        <translation>Ввод данных</translation>
    </message>
    <message>
        <location filename="../src/spravochnik_tara.cpp" line="96"/>
        <location filename="../src/spravochnik_tara.cpp" line="188"/>
        <source>Data is written to the directory!</source>
        <translation>Записано в справочник!</translation>
    </message>
    <message>
        <location filename="../src/spravochnik_tara.cpp" line="100"/>
        <location filename="../src/spravochnik_tara.cpp" line="192"/>
        <source>The car number already entered!</source>
        <translation>Такой номер уже есть!</translation>
    </message>
</context>
</TS>
