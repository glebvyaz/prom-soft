<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="uk_UA">
<context>
    <name>Form</name>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="17"/>
        <location filename="../src/Form_ttn.cpp" line="517"/>
        <source>Форма ТТН</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="29"/>
        <location filename="../src/Form_ttn.cpp" line="518"/>
        <source>ТОВАРНО-ТРАНСПОРТНА НАКЛАДНА</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="55"/>
        <location filename="../forms/ttn.ui" line="556"/>
        <location filename="../src/Form_ttn.cpp" line="520"/>
        <location filename="../src/Form_ttn.cpp" line="547"/>
        <source>№</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/ttn.ui" line="68"/>
        <location filename="../forms/ttn.ui" line="81"/>
        <location filename="../src/Form_ttn.cpp" line="521"/>
        <location filename="../src/Form_ttn.cpp" line="522"/>
        <source>&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/ttn.ui" line="114"/>
        <location filename="../forms/ttn.ui" line="622"/>
        <location filename="../src/Form_ttn.cpp" line="523"/>
        <location filename="../src/Form_ttn.cpp" line="549"/>
        <source>20</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="137"/>
        <location filename="../src/Form_ttn.cpp" line="524"/>
        <source>р.</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="150"/>
        <location filename="../src/Form_ttn.cpp" line="525"/>
        <source>Автомобіль</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="173"/>
        <location filename="../forms/ttn.ui" line="199"/>
        <location filename="../src/Form_ttn.cpp" line="526"/>
        <location filename="../src/Form_ttn.cpp" line="528"/>
        <source>(марка, модель, тип, реєстраційний номер)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="186"/>
        <location filename="../src/Form_ttn.cpp" line="527"/>
        <source>Причіп/напівпричіп</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="222"/>
        <location filename="../src/Form_ttn.cpp" line="529"/>
        <source>Вид перевезень</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="245"/>
        <source>Автомобільний перевiзник</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="268"/>
        <location filename="../src/Form_ttn.cpp" line="531"/>
        <source>Водій</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="291"/>
        <location filename="../forms/ttn.ui" line="340"/>
        <source>(найменування/П.І.Б.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="304"/>
        <source>(П.І.Б., номер посвідчення водія)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="317"/>
        <location filename="../src/Form_ttn.cpp" line="534"/>
        <source>Замовник </source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="353"/>
        <location filename="../src/Form_ttn.cpp" line="536"/>
        <source>Вантажовідправник</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="376"/>
        <location filename="../forms/ttn.ui" line="402"/>
        <location filename="../src/Form_ttn.cpp" line="537"/>
        <location filename="../src/Form_ttn.cpp" line="539"/>
        <location filename="../src/Form_ttn.cpp" line="545"/>
        <source>(повне найменування, місцезнаходження/П.І.Б., місце проживання)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="389"/>
        <location filename="../src/Form_ttn.cpp" line="538"/>
        <source>Вантажоодержувач</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="425"/>
        <location filename="../src/Form_ttn.cpp" line="540"/>
        <source>Пункт навантаження</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="448"/>
        <location filename="../forms/ttn.ui" line="474"/>
        <location filename="../src/Form_ttn.cpp" line="541"/>
        <location filename="../src/Form_ttn.cpp" line="543"/>
        <source>(місцезнаходження)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="461"/>
        <location filename="../src/Form_ttn.cpp" line="542"/>
        <source>Пункт розвантаження</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="497"/>
        <location filename="../src/Form_ttn.cpp" line="544"/>
        <source>Переадресування вантажу</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="520"/>
        <source>(найменування, місцезнаходження/П.І.Б., місце проживання нового вантажоодержувача; П.I.Б., посада та пiдпис)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="533"/>
        <source>відпуск за довіреністю вантажоодержувача: серія</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="579"/>
        <location filename="../src/Form_ttn.cpp" line="548"/>
        <source>від</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="635"/>
        <location filename="../src/Form_ttn.cpp" line="550"/>
        <source>р., виданою</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="658"/>
        <location filename="../src/Form_ttn.cpp" line="551"/>
        <source>Вантаж наданий для перевезення у стані, що</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="681"/>
        <source>(відповідає/не відповідає)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="694"/>
        <location filename="../src/Form_ttn.cpp" line="553"/>
        <source>правилам перевезень відповідних вантажів, номер пломби (за наявності)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="717"/>
        <source>кількість місць</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="740"/>
        <location filename="../forms/ttn.ui" line="799"/>
        <location filename="../src/Form_ttn.cpp" line="555"/>
        <location filename="../src/Form_ttn.cpp" line="558"/>
        <source>(словами)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="753"/>
        <location filename="../src/Form_ttn.cpp" line="556"/>
        <source>масою брутто, т</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="786"/>
        <location filename="../forms/ttn.ui" line="848"/>
        <location filename="../src/Form_ttn.cpp" line="557"/>
        <location filename="../src/Form_ttn.cpp" line="561"/>
        <source>(П.І.Б., посада, підпис)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="812"/>
        <location filename="../src/Form_ttn.cpp" line="559"/>
        <source>, отримав водій/експедитор</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="825"/>
        <source>Бухгалтер (відповідальна особа вантажовідправника)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="861"/>
        <source>Відпуск дозволив</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="884"/>
        <location filename="../src/Form_ttn.cpp" line="563"/>
        <source>(П.І.Б., посада, підпис, печатка)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="897"/>
        <location filename="../src/Form_ttn.cpp" line="564"/>
        <source>Усього відпущено на загальну суму</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="920"/>
        <source>,  у т.ч ПДВ</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="943"/>
        <location filename="../src/Form_ttn.cpp" line="566"/>
        <source>Супровідні документи на вантаж</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="966"/>
        <source>Транспортні послуги, які надаються автомобільним перевізником</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="999"/>
        <location filename="../src/Form_ttn.cpp" line="568"/>
        <source>Подтвердить</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="1012"/>
        <location filename="../src/Form_ttn.cpp" line="569"/>
        <source>Закрыть</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="1025"/>
        <location filename="../src/Form_ttn.cpp" line="570"/>
        <source>Печать</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/ttn.ui" line="1038"/>
        <location filename="../src/Form_ttn.cpp" line="571"/>
        <source>ВІДОМОСТІ ПРО ВАНТАЖ</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/Form_ttn.cpp" line="530"/>
        <source>Автомобільний перевезник</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/Form_ttn.cpp" line="532"/>
        <location filename="../src/Form_ttn.cpp" line="535"/>
        <source>(найменування П.І.Б)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/Form_ttn.cpp" line="533"/>
        <source>(П.І.Б, посвідчення водія)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/Form_ttn.cpp" line="546"/>
        <source>Відпуск за довіреністю вантажоодержувача: серія</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/Form_ttn.cpp" line="552"/>
        <source>(відповідає, не відповідає)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/Form_ttn.cpp" line="554"/>
        <source>Кількість місць</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/Form_ttn.cpp" line="560"/>
        <source>Бухгалтер (відповідальна особа вантавідправника)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/Form_ttn.cpp" line="562"/>
        <source>Відпуск дозвалив</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/Form_ttn.cpp" line="565"/>
        <source>,  у.т.ч ПДВ</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../src/Form_ttn.cpp" line="567"/>
        <source>Транспортні послуги, які надаються автомобільним перевізником:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Form_ttn</name>
    <message>
        <source>Windows styles missing</source>
        <translation type="obsolete">Стілі вікон не знайдені</translation>
    </message>
    <message>
        <source>Weighbridge</source>
        <translation type="obsolete">Weighbridge</translation>
    </message>
    <message>
        <source>Preview</source>
        <translation type="obsolete">Попередній перегляд</translation>
    </message>
    <message>
        <source>AV-Control</source>
        <translation type="obsolete">AV-Control</translation>
    </message>
    <message>
        <location filename="../src/Form_ttn.cpp" line="629"/>
        <location filename="../src/Form_ttn.cpp" line="649"/>
        <location filename="../src/Form_ttn.cpp" line="677"/>
        <location filename="../src/Form_ttn.cpp" line="743"/>
        <location filename="../src/Form_ttn.cpp" line="777"/>
        <location filename="../src/Form_ttn.cpp" line="841"/>
        <location filename="../src/Form_ttn.cpp" line="872"/>
        <location filename="../src/Form_ttn.cpp" line="956"/>
        <location filename="../src/Form_ttn.cpp" line="1049"/>
        <location filename="../src/Form_ttn.cpp" line="1080"/>
        <location filename="../src/Form_ttn.cpp" line="1119"/>
        <location filename="../src/Form_ttn.cpp" line="1147"/>
        <location filename="../src/Form_ttn.cpp" line="1275"/>
        <location filename="../src/Form_ttn.cpp" line="1312"/>
        <location filename="../src/Form_ttn.cpp" line="1353"/>
        <location filename="../src/Form_ttn.cpp" line="1384"/>
        <location filename="../src/Form_ttn.cpp" line="2003"/>
        <source>Prom-Soft</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HiddenWeighing</name>
    <message>
        <location filename="../forms/hiddenweighing.ui" line="20"/>
        <source>Hidden weighing</source>
        <translation>Прихованi зважування</translation>
    </message>
    <message>
        <location filename="../forms/hiddenweighing.ui" line="26"/>
        <source>Filter</source>
        <translation>Фiльтр</translation>
    </message>
    <message>
        <location filename="../forms/hiddenweighing.ui" line="60"/>
        <source>From</source>
        <translation>З</translation>
    </message>
    <message>
        <location filename="../forms/hiddenweighing.ui" line="87"/>
        <source>To</source>
        <translation>По</translation>
    </message>
    <message>
        <location filename="../forms/hiddenweighing.ui" line="125"/>
        <source>Accept</source>
        <translation>Прийняти</translation>
    </message>
    <message>
        <location filename="../forms/hiddenweighing.ui" line="144"/>
        <source>Reset</source>
        <translation>Скинути</translation>
    </message>
    <message>
        <location filename="../forms/hiddenweighing.ui" line="177"/>
        <source>Show photos</source>
        <translation>Фото</translation>
    </message>
    <message>
        <location filename="../src/HiddenWeighing.cpp" line="28"/>
        <source>Time</source>
        <translation>Час</translation>
    </message>
    <message>
        <location filename="../src/HiddenWeighing.cpp" line="29"/>
        <source>Weight, kg</source>
        <translation>Вага, кг</translation>
    </message>
    <message>
        <location filename="../src/HiddenWeighing.cpp" line="30"/>
        <source>Max weight, kg</source>
        <translation>Максимум ваги, кг</translation>
    </message>
    <message>
        <source>Select the entry first!</source>
        <translation type="obsolete">Спочатку оберіть запис!</translation>
    </message>
</context>
<context>
    <name>MailSend</name>
    <message>
        <location filename="../src/MailSend.cpp" line="139"/>
        <location filename="../src/MailSend.cpp" line="216"/>
        <source>Sending mail status</source>
        <translation>Відправка пошти</translation>
    </message>
    <message>
        <location filename="../src/MailSend.cpp" line="151"/>
        <source>SMTP client: Connection Failed</source>
        <translation>SMTP клієнт: Немає з&apos;єднання з поштовим сервером</translation>
    </message>
    <message>
        <location filename="../src/MailSend.cpp" line="164"/>
        <source>SMTP client: Authentification Failed</source>
        <translation>SMTP клієнт: Помилка аутентифікації</translation>
    </message>
    <message>
        <location filename="../src/MailSend.cpp" line="179"/>
        <source>SMTP client: Mail sending failed, Server response:</source>
        <translation>SMTP клієнт: Помилка відправки пошти, відповідь сервера:</translation>
    </message>
    <message>
        <location filename="../src/MailSend.cpp" line="179"/>
        <location filename="../src/MailSend.cpp" line="190"/>
        <location filename="../src/MailSend.cpp" line="200"/>
        <source> Code: </source>
        <translation>Код: </translation>
    </message>
    <message>
        <location filename="../src/MailSend.cpp" line="190"/>
        <location filename="../src/MailSend.cpp" line="200"/>
        <source>SMTP client: Mail sending successful, Server response:</source>
        <translation>SMTP клієнт: Пошту відправлено вдало, відповідь сервера:</translation>
    </message>
    <message>
        <location filename="../src/MailSend.cpp" line="215"/>
        <source>SMTP client: Mail sending failed, No file to send!</source>
        <translation type="unfinished">SMTP клієнт: Помилка відправки пошти, немає файла для відправки!</translation>
    </message>
</context>
<context>
    <name>MyClass</name>
    <message>
        <source>Weight</source>
        <translation type="obsolete">Вага</translation>
    </message>
    <message>
        <source>Report -</source>
        <translation type="obsolete">Звіт -</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="931"/>
        <source>Login in: </source>
        <translation>Вхід користувача:</translation>
    </message>
    <message>
        <source>The program has not been activated
or the temporary key has expired!
Please contact VIS-Service.</source>
        <translation type="obsolete">Программа не активована
або збігає термін дії ключа!
Будь ласка, зверніться у ВІС-Сервіс.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Скасувати</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="939"/>
        <source>Incorrect password</source>
        <translation>Невірний пароль</translation>
    </message>
    <message>
        <source>Windows styles missing</source>
        <translation type="obsolete">Стілі вікон не знайдені</translation>
    </message>
    <message>
        <source>The code is wrong!
Please contact VIS-Service.</source>
        <translation type="obsolete">Код не дійсний!
Зверніться до ВІС-Сервіс.</translation>
    </message>
    <message>
        <source>Enter the activation code for programm.</source>
        <translation type="obsolete">Введіть код активації ПЗ.</translation>
    </message>
    <message>
        <source>Enter the service code.</source>
        <translation type="obsolete">Введіть сервісний код.</translation>
    </message>
    <message>
        <source>Click to visit our website.</source>
        <translation type="obsolete">Перехід до нашого сайту.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1101"/>
        <source>Service</source>
        <translation>Службові</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1102"/>
        <source>Dictionary</source>
        <translation>Довідники</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1103"/>
        <source>Change user</source>
        <translation>Зміна користувача</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1106"/>
        <source>Add user</source>
        <translation>Додати користувача</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1111"/>
        <source>Event log</source>
        <translation>Журнал подій</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1137"/>
        <source>Settings</source>
        <translation>Налаштування</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1177"/>
        <source>Help</source>
        <translation>Допомога</translation>
    </message>
    <message>
        <source>User manual - F1</source>
        <translation type="obsolete">Довідник користувача F1</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1181"/>
        <location filename="../src/myclass.cpp" line="9786"/>
        <source>About</source>
        <translation>Про ПЗ</translation>
    </message>
    <message>
        <source>Activation</source>
        <translation type="obsolete">Активація</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1158"/>
        <source>Language</source>
        <translation>Мова</translation>
    </message>
    <message>
        <source>Search vehicles by number</source>
        <translation type="obsolete">Пошук авто за номером</translation>
    </message>
    <message>
        <source>Vehicles ¹</source>
        <translation type="obsolete">Авто №</translation>
    </message>
    <message>
        <source>Enter the vehicles number</source>
        <translation type="obsolete">Введіть номер авто</translation>
    </message>
    <message>
        <source>Num fragment</source>
        <translation type="obsolete">Фрагм. номеру</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="obsolete">Пошук</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="obsolete">Скинути</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1188"/>
        <source>Print</source>
        <translation>Друк</translation>
    </message>
    <message>
        <source>Cargo accept</source>
        <translation type="obsolete">Прийом</translation>
    </message>
    <message>
        <source>Cargo dispatch</source>
        <translation type="obsolete">Відправка</translation>
    </message>
    <message>
        <source>Press to accept the vehicle.</source>
        <translation type="obsolete">Натисніть для прийомки авто.</translation>
    </message>
    <message>
        <source>Press to send the vehicle</source>
        <translation type="obsolete">Натисніть для відправки авто</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1241"/>
        <source>Registry</source>
        <translation>Журнал</translation>
    </message>
    <message>
        <source>To view the history of weighing.</source>
        <translation type="obsolete">Подивитись проведені зважування.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1205"/>
        <source>Delete
record</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1207"/>
        <source>Deletes the selected entry in the table.</source>
        <translation>Видалити обраний запис з таблиці.</translation>
    </message>
    <message>
        <source>To enter the settings menu.</source>
        <translation type="obsolete">Вхід до налаштувань.</translation>
    </message>
    <message>
        <source>To edit dictionaries.</source>
        <translation type="obsolete">Редагувати довідники.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1249"/>
        <source>Zero &gt;0&lt;</source>
        <translation>Обнулення  &gt;0&lt;</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1279"/>
        <source>WEIGHT</source>
        <translation>МАСА</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1286"/>
        <source>
kg</source>
        <translation>
кг</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1343"/>
        <source>CAM 1</source>
        <translation>Камера 1</translation>
    </message>
    <message>
        <source>View weighing photo ¹1 cam ¹1</source>
        <translation type="obsolete">Перегляд фото 1 камери 1</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1364"/>
        <source>CAM 2</source>
        <translation>Камера 2</translation>
    </message>
    <message>
        <source>View weighing photo ¹2 cam ¹1</source>
        <translation type="obsolete">Перегляд фото 2 камери 1</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1386"/>
        <source>CAM 3</source>
        <translation>Камера 3</translation>
    </message>
    <message>
        <source>View weighing photo ¹1 cam ¹2</source>
        <translation type="obsolete">Перегляд фото 1 камери 2</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1407"/>
        <source>CAM 4</source>
        <translation>Камера 4</translation>
    </message>
    <message>
        <source>View weighing photo ¹2 cam ¹2</source>
        <translation type="obsolete">Перегляд фото 2 камери 2</translation>
    </message>
    <message>
        <source>View weighing photo ¹1 cam ¹3</source>
        <translation type="obsolete">Перегляд фото 1 камери 3</translation>
    </message>
    <message>
        <source>View weighing photo ¹1 cam ¹4</source>
        <translation type="obsolete">Перегляд фото 1 камери 4</translation>
    </message>
    <message>
        <source>View weighing photo ¹2 cam ¹3</source>
        <translation type="obsolete">Перегляд фото 2 камери 3</translation>
    </message>
    <message>
        <source>View weighing photo ¹2 cam ¹4</source>
        <translation type="obsolete">Перегляд фото 2 камери 4</translation>
    </message>
    <message>
        <source>Click on the traffic light to change its state.</source>
        <translation type="obsolete">Натисніть для зміни стану світлофора.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1767"/>
        <source>Select the entry to generate the report!</source>
        <translation>Оберіть запис для формування звіту!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1791"/>
        <source>Select record to start!</source>
        <translation>Спочатку оберіть запис!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1819"/>
        <location filename="../src/myclass.cpp" line="1975"/>
        <source>Data not available</source>
        <translation>Дані відсутні</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1837"/>
        <location filename="../src/myclass.cpp" line="1993"/>
        <source>Microsoft Office - Excel is not installed!</source>
        <translation>Microsoft Office - Excel не встановлено!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="2209"/>
        <location filename="../src/myclass.cpp" line="2279"/>
        <location filename="../src/myclass.cpp" line="3231"/>
        <location filename="../src/myclass.cpp" line="4701"/>
        <location filename="../src/myclass.cpp" line="4824"/>
        <source>From scales</source>
        <translation>З вагопроцесору</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="2211"/>
        <location filename="../src/myclass.cpp" line="2281"/>
        <location filename="../src/myclass.cpp" line="3233"/>
        <location filename="../src/myclass.cpp" line="4703"/>
        <location filename="../src/myclass.cpp" line="4826"/>
        <source>Manually</source>
        <translation>Вручну</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6438"/>
        <source>Select the entry to delete!</source>
        <translation>Оберіть запис для видалення!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6448"/>
        <source>Do you want to remove the entry?</source>
        <translation>Дійсно видалити запис?</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6450"/>
        <location filename="../src/myclass.cpp" line="9718"/>
        <source>Yes</source>
        <translation>Так</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6451"/>
        <location filename="../src/myclass.cpp" line="9719"/>
        <source>No</source>
        <translation>Ні</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6505"/>
        <source>Removed the record. Trailer num:</source>
        <translation>Видалений запис. Номер причепу:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6511"/>
        <source>Removed the record. Vehicle num:</source>
        <translation>Видалений запис. Номер авто:</translation>
    </message>
    <message>
        <source>This software - an automatic workstation
weigher (ARM) is intended to control and account
weighing for auto weighing using operator-weigher. 


This software is the property of
the company VIS-Service.</source>
        <translation type="obsolete">Це програмнє забеспечення - автоматичне робоче місце
вагаря (АРМ) - призначено для контролю та обліку
зважувань на автомобільних вагах оператором-вагарем.


Є власністю компанії ВІС-Сервіс.</translation>
    </message>
    <message>
        <source>Cargo send</source>
        <translation type="obsolete">Відправлення</translation>
    </message>
    <message>
        <source>   Delete
   record</source>
        <translation type="obsolete">   Видалити
   запис</translation>
    </message>
    <message>
        <source>Vehicles ?</source>
        <translation type="obsolete">Авто №</translation>
    </message>
    <message>
        <source>View weighing photo ?1 cam ?1</source>
        <translation type="obsolete">Перегляд фото 1 камери 1</translation>
    </message>
    <message>
        <source>View weighing photo ?2 cam ?1</source>
        <translation type="obsolete">Перегляд фото 2 камери 1</translation>
    </message>
    <message>
        <source>View weighing photo ?1 cam ?2</source>
        <translation type="obsolete">Перегляд фото 1 камери 2</translation>
    </message>
    <message>
        <source>View weighing photo ?2 cam ?2</source>
        <translation type="obsolete">Перегляд фото 2 камери 2</translation>
    </message>
    <message>
        <source>View weighing photo ?1 cam ?3</source>
        <translation type="obsolete">Перегляд фото 1 камери 3</translation>
    </message>
    <message>
        <source>View weighing photo ?1 cam ?4</source>
        <translation type="obsolete">Перегляд фото 1 камери 4</translation>
    </message>
    <message>
        <source>View weighing photo ?2 cam ?3</source>
        <translation type="obsolete">Перегляд фото 2 камери 3</translation>
    </message>
    <message>
        <source>View weighing photo ?2 cam ?4</source>
        <translation type="obsolete">Перегляд фото 2 камери 4</translation>
    </message>
    <message>
        <source>This software - an automatic workstation 
weigher (ARM) is intended to control and account 
weighing for auto weighing using operator-weigher. 


This software is the property of 
the company VIS-Service.</source>
        <translation type="obsolete">Це програмнє забеспечення - автоматичне робоче місце
вагаря (АРМ) - призначено для контролю та обліку
зважувань на автомобільних вагах оператором-вагарем.


Є власністю компанії ВІС-Сервіс.</translation>
    </message>
    <message>
        <source>Vehicles #</source>
        <translation type="obsolete">Авто №</translation>
    </message>
    <message>
        <source>Optional devices</source>
        <translation type="obsolete">Додат. обладнання</translation>
    </message>
    <message>
        <source>This software - an automatic workstation 
weigher (AWW) is intended to control and account 
weighing for auto weighing using operator-weigher. 


This software is the property of 
the company VIS-Service.</source>
        <translation type="obsolete">Це програмнє забеспечення - автоматичне робоче місце
вагаря (АРМ) - призначено для контролю та обліку
зважувань на автомобільних вагах оператором-вагарем.


Є власністю компанії ВІС-Сервіс.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="9814"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="2514"/>
        <location filename="../src/myclass.cpp" line="3317"/>
        <location filename="../src/myclass.cpp" line="3337"/>
        <location filename="../src/myclass.cpp" line="3471"/>
        <location filename="../src/myclass.cpp" line="3790"/>
        <location filename="../src/myclass.cpp" line="6819"/>
        <location filename="../src/myclass.cpp" line="6839"/>
        <source>Select the entry first!</source>
        <translation>Спочатку оберіть запис!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="94"/>
        <source>The trial version has expired!
Please activate the software
Technical support - 093 009 9990 
Prom-Soft</source>
        <translation>Программа не активована
або збігає термін дії ключа!

Будь ласка, зверніться за техніною підтримкою 093 009 999
Prom-Soft</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="2579"/>
        <location filename="../src/myclass.cpp" line="2595"/>
        <location filename="../src/myclass.cpp" line="4963"/>
        <location filename="../src/myclass.cpp" line="4974"/>
        <location filename="../src/myclass.cpp" line="5033"/>
        <location filename="../src/myclass.cpp" line="5044"/>
        <location filename="../src/myclass.cpp" line="5108"/>
        <location filename="../src/myclass.cpp" line="5119"/>
        <location filename="../src/myclass.cpp" line="5177"/>
        <location filename="../src/myclass.cpp" line="5188"/>
        <source>Communication with scales missing!</source>
        <translation>Немає зв&apos;язку з вагами!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="2813"/>
        <source>Trailer accepted:</source>
        <translation>Прийнятий причеп:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="2818"/>
        <source>Vehicle accepted:</source>
        <translation>Прийняте авто:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="3113"/>
        <source>Trailer send:</source>
        <translation>Відправлений причіп:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="3118"/>
        <source>Vehicle send:</source>
        <translation>Відправлене авто:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="3750"/>
        <location filename="../src/myclass.cpp" line="4012"/>
        <location filename="../src/myclass.cpp" line="4323"/>
        <source>Error. There are no photos to save!</source>
        <translation>Помилка. Фото відсутні!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="4656"/>
        <source>Data according to the filters was not found</source>
        <translation>Даних за обраними фільтрами немає</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="3245"/>
        <location filename="../src/myclass.cpp" line="4715"/>
        <location filename="../src/myclass.cpp" line="4839"/>
        <source>Photos</source>
        <translation>Фото</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1360"/>
        <source>View weighing photo #1 cam #1</source>
        <translation>Перегляд фото 1 камери 1</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1382"/>
        <source>View weighing photo #2 cam #1</source>
        <translation>Перегляд фото 2 камери 1</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1403"/>
        <source>View weighing photo #1 cam #2</source>
        <translation>Перегляд фото 1 камери 2</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1424"/>
        <source>View weighing photo #2 cam #2</source>
        <translation>Перегляд фото 2 камери 2</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1432"/>
        <source>View weighing photo #1 cam #3</source>
        <translation>Перегляд фото 1 камери 3</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1447"/>
        <source>View weighing photo #1 cam #4</source>
        <translation>Перегляд фото 1 камери 4</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1439"/>
        <source>View weighing photo #2 cam #3</source>
        <translation>Перегляд фото 2 камери 3</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="217"/>
        <source>Please wait until the process ends.</source>
        <translation>Будь ласка, дочекайтесь завершення процесу.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="227"/>
        <source>Database backup</source>
        <translation>Резервна копiя</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="233"/>
        <source>Push the button to start the process. It can take a long time.</source>
        <translation>Для старту процесу резервування натисніть кнопку. Це може зайняти тривалий час.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="234"/>
        <source>Save photos</source>
        <translation>Зберігати фото</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="237"/>
        <source>Dump</source>
        <translation>Створити копію</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="249"/>
        <source>Database restore</source>
        <translation>Відновлення бази даних</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="255"/>
        <source>Select the file to restore. Be careful! Current data will be deleted!</source>
        <translation>Виберіть файл, який потрібно відновити. Будьте обережні! Поточні дані будуть видалені!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="266"/>
        <location filename="../src/myclass.cpp" line="1149"/>
        <source>Restore</source>
        <translation>Відновити</translation>
    </message>
    <message>
        <source>Service engineer password</source>
        <translation type="obsolete">Пароль сервісного інженера</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1115"/>
        <source>Tara dictionary</source>
        <translation>Довідник тари</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1125"/>
        <source>Analytics of sensor&apos;s codes</source>
        <translation>Аналітика датчиків</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1130"/>
        <source>Hidden weighing</source>
        <translation>Прихванi зважування</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1179"/>
        <source>Enable modules</source>
        <translation>Підключення модулів</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1147"/>
        <source>Database</source>
        <translation>База даних</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1118"/>
        <source>Cards mode</source>
        <translation>Режим RFID карт</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1151"/>
        <source>Backup</source>
        <translation>Резервне копіювання</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1156"/>
        <source>Open the dump folder</source>
        <translation>Відкрити папку з копiями</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1201"/>
        <source>Tara</source>
        <translation>Тара</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1224"/>
        <location filename="../src/myclass.cpp" line="12508"/>
        <source>Check in</source>
        <translation>В&apos;їзд</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1232"/>
        <source>Check out</source>
        <translation>Виїзд</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1275"/>
        <location filename="../src/myclass.cpp" line="9672"/>
        <location filename="../src/myclass.cpp" line="9683"/>
        <source>No connection</source>
        <translation>Немає з&apos;єднання</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="93"/>
        <location filename="../src/myclass.cpp" line="12384"/>
        <source>Software License</source>
        <translation>Активація ПО</translation>
    </message>
    <message>
        <source>The trial version has expired!
Please activate the software
Technical support - 097 898 78 25
OOO &apos;Etalon-Systems&apos;</source>
        <translation type="obsolete">Программа не активована
або збігає термін дії ключа!
Будь ласка, зверніться у ВІС-Сервіс.</translation>
    </message>
    <message>
        <source>Dispenser</source>
        <translation type="obsolete">Дозатор</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1454"/>
        <source>View weighing photo #2 cam #4</source>
        <translation>Перегляд фото 2 камери 4</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1582"/>
        <source>Customer Service. Maintenance, repair, calibration of scales. 067-754-96-76</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1771"/>
        <location filename="../src/myclass.cpp" line="1795"/>
        <location filename="../src/myclass.cpp" line="1823"/>
        <location filename="../src/myclass.cpp" line="1841"/>
        <location filename="../src/myclass.cpp" line="1979"/>
        <location filename="../src/myclass.cpp" line="1997"/>
        <location filename="../src/myclass.cpp" line="2518"/>
        <location filename="../src/myclass.cpp" line="2583"/>
        <location filename="../src/myclass.cpp" line="2599"/>
        <location filename="../src/myclass.cpp" line="3794"/>
        <location filename="../src/myclass.cpp" line="4016"/>
        <location filename="../src/myclass.cpp" line="4327"/>
        <location filename="../src/myclass.cpp" line="4967"/>
        <location filename="../src/myclass.cpp" line="4978"/>
        <location filename="../src/myclass.cpp" line="5037"/>
        <location filename="../src/myclass.cpp" line="5048"/>
        <location filename="../src/myclass.cpp" line="5112"/>
        <location filename="../src/myclass.cpp" line="5123"/>
        <location filename="../src/myclass.cpp" line="5181"/>
        <location filename="../src/myclass.cpp" line="5192"/>
        <location filename="../src/myclass.cpp" line="6823"/>
        <location filename="../src/myclass.cpp" line="6843"/>
        <location filename="../src/myclass.cpp" line="7622"/>
        <location filename="../src/myclass.cpp" line="9721"/>
        <location filename="../src/myclass.cpp" line="10709"/>
        <location filename="../src/myclass.cpp" line="10977"/>
        <location filename="../src/myclass.cpp" line="10993"/>
        <location filename="../src/myclass.cpp" line="11009"/>
        <location filename="../src/myclass.cpp" line="11345"/>
        <location filename="../src/myclass.cpp" line="11420"/>
        <location filename="../src/myclass.cpp" line="11447"/>
        <location filename="../src/myclass.cpp" line="11457"/>
        <location filename="../src/myclass.cpp" line="11487"/>
        <location filename="../src/myclass.cpp" line="11554"/>
        <location filename="../src/myclass.cpp" line="11564"/>
        <location filename="../src/myclass.cpp" line="11581"/>
        <location filename="../src/myclass.cpp" line="11796"/>
        <source>Prom-Soft</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="2018"/>
        <source>(REAL)</source>
        <translation>(факт.)</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="2213"/>
        <location filename="../src/myclass.cpp" line="2283"/>
        <location filename="../src/myclass.cpp" line="3235"/>
        <location filename="../src/myclass.cpp" line="4705"/>
        <location filename="../src/myclass.cpp" line="4828"/>
        <source>Saved tara</source>
        <translation>Збережена</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="4069"/>
        <source>Error</source>
        <translation type="unfinished">Помилка</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="4070"/>
        <source>Not found TelSend.dll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="4887"/>
        <source>Entered the settings</source>
        <translation>Вхід до налаштувань</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="4916"/>
        <source>Entered the registry:</source>
        <translation>Вхід до журналу:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="4933"/>
        <source>Closed the registry:</source>
        <translation>Вихід з журналу:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="4943"/>
        <source>Closed the settings:</source>
        <translation>Вихід з налаштувань:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="4988"/>
        <location filename="../src/myclass.cpp" line="5058"/>
        <location filename="../src/myclass.cpp" line="5133"/>
        <location filename="../src/myclass.cpp" line="5202"/>
        <source>by Admin</source>
        <translation>під Адміністратором</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="4989"/>
        <location filename="../src/myclass.cpp" line="5059"/>
        <location filename="../src/myclass.cpp" line="5134"/>
        <location filename="../src/myclass.cpp" line="5203"/>
        <source>Do you want to weigh the vehicle?</source>
        <translation>Зважити авто?</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5003"/>
        <source>Weighing Brutto ignoring auto position!</source>
        <translation>Зважування Бруто ігноруючи позицію авто!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5073"/>
        <source>Weighing Tara ignoring auto position!</source>
        <translation>Зважування Тари ігноруючи позицію авто!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5148"/>
        <source>Weighing Brutto trailer ignoring auto position!</source>
        <translation>Зважування Бруто причепа ігноруючи позицію авто!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5217"/>
        <source>Weighing Tara trailer ignoring auto position!</source>
        <translation>Зважування Тари причепа ігноруючи позицію авто!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5251"/>
        <source>Zero error</source>
        <translation>Помилка обнулення</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5251"/>
        <source>COM-port closed</source>
        <translation>COM-порт закритий</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5257"/>
        <source>Set to zero: </source>
        <translation>Обнулення: </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5337"/>
        <source>Entered dictionaries: </source>
        <translation>Вхід у довідники:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5356"/>
        <source>Dictionaries closed: </source>
        <translation>Вихід з довідників: </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5383"/>
        <location filename="../src/myclass.cpp" line="5488"/>
        <location filename="../src/myclass.cpp" line="5593"/>
        <location filename="../src/myclass.cpp" line="5698"/>
        <location filename="../src/myclass.cpp" line="5804"/>
        <location filename="../src/myclass.cpp" line="5911"/>
        <location filename="../src/myclass.cpp" line="6013"/>
        <source>Enter the name</source>
        <translation>Введіть найменування</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5402"/>
        <location filename="../src/myclass.cpp" line="5462"/>
        <location filename="../src/myclass.cpp" line="5507"/>
        <location filename="../src/myclass.cpp" line="5567"/>
        <location filename="../src/myclass.cpp" line="5612"/>
        <location filename="../src/myclass.cpp" line="5672"/>
        <location filename="../src/myclass.cpp" line="5717"/>
        <location filename="../src/myclass.cpp" line="5778"/>
        <location filename="../src/myclass.cpp" line="5823"/>
        <location filename="../src/myclass.cpp" line="5883"/>
        <location filename="../src/myclass.cpp" line="5930"/>
        <location filename="../src/myclass.cpp" line="5989"/>
        <location filename="../src/myclass.cpp" line="6032"/>
        <location filename="../src/myclass.cpp" line="6091"/>
        <location filename="../src/myclass.cpp" line="6254"/>
        <location filename="../src/myclass.cpp" line="6281"/>
        <location filename="../src/myclass.cpp" line="6308"/>
        <location filename="../src/myclass.cpp" line="6335"/>
        <location filename="../src/myclass.cpp" line="6362"/>
        <location filename="../src/myclass.cpp" line="6390"/>
        <location filename="../src/myclass.cpp" line="6416"/>
        <source>Database error!</source>
        <translation>Помилка БД!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5424"/>
        <location filename="../src/myclass.cpp" line="5529"/>
        <location filename="../src/myclass.cpp" line="5634"/>
        <location filename="../src/myclass.cpp" line="5739"/>
        <location filename="../src/myclass.cpp" line="5845"/>
        <location filename="../src/myclass.cpp" line="5952"/>
        <location filename="../src/myclass.cpp" line="6054"/>
        <source>The name is already exist!</source>
        <translation>Таке найменування вже є!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5439"/>
        <location filename="../src/myclass.cpp" line="5469"/>
        <location filename="../src/myclass.cpp" line="5544"/>
        <location filename="../src/myclass.cpp" line="5574"/>
        <location filename="../src/myclass.cpp" line="5649"/>
        <location filename="../src/myclass.cpp" line="5679"/>
        <location filename="../src/myclass.cpp" line="5755"/>
        <location filename="../src/myclass.cpp" line="5785"/>
        <location filename="../src/myclass.cpp" line="5860"/>
        <location filename="../src/myclass.cpp" line="5890"/>
        <location filename="../src/myclass.cpp" line="5967"/>
        <location filename="../src/myclass.cpp" line="5996"/>
        <location filename="../src/myclass.cpp" line="6069"/>
        <location filename="../src/myclass.cpp" line="6098"/>
        <source>The record is successfully saved.</source>
        <translation>Запис збережений.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="5446"/>
        <location filename="../src/myclass.cpp" line="5476"/>
        <location filename="../src/myclass.cpp" line="5551"/>
        <location filename="../src/myclass.cpp" line="5581"/>
        <location filename="../src/myclass.cpp" line="5656"/>
        <location filename="../src/myclass.cpp" line="5686"/>
        <location filename="../src/myclass.cpp" line="5762"/>
        <location filename="../src/myclass.cpp" line="5792"/>
        <location filename="../src/myclass.cpp" line="5867"/>
        <location filename="../src/myclass.cpp" line="5897"/>
        <location filename="../src/myclass.cpp" line="5974"/>
        <location filename="../src/myclass.cpp" line="6002"/>
        <location filename="../src/myclass.cpp" line="6076"/>
        <location filename="../src/myclass.cpp" line="6105"/>
        <source>Added to dictionary: </source>
        <translation>Додано у довідник:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6242"/>
        <location filename="../src/myclass.cpp" line="6269"/>
        <location filename="../src/myclass.cpp" line="6296"/>
        <location filename="../src/myclass.cpp" line="6323"/>
        <location filename="../src/myclass.cpp" line="6350"/>
        <location filename="../src/myclass.cpp" line="6378"/>
        <location filename="../src/myclass.cpp" line="6404"/>
        <source>The entry was successfully deleted from the database!</source>
        <translation>Запис видалений з БД!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6249"/>
        <location filename="../src/myclass.cpp" line="6276"/>
        <location filename="../src/myclass.cpp" line="6303"/>
        <location filename="../src/myclass.cpp" line="6330"/>
        <location filename="../src/myclass.cpp" line="6357"/>
        <location filename="../src/myclass.cpp" line="6385"/>
        <location filename="../src/myclass.cpp" line="6411"/>
        <source>Deleted from dictionaries: </source>
        <translation>Видалений з довідника: </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6442"/>
        <source>Delete Record</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6452"/>
        <source>Delete record</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6887"/>
        <location filename="../src/myclass.cpp" line="7664"/>
        <source>Preview</source>
        <translation>Попередній перегляд</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6893"/>
        <source>Weight data from  </source>
        <translation>Зважування від </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6899"/>
        <source>Operating mode:</source>
        <translation>Режим роботи:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6900"/>
        <source>Accepting</source>
        <translation>Прийом</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6900"/>
        <source>Sending</source>
        <translation>Відправлення</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6936"/>
        <source>Total gross: </source>
        <translation>Сума брутто: </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6939"/>
        <source>Total tare: </source>
        <translation>Сума тара:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6942"/>
        <source>Total net: </source>
        <translation>Сума нетто: </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6946"/>
        <location filename="../src/myclass.cpp" line="7704"/>
        <source>Responsible: </source>
        <translation>Відповідальний: </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="6948"/>
        <source>Date/time </source>
        <translation>Дата/час </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7146"/>
        <location filename="../src/myclass.cpp" line="7180"/>
        <location filename="../src/myclass.cpp" line="12908"/>
        <source>Vehicle
number</source>
        <translation>Номер
авто</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7147"/>
        <source>Sender</source>
        <translation>Відправник</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7148"/>
        <source>Recipient</source>
        <translation>Отримувач</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7149"/>
        <source>Payer</source>
        <translation>Платник</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7150"/>
        <source>Carrier</source>
        <translation>Перевізник</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7151"/>
        <source>Cargo</source>
        <translation>Вантаж</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7152"/>
        <source>Cargo value</source>
        <translation>Ціна вантажу</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7173"/>
        <source>Scales</source>
        <translation>Ваги</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7178"/>
        <location filename="../src/myclass.cpp" line="12906"/>
        <source>Supplier</source>
        <translation>Постачальник</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7179"/>
        <location filename="../src/myclass.cpp" line="12907"/>
        <source>Material</source>
        <translation>Матеріал</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7184"/>
        <location filename="../src/myclass.cpp" line="12912"/>
        <source>Blocked</source>
        <translation>Блокована</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="9810"/>
        <source>This software is an automatic workstation 
weighing system wich is intended to control and account 
auto weighing with assistance of operator-weigher. 


Technical support - 093 009 9990 
Prom-Soft</source>
        <translation>Це програмнє забеспечення - автоматичне робоче місце
вагаря - призначене для контролю та обліку
зважувань на автомобільних вагах оператором-вагарем.

Телефон технічної підтримки - 093 009 9990 
Prom-Soft
.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12385"/>
        <source>The trial version has expired!
Please activate the software
Technical support - 093 009 9990 
 &apos;Prom-Soft&apos;</source>
        <translation>Программа не активована
або збігає термін дії ключа!

Будь ласка, зверніться за техніною підтримкою 093 009 999
 &apos;Prom-Soft</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12464"/>
        <source>Registration</source>
        <translation>Регістрація</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12605"/>
        <location filename="../src/myclass.cpp" line="12713"/>
        <source>Repeated reading of RFID card %1 !</source>
        <translation>Повторне зчитування карти %1 !</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12653"/>
        <source>Second weighing for card # </source>
        <translation>Друге зважування для карти № </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12654"/>
        <location filename="../src/myclass.cpp" line="12757"/>
        <location filename="../src/myclass.cpp" line="12819"/>
        <source>, auto number </source>
        <translation>, авто з номером </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12655"/>
        <location filename="../src/myclass.cpp" line="12758"/>
        <location filename="../src/myclass.cpp" line="12820"/>
        <source>, material </source>
        <translation>, матеріалом </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12656"/>
        <location filename="../src/myclass.cpp" line="12759"/>
        <location filename="../src/myclass.cpp" line="12821"/>
        <source> and supplier </source>
        <translation> та постачальником </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12656"/>
        <location filename="../src/myclass.cpp" line="12759"/>
        <location filename="../src/myclass.cpp" line="12821"/>
        <source> is done.</source>
        <translation> зроблено.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12756"/>
        <source>First weighing for card # </source>
        <translation>Перше зважування для карти № </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12817"/>
        <source>First and Second weighing for card # </source>
        <translation>Перше та друге зважування для карти № </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12818"/>
        <source>, auto tare </source>
        <translation>, тара авто</translation>
    </message>
    <message>
        <source>Second weighing for card ? %1, auto number %2, material %3 and supplier %4 is done.</source>
        <translation type="obsolete">Друге зважування для карти № %1, номером авто %2, матеріалом %3 та постчальником %4 зроблено.</translation>
    </message>
    <message>
        <source>First weighing for card ? %1, auto number %2, material %3 and supplier %4 is done.</source>
        <translation type="obsolete">Перше зважування для карти № %1, номером авто %2, матеріалом %3 та постчальником %4 зроблено.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="12861"/>
        <source>Weighing is not possible! Card with code %1 is not registered!</source>
        <translation>зважування неможливе! Карта з кодом %1 не зареєстрована!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1194"/>
        <location filename="../src/myclass.cpp" line="7154"/>
        <source>Gross</source>
        <translation>Брутто</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="540"/>
        <source>Enter password</source>
        <translation>Ввести пароль</translation>
    </message>
    <message>
        <source>Dispenser Mode</source>
        <translation type="obsolete">Режим Дозатор</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="3321"/>
        <location filename="../src/myclass.cpp" line="3341"/>
        <location filename="../src/myclass.cpp" line="3475"/>
        <location filename="../src/myclass.cpp" line="3754"/>
        <source>AV-Control</source>
        <translation type="unfinished">AV-Control</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7155"/>
        <source>Tare</source>
        <translation>Тара</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7156"/>
        <location filename="../src/myclass.cpp" line="7177"/>
        <location filename="../src/myclass.cpp" line="12905"/>
        <source>Net</source>
        <translation>Нетто</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7157"/>
        <location filename="../src/myclass.cpp" line="7172"/>
        <location filename="../src/myclass.cpp" line="12901"/>
        <source>Date
time</source>
        <translation>Дата
час</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7158"/>
        <source>Waybill</source>
        <translation>Накл.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7159"/>
        <source>User</source>
        <translation>Прийняв</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7160"/>
        <location filename="../src/myclass.cpp" line="7181"/>
        <location filename="../src/myclass.cpp" line="12909"/>
        <source>Trailer
number</source>
        <translation>Номер
причепу</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7161"/>
        <source>Gross
(trailer)</source>
        <translation>Брутто
(причіп)</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7162"/>
        <source>Tare
(trailer)</source>
        <translation>Тара
(причіп)</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7163"/>
        <source>Net
(trailer)</source>
        <translation>Нетто
(причіп)</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7164"/>
        <source>Tara
enter</source>
        <translation>Введення
тари</translation>
    </message>
    <message>
        <source>Entries by specified criteria was not found!</source>
        <translation type="obsolete">Записів за обраним критерієм немає!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7462"/>
        <location filename="../src/myclass.cpp" line="7478"/>
        <location filename="../src/myclass.cpp" line="7494"/>
        <location filename="../src/myclass.cpp" line="7510"/>
        <source>Cam is turn off</source>
        <translation>Камера вимкнена</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7618"/>
        <source>There is no data to print!</source>
        <translation>Немає даних для друку!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7671"/>
        <source>vehicles accepted</source>
        <translation>прийняті авто</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7673"/>
        <source>vehicles sended</source>
        <translation>відправлені авто</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7676"/>
        <source>Weight data - </source>
        <translation>Дані зважувань - </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7706"/>
        <source>Date time </source>
        <translation>Дата час </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="9621"/>
        <source>Start server</source>
        <translation>Старт серверу</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="9647"/>
        <source>Stop server</source>
        <translation>Стоп серверу</translation>
    </message>
    <message>
        <source>User: </source>
        <translation type="obsolete">Користувач: </translation>
    </message>
    <message>
        <source>Scales link status </source>
        <translation type="obsolete">Зв&apos;язок з вагами </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="9668"/>
        <location filename="../src/myclass.cpp" line="9679"/>
        <source>Connected</source>
        <translation>З&apos;єднано</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="obsolete">Відсутній</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="9716"/>
        <source>Do you want to exit the program?</source>
        <translation>Закрити програму?</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="9757"/>
        <source>Exit program</source>
        <translation>Вихід</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="10722"/>
        <source>Attempt to send e-mail successfully </source>
        <translation>Спроба відправлення пошти успішна </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="10726"/>
        <source>Sended to </source>
        <translation>Відправлено на </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="10732"/>
        <source>Attempt to send e-mail unsuccessful </source>
        <translation>Спроба відправлення пошти неуспішна </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="10736"/>
        <source>The letter is not sent to destination. Check your mail settings and internet connection.</source>
        <translation>Лист не відправляється до місця призначення. Перевірте налаштування електронної пошти і підключення до інтернету.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11791"/>
        <source>The weight on the weighing platform is less than the minimum </source>
        <translation>Вага на платформі меньше мінімальної </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11775"/>
        <location filename="../src/myclass.cpp" line="12571"/>
        <source>The vehicle is not correctly standing on the weighing platform!</source>
        <translation>Авто не коректно встановлено на ваговій платформі!</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11732"/>
        <source>CABINET IS </source>
        <translation>ШАФА </translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11732"/>
        <source>CLOSED</source>
        <translation>ЗАЧИНЕНА</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11732"/>
        <source>OPENED</source>
        <translation>ВІДКРИТА</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11774"/>
        <source>Positioning</source>
        <translation>Позиціонування</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11336"/>
        <source>Setting of zero successful.
Press &apos;Ok&apos; to continue.</source>
        <translation>ВП обнулений успiшно.
Натиснiть &apos;Ок&apos;  для продовження.</translation>
    </message>
    <message>
        <source>AV-Control </source>
        <translation type="obsolete">AV-Control</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1098"/>
        <source> - Material accounting system</source>
        <translation> - Система обліку материалів</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7175"/>
        <location filename="../src/myclass.cpp" line="12903"/>
        <source>Weight 1</source>
        <translation>Зважування 1</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7176"/>
        <location filename="../src/myclass.cpp" line="12904"/>
        <source>Weight 2</source>
        <translation>Зважування 2</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7185"/>
        <location filename="../src/myclass.cpp" line="12913"/>
        <source>Photo</source>
        <translation>Фото</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7174"/>
        <location filename="../src/myclass.cpp" line="12914"/>
        <source>RFID Code</source>
        <translation>Код карти</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="1098"/>
        <source>Prom-Soft </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7182"/>
        <location filename="../src/myclass.cpp" line="12910"/>
        <source>Driver</source>
        <translation>Водій</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="7183"/>
        <location filename="../src/myclass.cpp" line="12911"/>
        <source>Order number</source>
        <translation>Порядк. номер</translation>
    </message>
    <message>
        <source>This software - an automatic workstation 
weigher (AWW) is intended to control and account 
weighing for auto weighing using operator-weigher. 


This software is the property of 
the company ETALON-System.</source>
        <translation type="obsolete">Це програмне забезпечення розроблене і належить ТОВ «Еталон-Систем».
Будь-яке копіювання та розповсюдження без узгодження, суворо заборонено.
ТОВ «Еталон-Систем» р.Дніпро,
телефон 097 898 78 25. адреса сайту www.e-s.in.ua
Вагове обладнання і програмне забезпечення.
Системи АСУ технологічних процесів і програми обліку.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11340"/>
        <source>Setting of zero unsuccessful.
Press &apos;Ok&apos; to continue.</source>
        <translation>Не вдалося обнулити ВП.
Натиснiть &apos;Ок&apos;  для продовження.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11416"/>
        <source>Folder to save the dump file can not be found. A new folder will be created.</source>
        <translation>Папка для збереження резервної копії базі данних відсутня. Буде створена нова папка.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11443"/>
        <source>The dump was successfully created. Filename: </source>
        <translation>Дамп був успішно створений з ім&apos;ям:</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11453"/>
        <source>Creating dump failed.</source>
        <translation>Створення дампа не вдалося.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11483"/>
        <source>Dump file can not be found.</source>
        <translation>Файл не існуе.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11550"/>
        <source>The database is restored. Please restart the program.</source>
        <translation>База даних відновлена. Будь ласка, перезапустіть програму.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11560"/>
        <source>Unexpected error.</source>
        <translation>Несподівана помилка.</translation>
    </message>
    <message>
        <location filename="../src/myclass.cpp" line="11577"/>
        <source>Dump folder can not be found. Make your first dump for creation it.</source>
        <translation>Папка із файлами не знайдена. Здісніть своє перше резервування для її створення.</translation>
    </message>
    <message>
        <source>Weighbridge</source>
        <translation type="obsolete">Weighbridge</translation>
    </message>
</context>
<context>
    <name>MyClassClass</name>
    <message>
        <location filename="../forms/myclass.ui" line="26"/>
        <source>MyClass</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>RegistrationModel</name>
    <message>
        <location filename="../src/Models/registrationModel.cpp" line="56"/>
        <source>Yes</source>
        <translation>Так</translation>
    </message>
    <message>
        <location filename="../src/Models/registrationModel.cpp" line="56"/>
        <source>No</source>
        <translation>Ні</translation>
    </message>
</context>
<context>
    <name>SettingsForm</name>
    <message>
        <location filename="../src/settings.cpp" line="64"/>
        <source>Settings</source>
        <translation>Налаштування</translation>
    </message>
    <message>
        <source>COM-port settings</source>
        <translation type="obsolete">Налаштування
COM-порту</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="89"/>
        <source>Analytics of
sensor&apos;s codes</source>
        <translation>Аналітика
датчиків</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="93"/>
        <source>Recording interval, sec</source>
        <translation>Інтервал запису, сек</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="99"/>
        <source>Number of sensors</source>
        <translation>Кількість датчиків</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="105"/>
        <source>Contracting party</source>
        <translation>Контрагент</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="111"/>
        <source>Type of scales</source>
        <translation>Тип ваг</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="175"/>
        <source>Weighing proc. model</source>
        <translation>Модель вагопроцесору</translation>
    </message>
    <message>
        <source>Adress WP</source>
        <translation type="obsolete">Адреса ВП</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="231"/>
        <location filename="../src/settings.cpp" line="309"/>
        <source>Koef.</source>
        <translation>Коеф.</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="242"/>
        <source>Baud rate</source>
        <translation>Швидкість</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="257"/>
        <source>End of frame</source>
        <translation>Кінець посилки</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="271"/>
        <source>Radio modem</source>
        <translation>Радіомодем</translation>
    </message>
    <message>
        <source>Use USB</source>
        <translation type="obsolete">Використання USB</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="288"/>
        <source>Switch ON</source>
        <translation>Ввести в дію</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="471"/>
        <source>Show traffic light</source>
        <translation>Відобразити світлофор</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="430"/>
        <source>Use BC-01</source>
        <translation>Використ. БК-01</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="82"/>
        <source>COM-port</source>
        <translation>Com-порт</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="442"/>
        <source>WP Model via BC-01</source>
        <translation>Модель ВП для зв&apos;язку</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="457"/>
        <source>Input delay, ms</source>
        <translation>Вхідна затримка, мс</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="116"/>
        <location filename="../src/settings.cpp" line="490"/>
        <location filename="../src/settings.cpp" line="726"/>
        <location filename="../src/settings.cpp" line="837"/>
        <location filename="../src/settings.cpp" line="1005"/>
        <location filename="../src/settings.cpp" line="1133"/>
        <source>Save</source>
        <translation>Зберегти</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Закрити</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="510"/>
        <source>UDP params</source>
        <translation>UDP параметри</translation>
    </message>
    <message>
        <source>IP-adress</source>
        <translation type="obsolete">IP-адреса</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="219"/>
        <source>Address WP</source>
        <translation>Адреса ВП</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="280"/>
        <source>Periodic reopen</source>
        <translation>Періодичний перезапуск</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="352"/>
        <source>Position on Socket1</source>
        <translation>Позиціонування по Socket1</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="477"/>
        <source>Enable position sensors</source>
        <translation>Задiяти
датчики положення</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="483"/>
        <source>Sensor inversion</source>
        <translation>Інверсія</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="499"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="535"/>
        <location filename="../src/settings.cpp" line="637"/>
        <source>IP-address</source>
        <translation>IP-адреса</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="543"/>
        <source>Src. port</source>
        <translation>Порт відправки</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="551"/>
        <source>Dest. port</source>
        <translation>Порт отримки</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="601"/>
        <source>IP cams</source>
        <translation>Відеокамери</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="635"/>
        <source>IP cam settings</source>
        <translation>Налаштування IP камер</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="647"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="299"/>
        <source>Soreness</source>
        <translation>Засміченість</translation>
    </message>
    <message>
        <source>Massa sornosti</source>
        <translation type="obsolete">Маса сорности</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="320"/>
        <source>Discret 20kg</source>
        <translation>Дискрет 20кг</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="325"/>
        <source>Show weight</source>
        <translation>Відображати вагу завжди</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="330"/>
        <source>Use for brutto only</source>
        <translation>Лише для брутування</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="336"/>
        <source>Weight by TCP/IP converter</source>
        <translation>Вага через TCP конвертор</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="343"/>
        <location filename="../src/settings.cpp" line="356"/>
        <location filename="../src/settings.cpp" line="394"/>
        <location filename="../src/settings.cpp" line="414"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="352"/>
        <source>Position on Socket2</source>
        <translation>Позиціонування по Socket2</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="362"/>
        <source>Inversion</source>
        <translation>Інверсія</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="366"/>
        <source>Use S1</source>
        <translation>Викор. S1</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="370"/>
        <source>Use 4ch</source>
        <translation>Заст.4к</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="374"/>
        <source>Ignore</source>
        <translation>Ігнор.</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="390"/>
        <source>Trafflight on Socket2</source>
        <translation>Cвітлофор по Socket2</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="410"/>
        <source>Barrier on Socket2</source>
        <translation>Шлагбаум по Socket2</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="659"/>
        <location filename="../src/settings.cpp" line="897"/>
        <location filename="../src/settings.cpp" line="911"/>
        <location filename="../src/settings.cpp" line="925"/>
        <source>Enable</source>
        <translation>Задіяти</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="668"/>
        <source>Photo enable</source>
        <translation>Фотофіксація</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="676"/>
        <source>Photo preview enable</source>
        <translation>Перегляд фото</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="683"/>
        <location filename="../src/settings.cpp" line="765"/>
        <source>Login</source>
        <translation>Логін</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="693"/>
        <location filename="../src/settings.cpp" line="772"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="722"/>
        <source>Show cams in a horizontal row (vertical if not checked)</source>
        <translation>Відобразити камери в ряд по горизонталі (інакше по вертикалі)</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="734"/>
        <source>Server autostart</source>
        <translation>Автостарт серверу</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="749"/>
        <source>Email settings</source>
        <translation>Налаштування
пошти</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="751"/>
        <source>Host</source>
        <translation>Поштовий сервер</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="758"/>
        <source>SMTP</source>
        <translation>Серверний порт</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="791"/>
        <source>Send interval, min</source>
        <translation>Інтервал відправлення, мін</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="807"/>
        <source>Send each record</source>
        <translation>Відсилати кожен запис</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="811"/>
        <source>Addressee</source>
        <translation>Адресат</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="818"/>
        <source>Use SSL</source>
        <translation>Використання SSL</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="822"/>
        <source>Mail ON</source>
        <translation>Включити розсилку</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="826"/>
        <source>Save all photos</source>
        <translation>Відправляти фото</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="845"/>
        <source>Excel datetime</source>
        <translation>Формат часу Excel</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="856"/>
        <source>Additional addresses</source>
        <translation>Додаткові адресати</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="932"/>
        <source>Rfid on Socket2</source>
        <translation>Зчитувач Rfid карт</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="936"/>
        <source>Rfid</source>
        <translation>Rfid</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="945"/>
        <source>For registration</source>
        <translation>Для реєстрації</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="950"/>
        <source>Lamps</source>
        <translation>Лампи</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="1096"/>
        <source>MySQL</source>
        <translation>MySQL</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="1143"/>
        <source>Photo
 stamp</source>
        <translation>Інформація для фото</translation>
    </message>
    <message>
        <source>Internet connection</source>
        <translation type="obsolete">Інтернет з&apos;єднання</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="885"/>
        <source>Optional devices</source>
        <translation>Додат.
обладнання</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="888"/>
        <location filename="../src/settings.cpp" line="902"/>
        <location filename="../src/settings.cpp" line="916"/>
        <location filename="../src/settings.cpp" line="969"/>
        <location filename="../src/settings.cpp" line="975"/>
        <location filename="../src/settings.cpp" line="981"/>
        <location filename="../src/settings.cpp" line="987"/>
        <location filename="../src/settings.cpp" line="993"/>
        <location filename="../src/settings.cpp" line="999"/>
        <source>Socket2</source>
        <translation>IP-комутатор</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="1011"/>
        <location filename="../src/settings.cpp" line="1037"/>
        <source>TCM 2</source>
        <translation>TCM 2</translation>
    </message>
    <message>
        <source>Photo
time stamp</source>
        <translation type="obsolete">Фото
штамп часу</translation>
    </message>
    <message>
        <source>Photo/ntime stamp</source>
        <translation type="obsolete">Фото
штамп часу</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="1145"/>
        <source>Top-left</source>
        <translation>Верх-ліво</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="1151"/>
        <source>Bottom-left</source>
        <translation>Низ-ліво</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="1157"/>
        <source>Top-right</source>
        <translation>Верх-право</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="1163"/>
        <source>Bottom-right</source>
        <translation>Низ-право</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="1064"/>
        <source>Minimum weight, kg</source>
        <translation>Мінімальна вага, кг</translation>
    </message>
    <message>
        <source>Minimum weight</source>
        <translation type="obsolete">Мінімальна вага</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="1071"/>
        <source>Time switch, sec</source>
        <translation>Очікувати, сек</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="1520"/>
        <source>Scale</source>
        <translation>Ваги</translation>
    </message>
    <message>
        <source>Checking</source>
        <translation type="obsolete">Перевірка</translation>
    </message>
    <message>
        <source>Internet connection is available.</source>
        <translation type="obsolete">Інтернет з&apos;єднання присутнє.</translation>
    </message>
    <message>
        <source>Internet connection is not available.</source>
        <translation type="obsolete">Інтернет з&apos;єднання відсутнє.</translation>
    </message>
</context>
<context>
    <name>acceptionForm</name>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="30"/>
        <source>Accept/Send</source>
        <translation>Прийом\відправлення</translation>
    </message>
    <message>
        <source>Current operation</source>
        <translation type="obsolete">Поточна операція</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="46"/>
        <source>Empty vehicle</source>
        <translation>Порожнє авто</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="56"/>
        <source>Loaded vehicle</source>
        <translation>Навантажене авто</translation>
    </message>
    <message>
        <source>Waybill</source>
        <translation type="obsolete">Накл.</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="39"/>
        <source>Scales</source>
        <translation>Ваги</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="68"/>
        <source>Waybill </source>
        <translation>Накладна </translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="72"/>
        <source>Waybill number</source>
        <translation>Номер накладної</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="81"/>
        <source>Vehicle number</source>
        <translation>Номер авто</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="95"/>
        <source>Trailer separately</source>
        <translation>Зважити причіп окремо</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="99"/>
        <source>Trailer number</source>
        <translation>Номер причіпу</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="113"/>
        <source>Sender</source>
        <translation>Відправник</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="90"/>
        <location filename="../src/AcceptionForms.cpp" line="108"/>
        <location filename="../src/AcceptionForms.cpp" line="122"/>
        <location filename="../src/AcceptionForms.cpp" line="136"/>
        <location filename="../src/AcceptionForms.cpp" line="150"/>
        <location filename="../src/AcceptionForms.cpp" line="164"/>
        <location filename="../src/AcceptionForms.cpp" line="178"/>
        <source>Add</source>
        <translation>Додати</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="127"/>
        <source>Recipient</source>
        <translation>Отримувач</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="141"/>
        <source>Payer</source>
        <translation>Платник</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="155"/>
        <source>Carrier</source>
        <translation>Перевізник</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="169"/>
        <source>Cargo</source>
        <translation>Вантаж</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="183"/>
        <source>Cargo price</source>
        <translation>Ціна за т.</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="195"/>
        <source>Latest data</source>
        <translation>Останні відомості</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="246"/>
        <source>Soreness</source>
        <translation>Засміченість</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="1045"/>
        <source>Scales - </source>
        <translation>Ваги - </translation>
    </message>
    <message>
        <source>Last data</source>
        <translation type="obsolete">Останнє заповнення</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="205"/>
        <source>Actual weight</source>
        <translation>Фактична вага</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="209"/>
        <location filename="../src/AcceptionForms.cpp" line="289"/>
        <source>Gross</source>
        <translation>Брутто</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="218"/>
        <location filename="../src/AcceptionForms.cpp" line="232"/>
        <location filename="../src/AcceptionForms.cpp" line="298"/>
        <location filename="../src/AcceptionForms.cpp" line="312"/>
        <source>Weigh</source>
        <translation>Зважити</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="223"/>
        <location filename="../src/AcceptionForms.cpp" line="303"/>
        <source>Tare</source>
        <translation>Тара</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="237"/>
        <location filename="../src/AcceptionForms.cpp" line="317"/>
        <source>Net</source>
        <translation>Нетто</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="257"/>
        <location filename="../src/AcceptionForms.cpp" line="326"/>
        <source>Manual tare</source>
        <translation>Тара вручну</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="401"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <source>Tare from datbase</source>
        <translation type="obsolete">Тара з довідника</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="265"/>
        <source>Tare from database</source>
        <translation>Тара з довiдника</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="285"/>
        <source>Actual trailer weight</source>
        <translation>Фактична вага причепа</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="349"/>
        <source>Accepted:</source>
        <translation>Прийняв:</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="353"/>
        <location filename="../src/AcceptionForms.cpp" line="375"/>
        <source>User:</source>
        <translation>Користувач:</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="362"/>
        <location filename="../src/AcceptionForms.cpp" line="384"/>
        <source>Date/time</source>
        <translation>Дата/час</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="371"/>
        <source>Sended:</source>
        <translation>Відправив:</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="394"/>
        <source>Confirm</source>
        <translation>Підтвердити</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Закрити</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="668"/>
        <location filename="../src/AcceptionForms.cpp" line="965"/>
        <source>Enter waybill number</source>
        <translation>Введіть номер накладної</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="682"/>
        <location filename="../src/AcceptionForms.cpp" line="969"/>
        <source>Enter vehicle number</source>
        <translation>Введіть номер авто</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="694"/>
        <location filename="../src/AcceptionForms.cpp" line="967"/>
        <source>Enter trailer number</source>
        <translation>Введіть номер причепа</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="922"/>
        <location filename="../src/AcceptionForms.cpp" line="929"/>
        <location filename="../src/AcceptionForms.cpp" line="935"/>
        <location filename="../src/AcceptionForms.cpp" line="941"/>
        <location filename="../src/AcceptionForms.cpp" line="947"/>
        <location filename="../src/AcceptionForms.cpp" line="953"/>
        <source>The number of characters entered is greater than MAX possible!</source>
        <translation>Кількість введених символів перевищує максимально можливу!</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="971"/>
        <source>Enter sender name</source>
        <translation>Введіть відправника</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="973"/>
        <source>Enter recipient name</source>
        <translation>Введіть одержувача</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="975"/>
        <source>Enter payer name</source>
        <translation>Введіть платника</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="977"/>
        <source>Enter carrier name</source>
        <translation>Введіть перевізника</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="979"/>
        <source>Enter cargo name</source>
        <translation>Введіть найменування вантажу</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="996"/>
        <source>Set tare manually</source>
        <translation>Встановити тару вручну</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="998"/>
        <location filename="../src/AcceptionForms.cpp" line="1002"/>
        <source>Enter tare</source>
        <translation>Ввести тару</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="1000"/>
        <source>Set trailer tare manually</source>
        <translation>Встановити тару вручну</translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="1063"/>
        <source>Prom-Soft</source>
        <translation></translation>
    </message>
    <message>
        <source>Current operation - </source>
        <translation type="obsolete">Поточна операція - </translation>
    </message>
    <message>
        <location filename="../src/AcceptionForms.cpp" line="1059"/>
        <source>Data for today is not available.</source>
        <translation>Даних на сьогодні немає.</translation>
    </message>
    <message>
        <source>Weighbridge</source>
        <translation type="obsolete">Weighbridge</translation>
    </message>
</context>
<context>
    <name>activationOfProgramForm</name>
    <message>
        <source>Activation of program</source>
        <translation type="obsolete">Активація програмного забезпечення</translation>
    </message>
    <message>
        <source>Software activation</source>
        <translation type="obsolete">Активація ПО</translation>
    </message>
    <message>
        <source>The activation parameters</source>
        <translation type="obsolete">Параметри активації</translation>
    </message>
    <message>
        <source>Enter activation code</source>
        <translation type="obsolete">Введіть код активації</translation>
    </message>
    <message>
        <source>Confirm code</source>
        <translation type="obsolete">Прийняти код</translation>
    </message>
    <message>
        <source>Expiration date</source>
        <translation type="obsolete">Строк дії</translation>
    </message>
    <message>
        <source>Service activation</source>
        <translation type="obsolete">Активація сервісного обслуговування</translation>
    </message>
    <message>
        <source>Enter service code</source>
        <translation type="obsolete">Введіть сервісний код</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="obsolete">Вихід</translation>
    </message>
    <message>
        <source>Maintenance</source>
        <translation type="obsolete">Технічне обслуговування</translation>
    </message>
    <message>
        <source>Complete maintenance of scales.
Inquiries by phone at 
Dnepropetrovsk:

</source>
        <translation type="obsolete">Час пройти технічне обслуговування!
Довідки за телефоном у Дніпропетровську:
</translation>
    </message>
    <message>
        <source>(around the clock)</source>
        <translation type="obsolete">(цілодобово)</translation>
    </message>
    <message>
        <source>(office hours)</source>
        <translation type="obsolete">(в робочій час)</translation>
    </message>
</context>
<context>
    <name>dialogVPSensors</name>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="20"/>
        <source>Аналитика датчиков</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="38"/>
        <source>Фильтр</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="56"/>
        <source>По времени</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="68"/>
        <source>с</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="103"/>
        <source>по</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="139"/>
        <source>Отклонение</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="161"/>
        <source>кг</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="184"/>
        <source>код</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="204"/>
        <source>Экспортировать
таблицу в Excel</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="237"/>
        <source>Принять</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/dialogvpsensors.ui" line="256"/>
        <source>Отмена</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dialogvpsensors.cpp" line="188"/>
        <location filename="../src/dialogvpsensors.cpp" line="336"/>
        <source>Data not available</source>
        <translation>Дані відсутні</translation>
    </message>
    <message>
        <location filename="../src/dialogvpsensors.cpp" line="192"/>
        <location filename="../src/dialogvpsensors.cpp" line="210"/>
        <location filename="../src/dialogvpsensors.cpp" line="340"/>
        <location filename="../src/dialogvpsensors.cpp" line="358"/>
        <source>Prom-Soft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weighbridge</source>
        <translation type="obsolete">Weighbridge</translation>
    </message>
    <message>
        <location filename="../src/dialogvpsensors.cpp" line="206"/>
        <location filename="../src/dialogvpsensors.cpp" line="354"/>
        <source>Microsoft Office - Excel is not installed!</source>
        <translation>Microsoft Office - Excel не встановлено!</translation>
    </message>
</context>
<context>
    <name>dictionaryFormClass</name>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="23"/>
        <location filename="../src/dictionaryForm.cpp" line="29"/>
        <source>Dictionaries</source>
        <translation>Довідники</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="68"/>
        <source>Senders</source>
        <translation>Відправники</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="69"/>
        <source>Recipients</source>
        <translation>Отримувачі</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="70"/>
        <source>Payers</source>
        <translation>Платники</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="71"/>
        <source>Carriers</source>
        <translation>Перевізники</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="72"/>
        <source>Cargos</source>
        <translation>Вантажі</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="73"/>
        <source>Auto</source>
        <translation>Авто</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="74"/>
        <source>Prizep</source>
        <translatorcomment>Причiп</translatorcomment>
        <translation type="unfinished">Причiп</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="76"/>
        <source>Name:</source>
        <translation>Найменування:</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Назва</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="112"/>
        <source>Add</source>
        <translation>Додати</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="119"/>
        <source>Delete record</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <location filename="../src/dictionaryForm.cpp" line="126"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <source>Windows styles missing</source>
        <translation type="obsolete">Стілі вікон не знайдені</translation>
    </message>
</context>
<context>
    <name>logEventsFormClass</name>
    <message>
        <location filename="../src/logEventsForm.cpp" line="26"/>
        <source>Logging</source>
        <translation>Журнал подій</translation>
    </message>
    <message>
        <location filename="../src/logEventsForm.cpp" line="34"/>
        <source>Datetime</source>
        <translation>Час подій</translation>
    </message>
    <message>
        <source>Confirm
 filter</source>
        <translation type="obsolete">Прийняти
фільтр</translation>
    </message>
    <message>
        <location filename="../src/logEventsForm.cpp" line="36"/>
        <source>Confirm
filter</source>
        <translation>Прийняти
фільтр</translation>
    </message>
    <message>
        <location filename="../src/logEventsForm.cpp" line="43"/>
        <source>Reset
filter</source>
        <translation>Скасувати
фільтр</translation>
    </message>
    <message>
        <location filename="../src/logEventsForm.cpp" line="50"/>
        <source>Date (start)</source>
        <translation>Дата (початок)</translation>
    </message>
    <message>
        <location filename="../src/logEventsForm.cpp" line="56"/>
        <source>Date (end)</source>
        <translation>Дата (кінець)</translation>
    </message>
    <message>
        <location filename="../src/logEventsForm.cpp" line="62"/>
        <source>Time(start)</source>
        <translation>Час (початок)</translation>
    </message>
    <message>
        <location filename="../src/logEventsForm.cpp" line="68"/>
        <source>Time(end)</source>
        <translation>Час (кінець)</translation>
    </message>
    <message>
        <location filename="../src/logEventsForm.cpp" line="75"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
</context>
<context>
    <name>loginWindowClass</name>
    <message>
        <location filename="../src/loginWindow.cpp" line="22"/>
        <source>Login</source>
        <translation>Логін</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="27"/>
        <location filename="../src/loginWindow.cpp" line="76"/>
        <source>Username</source>
        <translation>Користувач</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="31"/>
        <source>Incorrect name</source>
        <translation>Некоректне ім&apos;я</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="42"/>
        <location filename="../src/loginWindow.cpp" line="96"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="46"/>
        <source>Incorrect password</source>
        <translation>Невірний пароль</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="55"/>
        <source>Enter</source>
        <translation>Вхід</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="60"/>
        <location filename="../src/loginWindow.cpp" line="120"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="72"/>
        <source>Users</source>
        <translation>Користувачі</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="104"/>
        <source>Repeat</source>
        <translation>Повтор</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="116"/>
        <source>Confirm</source>
        <translation>Підтвердити</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="124"/>
        <source>Delete user</source>
        <translation>Видалити користувача</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="136"/>
        <source>Change user data</source>
        <translation>Редагувати користувача</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="278"/>
        <location filename="../src/loginWindow.cpp" line="419"/>
        <location filename="../src/loginWindow.cpp" line="431"/>
        <location filename="../src/loginWindow.cpp" line="443"/>
        <location filename="../src/loginWindow.cpp" line="451"/>
        <source>Prom-Soft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Windows styles missing</source>
        <translation type="obsolete">Стілі вікон не знайдені</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="235"/>
        <source>without spaces!</source>
        <translation>без пробілів!</translation>
    </message>
    <message>
        <source>the password should be without spaces</source>
        <translation type="obsolete">пароль не повинен мати пробіли</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="239"/>
        <source>re-enter password incorrect</source>
        <translation>паролі відрізняються</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="263"/>
        <source>User password changed: </source>
        <translation>Пароль змінений для користувача: </translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="274"/>
        <source>Cannot create a second administrator account!</source>
        <translation>Неможливо створити двох адміністраторів!</translation>
    </message>
    <message>
        <source>Weighbridge</source>
        <translation type="obsolete">Weighbridge</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="294"/>
        <source>Add user: </source>
        <translation>Доданий користувач: </translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="362"/>
        <source>Such a name is already exist</source>
        <translation>Таке ім&apos;я вже записано</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="415"/>
        <source>Do you want to delete record?</source>
        <translation>Видалити запис?</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="417"/>
        <source>Yes</source>
        <translation>Так</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="418"/>
        <source>No</source>
        <translation>Ні</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="427"/>
        <source>You cannot delete Admin user!</source>
        <translation>Неможливо видалити адміністратора!</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="439"/>
        <source>Delete error.</source>
        <translation>Помилка видалення.</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="448"/>
        <source>Deleted</source>
        <translation>Видалено</translation>
    </message>
    <message>
        <location filename="../src/loginWindow.cpp" line="457"/>
        <source>User deleted: </source>
        <translation>Видалений користувач: </translation>
    </message>
</context>
<context>
    <name>mysqlProccess</name>
    <message>
        <source>Weighbridge</source>
        <translation type="obsolete">Weighbridge</translation>
    </message>
    <message>
        <location filename="../src/DB_Driver.cpp" line="157"/>
        <source>Error connect with Remote Data Base</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DB_Driver.cpp" line="161"/>
        <source>AV-CONTROL PLUS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DB_Driver.cpp" line="590"/>
        <source>Photos</source>
        <translation type="unfinished">Фото</translation>
    </message>
</context>
<context>
    <name>names::Form_ttn</name>
    <message>
        <source>Windows styles missing</source>
        <translation type="obsolete">Стілі вікон не знайдені</translation>
    </message>
    <message>
        <source>Weighbridge</source>
        <translation type="obsolete">АРМ - ВВС-Сервіс</translation>
    </message>
</context>
<context>
    <name>names::MyClass</name>
    <message>
        <source>Weight</source>
        <translation type="obsolete">Вага</translation>
    </message>
    <message>
        <source>Report -</source>
        <translation type="obsolete">Звіт -</translation>
    </message>
    <message>
        <source>Login in: </source>
        <translation type="obsolete">Вхід користувача: </translation>
    </message>
    <message>
        <source>The program has not been activated
or the temporary key has expired!
Please contact VIS-Service.</source>
        <translation type="obsolete">Программа не активована
або збігає термін дії ключа!
Будь ласка, зверніться у ВІС-Сервіс.</translation>
    </message>
    <message>
        <source>Weighbridge</source>
        <translation type="obsolete">АРМ - ВВС-Сервіс</translation>
    </message>
    <message>
        <source>Incorrect password</source>
        <translation type="obsolete">Невірний пароль</translation>
    </message>
    <message>
        <source>Windows styles missing</source>
        <translation type="obsolete">Стілі вікон не знайдені</translation>
    </message>
    <message>
        <source>The code is wrong!
Please contact VIS-Service.</source>
        <translation type="obsolete">Код не дійсний!
Зверніться до ВІС-Сервіс.</translation>
    </message>
    <message>
        <source>Enter the activation code for programm.</source>
        <translation type="obsolete">Введіть код активації ПЗ.</translation>
    </message>
    <message>
        <source>Enter the service code.</source>
        <translation type="obsolete">Введіть сервісний код.</translation>
    </message>
    <message>
        <source>Click to visit our website.</source>
        <translation type="obsolete">Перехід до нашого сайту.</translation>
    </message>
    <message>
        <source>Service</source>
        <translation type="obsolete">Службові</translation>
    </message>
    <message>
        <source>Dictionary</source>
        <translation type="obsolete">Довідники</translation>
    </message>
    <message>
        <source>Change user</source>
        <translation type="obsolete">Зміна користувача</translation>
    </message>
    <message>
        <source>Add user</source>
        <translation type="obsolete">Додати користувача</translation>
    </message>
    <message>
        <source>Event log</source>
        <translation type="obsolete">Журнал подій</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="obsolete">Налаштування</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Допомога</translation>
    </message>
    <message>
        <source>User manual - F1</source>
        <translation type="obsolete">Довідник користувача F1</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="obsolete">Про ПЗ</translation>
    </message>
    <message>
        <source>Activation</source>
        <translation type="obsolete">Активація</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="obsolete">Мова</translation>
    </message>
    <message>
        <source>Search vehicles by number</source>
        <translation type="obsolete">Пошук авто за номером</translation>
    </message>
    <message>
        <source>Vehicles ¹</source>
        <translation type="obsolete">Авто №</translation>
    </message>
    <message>
        <source>Enter the vehicles number</source>
        <translation type="obsolete">Введіть номер авто</translation>
    </message>
    <message>
        <source>Num fragment</source>
        <translation type="obsolete">Фрагм. номеру</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="obsolete">Пошук</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="obsolete">Скинути</translation>
    </message>
    <message>
        <source>Print</source>
        <translation type="obsolete">Друк</translation>
    </message>
    <message>
        <source>Cargo accept</source>
        <translation type="obsolete">Прийом</translation>
    </message>
    <message>
        <source>Cargo send</source>
        <translation type="obsolete">Відправлення</translation>
    </message>
    <message>
        <source>Press to accept the vehicle.</source>
        <translation type="obsolete">Натисніть для прийомки авто.</translation>
    </message>
    <message>
        <source>Press to send the vehicle</source>
        <translation type="obsolete">Натисніть для відправки авто</translation>
    </message>
    <message>
        <source>Registry</source>
        <translation type="obsolete">Журнал</translation>
    </message>
    <message>
        <source>To view the history of weighing.</source>
        <translation type="obsolete">Подивитись проведені зважування.</translation>
    </message>
    <message>
        <source>Delete
record</source>
        <translation type="obsolete">Видалити</translation>
    </message>
    <message>
        <source>Deletes the selected entry in the table.</source>
        <translation type="obsolete">Видалити обраний запис з таблиці.</translation>
    </message>
    <message>
        <source>To enter the settings menu.</source>
        <translation type="obsolete">Вхід до налаштувань.</translation>
    </message>
    <message>
        <source>To edit dictionaries.</source>
        <translation type="obsolete">Редагувати довідники.</translation>
    </message>
    <message>
        <source>Zero &gt;0&lt;</source>
        <translation type="obsolete">Обнулення  &gt;0&lt;</translation>
    </message>
    <message>
        <source>WEIGHT</source>
        <translation type="obsolete">МАСА</translation>
    </message>
    <message>
        <source>
kg</source>
        <translation type="obsolete">
кг</translation>
    </message>
    <message>
        <source>CAM 1</source>
        <translation type="obsolete">Камера 1</translation>
    </message>
    <message>
        <source>View weighing photo ¹1 cam ¹1</source>
        <translation type="obsolete">Перегляд фото 1 камери 1</translation>
    </message>
    <message>
        <source>CAM 2</source>
        <translation type="obsolete">Камера 2</translation>
    </message>
    <message>
        <source>View weighing photo ¹2 cam ¹1</source>
        <translation type="obsolete">Перегляд фото 2 камери 1</translation>
    </message>
    <message>
        <source>CAM 3</source>
        <translation type="obsolete">Камера 3</translation>
    </message>
    <message>
        <source>View weighing photo ¹1 cam ¹2</source>
        <translation type="obsolete">Перегляд фото 1 камери 2</translation>
    </message>
    <message>
        <source>CAM 4</source>
        <translation type="obsolete">Камера 4</translation>
    </message>
    <message>
        <source>View weighing photo ¹2 cam ¹2</source>
        <translation type="obsolete">Перегляд фото 2 камери 2</translation>
    </message>
    <message>
        <source>View weighing photo ¹1 cam ¹3</source>
        <translation type="obsolete">Перегляд фото 1 камери 3</translation>
    </message>
    <message>
        <source>View weighing photo ¹1 cam ¹4</source>
        <translation type="obsolete">Перегляд фото 1 камери 4</translation>
    </message>
    <message>
        <source>View weighing photo ¹2 cam ¹3</source>
        <translation type="obsolete">Перегляд фото 2 камери 3</translation>
    </message>
    <message>
        <source>View weighing photo ¹2 cam ¹4</source>
        <translation type="obsolete">Перегляд фото 2 камери 4</translation>
    </message>
    <message>
        <source>Click on the traffic light to change its state.</source>
        <translation type="obsolete">Натисніть для зміни стану світлофора.</translation>
    </message>
    <message>
        <source>Select the entry to generate the report!</source>
        <translation type="obsolete">Оберіть запис для формування звіту!</translation>
    </message>
    <message>
        <source>Select record to start!</source>
        <translation type="obsolete">Спочатку оберіть запис!</translation>
    </message>
    <message>
        <source>Data not available</source>
        <translation type="obsolete">Дані відсутні</translation>
    </message>
    <message>
        <source>Microsoft Office - Excel is not installed!</source>
        <translation type="obsolete">Microsoft Office - Excel не встановлено!</translation>
    </message>
    <message>
        <source>From scales</source>
        <translation type="obsolete">З вагопроцесору</translation>
    </message>
    <message>
        <source>Manually</source>
        <translation type="obsolete">Вручну</translation>
    </message>
    <message>
        <source>Select the entry to delete!</source>
        <translation type="obsolete">Оберіть запис для видалення!</translation>
    </message>
    <message>
        <source>Do you want to remove the entry?</source>
        <translation type="obsolete">Дійсно видалити запис?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Так</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Ні</translation>
    </message>
    <message>
        <source>Removed the record. Trailer num:</source>
        <translation type="obsolete">Видалений запис. Номер причепу:</translation>
    </message>
    <message>
        <source>Removed the record. Vehicle num:</source>
        <translation type="obsolete">Видалений запис. Номер авто:</translation>
    </message>
    <message>
        <source>This software - an automatic workstation
weigher (ARM) is intended to control and account
weighing for auto weighing using operator-weigher. 


This software is the property of
the company VIS-Service.</source>
        <translation type="obsolete">Це програмнє забеспечення - автоматичне робоче місце
вагаря (АРМ) - призначено для контролю та обліку
зважувань на автомобільних вагах оператором-вагарем.


Є власністю компанії ВІС-Сервіс.</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Закрити</translation>
    </message>
    <message>
        <source>Select the entry first!</source>
        <translation type="obsolete">Спочатку оберіть запис!</translation>
    </message>
    <message>
        <source>Communication with scales missing!</source>
        <translation type="obsolete">Немає зв&apos;язку з вагами!</translation>
    </message>
    <message>
        <source>Trailer accepted:</source>
        <translation type="obsolete">Прийнятий причеп:</translation>
    </message>
    <message>
        <source>Vehicle accepted:</source>
        <translation type="obsolete">Прийняте авто:</translation>
    </message>
    <message>
        <source>Trailer send:</source>
        <translation type="obsolete">Відправлений причіп:</translation>
    </message>
    <message>
        <source>Vehicle send:</source>
        <translation type="obsolete">Відправлене авто:</translation>
    </message>
    <message>
        <source>Error. There are no photos to save!</source>
        <translation type="obsolete">Помилка. Фото відсутні!</translation>
    </message>
    <message>
        <source>Data according to the filters was not found</source>
        <translation type="obsolete">Даних за обраними фільтрами немає</translation>
    </message>
    <message>
        <source>Photos</source>
        <translation type="obsolete">Фото</translation>
    </message>
    <message>
        <source>Entered the settings</source>
        <translation type="obsolete">Вхід до налаштувань</translation>
    </message>
    <message>
        <source>Entered the registry:</source>
        <translation type="obsolete">Вхід до журналу:</translation>
    </message>
    <message>
        <source>Closed the registry:</source>
        <translation type="obsolete">Вихід з журналу:</translation>
    </message>
    <message>
        <source>Closed the settings:</source>
        <translation type="obsolete">Вихід з налаштувань:</translation>
    </message>
    <message>
        <source>by Admin</source>
        <translation type="obsolete">під Адміністратором</translation>
    </message>
    <message>
        <source>Do you want to weigh the vehicle?</source>
        <translation type="obsolete">Зважити авто?</translation>
    </message>
    <message>
        <source>Zero error</source>
        <translation type="obsolete">Помилка обнулення</translation>
    </message>
    <message>
        <source>COM-port closed</source>
        <translation type="obsolete">COM-порт закритий</translation>
    </message>
    <message>
        <source>Set to zero: </source>
        <translation type="obsolete">Обнулення: </translation>
    </message>
    <message>
        <source>Entered dictionaries: </source>
        <translation type="obsolete">Вхід у довідники: </translation>
    </message>
    <message>
        <source>Dictionaries closed: </source>
        <translation type="obsolete">Вихід з довідників: </translation>
    </message>
    <message>
        <source>Enter the name</source>
        <translation type="obsolete">Введіть найменування</translation>
    </message>
    <message>
        <source>Database error!</source>
        <translation type="obsolete">Помилка БД!</translation>
    </message>
    <message>
        <source>The name is already exist!</source>
        <translation type="obsolete">Таке найменування вже є!</translation>
    </message>
    <message>
        <source>The record is successfully saved.</source>
        <translation type="obsolete">Запис збережений.</translation>
    </message>
    <message>
        <source>Added to dictionary: </source>
        <translation type="obsolete">Додано у довідник:</translation>
    </message>
    <message>
        <source>The entry was successfully deleted from the database!</source>
        <translation type="obsolete">Запис видалений з БД!</translation>
    </message>
    <message>
        <source>Deleted from dictionaries: </source>
        <translation type="obsolete">Видалений з довідника: </translation>
    </message>
    <message>
        <source>Weight data from  </source>
        <translation type="obsolete">Зважування від </translation>
    </message>
    <message>
        <source>Total gross: </source>
        <translation type="obsolete">Сума брутто: </translation>
    </message>
    <message>
        <source>Total tare: </source>
        <translation type="obsolete">Сума тара: </translation>
    </message>
    <message>
        <source>Total net: </source>
        <translation type="obsolete">Сума нетто: </translation>
    </message>
    <message>
        <source>Responsible: </source>
        <translation type="obsolete">Відповідальний: </translation>
    </message>
    <message>
        <source>Date/time </source>
        <translation type="obsolete">Дата/час </translation>
    </message>
    <message>
        <source>Vehicle
number</source>
        <translation type="obsolete">Номер
авто</translation>
    </message>
    <message>
        <source>Sender</source>
        <translation type="obsolete">Відправник</translation>
    </message>
    <message>
        <source>Recipient</source>
        <translation type="obsolete">Отримувач</translation>
    </message>
    <message>
        <source>Payer</source>
        <translation type="obsolete">Платник</translation>
    </message>
    <message>
        <source>Carrier</source>
        <translation type="obsolete">Перевізник</translation>
    </message>
    <message>
        <source>Cargo</source>
        <translation type="obsolete">Вантаж</translation>
    </message>
    <message>
        <source>Cargo value</source>
        <translation type="obsolete">Ціна вантажу</translation>
    </message>
    <message>
        <source>Gross</source>
        <translation type="obsolete">Брутто</translation>
    </message>
    <message>
        <source>Tare</source>
        <translation type="obsolete">Тара</translation>
    </message>
    <message>
        <source>Net</source>
        <translation type="obsolete">Нетто</translation>
    </message>
    <message>
        <source>Date
time</source>
        <translation type="obsolete">Дата
час</translation>
    </message>
    <message>
        <source>Waybill</source>
        <translation type="obsolete">Накл.</translation>
    </message>
    <message>
        <source>User</source>
        <translation type="obsolete">Прийняв</translation>
    </message>
    <message>
        <source>Trailer
number</source>
        <translation type="obsolete">Номер
причепа</translation>
    </message>
    <message>
        <source>Gross
(trailer)</source>
        <translation type="obsolete">Брутто
(причіп)</translation>
    </message>
    <message>
        <source>Tare
(trailer)</source>
        <translation type="obsolete">Тара
(причіп)</translation>
    </message>
    <message>
        <source>Net
(trailer)</source>
        <translation type="obsolete">Нетто
(причіп)</translation>
    </message>
    <message>
        <source>Tara
enter</source>
        <translation type="obsolete">Ввод
тара</translation>
    </message>
    <message>
        <source>Entries by specified criteria was not found!</source>
        <translation type="obsolete">Записів за обраним критерієм немає!</translation>
    </message>
    <message>
        <source>Cam is turn off</source>
        <translation type="obsolete">Камера вимкнена</translation>
    </message>
    <message>
        <source>There is no data to print!</source>
        <translation type="obsolete">Немає даних для друку!</translation>
    </message>
    <message>
        <source>vehicles accepted</source>
        <translation type="obsolete">прийняті авто</translation>
    </message>
    <message>
        <source>vehicles sended</source>
        <translation type="obsolete">відправлені авто</translation>
    </message>
    <message>
        <source>Weight data - </source>
        <translation type="obsolete">Дані зважувань - </translation>
    </message>
    <message>
        <source>Date time </source>
        <translation type="obsolete">Дата час </translation>
    </message>
    <message>
        <source>Start server</source>
        <translation type="obsolete">Старт серверу</translation>
    </message>
    <message>
        <source>Stop server</source>
        <translation type="obsolete">Стоп серверу</translation>
    </message>
    <message>
        <source>User: </source>
        <translation type="obsolete">Користувач: </translation>
    </message>
    <message>
        <source>Scales link status </source>
        <translation type="obsolete">Зв&apos;язок з вагами </translation>
    </message>
    <message>
        <source>Connected</source>
        <translation type="obsolete">З&apos;єднано</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="obsolete">Відсутній</translation>
    </message>
    <message>
        <source>Do you want to exit the program?</source>
        <translation type="obsolete">Закрити програму?</translation>
    </message>
    <message>
        <source>Exit program</source>
        <translation type="obsolete">Вихід</translation>
    </message>
    <message>
        <source>Attempt to send e-mail successfully </source>
        <translation type="obsolete">Спроба відправлення пошти успішна </translation>
    </message>
    <message>
        <source>Sended to </source>
        <translation type="obsolete">Відправлено на </translation>
    </message>
    <message>
        <source>Attempt to send e-mail unsuccessful </source>
        <translation type="obsolete">Спроба відправлення пошти неуспішна </translation>
    </message>
    <message>
        <source>The letter is not sent to destination. Check your mail settings and internet connection.</source>
        <translation type="obsolete">Лист не відправляється до місця призначення. Перевірте налаштування електронної пошти і підключення до інтернету.</translation>
    </message>
    <message>
        <source>The weight on the weighing platform is less than the minimum </source>
        <translation type="obsolete">Вага на платформі меньше мінімальної </translation>
    </message>
    <message>
        <source>The vehicle is not correctly standing on the weighing platform!</source>
        <translation type="obsolete">Авто не коректно встановлено на ваговій платформі!</translation>
    </message>
    <message>
        <source>Positioning</source>
        <translation type="obsolete">Позиціонування</translation>
    </message>
</context>
<context>
    <name>names::SettingsForm</name>
    <message>
        <source>Settings</source>
        <translation type="obsolete">Налаштування</translation>
    </message>
    <message>
        <source>COM-port settings</source>
        <translation type="obsolete">Налаштування COM</translation>
    </message>
    <message>
        <source>Weighing proc. model</source>
        <translation type="obsolete">Модель вагопроцесору</translation>
    </message>
    <message>
        <source>Adress WP</source>
        <translation type="obsolete">Адреса ВП</translation>
    </message>
    <message>
        <source>Koef.</source>
        <translation type="obsolete">Коеф.</translation>
    </message>
    <message>
        <source>Baud rate</source>
        <translation type="obsolete">Швидкість</translation>
    </message>
    <message>
        <source>End of frame</source>
        <translation type="obsolete">Кінець посилки</translation>
    </message>
    <message>
        <source>Radio modem</source>
        <translation type="obsolete">Радіомодем</translation>
    </message>
    <message>
        <source>Use USB</source>
        <translation type="obsolete">Використання USB</translation>
    </message>
    <message>
        <source>Switch ON</source>
        <translation type="obsolete">Ввести в дію</translation>
    </message>
    <message>
        <source>Show traffic light</source>
        <translation type="obsolete">Відобразити світлофор</translation>
    </message>
    <message>
        <source>Use BC-01</source>
        <translation type="obsolete">Використ. БК-01</translation>
    </message>
    <message>
        <source>WP Model via BC-01</source>
        <translation type="obsolete">Модель ВП для зв&apos;язку</translation>
    </message>
    <message>
        <source>Input delay, ms</source>
        <translation type="obsolete">Вхідна затримка, мс</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Зберегти</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Закрити</translation>
    </message>
    <message>
        <source>UDP params</source>
        <translation type="obsolete">UDP параметри</translation>
    </message>
    <message>
        <source>IP-adress</source>
        <translation type="obsolete">IP-адреса</translation>
    </message>
    <message>
        <source>Src. port</source>
        <translation type="obsolete">Порт відправки</translation>
    </message>
    <message>
        <source>Dest. port</source>
        <translation type="obsolete">Порт отримки</translation>
    </message>
    <message>
        <source>IP cams</source>
        <translation type="obsolete">Відеокамери</translation>
    </message>
    <message>
        <source>IP cam settings</source>
        <translation type="obsolete">Налаштування IP камер</translation>
    </message>
    <message>
        <source>Port</source>
        <translation type="obsolete">Порт</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation type="obsolete">Задіяти</translation>
    </message>
    <message>
        <source>Photo enable</source>
        <translation type="obsolete">Фотофіксація</translation>
    </message>
    <message>
        <source>Photo preview enable</source>
        <translation type="obsolete">Перегляд фото</translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="obsolete">Логін</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">Пароль</translation>
    </message>
    <message>
        <source>Server autostart</source>
        <translation type="obsolete">Автостарт серверу</translation>
    </message>
    <message>
        <source>Email settings</source>
        <translation type="obsolete">Налаштування пошти</translation>
    </message>
    <message>
        <source>Send interval, min</source>
        <translation type="obsolete">Інтервал відправлення, мін</translation>
    </message>
    <message>
        <source>Addressee</source>
        <translation type="obsolete">Адресат</translation>
    </message>
    <message>
        <source>Use SSL</source>
        <translation type="obsolete">Використання SSL</translation>
    </message>
    <message>
        <source>Internet connection</source>
        <translation type="obsolete">Інтернет з&apos;єднання</translation>
    </message>
    <message>
        <source>Scale</source>
        <translation type="obsolete">Ваги</translation>
    </message>
    <message>
        <source>Checking</source>
        <translation type="obsolete">Перевірка</translation>
    </message>
    <message>
        <source>Internet connection is available.</source>
        <translation type="obsolete">Інтернет з&apos;єднання присутнє.</translation>
    </message>
    <message>
        <source>Internet connection is not available.</source>
        <translation type="obsolete">Інтернет з&apos;єднання відсутнє.</translation>
    </message>
</context>
<context>
    <name>names::reportFormClass</name>
    <message>
        <source>Registry</source>
        <translation type="obsolete">Журнал</translation>
    </message>
    <message>
        <source>Filter params</source>
        <translation type="obsolete">Параметри фільтра</translation>
    </message>
    <message>
        <source>Vehicle num</source>
        <translation type="obsolete">Номер авто</translation>
    </message>
    <message>
        <source>Confirm
 filter</source>
        <translation type="obsolete">Схвалити\n фільтр</translation>
    </message>
    <message>
        <source>Reset
filter</source>
        <translation type="obsolete">Скинути\nфільтр</translation>
    </message>
    <message>
        <source>Accepting</source>
        <translation type="obsolete">Прийом</translation>
    </message>
    <message>
        <source>Sending</source>
        <translation type="obsolete">Відправлення</translation>
    </message>
    <message>
        <source>Date (start)</source>
        <translation type="obsolete">Дата (початок)</translation>
    </message>
    <message>
        <source>Date (end)</source>
        <translation type="obsolete">Дата (кінець)</translation>
    </message>
    <message>
        <source>Time (start)</source>
        <translation type="obsolete">Час (початок)</translation>
    </message>
    <message>
        <source>Time (end)</source>
        <translation type="obsolete">Час (кінець)</translation>
    </message>
    <message>
        <source>Sender</source>
        <translation type="obsolete">Відправник</translation>
    </message>
    <message>
        <source>Recipient</source>
        <translation type="obsolete">Отримувач</translation>
    </message>
    <message>
        <source>Carrier</source>
        <translation type="obsolete">Перевізник</translation>
    </message>
    <message>
        <source>Scales num</source>
        <translation type="obsolete">Номер вагів</translation>
    </message>
    <message>
        <source>Cargo</source>
        <translation type="obsolete">Вантаж</translation>
    </message>
    <message>
        <source>Payer</source>
        <translation type="obsolete">Платник</translation>
    </message>
    <message>
        <source>Create &quot;ÒÒÍ&quot;</source>
        <translation type="obsolete">Нова ТТН</translation>
    </message>
    <message>
        <source>Create
 waybill</source>
        <translation type="obsolete">Друк\nнакладних</translation>
    </message>
    <message>
        <source>Export to
Excel</source>
        <translation type="obsolete">Експорт\nв Excel</translation>
    </message>
    <message>
        <source>Print
 table</source>
        <translation type="obsolete">Друк\nтаблиці</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Закрити</translation>
    </message>
    <message>
        <source>Send
e-mail</source>
        <translation type="obsolete">Відправити\n e-mail</translation>
    </message>
    <message>
        <source>The number of prints on the A4</source>
        <translation type="obsolete">Кількість накладних на аркушу А4</translation>
    </message>
    <message>
        <source>Set number</source>
        <translation type="obsolete">Встановити номер</translation>
    </message>
    <message>
        <source>Print</source>
        <translation type="obsolete">Друк</translation>
    </message>
    <message>
        <source>Cargo value</source>
        <translation type="obsolete">Ціна вантажу</translation>
    </message>
    <message>
        <source>Gross</source>
        <translation type="obsolete">Брутто</translation>
    </message>
    <message>
        <source>Tare</source>
        <translation type="obsolete">Тара</translation>
    </message>
    <message>
        <source>Net</source>
        <translation type="obsolete">Нетто</translation>
    </message>
    <message>
        <source>Datetime
accept</source>
        <translation type="obsolete">Час\nприйому</translation>
    </message>
    <message>
        <source>Accept
user</source>
        <translation type="obsolete">Прийняв</translation>
    </message>
    <message>
        <source>Sender
user</source>
        <translation type="obsolete">Відправив</translation>
    </message>
    <message>
        <source>Waybill</source>
        <translation type="obsolete">Накл.</translation>
    </message>
    <message>
        <source>Photo</source>
        <translation type="obsolete">Фото</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Зберегти</translation>
    </message>
    <message>
        <source>Vehicle number</source>
        <translation type="obsolete">Номер авто</translation>
    </message>
    <message>
        <source>Shipment details</source>
        <translation type="obsolete">Деталі відправки</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Назва</translation>
    </message>
    <message>
        <source>unit</source>
        <translation type="obsolete">од.вим.</translation>
    </message>
    <message>
        <source>Count</source>
        <translation type="obsolete">Кіл-ть</translation>
    </message>
    <message>
        <source>Price, uah</source>
        <translation type="obsolete">Ціна, грн</translation>
    </message>
    <message>
        <source>Sum, uah</source>
        <translation type="obsolete">Сума, грн</translation>
    </message>
    <message>
        <source>Sum +VAT, uah</source>
        <translation type="obsolete">Сума з ПДВ, грн</translation>
    </message>
    <message>
        <source>tons</source>
        <translation type="obsolete">тон</translation>
    </message>
    <message>
        <source>VAT</source>
        <translation type="obsolete">ПДВ</translation>
    </message>
    <message>
        <source>Accept user</source>
        <translation type="obsolete">Прийняв</translation>
    </message>
    <message>
        <source>Sender user</source>
        <translation type="obsolete">Відправив</translation>
    </message>
    <message>
        <source>Report</source>
        <translation type="obsolete">Звіт</translation>
    </message>
    <message>
        <source>No data to e-mail</source>
        <translation type="obsolete">Немає даних поштової відправки</translation>
    </message>
    <message>
        <source>Weight mail</source>
        <translation type="obsolete">Вагова</translation>
    </message>
    <message>
        <source>Report - </source>
        <translation type="obsolete">Звіт за </translation>
    </message>
</context>
<context>
    <name>optionalDevices</name>
    <message>
        <location filename="../forms/optionaldevices.ui" line="26"/>
        <source>Socket-2</source>
        <translation>Управління реле</translation>
    </message>
    <message>
        <location filename="../forms/optionaldevices.ui" line="38"/>
        <source>Socket2 #1</source>
        <translation>Socket2 #1</translation>
    </message>
    <message>
        <location filename="../forms/optionaldevices.ui" line="50"/>
        <location filename="../forms/optionaldevices.ui" line="170"/>
        <location filename="../forms/optionaldevices.ui" line="290"/>
        <source>Relay 0</source>
        <translation>Реле 0</translation>
    </message>
    <message>
        <location filename="../forms/optionaldevices.ui" line="62"/>
        <location filename="../forms/optionaldevices.ui" line="114"/>
        <location filename="../forms/optionaldevices.ui" line="182"/>
        <location filename="../forms/optionaldevices.ui" line="234"/>
        <location filename="../forms/optionaldevices.ui" line="302"/>
        <location filename="../forms/optionaldevices.ui" line="354"/>
        <source>ON</source>
        <translation>Вкл.</translation>
    </message>
    <message>
        <location filename="../forms/optionaldevices.ui" line="75"/>
        <location filename="../forms/optionaldevices.ui" line="101"/>
        <location filename="../forms/optionaldevices.ui" line="195"/>
        <location filename="../forms/optionaldevices.ui" line="221"/>
        <location filename="../forms/optionaldevices.ui" line="315"/>
        <location filename="../forms/optionaldevices.ui" line="341"/>
        <source>OFF</source>
        <translation>Викл.</translation>
    </message>
    <message>
        <location filename="../forms/optionaldevices.ui" line="89"/>
        <location filename="../forms/optionaldevices.ui" line="209"/>
        <location filename="../forms/optionaldevices.ui" line="329"/>
        <source>Relay 1</source>
        <translation>Реле 1</translation>
    </message>
    <message>
        <location filename="../forms/optionaldevices.ui" line="141"/>
        <location filename="../forms/optionaldevices.ui" line="261"/>
        <location filename="../forms/optionaldevices.ui" line="381"/>
        <source>Link:  </source>
        <translation>З&apos;єднання: </translation>
    </message>
    <message>
        <location filename="../forms/optionaldevices.ui" line="158"/>
        <source>Socket2 #2</source>
        <translation>Socket2 #2</translation>
    </message>
    <message>
        <location filename="../forms/optionaldevices.ui" line="278"/>
        <source>Socket2 #3</source>
        <translation>Socket2 #3</translation>
    </message>
    <message>
        <location filename="../src/optionaldevices.cpp" line="99"/>
        <location filename="../src/optionaldevices.cpp" line="111"/>
        <location filename="../src/optionaldevices.cpp" line="123"/>
        <source>Connected</source>
        <translation>Підключено</translation>
    </message>
    <message>
        <location filename="../src/optionaldevices.cpp" line="104"/>
        <location filename="../src/optionaldevices.cpp" line="116"/>
        <location filename="../src/optionaldevices.cpp" line="128"/>
        <source>Not connected</source>
        <translation>Не підключено</translation>
    </message>
</context>
<context>
    <name>options</name>
    <message>
        <location filename="../forms/options.ui" line="38"/>
        <source>Photographic fixation module</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="50"/>
        <location filename="../forms/options.ui" line="131"/>
        <location filename="../forms/options.ui" line="175"/>
        <location filename="../forms/options.ui" line="219"/>
        <location filename="../forms/options.ui" line="263"/>
        <location filename="../forms/options.ui" line="307"/>
        <location filename="../forms/options.ui" line="351"/>
        <location filename="../forms/options.ui" line="429"/>
        <location filename="../forms/options.ui" line="491"/>
        <source>Activate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="68"/>
        <location filename="../forms/options.ui" line="149"/>
        <location filename="../forms/options.ui" line="193"/>
        <location filename="../forms/options.ui" line="237"/>
        <location filename="../forms/options.ui" line="281"/>
        <location filename="../forms/options.ui" line="325"/>
        <location filename="../forms/options.ui" line="369"/>
        <location filename="../forms/options.ui" line="447"/>
        <location filename="../forms/options.ui" line="478"/>
        <source>enter key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="82"/>
        <source>Your number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="102"/>
        <source>____________</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="119"/>
        <source>Waste definition module</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="163"/>
        <source>Control module - traffic lights</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="207"/>
        <source>Duplicative scoreboards module</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="251"/>
        <source>Control module - barriers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="295"/>
        <source>Control module - positioning sensors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="339"/>
        <source>Program activation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="386"/>
        <source>Program fist run :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="417"/>
        <source>Rfid module</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="461"/>
        <source>Telegramm-Bot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="26"/>
        <source>Software modules activation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/options.ui" line="404"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>registration_rfid</name>
    <message utf8="true">
        <location filename="../forms/registration_rfid.ui" line="14"/>
        <source>Модуль RFID</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/registration_rfid.ui" line="37"/>
        <source>Регистрационные данные</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/registration_rfid.ui" line="43"/>
        <source>Код карты</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/registration_rfid.ui" line="97"/>
        <source>Добавить</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/registration_rfid.ui" line="116"/>
        <source>Изменить</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/registration_rfid.ui" line="135"/>
        <source>Удалить</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/registration_rfid.ui" line="154"/>
        <source>Закрыть</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/registration_rfid.cpp" line="556"/>
        <source>Data for today is not available.</source>
        <translation type="unfinished">Даних на сьогодні немає.</translation>
    </message>
    <message>
        <location filename="../src/registration_rfid.cpp" line="560"/>
        <source>Prom-Soft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>AV-Control</source>
        <translation type="obsolete">AV-Control</translation>
    </message>
</context>
<context>
    <name>reportFormClass</name>
    <message>
        <location filename="../src/reportFormClass.cpp" line="46"/>
        <source>Registry</source>
        <translation>Журнал</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="51"/>
        <source>Filter params</source>
        <translation>Параметри фільтра</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="53"/>
        <source>Vehicle num</source>
        <translation>Номер авто</translation>
    </message>
    <message>
        <source>Confirm
 filter</source>
        <translation type="obsolete">Схвалити
фільтр</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="69"/>
        <source>Reset
filter</source>
        <translation>Скинути
фільтр</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="76"/>
        <source>Accepting</source>
        <translation>Прийом</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="80"/>
        <source>Sending</source>
        <translation>Відправлення</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="83"/>
        <source>Date (start)</source>
        <translation>Дата (початок)</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="112"/>
        <source>Date (end)</source>
        <translation>Дата (кінець)</translation>
    </message>
    <message>
        <source>Time (start)</source>
        <translation type="obsolete">Час (початок)</translation>
    </message>
    <message>
        <source>Time (end)</source>
        <translation type="obsolete">Час (кінець)</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="133"/>
        <source>Time(start)</source>
        <translation>Час(початок)</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="140"/>
        <source>Time(end)</source>
        <translation>Час(кінець)</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="147"/>
        <location filename="../src/reportFormClass.cpp" line="611"/>
        <location filename="../src/reportFormClass.cpp" line="715"/>
        <source>Sender</source>
        <translation>Відправник</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="155"/>
        <location filename="../src/reportFormClass.cpp" line="612"/>
        <location filename="../src/reportFormClass.cpp" line="724"/>
        <source>Recipient</source>
        <translation>Отримувач</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="163"/>
        <location filename="../src/reportFormClass.cpp" line="614"/>
        <location filename="../src/reportFormClass.cpp" line="721"/>
        <source>Carrier</source>
        <translation>Перевізник</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="171"/>
        <source>Scales num</source>
        <translation>Номер вагів</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="178"/>
        <location filename="../src/reportFormClass.cpp" line="615"/>
        <source>Cargo</source>
        <translation>Вантаж</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="186"/>
        <location filename="../src/reportFormClass.cpp" line="613"/>
        <location filename="../src/reportFormClass.cpp" line="718"/>
        <source>Payer</source>
        <translation>Платник</translation>
    </message>
    <message>
        <source>Create &quot;ÒÒÍ&quot;</source>
        <translation type="obsolete">Нова ТТН</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="246"/>
        <source>Create TTN</source>
        <translation>Нова ТТН</translation>
    </message>
    <message>
        <source>Create
 waybill</source>
        <translation type="obsolete">Друк
накладних</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="265"/>
        <source>Export to
Excel</source>
        <translation>Експорт
в Excel</translation>
    </message>
    <message>
        <source>Print
 table</source>
        <translation type="obsolete">Друк
таблиці</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="62"/>
        <source>Confirm
filter</source>
        <translation>Прийняти
фільтр</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="255"/>
        <source>Create
waybill</source>
        <translation>Друк
накладних</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="276"/>
        <source>Export to
Excel (+sor)</source>
        <translation>Експорт
в Excel (+засмiч.)</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="284"/>
        <source>Print
table</source>
        <translation>Друк
таблиці</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="294"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="304"/>
        <source>Save all
photos</source>
        <translation type="unfinished">Відправляти фото</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="314"/>
        <source>Send
e-mail</source>
        <translation>Відправити
 e-mail</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="320"/>
        <source>Save all photos</source>
        <translation type="unfinished">Відправляти фото</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="368"/>
        <source>The number of prints on the A4</source>
        <translation>Кількість накладних на аркушу А4</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="373"/>
        <source>Set number</source>
        <translation>Встановити номер</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="384"/>
        <source>Print</source>
        <translation>Друк</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="610"/>
        <source>Vehicle
number</source>
        <translation>Номер
авто</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="616"/>
        <source>Cargo value</source>
        <translation>Ціна вантажу</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="617"/>
        <source>Gross</source>
        <translation>Брутто</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="618"/>
        <source>Tare</source>
        <translation>Тара</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="619"/>
        <source>Net</source>
        <translation>Нетто</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="620"/>
        <source>Datetime
accept</source>
        <translation>Дата/час
прийому</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="621"/>
        <source>Accept
user</source>
        <translation>Прийняв</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="622"/>
        <source>Datetime
send</source>
        <translation>Дата/час
відправки</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="623"/>
        <source>Sender
user</source>
        <translation>Відправив</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="624"/>
        <location filename="../src/reportFormClass.cpp" line="712"/>
        <source>Waybill</source>
        <translation>Накл.</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="625"/>
        <source>Trailer
number</source>
        <translation>Номер
причепа</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="626"/>
        <source>Gross
(trailer)</source>
        <translation>Брутто
(причіп)</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="627"/>
        <source>Tare
(trailer)</source>
        <translation>Тара
(причіп)</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="628"/>
        <source>Net
(trailer)</source>
        <translation>Нетто
(причіп)</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="629"/>
        <source>Photo</source>
        <translation>Фото</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="630"/>
        <source>Save</source>
        <translation>Зберегти</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="631"/>
        <source>Tara
enter</source>
        <translation>Введення
 тари</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="663"/>
        <source>Preview</source>
        <translation>Попередній перегляд</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="727"/>
        <source>Vehicle number</source>
        <translation>Номер авто</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="730"/>
        <source>Shipment details</source>
        <translation>Деталі відправки</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="733"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="735"/>
        <source>Unit</source>
        <translation>Од.вим.</translation>
    </message>
    <message>
        <source>unit</source>
        <translation type="obsolete">од.вим.</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="737"/>
        <source>Count</source>
        <translation>Кіл-ть</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="739"/>
        <source>Price, uah</source>
        <translation>Ціна, грн</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="741"/>
        <source>Sum, uah</source>
        <translation>Сума, грн</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="743"/>
        <source>Sum +VAT, uah</source>
        <translation>Сума з ПДВ, грн</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="746"/>
        <source>tons</source>
        <translation>тон</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="752"/>
        <source>VAT</source>
        <translation>ПДВ</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="754"/>
        <source>Accept user</source>
        <translation>Прийняв</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="756"/>
        <source>Sender user</source>
        <translation>Відправив</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="776"/>
        <source>Report</source>
        <translation>Звіт</translation>
    </message>
    <message>
        <location filename="../src/reportFormClass.cpp" line="777"/>
        <source>No data to e-mail</source>
        <translation>Немає даних поштової відправки</translation>
    </message>
    <message>
        <source>Weight mail</source>
        <translation type="obsolete">Вагова</translation>
    </message>
    <message>
        <source>Report - </source>
        <translation type="obsolete">Звіт за </translation>
    </message>
</context>
<context>
    <name>reports_rfid</name>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="35"/>
        <source>Журнал взвешиваний</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="64"/>
        <source>Фильтры </source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="93"/>
        <source>Дата заезда автомобиля 
       (конечная дата)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="162"/>
        <source>     Печать
  накладной</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="176"/>
        <source>Печать отчёта</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="189"/>
        <source>Экспорт в Excel</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="202"/>
        <source>Создать ТТН</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="256"/>
        <source>Время заезда автомобиля 
       (конечное время)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="303"/>
        <source>Применить фильтр</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="322"/>
        <source>Сбросить фильтр</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="360"/>
        <source>Дата заезда автомобиля 
       (начальная дата)</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="426"/>
        <source>Поставщик</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="439"/>
        <source>Номер весов</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="484"/>
        <source>Материал</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="519"/>
        <source>Завершённые</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="524"/>
        <source>Незавершённые</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="538"/>
        <source>Номер автомобиля</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="580"/>
        <source>Ф.И.О. водителя</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="587"/>
        <source>Номер карты</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="594"/>
        <source>Завершённость взвешивания</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/reports_rfid.ui" line="615"/>
        <source>Время заезда автомобиля 
      (начальное время)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="106"/>
        <source>Datetime
accept</source>
        <translation>Дата/час
прибуття</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="107"/>
        <source>Datetime
send</source>
        <translation>Дата/час
вибуття</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="109"/>
        <source>Weight 1</source>
        <translation>Зважування 1</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="110"/>
        <source>Weight 2</source>
        <translation>Зважування 2</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="111"/>
        <source>Net</source>
        <translation>Нетто</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="112"/>
        <source>Photo</source>
        <translation>Фото</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="113"/>
        <source>Save Photo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="114"/>
        <source>RFID Code</source>
        <translation>Код карти</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="115"/>
        <source>Supplier</source>
        <translation>Постачальник</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="116"/>
        <source>Material</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="117"/>
        <source>Vehicle
number</source>
        <translation>Номер
авто</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="118"/>
        <source>Trailer
number</source>
        <translation>Номер
причепа</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="119"/>
        <source>Driver</source>
        <translation>Водій</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="120"/>
        <source>Order number</source>
        <translation>Порядк. номер</translation>
    </message>
    <message>
        <location filename="../include/reports_rfid.h" line="121"/>
        <source>Type of container</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Blocked</source>
        <translation type="obsolete">Блокована</translation>
    </message>
</context>
<context>
    <name>spravochnik_tara</name>
    <message>
        <location filename="../forms/spravochnik_tara.ui" line="26"/>
        <source>Tara directory</source>
        <translation>Довідник тари</translation>
    </message>
    <message>
        <location filename="../forms/spravochnik_tara.ui" line="53"/>
        <location filename="../src/spravochnik_tara.cpp" line="34"/>
        <source>Add</source>
        <translation>Додати</translation>
    </message>
    <message>
        <location filename="../forms/spravochnik_tara.ui" line="69"/>
        <location filename="../src/spravochnik_tara.cpp" line="117"/>
        <source>Modify</source>
        <translation>Редагувати</translation>
    </message>
    <message>
        <location filename="../forms/spravochnik_tara.ui" line="85"/>
        <source>Delete</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <location filename="../forms/spravochnik_tara.ui" line="101"/>
        <location filename="../src/spravochnik_tara.cpp" line="79"/>
        <location filename="../src/spravochnik_tara.cpp" line="172"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
    <message>
        <location filename="../include/spravochnik_tara.h" line="26"/>
        <source>Datetime</source>
        <translation>Час</translation>
    </message>
    <message>
        <location filename="../include/spravochnik_tara.h" line="27"/>
        <location filename="../src/spravochnik_tara.cpp" line="36"/>
        <location filename="../src/spravochnik_tara.cpp" line="123"/>
        <source>Vehicle number</source>
        <translation>Номер авто</translation>
    </message>
    <message>
        <location filename="../include/spravochnik_tara.h" line="28"/>
        <location filename="../src/spravochnik_tara.cpp" line="45"/>
        <location filename="../src/spravochnik_tara.cpp" line="135"/>
        <source>Brand</source>
        <translation>Марка</translation>
    </message>
    <message>
        <location filename="../include/spravochnik_tara.h" line="29"/>
        <location filename="../src/spravochnik_tara.cpp" line="56"/>
        <location filename="../src/spravochnik_tara.cpp" line="148"/>
        <source>Tara weight</source>
        <translation>Вага тари</translation>
    </message>
    <message>
        <location filename="../src/spravochnik_tara.cpp" line="71"/>
        <location filename="../src/spravochnik_tara.cpp" line="163"/>
        <source>Refresh</source>
        <translation>Оновити</translation>
    </message>
    <message>
        <location filename="../src/spravochnik_tara.cpp" line="75"/>
        <location filename="../src/spravochnik_tara.cpp" line="168"/>
        <source>Confirm</source>
        <translation>Підтвердити</translation>
    </message>
    <message>
        <location filename="../src/spravochnik_tara.cpp" line="90"/>
        <location filename="../src/spravochnik_tara.cpp" line="100"/>
        <location filename="../src/spravochnik_tara.cpp" line="182"/>
        <location filename="../src/spravochnik_tara.cpp" line="192"/>
        <source>Error</source>
        <translation>Помилка</translation>
    </message>
    <message>
        <location filename="../src/spravochnik_tara.cpp" line="90"/>
        <location filename="../src/spravochnik_tara.cpp" line="182"/>
        <source>Not all fields are filled</source>
        <translation>Не всі поля заповнені</translation>
    </message>
    <message>
        <location filename="../src/spravochnik_tara.cpp" line="96"/>
        <location filename="../src/spravochnik_tara.cpp" line="188"/>
        <source>Data entry</source>
        <translation>Ввід даних</translation>
    </message>
    <message>
        <location filename="../src/spravochnik_tara.cpp" line="96"/>
        <location filename="../src/spravochnik_tara.cpp" line="188"/>
        <source>Data is written to the directory!</source>
        <translation>Збережено у довідник!</translation>
    </message>
    <message>
        <location filename="../src/spravochnik_tara.cpp" line="100"/>
        <location filename="../src/spravochnik_tara.cpp" line="192"/>
        <source>The car number already entered!</source>
        <translation>Такий номер авто вже записаний!</translation>
    </message>
</context>
</TS>
